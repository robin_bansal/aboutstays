package com.aboutstays.request.dto;

public class UpdateRoomCategoryRequest extends AddRoomCategoryRequest {
	private String roomCategoryId;

	public String getRoomCategoryId() {
		return roomCategoryId;
	}

	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
}
