package com.aboutstays.request.dto;

public class DeleteEntertaimentServiceRequest {

	private String serviceId;
	
	public String getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
}
