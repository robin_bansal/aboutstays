package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.RoomWithAmenityIds;

public class BatchUpdateAmenitiesRequest {
	
	private List<RoomWithAmenityIds> dataToUpdate;

	public List<RoomWithAmenityIds> getDataToUpdate() {
		return dataToUpdate;
	}
	public void setDataToUpdate(List<RoomWithAmenityIds> dataToUpdate) {
		this.dataToUpdate = dataToUpdate;
	}
}
