package com.aboutstays.request.dto;

import com.aboutstays.pojos.EntertaimentItemPojo;
import com.aboutstays.response.dto.GetMasterChannelResponse;

public class GetEntertaimentItemResponse extends EntertaimentItemPojo {

	private String itemId;
	private GetMasterChannelResponse itemDetails;
	
	public String getItemId() {
		return itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public GetMasterChannelResponse getItemDetails() {
		return itemDetails;
	}
	
	public void setItemDetails(GetMasterChannelResponse itemDetails) {
		this.itemDetails = itemDetails;
	}
}
