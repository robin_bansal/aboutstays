package com.aboutstays.request.dto;

public class RoomgsAndStaysRequest {
	private String date;
	private String floorId;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
}
