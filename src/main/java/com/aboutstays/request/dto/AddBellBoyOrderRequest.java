package com.aboutstays.request.dto;

import com.aboutstays.pojos.BellBoyOrderPojo;
import com.aboutstays.utils.JsonUtil;

public class AddBellBoyOrderRequest extends BellBoyOrderPojo {

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "AddBellBoyOrderRequest [getStatus()=" + getStatus() + ", getDeliveryTime()=" + getDeliveryTime()
					+ ", getComment()=" + getComment() + ", isRequestedNow()=" + isRequestedNow()
					+ ", getDeliverTimeString()=" + getDeliverTimeString() + ", getGsId()=" + getGsId()
					+ ", toString()=" + super.toString() + ", getUserId()=" + getUserId() + ", getHotelId()="
					+ getHotelId() + ", getStaysId()=" + getStaysId() + ", getClass()=" + getClass() + ", hashCode()="
					+ hashCode() + "]";
		}
	}

}
