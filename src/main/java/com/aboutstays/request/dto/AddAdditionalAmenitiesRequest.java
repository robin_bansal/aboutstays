package com.aboutstays.request.dto;

import java.util.List;

public class AddAdditionalAmenitiesRequest {

	private String roomId;
	private List<String> amenityIds;
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public List<String> getAmenityIds() {
		return amenityIds;
	}
	public void setAmenityIds(List<String> amenityIds) {
		this.amenityIds = amenityIds;
	}
	
}
