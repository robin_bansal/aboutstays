package com.aboutstays.request.dto;

import com.aboutstays.pojos.ReservationItemPojo;

public class AddReservationItemRequest extends ReservationItemPojo{

	private String hotelId;
	private String categoryId;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
}
