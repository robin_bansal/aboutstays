package com.aboutstays.request.dto;

import com.aboutstays.pojos.UserOptedServicePojo;
import com.aboutstays.utils.JsonUtil;

public class UpdateUserOptedServiceRequest extends UserOptedServicePojo {

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "UpdateUserOptedServiceRequest [getId()=" + getId() + ", getRefId()=" + getRefId()
					+ ", getRefCollectionType()=" + getRefCollectionType() + ", getGsId()=" + getGsId() + ", getSfId()="
					+ getSfId() + ", getUserId()=" + getUserId() + ", getHotelId()=" + getHotelId() + ", getStaysId()="
					+ getStaysId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
					+ super.toString() + "]";
		}
	}

}
