package com.aboutstays.request.dto;

import com.aboutstays.pojos.TypeValuePojo;

public class UpdatePasswordRequest extends TypeValuePojo {

	private String password;

	public UpdatePasswordRequest(Integer type, String value, String password) {
		super(type, value);
		this.password = password;
	}
	
	public UpdatePasswordRequest(){}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
