package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.RoomMetaInfo;

public class BatchUpdateRoomMetaInfoRequest {

	private String hotelId;
	private List<RoomMetaInfo> dataToUpdate;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<RoomMetaInfo> getDataToUpdate() {
		return dataToUpdate;
	}
	public void setDataToUpdate(List<RoomMetaInfo> dataToUpdate) {
		this.dataToUpdate = dataToUpdate;
	}
	
	
}
