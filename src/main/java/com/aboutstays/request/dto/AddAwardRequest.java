package com.aboutstays.request.dto;

import com.aboutstays.pojos.HotelAward;

public class AddAwardRequest {
private String hotelId;
private HotelAward awardData;
public String getHotelId() {
	return hotelId;
}
public void setHotelId(String hotelId) {
	this.hotelId = hotelId;
}
public HotelAward getAwardData() {
	return awardData;
}
public void setAwardData(HotelAward awardData) {
	this.awardData = awardData;
}

}
