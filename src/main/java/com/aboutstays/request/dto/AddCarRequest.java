package com.aboutstays.request.dto;

import com.aboutstays.pojos.CarPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddCarRequest extends CarPojo{

}
