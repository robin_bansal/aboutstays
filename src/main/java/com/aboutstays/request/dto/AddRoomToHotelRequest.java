package com.aboutstays.request.dto;

public class AddRoomToHotelRequest{
	
	private String hotelId;
	private AddRoomCategoryRequest addRoomRequest;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public AddRoomCategoryRequest getAddRoomRequest() {
		return addRoomRequest;
	}
	public void setAddRoomRequest(AddRoomCategoryRequest addRoomRequest) {
		this.addRoomRequest = addRoomRequest;
	}

}
