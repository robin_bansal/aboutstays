package com.aboutstays.request.dto;

import com.aboutstays.pojos.MasterChannelItemPojo;

public class AddMasterChannelRequest extends MasterChannelItemPojo {

	private String genereId;
	
	public String getGenereId() {
		return genereId;
	}
	
	public void setGenereId(String genereId) {
		this.genereId = genereId;
	}
	
}
