package com.aboutstays.request.dto;

public class FetchReservationItemsByServiceIdRequest {

	private String rsId;
	
	public String getRsId() {
		return rsId;
	}
	
	public void setRsId(String rsId) {
		this.rsId = rsId;
	}
	
}
