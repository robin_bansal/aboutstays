package com.aboutstays.request.dto;

import java.util.List;

public class BatchUpdateReservationItemRequest {

	private List<UpdateReservationItemRequest> items;
	
	public List<UpdateReservationItemRequest> getItems() {
		return items;
	}
	
	public void setItems(List<UpdateReservationItemRequest> items) {
		this.items = items;
	}
}
