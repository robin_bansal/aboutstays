package com.aboutstays.request.dto;

public class UpdateReservationItemRequest extends AddReservationItemRequest{
	
	private String id;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
}
