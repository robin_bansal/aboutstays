package com.aboutstays.request.dto;

import com.aboutstays.pojos.PlacePojo;

public class PlaceRequest extends PlacePojo {

	private String placeId;
	
	public String getPlaceId() {
		return placeId;
	}
	
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
}
