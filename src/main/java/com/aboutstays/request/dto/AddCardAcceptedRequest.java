package com.aboutstays.request.dto;

import java.util.List;

public class AddCardAcceptedRequest {
	private String hotelId;
	private List<String> cardIds;
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<String> getCardIds() {
		return cardIds;
	}
	public void setCardIds(List<String> cardIds) {
		this.cardIds = cardIds;
	}

}
