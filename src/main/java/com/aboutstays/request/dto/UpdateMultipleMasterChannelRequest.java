package com.aboutstays.request.dto;

import java.util.List;

public class UpdateMultipleMasterChannelRequest {

	private List<UpdateMasterChannelRequest> channels;
	
	public List<UpdateMasterChannelRequest> getChannels() {
		return channels;
	}
	
	public void setChannels(List<UpdateMasterChannelRequest> channels) {
		this.channels = channels;
	}
}
