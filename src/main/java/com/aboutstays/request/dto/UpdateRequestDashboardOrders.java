package com.aboutstays.request.dto;

import java.util.Map;

public class UpdateRequestDashboardOrders {
	
	private String uosId;
	private Map<String, Integer> mapItemIdVSQuantity; 
	
	public String getUosId() {
		return uosId;
	}
	public void setUosId(String uosId) {
		this.uosId = uosId;
	}
	public Map<String, Integer> getMapItemIdVSQuantity() {
		return mapItemIdVSQuantity;
	}
	public void setMapItemIdVSQuantity(Map<String, Integer> mapItemIdVSQuantity) {
		this.mapItemIdVSQuantity = mapItemIdVSQuantity;
	}
	
	
	
	

}
