package com.aboutstays.request.dto;

import com.aboutstays.pojos.TypeValuePojo;

public class LoginRequest extends TypeValuePojo{
	
	private String password;
	private Integer loginType;
	private SignupRequest signupRequest;
	
	public LoginRequest(){}

	public LoginRequest(Integer type, String value, String password, SignupRequest signupRequest) {
		super(type, value);
		this.password = password;
		this.signupRequest = signupRequest;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SignupRequest getSignupRequest() {
		return signupRequest;
	}

	public void setSignupRequest(SignupRequest signupRequest) {
		this.signupRequest = signupRequest;
	}
	public Integer getLoginType() {
		return loginType;
	}
	
	public void setLoginType(Integer loginType) {
		this.loginType = loginType;
	}
	
}
