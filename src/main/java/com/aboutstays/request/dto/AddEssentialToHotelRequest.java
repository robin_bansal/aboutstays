package com.aboutstays.request.dto;

import java.util.List;

public class AddEssentialToHotelRequest {

	private String hotelId;
	private List<String> essentialsIds;
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<String> getEssentialsIds() {
		return essentialsIds;
	}
	public void setEssentialsIds(List<String> assentialsIds) {
		this.essentialsIds = assentialsIds;
	}
	
}
