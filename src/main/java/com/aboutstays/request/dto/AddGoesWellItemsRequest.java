package com.aboutstays.request.dto;

import java.util.List;

public class AddGoesWellItemsRequest {

	private String foodItemId;
	private List<String> goesWellWithItemIds;
	public String getFoodItemId() {
		return foodItemId;
	}
	public void setFoodItemId(String foodItemId) {
		this.foodItemId = foodItemId;
	}
	public List<String> getGoesWellWithItemIds() {
		return goesWellWithItemIds;
	}
	public void setGoesWellWithItemIds(List<String> goesWellWithItemIds) {
		this.goesWellWithItemIds = goesWellWithItemIds;
	}
	
}
