package com.aboutstays.request.dto;

public class ApproveEarlyCheckinRequest {

	private String bookingId;
	private double price;
	
	public String getBookingId() {
		return bookingId;
	}
	
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
