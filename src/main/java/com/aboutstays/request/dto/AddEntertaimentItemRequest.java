package com.aboutstays.request.dto;

import com.aboutstays.pojos.EntertaimentItemPojo;

public class AddEntertaimentItemRequest extends EntertaimentItemPojo {

	private String masterItemId;

	public String getMasterItemId() {
		return masterItemId;
	}
	
	public void setMasterItemId(String masterItemId) {
		this.masterItemId = masterItemId;
	}

	
	
}
