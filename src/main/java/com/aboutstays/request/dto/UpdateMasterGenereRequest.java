package com.aboutstays.request.dto;

import com.aboutstays.pojos.MasterGenereItemPojo;

public class UpdateMasterGenereRequest extends MasterGenereItemPojo {

	private String masterGenereId;
	
	public String getMasterGenereId() {
		return masterGenereId;
	}
	
	public void setMasterGenereId(String masterGenereId) {
		this.masterGenereId = masterGenereId;
	}
}
