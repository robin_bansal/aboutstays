package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.HotelRequestsPojo;

public class GetRequestsResponse {
	private List<HotelRequestsPojo> requestData;
	private int allPendingApproval;
	private int allOnGoingUpGoing;
	private int allCompleted;

	public List<HotelRequestsPojo> getRequestData() {
		return requestData;
	}

	public void setRequestData(List<HotelRequestsPojo> requestData) {
		this.requestData = requestData;
	}

	public int getAllPendingApproval() {
		return allPendingApproval;
	}

	public void setAllPendingApproval(int allPendingApproval) {
		this.allPendingApproval = allPendingApproval;
	}

	public int getAllOnGoingUpGoing() {
		return allOnGoingUpGoing;
	}

	public void setAllOnGoingUpGoing(int allOnGoingUpGoing) {
		this.allOnGoingUpGoing = allOnGoingUpGoing;
	}

	public int getAllCompleted() {
		return allCompleted;
	}

	public void setAllCompleted(int allCompleted) {
		this.allCompleted = allCompleted;
	}

	public void updateCount(HotelRequestsPojo hotelRequestsPojo) {
		allPendingApproval += hotelRequestsPojo.getPendingApproval();
		allOnGoingUpGoing += hotelRequestsPojo.getOnGoingUpGoing();
		allCompleted += hotelRequestsPojo.getCompleted();
	}

}
