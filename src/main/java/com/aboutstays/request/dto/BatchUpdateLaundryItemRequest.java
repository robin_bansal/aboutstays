package com.aboutstays.request.dto;

import java.util.List;

public class BatchUpdateLaundryItemRequest {

	private List<LaundryItemRequest> items;
	
	public List<LaundryItemRequest> getItems() {
		return items;
	}
	
	public void setItems(List<LaundryItemRequest> items) {
		this.items = items;
	}
}
