package com.aboutstays.request.dto;

import com.aboutstays.pojos.AirportPickupPojo;
import com.aboutstays.utils.JsonUtil;

public class UpdateAirportPickupRequest extends AirportPickupPojo{

	@Override
	public String toString() {
		try{
			return JsonUtil.getJson(this);
		}catch (Exception e) {
			return "UpdateAirportPickupRequest [getId()=" + getId() + ", getCarId()=" + getCarId() + ", getDriverId()="
					+ getDriverId() + ", getAirportName()=" + getAirportName() + ", getAirlineName()=" + getAirlineName()
					+ ", getFlightNumber()=" + getFlightNumber() + ", getPickupDateAndTime()=" + getPickupDateAndTime()
					+ ", getComment()=" + getComment() + ", getCarType()=" + getCarType() + ", getPrice()=" + getPrice()
					+ ", getDrop()=" + getDrop() + ", getPickupStatus()=" + getPickupStatus() + ", getDeliverTimeString()="
					+ getDeliverTimeString() + ", getUserId()=" + getUserId() + ", getHotelId()=" + getHotelId()
					+ ", getStayId()=" + getStaysId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
					+ ", toString()=" + super.toString() + "]";
		}
	}
	
	
}
