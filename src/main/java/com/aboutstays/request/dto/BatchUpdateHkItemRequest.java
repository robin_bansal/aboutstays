package com.aboutstays.request.dto;

import java.util.List;

public class BatchUpdateHkItemRequest {

	private List<HousekeepingItemRequest> items;
	
	public List<HousekeepingItemRequest> getItems() {
		return items;
	}
	
	public void setItems(List<HousekeepingItemRequest> items) {
		this.items = items;
	}
}
