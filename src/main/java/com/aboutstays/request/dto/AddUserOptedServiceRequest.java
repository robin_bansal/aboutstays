package com.aboutstays.request.dto;

import com.aboutstays.pojos.UserOptedServicePojo;
import com.aboutstays.utils.JsonUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddUserOptedServiceRequest extends UserOptedServicePojo {

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "AddUserOptedServiceRequest [getId()=" + getId() + ", getRefId()=" + getRefId()
					+ ", getRefCollectionType()=" + getRefCollectionType() + ", getGsId()=" + getGsId() + ", getSfId()="
					+ getSfId() + ", getUserId()=" + getUserId() + ", getHotelId()=" + getHotelId() + ", getStaysId()="
					+ getStaysId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
					+ super.toString() + "]";
		}
	}

}
