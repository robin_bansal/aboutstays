package com.aboutstays.request.dto;

import java.util.List;

public class AddAmenitiesToHotelRequest {

	private String hotelId;
	private List<String> amenityIds;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<String> getAmenityIds() {
		return amenityIds;
	}
	public void setAmenityIds(List<String> amenityIds) {
		this.amenityIds = amenityIds;
	}
	
}
