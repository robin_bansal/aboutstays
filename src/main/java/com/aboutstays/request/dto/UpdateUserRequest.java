package com.aboutstays.request.dto;

import com.aboutstays.pojos.UserPojo;

public class UpdateUserRequest extends UserPojo {
	
	private boolean otpValidated;
	private String referralCode;
	private Integer type;
	private String value;
	
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public boolean isOtpValidated() {
		return otpValidated;
	}
	
	public void setOtpValidated(boolean otpValidated) {
		this.otpValidated = otpValidated;
	}

}
