package com.aboutstays.request.dto;

import com.aboutstays.pojos.UserPasswordPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class SignupRequest extends UserPasswordPojo {


	private String referralCode;
	private boolean otpValidated;
	private Integer signupType;
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public boolean isOtpValidated() {
		return otpValidated;
	}

	public void setOtpValidated(boolean otpValidated) {
		this.otpValidated = otpValidated;
	}

	public Integer getSignupType() {
		return signupType;
	}

	public void setSignupType(Integer signupType) {
		this.signupType = signupType;
	}
	
}
