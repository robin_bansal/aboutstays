package com.aboutstays.request.dto;

import java.util.List;

public class BatchUpdateFoodItemsRequest {

	private List<UpdateFoodItemRequest> items;
	
	public List<UpdateFoodItemRequest> getItems() {
		return items;
	}
	public void setItems(List<UpdateFoodItemRequest> items) {
		this.items = items;
	}
}
