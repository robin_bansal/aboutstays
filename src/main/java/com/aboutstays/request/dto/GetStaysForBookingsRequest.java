package com.aboutstays.request.dto;

import java.util.List;

public class GetStaysForBookingsRequest {

	private String userId;
	private List<String> bookingIds;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<String> getBookingIds() {
		return bookingIds;
	}
	public void setBookingIds(List<String> bookingIds) {
		this.bookingIds = bookingIds;
	}
}
