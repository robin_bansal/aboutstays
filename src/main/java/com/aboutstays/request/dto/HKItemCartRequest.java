package com.aboutstays.request.dto;

import com.aboutstays.pojos.CartParams;
import com.aboutstays.pojos.HousekeepingItemOrderDetails;

public class HKItemCartRequest extends CartParams{
	
	private HousekeepingItemOrderDetails hkItemOrderDetails;

	public HousekeepingItemOrderDetails getHkItemOrderDetails() {
		return hkItemOrderDetails;
	}

	public void setHkItemOrderDetails(HousekeepingItemOrderDetails hkItemOrderDetails) {
		this.hkItemOrderDetails = hkItemOrderDetails;
	}
	
	

}
