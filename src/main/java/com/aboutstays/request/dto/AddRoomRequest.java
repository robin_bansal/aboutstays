package com.aboutstays.request.dto;

import com.aboutstays.pojos.RoomPojo;

public class AddRoomRequest extends RoomPojo{

	private String roomCategoryId;
	
	public String getRoomCategoryId() {
		return roomCategoryId;
	}

	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
}
