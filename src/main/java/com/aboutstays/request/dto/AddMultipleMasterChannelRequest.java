package com.aboutstays.request.dto;

import java.util.List;

public class AddMultipleMasterChannelRequest {

	private List<AddMasterChannelRequest> channels;
	
	public List<AddMasterChannelRequest> getChannels() {
		return channels;
	}
	
	public void setChannels(List<AddMasterChannelRequest> channels) {
		this.channels = channels;
	}
}
