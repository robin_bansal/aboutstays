package com.aboutstays.request.dto;

public class SocketMessageRequest {
	private int socketType;
	private String fromId;
	private String toId;
	private long messageId;
	private String metaData;
	
	public int getSocketType() {
		return socketType;
	}
	public void setSocketType(int socketType) {
		this.socketType = socketType;
	}
	public String getFromId() {
		return fromId;
	}
	public void setFromId(String fromId) {
		this.fromId = fromId;
	}
	public String getToId() {
		return toId;
	}
	public void setToId(String toId) {
		this.toId = toId;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public String getMetaData() {
		return metaData;
	}
	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}
	
}
