package com.aboutstays.request.dto;

public class CompleteCheckinRequest {
	private String bookingId;
	private Boolean checkinFormCompleted;
	private Boolean idVerified;
	private Boolean isEarlyCheckinFee;
	private Double earlyCheckinFee;
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public Boolean getCheckinFormCompleted() {
		return checkinFormCompleted;
	}
	public void setCheckinFormCompleted(Boolean checkinFormCompleted) {
		this.checkinFormCompleted = checkinFormCompleted;
	}
	public Boolean getIdVerified() {
		return idVerified;
	}
	public void setIdVerified(Boolean idVerified) {
		this.idVerified = idVerified;
	}
	public Double getEarlyCheckinFee() {
		return earlyCheckinFee;
	}
	public void setEarlyCheckinFee(Double earlyCheckinFee) {
		this.earlyCheckinFee = earlyCheckinFee;
	}
	public Boolean getIsEarlyCheckinFee() {
		return isEarlyCheckinFee;
	}
	public void setIsEarlyCheckinFee(Boolean isEarlyCheckinFee) {
		this.isEarlyCheckinFee = isEarlyCheckinFee;
	}
}
