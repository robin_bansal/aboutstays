package com.aboutstays.request.dto;

import com.aboutstays.pojos.FoodOrderRequestPojo;
import com.aboutstays.utils.JsonUtil;

public class UpdateFoodOrderRequest extends FoodOrderRequestPojo {

	private String id;
	private Integer status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "UpdateFoodOrderRequest [id=" + id + ", status=" + status + ", getId()=" + getId() + ", getStatus()="
					+ getStatus() + ", getFoodItemOrders()=" + getFoodItemOrders() + ", getDeliveryTime()="
					+ getDeliveryTime() + ", getComment()=" + getComment() + ", isRequestedNow()=" + isRequestedNow()
					+ ", getDeliverTimeString()=" + getDeliverTimeString() + ", getGsId()=" + getGsId()
					+ ", toString()=" + super.toString() + ", getUserId()=" + getUserId() + ", getHotelId()="
					+ getHotelId() + ", getStaysId()=" + getStaysId() + ", getClass()=" + getClass() + ", hashCode()="
					+ hashCode() + "]";
		}
	}

}
