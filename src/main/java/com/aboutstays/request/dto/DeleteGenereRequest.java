package com.aboutstays.request.dto;

public class DeleteGenereRequest {

	private String masterGenereId;
	
	public String getMasterGenereId() {
		return masterGenereId;
	}
	
	public void setMasterGenereId(String masterGenereId) {
		this.masterGenereId = masterGenereId;
	}
}
