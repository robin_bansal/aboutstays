package com.aboutstays.request.dto;

import java.util.List;

public class AddAllMasterCitiesRequest {

	private List<AddMasterCityRequest> masterCities;
	
	public List<AddMasterCityRequest> getMasterCities() {
		return masterCities;
	}
	
	public void setMasterCities(List<AddMasterCityRequest> masterCities) {
		this.masterCities = masterCities;
	}
}
