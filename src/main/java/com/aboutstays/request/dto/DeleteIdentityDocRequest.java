package com.aboutstays.request.dto;

import com.aboutstays.pojos.IdentityDoc;

public class DeleteIdentityDocRequest {

	private String userId;
	private IdentityDoc identityDoc;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public IdentityDoc getIdentityDoc() {
		return identityDoc;
	}
	public void setIdentityDoc(IdentityDoc identityDoc) {
		this.identityDoc = identityDoc;
	}
	
	
}
