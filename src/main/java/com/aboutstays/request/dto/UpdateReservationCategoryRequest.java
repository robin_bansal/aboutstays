package com.aboutstays.request.dto;

public class UpdateReservationCategoryRequest extends AddReservationCategoryRequest {

	private String categoryId;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

}
