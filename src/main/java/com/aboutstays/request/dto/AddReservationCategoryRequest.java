package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.Direction;
import com.aboutstays.pojos.ReservationCategoryPojo;

public class AddReservationCategoryRequest extends ReservationCategoryPojo {

	private List<Direction> directionList;
	
	public List<Direction> getDirectionList() {
		return directionList;
	}
	public void setDirectionList(List<Direction> directionList) {
		this.directionList = directionList;
	}
}
