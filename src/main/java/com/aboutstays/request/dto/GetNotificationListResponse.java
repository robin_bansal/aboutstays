package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.response.dto.GetNotificationResponse;

public class GetNotificationListResponse {

	private List<GetNotificationResponse> notificationsList;
	
	public List<GetNotificationResponse> getNotificationsList() {
		return notificationsList;
	}
	
	public void setNotificationsList(List<GetNotificationResponse> notificationsList) {
		this.notificationsList = notificationsList;
	}
}
