package com.aboutstays.request.dto;

import com.aboutstays.pojos.MessagePojo;

public class AddMessageRequest extends MessagePojo {

	private int creationType;
	
	public int getCreationType() {
		return creationType;
	}
	
	public void setCreationType(int creationType) {
		this.creationType = creationType;
	}
}
