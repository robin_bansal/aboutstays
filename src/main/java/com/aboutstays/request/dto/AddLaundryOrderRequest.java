package com.aboutstays.request.dto;

import com.aboutstays.pojos.LaundryOrderPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddLaundryOrderRequest extends LaundryOrderPojo{

}
