package com.aboutstays.request.dto;

import com.aboutstays.pojos.TypeValuePojo;

public class ValidateOtpRequest extends TypeValuePojo {
	
	private boolean validated;
	
	public ValidateOtpRequest(){};

	public ValidateOtpRequest(Integer type, String value, boolean validated) {
		super(type, value);
		this.validated = validated;
	}



	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}



	

	
	
	

}
