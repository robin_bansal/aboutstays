package com.aboutstays.request.dto;

public class UpdateMasterChannelRequest extends AddMasterChannelRequest {

	private String channelId;
	
	public String getChannelId() {
		return channelId;
	}
	
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
}
