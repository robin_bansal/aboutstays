package com.aboutstays.request.dto;

import com.aboutstays.pojos.HotelIdRoomIdPojo;

public class DeleteRoomRequest extends HotelIdRoomIdPojo{
	private String categoryId;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

}
