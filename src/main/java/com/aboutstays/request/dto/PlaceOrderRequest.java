package com.aboutstays.request.dto;

import com.aboutstays.pojos.BaseUserOrderRequestPojo;

public class PlaceOrderRequest extends BaseUserOrderRequestPojo{

	private Integer cartType;
	private int laundryPrefs;
	
	public Integer getCartType() {
		return cartType;
	}
	
	public void setCartType(Integer cartType) {
		this.cartType = cartType;
	}

	public int getLaundryPrefs() {
		return laundryPrefs;
	}

	public void setLaundryPrefs(int laundryPrefs) {
		this.laundryPrefs = laundryPrefs;
	}
	
	
	
}
