package com.aboutstays.request.dto;

import com.aboutstays.pojos.GeneralServicePojo;

public class UpdateGeneralServiceRequest extends GeneralServicePojo{
	
	private Integer serviceType;

	public Integer getServiceType() {
		return serviceType;
	}
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}

	
	

}
