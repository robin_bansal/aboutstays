package com.aboutstays.request.dto;

import com.aboutstays.pojos.AirportPickupPojo;
import com.aboutstays.pojos.CarPojo;
import com.aboutstays.pojos.DriverPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class GetAirportPickupResponse extends AirportPickupPojo{
	
	private CarPojo car;
	private DriverPojo driver;
	private int status;
	
	public CarPojo getCar() {
		return car;
	}
	public void setCar(CarPojo car) {
		this.car = car;
	}
	public DriverPojo getDriver() {
		return driver;
	}
	public void setDriver(DriverPojo driver) {
		this.driver = driver;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	
}
