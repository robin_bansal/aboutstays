package com.aboutstays.request.dto;

public class UpdateComplementaryServicesRequest extends AddComplementaryServicesRequest {

	private String servicesId;
	
	public String getServicesId() {
		return servicesId;
	}
	
	public void setServicesId(String servicesId) {
		this.servicesId = servicesId;
	}
}
