package com.aboutstays.request.dto;

import com.aboutstays.pojos.AirportPickupPojo;
import com.aboutstays.utils.JsonUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddAirportPickupRequest extends AirportPickupPojo{
	
	private String gsId;

	public String getGsId() {
		return gsId;
	}

	public void setGsId(String gsId) {
		this.gsId = gsId;
	}

	@Override
	public String toString() {
		try{
			return JsonUtil.getJson(this);
		} catch(Exception e){
			return "AddAirportPickupRequest [gsId=" + gsId + ", getId()=" + getId() + ", getCarId()=" + getCarId()
			+ ", getDriverId()=" + getDriverId() + ", getAirportName()=" + getAirportName() + ", getAirlineName()="
			+ getAirlineName() + ", getFlightNumber()=" + getFlightNumber() + ", getPickupDateAndTime()="
			+ getPickupDateAndTime() + ", getComment()=" + getComment() + ", getCarType()=" + getCarType()
			+ ", getPrice()=" + getPrice() + ", getDrop()=" + getDrop() + ", getPickupStatus()=" + getPickupStatus()
			+ ", getDeliverTimeString()=" + getDeliverTimeString() + ", getUserId()=" + getUserId()
			+ ", getHotelId()=" + getHotelId() + ", getStayId()=" + getStaysId() + ", getClass()=" + getClass()
			+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
		}
	}
	
	

}
