package com.aboutstays.request.dto;

import java.util.List;

public class UpdateHotelServices {

	private String hotelId;
	private Integer status;
	private List<String> listGSIds;
//	private Map<String, String> mapGSIdVsHSId;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<String> getListGSIds() {
		return listGSIds;
	}
	public void setListGSIds(List<String> listGSIds) {
		this.listGSIds = listGSIds;
	}
//	public Map<String, String> getMapGSIdVsHSId() {
//		return mapGSIdVsHSId;
//	}
//	public void setMapGSIdVsHSId(Map<String, String> mapGSIdVsHSId) {
//		this.mapGSIdVsHSId = mapGSIdVsHSId;
//	}
	
	
}
