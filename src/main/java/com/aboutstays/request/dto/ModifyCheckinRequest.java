package com.aboutstays.request.dto;

import com.aboutstays.pojos.CheckinPojo;

public class ModifyCheckinRequest extends CheckinPojo {

	private Long created;
	
	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}
	
	
}
