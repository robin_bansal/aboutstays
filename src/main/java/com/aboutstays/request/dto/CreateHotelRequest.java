package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.HotelPojo;

public class CreateHotelRequest extends HotelPojo{

	private List<String> essentialIds;
	
	public List<String> getEssentialIds() {
		return essentialIds;
	}
	
	public void setEssentialIds(List<String> essentialIds) {
		this.essentialIds = essentialIds;
	}
}
