package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.FoodItemPojo;
import com.aboutstays.pojos.GoesWellWithItem;

public class FoodItemRequest extends FoodItemPojo {

	private List<GoesWellWithItem> listGoesWellItems;
	
	public List<GoesWellWithItem> getListGoesWellItems() {
		return listGoesWellItems;
	}
	public void setListGoesWellItems(List<GoesWellWithItem> listGoesWellItems) {
		this.listGoesWellItems = listGoesWellItems;
	}
}
