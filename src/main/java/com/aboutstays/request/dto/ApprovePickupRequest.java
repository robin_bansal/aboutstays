package com.aboutstays.request.dto;

import com.aboutstays.utils.JsonUtil;

public class ApprovePickupRequest {

	private String id;
	private String carId;
	private String driverId;
	private Integer status;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "ApprovePickupRequest [id=" + id + ", carId=" + carId + ", driverId=" + driverId + ", status="
					+ status + "]";
		}
	}

}
