package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.FoodItemPojo;
import com.aboutstays.pojos.GoesWellWithItemPojo;

public class UpdateFoodItemRequest extends FoodItemRequest {

	private String fooditemId;
	public String getFooditemId() {
		return fooditemId;
	}
	public void setFooditemId(String fooditemId) {
		this.fooditemId = fooditemId;
	}
}
