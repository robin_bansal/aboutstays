package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.EntertaimentItemTypeInfo;
import com.aboutstays.pojos.EntertaimentServicePojo;

public class AddEntertaimentServiceRequest extends EntertaimentServicePojo {

	private List<EntertaimentItemTypeInfo> typeInfoList;
	
	public List<EntertaimentItemTypeInfo> getTypeInfoList() {
		return typeInfoList;
	}
	
	public void setTypeInfoList(List<EntertaimentItemTypeInfo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}
}
