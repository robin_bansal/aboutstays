package com.aboutstays.request.dto;

import com.aboutstays.pojos.LateCheckoutInfo;

public class SetLateCheckOutRoomRequest {
	
	private String roomCategoryId;
	private LateCheckoutInfo checkOutInfo;
	public String getRoomCategoryId() {
		return roomCategoryId;
	}
	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
	public LateCheckoutInfo getCheckOutInfo() {
		return checkOutInfo;
	}
	public void setCheckOutInfo(LateCheckoutInfo checkOutInfo) {
		this.checkOutInfo = checkOutInfo;
	}

}
