package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.StayPreferences;

public class UpdateStayPrefsRequest {
	
	private String userId;
	private List<StayPreferences> listStayPrefs;


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<StayPreferences> getListStayPrefs() {
		return listStayPrefs;
	}

	public void setListStayPrefs(List<StayPreferences> listStayPrefs) {
		this.listStayPrefs = listStayPrefs;
	}
	
	

}
