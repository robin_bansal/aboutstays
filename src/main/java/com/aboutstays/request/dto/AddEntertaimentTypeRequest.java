package com.aboutstays.request.dto;

import com.aboutstays.pojos.EntertaimentItemTypeInfo;

public class AddEntertaimentTypeRequest {

	private String serviceId;
	private EntertaimentItemTypeInfo typeInfo;
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public EntertaimentItemTypeInfo getTypeInfo() {
		return typeInfo;
	}
	public void setTypeInfo(EntertaimentItemTypeInfo typeInfo) {
		this.typeInfo = typeInfo;
	}
	
	
}
