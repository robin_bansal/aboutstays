package com.aboutstays.request.dto;

public class AllocateRoomRequest {
private String roomId;
private String bookingId;
public String getRoomId() {
	return roomId;
}
public void setRoomId(String roomId) {
	this.roomId = roomId;
}
public String getBookingId() {
	return bookingId;
}
public void setBookingId(String bookingId) {
	this.bookingId = bookingId;
}

}
