package com.aboutstays.request.dto;

import com.aboutstays.pojos.EarlyCheckinInfo;

public class SetEarlyCheckInRoomRequest {
	
	private String roomCategoryId;
	private EarlyCheckinInfo info;
	public String getRoomCategoryId() {
		return roomCategoryId;
	}
	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
	public EarlyCheckinInfo getInfo() {
		return info;
	}
	public void setInfo(EarlyCheckinInfo info) {
		this.info = info;
	}
	
	
}
