package com.aboutstays.request.dto;

import com.aboutstays.pojos.CheckoutPojo;

public class InitiateCheckoutRequest extends CheckoutPojo{

	private int paymentMethod;
	
	public int getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}
