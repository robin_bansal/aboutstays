package com.aboutstays.request.dto;

import com.aboutstays.pojos.UserStayHotelIdsPojo;

public class UserCartRequest<T> extends UserStayHotelIdsPojo{

	private T data;

	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
}
