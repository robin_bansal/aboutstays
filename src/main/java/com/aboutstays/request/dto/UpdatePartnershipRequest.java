package com.aboutstays.request.dto;

public class UpdatePartnershipRequest {

	private String hotelId;
	private Integer partnershipType;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getPartnershipType() {
		return partnershipType;
	}
	public void setPartnershipType(Integer partnershipType) {
		this.partnershipType = partnershipType;
	}
	
	
}
