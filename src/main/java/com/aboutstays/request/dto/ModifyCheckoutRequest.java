package com.aboutstays.request.dto;

import com.aboutstays.pojos.UserStayHotelIdsPojo;

public class ModifyCheckoutRequest extends UserStayHotelIdsPojo{
	
	private int paymentMethod;
	
	public int getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}
