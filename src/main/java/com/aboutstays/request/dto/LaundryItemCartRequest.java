package com.aboutstays.request.dto;

import com.aboutstays.pojos.CartParams;
import com.aboutstays.pojos.LaundryItemOrderDetails;

public class LaundryItemCartRequest extends CartParams{
	
	private LaundryItemOrderDetails itemOrderDetails;

	public LaundryItemOrderDetails getItemOrderDetails() {
		return itemOrderDetails;
	}

	public void setItemOrderDetails(LaundryItemOrderDetails itemOrderDetails) {
		this.itemOrderDetails = itemOrderDetails;
	}
	
}
