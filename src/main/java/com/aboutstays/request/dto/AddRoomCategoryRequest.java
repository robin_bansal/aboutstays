package com.aboutstays.request.dto;

import java.util.List;

import com.aboutstays.pojos.RoomCategoryPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddRoomCategoryRequest extends RoomCategoryPojo {
	
	private List<String> inRoomAmenities;
	private List<String> preferenceBasedAmenities;
	public List<String> getInRoomAmenities() {
		return inRoomAmenities;
	}
	public void setInRoomAmenities(List<String> inRoomAmenities) {
		this.inRoomAmenities = inRoomAmenities;
	}
	public List<String> getPreferenceBasedAmenities() {
		return preferenceBasedAmenities;
	}
	public void setPreferenceBasedAmenities(List<String> preferenceBasedAmenities) {
		this.preferenceBasedAmenities = preferenceBasedAmenities;
	}
}
