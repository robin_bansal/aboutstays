package com.aboutstays.request.dto;

import com.aboutstays.pojos.FoodOrderRequestPojo;
import com.aboutstays.utils.JsonUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddFoodOrderRequest extends FoodOrderRequestPojo{

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "AddFoodOrderRequest [getFoodItemOrders()=" + getFoodItemOrders() + ", getDeliveryTime()="
					+ getDeliveryTime() + ", getComment()=" + getComment() + ", isRequestedNow()=" + isRequestedNow()
					+ ", getDeliverTimeString()=" + getDeliverTimeString() + ", getGsId()=" + getGsId() + ", toString()="
					+ super.toString() + ", getUserId()=" + getUserId() + ", getHotelId()=" + getHotelId()
					+ ", getStaysId()=" + getStaysId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
		}
	}

}
