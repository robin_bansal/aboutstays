package com.aboutstays.request.dto;

import com.aboutstays.pojos.HousekeepingOrderPojo;

public class UpdateHKOrderRequest extends HousekeepingOrderPojo{
	
	private String id;
	private Integer status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
