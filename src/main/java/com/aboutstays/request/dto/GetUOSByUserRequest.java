package com.aboutstays.request.dto;

import com.aboutstays.utils.JsonUtil;

public class GetUOSByUserRequest {

	private String userId;
	private String stayId;
	private String gsId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStayId() {
		return stayId;
	}

	public void setStayId(String stayId) {
		this.stayId = stayId;
	}

	public String getGsId() {
		return gsId;
	}

	public void setGsId(String gsId) {
		this.gsId = gsId;
	}

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "GetUOSByUserRequest [userId=" + userId + ", stayId=" + stayId + ", gsId=" + gsId + "]";
		}
	}

}
