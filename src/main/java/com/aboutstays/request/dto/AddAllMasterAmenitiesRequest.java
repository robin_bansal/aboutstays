package com.aboutstays.request.dto;

import java.util.List;

public class AddAllMasterAmenitiesRequest {

	private List<AddMasterAmenityRequest> amenities;
	
	public List<AddMasterAmenityRequest> getAmenities() {
		return amenities;
	}
	
	public void setAmenities(List<AddMasterAmenityRequest> amenities) {
		this.amenities = amenities;
	}
}
