package com.aboutstays.request.dto;

public class LinkLoyaltyProgramsRequest {
	private String hotelId;
	private String loyaltyProgramsId;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getLoyaltyProgramsId() {
		return loyaltyProgramsId;
	}
	public void setLoyaltyProgramsId(String loyaltyProgramsId) {
		this.loyaltyProgramsId = loyaltyProgramsId;
	}
	
}
