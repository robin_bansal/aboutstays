package com.aboutstays.request.dto;

public class UpdateRoomRequest extends AddRoomRequest{
	private String roomId;
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
}
