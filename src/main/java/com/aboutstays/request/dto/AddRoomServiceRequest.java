package com.aboutstays.request.dto;

import com.aboutstays.pojos.RoomServicePojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddRoomServiceRequest extends RoomServicePojo {

}
