package com.aboutstays.request.dto;

import java.util.List;

public class FreezeBookingRequest {
private List<String> bookingIds;

public List<String> getBookingIds() {
	return bookingIds;
}

public void setBookingIds(List<String> bookingIds) {
	this.bookingIds = bookingIds;
}

}
