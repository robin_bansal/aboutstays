package com.aboutstays.request.dto;

public class UpdateEntertaimentItemRequest extends AddEntertaimentItemRequest {

	private String itemId;
	
	public String getItemId() {
		return itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
}
