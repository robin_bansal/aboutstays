package com.aboutstays.request.dto;

import com.aboutstays.pojos.HousekeepingItemPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class HousekeepingItemRequest extends HousekeepingItemPojo{

}
