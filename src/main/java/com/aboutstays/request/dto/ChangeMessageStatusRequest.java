package com.aboutstays.request.dto;

public class ChangeMessageStatusRequest {
private String userId;
private String hotelId;
private int creationType;
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
public String getHotelId() {
	return hotelId;
}
public void setHotelId(String hotelId) {
	this.hotelId = hotelId;
}
public int getCreationType() {
	return creationType;
}
public void setCreationType(int creationType) {
	this.creationType = creationType;
}
}
