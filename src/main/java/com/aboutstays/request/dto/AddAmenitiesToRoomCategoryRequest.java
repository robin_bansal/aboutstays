package com.aboutstays.request.dto;

import java.util.List;

public class AddAmenitiesToRoomCategoryRequest {

	private String roomCategoryId;
	private List<String> amenityIds;
	public String getRoomCategoryId() {
		return roomCategoryId;
	}
	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
	public List<String> getAmenityIds() {
		return amenityIds;
	}
	public void setAmenityIds(List<String> amenityIds) {
		this.amenityIds = amenityIds;
	}
	
}
