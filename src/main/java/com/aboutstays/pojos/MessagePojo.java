package com.aboutstays.pojos;

public class MessagePojo {

	private String messageText;
	private String userId;
	private String staysId;
	private String hotelId;
	private Integer status;

	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
