package com.aboutstays.pojos;

public class MasterAmenityPojo {

	private String name;
	private String iconUrl;
	private Integer type;
	
	public MasterAmenityPojo() {
	}
	public MasterAmenityPojo(String name, String iconUrl) {
		super();
		this.name = name;
		this.iconUrl = iconUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
}
