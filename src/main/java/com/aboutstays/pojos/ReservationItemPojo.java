package com.aboutstays.pojos;

import java.util.List;

public class ReservationItemPojo {

	
	private String name;
	private String startTime;
	private String endTime;
	private String imageUrl;
	private List<String> additionalImageUrls;
	private String entryCriteria;
	private String descripton;
	private double price;
	private long duration;
	private List<String> mostPopularList;
	private List<String> offerings;
	private boolean amountApplicable;
	private boolean durationBased;
	private String subCategory;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}
	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}
	public String getEntryCriteria() {
		return entryCriteria;
	}
	public void setEntryCriteria(String entryCriteria) {
		this.entryCriteria = entryCriteria;
	}
	public String getDescripton() {
		return descripton;
	}
	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public List<String> getMostPopularList() {
		return mostPopularList;
	}
	public void setMostPopularList(List<String> mostPopularList) {
		this.mostPopularList = mostPopularList;
	}
	public List<String> getOfferings() {
		return offerings;
	}
	public void setOfferings(List<String> offerings) {
		this.offerings = offerings;
	}
	public boolean isAmountApplicable() {
		return amountApplicable;
	}
	public void setAmountApplicable(boolean amountApplicable) {
		this.amountApplicable = amountApplicable;
	}
	public boolean isDurationBased() {
		return durationBased;
	}
	public void setDurationBased(boolean durationBased) {
		this.durationBased = durationBased;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
}
