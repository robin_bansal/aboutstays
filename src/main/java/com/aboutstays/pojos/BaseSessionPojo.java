package com.aboutstays.pojos;

public class BaseSessionPojo {
	private String userId;
	private String hotelId;
	private String staysId;
	private String gsId;
	
	public String getGsId() {
		return gsId;
	}
	
	public void setGsId(String gsId) {
		this.gsId = gsId;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String stayId) {
		this.staysId = stayId;
	}

}
