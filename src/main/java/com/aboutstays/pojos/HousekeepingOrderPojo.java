package com.aboutstays.pojos;

import java.util.List;

public class HousekeepingOrderPojo extends BaseUserOrderRequestPojo{
	
	private List<HousekeepingItemOrderDetails> listOrders;

	public List<HousekeepingItemOrderDetails> getListOrders() {
		return listOrders;
	}

	public void setListOrders(List<HousekeepingItemOrderDetails> listOrders) {
		this.listOrders = listOrders;
	}

}
