package com.aboutstays.pojos;

public class HotelInfoMetaData {
private int totalItems;
private int items;
public int getTotalItems() {
	return totalItems;
}
public void setTotalItems(int totalItems) {
	this.totalItems = totalItems;
}
public int getItems() {
	return items;
}
public void setItems(int items) {
	this.items = items;
}

}
