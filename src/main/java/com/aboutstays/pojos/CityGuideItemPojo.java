package com.aboutstays.pojos;

import java.util.List;

public class CityGuideItemPojo {
	
	private String id;
	private String hotelId;
	private String cityGuideId;
	private Integer cityGuideItemType;
	private CityGuideGeneralInfo generalInfo;
	private String openingTime;
	private String closingTime;
	private String destinationType;
	private Double distance;
	private List<String> travelOptionsList;
	private String description;
	private List<String> activityList;
	private boolean recommended;
	
	public CityGuideGeneralInfo getGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(CityGuideGeneralInfo generalInfo) {
		this.generalInfo = generalInfo;
	}
	public String getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}
	public String getClosingTime() {
		return closingTime;
	}
	public void setClosingTime(String closingTime) {
		this.closingTime = closingTime;
	}
	public String getDestinationType() {
		return destinationType;
	}
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public List<String> getTravelOptionsList() {
		return travelOptionsList;
	}
	public void setTravelOptionsList(List<String> travelOptionsList) {
		this.travelOptionsList = travelOptionsList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<String> activityList) {
		this.activityList = activityList;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getCityGuideId() {
		return cityGuideId;
	}
	public void setCityGuideId(String cityGuideId) {
		this.cityGuideId = cityGuideId;
	}
	public Integer getCityGuideItemType() {
		return cityGuideItemType;
	}
	public void setCityGuideItemType(Integer cityGuideItemType) {
		this.cityGuideItemType = cityGuideItemType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	
}
