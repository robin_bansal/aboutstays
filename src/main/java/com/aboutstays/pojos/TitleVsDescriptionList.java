package com.aboutstays.pojos;

import java.util.List;

public class TitleVsDescriptionList {

	private String title;
	private List<String> descriptionList;
	
	public TitleVsDescriptionList(){
		
	}
	
	public TitleVsDescriptionList(String title, List<String> descriptionList) {
		this.title = title;
		this.descriptionList = descriptionList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<String> getDescriptionList() {
		return descriptionList;
	}
	public void setDescriptionList(List<String> descriptionList) {
		this.descriptionList = descriptionList;
	}
	
	
}
