package com.aboutstays.pojos;

import java.util.List;

public class LaundryServicePojo {

	private String serviceId;
	private List<SupportedLaundryType> supportedLaundryTypeList;
	private boolean slaApplicable;
	private int slaTime;
	private String startTime;
	private String endTime;
	private boolean swiftDeliveryEnabled;
	private String pickupByTime;
	private String swiftDeliveryMessage;

	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public List<SupportedLaundryType> getSupportedLaundryTypeList() {
		return supportedLaundryTypeList;
	}
	public void setSupportedLaundryTypeList(List<SupportedLaundryType> supportedLaundryTypeList) {
		this.supportedLaundryTypeList = supportedLaundryTypeList;
	}
	public boolean isSlaApplicable() {
		return slaApplicable;
	}
	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}
	public int getSlaTime() {
		return slaTime;
	}
	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public boolean isSwiftDeliveryEnabled() {
		return swiftDeliveryEnabled;
	}
	public void setSwiftDeliveryEnabled(boolean swiftDeliveryEnabled) {
		this.swiftDeliveryEnabled = swiftDeliveryEnabled;
	}
	public String getPickupByTime() {
		return pickupByTime;
	}
	public void setPickupByTime(String pickupByTime) {
		this.pickupByTime = pickupByTime;
	}
	public String getSwiftDeliveryMessage() {
		return swiftDeliveryMessage;
	}
	public void setSwiftDeliveryMessage(String swiftDeliveryMessage) {
		this.swiftDeliveryMessage = swiftDeliveryMessage;
	}
	
}
