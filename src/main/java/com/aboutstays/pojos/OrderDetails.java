package com.aboutstays.pojos;

public class OrderDetails {
	
	private String itemName;
	private int quantity;
	private int price;
	private String logoUrl;
	private String preferredDateAndTime;
	private String comment;
	
	public OrderDetails(){};
	
	public OrderDetails(String itemName, int quantity, int price, String logoUrl, String preferredDateAndTime,
			String comment) {
		this.itemName = itemName;
		this.quantity = quantity;
		this.price = price;
		this.logoUrl = logoUrl;
		this.preferredDateAndTime = preferredDateAndTime;
		this.comment = comment;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getQuantitye() {
		return quantity;
	}
	public void setQuantitye(int quantitye) {
		this.quantity = quantitye;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPreferredDateAndTime() {
		return preferredDateAndTime;
	}
	public void setPreferredDateAndTime(String preferredDateAndTime) {
		this.preferredDateAndTime = preferredDateAndTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
	

}
