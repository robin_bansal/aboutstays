package com.aboutstays.pojos;

import java.util.List;

public class ReservationCategoryPojo {

	private String name;
	private String startTime;
	private String endTime;
	private String hotelId;
	private String wingId;
	private String floorId;
	private String entryCriteria;
	private String description;
	private List<String> mostPopular;
	private boolean hasDeals;
	private boolean provideReservations;
	private String imageUrl;
	private List<String> additionalImageUrls;
	private int reservationType;
	private String primaryCuisine;
	private String guestSenseCategory;
	private String amenityType;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getWingId() {
		return wingId;
	}
	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	public String getFloorId() {
		return floorId;
	}
	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
	public String getEntryCriteria() {
		return entryCriteria;
	}
	public void setEntryCriteria(String entryCriteria) {
		this.entryCriteria = entryCriteria;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getMostPopular() {
		return mostPopular;
	}
	public void setMostPopular(List<String> mostPopular) {
		this.mostPopular = mostPopular;
	}
	public boolean isHasDeals() {
		return hasDeals;
	}
	public void setHasDeals(boolean hasDeals) {
		this.hasDeals = hasDeals;
	}
	public boolean isProvideReservations() {
		return provideReservations;
	}
	public void setProvideReservations(boolean provideReservations) {
		this.provideReservations = provideReservations;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}
	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}
	public int getReservationType() {
		return reservationType;
	}
	public void setReservationType(int reservationType) {
		this.reservationType = reservationType;
	}
	public String getGuestSenseCategory() {
		return guestSenseCategory;
	}
	public void setGuestSenseCategory(String guestSenseCategory) {
		this.guestSenseCategory = guestSenseCategory;
	}
	public String getPrimaryCuisine() {
		return primaryCuisine;
	}
	public void setPrimaryCuisine(String primaryCuisine) {
		this.primaryCuisine = primaryCuisine;
	}
	public String getAmenityType() {
		return amenityType;
	}
	public void setAmenityType(String amenityType) {
		this.amenityType = amenityType;
	}
}
