package com.aboutstays.pojos;

public class ChildCategoryItem extends TitleVsDescriptionPojo{

	public ChildCategoryItem() {};
	
	public ChildCategoryItem(String title, String description) {
		super(title, description);
	}
}
