package com.aboutstays.pojos;

import java.util.List;

public class MaintenancePojo {
	
	private String id;
	private List<MaintenanceItemByType> listMaintenanceItemType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<MaintenanceItemByType> getListMaintenanceItemType() {
		return listMaintenanceItemType;
	}
	public void setListMaintenanceItemType(List<MaintenanceItemByType> listMaintenanceItemType) {
		this.listMaintenanceItemType = listMaintenanceItemType;
	}

	
}
