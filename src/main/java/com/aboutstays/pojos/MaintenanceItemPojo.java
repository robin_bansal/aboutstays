package com.aboutstays.pojos;

public class MaintenanceItemPojo {
	
	private String id;
	private String hotelId;
	private String maintenanceServiceId;
	private String name;
	private int itemType;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getMaintenanceServiceId() {
		return maintenanceServiceId;
	}
	public void setMaintenanceServiceId(String maintenanceServiceId) {
		this.maintenanceServiceId = maintenanceServiceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getItemType() {
		return itemType;
	}
	public void setItemType(int itemType) {
		this.itemType = itemType;
	}
	
}
