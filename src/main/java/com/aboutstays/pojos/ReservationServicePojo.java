package com.aboutstays.pojos;

import java.util.List;

public class ReservationServicePojo {
	
	private String id;
	private List<ReservationItemTypeInfo> typeInfoList;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<ReservationItemTypeInfo> getTypeInfoList() {
		return typeInfoList;
	}
	public void setTypeInfoList(List<ReservationItemTypeInfo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}
	
	

}
