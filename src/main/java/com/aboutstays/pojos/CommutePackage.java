package com.aboutstays.pojos;

import java.util.List;

public class CommutePackage {

	private String packageName;
	private List<CarDetailsAndPrice> carDetailsAndPriceList;
	
	public CommutePackage(){};
	
	public CommutePackage(String packageName, List<CarDetailsAndPrice> carDetailsAndPriceList) {
		this.packageName = packageName;
		this.carDetailsAndPriceList = carDetailsAndPriceList;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public List<CarDetailsAndPrice> getCarDetailsAndPriceList() {
		return carDetailsAndPriceList;
	}
	public void setCarDetailsAndPriceList(List<CarDetailsAndPrice> carDetailsAndPriceList) {
		this.carDetailsAndPriceList = carDetailsAndPriceList;
	}
	
	
}
