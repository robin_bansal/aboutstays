package com.aboutstays.pojos;

public class Amenity extends MasterAmenityPojo{

	private String amenityId;

	public Amenity(){}
	
	public Amenity(String amenityId, String name) {
		this.amenityId = amenityId;
	}

	public String getAmenityId() {
		return amenityId;
	}
	
	public void setAmenityId(String amenityId) {
		this.amenityId = amenityId;
	}
}
