package com.aboutstays.pojos;

public class CredentialsPojo {
	
	private String emailId;
	private String password;
	private String phoneNumber;
	private String userId;
	
	public CredentialsPojo(){};
	
	public CredentialsPojo(String emailId, String password) {
		this.emailId = emailId;
		this.password = password;
	}
	

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	

}
