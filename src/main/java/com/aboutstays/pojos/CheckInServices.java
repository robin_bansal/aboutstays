package com.aboutstays.pojos;

public class CheckInServices {
	
	private AirportPickupPojo airportPickup;
	private String messages;
	private boolean pickupInitiated;
	private DriverPojo driver;
	
	public AirportPickupPojo getAirportPickup() {
		return airportPickup;
	}
	public void setAirportPickup(AirportPickupPojo airportPickup) {
		this.airportPickup = airportPickup;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	public boolean isPickupInitiated() {
		return pickupInitiated;
	}
	public void setPickupInitiated(boolean pickupInitiated) {
		this.pickupInitiated = pickupInitiated;
	}
	public DriverPojo getDriver() {
		return driver;
	}
	public void setDriver(DriverPojo driver) {
		this.driver = driver;
	}
	
	
	
	
	
	

}
