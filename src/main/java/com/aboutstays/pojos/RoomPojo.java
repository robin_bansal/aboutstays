package com.aboutstays.pojos;

public class RoomPojo {

private String roomName;
	
	private String roomNumber;
	
	private String wingId;
	
	private String floorId;
	
	private String hotelId;
	
	private Integer panelUiOrder;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	
	public Integer getPanelUiOrder() {
		return panelUiOrder;
	}
	public void setPanelUiOrder(Integer panelUiOrder) {
		this.panelUiOrder = panelUiOrder;
	}

}
