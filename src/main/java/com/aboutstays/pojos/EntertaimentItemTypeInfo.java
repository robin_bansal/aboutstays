package com.aboutstays.pojos;

public class EntertaimentItemTypeInfo{

	private String typeId;
	
	public EntertaimentItemTypeInfo(){
	}
	
	public EntertaimentItemTypeInfo(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeId() {
		return typeId;
	}
	
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
}
