package com.aboutstays.pojos;

import java.util.List;

public class Essentials {

	private List<Essential> itemsList;
	
	public List<Essential> getItemsList() {
		return itemsList;
	}
	public void setItemsList(List<Essential> itemsList) {
		this.itemsList = itemsList;
	}
}
