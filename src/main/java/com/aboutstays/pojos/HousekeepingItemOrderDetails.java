package com.aboutstays.pojos;

public class HousekeepingItemOrderDetails extends GeneralItemTypeInfo{

	private String hkItemId;
	private int quantity;
	private Double price;
	private String name;
	
	public HousekeepingItemOrderDetails() {}
	
	public HousekeepingItemOrderDetails(String hkItemId, int quantity, Double price, int hkItemType){
		this.hkItemId = hkItemId;
		this.quantity = quantity;
		this.price = price;
		this.setType(hkItemType);
	}
	public String getHkItemId() {
		return hkItemId;
	}
	public void setHkItemId(String hkItemId) {
		this.hkItemId = hkItemId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
