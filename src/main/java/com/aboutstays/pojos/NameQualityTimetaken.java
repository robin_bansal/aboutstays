package com.aboutstays.pojos;

public class NameQualityTimetaken {
	
	private String name;
	private int quality;
	private int timeTaken;
	private boolean qualitySubmitted;
	private boolean timeTakenSubmitted;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuality() {
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}
	public int getTimeTaken() {
		return timeTaken;
	}
	public void setTimeTaken(int timeTaken) {
		this.timeTaken = timeTaken;
	}
	public boolean isQualitySubmitted() {
		return qualitySubmitted;
	}
	public void setQualitySubmitted(boolean qualitySubmitted) {
		this.qualitySubmitted = qualitySubmitted;
	}
	public boolean isTimeTakenSubmitted() {
		return timeTakenSubmitted;
	}
	public void setTimeTakenSubmitted(boolean timeTakenSubmitted) {
		this.timeTakenSubmitted = timeTakenSubmitted;
	}
	
	

}
