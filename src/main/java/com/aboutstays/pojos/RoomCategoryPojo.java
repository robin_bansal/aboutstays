package com.aboutstays.pojos;

import java.util.List;

public class RoomCategoryPojo {
	
	private String categoryName;
	private String categoryCode;
	private Integer count;
	private String imageUrl;
	private String description;
	private String size;
	private String sizeMetric;
	private int adultCapacity;
	private int childCapacity;
	private List<String> additionalImageUrls;

	private EarlyCheckinInfo earlyCheckinInfo;
	private LateCheckoutInfo lateCheckoutInfo;
	private String hotelId;

	public RoomCategoryPojo() {
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSizeMetric() {
		return sizeMetric;
	}

	public void setSizeMetric(String sizeMetric) {
		this.sizeMetric = sizeMetric;
	}

	public int getAdultCapacity() {
		return adultCapacity;
	}

	public void setAdultCapacity(int adultCapacity) {
		this.adultCapacity = adultCapacity;
	}

	public int getChildCapacity() {
		return childCapacity;
	}

	public void setChildCapacity(int childCapacity) {
		this.childCapacity = childCapacity;
	}

	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}

	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}

	public EarlyCheckinInfo getEarlyCheckinInfo() {
		return earlyCheckinInfo;
	}

	public void setEarlyCheckinInfo(EarlyCheckinInfo earlyCheckinInfo) {
		this.earlyCheckinInfo = earlyCheckinInfo;
	}

	public LateCheckoutInfo getLateCheckoutInfo() {
		return lateCheckoutInfo;
	}

	public void setLateCheckoutInfo(LateCheckoutInfo lateCheckoutInfo) {
		this.lateCheckoutInfo = lateCheckoutInfo;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}



}
