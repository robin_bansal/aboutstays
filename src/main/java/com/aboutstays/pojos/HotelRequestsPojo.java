package com.aboutstays.pojos;

import com.aboutstays.enums.GeneralOrderStatus;

public class HotelRequestsPojo {
	private String serviceName;
	private String iconUrl;
	private int pendingApproval;
	private int onGoingUpGoing;
	private int completed;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}


	public int getPendingApproval() {
		return pendingApproval;
	}

	public void setPendingApproval(int pendingApproval) {
		this.pendingApproval = pendingApproval;
	}

	public int getOnGoingUpGoing() {
		return onGoingUpGoing;
	}

	public void setOnGoingUpGoing(int onGoingUpGoing) {
		this.onGoingUpGoing = onGoingUpGoing;
	}

	public int getCompleted() {
		return completed;
	}

	public void setCompleted(int completed) {
		this.completed = completed;
	}

	public void updateStaus(Integer status) {
		GeneralOrderStatus orderStatus = GeneralOrderStatus.findByCode(status);
		if (orderStatus != null) {
			switch (orderStatus) {
			case INITIATED:
			case PENDING:
				pendingApproval++;
				break;
			case COMPLETED:
			case CANCELLED:
				completed++;
				break;
			case PROCESSING:
			case PREPAIRING:
			case UPDATED:
			case SCHEDULED:
				onGoingUpGoing++;
				break;
			}
		}
	}
}
