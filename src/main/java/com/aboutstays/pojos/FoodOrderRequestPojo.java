package com.aboutstays.pojos;

import java.util.List;

public class FoodOrderRequestPojo extends BaseUserOrderRequestPojo{
	
	private List<FoodItemOrderDetails> foodItemOrders;
	
	public List<FoodItemOrderDetails> getFoodItemOrders() {
		return foodItemOrders;
	}
	
	public void setFoodItemOrders(List<FoodItemOrderDetails> foodItemOrders) {
		this.foodItemOrders = foodItemOrders;
	}

	
	
	
}
