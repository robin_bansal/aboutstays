package com.aboutstays.pojos;

import java.util.List;

import com.aboutstays.entities.LoyaltyLevel;

public class LoyaltyProgramPojo {
	private String name;
	private String iconUrl;
	private List<LoyaltyLevel> levels;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public List<LoyaltyLevel> getLevels() {
		return levels;
	}
	public void setLevels(List<LoyaltyLevel> levels) {
		this.levels = levels;
	}

}
