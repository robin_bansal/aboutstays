package com.aboutstays.pojos;

public class CompanyInfo {

	private String name;
	
	public CompanyInfo(){}
	
	public CompanyInfo(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
