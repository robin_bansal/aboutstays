package com.aboutstays.pojos;

public class CheckinSpecialRequest {

	private String request;
	
	public CheckinSpecialRequest(){}	
	
	public CheckinSpecialRequest(String request) {
		this.request = request;
	}

	public String getRequest() {
		return request;
	}
	
	public void setRequest(String request) {
		this.request = request;
	}
}
