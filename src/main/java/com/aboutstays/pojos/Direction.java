package com.aboutstays.pojos;

public class Direction {

	private String directionId;
	private String directionInfo;
	
	public Direction(){}

	public Direction(String directionId, String directionInfo) {
		this.directionId = directionId;
		this.directionInfo = directionInfo;
	}
	
	public String getDirectionId() {
		return directionId;
	}
	public void setDirectionId(String directionId) {
		this.directionId = directionId;
	}
	public String getDirectionInfo() {
		return directionInfo;
	}
	public void setDirectionInfo(String directionInfo) {
		this.directionInfo = directionInfo;
	}
	
}
