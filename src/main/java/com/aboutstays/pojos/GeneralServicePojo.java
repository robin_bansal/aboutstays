package com.aboutstays.pojos;

public class GeneralServicePojo {
	
	private String activatedImageUrl;
	private String unactivatedImageUrl;
	private Integer status;
	private String id;
	private String name;
	private Integer serviceType;
	private boolean numerable;
	private boolean tickable;
	private Boolean generalInfo;
	private String expenseId;
	private int uiOrder;
	private boolean supportsPast;
	private boolean supportsFuture;
	private boolean supportsNonPartneredHotel;
	
	public String getActivatedImageUrl() {
		return activatedImageUrl;
	}
	public void setActivatedImageUrl(String activatedImageUrl) {
		this.activatedImageUrl = activatedImageUrl;
	}
	public String getUnactivatedImageUrl() {
		return unactivatedImageUrl;
	}
	public void setUnactivatedImageUrl(String unactivatedImageUrl) {
		this.unactivatedImageUrl = unactivatedImageUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getServiceType() {
		return serviceType;
	}
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}
	public boolean isNumerable() {
		return numerable;
	}
	public void setNumerable(boolean numerable) {
		this.numerable = numerable;
	}
	public boolean isTickable() {
		return tickable;
	}
	public void setTickable(boolean tickable) {
		this.tickable = tickable;
	}
	public Boolean isGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(Boolean generalInfo) {
		this.generalInfo = generalInfo;
	}
	public String getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}
	public int getUiOrder() {
		return uiOrder;
	}
	public void setUiOrder(int uiOrder) {
		this.uiOrder = uiOrder;
	}
	public Boolean getGeneralInfo() {
		return generalInfo;
	}
	public boolean isSupportsPast() {
		return supportsPast;
	}
	public void setSupportsPast(boolean supportsPast) {
		this.supportsPast = supportsPast;
	}
	public boolean isSupportsFuture() {
		return supportsFuture;
	}
	public void setSupportsFuture(boolean supportsFuture) {
		this.supportsFuture = supportsFuture;
	}
	public boolean isSupportsNonPartneredHotel() {
		return supportsNonPartneredHotel;
	}
	public void setSupportsNonPartneredHotel(boolean supportsNonPartneredHotel) {
		this.supportsNonPartneredHotel = supportsNonPartneredHotel;
	}
	
	
	
}
