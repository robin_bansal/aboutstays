package com.aboutstays.pojos;

public class GeneralDashboardItem {

	private String uosId;
	private String imageUrl;
	private String title;
	private String description;
	private String status;
	private String statusImageUrl;
	private long orderedTime;
	private int count;
	private Long updatedTime;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getOrderedTime() {
		return orderedTime;
	}
	public void setOrderedTime(long orderedTime) {
		this.orderedTime = orderedTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusImageUrl() {
		return statusImageUrl;
	}
	public void setStatusImageUrl(String statusImageUrl) {
		this.statusImageUrl = statusImageUrl;
	}
	public String getUosId() {
		return uosId;
	}
	public void setUosId(String uosId) {
		this.uosId = uosId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public long getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	
	
	
}
