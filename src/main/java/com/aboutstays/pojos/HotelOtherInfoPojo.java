package com.aboutstays.pojos;

import java.util.List;

public class HotelOtherInfoPojo {
	
	private String id;
	private List<GeneralNameImageInfo> listCardAccepted;
	private List<GeneralNameImageInfo> listAwards;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<GeneralNameImageInfo> getListCardAccepted() {
		return listCardAccepted;
	}
	public void setListCardAccepted(List<GeneralNameImageInfo> listCardAccepted) {
		this.listCardAccepted = listCardAccepted;
	}
	public List<GeneralNameImageInfo> getListAwards() {
		return listAwards;
	}
	public void setListAwards(List<GeneralNameImageInfo> listAwards) {
		this.listAwards = listAwards;
	}
	
	

}
