package com.aboutstays.pojos;

import java.util.List;

public class RoomServicePojo {
	
	private String id;
	private List<MenuItemsByType> listMenuItem;
	private boolean slaApplicable;
	private int slaTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<MenuItemsByType> getListMenuItem() {
		return listMenuItem;
	}

	public void setListMenuItem(List<MenuItemsByType> listMenuItem) {
		this.listMenuItem = listMenuItem;
	}

	public boolean isSlaApplicable() {
		return slaApplicable;
	}

	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}

	public int getSlaTime() {
		return slaTime;
	}

	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}

}
