package com.aboutstays.pojos;

import java.util.List;

public class TimeBasedExpenseItem extends ExpenseItem{

	private Long created;
	private List<ExpenseItem> subExpenseItemList;
	
	public List<ExpenseItem> getSubExpenseItemList() {
		return subExpenseItemList;
	}
	
	public void setSubExpenseItemList(List<ExpenseItem> subExpenseItemList) {
		this.subExpenseItemList = subExpenseItemList;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
}
