package com.aboutstays.pojos;

public class EntertaimentItemTypeInfoPojo extends GeneralItemTypeInfo {

	private String genereId;
	
	public String getGenereId() {
		return genereId;
	}
	
	public void setGenereId(String genereId) {
		this.genereId = genereId;
	}
}
