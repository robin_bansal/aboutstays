package com.aboutstays.pojos;

public class AirportPickupPojo extends BaseSessionPojo{
	private String id;
	private String carId;
	private String driverId;
	private String airportName;
	private String airlineName;
	private String flightNumber;
	private long pickupDateAndTime;
	private String comment;
	private Integer carType;
	private Double price;
	private Boolean drop;
	private Integer pickupStatus;
	private String deliverTimeString;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public long getPickupDateAndTime() {
		return pickupDateAndTime;
	}
	public void setPickupDateAndTime(long pickupDateAndTime) {
		this.pickupDateAndTime = pickupDateAndTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getCarType() {
		return carType;
	}
	public void setCarType(Integer carType) {
		this.carType = carType;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Boolean getDrop() {
		return drop;
	}
	public void setDrop(Boolean drop) {
		this.drop = drop;
	}
	public Integer getPickupStatus() {
		return pickupStatus;
	}
	public void setPickupStatus(Integer pickupStatus) {
		this.pickupStatus = pickupStatus;
	}
	public String getDeliverTimeString() {
		return deliverTimeString;
	}
	public void setDeliverTimeString(String deliverTimeString) {
		this.deliverTimeString = deliverTimeString;
	}
	
	
}
