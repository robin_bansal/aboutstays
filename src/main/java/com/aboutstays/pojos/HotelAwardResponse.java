package com.aboutstays.pojos;

public class HotelAwardResponse {
	private AwardPojo award;
	private String year;

	public AwardPojo getAward() {
		return award;
	}

	public void setAward(AwardPojo award) {
		this.award = award;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
}
