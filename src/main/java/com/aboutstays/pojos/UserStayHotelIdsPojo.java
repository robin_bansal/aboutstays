package com.aboutstays.pojos;

import com.aboutstays.utils.JsonUtil;

public class UserStayHotelIdsPojo extends BaseSessionPojo {

	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {

			return "UserStayHotelIdsPojo [getUserId()=" + getUserId() + ", getHotelId()=" + getHotelId()
					+ ", getStaysId()=" + getStaysId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
					+ ", toString()=" + super.toString() + "]";
		}
	}

}
