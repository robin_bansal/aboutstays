package com.aboutstays.pojos;

public class InternetPack {

	private String title;
	private String description;
	private double mrp;
	private double price;
	private boolean complementry;
	private String category;
	private boolean showOnApp;

	public InternetPack() {}
	
	public InternetPack(String title, String description, double mrp, double price, boolean complementry, String category) {
		this.title = title;
		this.description = description;
		this.mrp = mrp;
		this.price = price;
		this.complementry = complementry;
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getMrp() {
		return mrp;
	}
	public void setMrp(double mrp) {
		this.mrp = mrp;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean isComplementry() {
		return complementry;
	}
	public void setComplementry(boolean complementry) {
		this.complementry = complementry;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public boolean isShowOnApp() {
		return showOnApp;
	}

	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}

}
