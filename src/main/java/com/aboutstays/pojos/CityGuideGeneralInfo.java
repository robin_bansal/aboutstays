package com.aboutstays.pojos;

import java.util.List;

public class CityGuideGeneralInfo extends LocationAndContact {
	
	private String name;
	private String imageUrl;
	private List<String> additionalImageUrls;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}
	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}
	
	

}
