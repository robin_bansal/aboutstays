package com.aboutstays.pojos;

import java.util.List;

public class PersonalInformation {

	private String title;
	private String firstName;
	private String lastName;
	private String imageUrl;
	private String emailId;
	private String number;
	private String gender;
	private String userId;
	private String dob;
	private String anniversaryDate;
	private Integer maritalStatus;
	private Address homeAddress;
	private Address officeAddress;
	private List<StayPreferences> listStayPreferences;
	private List<IdentityDoc> listIdentityDocs;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAnniversaryDate() {
		return anniversaryDate;
	}
	public void setAnniversaryDate(String anniversaryDate) {
		this.anniversaryDate = anniversaryDate;
	}
	public Integer getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public Address getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}
	public Address getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(Address officeAddress) {
		this.officeAddress = officeAddress;
	}
	public List<StayPreferences> getListStayPreferences() {
		return listStayPreferences;
	}
	public void setListStayPreferences(List<StayPreferences> listStayPreferences) {
		this.listStayPreferences = listStayPreferences;
	}
	public List<IdentityDoc> getListIdentityDocs() {
		return listIdentityDocs;
	}
	public void setListIdentityDocs(List<IdentityDoc> listIdentityDocs) {
		this.listIdentityDocs = listIdentityDocs;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
