package com.aboutstays.pojos.socketmetadatas;

public class ChatMetaData {
	private String text;
	private String stayId;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStayId() {
		return stayId;
	}

	public void setStayId(String stayId) {
		this.stayId = stayId;
	}
}
