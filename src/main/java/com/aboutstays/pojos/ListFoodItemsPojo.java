package com.aboutstays.pojos;

import java.util.List;

public class ListFoodItemsPojo {

	private List<FoodItemPojo> listFoodItems;

	public List<FoodItemPojo> getListFoodItems() {
		return listFoodItems;
	}

	public void setListFoodItems(List<FoodItemPojo> listFoodItems) {
		this.listFoodItems = listFoodItems;
	}
}
