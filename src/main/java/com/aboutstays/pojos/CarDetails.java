package com.aboutstays.pojos;

public class CarDetails {
	
	private Integer type;
	private String name;
	private String imageUrl;
	private int pax;
	private double basePrice;
	private double baseKm;
	private double pricePerKm;
	private String currency;

	public CarDetails() {}
	
	public CarDetails(Integer type, String imageUrl, int pax, double basePrice, double baseKm, double pricePerKm,
			String currency) {
		this.type = type;
		this.imageUrl = imageUrl;
		this.pax = pax;
		this.basePrice = basePrice;
		this.baseKm = baseKm;
		this.pricePerKm = pricePerKm;
		this.currency = currency;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public int getPax() {
		return pax;
	}
	public void setPax(int pax) {
		this.pax = pax;
	}
	public double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
	public double getBaseKm() {
		return baseKm;
	}
	public void setBaseKm(double baseKm) {
		this.baseKm = baseKm;
	}
	public double getPricePerKm() {
		return pricePerKm;
	}
	public void setPricePerKm(double pricePerKm) {
		this.pricePerKm = pricePerKm;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CarDetails [type=" + type + ", imageUrl=" + imageUrl + ", pax=" + pax + ", basePrice=" + basePrice
				+ ", baseKm=" + baseKm + ", pricePerKm=" + pricePerKm + ", currency=" + currency + "]";
	}
	
	

}
