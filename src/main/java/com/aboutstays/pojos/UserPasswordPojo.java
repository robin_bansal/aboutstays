package com.aboutstays.pojos;

public class UserPasswordPojo extends UserPojo{

	private String password;
	
    public UserPasswordPojo(){}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
