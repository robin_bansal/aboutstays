package com.aboutstays.pojos;

import java.util.List;

public class CityGuidePojo {
	
	private String id;
	private CityGuideGeneralInfo generalInfo;
	private List<CityGuideItemByType> itemByTypeList;


	public CityGuideGeneralInfo getGeneralInfo() {
		return generalInfo;
	}

	public void setGeneralInfo(CityGuideGeneralInfo generalInfo) {
		this.generalInfo = generalInfo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<CityGuideItemByType> getItemByTypeList() {
		return itemByTypeList;
	}

	public void setItemByTypeList(List<CityGuideItemByType> itemByTypeList) {
		this.itemByTypeList = itemByTypeList;
	}
	
	
	
	
	
}
