package com.aboutstays.pojos;

public class BookingVsStaysExists {

	private String bookingId;
	private boolean staysExists;
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public boolean isStaysExists() {
		return staysExists;
	}
	public void setStaysExists(boolean staysExists) {
		this.staysExists = staysExists;
	}
	
}
