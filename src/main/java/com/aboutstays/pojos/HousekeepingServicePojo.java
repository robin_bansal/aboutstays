package com.aboutstays.pojos;

import java.util.List;

public class HousekeepingServicePojo {
	
	private String id;
	private List<HousekeepingItemsByType> listHKItems;
	private boolean slaApplicable;
	private int slaTime;
	private String startTime;
	private String endTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<HousekeepingItemsByType> getListHKItems() {
		return listHKItems;
	}
	public void setListHKItems(List<HousekeepingItemsByType> listHKItems) {
		this.listHKItems = listHKItems;
	}
	public boolean isSlaApplicable() {
		return slaApplicable;
	}
	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}
	public int getSlaTime() {
		return slaTime;
	}
	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
