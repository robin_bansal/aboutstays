package com.aboutstays.pojos;

import java.util.List;

public class PickListPojo {

	private int type;
	private List<String> values;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	
}
