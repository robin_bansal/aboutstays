package com.aboutstays.pojos;

import java.util.Map;

public class StaysIdVsCartData<T> {

	private Map<String, T> staysIdVsCartDataMap;
	
	public Map<String, T> getStaysIdVsCartDataMap() {
		return staysIdVsCartDataMap;
	}
	public void setStaysIdVsCartDataMap(Map<String, T> staysIdVsCartDataMap) {
		this.staysIdVsCartDataMap = staysIdVsCartDataMap;
	}
}
