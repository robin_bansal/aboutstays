package com.aboutstays.pojos;

public class GeneralInfoPojo {

	private String activatedImageUrl;
	private String unactivatedImageUrl;
	private Integer status;
	private String id;
	private String name;
	private Integer infoType;
	public String getActivatedImageUrl() {
		return activatedImageUrl;
	}
	public void setActivatedImageUrl(String activatedImageUrl) {
		this.activatedImageUrl = activatedImageUrl;
	}
	public String getUnactivatedImageUrl() {
		return unactivatedImageUrl;
	}
	public void setUnactivatedImageUrl(String unactivatedImageUrl) {
		this.unactivatedImageUrl = unactivatedImageUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getInfoType() {
		return infoType;
	}
	public void setInfoType(Integer infoType) {
		this.infoType = infoType;
	}
	
	

}
