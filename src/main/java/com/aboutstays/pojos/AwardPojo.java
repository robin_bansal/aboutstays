package com.aboutstays.pojos;

public class AwardPojo extends MasterAmenityPojo {
	
	private String awardId;

	public String getAwardId() {
		return awardId;
	}

	public void setAwardId(String awardId) {
		this.awardId = awardId;
	}
}
