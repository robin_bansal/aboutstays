package com.aboutstays.pojos;

import java.util.List;

public class RequestDetailsTableEntries {

	private RequestDetailUIValues titles;
	private List<RequestDetailUIValues> rows;
	public RequestDetailUIValues getTitles() {
		return titles;
	}
	public void setTitles(RequestDetailUIValues titles) {
		this.titles = titles;
	}
	public List<RequestDetailUIValues> getRows() {
		return rows;
	}
	public void setRows(List<RequestDetailUIValues> rows) {
		this.rows = rows;
	}
	
}
