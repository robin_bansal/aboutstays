package com.aboutstays.pojos;

public class IdentityDoc {
	
	private String docType;
	private String nameOnDoc;
	private String docNumber;
	private String docUrl;
	
	@Override
	public int hashCode() {
		return docType.hashCode()+nameOnDoc.hashCode()+docNumber.hashCode()+docUrl.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null||!(obj instanceof IdentityDoc)){
			return false;
		}
		IdentityDoc identityDoc = (IdentityDoc) obj;
		if(docType.equals(identityDoc.docType)&&nameOnDoc.equals(identityDoc.nameOnDoc)&&docNumber.equals(identityDoc.docNumber)&&
				docUrl.equals(identityDoc.docUrl)){
			return true;
		}
		return false;
	}
	
	public IdentityDoc(){
		
	}
	
	public IdentityDoc(String docType, String nameOnDoc, String docNumber, String docUrl) {
		this.docType = docType;
		this.nameOnDoc = nameOnDoc;
		this.docNumber = docNumber;
		this.docUrl = docUrl;
	}

	public String getNameOnDoc() {
		return nameOnDoc;
	}
	public void setNameOnDoc(String nameOnDoc) {
		this.nameOnDoc = nameOnDoc;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getDocUrl() {
		return docUrl;
	}
	public void setDocUrl(String docUrl) {
		this.docUrl = docUrl;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}
	

}
