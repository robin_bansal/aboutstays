package com.aboutstays.pojos;

public class LateCheckoutRequestPojo extends BaseUserOrderRequestPojo {
	
	private String id;
	private long checkoutTime;
	private long checkoutDate;
	private double price;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getCheckoutTime() {
		return checkoutTime;
	}
	public void setCheckoutTime(long checkoutTime) {
		this.checkoutTime = checkoutTime;
	}
	public long getCheckoutDate() {
		return checkoutDate;
	}
	public void setCheckoutDate(long checkoutDate) {
		this.checkoutDate = checkoutDate;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	
	
	

}
