package com.aboutstays.pojos;

public class CartParams {
	
	private Integer cartType;//Refer CartType
	private Integer cartOperation;//Refer CartOperation
	public Integer getCartType() {
		return cartType;
	}
	public void setCartType(Integer cartType) {
		this.cartType = cartType;
	}
	public Integer getCartOperation() {
		return cartOperation;
	}
	public void setCartOperation(Integer cartOperation) {
		this.cartOperation = cartOperation;
	}
	
	

}
