package com.aboutstays.pojos;

public class FoodItemPojo {
	
	private String roomServicesId;
	private String hotelId;
	private String imageUrl;
	private String name;
	private Integer serves;
	private String cuisineType;
	private Double mrp;
	private Double price;
	private String description;
	private Integer foodLabel;//Refer FoodLabel enum
	private Integer spicyType;//Refer SpicyType enum
	private boolean recommended;
	private Integer type; //Refer MenuItemType enum
	private String category;
	private boolean showOnApp;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getRoomServicesId() {
		return roomServicesId;
	}
	public void setRoomServicesId(String roomServicesId) {
		this.roomServicesId = roomServicesId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getServes() {
		return serves;
	}
	public void setServes(Integer serves) {
		this.serves = serves;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getCuisineType() {
		return cuisineType;
	}
	public void setCuisineType(String cuisineType) {
		this.cuisineType = cuisineType;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getFoodLabel() {
		return foodLabel;
	}
	public void setFoodLabel(Integer foodLabel) {
		this.foodLabel = foodLabel;
	}
	public Integer getSpicyType() {
		return spicyType;
	}
	public void setSpicyType(Integer spicyType) {
		this.spicyType = spicyType;
	}
	public boolean isRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isShowOnApp() {
		return showOnApp;
	}
	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}
}
