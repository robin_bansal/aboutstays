package com.aboutstays.pojos;

public class TitleVsDescriptionPojo {

	private String title;
	private String description;
	private boolean showOnApp;
	
	public TitleVsDescriptionPojo(){}
	
	public TitleVsDescriptionPojo(String title, String description) {
		this.title = title;
		this.description = description;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isShowOnApp() {
		return showOnApp;
	}

	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}
}
