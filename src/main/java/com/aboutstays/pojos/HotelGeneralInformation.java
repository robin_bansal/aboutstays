package com.aboutstays.pojos;

import java.util.List;

public class HotelGeneralInformation extends LocationAndContact{

	private String logoUrl;
	private String name;
	private String description;
	private String checkInTime;
	private String checkOutTime;
	private String imageUrl;
	private List<String> additionalImageUrls;
	
	public HotelGeneralInformation(){}
	
	public HotelGeneralInformation(Address address, String phone1, String phone2, String email, String fax,
			String logoUrl, String name, String description, String checkInTime, String checkOutTime) {
		super(address, phone1, phone2, email, fax);
		this.logoUrl = logoUrl;
		this.name = name;
		this.description = description;
		this.checkInTime = checkInTime;
		this.checkOutTime = checkOutTime;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}



	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}

	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}
	
}
