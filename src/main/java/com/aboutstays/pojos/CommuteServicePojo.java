package com.aboutstays.pojos;

import java.util.List;

public class CommuteServicePojo {
	
	private String id;
	private List<CommutePackage> listCommutePackage;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<CommutePackage> getListCommutePackage() {
		return listCommutePackage;
	}
	public void setListCommutePackage(List<CommutePackage> listCommutePackage) {
		this.listCommutePackage = listCommutePackage;
	}
	
	
	

}
