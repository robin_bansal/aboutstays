package com.aboutstays.pojos;

public class RoomMetaInfo {

	private String roomId;
	private String roomCategoryId;
	private String roomNumber;
	private Integer panelUiOrder;
	
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getRoomCategoryId() {
		return roomCategoryId;
	}
	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
	public Integer getPanelUiOrder() {
		return panelUiOrder;
	}
	public void setPanelUiOrder(Integer panelUiOrder) {
		this.panelUiOrder = panelUiOrder;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
}
