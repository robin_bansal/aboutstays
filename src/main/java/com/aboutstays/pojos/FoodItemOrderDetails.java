package com.aboutstays.pojos;

public class FoodItemOrderDetails extends GeneralItemTypeInfo{

	private String foodItemId;
	private String itemName;
	private int quantity;
	private Double price;
	
	public FoodItemOrderDetails() {}

	public FoodItemOrderDetails(String foodItemId, int quantity, Double price, int menuItemType) {
		this.foodItemId = foodItemId;
		this.quantity = quantity;
		this.price = price;
		this.setType(menuItemType);
	}
	public String getFoodItemId() {
		return foodItemId;
	}
	public void setFoodItemId(String foodItemId) {
		this.foodItemId = foodItemId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
}
