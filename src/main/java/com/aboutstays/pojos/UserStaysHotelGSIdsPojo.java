package com.aboutstays.pojos;

import com.aboutstays.utils.JsonUtil;

public class UserStaysHotelGSIdsPojo extends UserStayHotelIdsPojo{


	@Override
	public String toString() {
		try {
			return JsonUtil.getJson(this);
		} catch (Exception e) {
			return "UserStaysHotelGSIdsPojo [getGsId()=" + getGsId() + ", toString()=" + super.toString()
			+ ", getUserId()=" + getUserId() + ", getHotelId()=" + getHotelId() + ", getStaysId()=" + getStaysId()
			+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
		}

	}
	
}
