package com.aboutstays.pojos;

import java.util.List;

public class LateCheckoutInfo {
	
	private String standardCheckoutTime;
	private List<GeneralDeviationInfo> listDeviationInfo;
	public String getStandardCheckoutTime() {
		return standardCheckoutTime;
	}
	public void setStandardCheckoutTime(String standardCheckoutTime) {
		this.standardCheckoutTime = standardCheckoutTime;
	}
	public List<GeneralDeviationInfo> getListDeviationInfo() {
		return listDeviationInfo;
	}
	public void setListDeviationInfo(List<GeneralDeviationInfo> listDeviationInfo) {
		this.listDeviationInfo = listDeviationInfo;
	}
	
	

}
