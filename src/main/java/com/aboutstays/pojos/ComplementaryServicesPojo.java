package com.aboutstays.pojos;

import java.util.List;

public class ComplementaryServicesPojo {

	private String servicesId;

	public String getServicesId() {
		return servicesId;
	}

	public void setServicesId(String servicesId) {
		this.servicesId = servicesId;
	}

	private List<TabWiseComplementaryService> tabWiseComplementaryServices;

	public List<TabWiseComplementaryService> getTabWiseComplementaryServices() {
		return tabWiseComplementaryServices;
	}

	public void setTabWiseComplementaryServices(List<TabWiseComplementaryService> tabWiseComplementaryServices) {
		this.tabWiseComplementaryServices = tabWiseComplementaryServices;
	}
}
