package com.aboutstays.pojos;

public class GeneralItemTypeInfo {

	private Integer type;
	private String typeName;
	private String imageUrl;
	
	
	public GeneralItemTypeInfo() {}
	
	public GeneralItemTypeInfo(Integer type) {
		this.type = type;
	}
	
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
