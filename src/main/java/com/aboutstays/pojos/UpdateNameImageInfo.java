package com.aboutstays.pojos;

import java.util.List;

public class UpdateNameImageInfo {

	public String id;
	public List<GeneralNameImageInfo> listAwards;
	public boolean delete;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<GeneralNameImageInfo> getListAwards() {
		return listAwards;
	}
	public void setListAwards(List<GeneralNameImageInfo> listAwards) {
		this.listAwards = listAwards;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
}
