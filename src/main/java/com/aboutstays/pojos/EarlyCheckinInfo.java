package com.aboutstays.pojos;

import java.util.List;

public class EarlyCheckinInfo {
	
	private String standardCheckinTime;
	private List<GeneralDeviationInfo> listDeviationInfo;

	public String getStandardCheckinTime() {
		return standardCheckinTime;
	}
	public void setStandardCheckinTime(String standardCheckinTime) {
		this.standardCheckinTime = standardCheckinTime;
	}
	public List<GeneralDeviationInfo> getListDeviationInfo() {
		return listDeviationInfo;
	}
	public void setListDeviationInfo(List<GeneralDeviationInfo> listDeviationInfo) {
		this.listDeviationInfo = listDeviationInfo;
	}
	
	
	

}
