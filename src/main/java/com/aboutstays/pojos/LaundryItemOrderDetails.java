package com.aboutstays.pojos;

public class LaundryItemOrderDetails extends GeneralItemTypeInfo{
	
	private String laundryItemId;
	private int quantity;
	private Double price;
	private String itemName;
	private boolean sameDaySelected;
	
	public String getLaundryItemId() {
		return laundryItemId;
	}
	
	public LaundryItemOrderDetails() {}
	
	public LaundryItemOrderDetails(String laundryItemId, int quantity, Double price, int laundryItemType) {
		this.laundryItemId = laundryItemId;
		this.quantity = quantity;
		this.price = price;
		this.setType(laundryItemType);
	}


	public void setLaundryItemId(String laundryItemId) {
		this.laundryItemId = laundryItemId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public boolean isSameDaySelected() {
		return sameDaySelected;
	}
	
	public void setSameDaySelected(boolean sameDaySelected) {
		this.sameDaySelected = sameDaySelected;
	}
	

}
