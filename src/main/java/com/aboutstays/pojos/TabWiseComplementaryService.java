package com.aboutstays.pojos;

import java.util.List;

public class TabWiseComplementaryService {

	private String tabName;
	private List<TitleVsDescriptionList> services;
	
	public String getTabName() {
		return tabName;
	}
	public void setTabName(String tabName) {
		this.tabName = tabName;
	}
	public List<TitleVsDescriptionList> getServices() {
		return services;
	}
	
	public void setServices(List<TitleVsDescriptionList> services) {
		this.services = services;
	}
	
}
