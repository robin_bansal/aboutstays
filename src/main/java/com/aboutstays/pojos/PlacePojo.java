package com.aboutstays.pojos;

public class PlacePojo {

	private Integer placeType;
	private String placeName;
	private String parentPlaceId;
	private String iconUrl;
	private String imageUrl;
	
	public Integer getPlaceType() {
		return placeType;
	}
	public void setPlaceType(Integer placeType) {
		this.placeType = placeType;
	}
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	public String getParentPlaceId() {
		return parentPlaceId;
	}
	public void setParentPlaceId(String parentPlaceId) {
		this.parentPlaceId = parentPlaceId;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
}
