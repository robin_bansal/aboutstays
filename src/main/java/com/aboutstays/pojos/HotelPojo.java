package com.aboutstays.pojos;

import java.util.List;

public class HotelPojo {

	private String hotelId;
	private Integer hotelType;
	private HotelGeneralInformation generalInformation;
	private List<String> roomsIds;
	private ReviewsAndRatings reviewsAndRatings;
	private Integer partnershipType;
	private ReservationServicePojo reservationServicePojo;
	private String timezone;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getHotelType() {
		return hotelType;
	}
	public void setHotelType(Integer hotelType) {
		this.hotelType = hotelType;
	}
	public HotelGeneralInformation getGeneralInformation() {
		return generalInformation;
	}
	public void setGeneralInformation(HotelGeneralInformation generalInformation) {
		this.generalInformation = generalInformation;
	}
	public ReviewsAndRatings getReviewsAndRatings() {
		return reviewsAndRatings;
	}
	public void setReviewsAndRatings(ReviewsAndRatings reviewsAndRatings) {
		this.reviewsAndRatings = reviewsAndRatings;
	}

	public List<String> getRoomsIds() {
		return roomsIds;
	}
	public void setRoomsIds(List<String> roomsIds) {
		this.roomsIds = roomsIds;
	}
	public Integer getPartnershipType() {
		return partnershipType;
	}
	public void setPartnershipType(Integer partnershipType) {
		this.partnershipType = partnershipType;
	}
	public ReservationServicePojo getReservationServicePojo() {
		return reservationServicePojo;
	}
	public void setReservationServicePojo(ReservationServicePojo reservationServicePojo) {
		this.reservationServicePojo = reservationServicePojo;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	
}
