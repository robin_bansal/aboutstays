package com.aboutstays.pojos;

public class TypeValuePojo {
	
	private Integer type;
	private String value;
	
	public TypeValuePojo(Integer type, String value) {
		this.type = type;
		this.value = value;
	}
	
	public TypeValuePojo(){}
	
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	

}
