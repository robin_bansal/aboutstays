package com.aboutstays.pojos;

import java.util.List;

import com.aboutstays.pojos.SupportedAirports;

public class AirportServicePojo {
	
	private String id;
	private List<SupportedAirports> supportedAirportList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<SupportedAirports> getSupportedAirportList() {
		return supportedAirportList;
	}

	public void setSupportedAirportList(List<SupportedAirports> supportedAirportList) {
		this.supportedAirportList = supportedAirportList;
	}
	
}
