package com.aboutstays.pojos;

import java.util.List;

public class RoomWithAmenityIds {

	private String roomId;
	private List<String> amentityIds;
	
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public List<String> getAmentityIds() {
		return amentityIds;
	}
	public void setAmentityIds(List<String> amentityIds) {
		this.amentityIds = amentityIds;
	}
	
	
}
