package com.aboutstays.pojos;

public class LaundryItemPojo {
	
	private String itemId;
	private String hotelId;
	private String laundryServicesId;
	private String name;
	private int laundryFor;//Refer LaundryFor enum
	private int type;//Refer LaundryItemType
	private Double mrp;
	private Double price;
	private String category;
	private Double sameDayPrice;
	private boolean showOnApp;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getLaundryServicesId() {
		return laundryServicesId;
	}
	public void setLaundryServicesId(String laundryServicesId) {
		this.laundryServicesId = laundryServicesId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLaundryFor() {
		return laundryFor;
	}
	public void setLaundryFor(int laundryFor) {
		this.laundryFor = laundryFor;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	public Double getSameDayPrice() {
		return sameDayPrice;
	}
	
	public void setSameDayPrice(Double sameDayPrice) {
		this.sameDayPrice = sameDayPrice;
	}
	public boolean isShowOnApp() {
		return showOnApp;
	}
	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}
}
