package com.aboutstays.pojos;

public class EarlyCheckinRequest {

	private String scheduledCheckinTime;
	private String requestedCheckinTime;
	private Double charges;
	public String getScheduledCheckinTime() {
		return scheduledCheckinTime;
	}
	public void setScheduledCheckinTime(String scheduledCheckinTime) {
		this.scheduledCheckinTime = scheduledCheckinTime;
	}
	public String getRequestedCheckinTime() {
		return requestedCheckinTime;
	}
	public void setRequestedCheckinTime(String requestedCheckinTime) {
		this.requestedCheckinTime = requestedCheckinTime;
	}
	public Double getCharges() {
		return charges;
	}
	public void setCharges(Double charges) {
		this.charges = charges;
	}
}
