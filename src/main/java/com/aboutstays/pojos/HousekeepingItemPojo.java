package com.aboutstays.pojos;

public class HousekeepingItemPojo {

	private String itemId;
	private String hotelId;
	private String housekeepingServicesId;
	private String name;
	private boolean isComplementory;
	private Double mrp;
	private Double price;
	private int type;//Refer HousekeepingItemType
	private String category;
	private boolean showOnApp;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getHousekeepingServicesId() {
		return housekeepingServicesId;
	}
	public void setHousekeepingServicesId(String housekeepingServicesId) {
		this.housekeepingServicesId = housekeepingServicesId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isComplementory() {
		return isComplementory;
	}
	public void setComplementory(boolean isComplementory) {
		this.isComplementory = isComplementory;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isShowOnApp() {
		return showOnApp;
	}
	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}
	
}