package com.aboutstays.pojos;

public class InternetOrderPojo extends BaseUserOrderRequestPojo{
	
	private String id;
	private InternetPack internetPack;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public InternetPack getInternetPack() {
		return internetPack;
	}
	public void setInternetPack(InternetPack internetPack) {
		this.internetPack = internetPack;
	}
	
	

}
