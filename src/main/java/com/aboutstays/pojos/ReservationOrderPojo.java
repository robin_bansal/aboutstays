package com.aboutstays.pojos;

public class ReservationOrderPojo extends BaseUserOrderRequestPojo{

	private String id;
	private String rsItemId;
	private double price;
	private String duration;
	private String name;
	private Integer pax;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRsItemId() {
		return rsItemId;
	}
	public void setRsItemId(String rsItemId) {
		this.rsItemId = rsItemId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPax() {
		return pax;
	}
	public void setPax(Integer pax) {
		this.pax = pax;
	}
	
	
}
