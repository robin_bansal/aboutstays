package com.aboutstays.pojos;

import java.util.List;

public class Request extends NameLogoPojo {
	
	private Integer type;
	private Integer status;
	private List<OrderDetails> listOrders;
	private String lastUpdatedOn;
	private int serviceCharge;

	public Request(){};
	
	public Request(String title, String description, String logoUrl, String time, String date, Integer type,
			Integer status, List<OrderDetails> listOrders, String lastUpdatedOn, int serviceCharge) {
		super(title, description, logoUrl, time, date);
		this.type = type;
		this.status = status;
		this.listOrders = listOrders;
		this.lastUpdatedOn = lastUpdatedOn;
		this.serviceCharge = serviceCharge;
	}

	public int getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(int serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public List<OrderDetails> getListOrders() {
		return listOrders;
	}

	public void setListOrders(List<OrderDetails> listOrders) {
		this.listOrders = listOrders;
	}

	public String getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(String lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
	
	

	
}
