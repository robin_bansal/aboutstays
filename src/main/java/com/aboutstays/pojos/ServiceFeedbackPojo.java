package com.aboutstays.pojos;

public class ServiceFeedbackPojo {
	
	private String id;
	private String hotelId;
	private String userId;
	private String staysId;
	private String uosId;
	private int timeTaken;
	private int quality;
	private String comments;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUosId() {
		return uosId;
	}
	public void setUosId(String uosId) {
		this.uosId = uosId;
	}
	public int getTimeTaken() {
		return timeTaken;
	}
	public void setTimeTaken(int timeTaken) {
		this.timeTaken = timeTaken;
	}
	public int getQuality() {
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	
	

}
