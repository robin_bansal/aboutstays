package com.aboutstays.pojos;

public class SupportedLaundryType extends GeneralItemTypeInfo{
	
	private boolean sameDayAvailable;
	
	public boolean isSameDayAvailable() {
		return sameDayAvailable;
	}
	
	public void setSameDayAvailable(boolean sameDayAvailable) {
		this.sameDayAvailable = sameDayAvailable;
	}
}
