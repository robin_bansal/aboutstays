package com.aboutstays.pojos;

public class GoesWellWithItem {
	
	private String foodItemId;
	
	public GoesWellWithItem() {
	}
	public GoesWellWithItem(String foodItemId) {
		this.foodItemId = foodItemId;
	}
	public String getFoodItemId() {
		return foodItemId;
	}
	public void setFoodItemId(String foodItemId) {
		this.foodItemId = foodItemId;
	}

}
