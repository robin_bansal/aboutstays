package com.aboutstays.pojos;

public class MasterCityPojo {

	private String name;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
