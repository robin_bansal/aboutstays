package com.aboutstays.pojos;

public class BookingPojo {
	private String checkInDate;
	private String checkOutDate;
	private BookedByUser bookedByUser;
	private BookedForUser bookedForUser;
	private String reservationNumber;

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public BookedByUser getBookedByUser() {
		return bookedByUser;
	}

	public void setBookedByUser(BookedByUser bookedByUser) {
		this.bookedByUser = bookedByUser;
	}

	public BookedForUser getBookedForUser() {
		return bookedForUser;
	}

	public void setBookedForUser(BookedForUser bookedForUser) {
		this.bookedForUser = bookedForUser;
	}

	public String getReservationNumber() {
		return reservationNumber;
	}

	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}
}
