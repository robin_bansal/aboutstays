package com.aboutstays.pojos;

public class FoodItemCartRequest extends CartParams{

	
	private FoodItemOrderDetails foodItemOrderDetails;

	public FoodItemOrderDetails getFoodItemOrderDetails() {
		return foodItemOrderDetails;
	}
	public void setFoodItemOrderDetails(FoodItemOrderDetails foodItemOrderDetails) {
		this.foodItemOrderDetails = foodItemOrderDetails;
	}
	
}

