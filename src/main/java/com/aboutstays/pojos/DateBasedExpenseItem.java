package com.aboutstays.pojos;

import java.util.List;

public class DateBasedExpenseItem {
	
	private Double dateWiseTotal;
	private List<TimeBasedExpenseItem> listTimeBasedExpenseItem;
	
	public List<TimeBasedExpenseItem> getListTimeBasedExpenseItem() {
		return listTimeBasedExpenseItem;
	}
	public void setListTimeBasedExpenseItem(List<TimeBasedExpenseItem> listTimeBasedExpenseItem) {
		this.listTimeBasedExpenseItem = listTimeBasedExpenseItem;
	}
	public Double getDateWiseTotal() {
		return dateWiseTotal;
	}
	public void setDateWiseTotal(Double dateWiseTotal) {
		this.dateWiseTotal = dateWiseTotal;
	}
	
	

}
