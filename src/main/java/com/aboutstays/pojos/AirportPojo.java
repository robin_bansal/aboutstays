package com.aboutstays.pojos;

import java.util.List;

public class AirportPojo {

	private String name;
	private String type;
	private Address address;
	private String description;
	private List<String> amenities;
	private String imageUrl;
	private List<String> travelOptions;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getAmenities() {
		return amenities;
	}
	public void setAmenities(List<String> amenities) {
		this.amenities = amenities;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<String> getTravelOptions() {
		return travelOptions;
	}
	public void setTravelOptions(List<String> travelOptions) {
		this.travelOptions = travelOptions;
	}
}
