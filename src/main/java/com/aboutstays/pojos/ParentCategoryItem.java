package com.aboutstays.pojos;

import java.util.List;

public class ParentCategoryItem {

	private String title;
	private List<ChildCategoryItem> listChildCategory;

	public ParentCategoryItem() {};
	
	public ParentCategoryItem(String title, List<ChildCategoryItem> listChildCategory) {
		this.title = title;
		this.listChildCategory = listChildCategory;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<ChildCategoryItem> getListChildCategory() {
		return listChildCategory;
	}
	public void setListChildCategory(List<ChildCategoryItem> listChildCategory) {
		this.listChildCategory = listChildCategory;
	}
	
	
}
