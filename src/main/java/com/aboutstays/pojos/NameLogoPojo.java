package com.aboutstays.pojos;

public class NameLogoPojo {

	private String title;
	private String description;
	private String logoUrl;
	private String time;
	private String date;
	
	public NameLogoPojo(){};
	
	public NameLogoPojo(String title, String description, String logoUrl, String time, String date) {
		this.title = title;
		this.description = description;
		this.logoUrl = logoUrl;
		this.time = time;
		this.date = date;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
