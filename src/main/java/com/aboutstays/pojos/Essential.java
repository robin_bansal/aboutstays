package com.aboutstays.pojos;

public class Essential extends MasterAmenityPojo{

	private String essentialId;

	public Essential(){}
	
	public Essential(String amenityId, String name) {
		this.essentialId = amenityId;
	}

	public String getEssentialId() {
		return essentialId;
	}
	
	public void setEssentialId(String amenityId) {
		this.essentialId = amenityId;
	}
}
