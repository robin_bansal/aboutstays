package com.aboutstays.pojos;

public class NotificationPojo {

	private String title;
	private String description;
	private String iconUrl;
	private String type;//Tab name
	private String userId;
	private String staysId;
	private String hotelId;
	private int maxUiLines = 3;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public int getMaxUiLines() {
		return maxUiLines;
	}
	public void setMaxUiLines(int maxUiLines) {
		this.maxUiLines = maxUiLines;
	}
}
