package com.aboutstays.pojos;

public class UserOptedServicePojo extends BaseSessionPojo {

	private String id;
	private String refId;
	private Integer refCollectionType;
	private String gsId;
	private String sfId;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public Integer getRefCollectionType() {
		return refCollectionType;
	}
	public void setRefCollectionType(Integer refCollectionType) {
		this.refCollectionType = refCollectionType;
	}
	public String getGsId() {
		return gsId;
	}
	public void setGsId(String gsId) {
		this.gsId = gsId;
	}
	public String getSfId() {
		return sfId;
	}
	public void setSfId(String sfId) {
		this.sfId = sfId;
	}
}
