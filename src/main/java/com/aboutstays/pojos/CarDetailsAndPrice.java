package com.aboutstays.pojos;

public class CarDetailsAndPrice {
	
	private CarDetails carDetails;
	private Double price;
	
	public CarDetailsAndPrice() {}
	
	public CarDetailsAndPrice(CarDetails carDetails, Double price) {
		this.carDetails = carDetails;
		this.price = price;
	}
	public CarDetails getCarDetails() {
		return carDetails;
	}
	public void setCarDetails(CarDetails carDetails) {
		this.carDetails = carDetails;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

}
