package com.aboutstays.pojos;

public class GeneralDeviationInfo {
	
	private double deviationInHours;
	private double price;
	
	public double getDeviationInHours() {
		return deviationInHours;
	}
	public void setDeviationInHours(double deviationInHours) {
		this.deviationInHours = deviationInHours;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
