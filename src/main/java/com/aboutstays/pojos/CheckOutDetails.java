package com.aboutstays.pojos;

import java.util.List;

public class CheckOutDetails {
	
	List<String> listGSIds;

	public List<String> getListGSIds() {
		return listGSIds;
	}

	public void setListGSIds(List<String> listGSIds) {
		this.listGSIds = listGSIds;
	}

}
