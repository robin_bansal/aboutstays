package com.aboutstays.pojos;

public class DirectionDetail extends Amenity {

	private String directionInfo;
	
	public String getDirectionInfo() {
		return directionInfo;
	}
	
	public void setDirectionInfo(String directionInfo) {
		this.directionInfo = directionInfo;
	}
}
