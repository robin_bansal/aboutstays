package com.aboutstays.pojos;

import com.aboutstays.enums.GetStayResponse;

public class StaysHotelPojo{
	
	private GetStayResponse staysPojo;
	private HotelGeneralInformation generalInfo;
	private RoomCategoryPojo roomPojo;
	private Integer hotelType;
	private Integer partnershipType;
	private String actualCheckedOut;
	private String actualCheckedIn;
	
	public GetStayResponse getStaysPojo() {
		return staysPojo;
	}
	public void setStaysPojo(GetStayResponse staysPojo) {
		this.staysPojo = staysPojo;
	}
	public HotelGeneralInformation getGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(HotelGeneralInformation generalInfo) {
		this.generalInfo = generalInfo;
	}
	public RoomCategoryPojo getRoomPojo() {
		return roomPojo;
	}
	public void setRoomPojo(RoomCategoryPojo roomPojo) {
		this.roomPojo = roomPojo;
	}
	public Integer getHotelType() {
		return hotelType;
	}
	public void setHotelType(Integer hotelType) {
		this.hotelType = hotelType;
	}
	public Integer getPartnershipType() {
		return partnershipType;
	}
	public void setPartnershipType(Integer partnershipType) {
		this.partnershipType = partnershipType;
	}
	public String getActualCheckedOut() {
		return actualCheckedOut;
	}
	public void setActualCheckedOut(String actualCheckedOut) {
		this.actualCheckedOut = actualCheckedOut;
	}
	public String getActualCheckedIn() {
		return actualCheckedIn;
	}
	public void setActualCheckedIn(String actualCheckedIn) {
		this.actualCheckedIn = actualCheckedIn;
	}

	
	

}
