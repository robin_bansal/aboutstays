package com.aboutstays.pojos;

import java.util.List;

public class LaundryOrderPojo extends BaseUserOrderRequestPojo {
	
	private List<LaundryItemOrderDetails> listOrders;
	private int laundryPreference;

	public int getLaundryPreference() {
		return laundryPreference;
	}
	public void setLaundryPreference(int laundryPreference) {
		this.laundryPreference = laundryPreference;
	}
	public List<LaundryItemOrderDetails> getListOrders() {
		return listOrders;
	}
	public void setListOrders(List<LaundryItemOrderDetails> listOrders) {
		this.listOrders = listOrders;
	}
	

}
