package com.aboutstays.pojos;

public class BaseUserOrderRequestPojo extends UserStaysHotelGSIdsPojo{

	private Long deliveryTime;
	private String deliverTimeString;
	private String comment;
	private boolean requestedNow;

	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public boolean isRequestedNow() {
		return requestedNow;
	}
	
	public void setRequestedNow(boolean requestedNow) {
		this.requestedNow = requestedNow;
	}
	
	public String getDeliverTimeString() {
		return deliverTimeString;
	}
	
	public void setDeliverTimeString(String deliverTimeString) {
		this.deliverTimeString = deliverTimeString;
	}
}
