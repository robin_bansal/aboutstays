package com.aboutstays.pojos;

public class MaintenanceRequestPojo extends BaseUserOrderRequestPojo {
	
	private String id;
	private int type;
	private String itemName;
	private String problemType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getProblemType() {
		return problemType;
	}
	public void setProblemType(String problemType) {
		this.problemType = problemType;
	}

}
