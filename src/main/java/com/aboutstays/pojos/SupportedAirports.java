package com.aboutstays.pojos;

import java.util.List;

public class SupportedAirports {
	
	private String airportName;
	private List<CarDetailsAndPrice> carDetailsAndPriceList;
	
	public SupportedAirports() {}
	
	public SupportedAirports(String airportName, List<CarDetailsAndPrice> carDetailsAndPriceList) {
		this.airportName = airportName;
		this.carDetailsAndPriceList = carDetailsAndPriceList;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public List<CarDetailsAndPrice> getCarDetailsAndPriceList() {
		return carDetailsAndPriceList;
	}
	public void setCarDetailsAndPriceList(List<CarDetailsAndPrice> carDetailsAndPriceList) {
		this.carDetailsAndPriceList = carDetailsAndPriceList;
	}
}
