package com.aboutstays.pojos;

import java.util.List;

public class CheckinPojo extends BaseSessionPojo {
	private PersonalInformation personalInformation;
	private List<IdentityDoc> identityDocuments;
	private List<StayPreferences> listStayPreferences;//StayPreferencesType
	private List<CheckinSpecialRequest> specialRequests;
	private boolean officialTrip;
	private List<CompanyInfo> companies;
	private EarlyCheckinRequest earlyCheckinRequest;
	private boolean earlyCheckinRequested;
	private Boolean checkinFormCompleted;
	private Boolean idVerified;
	private Boolean isEarlyCheckinFee;


	
	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}
	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}
	public List<IdentityDoc> getIdentityDocuments() {
		return identityDocuments;
	}
	public void setIdentityDocuments(List<IdentityDoc> identityDocuments) {
		this.identityDocuments = identityDocuments;
	}
	public List<StayPreferences> getListStayPreferences() {
		return listStayPreferences;
	}
	public void setListStayPreferences(List<StayPreferences> listStayPreferences) {
		this.listStayPreferences = listStayPreferences;
	}
	public List<CheckinSpecialRequest> getSpecialRequests() {
		return specialRequests;
	}
	public void setSpecialRequests(List<CheckinSpecialRequest> specialRequests) {
		this.specialRequests = specialRequests;
	}
	public boolean isOfficialTrip() {
		return officialTrip;
	}
	public void setOfficialTrip(boolean officialTrip) {
		this.officialTrip = officialTrip;
	}
	public List<CompanyInfo> getCompanies() {
		return companies;
	}
	public void setCompanies(List<CompanyInfo> companies) {
		this.companies = companies;
	}
	public EarlyCheckinRequest getEarlyCheckinRequest() {
		return earlyCheckinRequest;
	}
	public void setEarlyCheckinRequest(EarlyCheckinRequest earlyCheckinRequest) {
		this.earlyCheckinRequest = earlyCheckinRequest;
	}
	public boolean getEarlyCheckinRequested() {
		return earlyCheckinRequested;
	}
	public void setEarlyCheckinRequested(boolean earlyCheckinRequested) {
		this.earlyCheckinRequested = earlyCheckinRequested;
	}
	public Boolean getCheckinFormCompleted() {
		return checkinFormCompleted;
	}
	public void setCheckinFormCompleted(Boolean checkinFormCompleted) {
		this.checkinFormCompleted = checkinFormCompleted;
	}
	public Boolean getIdVerified() {
		return idVerified;
	}
	public void setIdVerified(Boolean idVerified) {
		this.idVerified = idVerified;
	}
	public Boolean getIsEarlyCheckinFee() {
		return isEarlyCheckinFee;
	}
	public void setIsEarlyCheckinFee(Boolean isEarlyCheckinFee) {
		this.isEarlyCheckinFee = isEarlyCheckinFee;
	}
}
