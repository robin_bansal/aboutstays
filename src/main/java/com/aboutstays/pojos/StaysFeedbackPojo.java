package com.aboutstays.pojos;

import java.util.List;

public class StaysFeedbackPojo extends UserStaysHotelGSIdsPojo{
	
	private String id;
	private List<NameQualityTimetaken> listFeedback;
	private String specialStaff;
	private String comment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<NameQualityTimetaken> getListFeedback() {
		return listFeedback;
	}
	public void setListFeedback(List<NameQualityTimetaken> listFeedback) {
		this.listFeedback = listFeedback;
	}
	public String getSpecialStaff() {
		return specialStaff;
	}
	public void setSpecialStaff(String specialStaff) {
		this.specialStaff = specialStaff;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	

	
	
	
	
	
	

}
