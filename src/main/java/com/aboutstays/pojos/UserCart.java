package com.aboutstays.pojos;

import java.util.Map;

public class UserCart<T> {

	private Map<Integer, StaysIdVsCartData<T>> cartTypeVsDataMap;
	
	public Map<Integer, StaysIdVsCartData<T>> getCartTypeVsDataMap() {
		return cartTypeVsDataMap;
	}
	
	public void setCartTypeVsDataMap(Map<Integer, StaysIdVsCartData<T>> cartTypeVsDataMap) {
		this.cartTypeVsDataMap = cartTypeVsDataMap;
	}
}
