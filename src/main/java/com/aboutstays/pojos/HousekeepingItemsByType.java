package com.aboutstays.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class HousekeepingItemsByType {
	
	private int type;//Refer HousekeepingItemType enum
	private String typeName;
	private String imageUrl;
	
	public HousekeepingItemsByType() {}
	
	public HousekeepingItemsByType(int type) {
		this.type = type;
	}
	public HousekeepingItemsByType(int type, String typeName){
		this.type = type;
		this.typeName = typeName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	
	
}
