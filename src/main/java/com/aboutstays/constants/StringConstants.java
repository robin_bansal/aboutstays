package com.aboutstays.constants;

public interface StringConstants {

	String INITIATE_CHECK_IN = "INITIATE CHECKIN";
	String MODIFY_CHECKIN = "MODIFY CHECKIN";
	String ITEM = "Item";
	String QTY = "Qty";
	String PAX = "Pax";
	String PROBLEM_TYPE = "Problem Type";
	String RATE = "Rate";
	String PRICE = "Price";
}
