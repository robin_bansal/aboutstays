package com.aboutstays.constants;

public interface PropertyConstant {
	
	String PROCESS_AWS = "aws";
	String PROPERTY_IAM_ACCESS_KEY = "iamAccessKey";
	String PROPERTY_IAM_SECRET_KEY = "iamSecretKey";
	String BUCKET_NAME = "bucketName";
	String IMAGE_FILE_PATH = "imageFilePath";
	String DOCS_FILE_PATH = "docsFilePath";
	String PROCESS_STAYS = "stays";
	String PROPERTY_SHOW_UNACTIVATED_SERVICES = "showUnactivatedServices";
	String PROPERTY_ENABLE_CHECKOUT_SERVICES_HOURS = "enableCheckoutServicesHours";
	String PROPERTY_FUTURE_STAYLINE_HOURS = "futureStaylineHours";
	String PROPERTY_APPROVE_CHECKIN_AUTOMATICALLY = "approveCheckinAutomatically";
	String PROPERTY_SHOW_CHECKOUT_BUTTON = "showCheckoutButton";
	String PROPERTY_TITLES = "titles";
	String PROPERTY_IDENTITY_DOCS = "identityDocs";
	String PROCESS_MESSAGE = "msg";
	String PROPERT_DEBUG_MODE = "debugMode";
	
}
