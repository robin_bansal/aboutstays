package com.aboutstays.constants;

public interface RestMappingConstants {

	String APP_BASE = "";
	String REQUEST = "/request";
	String RESPONSE = "/response";
	String BATCH = "/batch";

	interface UserConstants {
		String BASE = APP_BASE + "/user";
		String SIGNUP = "/signup";
		String LOGIN = "/login";
		String SAMPLE = "/sample";
		String GET = "/get";
		String GENERATE_OTP = "/generateOtp";
		String OTP_VALIDATED = "/otpValidated";
		String UPDATE_BY_TYPE = "/udpate";
		String UPDATE_PASSWORD = "/updatePassword";
		String FORGOT_PASSWORD = "/forgotPassword";
		String VALIDATE_SIGNUP = "/validateSignup";
		String UPDATE_PERSONAL_DETAILS = "/updatePersonalDetails";
		String UPDATE_CONTACT_DETAILS = "/updateContactDetails";
		String UPDATE_STAY_PREFS = "/updateStayPrefs";
		String UPDATE_IDENTITY_DOCS = "/updateIdentityDocs";
		String UPDATE_PROFILE_PICTURE = "/updateProfilePic";
		String CHECK_EXISTING_STAYS = "/checkExistingStays";
		String DELETE_IDENTITY_DOC = "/deleteIdentityDoc";
	}

	interface UserParameters {
		String TYPE = "type";
		String VALUE = "value";
		String IS_VALIDATED = "isValidated";
		String NAME_ON_DOCUMENT = "nameOnDoc";
		String DOCUMENT_NUMBER = "docNumber";
		String OVERWRITE = "overwrite";
		String DOCUMENT_TYPE = "docType";
		String SCANNED_COPY = "scannedCopy";
		String USER_ID = "userId";
		String DISPLAY_PICTURE = "displayPicture";
		String DOC_URL = "docUrl";

	}

	interface HotelConstants {
		String BASE = APP_BASE + "/hotel";
		String SAMPLE_REQUEST = "/sample";
		String CREATE = "/create";
		String GET_HOTELS = "/getHotels";
		String UPDATE_HOTEL_BY_TYPE = "/update";
		String GET_ROOMS = "/getRooms";
		String HOTEL_BY_ID = "/get";
		String GENERAL_INFO_BY_ID = "/generalInfo";
		String CITY_AND_NAME = "/cityAndName";
		String UPDATE_BY_TYPE = "update";
		String UPDATE_GENERAL_INFO = "updateGeneralInfo";
		String DELETE_HOTEL = "delete";
		String UPDATE_PARTNERSHIP_TYPE = "/updatePartnership";
		String GET_GENERAL_SERVICES = "/getGS";
		String UPDATE_GENERAL_SERVICES = "/updateGS";
		String GET_ALL_GS = "/getAllGS";
		String LINK_AMENITIES = "/linkAmenities";
		String LINK_ESSENTIALS = "/linkEssentials";
		String LINK_AWARD = "/linkAward";
		String LINK_CARD_ACCEPTED = "/linkCardAccepted";
		String LINK_LOYALTY_PROGRAM = "/linkLoyaltyProgram";
		String DELETE_AWARD = "/deleteAward";
		String DELETE_LOYALTY_PROGRAMS = "/deleteLoyaltyProgram";
		String GET_HOTEL_META_DATA = "/getHotelMetaData";

	}

	interface HotelParameters {
		String HOTEL_ID = "hotelId";
		String HOTEL_TYPE = "hotelType";
		String STAY_STATUS = "stayStatus";
		String IS_GI = "isGI";
	}

	interface HotelOtherInfoConstants {
		String BASE = APP_BASE + "/hotelOtherInfo";
		String SAMPLE = "/sample";
		String ADD = "/add";
		String GET = "/get";
		String ADD_OR_REMOVE_CARDS = "/addOrRemoveCards";
		String ADD_OR_REMOVE_AWARDS = "/addOrRemoveAwards";
		String DELETE = "/delete";

	}

	interface HotelOtherInfoParameters {
		String ID = "id";
	}

	interface RoomCategoryConstants {
		String BASE = APP_BASE + "/roomCategory";
		String CREATE = "/create";
		String UPDATE = "/update";
		String GET = "/get";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String KEY_ID = "id";
		String DELETE_AND_REMOVE_FROM_HOTEL = "/deleteAndRemove";
		String CREATE_AND_ADD_TO_HOTEL = "/createAndAddToHotel";
		String SET_EARLY_CHECKIN_INFO = "/setCheckinInfo";
		String GET_EARLY_CHECKIN_INFO = "/getCheckinInfo";
		String SET_LATE_CHECKOUT_INFO = "/setLateCheckoutInfo";
		String GET_LATE_CHECKOUT_INFO = "/getLateCheckoutInfo";
		String LINK_IN_ROOM_AMENITIES = "linkInRoomAmenities";
		String LINK_PREFERENCE_BASED_AMENITIES = "linkPreferenceBasedAmenities";
		String GET_BY_HOTEL_ID = "getByHotelId";
	}

	interface RoomCategoryParameters {
		String ROOM_CATEGORY_ID = "roomCategoryId";
		String HOTEL_ID = "hotelId";
	}

	interface RoomConstants {
		String BASE = APP_BASE + "/room";
		String CREATE = "/create";
		String UPDATE = "/update";
		String GET = "/get";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String LINK_ADDITIONAL_AMENITIES = "/linkAdditionalAmenities";
		String GET_BY_ROOM_CATEGORY_ID = "/getByRoomCategoryId";
		String GET_BY_WING_Id = "/getByWingId";
		String GET_BY_FLOOR_ID = "/getByFloorId";
		String GET_BY_HOTEL_Id = "/getByHotelId";
		String BATCH_DELETE_ROOMS = BATCH + "/delete";
		String BATCH_UPDATE_METAINFO = BATCH + "/updateMetaInfo";
		String BATCH_LINK_ADDITIONAL_AMENITIES = BATCH + "/linkAdditionalAmenities";
	}

	interface RoomParameters {
		String ROOM_CATEGORY_ID = "roomCategoryId";
		String ROOM_ID = "roomId";
		String WING_ID = "wingId";
		String FLOOR_ID = "floorId";
		String HOTEL_ID = "hotelId";
		String KEY_ID = "id";
	}

	interface BookingConstants {
		String BASE = APP_BASE + "/booking";
		String SAMPLE = "/sample";
		String CREATE = "/create";
		String GET = "/get";
		String DELETE = "/delete";
		String UPDATE = "/update";
	}

	interface BookingParameters {
		String RESERVATION_NUMBER = "reservationNumber";
	}

	interface StaysConstants {
		String BASE = APP_BASE + "/stays";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String DELETE = "/delete";
		String GET_ALL = "/getAll";
		String GET = "/get";
		String GET_STAYS_WITH_HOTEL_INFO = "/getAllWithHotelInfo";
		String GET_ALL_STAYS_OF_USER = "/getUsersAllStays";
	}

	interface StaysParameters {
		String STAY_ID = "id";
		String USER_ID = "userId";
	}

	interface ServiceConstants {
		String BASE = APP_BASE + "/service";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
	}

	interface ServiceParameters {
		String HOTEL_ID = "hotelId";
	}

	interface FileConstants {
		String BASE = APP_BASE + "/file";
		String IMAGE_UPLOAD = "/image/upload";
		String PERIOD = ".";
		String UPLOAD_MULTIPLE = "/uploadMultiple";
		String UPLOAD_DOC = "/uploadDoc";
	}

	interface FileParameters {
		String FILE = "file";
		String KEY_NAMES = "keys";
		String KEY_FILES = "files";
		String DOC_FILE = "docFile";
	};

	interface GeneralServicesConstants {
		String BASE = APP_BASE + "/generalService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELTE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface GeneralServiceParameters {
		String STATUS = "status";
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String DATE = "date";
	}

	interface GeneralInfoConstants {
		String BASE = APP_BASE + "/generalInfo";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELTE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface GeneralInfoParameters {
		String STATUS = "status";
		String ID = "id";
	}

	interface AirportServiceConstants {
		String BASE = APP_BASE + "/airportService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface AirportServiceParameters {
		String ID = "id";
	}

	interface CommuteServiceConstants {
		String BASE = APP_BASE + "/commuteService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface CommuteServiceParameters {
		String ID = "id";
	}

	interface ServiceFeedbackConstants {
		String BASE = APP_BASE + "/serviceFeedback";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_BY_UOSID = "/getByUosId";

	}

	interface ServiceFeedbackParameters {
		String ID = "id";
		String UOS_ID = "uosId";

	}

	interface DriverConstants {
		String BASE = APP_BASE + "/driver";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface DriverParameters {
		String ID = "id";
	}

	interface SuggestedHotelsConstants {
		String BASE = APP_BASE + "/suggestedHotels";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface SuggestedHotelsParameters {
		String ID = "id";
	}

	interface CarConstants {
		String BASE = APP_BASE + "/car";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface CarParameters {
		String ID = "id";
	}

	interface HouseRulesConstants {
		String BASE = APP_BASE + "/houseRules";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface HouseRulesParameters {
		String ID = "id";
	}

	interface AirportPickupConstants {
		String BASE = APP_BASE + "/airportPickup";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String APPROVE_STATUS = "/approveStatus";
		String CANCEL = "/cancel";
	}

	interface AirportPickupParameters {
		String ID = "id";
		String USER_ID = "userId";
		String UOS_ID = "uosId";
	}

	interface UserOptedServiceConstants {
		String BASE = APP_BASE + "/userOptedService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_BY_USER_AND_STAY = "/getByUserAndStay";
		String GET_BY_USER_STAYS_HOTEL = "getByUserStaysHotel";
	}

	interface UserOptedServiceParameters {
		String ID = "id";
		String USER_ID = "userId";
	}

	interface RoomServiceConstants {
		String BASE = APP_BASE + "/roomService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String SEARCH_FOOD_ITEM = "/searchFoodItem";
	}

	interface RoomServiceParameters {
		String ID = "id";
		String QUERY = "query";
	}

	interface FoodItemConstants {
		String BASE = APP_BASE + "/foodItem";
		String ADD = "/add";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_ROOM_SERVICE_ID = "/getAllByRoomServiceId";
		String ADD_ALL = "/addAll";
		String LINK_GOES_LIST = "/linkGoesList";
		String FETCH_BY_TYPE = "/fetchByType";
		String DELETE = "/delete";
		String BATCH_UPDATE = BATCH + UPDATE;
	}

	interface FoodItemParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String ROOM_SERVICE_ID = "roomServicesId";
		String TYPE = "type";
	}

	interface HousekeepingItemConstants {
		String BASE = APP_BASE + "/housekeepingItem";
		String SAMPLE = "/sample";
		String ADD = "/add";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_HK_SERVICE_ID = "/getAllByHKServiceId";
		String ADD_ALL = "/addAll";
		String LINK_GOES_LIST = "/linkGoesList";
		String FETCH_BY_TYPE = "/fetchByType";
		String DELETE = "/delete";
		String BATCH_UPDATE = BATCH + UPDATE;
	}

	interface HousekeepingItemParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String HK_SERVICE_ID = "hkServicesId";
		String TYPE = "type";
	}

	interface MaintenanceItemConstants {
		String BASE = APP_BASE + "/maintenanceItem";
		String SAMPLE = "/sample";
		String ADD = "/add";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String DELETE = "/delete";
		String FETCH_BY_TYPE = "/fetchByType";
	}

	interface MaintenanceItemParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String MT_SERVICE_ID = "mtServicesId";
		String TYPE = "type";
	}

	interface ReservationItemConstants {
		String BASE = APP_BASE + "/reservationItem";
		String SAMPLE = "/sample";
		String ADD = "/add";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String DELETE = "/delete";
		String BATCH_UPDATE = BATCH+UPDATE;
	}

	interface ReservationItemParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String CATEGORY_ID = "categoryId";
	}

	interface EntertainmentItemConstants {
		String BASE = APP_BASE + "entItem";
		String ADD = "/add";
		String ADD_MULTIPLE = "/addMultiple";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_CATEGORY = "/getAllbyCategory";
		String DELETE = "/delete";
	}

	interface EntertainmentItemParameters {
		String ID = "id";
		String ENT_SERVICE_ID = "entServiceId";
		String GENERE_ID = "genereId";
	}

	interface LaundryServiceConstants {
		String BASE = APP_BASE + "/laundry";
		String ADD = "/add";
		String GET = "/get";
		String GET_ALL = "/getAll";
		String GET_BY_HOTEL_ID = "/getByHotelId";
		String UPDATE = "/update";
		String DELETE = "/delete";
	}

	interface LaundryServiceParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
	}

	interface LaundryItemConstants {
		String BASE = APP_BASE + "/laundryItem";
		String ADD = "/add";
		String GET = "/get";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_LAUNDRY_SERVICE_ID = "/getAllByLaundryServicesId";
		String UPDATE = "/update";
		String DELETE = "/delete";
		String GET_BY_TYPE = "/getByLaundryType";
		String SEARCH = "/search";
		String BATCH_UPDATE = BATCH + UPDATE;
	}

	interface LaundryItemParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String LAUNDRY_SERVICES_ID = "laundryServicesId";
		String LAUNDRY_TYPE = "laundryType";
		String QUERY = "query";
	}

	interface HousekeepingServiceConstants {
		String BASE = APP_BASE + "/housekeepingService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String SEARCH_HK_ITEM = "/searchHKItem";
	}

	interface HousekeepingServiceParameters {
		String ID = "id";
		String QUERY = "query";
	}

	interface FoodOrderConstants {
		String BASE = APP_BASE + "/foodOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL_BY_USER_ID = "/getAllByUserId";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_USER_ID_AND_STAY_ID = "/getAllByUserIdAndHotelId";

	}

	interface FoodOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface CartConstants {
		String BASE = APP_BASE + "/cart";
		String ADD_OR_REMOVE = "/addOrRemove";
		String ADD_OR_REMOVE_MULTIPLE = "/addOrRemoveMultiple";
		String ADD_OR_REMOVE_FOOD_ITEM = ADD_OR_REMOVE + "/foodItem";
		String SAMPLE_FOOD_ITEM = "/sampleFoodItemRequest";
		String GET = "/get";
		String ADD_OR_REMOVE_MULTIPLE_FOOD_ITEM = ADD_OR_REMOVE_MULTIPLE + "/foodItem";
		String SAMPLE_FOOD_ITEM_LIST = "/sampleFoodItemListRequest";
		String PLACE_ORDER = "/placeOrder";
		String ADD_OR_REMOVE_MULTIPLE_LAUNDRY_ITEM = ADD_OR_REMOVE_MULTIPLE + "/laundryItem";
		String ADD_OR_REMOVE_MULTIPLE_HK_ITEM = ADD_OR_REMOVE_MULTIPLE + "/hkItem";
	}

	interface CartParameters {
		String USER_ID = "userId";
		String STAYS_ID = "staysId";
		String HOTEL_ID = "hotelId";
		String CART_TYPE = "cartType";
	}

	interface InternetServiceConstants {
		String BASE = APP_BASE + "/internetService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface InternetServiceParameters {
		String ID = "id";
	}

	interface LaundryOrderConstants {
		String BASE = APP_BASE + "/laundryOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL_BY_USER_ID = "/getAllByUserId";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_USER_ID_AND_STAY_ID = "/getAllByUserIdAndHotelId";

	}

	interface LaundryOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface HousekeepingOrderConstants {
		String BASE = APP_BASE + "/hkOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL_BY_USER_ID = "/getAllByUserId";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_USER_ID_AND_STAY_ID = "/getAllByUserIdAndHotelId";
	}

	interface HousekeepingOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface StaysFeedbackConstants {
		String BASE = APP_BASE + "/staysFeedback";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_BY_BASIC_IDS = "/getByUserHotelStays";
		String GET_ALL_BY_USER_ID = "/getAllByUserId";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_USER_ID_AND_STAY_ID = "/getAllByUserIdAndHotelId";
		String UPDATE_NEW = "/updateNew";
	}

	interface StaysFeedbackParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAYS_ID = "staysId";
		String HOTEL_ID = "hotelId";
	}

	interface MaintenanceRequestConstants {
		String BASE = APP_BASE + "/maintenanceRequest";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String DELETE = "/delete";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";

	}

	interface MaintenanceRequestParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface CommuteOrderConstants {
		String BASE = APP_BASE + "/commuteOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String DELETE = "/delete";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";

	}

	interface CommuteOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface InternetOrderConstants {
		String BASE = APP_BASE + "/internetOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String DELETE = "/delete";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";

	}

	interface InternetOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface LateCheckoutRequestConstants {
		String BASE = APP_BASE + "/lateCheckoutRequest";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String DELETE = "/delete";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String MODIFY = "/modify";
		String RETREIVE = "/retreive";

	}

	interface LateCheckoutRequestParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface ReservationOrderConstants {
		String BASE = APP_BASE + "/reservationOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String DELETE = "/delete";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";

	}

	interface ReservationOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface BellBoyOrderConstants {
		String BASE = APP_BASE + "/bellBoyOrder";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String DELETE = "/delete";

	}

	interface BellBoyOrderParameters {
		String ID = "id";
		String USER_ID = "userId";
		String STAY_ID = "stayId";
		String HOTEL_ID = "hotelId";
	}

	interface ExpenseConstants {
		String BASE = APP_BASE + "/expenseDashboard";
		String GENERATE_CATEGORY = "/generateCategorized";
		String GENERATE_TIME_BASED = "/generateTimeBased";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_BY_USER_AND_STAY = "/getByUserAndStay";
	}

	interface RequestConstants {
		String BASE = APP_BASE + "/requestDashboard";
		String CREATE = "/create";
		String GET_DETAILS = "/getDetails";
		String CANCEL_ORDER = "/cancel";
		String EDIT_ORDER = "/edit";
		String GET_ALL_REQUESTS = "/getAllRequests";
	}

	interface RequestParameters {
		String uosId = "uosId";
	}

	interface CityGuideConstants {
		String BASE = APP_BASE + "/cityGuide";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface CityGuideParameters {
		String ID = "id";
	}

	interface ReservationServiceConstants {
		String BASE = APP_BASE + "/reservationService";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface ReservationServiceParameters {
		String ID = "id";
	}

	interface MaintenanceConstants {
		String BASE = APP_BASE + "/maintenance";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface MaintenanceParameters {
		String ID = "id";
	}

	interface CityGuideItemConstants {
		String BASE = APP_BASE + "/cgItem";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
		String GET_ALL_BY_HOTEL_ID = "/getAllByHotelId";
		String GET_ALL_BY_CG_ID = "/getAllByCGId";
		String FETCH_BY_TYPE = "/fetchByType";
		String DELETE = "/delete";
	}

	interface CityGuideItemParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String CG_ID = "cgId";
		String TYPE = "type";
	}

	interface CheckinConstants {
		String BASE = APP_BASE + "/checkin";
		String REQUEST_CHECKIN = "/initiate";
		String MODIFY_CHECKIN = "/modify";
		String CHANGE_STATUS = "/changeStatus";
		String RETREIVE_CHECKIN = "/retreive";
	}

	interface CheckoutConstants {
		String BASE = APP_BASE + "/checkout";
		String REQUEST_CHECKOUT = "/initiate";
		String MODIFY_CHECKOUT = "/modify";
		String CHANGE_STATUS = "/changeStatus";
		String RETREIVE_CHECKOUT = "/retreive";
	}

	interface CommonConstants {
		String BASE = APP_BASE + "/common";
		String GET_PREFERENCES = "/getAllPreferences";
		String GET_PROFILE_TITLES = "/getProfileTitles";
		String GET_IDENTITY_DOCS = "/getIdentityDocs";
		String GET_COMMON_DATA = "/getCommonData";
	}

	interface MasterGenereConstants {
		String BASE = APP_BASE + "/masterGenere";
		String ADD = "/add";
		String GET_ALL = "/getAll";
		String GET_BY_ID = "/getById";
		String UPDATE = "/update";
		String GET_BY_NAME_LOGO = "/getByNameLogo";
		String DELETE = "/delete";
	}

	interface MasterGenereParameters {
		String ID = "id";
	}

	interface MasterChannelConstants {
		String BASE = APP_BASE + "/masterChannel";
		String ADD = "/add";
		String ADD_ALL = "/addAll";
		String GET_BY_ID = "/getById";
		String GET_ALL = "/getAll";
		String UPDATE = "/update";
		String DELETE = "/delete";
		String GET_ALL_BY_GENERE = "/getAllByGenere";
	}

	interface MasterChannelParameters {
		String ID = "id";
		String GENERE_ID = "genereId";
	}

	interface EntertaimentServiceConstants {
		String BASE = APP_BASE + "/entertaimentService";
		String ADD = "/add";
		String GET_BY_ID = "/getById";
		String GET_ALL = "/getAll";
		String ADD_TYPE = "/addType";
		String REMOVE_TYPE = "/removeType";
		String DELETE = "/delete";
	}

	interface EntertaimentServiceParameters {
		String ID = "id";
	}

	interface MasterCityConstants {
		String BASE = APP_BASE + "/masterCity";
		String ADD = "/add";
		String ADD_ALL = "/addAll";
		String GET_ALL = "/getAll";
		String GET_BY_ID = "/getById";
		String DELETE = "/delete";
	}

	interface MasterCityParameters {
		String ID = "id";
	}

	interface MasterAmenityConstants {
		String BASE = APP_BASE + "/masterAmenity";
		String ADD = "/add";
		String ADD_ALL = "/addAll";
		String GET_ALL = "/getAll";
		String GET_BY_ID = "/getById";
		String DELETE = "/delete";
	}

	interface MasterAmenityParamaters {
		String ID = "id";
		String TYPE = "type";
	}

	interface ComplementaryServicesConstants {
		String BASE = APP_BASE + "/complementaryServices";
		String ADD = "/add";
		String GET_ALL = "/getAll";
		String GET_BY_ID = "/getById";
		String DELETE = "/delete";
		String UPDATE = "/update";
	}

	interface ComplementaryServicesParameters {
		String ID = "id";
	}

	interface CurrentStaysConstants {
		String BASE = APP_BASE + "/currentStays";
		String GET_ALL_SERVICES = "/getAllServices";
	}

	interface NotificationConstants {
		String BASE = APP_BASE + "/notification";
		String ADD = "/add";
		String GET_BY_ID = "/getById";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET_STAY_NOTIFICATIONS = "/getStayNotifications";
	}

	interface NotificationParameters {
		String ID = "id";
	}

	interface MessageConstants {
		String BASE = APP_BASE + "/message";
		String ADD = "/add";
		String GET_BY_ID = "/getById";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET_STAY_MESSAGES = "/getStayNotifications";
		String READ_MESSAGES = "/readMessages";
		String GET_CHAT_USERS = "/getChatUsers";
	}

	interface MessageParameters {
		String ID = "id";
		String USER_ID = "userId";
		String HOTEL_ID = "hotelId";
		String CREATION_TYPE = "creationType";
	}

	interface TimezoneConstants {
		String BASE = APP_BASE + "/timezone";
		String ADD = "/add";
		String GET_BY_ID = "/getById";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String KEY_ID = "id";
	}

	interface LoyaltyProgramsConstants {
		String BASE = APP_BASE + "/loyaltyPrograms";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
	}

	interface LoyaltyProgramsParameters {
		String ID = "id";
	}

	interface WingsConstants {
		String BASE = APP_BASE + "/wings";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_BY_HOTEL_ID = "getByHotelId";
	}

	interface WingsParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
	}

	interface FloorConstants {
		String BASE = APP_BASE + "/floors";
		String ADD = "/add";
		String SAMPLE = "/sample";
		String GET_ALL = "/getAll";
		String DELETE = "/delete";
		String GET = "/get";
		String UPDATE = "/update";
		String GET_BY_WING_ID = "getByWingId";
		String GET_BY_HOTEL_ID = "getByHotelId";
	}

	interface FloorParameters {
		String ID = "id";
		String WING_ID = "wingId";
		String HOTEL_ID = "hotelId";
	}

	interface ReservationCategoryConstants {
		String BASE = APP_BASE + "/reservationCategory";
		String ADD = "/add";
		String GET_BY_ID = "/get";
		String GET_ALL = "/getAll";
		String UPDATE = "/update";
		String DELETE = "/delete";
	}

	interface ReservationCategoryParameters {
		String ID = "id";
		String HOTEL_ID = "hotelId";
		String RESERVATION_TYPE = "reservationType";
	}

	interface PickListConstants {
		String BASE = APP_BASE + "/picklist";
		String ADD_MODIFY = "/addOrModify";
		String DELETE_BY_TYPE = "/deleteByType";
		String GET_ALL = "/getAll";
		String GET_BY_TYPE = "/getByType";
	}

	interface PickListParameters {
		String TYPE = "type";
	}

	interface ArrivalsAndDeparturesConstants {
		String BASE = APP_BASE + "/arrivalsAndDepartures";
		String GET_UNCOMPLETE_BOOKINGS = "getUnCompleteBookings";
		String ALLOCATE_ROOM = "/allocateRoom";
		String FREEZE_BOOKING = "/freezeBooking";
		String COMPLETE_CHECKIN = "/completeCheckin";
		String ROOMS_AND_STAYS = "/getRoomAndStays";
		String APPROVE_EARLY_CHECKIN = "/approve";
		String REJECT_EARLY_CHECKIN = "/reject";
	}

	interface ArrivalsAndDeparturesParameters {
		String HOTEL_ID = "hotelId";
		String BOOKING_DATE = "bookingDate";
		String FLOOR_ID = "floorId";
	}
	interface Request{
		String BASE = APP_BASE + "/request";
		String GET_ALL_REQUESTS="/getAllRequests";
		String GET_ALL_RESERVATIONS = "/getAllReservations";
		String GET_ALL_ROOM_SERVICES = "/getAllRoomServices";
	}
	
	interface BellBoyServiceConstants{
		String BASE = APP_BASE + "/bellBoyService";
		String ADD = "/add";
		String GET_BY_HOTEL_ID = "/getByHotelId";
		String DELETE = "/delete";
		String GET_ALL = "/getAll";
		String UPDATE = "/update";
	}
	
	interface PlaceConstants{
		String BASE = APP_BASE + "/place";
		String ADD = "/add";
		String DELETE = "/delete";
		String UPDATE = "/update";
		String GET_ALL = "/getAll";
	}
	
	interface AirportConstants{
		String BASE = APP_BASE + "/airport";
		String ADD = "/add";
		String UPDATE = "/update";
		String DELETE = "/delete";
		String GET_ALL = "/getAll";
		String GET_BY_ID = "/getById";
	}

}
