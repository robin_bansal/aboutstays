package com.aboutstays.entities;

import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.entities.BellBoyOrder.FIELDS;

public class BaseSessionEntity {
	public interface FIELDS{
		String USER_ID = "userId";
		String HOTEL_ID = "hotelId";
		String STAYS_ID = "staysId";
		String GS_ID = "gsId";
	}
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.STAYS_ID)
	private String staysId;
	@Field(value=FIELDS.GS_ID)
	private String gsId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getStaysId() {
		return staysId;
	}

	public void setStaysId(String stayId) {
		this.staysId = stayId;
	}

	public String getGsId() {
		return gsId;
	}

	public void setGsId(String gsId) {
		this.gsId = gsId;
	}
	
}
