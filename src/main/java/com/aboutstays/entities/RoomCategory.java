package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.EarlyCheckinInfo;
import com.aboutstays.pojos.LateCheckoutInfo;

@Document(collection=RoomCategory.FIELDS.COLLECTION)
public class RoomCategory {
	
	public interface FIELDS{
		String COLLECTION = "roomCategories";
		String NAME = "name";
		String IMAGE_URL = "imageUrl";
		String DESCRIPTION = "description";
		String CODE="code";
		String COUNT="count";
		
		String SIZE = "size";
		String SIZE_METRIC="sizeMetric";
		
		String ADULT_CAPACITY = "adultCapacity";
		String CHILD_CAPACITY = "childCapacity";

		String IN_ROOM_AMENITIES = "in_room_amenities";
		String PREFERENCE_BASED_AMENITIES = "preferenceBasedAmenities";
		String ADDITIONAL_IMAGE_URLS = "additionalImageUrls";

		String EARLY_CHECKIN_INFO = "earlyCheckinInfo";
		String LATE_CHECKOUT_INFO = "lateCheckoutInfo";
		String HOTEL_ID="hotelId";
	}

	@Id
	private String roomCategegoryId;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.CODE)
	private String code;
	@Field(value=FIELDS.COUNT)
	private Integer count;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.DESCRIPTION)
	private String description;
	
	@Field(value=FIELDS.SIZE)
	private String size;
	@Field(value=FIELDS.SIZE_METRIC)
	private String sizeMetric;
	
	@Field(value=FIELDS.ADULT_CAPACITY)
	private int adultCapacity;
	@Field(value=FIELDS.CHILD_CAPACITY)
	private int childCapacity;

	
	@Field(value=FIELDS.ADDITIONAL_IMAGE_URLS)
	private List<String> additionalImageUrls;
	@Field(value=FIELDS.IN_ROOM_AMENITIES)
	private List<String> inRoomAmenities;
	@Field(value=FIELDS.PREFERENCE_BASED_AMENITIES)
	private List<String> preferenceBasedAmenities;
	
	@Field(value=FIELDS.EARLY_CHECKIN_INFO)
	private EarlyCheckinInfo earlyCheckinInfo;
	@Field(value=FIELDS.LATE_CHECKOUT_INFO)
	private LateCheckoutInfo lateCheckoutInfo;
	
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	
	public RoomCategory(){}

	public String getRoomCategegoryId() {
		return roomCategegoryId;
	}

	public void setRoomCategegoryId(String roomCategegoryId) {
		this.roomCategegoryId = roomCategegoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSizeMetric() {
		return sizeMetric;
	}

	public void setSizeMetric(String sizeMetric) {
		this.sizeMetric = sizeMetric;
	}

	public int getAdultCapacity() {
		return adultCapacity;
	}

	public void setAdultCapacity(int adultCapacity) {
		this.adultCapacity = adultCapacity;
	}

	public int getChildCapacity() {
		return childCapacity;
	}

	public void setChildCapacity(int childCapacity) {
		this.childCapacity = childCapacity;
	}

	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}

	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}

	public List<String> getInRoomAmenities() {
		return inRoomAmenities;
	}

	public void setInRoomAmenities(List<String> inRoomAmenities) {
		this.inRoomAmenities = inRoomAmenities;
	}

	public List<String> getPreferenceBasedAmenities() {
		return preferenceBasedAmenities;
	}

	public void setPreferenceBasedAmenities(List<String> preferenceBasedAmenities) {
		this.preferenceBasedAmenities = preferenceBasedAmenities;
	}

	public EarlyCheckinInfo getEarlyCheckinInfo() {
		return earlyCheckinInfo;
	}

	public void setEarlyCheckinInfo(EarlyCheckinInfo earlyCheckinInfo) {
		this.earlyCheckinInfo = earlyCheckinInfo;
	}

	public LateCheckoutInfo getLateCheckoutInfo() {
		return lateCheckoutInfo;
	}

	public void setLateCheckoutInfo(LateCheckoutInfo lateCheckoutInfo) {
		this.lateCheckoutInfo = lateCheckoutInfo;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	}