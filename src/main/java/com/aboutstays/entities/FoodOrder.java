package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.FoodItemOrderDetails;


@Document(collection=FoodOrder.FIELDS.COLLECTION)
public class FoodOrder extends BaseSessionEntity{
	
	public interface FIELDS{
		String COLLECTION = "foodOrderRequests";
		String CREATED = "created";
		String UPDATED = "updated";
		String DELIVERY_TIME = "deliveryTime";
		String COMMENTS = "comments";
		String FOOD_ITEM_ORDERS = "foodItemOrders";
		String STATUS = "status";
	}
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DELIVERY_TIME)
	private Long deliveryTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.FOOD_ITEM_ORDERS)
	private List<FoodItemOrderDetails> foodItemOrders;
	@Field(value=FIELDS.STATUS)
	private Integer status;//Refer FoodOrderStatus
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<FoodItemOrderDetails> getFoodItemOrders() {
		return foodItemOrders;
	}
	public void setFoodItemOrders(List<FoodItemOrderDetails> foodItemOrders) {
		this.foodItemOrders = foodItemOrders;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	
	
	
}
