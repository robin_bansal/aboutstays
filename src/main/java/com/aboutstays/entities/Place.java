package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Place.FIELDS.COLLECTION)
public class Place {
	
	public interface FIELDS{
		String COLLECTION = "places";
		String PLACE_TYPE = "placeType";
		String PLACE_NAME = "placeName";
		String PARENT_PLACE_ID = "parentPlaceId";
		String ICON_URL = "iconUrl";
		String IMAGE_URL = "imageUrl";
		String CREATED = "created";
		String UPDATED = "updated";
	}

	@Id
	private String placeId;
	@Field(value=FIELDS.PLACE_TYPE)
	private Integer placeType;
	@Field(value=FIELDS.PLACE_NAME)
	private String placeName;
	@Field(value=FIELDS.PARENT_PLACE_ID)
	private String parentPlaceId;
	@Field(value=FIELDS.ICON_URL)
	private String iconUrl;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	public String getPlaceId() {
		return placeId;
	}
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
	public Integer getPlaceType() {
		return placeType;
	}
	public void setPlaceType(Integer placeType) {
		this.placeType = placeType;
	}
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	public String getParentPlaceId() {
		return parentPlaceId;
	}
	public void setParentPlaceId(String parentPlaceId) {
		this.parentPlaceId = parentPlaceId;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
}
