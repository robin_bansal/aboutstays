package com.aboutstays.entities;

public class LoyaltyLevel {
	private String name;
	private String code;
	private String colorCode;
	private String eligibility;
	private String benifits;

	public LoyaltyLevel() {
	}

	public LoyaltyLevel(String name, String code, String colorCode, String eligibility, String benifits) {
		this.name = name;
		this.code = code;
		this.colorCode = colorCode;
		this.eligibility = eligibility;
		this.benifits = benifits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public String getBenifits() {
		return benifits;
	}

	public void setBenifits(String benifits) {
		this.benifits = benifits;
	}

}
