package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=LoyaltyPrograms.FIELDS.COLLECTION)
public class LoyaltyPrograms {
	public interface FIELDS {
		String COLLECTION = "loyaltyPrograms";
		String _ID = "_id";
		String NAME = "name";
		String ICON_URL = "iconUrl";
		String LEVELS = "levels";
	}
	@Id
	private String id;

	@Field(value=FIELDS.NAME)
	private String name;

	@Field(value=FIELDS.ICON_URL)
	private String iconUrl;
	
	@Field(value=FIELDS.LEVELS)
	private List<LoyaltyLevel> levels;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public List<LoyaltyLevel> getLevels() {
		return levels;
	}

	public void setLevels(List<LoyaltyLevel> levels) {
		this.levels = levels;
	}

	
}
