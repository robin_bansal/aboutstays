package com.aboutstays.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Car.FIELDS.COLLECTION)
public class Car {

	public interface FIELDS{
		String COLLECTION = "cars";
		String BRAND = "brand";
		String _ID = "_id";
		String TYPE = "type";
		String NUMBER_PLATE = "numberPlate";
		String CREATED = "created";
		String UPDATED = "updated";
		}
	@Id
	private String id;
	@Field(value=FIELDS.BRAND)
	private String brand;
	@Field(value=FIELDS.TYPE)
	private Integer type;
	@Field(value=FIELDS.NUMBER_PLATE)
	private String numberPlate;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	
	public Car(){
		this.created = new Date().getTime();
		this.updated = this.created;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getNumberPlate() {
		return numberPlate;
	}
	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	
	
	
}
