package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=GeneralService.FIELDS.COLLECTION)
public class GeneralService {

	public interface FIELDS{
		String COLLECTION = "generalServices";
		String SERVICE_TYPE = "serviceType";
		String UNACTIVATED_IMAGE_URL = "unactivatedImageUrl";
		String ACTIVATED_IMAGE_URL = "activatedImageUrl";
		String STATUS = "status";
		String _ID = "id";
		String TICKABLE = "tickable";
		String NUMERABLE = "numerable";
		String GENERAL_INFO = "generalInfo";
		String EXPENSE_ID = "expenseId";
		String UI_ORDER = "uiOrder";
		String SUPPORTS_PAST = "supportsPast";
		String SUPPORTS_FUTURE = "supportsFuture";
		String SUPPORTS_NON_PARTNER_HOTEL = "supportsNonPartneredHotel";
		
	}
	@Id
	private String id;
	@Field(value=FIELDS.SERVICE_TYPE)
	private Integer serviceType;
	@Field(value=FIELDS.UNACTIVATED_IMAGE_URL)
	private String unactivatedImageUrl;
	@Field(value=FIELDS.ACTIVATED_IMAGE_URL)
	private String activatedImageUrl;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.NUMERABLE)
	private boolean numerable;
	@Field(value=FIELDS.TICKABLE)
	private boolean tickable;
	@Field(value=FIELDS.GENERAL_INFO)
	private Boolean generalInfo;
	@Field(value=FIELDS.EXPENSE_ID)
	private String expenseId;
	@Field(value=FIELDS.UI_ORDER)
	private int uiOrder;
	@Field(value=FIELDS.SUPPORTS_PAST)
	private boolean supportsPast;
	@Field(value=FIELDS.SUPPORTS_FUTURE)
	private boolean supportsFuture;
	@Field(value=FIELDS.SUPPORTS_NON_PARTNER_HOTEL)
	private boolean supportsNonPartneredHotel;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getServiceType() {
		return serviceType;
	}
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}
	public String getUnactivatedImageUrl() {
		return unactivatedImageUrl;
	}
	public void setUnactivatedImageUrl(String unactivatedImageUrl) {
		this.unactivatedImageUrl = unactivatedImageUrl;
	}
	public String getActivatedImageUrl() {
		return activatedImageUrl;
	}
	public void setActivatedImageUrl(String activatedImageUrl) {
		this.activatedImageUrl = activatedImageUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public boolean isNumerable() {
		return numerable;
	}

	public void setNumerable(boolean numerable) {
		this.numerable = numerable;
	}

	public boolean isTickable() {
		return tickable;
	}

	public void setTickable(boolean tickable) {
		this.tickable = tickable;
	}
	public Boolean getGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(Boolean generalInfo) {
		this.generalInfo = generalInfo;
	}
	public String getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}
	public int getUiOrder() {
		return uiOrder;
	}
	public void setUiOrder(int uiOrder) {
		this.uiOrder = uiOrder;
	}
	public boolean isSupportsPast() {
		return supportsPast;
	}
	public void setSupportsPast(boolean supportsPast) {
		this.supportsPast = supportsPast;
	}
	public boolean isSupportsFuture() {
		return supportsFuture;
	}
	public void setSupportsFuture(boolean supportsFuture) {
		this.supportsFuture = supportsFuture;
	}
	public boolean isSupportsNonPartneredHotel() {
		return supportsNonPartneredHotel;
	}
	public void setSupportsNonPartneredHotel(boolean supportsNonPartneredHotel) {
		this.supportsNonPartneredHotel = supportsNonPartneredHotel;
	}
	
	
	
}
