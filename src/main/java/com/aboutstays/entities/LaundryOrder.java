package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.LaundryItemOrderDetails;

@Document(collection=LaundryOrder.FIELDS.COLLECTION)
public class LaundryOrder extends BaseSessionEntity{
	
	public interface FIELDS{
		String COLLECTION = "laundryOrders";
		String CREATED = "created";
		String UPDATED = "updated";
		String DELIVERY_TIME = "deliveryTime";
		String COMMENTS = "comments";
		String LAUNDRY_ITEM_ORDERS = "laundryItemOrders";
		String PREFERENCES = "preferences";	
		String STATUS = "status";
	}

	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DELIVERY_TIME)
	private Long deliveryTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.LAUNDRY_ITEM_ORDERS)
	private List<LaundryItemOrderDetails> laundryItemOrders;
	@Field(value=FIELDS.PREFERENCES)
	private int preferences;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<LaundryItemOrderDetails> getLaundryItemOrders() {
		return laundryItemOrders;
	}
	public void setLaundryItemOrders(List<LaundryItemOrderDetails> laundryItemOrders) {
		this.laundryItemOrders = laundryItemOrders;
	}
	public int getPreferences() {
		return preferences;
	}
	public void setPreferences(int preferences) {
		this.preferences = preferences;
	}	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
