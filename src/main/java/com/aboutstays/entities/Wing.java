package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Wing.FIELDS.COLLECTION)
public class Wing {
	public interface FIELDS {
		String COLLECTION = "wings";
		String _ID = "_id";
		String NAME = "name";
		String CODE="code";
		String HOTEL_ID="hotelId";
	}

	@Id
	private String id;
	
	@Field(value = FIELDS.NAME)
	private String name;

	@Field(value = FIELDS.CODE)
	private String code;

	@Field(value = FIELDS.HOTEL_ID)
	private String hotelId;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	
}
