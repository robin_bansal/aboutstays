package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=MasterAmenity.FIELDS.COLLECTION)
public class MasterAmenity {

	public interface FIELDS{
		String COLLECTION = "masterAmenities";
		String NAME = "name";
		String ICON_URL = "iconUrl";
		String TYPE = "type";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.ICON_URL)
	private String iconUrl;
	@Field(value=FIELDS.TYPE)
	private Integer type;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
}
