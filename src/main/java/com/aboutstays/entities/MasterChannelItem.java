package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=MasterChannelItem.FIELDS.COLLECTION)
public class MasterChannelItem {

	public interface FIELDS{
		String COLLECTION = "masterChannels";
		String NAME = "name";
		String LOGO = "logo";
		String IMAGE_URL = "imageUrl";
		String GENERE_ID = "genereId";
		String CREATED = "created";
		String UPDATED = "updated";
		String DESCRIPTION = "description";
	}
	@Id
	private String id;
	@Field(FIELDS.NAME)
	private String name;
	@Field(FIELDS.LOGO)
	private String logo;
	@Field(FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(FIELDS.GENERE_ID)
	private String genereId;
	@Field(FIELDS.DESCRIPTION)
	private String description;
	@Field(FIELDS.CREATED)
	private Long created;
	@Field(FIELDS.UPDATED)
	private Long updated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getGenereId() {
		return genereId;
	}
	public void setGenereId(String genereId) {
		this.genereId = genereId;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
