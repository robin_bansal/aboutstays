package com.aboutstays.entities;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.Address;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.PreferencesType;

@Document(collection=User.FIELDS.COLLECTION)
public class User {
	
	public interface FIELDS{
		String COLLECTION = "users";
		String _ID = "_id";
		String EMAIL_ID = "emailId";
		String FIRST_NAME = "firstName";
		String LAST_NAME = "lastName";
		String MOBILE_NUMBER = "mobileNumber";
		String PASSWORD = "password";
		String REFERRAL_CODE = "referralCode";
		String IMAGE_URL = "imageUrl";
		String OTP_VALIDATED = "otpValidated";
		String GENDER = "gender";
		String SIGNUP_TYPE = "signupType";
		String DATE_OF_BIRTH = "dateOfBirth";
		String MARITAL_STATUS = "maritalStatus";
		String ANNIVERSARY = "anniversary";
		String HOME_ADDRESS = "homeAddress";
		String OFFICE_ADDRESS = "officeAddress";
		String STAY_PREFERENCES = "stayPreferences";
		String IDENTITY_DOCS = "identityDocs";
		String CREATED = "created";
		String UPDATED = "updated";
		String TITLE = "title";
	}
	
	@Id
	private String userId;
	@Field(value=FIELDS.EMAIL_ID)
	@Indexed(unique=true)
	private String emailId;
	@Field(value=FIELDS.FIRST_NAME)
	private String firstName;
	@Field(value=FIELDS.LAST_NAME)
	private String lastName;
	@Field(value=FIELDS.MOBILE_NUMBER)
	@Indexed(unique=true)
	private String mobileNmber;
	@Field(value=FIELDS.PASSWORD)
	private String password;
	@Field(value=FIELDS.REFERRAL_CODE)
	private String referralCode;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.OTP_VALIDATED)
	private boolean otpValidated;
	@Field(value=FIELDS.GENDER)
	private String gender;
	@Field(value=FIELDS.SIGNUP_TYPE)
	private int signupType;
	@Field(value=FIELDS.DATE_OF_BIRTH)
	private Date dateOfBirth;
	@Field(value=FIELDS.MARITAL_STATUS)
	private int maritalStatus;
	@Field(value=FIELDS.ANNIVERSARY)
	private Date anniversary;
	@Field(value=FIELDS.HOME_ADDRESS)
	private Address homeAddress;
	@Field(value=FIELDS.OFFICE_ADDRESS)
	private Address officeAddress;
	@Field(value=FIELDS.STAY_PREFERENCES)
	private List<PreferencesType> listStayPreferences;
	@Field(value=FIELDS.IDENTITY_DOCS)
	private List<IdentityDoc> listIdentityDocs;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.TITLE)
	private String title;
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public boolean isOtpValidated() {
		return otpValidated;
	}
	public void setOtpValidated(boolean otpValidated) {
		this.otpValidated = otpValidated;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobileNmber() {
		return mobileNmber;
	}
	public void setMobileNmber(String mobileNmber) {
		this.mobileNmber = mobileNmber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public int getSignupType() {
		return signupType;
	}
	public void setSignupType(int signupType) {
		this.signupType = signupType;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public int getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(int maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public Date getAnniversary() {
		return anniversary;
	}
	public void setAnniversary(Date anniversary) {
		this.anniversary = anniversary;
	}
	public Address getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}
	public Address getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(Address officeAddress) {
		this.officeAddress = officeAddress;
	}
	public List<PreferencesType> getListStayPreferences() {
		return listStayPreferences;
	}
	public void setListStayPreferences(List<PreferencesType> listStayPreferences) {
		this.listStayPreferences = listStayPreferences;
	}
	public List<IdentityDoc> getListIdentityDocs() {
		return listIdentityDocs;
	}
	public void setListIdentityDocs(List<IdentityDoc> listIdentityDocs) {
		this.listIdentityDocs = listIdentityDocs;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
