package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Notification.FIELDS.COLLECTION)
public class Notification {
	
	public interface FIELDS{
		String COLLECTION = "notifications";
		String USER_ID = "userId";
		String HOTEL_ID = "hotelId";
		String STAYS_ID = "staysId";
		String CREATED = "created";
		String UPDATED = "updated";
		String TITLE = "title";
		String DESCRIPTION = "description";
		String ICON_URL = "iconUrl";
		String TYPE = "type";
		String MAX_UI_LINES = "maxUiLines";
	}

	@Id
	private String id;
	@Field(value=FIELDS.TITLE)
	private String title;
	@Field(value=FIELDS.DESCRIPTION)
	private String description;
	@Field(value=FIELDS.ICON_URL)
	private String iconUrl;
	@Field(value=FIELDS.TYPE)
	private String type;//Tab name
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.STAYS_ID)
	private String staysId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.MAX_UI_LINES)
	private int maxUiLines;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public int getMaxUiLines() {
		return maxUiLines;
	}
	public void setMaxUiLines(int maxUiLines) {
		this.maxUiLines = maxUiLines;
	}
}
