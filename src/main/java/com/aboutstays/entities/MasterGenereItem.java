package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=MasterGenereItem.FIELDS.COLLECTION)
public class MasterGenereItem {

	public interface FIELDS{
		String COLLECTION = "masterGeneres";
		String NAME = "name";
		String LOGO = "logo";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	@Id
	private String id;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.LOGO)
	private String logo;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	
	
}
