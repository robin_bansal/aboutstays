package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.Address;

@Document(collection=CheckoutEntity.FIELDS.COLLECTION)
public class CheckoutEntity {
	
	public interface FIELDS{
		String COLLECTION = "checkOutRequests";
		String USER_ID = "userId";
		String STAYS_ID = "staysId";
		String HOTEL_ID = "hotelId";
		String OFFICIAL_TRIP = "officialTrip";
		String CREATED = "created";
		String UPDATED = "updated";
		String ROOM_NO = "roomNo";
		String TOTAL_AMOUNT = "totalAmount";
		String GRAND_TOTAL = "grandTotal";
		String PAYMENT_METHOD = "paymentMethod";
		String STATUS = "status";
		String FIRST_NAME = "firstName";
		String LAST_NAME = "lastName";
		String MOBILE_NO = "mobileNo";
		String EMAIL_ID = "emailId";
		String ADDRESS = "address";
				
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.STAYS_ID)
	private String staysId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.OFFICIAL_TRIP)
	private boolean officialTrip;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.ROOM_NO)
	private String roomNo;
	@Field(value=FIELDS.TOTAL_AMOUNT)
	private double totalAmount;
	@Field(value=FIELDS.GRAND_TOTAL)
	private double grandTotal;
	@Field(value=FIELDS.PAYMENT_METHOD)
	private int paymentMethod;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.MOBILE_NO)
	private String mobileNo;
	@Field(value=FIELDS.FIRST_NAME)
	private String firstName;
	@Field(value=FIELDS.LAST_NAME)
	private String lastName;
	@Field(value=FIELDS.EMAIL_ID)
	private String emailId;
	@Field(value=FIELDS.ADDRESS)
	private Address address;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public boolean isOfficialTrip() {
		return officialTrip;
	}
	public void setOfficialTrip(boolean officialTrip) {
		this.officialTrip = officialTrip;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public int getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	

}
