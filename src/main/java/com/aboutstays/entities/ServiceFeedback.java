package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=ServiceFeedback.FIELDS.COLLECTION)
public class ServiceFeedback {
	
	public interface FIELDS {
		String COLLECTION = "serviceFeedback";
		String UOS_ID = "uosId";
		String COMMENTS = "comments";
		String TIME_TAKEN = "timeTaken";
		String QUALITY = "quality";
		String UPDATED = "updated";
		String CREATED = "created";
	}

	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.UOS_ID)
	private String uosId;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.TIME_TAKEN)
	private int timeTaken;
	@Field(value=FIELDS.QUALITY)
	private int quality;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public String getUosId() {
		return uosId;
	}
	public void setUosId(String uosId) {
		this.uosId = uosId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getTimeTaken() {
		return timeTaken;
	}
	public void setTimeTaken(int timeTaken) {
		this.timeTaken = timeTaken;
	}
	public int getQuality() {
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}
	
	
	

}
