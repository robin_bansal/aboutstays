package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import com.aboutstays.pojos.InternetPack;

@Document(collection=InternetService.FIELDS.COLLECTION)
public class InternetService {
	
	public interface FIELDS{
		String COLLECTION = "internetService";
		String _ID = "_id";
		String CREATED = "created";
		String UPDATED = "updated";
		String LIST_INTERNET_PACKS = "listPacks";
		String SLA_APPLICABLE = "slaApplicable";
		String SLA_TIME = "slaTime";
		String START_TIME = "startTime";
		String END_TIME = "endTime";
	}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.LIST_INTERNET_PACKS)
	private List<InternetPack> listPacks;
	@Field(value=FIELDS.SLA_APPLICABLE)
	private boolean slaApplicable;
	@Field(value=FIELDS.SLA_TIME)
	private int slaTime;
	@Field(value=FIELDS.START_TIME)
	private String startTime;
	@Field(value=FIELDS.END_TIME)
	private String endTime;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public List<InternetPack> getListPacks() {
		return listPacks;
	}
	public void setListPacks(List<InternetPack> listPacks) {
		this.listPacks = listPacks;
	}
	public boolean isSlaApplicable() {
		return slaApplicable;
	}
	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}
	public int getSlaTime() {
		return slaTime;
	}
	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}	

}
