package com.aboutstays.entities;


import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.SupportedAirports;

@Document(collection=AirportServices.FIELDS.COLLECTION)
public class AirportServices {

	public interface FIELDS{
		String COLLECTION = "airportServices";
		String _ID = "_id";
		String SUPPORTED_AIRPORTS = "supportedAirports";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.SUPPORTED_AIRPORTS)
	private List<SupportedAirports> supportedAirports;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}

	public List<SupportedAirports> getSupportedAirports() {
		return supportedAirports;
	}

	public void setSupportedAirports(List<SupportedAirports> supportedAirports) {
		this.supportedAirports = supportedAirports;
	}
	

	
	
	
}
