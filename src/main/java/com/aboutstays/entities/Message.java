package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Message.FIELDS.COLLECTION)
public class Message {
	
	public interface FIELDS{
		String COLLECTION = "messages";
		String USER_ID = "userId";
		String HOTEL_ID = "hotelId";
		String STAYS_ID = "staysId";
		String CREATED = "created";
		String UPDATED = "updated";
		String MESSAGE_TEXT = "messageText";
		String CREATION_TYPE = "creationType";
		String STATUS="status";
	}

	@Id
	private String id;
	@Field(value=FIELDS.MESSAGE_TEXT)
	private String messageText;
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.STAYS_ID)
	private String staysId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.CREATION_TYPE)
	private int creationType;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	private Integer status;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public int getCreationType() {
		return creationType;
	}
	public void setCreationType(int creationType) {
		this.creationType = creationType;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
