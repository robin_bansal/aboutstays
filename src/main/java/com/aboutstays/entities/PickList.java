package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=PickList.FIELDS.COLLECTION)
public class PickList {
	
	public interface FIELDS{
		String COLLECTION = "picklists";
		String TYPE = "type";
		String VALUES = "values";
	}

	@Id
	private String id;
	@Field(value=FIELDS.TYPE)
	private int type;
	@Field(value=FIELDS.VALUES)
	private List<String> values;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
}
