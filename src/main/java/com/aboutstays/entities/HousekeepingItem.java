package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=HousekeepingItem.FIELDS.COLLECTION)
public class HousekeepingItem {
	
	public interface FIELDS{
		String COLLECTION = "housekeepingItems";
		String HOTEL_ID = "hotelId";
		String HOUSEKEEPING_SERVICES_ID = "housekeepingServicesId";
		String NAME = "name";
		String COMPLEMENTARY = "complementory";
		String MRP = "mrp";
		String PRICE = "price";
		String ITEM_TYPE = "housekeepingItemType";
		String CREATED = "created";
		String UPDATED = "updated";
		String CATEGORY = "category";
		String SHOW_ON_APP = "showOnApp";
	}

	@Id
	private String id;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.HOUSEKEEPING_SERVICES_ID)
	private String housekeepingServicesId;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.COMPLEMENTARY)
	private boolean complementory;
	@Field(value=FIELDS.MRP)
	private Double mrp;
	@Field(value=FIELDS.PRICE)
	private Double price;
	@Field(value=FIELDS.ITEM_TYPE)
	private int housekeepingItemType;//Refer HousekeepingItemType
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.CATEGORY)
	private String category;
	@Field(value=FIELDS.SHOW_ON_APP)
	private boolean showOnApp;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getHousekeepingServicesId() {
		return housekeepingServicesId;
	}
	public void setHousekeepingServicesId(String housekeepingServicesId) {
		this.housekeepingServicesId = housekeepingServicesId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isComplementory() {
		return complementory;
	}
	public void setComplementory(boolean complementory) {
		this.complementory = complementory;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getHousekeepingItemType() {
		return housekeepingItemType;
	}
	public void setHousekeepingItemType(int housekeepingItemType) {
		this.housekeepingItemType = housekeepingItemType;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isShowOnApp() {
		return showOnApp;
	}
	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}
}
