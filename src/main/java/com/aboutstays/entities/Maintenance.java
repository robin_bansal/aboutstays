package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.MaintenanceItemByType;

@Document(collection=Maintenance.FIELDS.COLLECTION)
public class Maintenance {
	
	public interface FIELDS{
		String COLLECTION = "maintenance";
		String CREATED = "created";
		String UPDATED = "updated";
		String LIST_MAINTENANCE_ITEM_TYPE = "listItemType";
		}
	
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.LIST_MAINTENANCE_ITEM_TYPE)
	private List<MaintenanceItemByType> listItemType;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public List<MaintenanceItemByType> getListItemType() {
		return listItemType;
	}
	public void setListItemType(List<MaintenanceItemByType> listItemType) {
		this.listItemType = listItemType;
	}

	

}
