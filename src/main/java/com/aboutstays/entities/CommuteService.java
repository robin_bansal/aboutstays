package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import com.aboutstays.pojos.CommutePackage;

@Document(collection=CommuteService.FIELDS.COLLECTION)
public class CommuteService {

	public interface FIELDS{
		String COLLECTION = "commuteServices";
		String LIST_COMMUTE_PACKAGE = "listCommutePackage";
		String CREATED = "created";
		String UPDATED = "updated";
		String START_TIME = "startTime";
		String END_TIME = "endTime";
	}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.LIST_COMMUTE_PACKAGE)
	private List<CommutePackage> listCommutePackage;
	@Field(value=FIELDS.START_TIME)
	private String startTime;
	@Field(value=FIELDS.END_TIME)
	private String endTime;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public List<CommutePackage> getListCommutePackage() {
		return listCommutePackage;
	}
	public void setListCommutePackage(List<CommutePackage> listCommutePackage) {
		this.listCommutePackage = listCommutePackage;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
