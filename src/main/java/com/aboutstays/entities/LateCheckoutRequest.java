package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=LateCheckoutRequest.FIELDS.COLLECTION)
public class LateCheckoutRequest extends BaseSessionEntity{
	
	public interface FIELDS{
		String COLLECTION = "lateCheckoutRequest";
		String CREATED = "created";
		String UPDATED = "updated";
		String DELIVERY_TIME = "deliveryTime";
		String COMMENTS = "comments";
		String STATUS = "status";
		String CHECKOUT_TIME = "checkoutTime";
		String CHECKOUT_DATE = "checkoutDate";
		String PRICE = "price";
				
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DELIVERY_TIME)
	private Long deliveryTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.CHECKOUT_TIME)
	private Long checkoutTime;
	@Field(value=FIELDS.CHECKOUT_DATE)
	private Long checkoutDate;
	@Field(value=FIELDS.PRICE)
	private Double price;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCheckoutTime() {
		return checkoutTime;
	}
	public void setCheckoutTime(Long checkoutTime) {
		this.checkoutTime = checkoutTime;
	}
	public Long getCheckoutDate() {
		return checkoutDate;
	}
	public void setCheckoutDate(Long checkoutDate) {
		this.checkoutDate = checkoutDate;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	

}
