package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.TabWiseComplementaryService;

@Document(collection=ComplementaryServicesEntity.FIELDS.COLLECTION)
public class ComplementaryServicesEntity {
	
	public interface FIELDS{
		String COLLECTION = "complementaryServices";
		String _ID = Fields.UNDERSCORE_ID;
		String CREATED = "created";
		String UPDATED = "updated";
		String TAB_WISE_SERVICES = "tabWiseServices";
	}
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.TAB_WISE_SERVICES)
	private List<TabWiseComplementaryService> tabWiseServiceList;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public List<TabWiseComplementaryService> getTabWiseServiceList() {
		return tabWiseServiceList;
	}
	public void setTabWiseServiceList(List<TabWiseComplementaryService> tabWiseServiceList) {
		this.tabWiseServiceList = tabWiseServiceList;
	}
	
}
