package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.ReservationItemTypeInfo;

@Document(collection=ReservationServiceEntity.FIELDS.COLLECTION)
public class ReservationServiceEntity {

	public interface FIELDS{
		String COLLECTION = "reservationServices";
		String LIST_TYPE_INFO = "typeInfoList";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.LIST_TYPE_INFO)
	private List<ReservationItemTypeInfo> typeInfoList;
	
	public String getHotelId() {
		return hotelId;
	}
	
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}

	public List<ReservationItemTypeInfo> getTypeInfoList() {
		return typeInfoList;
	}
	
	public void setTypeInfoList(List<ReservationItemTypeInfo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}
}
