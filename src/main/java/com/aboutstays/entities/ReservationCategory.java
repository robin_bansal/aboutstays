package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.Direction;

@Document(collection=ReservationCategory.FIELDS.COLLECTION)
public class ReservationCategory {
	
	public interface FIELDS{
		String COLLECTION = "reservationCategories";
		String NAME = "name";
		String START_TIME = "startTime";
		String END_TIME = "endTime";
		String HOTEL_ID = "hotelId";
		String WING_ID = "wingId";
		String FLOOR_ID = "floorId";
		String ENTRY_CRITERIA = "entryCriteria";
		String DESCRIPTION = "description";
		String MOST_POPULAR = "mostPopular";
		String HAS_DEALS = "hasDeals";
		String PROVIDE_RESERVATIONS = "provideReservations";
		String IMAGE_URL = "imageUrl";
		String ADDITIONAL_IMAGE_URLS = "additionalImageUrls";
		String RESERVATION_TYPE = "reservationType";
		String CREATED = "created";
		String UPDATED = "updated";
		String PRIMARY_CUISINE = "primaryCuisine";
		String GUEST_SENSE_CATEGORY = "guestSenseCategory";
		String AMENITY_TYPE = "amenityType";
		String DIRECTION_LIST = "directionList";
	}

	@Id
	private String id;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.START_TIME)
	private String startTime;
	@Field(value=FIELDS.END_TIME)
	private String endTime;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.WING_ID)
	private String wingId;
	@Field(value=FIELDS.FLOOR_ID)
	private String floorId;
	@Field(value=FIELDS.ENTRY_CRITERIA)
	private String entryCriteria;
	@Field(value=FIELDS.DESCRIPTION)
	private String description;
	@Field(value=FIELDS.MOST_POPULAR)
	private List<String> mostPopular;
	@Field(value=FIELDS.HAS_DEALS)
	private boolean hasDeals;
	@Field(value=FIELDS.PROVIDE_RESERVATIONS)
	private boolean provideReservations;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.ADDITIONAL_IMAGE_URLS)
	private List<String> additionalImageUrls;
	@Field(value=FIELDS.RESERVATION_TYPE)
	private int reservationType;
	@Field(value=FIELDS.PRIMARY_CUISINE)
	private String primaryCuisine;
	@Field(value=FIELDS.GUEST_SENSE_CATEGORY)
	private String guestSenseCategory;
	@Field(value=FIELDS.AMENITY_TYPE)
	private String amenityType;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DIRECTION_LIST)
	private List<Direction> directionList;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getWingId() {
		return wingId;
	}
	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	public String getFloorId() {
		return floorId;
	}
	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
	public String getEntryCriteria() {
		return entryCriteria;
	}
	public void setEntryCriteria(String entryCriteria) {
		this.entryCriteria = entryCriteria;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getMostPopular() {
		return mostPopular;
	}
	public void setMostPopular(List<String> mostPopular) {
		this.mostPopular = mostPopular;
	}
	public boolean isHasDeals() {
		return hasDeals;
	}
	public void setHasDeals(boolean hasDeals) {
		this.hasDeals = hasDeals;
	}
	public boolean isProvideReservations() {
		return provideReservations;
	}
	public void setProvideReservations(boolean provideReservations) {
		this.provideReservations = provideReservations;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}
	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}
	public int getReservationType() {
		return reservationType;
	}
	public void setReservationType(int reservationType) {
		this.reservationType = reservationType;
	}
	public String getPrimaryCuisine() {
		return primaryCuisine;
	}
	public void setPrimaryCuisine(String primaryCuisine) {
		this.primaryCuisine = primaryCuisine;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getGuestSenseCategory() {
		return guestSenseCategory;
	}
	public void setGuestSenseCategory(String guestSenseCategory) {
		this.guestSenseCategory = guestSenseCategory;
	}
	public String getAmenityType() {
		return amenityType;
	}
	public void setAmenityType(String amenityType) {
		this.amenityType = amenityType;
	}
	public List<Direction> getDirectionList() {
		return directionList;
	}
	public void setDirectionList(List<Direction> directionList) {
		this.directionList = directionList;
	}
}
