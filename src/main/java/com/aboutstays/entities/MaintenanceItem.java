package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=MaintenanceItem.FIELDS.COLLECTION)
public class MaintenanceItem {
	
	public interface FIELDS{
		String COLLECTION = "maintenanceItem";
		String HOTEL_ID = "hotelId";
		String MAINTENANCE_SERVICE_ID = "maintenanceServiceId";
		String NAME = "name";
		String ITEM_TYPE = "itemType";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.MAINTENANCE_SERVICE_ID)
	private String maintenanceServiceId;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.ITEM_TYPE)
	private int itemType;  // refer MaintenanceItemType
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getMaintenanceServiceId() {
		return maintenanceServiceId;
	}
	public void setMaintenanceServiceId(String maintenanceServiceId) {
		this.maintenanceServiceId = maintenanceServiceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getItemType() {
		return itemType;
	}
	public void setItemType(int itemType) {
		this.itemType = itemType;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	
	

}
