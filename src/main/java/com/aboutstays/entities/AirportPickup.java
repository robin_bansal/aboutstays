package com.aboutstays.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection=AirportPickup.FIELDS.COLLECTION)
public class AirportPickup extends BaseSessionEntity{

	public interface FIELDS{
		String COLLECTION = "airportPickups";
		String _ID = "_id";
		String CAR_ID = "carId";
		String DRIVER_ID = "driverId";
		String CREATED = "created";
		String UPDATED = "updated";
		String AIRPORT_NAME = "airportName";
		String AIRLINE_NAME = "airlineName";
		String FLIGH_NUMBER = "flighNumber";
		String CAR_TYPE = "carType";
		String PICKUP_DATE_AND_TIME = "pickupDateTime";
		String COMMENT = "comment";
		String PRICE = "price";
		String DROP = "drop";
		String STATUS = "status";
		String PICKUP_STATUS = "pickupStatus";
		}
	@Id
	private String id;

	@Field(value=FIELDS.CAR_ID)
	private String carId;
	@Field(value=FIELDS.DRIVER_ID)
	private String driverId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.AIRPORT_NAME)
	private String airportName;
	@Field(value=FIELDS.AIRLINE_NAME)
	private String airlineName;
	@Field(value=FIELDS.CAR_TYPE)
	private Integer carType;
	@Field(value=FIELDS.PICKUP_DATE_AND_TIME)
	private long pickupDateTime;
	@Field(value=FIELDS.COMMENT)
	private String comment;
	@Field(value=FIELDS.FLIGH_NUMBER)
	private String flightNumber;
	@Field(value=FIELDS.PRICE)
	private Double price;
	@Field(value=FIELDS.DROP)
	private Boolean drop;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.PICKUP_STATUS)
	private Integer pickupStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public AirportPickup(){
		this.created = new Date().getTime();
		this.updated = this.created;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public long getUpdated() {
		return updated;
	}

	public void setUpdated(long updated) {
		this.updated = updated;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public Integer getCarType() {
		return carType;
	}

	public void setCarType(Integer carType) {
		this.carType = carType;
	}

	public long getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(long pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Boolean getDrop() {
		return drop;
	}
	public void setDrop(Boolean drop) {
		this.drop = drop;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Integer getPickupStatus() {
		return pickupStatus;
	}


	public void setPickupStatus(Integer pickupStatus) {
		this.pickupStatus = pickupStatus;
	}


	
	
	
	
}
