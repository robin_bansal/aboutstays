package com.aboutstays.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.BookedBy;
import com.aboutstays.pojos.BookedFor;

@Document(collection=Booking.FIELDS.COLLECTION)
public class Booking {
	
	public interface FIELDS{
		String COLLECTION = "booking";
		String HOTEL_ID = "hotelId";
		String ROOM_ID = "roomId";
		String ROOM_CATEGORY_ID="roomCategoryId";
		String CHECK_IN_DATE = "checkInDate";
		String CHECK_OUT_DATE = "checkOutDate";
		String BOOKED_FOR = "bookedFor";
		String BOOKED_BY = "bookedBy";
		String BOOKING_STATUS="bookingStatus";
		String RESERVATION_NUMBER = "reservationNumber";
		String DEFAULT_STAYS_ID = "defaultStaysId";
		String _ID = "_id";
	}
	@Id
	private String bookingId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.ROOM_ID)
	private String roomId;
	@Field(value=FIELDS.ROOM_CATEGORY_ID)
	private String roomCategoryId;
	@Field(value=FIELDS.CHECK_IN_DATE)
	private Date checkInDate;
	@Field(value=FIELDS.CHECK_OUT_DATE)
	private Date checkOutDate;
	@Field(value=FIELDS.RESERVATION_NUMBER)
	private String reservationNumber;
	@Field(value=FIELDS.BOOKED_BY)
	private BookedBy bookedBy;
	@Field(value=FIELDS.BOOKING_STATUS)
	private Integer bookedStatus;
	@Field(value=FIELDS.BOOKED_FOR)
	private BookedFor bookedFor;
	@Field(value=FIELDS.DEFAULT_STAYS_ID)
	private String defaultStaysId;
	
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public String getReservationNumber() {
		return reservationNumber;
	}
	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}
	public BookedBy getBookedBy() {
		return bookedBy;
	}
	public void setBookedBy(BookedBy bookedBy) {
		this.bookedBy = bookedBy;
	}
	public BookedFor getBookedFor() {
		return bookedFor;
	}
	public void setBookedFor(BookedFor bookedFor) {
		this.bookedFor = bookedFor;
	}
	public String getRoomCategoryId() {
		return roomCategoryId;
	}
	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
	public Integer getBookedStatus() {
		return bookedStatus;
	}
	public void setBookedStatus(Integer bookedStatus) {
		this.bookedStatus = bookedStatus;
	}
	public String getDefaultStaysId() {
		return defaultStaysId;
	}
	public void setDefaultStaysId(String defaultStaysId) {
		this.defaultStaysId = defaultStaysId;
	}
}
