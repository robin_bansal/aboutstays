package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=LaundryItem.FIELDS.COLLECTION)
public class LaundryItem {
	
	public interface FIELDS{
		String COLLECTION = "laundryItem";
		String HOTEL_ID = "hotelId";
		String LAUNDRY_SERVICES_ID = "laundryServicesId";
		String NAME = "name";
		String MRP = "mrp";
		String PRICE = "price";
		String SAME_DAY_PRICE = "sameDayPrice";
		String ITEM_TYPE = "laundryItemType";
		String CREATED = "created";
		String UPDATED = "updated";
		String LAUNDRY_FOR = "laundryFor";
		String LAUNDRY_ITEM_TYPE = "laundryItemType";
		String SHOW_ON_APP = "showOnApp";
	}
	@Id
	private String id;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.LAUNDRY_SERVICES_ID)
	private String laundryServicesId;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.LAUNDRY_FOR)
	private int laundryFor;//Refer LaundryFor enum
	@Field(value=FIELDS.LAUNDRY_ITEM_TYPE)
	private int laundryItemType;//Refer LaundryItemType
	@Field(value=FIELDS.MRP)
	private Double mrp;
	@Field(value=FIELDS.PRICE)
	private Double price;
	@Field(value=FIELDS.SAME_DAY_PRICE)
	private Double sameDayPrice;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.SHOW_ON_APP)
	private boolean showOnApp;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getLaundryServicesId() {
		return laundryServicesId;
	}
	public void setLaundryServicesId(String laundryServicesId) {
		this.laundryServicesId = laundryServicesId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLaundryItemType() {
		return laundryItemType;
	}
	public void setLaundryItemType(int laundryItemType) {
		this.laundryItemType = laundryItemType;
	}
	public int getLaundryFor() {
		return laundryFor;
	}
	public void setLaundryFor(int laundryFor) {
		this.laundryFor = laundryFor;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Double getSameDayPrice() {
		return sameDayPrice;
	}
	public void setSameDayPrice(Double sameDayPrice) {
		this.sameDayPrice = sameDayPrice;
	}
	public boolean isShowOnApp() {
		return showOnApp;
	}
	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}

}
