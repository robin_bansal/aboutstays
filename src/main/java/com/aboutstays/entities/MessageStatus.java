package com.aboutstays.entities;

public enum MessageStatus {
	SENT(1), DELIVERED(2), READ(3);
	private int status;
	private MessageStatus(int status) {
		this.status = status;
	}
	public int getStatus() {
		return status;
	}
	public static MessageStatus findByStatus(Integer status){
		if(status==null){
			return null;
		}
		for(MessageStatus messageStatus:values()){
			if(messageStatus.getStatus()==status){
				return messageStatus;
			}
		}
		return null;
	}
}
