package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.CityGuideGeneralInfo;

@Document(collection=CityGuideItemEntity.FIELDS.COLLECTION)
public class CityGuideItemEntity {
	
	public interface FIELDS{
		String COLLECTION = "cityGuideItem";
		String HOTEL_ID = "hotelId";
		String CITY_GUIDE_ID = "cityGuideId";
		String CITY_GUIDE_ITEM_TYPE = "cityGuideItemType";
		String GENERAL_INFO = "generalInfo";
		String OPENING_TIME = "openingTime";
		String CLOSING_TIME = "closingTime";
		String DESTINATION_TYPE = "destinationType";
		String DISTANCE = "distance";
		String DESCRIPTION = "description";
		String LIST_ACTIVITIES = "listActivities";
		String TRAVEL_OPTIONS_LIST = "travelOptionsList";
		String CREATED = "created";
		String UPDATED = "updated";
		String RECOMMENDED = "recommended";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.CITY_GUIDE_ITEM_TYPE)
	private Integer cityGuideItemType;
	@Field(value=FIELDS.GENERAL_INFO)
	private CityGuideGeneralInfo generalInfo;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.CITY_GUIDE_ID)
	private String cityGuideId;
	@Field(value=FIELDS.OPENING_TIME)
	private String openingTime;
	@Field(value=FIELDS.CLOSING_TIME)
	private String closingTime;
	@Field(value=FIELDS.DESTINATION_TYPE)
	private String destinationType;
	@Field(value=FIELDS.DISTANCE)
	private Double distance;
	@Field(value=FIELDS.TRAVEL_OPTIONS_LIST)
	private List<String> travelOptionsList;
	@Field(value=FIELDS.DESCRIPTION)
	private String description;
	@Field(value=FIELDS.LIST_ACTIVITIES)
	private List<String> activityList;
	@Field(value=FIELDS.RECOMMENDED)
	private boolean recommended;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Integer getCityGuideItemType() {
		return cityGuideItemType;
	}
	public void setCityGuideItemType(Integer cityGuideItemType) {
		this.cityGuideItemType = cityGuideItemType;
	}
	public CityGuideGeneralInfo getGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(CityGuideGeneralInfo generalInfo) {
		this.generalInfo = generalInfo;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getCityGuideId() {
		return cityGuideId;
	}
	public void setCityGuideId(String cityGuideId) {
		this.cityGuideId = cityGuideId;
	}
	public String getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}
	public String getClosingTime() {
		return closingTime;
	}
	public void setClosingTime(String closingTime) {
		this.closingTime = closingTime;
	}
	public String getDestinationType() {
		return destinationType;
	}
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public List<String> getTravelOptionsList() {
		return travelOptionsList;
	}
	public void setTravelOptionsList(List<String> travelOptionsList) {
		this.travelOptionsList = travelOptionsList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<String> activityList) {
		this.activityList = activityList;
	}
	public boolean isRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	

}
