package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.GeneralNameImageInfo;


@Document(collection=HotelOtherInfo.FIELDS.COLLECTION)
public class HotelOtherInfo {
	
	public interface FIELDS {
		String COLLECTION = "hotelOtherInfo";
		String LIST_CARDS_ACCEPTED = "listCardAccepted";
		String LIST_AWARDS = "listAwards";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.LIST_CARDS_ACCEPTED)
	private List<GeneralNameImageInfo> listCardAccepted;
	@Field(value=FIELDS.LIST_AWARDS)
	private List<GeneralNameImageInfo> listAwards;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<GeneralNameImageInfo> getListCardAccepted() {
		return listCardAccepted;
	}
	public void setListCardAccepted(List<GeneralNameImageInfo> listCardAccepted) {
		this.listCardAccepted = listCardAccepted;
	}
	public List<GeneralNameImageInfo> getListAwards() {
		return listAwards;
	}
	public void setListAwards(List<GeneralNameImageInfo> listAwards) {
		this.listAwards = listAwards;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	
	
	

}
