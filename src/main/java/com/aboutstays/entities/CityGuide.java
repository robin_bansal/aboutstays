package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.CityGuideGeneralInfo;
import com.aboutstays.pojos.CityGuideItemByType;

@Document(collection=CityGuide.FIELDS.COLLECTION)
public class CityGuide {
	
	public interface FIELDS{
		String COLLECTION = "cityGuide";
		String _ID = "_id";
		String CREATED = "created";
		String UPDATED = "updated";
		String LIST_CITY_GUIDE_ITEMS = "listCityGuideItems";
		String GENERAL_INFO = "generalInfo";
		}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.LIST_CITY_GUIDE_ITEMS)
	private List<CityGuideItemByType> listCityGuideItems;
	@Field(value=FIELDS.GENERAL_INFO)
	private CityGuideGeneralInfo generalInfo;
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public List<CityGuideItemByType> getListCityGuideItems() {
		return listCityGuideItems;
	}
	public void setListCityGuideItems(List<CityGuideItemByType> listCityGuideItems) {
		this.listCityGuideItems = listCityGuideItems;
	}
	public CityGuideGeneralInfo getGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(CityGuideGeneralInfo generalInfo) {
		this.generalInfo = generalInfo;
	}
	
	

}
