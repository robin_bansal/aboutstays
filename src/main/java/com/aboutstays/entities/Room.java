package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
@Document(collection=Room.FIELDS.COLLECTION)
public class Room {
	
	public interface FIELDS{
		String COLLECTION = "rooms";
		String NAME = "name";
		String ROOM_NUMBER="roomNumber";
		String WING_ID = "wingId";
		String FLOOOR_ID = "floorId";
		String ROOM_CATEGORY_ID = "roomCategoryId";
		String HOTEL_ID="hotelId";
		String ADDITIONAL_AMENITY_IDS = "additionalAmenityIds";
		String PANEL_UI_ORDER = "panelUiOrder";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String id;
	
	@Field(value=FIELDS.NAME)
	private String name;
	
	@Field(value=FIELDS.ROOM_NUMBER)
	private String roomNumber;
	
	@Field(value=FIELDS.WING_ID)
	private String wingId;
	
	@Field(value=FIELDS.FLOOOR_ID)
	private String floorId;
	
	@Field(value=FIELDS.ROOM_CATEGORY_ID)
	private String roomCategoryId;
	
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	
	@Field(value=FIELDS.ADDITIONAL_AMENITY_IDS)
	private List<String> additionalAmenityIds;
	
	@Field(value=FIELDS.PANEL_UI_ORDER)
	private Integer panelUiOrder; 
	
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getRoomCategoryId() {
		return roomCategoryId;
	}

	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	
	public List<String> getAdditionalAmenityIds() {
		return additionalAmenityIds;
	}
	public void setAdditionalAmenityIds(List<String> additionalAmenityIds) {
		this.additionalAmenityIds = additionalAmenityIds;
	}
	public Integer getPanelUiOrder() {
		return panelUiOrder;
	}
	public void setPanelUiOrder(Integer panelUiOrder) {
		this.panelUiOrder = panelUiOrder;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	
}
