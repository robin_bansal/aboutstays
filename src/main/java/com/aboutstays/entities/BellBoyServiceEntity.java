package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class BellBoyServiceEntity {

	public interface FIELDS{
		String COLLECTION = "bellBoyServices";
		String SLA_APPLICABLE = "slaApplicable";
		String SLA_TIME = "slaTime";
		String START_TIME = "startTime";
		String END_TIME = "endTime";
		String CREATED = "created";
		String UPDATED = "updated";
		String SERVICE_ENABLED = "serviceEnabled";
	}
	@Id
	private String hotelId;
	@Field(value=BellBoyServiceEntity.FIELDS.SLA_APPLICABLE)
	private boolean slaApplicable;
	@Field(value=BellBoyServiceEntity.FIELDS.SLA_TIME)
	private int slaTime;
	@Field(value=BellBoyServiceEntity.FIELDS.SERVICE_ENABLED)
	private boolean serviceEnabled;
	@Field(value=BellBoyServiceEntity.FIELDS.START_TIME)
	private String startTime;
	@Field(value=BellBoyServiceEntity.FIELDS.END_TIME)
	private String endTime;
	@Field(value=BellBoyServiceEntity.FIELDS.CREATED)
	private Long created;
	@Field(value=BellBoyServiceEntity.FIELDS.UPDATED)
	private Long updated;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public boolean isSlaApplicable() {
		return slaApplicable;
	}
	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}
	public boolean isServiceEnabled() {
		return serviceEnabled;
	}
	public void setServiceEnabled(boolean serviceEnabled) {
		this.serviceEnabled = serviceEnabled;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getSlaTime() {
		return slaTime;
	}
	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
}
