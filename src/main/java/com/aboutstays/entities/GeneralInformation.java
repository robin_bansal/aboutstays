package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=GeneralInformation.FIELDS.COLLECTION)
public class GeneralInformation {
	
	public interface FIELDS{
		String COLLECTION = "generalInformation";
		String INFO_TYPE = "infoType";
		String UNACTIVATED_IMAGE_URL = "unactivatedImageUrl";
		String ACTIVATED_IMAGE_URL = "activatedImageUrl";
		String STATUS = "status";
		String _ID = "id";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	@Id
	private String id;
	@Field(value=FIELDS.INFO_TYPE)
	private Integer infoType;
	@Field(value=FIELDS.UNACTIVATED_IMAGE_URL)
	private String unactivatedImageUrl;
	@Field(value=FIELDS.ACTIVATED_IMAGE_URL)
	private String activatedImageUrl;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getInfoType() {
		return infoType;
	}
	public void setInfoType(Integer infoType) {
		this.infoType = infoType;
	}
	public String getUnactivatedImageUrl() {
		return unactivatedImageUrl;
	}
	public void setUnactivatedImageUrl(String unactivatedImageUrl) {
		this.unactivatedImageUrl = unactivatedImageUrl;
	}
	public String getActivatedImageUrl() {
		return activatedImageUrl;
	}
	public void setActivatedImageUrl(String activatedImageUrl) {
		this.activatedImageUrl = activatedImageUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	
	

}
