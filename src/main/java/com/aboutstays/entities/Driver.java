package com.aboutstays.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Driver.FIELDS.COLLECTION)
public class Driver {

	public interface FIELDS{
		String COLLECTION = "drivers";
		String NAME = "name";
		String _ID = "_id";
		String IMAGE_URL = "imageUrl";
		String MOBILE_NUMBER = "mobileNumber";
		String CREATED = "created";
		String UPDATED = "updated";
		}
	
	@Id
	private String id;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.MOBILE_NUMBER)
	private String mobileNumber;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	
	public Driver(){
	 this.created = new Date().getTime();
	 this.updated = this.created;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public long getUpdated() {
		return updated;
	}

	public void setUpdated(long updated) {
		this.updated = updated;
	}
	
	
}
