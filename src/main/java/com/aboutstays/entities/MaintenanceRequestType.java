package com.aboutstays.entities;

public enum MaintenanceRequestType {

	NOT_WORKING(1),
	PARTIALLY_WORKING(2),
	DAMAGED(3);
	
	private int code;
	
	public int getCode() {
		return code;
	}
	
	private MaintenanceRequestType(int code) {
		this.code = code;
	}
	
	public static MaintenanceRequestType findValueByCode(Integer value){
		for(MaintenanceRequestType type : MaintenanceRequestType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
	
}
