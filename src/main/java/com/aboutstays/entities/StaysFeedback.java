package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.NameQualityTimetaken;

@Document(collection=StaysFeedback.FIELDS.COLLECTION)
public class StaysFeedback extends BaseSessionEntity{

	public interface FIELDS{
		String COLLECTION = "staysFeedback";
		String CREATED = "created";
		String UPDATED = "updated";
		String LIST_FEEDBACKS = "listFeedbacks";
		String SPECIAL_STAFF = "specialStaff";
		String COMMENT = "comment";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.LIST_FEEDBACKS)
	private List<NameQualityTimetaken> listFeedback;
	@Field(value=FIELDS.SPECIAL_STAFF)
	private String specialStaff;
	@Field(value=FIELDS.COMMENT)
	private String comment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public List<NameQualityTimetaken> getListFeedback() {
		return listFeedback;
	}
	public void setListFeedback(List<NameQualityTimetaken> listFeedback) {
		this.listFeedback = listFeedback;
	}
	public String getSpecialStaff() {
		return specialStaff;
	}
	public void setSpecialStaff(String specialStaff) {
		this.specialStaff = specialStaff;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
}
