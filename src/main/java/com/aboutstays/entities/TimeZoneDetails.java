package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.StringUtils;

@Document(collection=TimeZoneDetails.FIELDS.COLLECTION)
public class TimeZoneDetails {
	
	public interface FIELDS{
		String COLLECTION = "timeZoneDetails";
		String TIMEZONE = "timezone";
		String HOURS_DIFFERENCE = "hoursDifference";
		String CREATED = "created";
		String UPDATED = "updated";
	}

	@Id
	private String id;
	@Field(value=FIELDS.TIMEZONE)
	@Indexed
	private String timezone;
	@Field(value=FIELDS.HOURS_DIFFERENCE)
	private Double hoursDifference;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTimezone() {
		if(!StringUtils.isEmpty(timezone))
			timezone = timezone.toUpperCase();
		return timezone;
	}
	public void setTimezone(String timezone) {
		if(!StringUtils.isEmpty(timezone))
			timezone = timezone.toUpperCase();
		this.timezone = timezone;
	}
	public Double getHoursDifference() {
		return hoursDifference;
	}
	public void setHoursDifference(Double hoursDifference) {
		this.hoursDifference = hoursDifference;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	
	
	
}
