package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=MaintenanceRequestEntity.FIELDS.COLLECTION)
public class MaintenanceRequestEntity extends BaseSessionEntity{
	
	public interface FIELDS{
		String COLLECTION = "maintenanceRequest";
		String CREATED = "created";
		String UPDATED = "updated";
		String DELIVERY_TIME = "deliveryTime";
		String COMMENTS = "comments";
		String REQUEST_TYPE = "requestType";
		String STATUS = "status";
		String ITEM_NAME = "itemName";
		String PROBLEM_TYPE = "problemType";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DELIVERY_TIME)
	private Long deliveryTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.REQUEST_TYPE)
	private int requestType;
	@Field(value=FIELDS.STATUS)
	private int status;
	@Field(value=FIELDS.ITEM_NAME)
	private String itemName;
	@Field(value=FIELDS.PROBLEM_TYPE)
	private String problemType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getRequestType() {
		return requestType;
	}
	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getProblemType() {
		return problemType;
	}
	public void setProblemType(String problemType) {
		this.problemType = problemType;
	}
	

}
