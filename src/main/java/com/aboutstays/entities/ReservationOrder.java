package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=ReservationOrder.FIELDS.COLLECTION)
public class ReservationOrder extends BaseSessionEntity{
	
	public interface FIELDS {
		String COLLECTION = "reservationOrder";
		String CREATED = "created";
		String UPDATED = "updated";
		String DELIVERY_TIME = "deliveryTime";
		String COMMENTS = "comments";
		String STATUS = "status";
		String RS_ITEM_ID = "rsItemId";
		String PRICE = "price";
		String DURATION = "duration";
		String NAME = "name";
		String PAX = "pax";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DELIVERY_TIME)
	private Long deliveryTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.RS_ITEM_ID)
	private String rsItemId;
	@Field(value=FIELDS.PRICE)
	private Double price;
	@Field(value=FIELDS.DURATION)
	private String duration;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.PAX)
	private Integer pax;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}

	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRsItemId() {
		return rsItemId;
	}
	public void setRsItemId(String rsItemId) {
		this.rsItemId = rsItemId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPax() {
		return pax;
	}
	public void setPax(Integer pax) {
		this.pax = pax;
	}
	
}
