package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.GoesWellWithItem;

@Document(collection=FoodItem.FIELDS.COLLECTION)
public class FoodItem {
	
	public interface FIELDS {
		String COLLECTION = "foodItems";
		String ROOM_SERVICES_ID = "roomServicesId";
		String HOTEL_ID = "hotelId";
		String IMAGE_URL = "imageUrl";
		String NAME = "name";
		String SERVES = "serves";
		String CUISINE_TYPE = "cuisineType";
		String MRP = "mrp";
		String PRICE = "price";
		String DESCRIPTION = "description";
		String FOOD_LABEL = "foodLabel";
		String SPICY_TYPE = "spicyType";
		String RECOMMENDED = "recommended";
		String GOES_WELL_WITH_ITEMS = "listGoesWellItems";
		String CREATED = "created";
		String UPDATED = "updated";
		String MENU_ITEM_TYPE = "menuItemtype";
		String CATEGORY = "category";
		String SHOW_ON_APP = "showOnApp";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.ROOM_SERVICES_ID)
	private String roomServicesId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.SERVES)
	private Integer serves;
	@Field(value=FIELDS.CUISINE_TYPE)
	private String cuisineType;
	@Field(value=FIELDS.MRP)
	private Double mrp;
	@Field(value=FIELDS.PRICE)
	private Double price;
	@Field(value=FIELDS.DESCRIPTION)
	private String description;
	@Field(value=FIELDS.FOOD_LABEL)
	private Integer foodLabel;//Refer FoodLabel enum
	@Field(value=FIELDS.SPICY_TYPE)
	private Integer spicyType;//Refer SpicyType enum
	@Field(value=FIELDS.RECOMMENDED)
	private boolean recommended;
	@Field(value=FIELDS.GOES_WELL_WITH_ITEMS)
	private List<GoesWellWithItem> listGoesWellItems;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.MENU_ITEM_TYPE)	
	private Integer menuItemtype; //Refer MenuItemType enum
	@Field(value=FIELDS.CATEGORY)
	private String category;
	@Field(value=FIELDS.SHOW_ON_APP)
	private boolean showOnApp;
	
	public String getRoomServicesId() {
		return roomServicesId;
	}
	public void setRoomServicesId(String roomServicesId) {
		this.roomServicesId = roomServicesId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getServes() {
		return serves;
	}
	public void setServes(Integer serves) {
		this.serves = serves;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCuisineType() {
		return cuisineType;
	}
	public void setCuisineType(String cuisineType) {
		this.cuisineType = cuisineType;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getFoodLabel() {
		return foodLabel;
	}
	public void setFoodLabel(Integer foodLabel) {
		this.foodLabel = foodLabel;
	}
	public Integer getSpicyType() {
		return spicyType;
	}
	public void setSpicyType(Integer spicyType) {
		this.spicyType = spicyType;
	}
	public boolean isRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	public List<GoesWellWithItem> getListGoesWellItems() {
		return listGoesWellItems;
	}
	public void setListGoesWellItems(List<GoesWellWithItem> listGoesWellItems) {
		this.listGoesWellItems = listGoesWellItems;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public Integer getMenuItemtype() {
		return menuItemtype;
	}
	public void setMenuItemtype(Integer menuItemtype) {
		this.menuItemtype = menuItemtype;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isShowOnApp() {
		return showOnApp;
	}
	public void setShowOnApp(boolean showOnApp) {
		this.showOnApp = showOnApp;
	}
}
