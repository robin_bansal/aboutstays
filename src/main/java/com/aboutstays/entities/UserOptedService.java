package com.aboutstays.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=UserOptedService.FIELDS.COLLECTION)
public class UserOptedService extends BaseSessionEntity{

	public interface FIELDS{
		String COLLECTION = "userOptedServices";
		String _ID = "_id";
		String CREATED = "created";
		String UPDATED = "updated";
		String REF_ID = "refId";
		String REF_COLLECTION_TYPE = "refCollectionType";
		String SF_ID = "sfId";
		}
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.REF_ID)
	private String refId;
	@Field(value=FIELDS.REF_COLLECTION_TYPE)
	private Integer refCollectionType;
	@Field(value=FIELDS.SF_ID)
	private String sfId;
	
	public UserOptedService(){
		this.created = new Date().getTime();
		this.updated = this.created;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public long getUpdated() {
		return updated;
	}

	public void setUpdated(long updated) {
		this.updated = updated;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Integer getRefCollectionType() {
		return refCollectionType;
	}

	public void setRefCollectionType(Integer refCollectionType) {
		this.refCollectionType = refCollectionType;
	}

	public String getSfId() {
		return sfId;
	}

	public void setSfId(String sfId) {
		this.sfId = sfId;
	}
	
	
}
