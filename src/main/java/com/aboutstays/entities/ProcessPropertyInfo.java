package com.aboutstays.entities;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=ProcessPropertyInfo.FIELDS.COLLECTION)
public class ProcessPropertyInfo {
	
	public interface FIELDS{
		String COLLECTION = "processPropertyInfo";
		String PROCESS_NAME = "processName";
		String PROPERTY_NAME = "propertyName";
		String PROPERTY_VALUE = "propertyValue";
	}
	
	@Field(value=FIELDS.PROCESS_NAME)
	private String processName;
	@Field(value=FIELDS.PROPERTY_NAME)	
	private String propertyName;
	@Field(value=FIELDS.PROPERTY_VALUE)
	private String propertyValue;
	
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	
	
	
	

}
