package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.ParentCategoryItem;

@Document(collection=HouseRules.FIELDS.COLLECTION)
public class HouseRules {

	public interface FIELDS{
		String COLLECTION = "houseRules";
		String _ID = "_id";
		String CREATED = "created";
		String UPDATED = "updated";
		String LIST_PARENT_CATEGORY_ITEM = "listPGItem";
		}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	@Field(value=FIELDS.LIST_PARENT_CATEGORY_ITEM)
	private List<ParentCategoryItem> listPGItem;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	public List<ParentCategoryItem> getListPGItem() {
		return listPGItem;
	}
	public void setListPGItem(List<ParentCategoryItem> listPGItem) {
		this.listPGItem = listPGItem;
	}	
}
