package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=SuggestedHotels.FIELDS.COLLECTION)
public class SuggestedHotels {
	
	public interface FIELDS{
		String COLLECTION = "suggestedHotels";
		String CITY = "city";
		String CREATED = "created";
		String UPDATED = "updated";
		String HOTEL = "hotel";
		String USER_ID = "userId";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.HOTEL)
	private String hotel;
	@Field(value=FIELDS.CITY)
	private String city;
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.CREATED)
	private long created;
	@Field(value=FIELDS.UPDATED)
	private long updated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotel() {
		return hotel;
	}
	public void setHotel(String hotel) {
		this.hotel = hotel;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getUpdated() {
		return updated;
	}
	public void setUpdated(long updated) {
		this.updated = updated;
	}
	
	

}
