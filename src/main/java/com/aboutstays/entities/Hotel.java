package com.aboutstays.entities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.CheckInDetails;
import com.aboutstays.pojos.CheckOutDetails;
import com.aboutstays.pojos.HotelAward;
import com.aboutstays.pojos.HotelGeneralInformation;
import com.aboutstays.pojos.ReviewsAndRatings;
import com.aboutstays.pojos.StaysDetails;

@Document(collection=Hotel.FIELDS.COLLECTION)
public class Hotel {

	public interface FIELDS{
		String COLLECTION = "hotels";
		String GENERAL_INFO = "generalInformation";
		String ROOMS_IDS = "roomsIds";
		String REVIEWS_AND_RATINGS = "reviewsAndRatings";
		String HOTEL_TYPE = "hotelType";
		String _ID = "_id";
		String ESSENTIALS = "essentials";
		String PARTNERSHIPT_TYPE = "partnershipType";
		String CHECK_IN_DETAILS = "checkInDetails";
		String STAYS_DETAILS = "stayDetails";
		String CHECK_OUT_DETAILS = "checkOutDetails";
		String AMENITIES = "amenities";
		String AWARDS="awards";
		String CARD_ACCEPTED="cardsAccepted";
		String LOYALTY_PROGRAMMS="loyaltyPrograms";
		String TIMEZONE = "timezone";
	}
	@Id
	private String id;
	@Field(value=FIELDS.HOTEL_TYPE)
	private Integer hotelType;
	@Field(value=FIELDS.GENERAL_INFO)
	private HotelGeneralInformation generalInformation;
	@Field(value=FIELDS.ROOMS_IDS)
	private List<String> roomsIds;
	@Field(value=FIELDS.REVIEWS_AND_RATINGS)
	private ReviewsAndRatings reviewsAndRatings;
	@Field(value=FIELDS.ESSENTIALS)
	private List<String> essentials;
	@Field(value=FIELDS.PARTNERSHIPT_TYPE)
	private Integer partnershipType;
	@Field(value=FIELDS.CHECK_IN_DETAILS)
	private CheckInDetails checkInDetails;
	@Field(value=FIELDS.STAYS_DETAILS)
	private StaysDetails staysDetails;
	@Field(value=FIELDS.CHECK_OUT_DETAILS)
	private CheckOutDetails checkOutDetails;
	@Field(value=FIELDS.AMENITIES)
	private List<String> amenities;
	@Field(value=FIELDS.AWARDS)
	private List<HotelAward> awards;
	@Field(value=FIELDS.CARD_ACCEPTED)
	private List<String> cardsAccepted;
	@Field(value=FIELDS.LOYALTY_PROGRAMMS)
	private List<String> loyaltyPrograms;

	@Field(value=FIELDS.TIMEZONE)
	private String timezone;
	
	public Hotel() {
		roomsIds = new ArrayList<>();
	}
	
	public String getId() {
		return id;
	}
	public HotelGeneralInformation getGeneralInformation() {
		return generalInformation;
	}
	public void setGeneralInformation(HotelGeneralInformation generalInformation) {
		this.generalInformation = generalInformation;
	}
	public List<String> getRoomsIds() {
		return roomsIds;
	}
	public void setRoomsIds(List<String> roomsIds) {
		this.roomsIds = roomsIds;
	}
	public ReviewsAndRatings getReviewsAndRatings() {
		return reviewsAndRatings;
	}
	public Integer getHotelType() {
		return hotelType;
	}
	public void setHotelType(Integer hotelType) {
		this.hotelType = hotelType;
	}
	public void setReviewsAndRatings(ReviewsAndRatings reviewsAndRatings) {
		this.reviewsAndRatings = reviewsAndRatings;
	}
	public List<String> getEssentials() {
		return essentials;
	}
	public void setEssentials(List<String> essentials) {
		this.essentials = essentials;
	}
	public Integer getPartnershipType() {
		return partnershipType;
	}
	public void setPartnershipType(Integer partnershipType) {
		this.partnershipType = partnershipType;
	}

	public CheckInDetails getCheckInDetails() {
		return checkInDetails;
	}

	public void setCheckInDetails(CheckInDetails checkInDetails) {
		this.checkInDetails = checkInDetails;
	}

	public StaysDetails getStaysDetails() {
		return staysDetails;
	}

	public void setStaysDetails(StaysDetails staysDetails) {
		this.staysDetails = staysDetails;
	}

	public CheckOutDetails getCheckOutDetails() {
		return checkOutDetails;
	}

	public void setCheckOutDetails(CheckOutDetails checkOutDetails) {
		this.checkOutDetails = checkOutDetails;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getAmenities() {
		return amenities;
	}
	public void setAmenities(List<String> amenities) {
		this.amenities = amenities;
	}
	
	public List<HotelAward> getAwards() {
		return awards;
	}

	public void setAwards(List<HotelAward> awards) {
		this.awards = awards;
	}

	public List<String> getCardsAccepted() {
		return cardsAccepted;
	}

	public void setCardsAccepted(List<String> cardsAccepted) {
		this.cardsAccepted = cardsAccepted;
	}

	
	public List<String> getLoyaltyPrograms() {
		return loyaltyPrograms;
	}

	public void setLoyaltyPrograms(List<String> loyaltyPrograms) {
		this.loyaltyPrograms = loyaltyPrograms;
	}

	public String getTimezone() {
		return timezone;
	}
	
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
}
