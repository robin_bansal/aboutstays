package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = Floor.FIELDS.COLLECTION)
public class Floor {
	public interface FIELDS {
		String COLLECTION = "floors";
		String _ID = "_id";
		String NAME = "name";
		String CODE="code";
		String WING_ID = "wingId";
		String HOTEL_ID="hotelId";
	}

	@Id
	private String id;

	@Field(value = FIELDS.NAME)
	private String name;

	@Field(value = FIELDS.CODE)
	private String code;

	@Field(value = FIELDS.WING_ID)
	private String wingsId;

	@Field(value = FIELDS.HOTEL_ID)
	private String hotelId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWingsId() {
		return wingsId;
	}

	public void setWingsId(String wingsId) {
		this.wingsId = wingsId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
		
}
