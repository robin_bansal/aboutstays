package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.Address;

@Document(collection=Airport.FIELDS.COLLECTION)
public class Airport {

	public interface FIELDS{
		String COLLECTION = "airports";
		String NAME = "name";
		String TYPE = "type";
		String ADDRESS = "address";
		String DESCRIPTION = "description";
		String AMENITIES = "amenities";
		String IMAGE_URL = "imageUrl";
		String TRAVEL_OPTIONS = "travelOptions";
		String CITY_ID = "cityId";
		String CREATED = "created";
		String UPDATED = "updated";
	}
	
	@Id
	private String airportId;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.TYPE)
	private String type;
	@Field(value=FIELDS.ADDRESS)
	private Address address;
	@Field(value=FIELDS.DESCRIPTION)
	private String description;
	@Field(value=FIELDS.AMENITIES)
	private List<String> amenities;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.TRAVEL_OPTIONS)
	private List<String> travelOptions;
	@Field(value=FIELDS.CITY_ID)
	private String cityId;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	public String getAirportId() {
		return airportId;
	}
	public void setAirportId(String airportId) {
		this.airportId = airportId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getAmenities() {
		return amenities;
	}
	public void setAmenities(List<String> amenities) {
		this.amenities = amenities;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public List<String> getTravelOptions() {
		return travelOptions;
	}
	public void setTravelOptions(List<String> travelOptions) {
		this.travelOptions = travelOptions;
	}
	
	
}
