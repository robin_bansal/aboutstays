package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.EntertaimentItemTypeInfo;

@Document(collection=EntertaimentServiceEntity.FIELDS.COLLECTION)
public class EntertaimentServiceEntity {
	
	public interface FIELDS{
		String COLLECTION = "entertaimentServices";
		String CREATED = "created";
		String UPDATED = "updated";
		String TYPE_INFO_LIST = "typeInfoList";
	}
	@Id
	private String hotelId;
	@Field(FIELDS.TYPE_INFO_LIST)
	private List<EntertaimentItemTypeInfo> typeInfoList;
	@Field(FIELDS.CREATED)
	private Long created;
	@Field(FIELDS.UPDATED)
	private Long updated;
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<EntertaimentItemTypeInfo> getTypeInfoList() {
		return typeInfoList;
	}
	public void setTypeInfoList(List<EntertaimentItemTypeInfo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	
}
