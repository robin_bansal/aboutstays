package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=ReservationItem.FIELDS.COLLECTION)
public class ReservationItem {
	
	public interface FIELDS {
		String COLLECTION = "reservationItem";
		String CREATED = "created";
		String UPDATED = "updated";
		String HOTEL_ID = "hotelId";
		String NAME = "name";
		String OPENING_TIME = "startTime";
		String CLOSING_TIME = "endTime";
		String IMAGE_URL = "imageUrl";
		String ADDITIONAL_IMAGE_URLS = "additionImageUrls";
		String ENTRY_CRITERIA = "entryCriteria";
		String DESCRIPTION = "description";
		String DURATION = "duration";
		String PRICE = "price";
		String LIST_MOST_POPULAR = "mostPopularList";
		String LIST_OFFERINGS = "offeringsList";
		String AMOUNT_APPLICABLE = "amountApplicable";
		String DURATION_BASED = "durationBased";
		String CATEGORY_ID = "categoryId";
		String SUB_CATEGORY = "subCategory";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.NAME)
	private String name;
	@Field(value=FIELDS.OPENING_TIME)
	private String startTime;
	@Field(value=FIELDS.CLOSING_TIME)
	private String endTime;
	@Field(value=FIELDS.IMAGE_URL)
	private String imageUrl;
	@Field(value=FIELDS.ADDITIONAL_IMAGE_URLS)
	private List<String> additionalImageUrls;
	@Field(value=FIELDS.ENTRY_CRITERIA)
	private String entryCriteria;
	@Field(value=FIELDS.DESCRIPTION)
	private String descripton;
	@Field(value=FIELDS.PRICE)
	private Double price;
	@Field(value=FIELDS.DURATION)
	private Long duration;
	@Field(value=FIELDS.LIST_MOST_POPULAR)
	private List<String> mostPopularList;
	@Field(value=FIELDS.LIST_OFFERINGS)
	private List<String> offerings;
	@Field(value=FIELDS.AMOUNT_APPLICABLE)
	private boolean amountApplicable;
	@Field(value=FIELDS.DURATION_BASED)
	private boolean durationBased;
	@Field(value=FIELDS.SUB_CATEGORY)
	private String subCategory;
	@Field(value=FIELDS.CATEGORY_ID)
	private String categoryId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<String> getAdditionalImageUrls() {
		return additionalImageUrls;
	}
	public void setAdditionalImageUrls(List<String> additionalImageUrls) {
		this.additionalImageUrls = additionalImageUrls;
	}
	public String getEntryCriteria() {
		return entryCriteria;
	}
	public void setEntryCriteria(String entryCriteria) {
		this.entryCriteria = entryCriteria;
	}
	public String getDescripton() {
		return descripton;
	}
	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public List<String> getMostPopularList() {
		return mostPopularList;
	}
	public void setMostPopularList(List<String> mostPopularList) {
		this.mostPopularList = mostPopularList;
	}
	public List<String> getOfferings() {
		return offerings;
	}
	public void setOfferings(List<String> offerings) {
		this.offerings = offerings;
	}
	public boolean isAmountApplicable() {
		return amountApplicable;
	}
	public void setAmountApplicable(boolean amountApplicable) {
		this.amountApplicable = amountApplicable;
	}
	public boolean isDurationBased() {
		return durationBased;
	}
	public void setDurationBased(boolean durationBased) {
		this.durationBased = durationBased;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
}
