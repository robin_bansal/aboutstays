package com.aboutstays.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=Stays.FIELDS.COLLECTION)
public class Stays {
	
	public interface FIELDS{
		String COLLECTION = "stays";
		String HOTEL_ID = "hotelId";
		String BOOKING_ID = "bookingId";
		String USER_ID = "userId";
		String ROOM_ID = "roomId";
		String CHECK_IN_TIME = "checkInTime";
		String CHECK_OUT_TIME = "checkOutTime";
		String _ID = "_id";
		String CHECK_IN_DATE = "checkInDate";
		String STAYS_STATUS = "staysStatus";
		String CHECK_OUT_DATE = "checkOutDate";
		String OFFICIAL = "official";
		String CREATED = "created";
		String UPDATED = "updated";
		String ADDED_IN_APP = "addedInApp";
	}
	@Id
	private String staysId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.BOOKING_ID)
	private String bookingId;
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.ROOM_ID)
	private String roomId;
	@Field(value=FIELDS.CHECK_IN_TIME)
	private String checkInTime;
	@Field(value=FIELDS.CHECK_OUT_TIME)
	private String checkOutTime;
	@Field(value=FIELDS.CHECK_IN_DATE)
	private Date checkInDate;
	@Field(value=FIELDS.STAYS_STATUS)
	private Integer staysStatus;
	@Field(value=FIELDS.CHECK_OUT_DATE)
	private Date checkOutDate;
	@Field(value=FIELDS.OFFICIAL)
	private boolean official;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.ADDED_IN_APP)
	private boolean addedInApp;
	
	
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getCheckInTime() {
		return checkInTime;
	}
	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}
	public String getCheckOutTime() {
		return checkOutTime;
	}
	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public Integer getStaysStatus() {
		return staysStatus;
	}
	public void setStaysStatus(Integer staysStatus) {
		this.staysStatus = staysStatus;
	}
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public boolean isOfficial() {
		return official;
	}
	public void setOfficial(boolean official) {
		this.official = official;
	}

	public boolean isAddedInApp() {
		return addedInApp;
	}
	public void setAddedInApp(boolean addedInApp) {
		this.addedInApp = addedInApp;
	}
}
