package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection=BellBoyOrder.FIELDS.COLLECTION)
public class BellBoyOrder extends BaseSessionEntity{
	
	public interface FIELDS{
		String COLLECTION = "bellBoyOrders";
		String CREATED = "created";
		String UPDATED = "updated";
		String PREFERRED_TIME = "preferredTime";
		String COMMENTS = "comments";
		String STATUS = "status";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.PREFERRED_TIME)
	private Long preferredTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Long getPreferredTime() {
		return preferredTime;
	}
	public void setPreferredTime(Long preferredTime) {
		this.preferredTime = preferredTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
