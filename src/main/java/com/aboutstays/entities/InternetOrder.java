package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.InternetPack;

@Document(collection=InternetOrder.FIELDS.COLLECTION)
public class InternetOrder extends BaseSessionEntity{
	
	public interface FIELDS{
		String COLLECTION = "internetOrder";
		String CREATED = "created";
		String UPDATED = "updated";
		String DELIVERY_TIME = "deliveryTime";
		String COMMENTS = "comments";
		String STATUS = "status";
		String INTERNET_PACK = "internetPack";
	}
	
	@Id
	private String id;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.DELIVERY_TIME)
	private Long deliveryTime;
	@Field(value=FIELDS.COMMENTS)
	private String comments;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.INTERNET_PACK)
	private InternetPack internetPack;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Long getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Long deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public InternetPack getInternetPack() {
		return internetPack;
	}
	public void setInternetPack(InternetPack internetPack) {
		this.internetPack = internetPack;
	}
	
	

}
