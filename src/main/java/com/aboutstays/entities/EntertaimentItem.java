package com.aboutstays.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class EntertaimentItem {

	public interface FIELDS{
		String COLLECTION = "entertainmentItem";
		String HOTEL_ID = "hotelId";
		String ENT_SERVICE_ID = "entServiceId";
		String MASTER_ITEM_ID = "masterItemId";
		String CATEGORY = "category";
		String NUMBER = "number";
		String CREATED = "created";
		String UPDATED = "updated";
	}

	@Id
	private String id;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.ENT_SERVICE_ID)
	private String entServiceId;
	@Field(value=FIELDS.MASTER_ITEM_ID)
	private String masterItemId;
	@Field(value=FIELDS.CATEGORY)
	private String category;//category is genere name here
	@Field(value=FIELDS.NUMBER)
	private String number;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getMasterItemId() {
		return masterItemId;
	}
	public void setMasterItemId(String masterItemId) {
		this.masterItemId = masterItemId;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getEntServiceId() {
		return entServiceId;
	}
	public void setEntServiceId(String entServiceId) {
		this.entServiceId = entServiceId;
	}
	
}
