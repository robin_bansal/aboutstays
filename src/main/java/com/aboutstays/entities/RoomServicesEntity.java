package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.MenuItemsByType;

@Document(collection=RoomServicesEntity.FIELDS.COLLECTION)
public class RoomServicesEntity {
	
	public interface FIELDS{
		String COLLECTION = "roomServices";
		String _ID = "_id";
		String CREATED = "created";
		String UPDATED = "updated";
		String LIST_MENU_ITEM = "listMenuItem";
		String SLA_APPLICABLE = "slaApplicable";
		String SLA_TIME = "slaTime";
	}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.LIST_MENU_ITEM)
	private List<MenuItemsByType> listMenuItem;
	@Field(value=FIELDS.SLA_APPLICABLE)
	private boolean slaApplicable;
	@Field(value=FIELDS.SLA_TIME)
	private int slaTime;
	
	public RoomServicesEntity() {
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public List<MenuItemsByType> getListMenuItem() {
		return listMenuItem;
	}
	public void setListMenuItem(List<MenuItemsByType> listMenuItem) {
		this.listMenuItem = listMenuItem;
	}
	public boolean isSlaApplicable() {
		return slaApplicable;
	}
	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}
	public int getSlaTime() {
		return slaTime;
	}
	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}
}
