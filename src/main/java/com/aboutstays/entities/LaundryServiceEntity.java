package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.SupportedLaundryType;

@Document(collection=LaundryServiceEntity.FIELDS.COLLECTION)
public class LaundryServiceEntity {
	
	public interface FIELDS{
		String COLLECTION = "laundryServices";
		String SUPPORTED_LAUNDRY_TYPES = "supportedLaundryTypeList";
		String CREATED = "created";
		String UPDATED = "updated";
		String SLA_APPLICABLE = "slaApplicable";
		String SLA_TIME = "slaTime";
		String START_TIME = "startTime";
		String END_TIME = "endTime";
		String SWIFT_DELIVERY_ENABLED = "swiftDeliveryEnabled";
		String PICKUP_BY_TIME = "pickupByTime";
		String SWIFT_DELIVERY_MESSAGE = "swiftDeliveryMessage";
	}
	
	@Id
	private String hotelId;
	@Field(value=FIELDS.SUPPORTED_LAUNDRY_TYPES)
	private List<SupportedLaundryType> supportedLaundryTypeList;//Refer Supported Item Type
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.SLA_APPLICABLE)
	private boolean slaApplicable;
	@Field(value=FIELDS.SLA_TIME)
	private int slaTime;
	@Field(value=FIELDS.START_TIME)
	private String startTime;
	@Field(value=FIELDS.END_TIME)
	private String endTime;
	@Field(value=FIELDS.SWIFT_DELIVERY_ENABLED)
	private boolean swiftDeliveryEnabled;
	@Field(value=FIELDS.PICKUP_BY_TIME)
	private String pickupByTime;
	@Field(value=FIELDS.SWIFT_DELIVERY_MESSAGE)
	private String swiftDeliveryMessage;
	
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<SupportedLaundryType> getSupportedLaundryTypeList() {
		return supportedLaundryTypeList;
	}
	public void setSupportedLaundryTypeList(List<SupportedLaundryType> supportedLaundryTypeList) {
		this.supportedLaundryTypeList = supportedLaundryTypeList;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public boolean isSlaApplicable() {
		return slaApplicable;
	}
	public void setSlaApplicable(boolean slaApplicable) {
		this.slaApplicable = slaApplicable;
	}
	public int getSlaTime() {
		return slaTime;
	}
	public void setSlaTime(int slaTime) {
		this.slaTime = slaTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public boolean isSwiftDeliveryEnabled() {
		return swiftDeliveryEnabled;
	}
	public void setSwiftDeliveryEnabled(boolean swiftDeliveryEnabled) {
		this.swiftDeliveryEnabled = swiftDeliveryEnabled;
	}
	public String getPickupByTime() {
		return pickupByTime;
	}
	public void setPickupByTime(String pickupByTime) {
		this.pickupByTime = pickupByTime;
	}
	public String getSwiftDeliveryMessage() {
		return swiftDeliveryMessage;
	}
	public void setSwiftDeliveryMessage(String swiftDeliveryMessage) {
		this.swiftDeliveryMessage = swiftDeliveryMessage;
	}
	
}
