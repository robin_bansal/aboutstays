package com.aboutstays.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.aboutstays.pojos.CheckinSpecialRequest;
import com.aboutstays.pojos.CompanyInfo;
import com.aboutstays.pojos.EarlyCheckinRequest;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.PersonalInformation;
import com.aboutstays.pojos.PreferencesType;

@Document(collection=CheckinEntity.FIELDS.COLLECTION)
public class CheckinEntity {

	public interface FIELDS{
		String COLLECTION = "checkInRequests";
		String USER_ID = "userId";
		String HOTEL_ID = "hotelId";
		String PERSONAL_INFORMATION = "personalInformation";
		String IDENTITY_DOCS = "identityDocuments";
		String STAY_PREFERENCES = "listStayPreferences";
		String SPECIAL_REQUESTS = "specialRequests";
		String OFFICIAL_TRIP = "officialTrip";
		String COMPANIES = "companies";
		String EARLY_CHECKIN_REQUEST = "earlyCheckinRequest";
		String STATUS = "status";
		String CREATED = "created";
		String UPDATED = "updated";
		String EARLY_CHECKIN_APPROVED = "earlyCheckinApproved";
		String EARLY_CHECKIN_REQUESTED = "earlyCheckinRequested";

		String CHECKIN_FORM_COMPLETED="checkinFormCompleted";
		String ID_VERIFIED="idVerified";
		String IS_EARLY_CHECKIN_FEES="isEarlyCheckinFee";
		String EARLY_CHECKIN_FEE="earlyCheckinFee";
	}
	
	@Id
	private String staysId;
	@Field(value=FIELDS.USER_ID)
	private String userId;
	@Field(value=FIELDS.HOTEL_ID)
	private String hotelId;
	@Field(value=FIELDS.PERSONAL_INFORMATION)
	private PersonalInformation personalInformation;
	@Field(value=FIELDS.IDENTITY_DOCS)
	private List<IdentityDoc> identityDocuments;
	@Field(value=FIELDS.STAY_PREFERENCES)
	private List<PreferencesType> listStayPreferences;//StayPreferencesType
	@Field(value=FIELDS.SPECIAL_REQUESTS)
	private List<CheckinSpecialRequest> specialRequests;
	@Field(value=FIELDS.OFFICIAL_TRIP)
	private boolean officialTrip;
	@Field(value=FIELDS.COMPANIES)
	private List<CompanyInfo> companies;
	@Field(value=FIELDS.EARLY_CHECKIN_REQUEST)
	private EarlyCheckinRequest earlyCheckinRequest;
	@Field(value=FIELDS.STATUS)
	private Integer status;
	@Field(value=FIELDS.CREATED)
	private Long created;
	@Field(value=FIELDS.UPDATED)
	private Long updated;
	@Field(value=FIELDS.EARLY_CHECKIN_APPROVED)
	private Boolean earlyCheckinApproved;
	@Field(value=FIELDS.EARLY_CHECKIN_REQUESTED)
	private Boolean earlyCheckinRequested;
	@Field(value=FIELDS.CHECKIN_FORM_COMPLETED)
	private Boolean checkinFormCompleted;
	@Field(value=FIELDS.ID_VERIFIED)
	private Boolean idVerified;
	@Field(value=FIELDS.IS_EARLY_CHECKIN_FEES)
	private Boolean isEarlyCheckinFee;
	@Field(value=FIELDS.EARLY_CHECKIN_FEE)
	private Double earlyCheckinFee;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStaysId() {
		return staysId;
	}
	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}
	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}
	public List<IdentityDoc> getIdentityDocuments() {
		return identityDocuments;
	}
	public void setIdentityDocuments(List<IdentityDoc> identityDocuments) {
		this.identityDocuments = identityDocuments;
	}
	public List<PreferencesType> getListStayPreferences() {
		return listStayPreferences;
	}
	public void setListStayPreferences(List<PreferencesType> listStayPreferences) {
		this.listStayPreferences = listStayPreferences;
	}
	public List<CheckinSpecialRequest> getSpecialRequests() {
		return specialRequests;
	}
	public void setSpecialRequests(List<CheckinSpecialRequest> specialRequests) {
		this.specialRequests = specialRequests;
	}
	public boolean isOfficialTrip() {
		return officialTrip;
	}
	public void setOfficialTrip(boolean officialTrip) {
		this.officialTrip = officialTrip;
	}
	public List<CompanyInfo> getCompanies() {
		return companies;
	}
	public void setCompanies(List<CompanyInfo> companies) {
		this.companies = companies;
	}
	public EarlyCheckinRequest getEarlyCheckinRequest() {
		return earlyCheckinRequest;
	}
	public void setEarlyCheckinRequest(EarlyCheckinRequest earlyCheckinRequest) {
		this.earlyCheckinRequest = earlyCheckinRequest;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public Long getUpdated() {
		return updated;
	}
	public void setUpdated(Long updated) {
		this.updated = updated;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Boolean getEarlyCheckinApproved() {
		return earlyCheckinApproved;
	}
	public void setEarlyCheckinApproved(Boolean earlyCheckinApproved) {
		this.earlyCheckinApproved = earlyCheckinApproved;
	}
	public Boolean getEarlyCheckinRequested() {
		return earlyCheckinRequested;
	}
	public void setEarlyCheckinRequested(Boolean earlyCheckinRequested) {
		this.earlyCheckinRequested = earlyCheckinRequested;
	}
	public Boolean getCheckinFormCompleted() {
		return checkinFormCompleted;
	}
	public void setCheckinFormCompleted(Boolean checkinFormCompleted) {
		this.checkinFormCompleted = checkinFormCompleted;
	}
	public Boolean getIdVerified() {
		return idVerified;
	}
	public void setIdVerified(Boolean idVerified) {
		this.idVerified = idVerified;
	}
	public Boolean getIsEarlyCheckinFee() {
		return isEarlyCheckinFee;
	}
	public void setIsEarlyCheckinFee(Boolean isEarlyCheckinFee) {
		this.isEarlyCheckinFee = isEarlyCheckinFee;
	}
	public Double getEarlyCheckinFee() {
		return earlyCheckinFee;
	}
	public void setEarlyCheckinFee(Double earlyCheckinFee) {
		this.earlyCheckinFee = earlyCheckinFee;
	}
	
}
