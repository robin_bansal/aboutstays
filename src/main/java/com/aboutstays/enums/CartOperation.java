package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum CartOperation {

	MODIFY(1), DELETE(2);
	
	private int code;
	
	public int getCode() {
		return code;
	}

	private CartOperation(int code) {
		this.code = code;
	}
	
	public static CartOperation getByCode(Integer code){
		if(code==null)
			return null;
		for(CartOperation cOperation : CartOperation.values()){
			if(cOperation.code==code){
				return cOperation;
			}
		}
		return null;
	}

	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for(CartOperation cOperation : CartOperation.values()){
			codeList.add(cOperation.code);
		}
		return codeList;
	}
}
