package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum LaundryItemType {

	WASH_IRON(1,"Wash & Iron","https://aboutstays.s3.amazonaws.com/images/wash.png"),
	DRY_CLEANING(2,"Dry Cleaning","https://aboutstays.s3.amazonaws.com/images/dry.png"),
	IRON(3,"Iron","https://aboutstays.s3.amazonaws.com/images/iron.png");
	
	private int code;
	private String displayName;
	private String imageUrl;
	public String getImageUrl() {
		return imageUrl;
	}
	public int getCode() {
		return code;
	}
	public String getDisplayName() {
		return displayName;
	}
	private LaundryItemType(int code, String displayName, String imageUrl) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
	}
	
	public static LaundryItemType findByCode(Integer code){
		if(code==null)
			return null;
		for(LaundryItemType itemType : LaundryItemType.values()){
			if(itemType.code==code){
				return itemType;
			}
		}
		return null;
	}
	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for(LaundryItemType itemType : LaundryItemType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}
	
	public static String getNameByCode(int code){
		for(LaundryItemType itemType : LaundryItemType.values()){
			if(itemType.code == code){
				return itemType.getDisplayName();
			}
		}
		return null;
	}
	
}
