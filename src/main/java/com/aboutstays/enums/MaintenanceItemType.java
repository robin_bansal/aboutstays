package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum MaintenanceItemType {

	INTERNET(1, "Internet", "https://aboutstays.s3.amazonaws.com/images/Internet.png"),
	APPLIANCES(2, "Appliances", "https://aboutstays.s3.amazonaws.com/images/Appliances.png"),
	ELECTRICAL(3, "Electrical", "https://aboutstays.s3.amazonaws.com/images/Electrical.png"),
	PLUMBING(4, "Plumbing", "https://aboutstays.s3.amazonaws.com/images/Plumbing.png");
	
	private int code;
	private String displayName;
	private String imageUrl;
	
	public int getCode() {
		return code;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	private MaintenanceItemType(int code, String displayName, String imageUrl) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
	}
	
	public static MaintenanceItemType getByCode(Integer code){
		if(code==null)
			return null;
		for(MaintenanceItemType itemType : MaintenanceItemType.values()){
			if(itemType.code==code)
				return itemType;
		}
		return null;
	}
	
	public static List<Integer> getCodeList(){
		List<Integer> codeList = new ArrayList<>(MenuItemType.values().length);
		for(MaintenanceItemType itemType : MaintenanceItemType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}
}
