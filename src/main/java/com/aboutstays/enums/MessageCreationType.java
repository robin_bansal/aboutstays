package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum MessageCreationType {

	BY_USER(1), BY_HOTEL(2), REQUEST_AUTOMATED(3);
	
	private int code;
	
	public int getCode() {
		return code;
	}

	private MessageCreationType(int code) {
		this.code = code;
	}
	
	public static MessageCreationType findByCode(Integer code){
		if(code==null)
			return null;
		for(MessageCreationType messageCreationType : MessageCreationType.values()){
			if(messageCreationType.code == code){
				return messageCreationType;
			}
		}
		return null;
	}

	public static boolean isMessageByUser(int creationType) {
		MessageCreationType messageCreationType = findByCode(creationType);
		if(MessageCreationType.BY_USER.equals(messageCreationType)){
			return true;
		}
		return false;
	}

	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for(MessageCreationType messageCreationType : MessageCreationType.values()){
			codeList.add(messageCreationType.code);
		}
		return codeList;
	}
}
