package com.aboutstays.enums;

import com.aboutstays.entities.GeneralService;

public enum StaysOrdering {

	CURRENT(1), FUTURE(2), PAST(3);
	
	private int code;
	
	public int getCode() {
		return code;
	}

	private StaysOrdering(int code) {
		this.code = code;
	}
	
	public static StaysOrdering findByCode(Integer code){
		if(code==null)
			return null;
		for(StaysOrdering staysOrdering : StaysOrdering.values()){
			if(staysOrdering.code == code){
				return staysOrdering;
			}
		}
		return null;
	}

	public static StaysOrdering getDefaultOrdering() {
		return StaysOrdering.FUTURE;
	}

	public static boolean isValid(StaysOrdering staysOrdering, GeneralService generalService) {
		if(StaysOrdering.CURRENT.equals(staysOrdering))
			return true;
		else if(StaysOrdering.PAST.equals(staysOrdering)){
			if(generalService.isSupportsPast()){
				return true;
			} else {
				return false;
			}
		} else if(StaysOrdering.FUTURE.equals(staysOrdering)){
			if(generalService.isSupportsFuture()){
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}
