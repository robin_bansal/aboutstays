package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum HotelType {
	
	ONE_STAR(1), TWO_STAR(2), THREE_STAR(3), FOUR_STAR(4), FIVE_STAR(5), SIX_STAR(6), SEVEN_STAR(7), PREMIUM(11);
	
	private Integer code;

	public Integer getCode() {
		return code;
	}

	private HotelType(Integer code) {
		this.code = code;
	}
	
	public static boolean matchHotelType(int value){
		if(findHotelByType(value)!=null)
			return true;
		return false;
	}
	
	public static HotelType findHotelByType(Integer value){
		for(HotelType type : HotelType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}

	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<>();
		for(HotelType type : HotelType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}

	public static int getDefaultValue() {
		return THREE_STAR.code;
	}
	
}
