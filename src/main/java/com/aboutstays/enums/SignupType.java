package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum SignupType {
	
	MANUAL(1), GMAIL(2), FACEBOOK(3);
	
	private Integer code;
	
	public Integer getCode() {
		return code;
	}
	
	private SignupType(Integer code) {
		this.code = code;
	}

	public static SignupType findSignupTypeByCode(Integer code){
		for(SignupType type : SignupType.values()){
			if(type.code==code){
				return type;
			}
		}
		return null;
	}

	public static SignupType findSignupTypeByValue(String value){
		for(SignupType type : SignupType.values()){
			if(type.name().equalsIgnoreCase(value)){
				return type;
			}
		}
		return null;
	}
	
	public static List<String> getTypeList(){
		List<String> typeList = new ArrayList<>();
		for(SignupType type : SignupType.values()){
			typeList.add(type.name());
		}
		return typeList;
	}
	
	public static Integer getDefaultValue(){
		return MANUAL.code;
	}

}
