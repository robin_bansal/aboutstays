package com.aboutstays.enums;

public enum Icons {

	ADULT(""),
	CHILD("");
	
	private String iconUrl;
	
	public String getIconUrl() {
		return iconUrl;
	}

	private Icons(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	
}
