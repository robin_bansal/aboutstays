package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum MenuItemType {

	MINI_BAR(1,"Mini Bar","https://aboutstays.s3.amazonaws.com/images/minibar.png"),
	BREAKFAST(2,"Breakfast","https://aboutstays.s3.amazonaws.com/images/breakfast.png"),
	LUNCH(3,"Lunch","https://aboutstays.s3.amazonaws.com/images/lunch.png"),
	DINNER(4,"Dinner","https://aboutstays.s3.amazonaws.com/images/dinner.png"),
	DESSERTS(5,"Desserts","https://aboutstays.s3.amazonaws.com/images/dessert.png"),
	ALL_DAY(6,"24 Hrs","https://aboutstays.s3.amazonaws.com/images/all_day.png"),
	BAKERY_CAFE(7,"Bakery & Cafe","https://aboutstays.s3.amazonaws.com/images/bakery.png"),
	BEVERAGES_DRINKS(8,"Becerages & Drinks","https://aboutstays.s3.amazonaws.com/images/beverage.png");
	
	private int code;
	private String displayName;
	private String imageUrl;
	
	public int getCode() {
		return code;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	private MenuItemType(int code, String displayName, String imageUrl) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
	}
	
	public static MenuItemType getByCode(Integer code){
		if(code==null)
			return null;
		for(MenuItemType itemType : MenuItemType.values()){
			if(itemType.code==code)
				return itemType;
		}
		return null;
	}
	
	public static List<Integer> getCodeList(){
		List<Integer> codeList = new ArrayList<>(MenuItemType.values().length);
		for(MenuItemType itemType : MenuItemType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}
}
