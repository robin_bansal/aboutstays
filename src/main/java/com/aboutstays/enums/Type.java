package com.aboutstays.enums;

public enum Type {
	
	EMAIL(1), PHONE(2), USERID(3);
	
	private Integer code;
	
	public Integer getCode() {
		return code;
	}

	private Type(Integer code){
		this.code = code;
	}


	public static Type findByType(String value){
		for(Type type:Type.values()){
			if(type.name().equalsIgnoreCase(value)){
				return type;
			}
		}
		return null;
	}

	public static Type findTypeByCode(Integer value){
		for(Type type : Type.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
}
