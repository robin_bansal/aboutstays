package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum LaundryPreferences {
	
	FOLDED(1), ON_HANGER(2);
	
	private Integer code;

	public Integer getCode() {
		return code;
	}

	private LaundryPreferences(Integer code) {
		this.code = code;
	}
	
	
	public static LaundryPreferences findLaundryByType(Integer value){
		for(LaundryPreferences type : LaundryPreferences.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}

	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<Integer>();
		for(LaundryPreferences type: LaundryPreferences.values()){
			typeList.add(type.code);
		}
		return typeList;
	}

}
