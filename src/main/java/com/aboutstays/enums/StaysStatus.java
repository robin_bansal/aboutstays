package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum StaysStatus {
	
	CHECK_IN(1), STAYING(2), CHECK_OUT(3), INITIATED_CHECK_IN(4), CHECKED_OUT(5), ALL(6)/*All Used only for general service*/;
	
	private Integer code;

	private StaysStatus(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public static StaysStatus findStatusByCode(Integer value){
		for(StaysStatus status : StaysStatus.values()){
			if(status.code.equals(value)){
				return status;
			}
		}
		return null;
	}
	
	public Integer getDefaultStatus(){
		return CHECK_IN.code;
	}
	
	public static List<Integer> getTypeList(){
		List<Integer> typeList = new ArrayList<>();
		for(StaysStatus type : StaysStatus.values()){
			typeList.add(type.code);
		}
		return typeList;
	}
	

}
