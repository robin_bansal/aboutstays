package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum RefCollectionType {

	FOOD(1), 
	COMMUTE(2), 
	MAINTENANCE(3), 
	AIRPORT_PICKUP(4), 
	LAUNDRY(5), 
	HOUSEKEEPING(6), 
	BELL_BOY(7), 
	RESERVATION(8),
	INTERNET(9), 
	LATE_CHECKOUT(10),  
	REQUESTS(11), 
	STAYS_FEEDBACK(12), 
	EARLY_CHECK_IN(13);
	
	private  Integer code;

	private RefCollectionType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public static RefCollectionType findValueByType(Integer value){
		if(value==null){
			return null;
		}
		for(RefCollectionType type : RefCollectionType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
	
	public static List<Integer> getTypeList(){
		List<Integer> typeList = new ArrayList<>();
		for(RefCollectionType type : RefCollectionType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}
}
