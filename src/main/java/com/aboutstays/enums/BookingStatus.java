package com.aboutstays.enums;

public enum BookingStatus {
	UN_ALLOCATED(1),ALLOCATED(2), FREEZE(3), COMPLETE(4);

	private int status;

	private BookingStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
