package com.aboutstays.enums;

public enum HousekeepingOrderStatus {

	INITIATED(1, "Initiated", ""), 
	PROCESSING(2, "Processing", ""), 
	PENDING(3, "Pending", ""),
	PREPAIRING(4, "Prepairing", ""),
	UPDATED(5, "Updated", ""),
	COMPLETED(6, "Compleated", ""),
	CANCELLED(7, "Cancelled", "");
	
	private int code;
	private String message;
	private String iconUrl;
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getIconUrl() {
		return iconUrl;
	}

	private HousekeepingOrderStatus(int code, String message, String iconUrl) {
		this.code = code;
		this.message = message;
		this.iconUrl = iconUrl;
	}
	
	public static HousekeepingOrderStatus findByCode(Integer code){
		if(code!=null){
			for(HousekeepingOrderStatus housekeepingOrderStatus : HousekeepingOrderStatus.values()){
				if(housekeepingOrderStatus.code==code)
					return housekeepingOrderStatus;
			}
		}
		return null;
	}
	
	public static RequestDashboardStatus getByLaundryOrderStatus(HousekeepingOrderStatus housekeepingOrderStatus){
		if(housekeepingOrderStatus==null)
			return null;
		if(HousekeepingOrderStatus.INITIATED.equals(housekeepingOrderStatus)||HousekeepingOrderStatus.UPDATED.equals(housekeepingOrderStatus)){
			return RequestDashboardStatus.SCHEDULED;
		}
		if(HousekeepingOrderStatus.PROCESSING.equals(housekeepingOrderStatus)||HousekeepingOrderStatus.PREPAIRING.equals(housekeepingOrderStatus)||
				HousekeepingOrderStatus.PENDING.equals(housekeepingOrderStatus)){
			return RequestDashboardStatus.OPEN;
		}
		if(HousekeepingOrderStatus.CANCELLED.equals(housekeepingOrderStatus)||HousekeepingOrderStatus.COMPLETED.equals(housekeepingOrderStatus)){
			return RequestDashboardStatus.CLOSED;
		}
		return null;
	}

}
