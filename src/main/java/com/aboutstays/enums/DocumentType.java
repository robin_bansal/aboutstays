package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum DocumentType {

	AADHAR_CARD(1), DRIVING_LICENSE(2), PAN_CARD(3), PASSPORT(4), VOTER_CARD(5); 
	
	private Integer code;
	
	private DocumentType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public static DocumentType findStatusByCode(Integer value){
		for(DocumentType status : DocumentType.values()){
			if(status.code == value){
				return status;
			}
		}
		return null;
	}

	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<Integer>();
		for(DocumentType type: DocumentType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}
}
