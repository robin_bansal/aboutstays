package com.aboutstays.enums;

public enum InformationType {
	
	REQUEST_RELATED(1), GENERAL(2), PROMOTIONAL(3);
	
	private Integer code;

	private InformationType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public static InformationType findValueByCode(Integer value){
		for(InformationType type : InformationType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}

	
}
