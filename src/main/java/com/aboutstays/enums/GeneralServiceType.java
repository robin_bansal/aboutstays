package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.services.AirportServicesService;
import com.aboutstays.services.impl.AirportServicesServiceImpl;

public enum GeneralServiceType {

	AIRPORT_PICKUP(1, "Airport Pickups"),
	NOTIFICATION(2, "Notifications"),
	REQUEST(3, "Requests"),
	EXPENSE(4, "Expenses"),
	ROOM_SERVICE(5, "Room Service"),
	HOUSEKEEPING(6, "Housekeeping"),
	LAUNDRY(7, "Laundry"),
	INTERNET(8, "Internet"),
	RESERVATION(9, "Reservations"),
	COMMUTE(10, "Commute"),
	MAINTENANCE(11, "Maintenance"),
	MESSAGE(12, "Message"),
	AIRPORT_DROP(13, "Airport Drop"),
	LATE_CHECKOUT(14, "Late Check-out"),
	BELL_BOY(15, "Bell Boy"),
	FEEDBACK_AND_SUGGESTION(16, "Feedback & Suggestion"),
	HOTEL_INFO(17, "Hotel Info"),
	HOTEL_INFO_STAYS(18, "Hotel Info"),
	HOUSE_RULES(19, "House Rules"),
	TV_ENTERTAINMENT(20, "Tv/Entertainment"),
	CITY_GUIDE(21, "City Guide"),
	COMPLEMENTRY_SERVICE(22, "Complementary Service"),
	IN_HOTEL_NAVIGATION(23, "In Hotel Navigation"),
	EXPENSE_CHECKOUT(24, "Expenses"),
	EARLY_CHECK_IN(25, "Early Check-In"),
	MESSAGE_CURRENT_STAY(26, "Message"),
	NOTIFICATIONS_CHECKIN(27, "Notifications"),
	REQUESTS_CHECKIN(28, "Requests"),
	EXPENSE_CHECKIN(29, "Expenses");
	
	private Integer code;
	private String message;
	
	
	private GeneralServiceType(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	
	public static GeneralServiceType findGeneralServiceByType(Integer value){
		for(GeneralServiceType type : GeneralServiceType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
	
	public static List<Integer> getTypeList(){
		List<Integer> typeList = new ArrayList<>();
		for(GeneralServiceType type : GeneralServiceType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}

	public static String getNameFromServiceCode(Integer code){
		for(GeneralServiceType type : GeneralServiceType.values()){
			if(type.code == code){
				return type.getMessage();
			}
		}
		return null;
	}
	
	public static void validate(GeneralServiceType gsType, String servicesId, AutowireCapableBeanFactory beanFactory) throws AboutstaysException{
		if(gsType==null){
			
		}
		switch(gsType){
		case AIRPORT_PICKUP:
			AirportServicesService airportServicesService = new AirportServicesServiceImpl();
			beanFactory.autowireBean(airportServicesService);
			if(!airportServicesService.existsById(servicesId)){
				throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_SERVICE_NOT_EXISTS);
			}
			break;
		case COMMUTE:
			break;
		case EXPENSE:
			break;
		case HOUSEKEEPING:
			break;
		case INTERNET:
			break;
		case LAUNDRY:
			break;
		case MAINTENANCE:
			break;
		case MESSAGE:
			break;
		case NOTIFICATION:
			break;
		case REQUEST:
			break;
		case RESERVATION:
			break;
		case ROOM_SERVICE:
			break;
		case AIRPORT_DROP:
			break;
		case BELL_BOY:
			break;
		case FEEDBACK_AND_SUGGESTION:
			break;
		case LATE_CHECKOUT:
			break;
		default:
			break;
		
		}
	}
}
