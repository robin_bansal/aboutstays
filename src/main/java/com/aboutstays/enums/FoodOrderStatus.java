package com.aboutstays.enums;

public enum FoodOrderStatus {

	INITIATED(1, "INITIATED", ""), 
	PROCESSING(2, "PROCESSING", ""), 
	PENDING(3, "PROCESSING", ""),
	PREPAIRING(4, "PROCESSING", ""),
	UPDATED(5, "UPDATED", ""),
	COMPLETED(6, "COMPLETED", ""),
	CANCELLED(7, "CANCELLED", ""),
	SCHEDULED(8,"SCHEDULED","");
	
	private int code;
	private String message;
	private String iconUrl;
	
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}

	public String getIconUrl() {
		return iconUrl;
	}
	
	private FoodOrderStatus(int code, String message, String iconUrl) {
		this.code = code;
		this.message = message;
		this.iconUrl = iconUrl;
	}
	
	public static FoodOrderStatus findByCode(Integer code){
		if(code!=null){
			for(FoodOrderStatus foodOrderStatus : FoodOrderStatus.values()){
				if(foodOrderStatus.code==code)
					return foodOrderStatus;
			}
		}
		return null;
	}
	
	
	
	public static RequestDashboardStatus getByFoodOrderStatus(FoodOrderStatus foodOrderStatus){
		if(foodOrderStatus==null)
			return null;
		if(FoodOrderStatus.INITIATED.equals(foodOrderStatus)||FoodOrderStatus.SCHEDULED.equals(foodOrderStatus)){
			return RequestDashboardStatus.SCHEDULED;
		}
		if(FoodOrderStatus.PROCESSING.equals(foodOrderStatus)||FoodOrderStatus.PREPAIRING.equals(foodOrderStatus)||
				FoodOrderStatus.PENDING.equals(foodOrderStatus)||FoodOrderStatus.UPDATED.equals(foodOrderStatus)){
			return RequestDashboardStatus.OPEN;
		}
		if(FoodOrderStatus.CANCELLED.equals(foodOrderStatus)||FoodOrderStatus.COMPLETED.equals(foodOrderStatus)){
			return RequestDashboardStatus.CLOSED;
		}
		return null;
	}

	public static boolean isClosed(FoodOrderStatus foodOrderStatus) {
		if(foodOrderStatus!=null){
			if(FoodOrderStatus.COMPLETED.equals(foodOrderStatus)||FoodOrderStatus.CANCELLED.equals(foodOrderStatus)){
				return true;
			}
		}
		return false;
		
	}
}
