package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum PickupStatus {

	INITIATED(1), APPROVED(2), CANCELLED(3);
	
	private  Integer code;

	private PickupStatus(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public static PickupStatus findValueByType(Integer value){
		for(PickupStatus type : PickupStatus.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}

	public static Integer getDefaultValue() {
		return INITIATED.code;
	}

	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<>();
		for(PickupStatus type : PickupStatus.values()){
			typeList.add(type.code);
		}
		return typeList;
	}
}
