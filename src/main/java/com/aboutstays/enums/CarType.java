package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum CarType {
	
	SEDAN(1, "Sedan", "https://aboutstays.s3.amazonaws.com/images/Sedan.png"), 
	SUV(2, "SUV", "https://aboutstays.s3.amazonaws.com/images/Suv.png"), 
	HATCHBACK(3, "Hatchback", "https://aboutstays.s3.amazonaws.com/images/Hatchback.png"), 
	PREMIUM(4, "Premium", "https://aboutstays.s3.amazonaws.com/images/Premium.png");
	
	private Integer code;
	private String message;
	private String imageUrl;
	
	
	private CarType(Integer code, String message, String imageUrl) {
		this.code = code;
		this.message = message;
		this.imageUrl = imageUrl;
	}

	public Integer getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public static CarType findValueByCode(Integer value){
		for(CarType type : CarType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
	
	public static String getNameFromServiceCode(Integer code){
		for(CarType type : CarType.values()){
			if(type.code == code){
				return type.getMessage();
			}
		}
		return null;
	}
	
	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<>();
		for(CarType type : CarType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}
	
	public Integer getDefaultCarType(){
		return SEDAN.code;
	}

}
