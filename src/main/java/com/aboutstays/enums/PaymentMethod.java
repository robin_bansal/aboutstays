package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum PaymentMethod {

	ONLINE(1), FRONT_DESK(2);
	
	private int code;
	
	public int getCode() {
		return code;
	}

	private PaymentMethod(int code) {
		this.code = code;
	}
	
	public static PaymentMethod findByCode(Integer code){
		if(code==null)
			return null;
		for(PaymentMethod paymentMethod : PaymentMethod.values()){
			if(paymentMethod.code==code){
				return paymentMethod;
			}
		}
		return null;
	}

	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for(PaymentMethod paymentMethod : PaymentMethod.values()){
			codeList.add(paymentMethod.code);
		}
		return codeList;
	}
}
