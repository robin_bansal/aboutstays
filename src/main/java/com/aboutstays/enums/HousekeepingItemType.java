package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum HousekeepingItemType {

	LINEN(1, "Linen","https://aboutstays.s3.amazonaws.com/images/lenin.png"),
	BEDROOM(2,"Bedroom","https://aboutstays.s3.amazonaws.com/images/bedroom.png"),
	BATHROOM(3,"Bathroom","https://aboutstays.s3.amazonaws.com/images/bath_red.png"),
	ROOM_CLEANING(4,"Room Cleaning","https://aboutstays.s3.amazonaws.com/images/room_cleaning.png"),
	MINIBAR_REFILL(5,"Minibar Refill","https://aboutstays.s3.amazonaws.com/images/minibar.png");
	
	private int code;
	private String displayName;
	private String imageUrl;
	
	public int getCode() {
		return code;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	private HousekeepingItemType(int code, String displayName, String imageUrl) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
	}
	
	public static HousekeepingItemType getByCode(Integer code){
		if(code==null)
			return null;
		for(HousekeepingItemType itemType : HousekeepingItemType.values()){
			if(itemType.code==code)
				return itemType;
		}
		return null;
	}
	
	public static List<Integer> getCodeList(){
		List<Integer> codeList = new ArrayList<>(MenuItemType.values().length);
		for(HousekeepingItemType itemType : HousekeepingItemType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}
	
	public static String getNameByCode(int code){
		for(HousekeepingItemType itemType : HousekeepingItemType.values()){
			if(itemType.getCode() == code){
				return itemType.getDisplayName();
			}
		}
		return null;
	}

}
