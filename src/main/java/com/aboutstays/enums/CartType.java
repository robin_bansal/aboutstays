package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum CartType {

	FOOD_ITEM(1), HOUSEKEEPING(2), LAUNDRY(3), INTERNET(4);
	
	private int code;
	
	public int getCode() {
		return code;
	}

	private CartType(int code) {
		this.code = code;
	}
	
	public static CartType getByCode(Integer code){
		if(code==null)
			return null;
		for(CartType cartType : CartType.values()){
			if(cartType.code==code){
				return cartType;
			}
		}
		return null;
	}

	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for(CartType cartType : CartType.values()){
			codeList.add(cartType.code);
		}
		return codeList;
	}
}
