package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum PlaceType {

	CONTINENT(1),
	COUNTRY(2),
	CITY(3);
	
	private int type;
	
	public int getType() {
		return type;
	}

	private PlaceType(int type) {
		this.type = type;
	}
	
	public static PlaceType findByType(Integer type){
		if(type==null)
			return null;
		for(PlaceType placeType : PlaceType.values()){
			if(placeType.type==type)
				return placeType;
		}
		return null;
	}

	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<>();
		for(PlaceType placeType : PlaceType.values()){
			typeList.add(placeType.type);
		}
		return typeList;
	}
}
