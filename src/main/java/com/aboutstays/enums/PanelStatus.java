package com.aboutstays.enums;

public enum PanelStatus {
	SCHEDULED(1,"Scheduled"),
	ONGOING(2,"Ongoing"),
	COMPLETED(3,"Completed"),
	PENDING(4,"Pending");
	private int code;
	private String type;
	private PanelStatus(int code, String type) {
		this.code = code;
		this.type = type;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
