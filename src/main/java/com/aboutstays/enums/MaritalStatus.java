package com.aboutstays.enums;

public enum MaritalStatus {
	
	SINGLE(1), MARRIED(2), ENGAGED(3);
	
	private Integer code;

	private MaritalStatus(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public static MaritalStatus findStatusByCode(Integer value){
		for(MaritalStatus status : MaritalStatus.values()){
			if(status.code == value){
				return status;
			}
		}
		return null;
	}
	
	public Integer getDefaultStatus(){
		return SINGLE.code;
	}

}
