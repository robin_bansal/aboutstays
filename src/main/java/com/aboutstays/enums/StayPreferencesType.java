package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

public enum StayPreferencesType {
	
	QUIET_ROOM(1, "Quiet Room", "https://aboutstays.s3.amazonaws.com/images/Quietroom.png"), 
	CLOSE_TO_ELEVATOR(2, "Close to Elevator", "https://aboutstays.s3.amazonaws.com/images/ClosertoElevator.png"), 
	NON_SMOKING(3, "Non Smoking", "https://aboutstays.s3.amazonaws.com/images/NonSmoking.png"), 
	SMOKING(4, "Smoking", "https://aboutstays.s3.amazonaws.com/images/Smoking.png"), 
	WITH_BALCONY(5, "With Balcony", "https://aboutstays.s3.amazonaws.com/images/WithBalcony.png"), 
	WITH_BATH(6, "With Bath Tub", "https://aboutstays.s3.amazonaws.com/images/Withbath.png"),
	HIGHER_FLOORS(7, "Higher Floors", "https://aboutstays.s3.amazonaws.com/images/HigherFloors.png"), 
	LOWER_FLOORS(8, "Lower Floors", "https://aboutstays.s3.amazonaws.com/images/Lower%20Floors.png"), 
	TWIN_BEDS(9, "Twin Beds", "https://aboutstays.s3.amazonaws.com/images/TwinBeds.png"), 
	EXTERNAL_VIEW(10, "External View", "https://aboutstays.s3.amazonaws.com/images/ExternalView.png"),
	CONNECTING_ROOM(13, "Connecting Rooms", "https://aboutstays.s3.amazonaws.com/images/Connectingroom.png"),
	CITY_VIEW(12, "City View", "https://aboutstays.s3.amazonaws.com/images/city_view"),
	MOUNTAIN_VIEW(14, "Mountain View", "https://aboutstays.s3.amazonaws.com/images/MountainView.png"),
	BEACH_VIEW(11, "Beach View", "https://aboutstays.s3.amazonaws.com/images/beach_view");
	
	
	private Integer code;
	private String displayName;
	private String imageUrl;

	private StayPreferencesType(Integer code, String displayName, String imageUrl) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
	}

	public Integer getCode() {
		return code;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public static StayPreferencesType findStatusByCode(Integer value){
		if(value == null) {
			return null;
		}
		for(StayPreferencesType status : StayPreferencesType.values()){
			if(status.code == value){
				return status;
			}
		}
		return null;
	}
	
	public static StayPreferencesType getByDisplayName(String value){
		if(StringUtils.isEmpty(value)) {
			return null;
		}
		for(StayPreferencesType status : StayPreferencesType.values()){
			if(status.displayName.equalsIgnoreCase(value)){
				return status;
			}
		}
		return null;
	}
	
	public static List<Integer> getCodeList(){
		List<Integer> codeList = new ArrayList<>(StayPreferencesType.values().length);
		for(StayPreferencesType itemType : StayPreferencesType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}

}
