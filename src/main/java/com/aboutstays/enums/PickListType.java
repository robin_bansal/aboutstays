package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum PickListType {

	GUEST_SENSE_CATEGORY(1),
	PRIMARY_CUISINE(2),
	MESSAGE_FIELDS(3),
	STAR_RATINGS(4),
	MARITAL_STATUS(5),
	TITLE(6),
	DISCOUNT_TYPE(7),
	NOTIFICATION_TYPE(8),
	GUEST_SENSE(9),
	;
	
	private int code;

	public int getCode() {
		return code;
	}
	
	private PickListType(int code) {
		this.code = code;
	}
	
	public static PickListType findByCode(Integer code){
		if(code==null)
			return null;
		for(PickListType pickListType : PickListType.values()){
			if(pickListType.code==code){
				return pickListType;
			}
		}
		return null;
	}

	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for(PickListType pickListType : PickListType.values()){
			codeList.add(pickListType.code);
		}
		return codeList;
	}
	
}
