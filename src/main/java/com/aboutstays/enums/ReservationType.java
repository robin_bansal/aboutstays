package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum ReservationType {
	
	RESTAURANTS_AND_CUISINES(1, "Restaurants & Cuisines", "https://aboutstays.s3.amazonaws.com/images/Restaurantscuisines.png", true),
	HEALTH_AND_BEAUTY(2, "Health & Beauty", "https://aboutstays.s3.amazonaws.com/images/Healthbeauty.png", false),
	SPORTS_AND_ENTERTAINMENT(3, "Sports & Entertainment", "https://aboutstays.s3.amazonaws.com/images/sports.png", true),
	BUSINESS_AND_PARTIES(4, "Business & Parties", "https://aboutstays.s3.amazonaws.com/images/Businessparties.png", true),
	SHOPS_AND_STORES(5, "Shops & Stores","https://aboutstays.s3.amazonaws.com/images/shopes.png", true);
	
	private int code;
	private String displayName;
	private String imageUrl;
	private boolean imageListing;
	
	public int getCode() {
		return code;
	}
	public String getDisplayName() {
		return displayName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public boolean isImageListing() {
		return imageListing;
	}
	private ReservationType(int code, String displayName, String imageUrl, boolean imageListing) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
		this.imageListing = imageListing;
	}
	
	
	public static ReservationType getByCode(Integer code){
		if(code==null)
			return null;
		for(ReservationType itemType : ReservationType.values()){
			if(itemType.code==code)
				return itemType;
		}
		return null;
	}
	
	public static List<Integer> getCodeList(){
		List<Integer> codeList = new ArrayList<>(MenuItemType.values().length);
		for(ReservationType itemType : ReservationType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}
	
}
