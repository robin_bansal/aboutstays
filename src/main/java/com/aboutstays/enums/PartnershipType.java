package com.aboutstays.enums;

import com.aboutstays.entities.GeneralService;

public enum PartnershipType {
	
	NOT_PARTNER(0), PARTNER(1);
	
	private Integer code;

	private PartnershipType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public static PartnershipType findValueByCode(Integer value){
		for(PartnershipType type : PartnershipType.values()){
			if(type.getCode().equals(value)){
				return type;
			}
		}
		return null;
	}
	
	public static Integer getDefaultParternshipType(){
		return NOT_PARTNER.code;
	}

	public static boolean isValid(Integer partnershipTypeValue, GeneralService generalService) {
		PartnershipType partnershipType = findValueByCode(partnershipTypeValue);
		if(partnershipType==null){
			partnershipType = PartnershipType.NOT_PARTNER;
		}
		if(PartnershipType.NOT_PARTNER.equals(partnershipType)){
			return generalService.isSupportsNonPartneredHotel();
		}
		return true;
	}
	
	

}
