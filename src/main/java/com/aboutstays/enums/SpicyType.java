package com.aboutstays.enums;

public enum SpicyType {

	NON_SPICY(1), LESS_SPICY(2), SPICY(3), MORE_SPLICY(4), NONE(5);
	
	private Integer code;

	private SpicyType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public SpicyType findStatusByCode(Integer value){
		for(SpicyType status : SpicyType.values()){
			if(status.code == value){
				return status;
			}
		}
		return null;
	}
	
	
}
