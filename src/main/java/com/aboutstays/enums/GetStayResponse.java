package com.aboutstays.enums;

import com.aboutstays.pojos.StaysPojo;

public class GetStayResponse extends StaysPojo{
	
	private int staysOrdering;
	
	public int getStaysOrdering() {
		return staysOrdering;
	}
	
	public void setStaysOrdering(int staysOrdering) {
		this.staysOrdering = staysOrdering;
	}
	
}
