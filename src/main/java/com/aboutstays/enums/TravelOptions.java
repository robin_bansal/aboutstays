package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum TravelOptions {
	
	PRIVATE_CAR(1), 
	SHUTTLE(2),
	AUTO(3),
	CITY_BUS(4),
	BUS(5),
	TRAIN(6);
	
	
	private Integer code;
	
	public Integer getCode() {
		return code;
	}
	
	private TravelOptions(Integer code) {
		this.code = code;
	}
	
	public static TravelOptions getByCode(Integer code){
		if(code == null){
			return null;
		}
		for(TravelOptions options : TravelOptions.values()){
			if(options.code == code){
				return options;
			}
		}
		return null;
	}
	
	public List<Integer> getTypeList(){
		List<Integer> optionList = new ArrayList<>();
		for(TravelOptions options : TravelOptions.values()){
			optionList.add(options.code);
		}
		return optionList;
	}
	
	

}
