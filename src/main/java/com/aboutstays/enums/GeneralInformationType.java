package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum GeneralInformationType {
	
	HOTEL_INFO(1, "Hotel Info"),
	IN_HOTEL_NAVIGATION(2, "In Hotel Navigation"),
	HOUSE_RULES(3, "House Rules"),
	TV_ENTERTAINMENT(4, "Tv/Entertainment"),
	CITY_GUIDE(5, "City Guide"),
	COMPLEMENTRY_SERVICE(6, "Complementry Service");
	
	private Integer code;
	private String message;
	
	private GeneralInformationType(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	public static GeneralInformationType findGeneralInfoByType(Integer value){
		for(GeneralInformationType type : GeneralInformationType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
	
	public static List<Integer> getTypeList(){
		List<Integer> typeList = new ArrayList<>();
		for(GeneralInformationType type : GeneralInformationType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}

	public static String getNameFromServiceCode(Integer code){
		for(GeneralInformationType type : GeneralInformationType.values()){
			if(type.code == code){
				return type.getMessage();
			}
		}
		return null;
	}

}
