package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum CityGuideItemType {
	
	LANDMARKS(1, "Landmarks", "https://aboutstays.s3.amazonaws.com/images/landmark.png"),
	MUSEUMS(2, "Museums", "https://aboutstays.s3.amazonaws.com/images/museum.png"),
	NATURE_AND_PARKS(3, "Nature & Parks", "https://aboutstays.s3.amazonaws.com/images/nature.png"),
	ADVENTURE(4, "Adventure", "https://aboutstays.s3.amazonaws.com/images/adventure.png"),
	SHOPPING(5, "Shopping", "https://aboutstays.s3.amazonaws.com/images/shopping.png"),
	NIGHTLIFE(6, "Nightlife", "https://aboutstays.s3.amazonaws.com/images/nightlife.png");
	
	private int code;
	private String displayName;
	private String imageUrl;
	
	public int getCode() {
		return code;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	private CityGuideItemType(int code, String displayName, String imageUrl) {
		this.code = code;
		this.displayName = displayName;
		this.imageUrl = imageUrl;
	}
	
	public static CityGuideItemType getByCode(Integer code){
		if(code==null)
			return null;
		for(CityGuideItemType itemType : CityGuideItemType.values()){
			if(itemType.code==code)
				return itemType;
		}
		return null;
	}
	
	public static List<Integer> getCodeList(){
		List<Integer> codeList = new ArrayList<>(MenuItemType.values().length);
		for(CityGuideItemType itemType : CityGuideItemType.values()){
			codeList.add(itemType.code);
		}
		return codeList;
	}

}
