package com.aboutstays.enums;

public enum RequestStatus {
	
	SCHEDULED(1), PROCESSING(2), COMPLETED(3), CANCELLED(4);
	
	private  Integer code;

	private RequestStatus(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public static RequestStatus findValueByType(Integer value){
		for(RequestStatus type : RequestStatus.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}
	

}
