package com.aboutstays.enums;

public enum CheckinRequestStatus {

	USER_INITIATED(1, "INITIATED"), SYSTEM_INITIATED(2, "PROCESSING"), USER_MODIFIED(3, "UPDATED"), SYSTEM_MODIFIED(4, "UPDATED"), APPROVED(5, "COMPLETED"),
	DECLINED(6, "declined");
	
	private int code;
	private String displayName;
	
	public int getCode() {
		return code;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	private CheckinRequestStatus(int code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	public static CheckinRequestStatus findByCode(Integer code){
		if(code!=null){
			for(CheckinRequestStatus checkinRequestStatus : CheckinRequestStatus.values()){
				if(checkinRequestStatus.code==code)
					return checkinRequestStatus;
			}
		}
		return null;
	}
	
	public static RequestDashboardStatus getByOrderStatus(CheckinRequestStatus generalOrderStatus) {
		if(generalOrderStatus==null)
			return null;
		if(CheckinRequestStatus.USER_MODIFIED.equals(generalOrderStatus)||CheckinRequestStatus.SYSTEM_MODIFIED.equals(generalOrderStatus)||
				CheckinRequestStatus.USER_INITIATED.equals(generalOrderStatus)||CheckinRequestStatus.SYSTEM_INITIATED.equals(generalOrderStatus)){
			return RequestDashboardStatus.OPEN;
		}
		if(CheckinRequestStatus.APPROVED.equals(generalOrderStatus)){
			return RequestDashboardStatus.CLOSED;
		}
		return null;
	}
	
	public static GeneralOrderStatus getGeneralOrderStatus(CheckinRequestStatus generalOrderStatus) {
		if(generalOrderStatus==null)
			return null;
		if(CheckinRequestStatus.USER_MODIFIED.equals(generalOrderStatus)||CheckinRequestStatus.SYSTEM_MODIFIED.equals(generalOrderStatus)){
			return GeneralOrderStatus.UPDATED;
		}
		if(CheckinRequestStatus.APPROVED.equals(generalOrderStatus)){
			return GeneralOrderStatus.COMPLETED;
		}
		if(CheckinRequestStatus.USER_INITIATED.equals(generalOrderStatus)||CheckinRequestStatus.SYSTEM_INITIATED.equals(generalOrderStatus)){
			return GeneralOrderStatus.PROCESSING;
		}
		if(CheckinRequestStatus.DECLINED.equals(generalOrderStatus)) {
			return GeneralOrderStatus.CANCELLED;
		}
		return null;
	}
	
	
}
