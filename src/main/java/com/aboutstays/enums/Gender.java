package com.aboutstays.enums;

public enum Gender {
	
	MALE(1), FEMALE(2), OTHER(3);
	
	private Integer code;

	private Gender(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public Gender findStatusByCode(Integer value){
		for(Gender status : Gender.values()){
			if(status.code == value){
				return status;
			}
		}
		return null;
	}

}
