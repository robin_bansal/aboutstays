package com.aboutstays.enums;

import com.aboutstays.utils.DateUtil;

public enum AboutstaysResponseCode {
	//Request success
	SUCCESS("SUCCESS","0"),
	
	//Request failed
	FAILED("FAILED","0"),
	
	//Common
	INTERENAL_SERVER_ERROR("Internal server error!","500"),
	EMPTY_USERNAME("Username cannot be empty","EMPTY_USER"),
	EMPTY_PASSWORD("Password cannot be empty","EMP_PWD"),
	NO_TITLES_FOUND("No profile titles found!","NF_TITLES"),
	INVALID_SLA_TIME("Sla time must be greater than 0 minute!","INV_SLA"),
	INVALID_START_TIME("Start time value must be in the format : "+DateUtil.MENU_ITEM_TIME_FORMAT,"INV_START_TIME"),
	INVALID_END_TIME("End time value must be in the format : "+DateUtil.MENU_ITEM_TIME_FORMAT,"INV_END_TIME"),
	
	//Signup related
	SUCCESS_SIGNUP("Registered Successfully!","0"),
	INVALID_SIGNUP_REQUEST("Invalid signup request!","INV_REQ"),
	DUPLICATE_CONTACT("Contact number already exists","DUP_CONTACT"),
	EMPTY_FIRST_NAME("First name cannot be empty!","EMP_FIRST_NAME"),
	EMPTY_LAST_NAME("Last name cannot be empty!","EMP_LAST_NAME"),
	INVALID_GENDER("Please provide valid gender info!","INV_SEX"), 
	EMPTY_NUMBER("Phone number cannot be empty","EMP_NUM"),
	EMPTY_EMAIL("email id can not be empty!", "EMP_EMAIL"),
	DUPLICATE_EMAIL("email id already exists", "DUP_EMAIL"),
	INVALID_SIGNUP_TYPE("Sign up type can only be of " + SignupType.getTypeList(), "INV_TYPE"),
	SUCCESS_SIGNUP_VALIDATION("User details are valid", "0"),
	
	//login related
	SUCCESS_LOGIN("Successfully loged in", "0"),
	INVALID_LOGIN_REQUEST("Invalid login request!", "INV_LOGIN"),
	INVALID_USER("Invalid email or password", "INV_CRED"),
	NO_ACCOUNT("Account does not exists", "INV_ACC"),
	INV_PASS("Wrong password. Please try again", "INV_PASS"),
	INVALID_LOGIN_TYPE("login type can only be of " + LoginType.getTypeList().toString(), "INV_LOGIN_TYPE"), 
	
	// otp validation related
	EMPTY_TYPE("Type is empty", "EMP_TYPE"),
	INVALID_TYPE("Type is invalid", "INV_TYPE"),
	EMPTY_VALUE("Value is empty", "EMP_VAL"),
	EMPTY_VALIDATED("Otp validated field is empty", "EMP_FLAG"),
	VERIFICATION_FAILED("Otp verification failed", "VER_FAIL"),
	INVALID_REQUEST("Invalid otp verification request", "INV_REQ"),
	
	// update user request
	INVALID_UPDATE_REQUEST("Invalid update request", "INV_UPD"),
	EMPTY_ID("User id can not be empty", "EMP_ID"),
	NO_USER("User does not exists", "NO_EXISTS"),
	UPDATED("Successfully updated", "SUC_UPD"),
	EMPTY_USER_ID("user id can not be empty for adding stays", "EMP_USER_ID"),
	UPDATE_USER_FAILED("Could not update user details with current request", "UPD_FAILED"),
	INVALID_DOCUMENT_TYPE("Accepted identity document type can only be of " + DocumentType.getTypeList().toString(), "INV_DOC_TYPE"),
	NO_USER_WITH_EMAIL("No registered user found with this Email Id","NO_USR"),
	NO_USER_WITH_PHONE("No registered user found with this phone number","NO_USR"),
	
	//Hotel related
	INVALID_ADD_HOTEL_REQUEST("Invalid create hotel request", "INV_ADD_REQ"),
	HOTEL_CREATION_FAILED("Hotel creation failed!", "ADD_FAIL"),
	HOTEL_ADDED_SUCCESSFULLY("Hotel added successfully!","0"),
	HOTEL_NOT_FOUND("Hotel not found!","NO_HOTEL"),
	NO_ROOM_REGISTERED("No room registered with this hotel yet!","NO_ROOM"),
	INVALID_HOTEL_PARTNER_TYPE("This is not a valid partership type parameter", "INV_PTR"),
	INVALID_PARTNERSHIP_REQUEST("Updataing hotel partership request can not be empty", "INV_REQ"),
	PARTNERSHIP_UPDATION_FAILED("could not update partnership type with current request", "UPD_FAIL"),
	EMPTY_LIST_AND_MAP_GSIDS("List GSID and Map GsID VS HS can not be empty", "EMP_LIST_AND_MAP"),

	
	//RoomCategory Related
	INVALID_ADD_ROOM_CATEGORY_REQUEST("Invalid add room category request!","INV_ADD_ROOM_CAT"), 
	EMPTY_ROOM_CATEGORY_NAME("Room Category Name cannot be null or empty!","EMP_ROOM_CAT_NAME"),
	EMPTY_ROOM_CATEGORY_ID("Room Category Id cannot be null or empty!","EMP_ROOM_CAT_ID"),
	ROOM_CATEGORY_ADDED_SUCCESSFULLY("Room category added successfully!","0"), 
	NO_ROOM_CATEGORY("No room category added yet!","NO_ROOM_CAT"), 
	ROOM_CATEGORY_NOT_FOUND("Room category not found!","NF_ROOM_CAT"), 
	UPDATE_FAILED_ROOM_CATEGORY("failed to update!","UP_FAIL_ROOM_CAT"), 
	ROOM_CATEGORY_DELETED_SUCCESSFULLY("Room category Deleted Successfully!","0"),
	
	//Room Related
	INVALID_ADD_ROOM_REQUEST("Invalid add room request!","INV_ADD_ROOM"), 
	EMPTY_ROOM_ID("Room Id can not be empty", "EMP_ROOM_ID"),
	EMPTY_ROOM_NAME("Room Name cannot be null or empty!","EMP_ROOM_NAME"),
	EMPTY_ROOM_NUMBER("Room Number cannot be null or empty!","EMP_ROOM_NO"),
	ROOM_ADDED_SUCCESSFULLY("Room added successfully!","0"), 
	NO_ROOMS("No rooms added yet!","NO_ROOM"), 
	ROOM_NOT_FOUND("Room not found!","NF_ROOM"), 
	UPDATE_FAIL_ROOM("Fail to update room request!","UP_FAIL_ROOM"),
	ROOM_DELETED_SUCCESSFULLY("Room Deleted Successfully!","0"),
	ROOMS_DELETED_SUCCESSFULLY("Rooms deleted successfully!","0"),
	ROOM_WITH_ID_NOT_FOUND("Room with id %s not found!","NF_ROOM"),
	EMPTY_ROOM_NUMBER_WITH_ID("Room Number with id %s cannot be null or empty!","EMP_ROOM_NO"),
	EMPTY_ROOM_CATEGORYID_ID_WITH_ID("Room category id for room id %s cannot be empty!","EMP_RC_ID"),
	ROOM_WITH_NO_UPDATED_SUCCESSFULLY("Room with number %s updated successfully!","0"),
	ROOMS_UPDATED_SUCCESSFULLY("All rooms updated successfully!","0"),
	SOME_ROOMS_NOT_UPDATED("Some rooms not updated!","FAILED"),
	
	// get hotel request
	INVALID_HOTEL_TYPE("Hotel type can be out of "+HotelType.getTypeList().toString(), "INV_H_TYPE"),
	INVALID_HOTEL_ID("Hotel Id is not valid", "INV_HOTEL_ID"),
	GET_HOTEL_FAILED("Could not load any hotel with current request", "GET_HTL_FAIL"),
	INVALID_GET_HOTEL_REQUEST("could not fetch hotels with null inputs", "NO_QUERY"),
	NO_CITY_AND_NAME("can not fetch hotels if both city and name of hotel is empty", "NO_INPUT"),
	
	
	// update hotel by type
	INVALID_HOTEL_UPDATE_TYPE("Can not update with current type request", "INV_HTL_TYPE"),
	UPDATE_HOTEL_FAILED("Hotel updation failed", "HTL_FAIL"),
	EMPTY_GENERAL_INFO("Hotel general information can not be empty", "EMP_GEN_INFO"),
	DELETE_HOTEL_FAILED("Hotel could not be deleted", "DEL_FAIL"),
	EMPTY_HOTEL_NAME("Hotel name can not be empty", "EMP_HTL_NAME"),
	EMPTY_CHECKIN_TIME("Hotel check in time can not be empty", "EMP_CHKIN_TIME"),
	EMPTY_CHECKOUT_TIME("Hotel check out time can not be empty", "EMP_CHKOUT_TIME"),
	
	//create booking
	INVALID_BOOKING_REQUEST("Invalid create booking request", "INV_BOOK_REQ"),
	EMPTY_HOTEL_ID("Hotel Id can not be empty", "EMP_HTL_ID"),
	EMPTY_RESERVATION_NUMBER("Booking reservation number can not be empty", "EMP_RES_NUM"),
	EMPTY_CHECKIN_DATE("Check In date can not be empty", "EMP_CHK_IN"),
	RESERVATION_EXISTS("Reservation already exists", "DUP_RES"),
	BOOKING_CREATI0N_FAILED("Booking creation failed", "BOOKING_FAIL"),
	INVALID_GET_BOOKINGS_REQUEST("Invalid get bookings request", "INV_BOOK_REQ"),
	INVALID_GET_BOOKING_FIELDS("Both reservation number and check In date can not be empty", "INV_BOOK_FILEDS"),
	NO_BOOKINGS("Could not get any bookings", "NO_BKGS"),
	BOOKING_NOT_FOUND("Booking not found!", "NF_BKG"),
	BOOKING_NOT_FOUND_WITH_ID("Booking not found with this id %s !", "NF_BKG"),
	NO_BOOKINGS_EXISTS("There are no bookings with current booking id", "NO_EXISTS_BKG"),
	INVALID_DATE_FORMAT("Allowed date format is " + DateUtil.BASE_DATE_FORMAT, "INV_DATE_FORMAT"),
	INVALID_TIME_FORMAT("Allowed time format is " + DateUtil.BASE_TIME_FORMAT, "INV_TIME_FORMAT"),
	INVALID_DATE_AND_TIME_FORMAT("Allowed date format is " + DateUtil.BASE_DATE_FORMAT + " and Time foramt is " + DateUtil.BASE_TIME_FORMAT, "INV_DATE_TIME_FORMAT"),
	EMPTY_CHECKOUT_DATE("Check out date can not be empty", "EMP_CHKOUT_DATE"),
	INVALID_DELETE_REQUEST("Invalid delete booking request. It can not be null", "EMP_DEL_REQ"),
	EMPTY_ID_AND_RESERVATION("Both reservation number and booking Id can not be empty for booking deletion", "EMP_ID_AND_RES"),
	DELETION_FAILED("Could not delete bookings", "DEL_FAIL"),
	INVALID_UPDATE_BOOKING_REQUEST("Update request can not be null", "INV_UPD_REQ"),
	BOOKING_UPDATION_FAILED("Could not update booking with current request", "UPD_BOOKING_FAILED"),
	
	// stays related
	INVALID_ADD_STAY_REQUEST("This request is invalid for adding any stay", "INV_ADD_STAY"),
	EMPTY_BOOKING_ID("Booking id can not be empty for adding stays", "EMP_BOOK_ID"),
	EMPTY_CHECK_IN_TIME("Check in time can not be empty for adding stays", "EMP_CHECKIN_TIME"),
	EMPTY_CHECK_OUT_TIME("Chcekc out time can not be empty for adding stays", "EMP_CHECKOUT_TIME"),
	ADD_STAYS_FAILED("could not add stays for current request", "ADD_FAIL"),
	EMPTY_STAYS_ID("Stays id can not be empty for deleting any stays", "EMP_STAYS_ID"),
	NO_STAYS_EXISTS("Stays does not exists with current stays Id", "NO_STAYS"),
	DELETE_STAYS_FAILED("Could not delete stays with current request", "DEL_FAILED"),
	GET_STAYS_FAILED("Could not get stays with current request", "GET_STAYS_FAILED"),
	STAY_ALREADY_ADDED("Stays with this booking is already added","DUP_STAY"),
	GET_ALL_STAYS_OF_USER_FAILED("Could not get all stays of requested user", "ALL_STAYS_OF_USER_FAIL"),
	SAME_STAY_ALREADY_ADDED("You have already added this stay", "SAME_STAY_ADDED"),
	OTHER_USER_ALREADY_ADDED_STAY("Some other user have already added this stay", "SAME_STAY_OTHER_USER"),
	INVALID_EXISTING_STAYS_GET_REQUEST("Invalid existing stays get request!","INV_REQ"),
	NO_ADDED_STAYS_FOR_USER("No stays added for this user!","NO_STAYS"),
	STAYS_NOT_FOUND("Stays not found!","NF_STAYS"),
	EARLY_CHECKIN_APPROVED("Early checkin approved!","0"), 
	EARLY_CHECKIN_REJECTED("Early checkin rejected!","0"),
	
	// Service related
	INVALID_ADD_SERVICE_REQUEST("This is a invalid request for service addition", "INV_ADD_REQ"),
	EMPTY_STAYS_ID_FOR_SERVICE("Stays Id can not be empty for service addition", "EMP_STAYS_ID"),
	EMPTY_USER_ID_FOR_SERVICE("User Id can not be empty for service addition", "EMP_USER_ID"),
	INVALID_STAY_STATUS("Stays status can only be one of the " + StaysStatus.getTypeList().toString(), "INV_STAY_STATUS"),
	EMPTY_CHECKIN_INFORMATION("CheckIn information can not be empty", "EMP_CHECKIN_INFO"),
	EMPTY_CHECKIN_SERVICES("CheckIn services can not be empty", "EMP_CHECKIN_SERVICES"),
	EMPTY_STAY_SERVICES("Stay services can not be empty", "EMP_STAY_SERVICES"),
	EMPTY_STAY_INFORMATION("stay information can not be empty", "EMP_STAY_INFO"),
	EMPTY_CHECKOUT_INFORMATION("check out information can not be empty", "EMP_CHECKOUT_INFO"),
	EMPTY_CHECKOUT_SERVICES("check out services can not be empty", "EMP_CHECKOUT_SERVICES"),
	
	// File upload related
	INVALID_FILE("Current file for upload is not valid", "INV_FILE_PATH"),
	INVALID_FILE_UPLOAD_REQUEST_SIZE("Number of files must be equal to number of names provided in the request", "INV_UPD_REQ"), 
	
	DOCUMENT_TYPE_EXISTS("Document type already exists","DUP_DOC_TYPE"), 
	DOCUMENTS_UPDATION_FAILED("could not update documents with current request", "UPD_DOC_FAILED"), 
	DOCUMENT_UPLOAD_FAILED("could not upload current document", "DOC_FAILED"),
	IMAGE_UPLOAD_FAILED("could not upload selected image", "IMG_UPD_FAILED"), 
	IMAGE_UPDATE_FAILED("could not update image with current userId", "IMG_UPDATE_FAIL"),
	SERVICES_NOT_FOUND("could not get any services with current hotel", "NO_SERVICES"), 
	
	// General Services related
	INVALID_GENERAL_SERVICE_REQUEST("Could not add general sevice with current request", "INV_GEN_SER_REQ"), 
	EMPTY_ACTIVATED_IMAGE_URL("Activated image url can not be empty", "EMP_URL"),
	EMPTY_UNACTIVE_IMAGE_URL("Unactive image url can not be empty", "EMP_URL"),
	EMPTY_GENERAL_SERVICE_TYPE("General service type can not be empty", "EMP_TYPE"),
	EMPTY_GENERAL_SERVICE_STATUS("Status can not be empty", "EMP_STATUS"),
	INVALID_GENERAL_SERVICE_TYPE("General service can only be of " + GeneralServiceType.getTypeList().toString(), "INV_SER_TYPE"),
	ADD_GENERAL_SERVICE_FAILED("Adding general services failed", "ADD_GS_FAILED"), 
	GET_GENERAL_SERVICES_FAILED("Could not get general services with current request", "GET_SERV_FAIL"), 
	GENERAL_SERVICE_NOT_EXISTS("Current general service does not exists", "GS_NOT_EXISTS"), 
	GS_DELETE_FAILED("could not delete current service", "DEL_GS_FAILED"), 
	EMPTY_GS_ID("General service id can not be empty", "EMPTY_GS_ID"), 
	UPDATE_GS_FAILED("Could not update General service with current request", "UPD_GS_FAIL"), 
	
	// Airport Services
	INVALID_ADD_AIRPORT_SERVICE_REQUEST("Airport service request cannot be null!","INV_REQ"),
	EMPTY_AIRPORT_VS_CAR_MAP("Airport vs car details map can not be empty", "EMP_MAP"), 
	ADDING_AIRPORT_SERVICE_FAILED("Could not add airport service with current request", "ADD_AS_FAILED"), 
	GET_ALL_AIRPORT_SERVICE_FAILED("Could not get all airport service", "GET_ALL_AS_FAILED"), 
	AIRPORT_SERVICE_NOT_EXISTS("Airpot service does not exists", "AS_NOT_EXISTS"), 
	AS_DELETE_FAILED("Could not delete airport service with current request", "AS_DEL_FAILED"), 
	EMPTY_AIRPORT_SERVICE_ID("Airport service id(equal to hotel id) can not be empty", "EMPTY_AS_ID"), 
	UPDATE_AIRPORT_SERVICE_FAILED("Could not udpdate airport service with current request", "UPD_AS_FAILED"), 
	AIRPORT_SERVICE_SAME_CAR_TYPE("Same type of car can not be added for a particular airport service", "SAME_TYPE_CAR"), 
	INVALID_CAR_TYPE("Car type can only be one of " + CarType.getTypeList().toString(), "INV_CAR_TYPE"), 
	EMPTY_CAR_TYPE("Car type can not be empty", "EMP_CAR_TYPE"), 
	
	// Driver
	INVALID_ADD_DRIVER_REQUEST("Add driver request is invalid", "INV_DRV_REQ"), 
	EMPTY_DRIVER_MOBILE_NUMBER("Driver contact number can not be empty", "EMP_DRV_NUMBER"), 
	EMPTY_DRIVER_NAME("Driver name can not be empty", "EMP_DRV_NAME"), 
	ADD_DRIVER_FAILED("Add Driver failed for current request", "ADD_DRV_FAILED"), 
	GET_ALL_DRIVERS_FAILED("Get all drivers failed", "GET_ALL_DRV_FAILED"), 
	DRIVER_NOT_EXISTS("Driver does not exists with current request", "DRV_NO_EXISTS"), 
	DRIVER_DELETE_FAILED("Driver deletion failed with current request", "DRV_FAILED"),
	EMPTY_DRIVER_ID("Driver id can not be empty", "EMP_DRV_ID"), 
	UPDATE_DRIVER_FAILED("Driver updataion failed with current request", "DRV_UPD_FAILED"), 
	
	// Car 
	INVALID_ADD_CAR_REQUEST("Add car request is invalid", "INV_CAR_REQ"), 
	EMPTY_CAR_NUMBER_PLATE("Car number plate can not be empty", "EMP_CAR_NUMBER"), 
	ADD_CAR_FAILED("Add CAR failed for current request", "ADD_CAR_FAILED"), 
	GET_ALL_CARS_FAILED("Get all cars failed", "GET_ALL_CAR_FAILED"), 
	CAR_NOT_EXISTS("Car does not exists with current request", "CAR_NO_EXISTS"), 
	CAR_DELETE_FAILED("Car deletion failed with current request", "CAR_DEL_FAILED"), 
	EMPTY_CAR_ID("Car id can not be empty for updation", "EMP_CAR_ID"), 
	UPDATE_CAR_FAILED("Car update failed with current request", "CAR_UPD_FAILED"), 
	NUMBER_PLATE_ALREADY_EXISTS("Car with same number plate already exists", "DUP_NUM_PLATE"), 
	
	// Airport pickup
	INVALID_AIRPORT_PICKUP_REQUEST("Add airport pickup request is invalid", "INV_AIR_PICK_REQ"), 
	EMPTY_AIRLINE_NAME("Airline name can not be empty", "EMP_AIRLINE_NAME"), 
	EMPTY_FLIGHT_NUMBER("Flight number can not be empty", "EMP_FLIGHT_NUM"), 
	EMPTY_PICKUP_DATE_TIME("Pickup date and time can not be empty", "EMP_DATE_TIME"), 
	GET_ALL_AIRPORT_PICKUPS_FAILED("Get all airport pickups failed for current request", "ALL_AP_FAILED"), 
	AIRPORT_PICKUP_NOT_EXISTS("Airport pickup does not exists with current request", "AP_NO_EXISTS"), 
	EMPTY_AIRPORT_PICKUP_ID("Airport pickup id can not be empty for updation", "EMP_AP_ID"), 
	AIRPORT_PICKUP_CANCEL_FAILED("Could not cancel airport pickup for current request", "AP_CANCEL_FAIL"),
	
	// User opted services
	INVALID_ADD_UOS_REQUEST("Add user opted service request failed", "INV_UOS_REQ"), 
	EMPTY_REF_ID("Ref id can not be empty for adding user opted service", "EMP_REF_ID"), 
	INVALID_REF_COLLECTION_TYPE("Ref collection type can only be of " + RefCollectionType.getTypeList().toString(), "INV_REF_COLL_TYPE"),
	EMPTY_REF_COLLECTION_TYPE("Ref collection type can not be empty", "EMP_REFCOLL_TYPE"), 
	ADD_USER_OPTED_SERVICE_FAILED("Add user opted service failed", "ADD_UOS_FAILED"), 
	GET_ALL_USER_OPTED_SERVICE_FAILED("Get all user opted service failed", "GET_ALL_UOS_FAILED"), 
	USER_OPTED_SERVICE_NOT_EXISTS("Request current opted service does not exists", "UOS_NO_EXISTS"), 
	EMPTY_USER_OPTED_SERVICE_ID("user opted service id can note be empty", "EMP_UOS_ID"), 
	UPDATE_USER_OPTED_SERVICE_FAILED("User opted service update failed", "UPD_UOS_FAILED"), 
	USER_OPTED_SERVICE_DELETE_FAILED("User opted service deletion failed for current request", "UOS_DEL_FAILED"), 
	AIRPORT_PICKUP_DELETE_FAILED("Airport pickup deletion failed for current request", "AP_DEL_FAILED"),
	ADD_AIRPORT_PICKUP_FAILED("Add airport pickup failed for current request", "ADD_AP_FAILED"), 
	INVALID_APPROVE_PICKUP_REQUEST("Invalid approve pickup request", "INV_APQ"), 
	UPDATE_AIRPORT_PICKUP_FAILED("Updation airport pickup failed for current request", "UPD_AP_FAIL"), 
	APPROVE_PICKUP_UPDATE_FAILED("Approve pickup update failed", "APQ_FAIL"), 
	INVALID_PICKUP_STATUS("Airport pickup status can only be of " + PickupStatus.getTypeList().toString(), "INV_PICK_STATUS"), 
	GET_HOTEL_GENERAL_SERVICES_FAILED("Could not get hotel general services failed", "GET_GEN_SERV_FAILED"), 
	INVALID_GET_UOS_REQUEST("Invalid user opted service get request", "INV_UOS_REQ"),
	INVALID_ADD_HOTEL_SERVICE_REQUEST("Add hotel service details request is invalid", "INV_ADD_HSL_REQ"), 
	UPDATE_HOTEL_SERVICE_DETAILS_FAILED("Update hotel service details failed for current request", "UPD_HSD_FAIL"), 
	EMPTY_MAP_GS_VS_HS("Map General service vs hotel service can not be empty", "MAP_GS_VS_HS_EMP"), 
	
	// Room Service
	INVALID_ADD_ROOM_SERVICE_REQUEST("Invalid add room service request", "INV_ADD_ROOM_SERV_REQ"), 
	ADD_ROOM_SERVICE_FAILED("Add room service failed for current request", "ADD_ROOM_SERVICE_FAILED"), 
	GET_ALL_ROOM_SERVICE_FAILED("Get all room service failed for current request", "GET_ALL_ROOM_SERV_FAILED"), 
	ROOM_SERVICE_NOT_EXISTS("Room service does not exists with give id", "RS_NOT_EXISTS"), 
	RS_DELETE_FAILED("Room service deletion failed", "RS_DEL_FAIL"), 
	EMPTY_ROOM_SERVICE_ID("Room service id(equal to hotel id) can note be empty for updation", "EMP_RS_ID"), 
	UPDATE_ROOM_SERVICE_FAILED("Room service update failed for current request", "UPD_RS_FAIL"),
	UNSUPPORTED_MENU_ITEM_TYPE("Supported menu item types are "+MenuItemType.getCodeList().toString(),"UNSUPP_TYPE"),
	EMPTY_ROOM_SERVICE_MENU_TYPE_LIST("Room service menu type cannot be null or empty!","EMP_RS_MENU_TYPE_LIST"),
	
	//Food Item related
	INVALID_ADD_FOOD_ITEM_REQUEST("Add Food Item request cannot be null!","INV_REQ"), 
	EMPTY_CUISINE_TYPE("Cuisine type cannot be null or empty!","EMP_CUISINE"),
	EMPTY_NAME("Item name cannot be null or empty!","EMP_NAME"),
	EMPTY_DESC("Item description cannot be null or empty!","EMP_DESC"), 
	EMPTY_IMAGE_URL("Item image url cannot be null or empty!","EMP_IMG"), 
	NULL_MRP_PRICE("Mrp or price cannot be null","NULL_PRICE"), 
	FOOD_ITEM_NOT_FOUND("Food item with id %s not found!","NF_ITEM"), 
	FOOD_ITEM_ADDED_SUCCESSFULLY("Food item added successfully!","0"), 
	EMPTY_REQUEST_LIST("Empty request list","EMP_LIST"), 
	NO_FOOD_ITEM_ADDED_YET("No food item added yet!","NO_ITEM"), 
	EMPTY_SEARCH_QUERY("Search string cannot be empty!","EMP_QRY"), 
	INVALID_ADD_GOES_WELL_ITEMS_REQ("Invalid goes well with items add request!","INV_REQ"), 
	EMPTY_GOES_LIST("Goes well with item ids list cannot be empty!","EMP_GOES_LIST"), 
	ITEM_ID_CLASH("Goes list cannot contain same food item id","INV_REQ"), 
	GOES_IDS_ALREADY_EXIST("All the provided goes items list already exist for this food item id!","DUP_GOES_IDS"), 
	GOES_ITEMS_ADDED_SUCCESSFULLY("All items are added successfully!","0"), 
	FOOD_ITEM_UPDATED_SUCCESSFULLY("Food Item %s updated successfully!","0"),
	SOME_FOOD_ITEMS_NOT_UPDATED("Some food items not updated!","FAILED"),
	ALL_FOOD_ITEMS_UPDATED("Food items updated successfully!","0"),

	
	// House keeping related
	INVALID_ADD_HOUSEKEEPING_SERVICE_REQUEST("Invalid add housekeeping service request", "INV_ADD_HK_REQ"), 
	UNSUPPORTED_HOUSEKEEPING_ITEM_TYPE("Supported housekeeping item types are "+HousekeepingItemType.getCodeList().toString(),"UNSUPP_TYPE"), 
	ADD_HOUSEKEEPING_SERVICE_FAILED("Housekeeping add service failed for current request", "ADD_HK_SERVICE_FAIL"), 
	GET_ALL_HOUSEKEEPING_SERVICE_FAILED("Get all housekeeping service failed for current request", "GET_ALL_HKS_FAIL"), 
	HOUSEKEEPING_SERVICE_NOT_EXISTS("Housekeeping service does not exists for current id", "HKS_NOT_EXISTS"), 
	HOUSEKEEPING_SERVICE_DELETION_FAILED("Housekeeping service deletion failed for current request", "HKS_DEL_FAIL"), 
	EMPTY_HOUSEKEEPING_SERVICE_ID("Housekeeping service id(equal to hotel id) is empty", "EMP_HKS_ID"), 
	UPDATE_HOUSEKEEPING_SERVICE_FAILED("Housekeeping service updation failed for current request", "HKS_UPD_FAIL"), 
	
	// Housekeeping item related
	INVALID_ADD_HOUSEKEEPING_ITEM_REQEUST("Add housekeeping item request is invalid", "ADD_HK_ITEM_INVALID"), 
	ADD_HOUSEKEEPING_ITEM_FAILED("Add housekeeping item failed for current request", "ADD_HK_ITEM_FAIL"), 
	NO_HOUSEKEEPING_ITEMS_FOUND("No housekeeping items have been added yet", "NO_HK_ITEMS_ADDED"),
	HOUSEKEEPING_ITEM_DELETION_FAILED("Housekeeping item could not be deleted for current id", "HK_ITEM_DEL_FAIL"),
	HOUSEKEEPING_ITEM_WITH_NAME_UPDATED_SUCCESSFULLY("Housekeeping item %s updated successfully!","0"),
	SOME_HK_ITEMS_NOT_UPDATED("Some housekeeping items not updated!","FAILED"),
	ALL_HK_ITEMS_UPDATED("All housekeeping items updated successfully!","0"),

	INVALID_MRP_PRICE("Invalid item price or mrp!","INV_PRICE"), 
	FOOD_ITEM_DELETED_SUCCESS("Food item deleted successfully!","0"), 
	
	//Laundry related
	INVALID_LAUNDRY_ADD_REQUEST("Invalid add laundry service request!","INV_REQ"), 
	EMPTY_LAUNDRY_TYPE_LIST("Laundry type list cannot be empty!","EMP_TYPE_LIST"), 
	LAUNDRY_SERVICE_ADDED_SUCCESSFULLY("Laundry service added successfully!","0"), 
	UNSUPPORTED_LAUNDRY_TYPE("Supported laundry types are "+LaundryItemType.getCodeList().toString(),"UNSUPP_TYPE"), 
	LAUNDRY_SERVICE_NOT_FOUND("Laundry service not found!","NF_LS"), 
	EMPTY_LAUNDRY_ITEM_NAME("Laundry item name cannot be null or empty!","EMP_NAME"), 
	UNSUPPORTED_LAUNDRY_FOR_VALUE("Supported laundryFor values are "+ LaundryFor.getCodeList().toString(),"UNSUPP_LND_FOR"), 
	EMPTY_LAUNDRY_SERVICES_ID("Laundry services id cannot be null or empty!","EMP_LND_SER_ID"), 
	LAUNDRY_ITEM_ADDED_SUCCESSFULLY("Laundry item added successfully!","0"), 
	LAUNDRY_ITEM_NOT_FOUND("Laundry Item not found!","NF_ITEM"), 
	NO_LAUNDRY_ITEM_FOUND("No laundry item found!","NF_ITEM"), 
	LAUNDRY_ITEM_DELETED_SUCCESSFULLY("Laundry Item deleted successfully!","0"),
	INVALID_PICKUP_BY_TIME("PickupBy time must be in the format : "+DateUtil.MENU_ITEM_TIME_FORMAT,"INV_PKUP_TIME"),
	LAUNDRY_ITEM_WITH_NAME_UPDATED("Laundry item %s updated successfully!","0"),
	ALL_LAUNDRY_ITEMS_UPDATED("All laundry items updated successfully!","0"),
	SOME_LAUNDRY_ITEMS_NOT_UPDATED("Some laundry items not updated!","FAILED"),
	
	// food order related
	INVALID_ADD_FOOD_REQUEST("Add food order request is invalid", "INV_FOOD_ORDER_REQ"), 
	ADD_FOOD_ORDER_FAILED("Add food order request failed for current request", "ADD_FOOD_ORDER_FAIL"), 
	FOOD_ORDER_NOT_EXISTS("Food order does not exists for current request", "FOOD_ORDER_NO_EXISTS"), 
	FOOD_ORDER_DELETION_FAILED("Food order deletion failed for current id", "FOOD_ORDER_DEL_FAIL"), 
	EMPTY_FOOD_ORDER_ID("Food order id can not be empty for current request", "EMP_FOOD_ORDER_ID"), 
	UPDATE_FOOD_ORDER_FAILED("Food order update failed for current request", "UPD_FOOD_ORDER_FAIL"), 
	GET_ALL_FOOD_ORDERS_FAILED("Get all food order failed", "GET_ALL_FOOD_ORDER_FAIL"), 
	GET_ALL_FOOD_ORDER_BY_USER_ID_FAILED("Could not get any food order by current user Id", "FOOD_ORDER_BY_USER_ID_FAIL"), 
	GET_ALL_FOOD_ORDER_BY_HOTEL_ID_FAILED("Could not get any food order by current hotel Id", "FOOD_ORDER_BY_HOTEL_ID_FAILE"), 
	GET_ALL_BY_USERID_AND_STAYID_FAILED("Could not get all food orders by current userId and hotelId", "FOOD_ORDERS_BY_USER_AND_STAY_FAIL"),
	INVALID_ADD_FOOD_ORDER_REQ("Invalid food order request!","INV_REQ"), 
	FOOD_ORDER_ADDED_SUCCESFULLY("Food order added successfully!","0"), 
	INVALID_FOOD_ITEM_REQUEST("Food item details cannot be null!","EMP_FID"), 
	EMPTY_FOOD_ITEM_ID("Food item cannot be null or empty!","EMP_ID"),
	INVALID_QTY("Order qty must be greater than equal to 0!","INV_QTY"),
	FOOD_ITEM_NOT_YET_ADDED("This food item is not added in the order yet!","NF_ITEM"),
	INVALID_DELIVERY_TIME("Delivery time can not be smaller than current time", "INV_DEL_TIME"),
	
	//Cart related
	INVALID_CART_REQUEST("Invalid cart request!","INV_REQ"), 
	INVALID_CART_OPERATION("Cart operation can be one out of "+CartOperation.getCodeList().toString(),"INV_CART_OPR"), 
	UNIMPLEMENTED_CART_OPERATION("This cart operation is not yet implemented!","UNIMPL_OPR"), 
	OPERATION_APPLIED_SUCCESSFULLY("Operation applied successfully!","0"), 
	INVALID_CART_TYPE("Cart type can be one out of "+CartType.getCodeList().toString(),"INV_CART_TYPE"), 
	CART_NOT_EXIST("Empty cart!","EMP_CART"), 
	EMPTY_CART_LIST("Cart list cannot be null or empty!","EMP_CART_LIST"), 
	CART_DATA_CANNOT_BE_NULL("Cart data cannot be null!","EMP_CART_DATA"), 
	INVALID_PLACE_ORDER_REQUEST("Invalid place order request!","INV_REQ"),	
	SUCCESS_ORDER_PLACED("Order placed successfully!","0"),
	
	// Internet packs related
	INVALID_INTERNET_PACK_REQUEST("Add internet packs request is invlaid", "INV_ADD_INTERNET_REQ"), 
	ADD_INTERNET_PACK_FAILED("Add internet pack failed for current request", "ADD_INTERNET_PACK_FAIL"), 
	GET_ALL_INTERNET_PACKS_FAILED("Get all internet packs for current request failed", "GET_ALL_INT_PACKS_FAIL"), 
	INTERNET_PACK_SERVICE_NOT_EXISTS("Internet pack service does note exists", "INT_PACK_NOTE_FOUND"), 
	INTERNET_PACK_DELETION_FAILED("Internet pack deletion failed for current request", "INT_PACK_DEL_FAIL"), 
	EMPTY_INTERNET_PACK_SERVICE_ID("Internet pack id can not be empty", "EMP_INT_PACK_ID"), 
	UPDATE_INTERNET_PACK_FAILED("Could not updated internet pack for current request", "UPD_INT_PACK_FAIL"), 
	EMPTY_LIST_INTERNET_PACKS("Internet packs list can not be empty", "EMP_LIST_INT_PACK"), 
	
	// Laundry order related
	INVALID_LAUNDRY_ORDER_REQUEST("Invalid add laundry order request", "INV_ADD_LAUNDRY_ORDER"), 
	ADD_LAUNDRY_ORDER_FAILED("Add laundry order failed for current request", "ADD_LAUN_ORDER_FAIL"), 
	GET_ALL_LAUNDRY_ORDERS_FAILED("Get all laundry orders failed for current request", "GET_ALL_LAUNDRY_FAIL"), 
	LAUNDRY_ORDER_NOT_EXISTS("Laundry order does not exists for current id", "LAUNDRY_ORDER_NOT_FOUND"), 
	LAUNDRY_ORDER_DELETION_FAILED("Laundry order deletion failed for current request", "LAUNDRY_DEL_FAIL"), 
	INVALID_UPDATE_LAUNDRY_REQUEST("Invalid laundry update request", "INV_UPD_LAUNDRY_REQ"), 
	EMP_LAUNDRY_ORDER_ID("Laundry order id can not be empty for updation", "EMP_LAUNDRY_ORDER_ID"), 
	UPDATE_LAUNDRY_ORDER_FAILED("Update laundry order failed for current request", "UPD_LAUNDRY_ORDER_FAIL"), 
	GET_ALL_LAUNDRY_ORDER_OF_USER_FAILED("Could not get all laundry orders of the user", "GET_ALL_LAUNDRY_OF_USERS_FAIL"), 
	GET_ALL_LAUNDRY_ORDERS_OF_HOTEL_FAILED("Could not list all laundry orders of current hotel", "ALL_LAUNDRY_OF_HOTEL_FAIL"), 
	GET_ALL_LAUNDRY_OF_USER_STAYS_FAILED("Could not list all laundry orders of a users stay", "ALL_LAUNDRY_FAIL_FOR_STAY"), 
	
	// laundry order through cart related
	INVALID_LAUNDRY_ITEM_REQUEST("Invalid laundry item request for cart", "INV_LAUNDRY_ITEM_REQUEST"), 
	EMPTY_LAUNDRY_ITEM_ID("Laundry item id can not be empty", "EMP_LAUNDERY_ITE_ID"), 
	LAUNDRY_ITEM_NOT_YET_ADDED("Current laundry item has not been added to cart yet", "NF_LAUDRY_ITEM"), 
	
	// Houskeeping order related
	INVALID_ADD_HK_ORDER_REQUEST("Invalid add housekeeping order request", "INV_ADD_HK_REQ"), 
	EMPTY_GENERAL_SERVICE_ID("General service id can not be empty", "EMP_GS_ID"), 
	ADD_HOUSEKEEPING_ORDER_FAILED("Add housekeeping order failed for current request", "ADD_HK_ORDER_FAIL"), 
	GET_ALL_HK_ORDERS_FAILED("Get all housekeeping orders failed", "GET_ALL_HK_ORDER_FAIL"), 
	HOUSEKEEPING_ORDER_NOT_EXISTS("House keeping order does not exists for current id", "HK_ORDER_NF"), 
	HOUSEKEEPING_ORDER_DELETION_FAILED("Housekeeping order can not be deleted for current request", "DEL_HK_ORDER_FAIL"), 
	EMPTY_HK_ORDER_ID("House keeping order id can not be empty", "EMP_HK_ORDER_ID"), 
	GET_ALL_HK_ORDER_OF_USER_FAILED("Get all housekeeping orders of users failed", "GET_HK_ORDER_OF_USER_FAIL"), 
	GET_ALL_HOUSEKEEPING_ORDERS_OF_HOTEL_FAILED("Get all hk orders of hotel failed", "GET_HK_ORDER_OF_HOTEL_FAIL"), 
	GET_ALL_HK_OF_USER_STAYS_FAILED("Get all housekeeping order of user's stay failed", "GET_ALL_HK_ORDER_OF_USERS_STAY_FAIL"), 
	INVALID_HK_ITEM_ORDER_REQUEST("Invalid housekeeping item order details for adding to cart", "INV_HK_REQ_FOR_CART"), 
	EMPTY_HK_ITEM_ID("Housekeeping item id can not be empty for cart operations", "EMP_HK_ITEM_ID"), 
	HOUSEKEEPING_ITEM_NOT_ADDED_YET("Current house keeping item has not been added to cart yet for deletion", "HK_ITEM_NOT_ADDED"), 
	INVALID_LAUNDRY_PREF_TYPE("Laundry pref can only be from " + LaundryPreferences.getTypeList(), "INV_LAUNDRY_PREF" ), 
	
	// Generate expense/reuqest dashboard related
	INVALID_GENERATE_DASHBOARD_REQUEST("Invalid generate expense dashboard request!", "INV_EXP_DASH_REQ"), 
	INVALID_CREATE_DASHBOARD_REQUEST("Invalid create request dashboard request!","INV_REQ_DASH_REQ"),
	INVALID_CANCEL_FROM_REQUEST_DASHBOARD_REQUES("Invalid request dashboard cancel request", "INV_CANCEL_REQ"),
	
	// General Info related
	INVALID_GENERAL_INFO_REQUEST("Invalid General Info add request", "INV_GI_REQ"), 
	EMPTY_GENERAL_INFO_TYPE("General information type can not be empty for adding information", "EMP_GI_TYPE"), 
	INVALID_GENERAL_INFORMATION_TYPE("General information type can only be from " + GeneralInformationType.getTypeList(), "INV_GI_TYPE"), 
	ADD_GENERAL_INFORMATION_FAILED("General Information add failed for current request", "ADD_GI_FAIL"), 
	GET_GENERAL_INFO_FAILED("Get all general Information failed for current request", "GET_ALL_GI_FAIL"), 
	GENERAL_INFORMATION_NOT_FOUND("General information does not exists for current request", "GI_NF"), 
	GENERAL_INFO_DELETE_FAILED("General info delete failed for current request", "GI_DEL_FAIL"), 
	EMPTY_GI_ID("General info id can not be null for updation", "EMP_GI_ID"), 
	UPDATE_GI_FAILED("Update general info failed for current request", "UPD_GI_FAIL"), 
	
	// city guide related
	INVALID_ADD_CITY_GUIDE_REQUEST("Add city guide request is invalid", "INV_CG_REQ"), 
	UNSUPPORTED_CITY_GUIDE_ITEM_TYPE("City guide item type can be only of " + CityGuideItemType.getCodeList(), "INV_CG_TYPE"), 
	ADD_CITY_GUIDE_FAILED("Add city guide failed for the current request", "ADD_CG_FAIL"), 
	GET_ALL_CITY_GUIDE_SERVICE_FAILED("Get all city guide service failed", "GET_ALL_CG_FAIL"), 
	CITY_GUIDE_NOT_FOUND("City guide service does not exists", "CG_NF"), 
	CITY_GUIDE_DELETE_FAILED("City guide deletion failed for current request", "CG_DEL_FAIL"), 
	EMPTY_CITY_GUIDE_ID("City guide id can not be empty for current request", "EMP_CG_ID"), 
	CITY_GUIDE_UPDATE_FAILED("City guide service updation failed for current request", "UPD_CG_FAIL"), 
	
	// CITY GUIDE ITEMS related
	INVALID_ADD_CG_ITEM_REQUEST("Invalid request for adding CG Item", "INV_CGI_REQ"), 
	EMPTY_CG_ITEM_NAME("Name can not be empty for adding city guide item", "EMP_CGI_NAME"), 
	ADD_CG_ITEM_FAILED("Add city guide item failed for current request", "ADD_CGI_FAIL"), 
	NO_CITY_GUIDE_ITEM_ADDED_YET("There are no city guide items to show yet", "NO_CGI_ITEMS"), 
	CITY_GUIDE_ITEM_NOT_FOUND("City guide item not found", "CGI_NF"), 
	CITY_GUIDE_ITEM_DELETE_FAILED("Could not delete city guide item for current request", "CGI_DEL_FAIL"), 
	
	// HOUSE RULES related
	INVALID_ADD_HR_REQUEST("Invalid add house rules request", "INV_HR_REQ"), 
	ADD_HOUSERULES_FAILED("Adding house rules failed for current request", "ADD_HR_FAIL"), 
	NO_HOUSE_RULES_ADD_YET("There are no house rules added yet", "NO_HR_FOUND"), 
	NO_HOUSERULES_FOUND("Cound not found any house rules for current request", "NF_HR"), 
	HOUSE_RULE_DELETION_FAILED("House rules deletion failed for current request", "HR_DEL_FAIL"), 
	EMPTY_HOUSE_RULES_ID("House rules id(euqal to hotel id) can not be empty for current request", "EMP_HR_ID"), 
	UPDATE_HOUSE_RULES_FAILED("Update house rules failed for current request", "UPD_HR_FAIL"), 
	
	// BELL BOY related
	INVALID_ADD_BELL_BOY_ORDER_REQUEST("Bell boy order add request is invalid", "INV_BB_ORDER"), 
	ADD_BELL_BOY_ORDER_FAILED("Add bell boy order failed for current request", "ADD_BB_REQ_FAIL"), 
	CANCEL_REQUEST_FAILED("Could not cancel the order request", "CANCEL_REQ_FAIL"), 
	UPDATE_REQUEST_DASHBOARD_ITEM_ORDER_INVALID("Invalid map request for updating orderd items", "INV_REQ_ORDER_UPDATE"), 
	UPDATE_HK_ORDER_FAILED("Update House keeping order failed for current request", "HK_ORDER_UPD_FAIL"), 
	
	// commute Service related
	INVALID_COMMUTE_SERVICE_REQUEST("Commute Service request is invalid", "CS_REQ_INV"), 
	EMPTY_CS_SERVICE_SUPPORTED_PACKAGES("Commute Service packages can not be empty", "EMP_CS_PACKAGE"), 
	COMMUTE_SERVICE_SAME_CAR_TYPE("Commute package can not have same car types", "CS_SAME_CAR_TYPE"), 
	ADDING_COMMUTE_SERVICE_FAILED("Adding commute package failed for current request", "ADD_CS_PACKAGE_FAIL"), 
	GET_ALL_COMMUTE_SERVICE_FAILED("Could not list all the commute services", "CS_GET_ALL_FAIL"), 
	COMMUTE_SERVICE_NOT_EXISTS("Commute Service does not exists", "CS_NF"), 
	COMMUTE_SERVICE_DELETE_FAILED("Commute Service deletion failed for current request","CS_DEL_FAIL"), 
	EMPTY_COMMUTE_SERVICE_ID("Commute service id can not be empty for updation","EMP_CS_ID"), 
	UPDATE_COMMUTE_SERVICE_FAILED("Commute service could not be updated for current request","CS_UPD_FAIL"), 
	
	// Maintenance related
	INVALID_ADD_MAINTENANCE_REQUEST("Add maintenance request is invalid", "INV_ADD_MT_REQ"), 
	UNSUPPORTED_MAINTENANCE_ITEM_TYPE("Maintenance item type can only be of " + MaintenanceItemType.getCodeList(), "INV_MT_TYPE"), 
	ADD_MAINTENANCE_BY_TYPE_FAILED("Could not add maintenance item for current request", "ADD_MT_FAIL"), 
	GET_ALL_MAINTENANCE_SERVICE_FAILED("No Maintenance service add yet", "NO_MT_ADDED"), 
	MAINTENANCE_NOT_FOUND("Maintenance service not found for current request", "MT_NF"), 
	MAINTENANCE_DELETE_FAILED("Could not delete maintenance service for current request", "MT_DEL_FAIL"), 
	EMPTY_MAINTENANCE_ID("Maintenance id can not be null for current request", "EMP_MT_ID"), 
	MAINTENANCE_UPDATE_FAILED("Maintenance update failed for current request", "MT_UPD_FAIL"), 
	AIRPORT_PICKUP_CANCELLED("Airport Pickup has been cancelled", "AP_CANCELLED"), 
	
	// Maintenance item related
	INVALID_ADD_MAINTENANCE_ITEM_REQUEST("Add maintenance item request is invalid", "INV_MT_ITEM_REQ"), 
	EMPTY_MT_ITEM_NAME("Maintenance item name can not be empty", "EMP_MT_ITEM_NAME"), 
	ADD_MAINTENANCE_ITEM_FAILED("Add maintenance item failed for current request", "ADD_MT_ITEM_FAIL"), 
	NO_MAINTENANCE_ITEMS_FOUND("No maintenance item has been added yet", "MT_ITEM_NF"), 
	MAINTENANCE_ITEM_DELETION_FAILED("Maintenance item deletion failed for current request","MT_ITEM_DEL_FAIL"), 
	
	// Maintenance order related
	INVALID_ADD_MT_ORDER_REQUEST("Maintenance order request is invalid", "MT_ORDER_INV"), 
	ADD_MAINTENANCE_ORDER_FAILED("Add maintenance order request is failed", "MT_ORDER_FAIL"), 
	MAINTENANCE_ORDER_NOT_FOUND("Could not get any maintenance order", "MT_ORDER_FAIL"), 
	MAINTENANCE_ORDER_DELETION_FAILED("Maintenance order deletion failed", "MT_ORDER_DEL_FAIL"), 
	EMPTY_MT_ORDER_ID("Maintenance order id cant be empty", "EMP_MT_ORDER_ID"), 
	UPDATE_MT_ORDER_FAILED("Maintenance order update failed for current", "MT_ORDER_UPDATE_FAIL"), 
	
	// Commute Order related
	ADD_COMMUTE_ORDER_REQUEST("Request commute order failed for current request", "ADD_CO_REQ_FAIL"), 
	INVALID_COMMUTE_ORDER_REQUEST("Commute order request is invalid", "INV_ADD_CO_REQ"), 
	COMMUTE_ORDER_NOT_FOUND("No commute orders found for current request", "CO_NF"), 
	COMMUTE_ORDER_DELETION_FAILED("Commute order deletion failed for current request", "CO_DEL_FAIL"), 
	EMPTY_COMMUTE_ORDER_ID("Commute order id can not be empty for current request", "EMP_CO_ID"), 
	UPDATE_COMMUTE_ORDER_FAILED("Comute order updation failed for current request", "UPD_CO_REQ"), 
	UPDATE_MAINTENANCE_ITEM_FAILED("Maintenance item could not be updated", "UPD_MT_ITEM_FAIL"),
	
	//Checkin related
	INVALID_INITIATE_CHECKIN_REQ("Invalid initiate checkin request!","INV_REQ"), 
	REQUIRED_COMPANY_INFO("Please specify atleast one company.","REQ_COM_INFO"), 
	PERSONAL_INFO_REQUIRED("Personal info is required!","REQ_PER_INFO"), 
	IDENTITY_DOCS_REQUIRED("Please specify atleast one identity document!","REQ_ID"), 
	CHECKIN_INITIATED_SUCCESSFULLY("Checkin initiated successfully!","0"), 
	CHECKIN_MODIFIED_SUCCESSFULLY("Checkin details modified successfully!","0"), 
	CHECKIN_NOT_FOUND("Checkin not yet initiated!","NO_CHECKIN"), 
	EMPTY_DOB("Date of birth cannot be empty!","EMP_DOB"),
	
	// Reservation service related
	INVALID_ADD_RESERVATION_REQUEST("Invalid add reservation request", "INV_ADD_RS_REQ"), 
	UNSUPPORTED_RESERVATION_TYPE("Reservation type can only be of " + ReservationType.getCodeList(), "INV_RS_TYPE"), 
	ADD_RESERVATION_SERVICE_FAILED("Add reservation service failed for current request", "ADD_RS_FAIL"), 
	RESERVATION_SERVICE_NOT_FOUND("Could not find any reservation service", "RS_NF"), 
	RESERVATION_SERVICE_DELETE_FAILED("Reservation service delete failed", "RS_DEL_FAIL"), 
	EMPTY_RESERVATION_SERVICE_ID("Reservation service id can not be empty for current request", "EMP_RS_ID"), 
	RESERVATION_SERVICE_UPDATE_FAILED("Reservation service update failed", "RS_UPD_FAIL"), 
	
	// Reservation Item related
	INVALID_ADD_RESERVATION_ITEM_REQUEST("Reservation Item add request is invalid", "INV_ADD_RS_ITEM_REQ"), 
	ADD_RESERVATION_ITEM_FAILED("Reservation item addition failed for current request", "ADD_RS_ITEM_FAIL"), 
	NO_RESERVATION_ITEM_FOUND("No reservation items found for current request", "RS_ITEM_NF"), 
	RESERVATION_ITEM_DELETION_FAILED("Reservation item deletion failed for current request", "RS_ITEM_DEL_FAIL"), 
	UPDATE_RESERVATION_ITEM_FAILED("Reservation item updation failed for current request", "RS_ITEM_UPD_FAIL"), 
	EMPTY_RESERVATION_ITEM_ID("Reservation item id can not be empty for current request", "RS_ITEM_ID_EMP"), 
	EMPTY_MAINTENANCE_ITEM_ID("Maintenance item id can not be empty for current request","EMP_MT_ITEM_ID"), 
	INVALID_FETCH_RESERVATION_ITEM_REQUEST("Invalid fetch reservation item request!","INV_REQ"),
	INVALID_DURATION("Please provide valid duration","INV_DUR"),
	INVALID_UPDATE_RES_ITEM_REQUEST("Invalid reservation item update request!","INV_REQ"),
	RS_ITEM_WITH_NAME_UPDATED("Reservation item %s updated successfully!","0"),
	SOME_RS_ITEMS_NOT_UPDATED("Some reservation items not updated!","FAILED"),
	ALL_RS_ITEMS_UPDATED("All reservation items updated successfully!","0"),
	
	// Reservation Order related
	INVALID_ADD_RESERVATION_ORDER_REQUEST("Invalid reservation order request", "INV_RS_ORDER_REQ"), 
	ADD_RESERVATION_ORDER_FAILED("Add reservation Order failed for current request", "ADD_RS_ORDER_FAIL"), 
	RESERVATION_ORDER_NOT_FOUND("Reservation order could not be found", "RS_ORDER_NF"), 
	RESERVATION_ORDER_DELETION_FAILED("Reservation deletion failed for the current request", "RS_ORDER_DEL_FAIL"), 
	EMPTY_RESERVATION_ORDER_ID("Reservation order id can not be empty for current request", "RS_ORDER_ID_EMP"), 
	UPDATE_RESERVATION_ORDER_FAILED("Reservation order update failed for current request", "RS_ORDER_UPD_FAIL"), 
	
	// Check in related
	INVALID_EARLY_CHECK_IN_INFO("Invalid check in information for room", "INV_CHECKIN_INFO_REQ"), 
	EARLY_CHECKIN_INFO_SET_FAILED("Could not set early check In info for current room", "SET_CHECKIN_INFO_FAILED"), 
	GET_EARLY_CHECKIN_DETAILS_FAILED("Could not get early check in details for room", "GET_EARLY_CHECKIN_INFO_FAIL"), 
	INVALID_LATE_CHECKOUT_INFO_REQ("Invalid late checkout information request", "INV_LATE_CHECKOUT_INFO_REQ"), 
	LATE_CHECKOUT_INFO_UPDATE_FAILED("Update late checkout info for room failed", "LATE_CHECKOUT_INFO_FAIL"), 
	GET_LATE_CHECKOUT_DETAILS_FAILED("Get late checkout info failed for current request", "GET_LATE_CHECKOUT_FAIL"), 
	EMPTY_PERSONAL_INFO("Please provide personal information","EMP_INFO"),
	EMPTY_ADDRESS("Address cannot be null or empty!","EMP_ADDRESS"),
	
	// hotel other info related 
	ADD_HOTEL_OTHER_INFO_FAILED("Hotel other info add feature failed for current request", "ADD_HOTEL_OTHER_INFO_FAILED"), 
	NO_HOTEL_OTHER_INFO_FOUND("Could not find hotel other info for current request", "HOTEL_OTHER_INFO_NF"), 
	HOTEL_OTHER_INFO_DEL_FAILED("Could not delete hotel other info", "HOTEL_OTHER_INFO_DEL_FAIL"), 
	ADD_OR_REMOVE_AWARDS_FAILED("Add or remove awards list failed for current request", "LIST_UPD_FAILED"), 
	
	// SERVICE FEEDBACK RELATED
	INVALID_SERVICE_FEEDBACK_REQUEST("Service feedback add request is invalid", "SF_ADD_REQ_INV"), 
	INVALID_QUALITY_SELECTION("Quality can not be more than 5 and less than 1", "INV_QUALITEY_SF"), 
	INVALID_TIME_SELECTION("Time taken value is invalid", "INV_TIME_TAKEN_SF"), 
	ADD_SERVICE_FEEDBACK_FAILED("Add service feedback failed for current request", "ADD_SF_FAILED"), 
	SERVICE_FEEDBACK_NOT_FOUND("Service feedback not found", "SF_NF"), 
	UPDATE_SERVICE_FEEDBACK_FAILED("Service feedback updation failed for current request", "SF_UPD_FAIL"), 
	SERVICE_FEEDBACK_DEL_FAILED("Service feedback delete failed for current request","SF_DEL_FAIL"), 
	EMPTY_FEEDBACK_LIST("Feedback list can not be empty for current request", "EMP_SF_LIST"),
	
	// internet order related
	INVALID_INTERNET_ORDER_REQUEST("Internet order request is invalid", "INV_IO_REQ"), 
	ADD_INTERNET_ORDER_REQUEST_FAILED("Add internet order request failed", "ADD_IO_REQ_FAIL"), 
	INTERNET_ORDER_NOT_FOUND("Internet order not found for current request", "IO_NF"), 
	INTERNET_ORDER_DELETION_FAILED("Internet order deletion failed for current request", "IO_DEL_FAIL"), 
	EMPTY_INTERNET_ORDER_ID("Internet order id is empty for current request", "EMP_IO_ID"), 
	UPDATE_INTERNET_ORDER_FAILED("Internet order update failed for current request", "UPD_IO_FAIL"), 
	
	// late checkout request related
	INVALID_LATE_CHECKOUT_REQUEST("Late checkout request is invalid", "LCR_INV"), 
	ADD_LATE_CHECKOUT_REQUEST_FAILED("Late checkout request failed", "LCR_ADD_FAIL"), 
	LATE_CHECKOUT_NOT_FOUND("Could not find late checkout", "LCR_NF"), 
	LATE_CHECKOUT_REQUEST_DEL_FAILED("Late checkout request deletion failed for current request", "LCR_DEL_FAIL"), 
	EMPTY_LATE_CHECKOUT_REQUEST_ID("Late checkout request id can not be empty for current request", "LCR_ID_EMP"), 
	UPDATE_LATE_CHECKOUT_FAILED("Update late checkout request failed", "LCR_UPD_FAIL"),
	
	//TV/Entertaimenet related
	INVALID_ADD_GENERE_REQUEST("Invalid add genere request!","INV_REQ"),
	EMPTY_GENERE_NAME("Genere name cannot be null or empty!","EMP_GEN_NAME"),
	EMPTY_GENERE_LOGO("Genere logo url cannot be null or empty!","EMP_GEN_LOGO"),
	EMPTY_GENERE_ID("Genere id cannot be null or empty!","EMP_GEN_ID"),
	GENERE_ALREADY_EXISTS("This type of genere already exists!","DUP_GEN"),
	GENERE_ADDED_SUCCESSFULLY("Genere added successfully!","0"), 
	INVALID_UPDATE_GENERE_REQUEST("Invalid update genere request!","INV_REQ"),
	GENERE_NOT_FOUND("Genere with id %s not found!","NF_GEN"), 
	GENERE_UPDATED_SUCCESSFULLY("Genere updated successfully!","0"),
	GENERE_DELETED_SUCCESSFULLY("Genere deleted successfully!","0"), 
	NO_GENERE_FOUND("No generes are created yet!","NF_GEN"), 
	INVALID_ADD_CHANNEL_REQUEST("Invalid add channel request!","INV_REQ"),
	EMPTY_CHANNEL_NAME("Channel name cannot be null or empty!","EMP_CH_NAME"),
	EMPTY_CHANNEL_LOGO("Channel logo cannot be null or emoty!","EMP_CH_LOGO"),
	EMPTY_CHANNEL_IMAGE("Channel imageUrl cannot be null or empty!","EMP_CH_IMAGE"),
	CHANNEL_ALREADY_EXISTS("Channel with name %s already exists","DUP_CH"),
	CHANNEL_ADDED_SUCCESSFULLY("Channel added successfully!","0"),
	CHANNEL_DELETED_SUCCESSFULLY("Channel deleted successfully","0"),
	CHANNEL_UPDATED_SUCCESSFULLY("Channel updated successfully","0"),
	ALL_CHANNEL_ADDED_SUCCESSFULLY("All channels added successfully!","0"),
	CHANNEL_NOT_FOUND("Channel not found!","NF_CH"),
	NO_CHANNEL_FOUND("No channels found for this request!","NF_CH"), 
	EMPTY_CHANNEL_ID("Channel Id cannot be null or empty!","EMP_CH_ID"), 
	DUPLICATE_CHANNEL_NAME("Channel name cannot be duplicated","DUP_CH_NAME"), 
	ENTERTAIMENT_SERVICE_NOT_FOUND("Entertaiment service not found!","NF_ES"),
	DUP_ENTERTAIMENT_TYPE("Duplicate generes not allowed!","DUP_ENT_TYPE"),
	ENTERTAIMENT_TYPE_NOT_FOUND("The type is not found in service!","NF_ENT_TYPE"), 
	INVALID_ADD_TYPE_INFO_REQUEST("Invalid add type info request!","INV_REQ"), 
	INVALID_REMOVE_TYPE_INFO_REQUEST("Invalid remove type info request!","INV_REQ"), 
	MASTER_CHANNEL_NOT_FOUND("Master channel not found", "MC_NF"), 
	
	// channel hotel related
	EMPTY_CHANNEL_NUMBER("Channel number can not be empty", "EMP_CN"),
	INVALID_ADD_ENT_SERVICE_REQ("Invalid add entertaiment service request!","INV_REQ"),
	EMPTY_TYPE_ID("Type id cannot be null or empty!","EMP_TYPE_ID"), 
	ENTERTAIMENT_SERVICE_ADDED_SUCCESSFULLY("Entertaiment service added successfully!","0"),
	ENTERTAIMENT_SERVICE_DELETED_SUCCESSFULLY("Entertaiment service deleted successfully!","0"),
	ENTERTAIMENT_SERVICE_UPDATED_SUCCESSFULLY("Entertaiment service updated successfully!","0"),
	NO_ENTERTAIMENT_SERVICES("No entertaiment services found!","NF_ES"), 
	ADD_ENTERTAINMENT_ITEM_FAILED("Add entertainment item failed for current request", "ADD_EI_FAIL"), 
	INVALID_ENTERTAINMENT_ITEM_REQUEST("Invalid entertainment item add request", "INV_EI_REQ"), 
	NO_ENTERTAIMENT_ITEM_FOUND("No entertainment item found", "EI_NF"), 
	ENTERTAINMENT_ITEM_DELETE_FAILED("Entertainment item delete failed for current request", "EI_DEL_FAILED"), 
	EMPTY_ENTERTAINMENT_ITEM_ID("Entertainment item id can not be empty", "EI_ID_EMP"), 
	UPDATE_ENTERTAINMENT_ITEM_FAILED("Entertainment item update failed", "UPD_EI_FAIL"), 
	MASTER_GENERE_NOT_FOUND("Master genere not found", "MG_NF"),
	EMPTY_ENT_SERVICE_ID("Service id cannot be null or empty!","EMP_ID"),
	ITEM_ALREADY_EXISTS("This item with master item id %s already exists","DUP_ENT_ITEM"),
	DUPLICATE_ENT_ITEM("Request cannot contain duplicate items!","DUP_ENT_ITEM"),
	
	//Common
	COMMON("",""), 
	
	//master City related
	INVALID_ADD_MASTER_CITY_REQ("Invalid add master city request","INV_REQ"), 
	EMPTY_CITY_NAME("City name cannot be null or empty!","EMP_CITY_NAME"), 
	CITY_BY_NAME_ALREADY_EXISTS("City by name %s already exists!","DUP_CITY"),
	DUP_CITY_NAME("City names cannot be duplicated!","DUP_CITY"),
	CITY_ADDED_SUCCESSFULLY("City added successfully","0"),
	ALL_CITIES_ADDED_SUCCESSFULLY("All cities added successfully","0"),
	CITY_DELETED_SUCCESSFULLY("City deleted successfully","0"),
	CITY_NOT_FOUND("City not found!","NF_CITY"),
	NO_CITIES_FOUND("No citites found!","NF_CITY"), 
	
	// Stays feedback related
	STAYS_FEEDBACK_REQUEST_INVALID("Invalid add stays feedback request is invalid", "INV_SF"), 
	EMPTY_FEEDBACK_NAME("Empty feedback name for current request", "EMP_FEEDBACK_NAME"), 
	ADD_STAYS_FEEDBACK_FAILED("Add stays feedback service failed for current request", "SF_ADD_FAIL"), 
	NO_STAYS_FEEDBACK_FOUND("No stays feedback found for current request", "SF_NF"), 
	STAYS_FEEDBACK_DELETION_FAILED("Stays feedback deletion failed for current request", "SF_DEL_FAIL"), 
	EMPTY_STAYS_FEEDBACK_ID("Stays feedback id can not be empty", "SF_ID_EMP"), 
	UPDATE_STAYS_FEEDBACK_FAILED("Stays feedback update failed for current request", "SF_UPD_FAILED"),
	
	
	//Master Amenity related
	INVALID_ADD_MASTER_AMENITY_REQ("Invalid master amenity request!","INV_REQ"), 
	EMPTY_AMENITY_NAME("Amenity name cannot be null or empty!","EMP_NAME"), 
	EMPTY_AMENITY_ICON("Amenity icon cannot be null or empty!","EMP_ICON"), 
	INVALID_AMENITY_TYPE("Amenity type can be one out of "+AmenityType.getCodeList().toString(), "INV_TYPE"), 
	AMENITY_ALREADY_EXISTS("Amenity with name %s already exists!","DUP_AMENITY"), 
	AMENITY_ADDED_SUCCESSFULLY("Amenity added successfully!","0"), 
	AMENITY_NOT_FOUND("Amenity with id %s not found!","NF_AMENITY"),
	NO_AMENITIES_FOUND("No amenities found!","NF_AMENITY"), 
	AMENITY_DELETED_SUCCESSFULLY("Amenity deleted successfully!","0"), 
	INVALID_LINK_AMENITY_REQUEST("Invalid link amenity request!","INV_REQ"),
	DIRECTION_NOT_FOUND("Direction with id %s not found","NF_DIR"),
	
	//Essentials
	INVALID_LINK_ESSENTIAL_REQUEST("Invalid link essential request!","INV_REQ"), 
	ESSENTIAL_NOT_FOUND("Essential with id %s not found!","NF_ASSENTIAL"),
	ESSENTIAL_ADDED_SUCCESSFULLY("Essentials added successfully!","0"),
	
	//Awards
	INVALID_LINK_AWARD_REQUEST("Invalid link award request!","INV_REQ"), 	
	EMAPTY_AWARD_DATA("Award Data cannot be null!","INV_REQ"), 	
	EMAPTY_AWARD_ID("Award id cannot be empty!","EMP_AWARD_ID"), 	
	EMAPTY_AWARD_YEAR("Award's getting year cannot be empty!","EMP_AWARD_YEAR"), 	
	AWARD_NOT_FOUND("Award with id %s not found!","NF_ASSENTIAL"),
	AWARD_ALREADY_EXIST("Award already exist!","EXIST_AWARD"), 	
	AWARD_NOT_EXIST("Award not exist!","EXIST_AWARD"), 	
	AWARD_ADDED_SUCCESSFULLY("Award added successfully!","0"),
	
	//Card accepted
	INVALID_LINK_CARD_REQUEST("Invalid link card request!","INV_REQ"), 
	CARD_ADDED_SUCCESSFULLY("Card added successfully!","0"),
	CARD_NOT_FOUND("Card with id %s not found!","NF_CRAD"),

	
	// Early checkout request
	INVALID_INITATE_CHECKOUT_REQUEST("Initiate checkout request is invalid", "INV_CHECKOUT_REQ"), 
	INVALID_MODIFY_CHECKOUT_REQUEST("Modify checkout request is invalid", "INV_MODIFY_CR"), 
	EMPTY_CHECKOUT_ID("CHeckout id can not be empty for modification", "EMP_CHEKCOUT_ID"),
	CHECKOUT_INITIATED_SUCCESSFULLY("Checkout initiated successfully!","0"),
	CHECKOUT_MODIFIED_SUCCESSFULLY("Checkout modified successfully!","0"), 
	CHECKOUT_NOT_FOUND("Checkout not found", "CHECKOUT_NF"),
	
	//Identity docs
	EMPTY_DOC_NUMBER("Identity document number required!","EMP_DOC_NO"), 
	EMPTY_DOC_NAME("Name on document is required!","EMP_DOC_NAME"), 
	EMPTY_DOC_URL("Document url cannot be null or empty!","EMP_DOC_URL"), 
	INVALID_PAYMENT_METHOD("Payment method can be one out of "+PaymentMethod.getCodeList().toString(),"INV_PAY_METHOD"), 
	DOCUMENT_DELETED_SUCCESSFULLY("Document deleted successfully!","0"),
	
	// room service left overs
	EMPTY_START_TIME("Start time can not be empty", "EMP_START_TIME"), 
	EMPTY_ENDING_TIME("End time can not be empty", "EMP_END_TIME"), 
	INVALID_FOOD_ORDER_TIME_REQUEST("Room service for %s can only be availed at mentioned time", "ROOM_SERVICE_INVALID_TIME"), 
	
	// Modify checkin Request
	INVALID_MODIFY_CHECKIN_REQUEST("Modify checkin request is invalid", "INVALID_MODIFY_REQ"), 
	EMPTY_EARLY_CHECKIN_TIME("Early checkin time can not be empty", "EMP_EARLY_CHECKIN"), 
	INVALID_EARLY_CHECKIN_TIME_FORMAT("Eearly checkin time format is " + DateUtil.BASE_TIME_FORMAT, "INV_CHECKIN_FORMAT"), 
	
	// Suggested hotel related
	INVALID_SUGGESTED_HOTELS("Invalid suggested hotel request", "INV_SUGGESTED_HOTEL_REQ"), 
	ADD_SUGGESTED_HOTELS_FAILED("Could not add suggested hotel", "SUGGESTED_HOTEL_FAIL"), 
	NO_SUGGESTED_HOTELS_FOUND("Could not find any suggested hotels", "SH_NF"), 
	SUGGESTED_HOTEL_DELETION_FAILED("Suggested hotel deletion failed", "SH_DEL_FAIL"),
	
	//Complementary services related
	EMPTY_COMPLEMENTARY_SERVICE_ID("Service id(equal to hotel id) cannot be null or empty!","EMP_COMP_SER_ID"),
	INVALID_ADD_COMPLEMENTARY_SERVICES_REQUEST("Invalid add complementary services request!","INV_REQ"),
	INVALID_UPDATE_COMPLEMENTARY_SERVICES_REQUEST("Invalid update complementary services request!","INV_REQ"),
	EMPTY_SERVICE_NAME("Service name cannot be empty!","EMP_SER_NAME"),
	EMPTY_SERVICE_DESCRIPTION("Service description cannot be empty!","EMP_SER_DESC"),
	COMPLEMENTARY_SERVICE_NOT_FOUND("Complementary service not found!","NF_CS"),
	NO_COMPLEMENTARY_SERVICE("No complementary services are created yet!","NF_CS"),
	COMPLEMENTARY_SERVICE_ADDED_SUCCESSFULLY("Complementary services added successfully!","0"),
	COMPLEMENTARY_SERVICE_UPDATED_SUCCESSFULLY("Complementary services updated successfully!","0"),
	COMPLEMENTARY_SERVICE_DELETED_SUCCESSFULLY("Complementary services deleted successfully!","0"), 
	EMPTY_TAB_NAME("Tab name cannot be empty!","EMP_TAB"), 
	
	//Current stays services related
	INVALID_GET_ALL_STAYS_SERVICE_REQUEST("Invalid getAllServices for stays request!","INV_REQ"), 
	NO_SERVICE_FOR_CURRENT_STAY("No services found for this stay!","NF_SER"), 
	
	//Notifications related
	INVALID_ADD_NOTIFICATION_REQUEST("Invalid add notification request!","INV_REQ"),
	EMPTY_NTFY_TITLE("Title cannot be null or empty!","EMP_TITLE"),
	EMPTY_NTFY_DESCRIPTION("Description cannot be null or empty!","EMP_DESC"),
	EMPTY_NTFY_ICON_URL("Icon url cannot be null or empty!","EMP_ICON"),
	NOTIFICATION_ADDED_SUCCESSFULLY("Notification added successfully!","0"),
	NOTIFICATION_NOT_FOUND("Notification not found!","NF_NTFY"),
	NO_NOTIFICATIONS_FOUND("No notifications found!","NF_NTFY"), 
	EMPTY_NTFY_TYPE("Empty notification type!","EMP_NTFY_TYPE"), 
	NOTIFICATION_DELETED_SUCCESSFULLY("Notification deleted successfully!","0"), 
	INVALID_GET_STAY_NOTIFICATION_REQUEST("Invalid get stay notifications request!","INV_REQ"), 
	
	//Message related
	INVALID_ADD_MESSAGE_REQUEST("Invalid add message request","INV_REQ"),
	INVALID_READ_MESSAGE_REQUEST("Invalid read message request","INV_REQ"),
	EMPTY_MESSAGE_TEXT("Message text cannot be null or empty","EMP_MSG_TXT"),
	MESSAGE_ADDED_SUCCESSFULLY("Message added successfully!","0"),
	MESSAGE_STATUS_CHANGED_SUCCESSFULLY("Message status changed successfully!","0"),
	MESSAGE_DELETED_SUCCESSFULLT("Message deleted successfully!","0"),
	INVALID_CREATION_TYPE("Message creation type can be one out of "+MessageCreationType.getCodeList().toString(),"INV_MCT"), 
	MESSAGE_NOT_FOUND("Message not found!","NF_MSG"),
	NO_MESSAGES_FOUND("No messages found!","NF_MSG"), 
	NO_DOCS_FOUND("Identity documents not found", "DOCS_NF"),
	NO_CHAT_USERS("No chat users found!","NF_CHAT_USERS"), 
	
	//Time related
	INVALID_GENERAL_TIME_FORMAT("Valid time format is %s","INV_FMT"), 
	
	// General
	EMPTY_RESEVATION_ITEM_NAME("Reservation item name can not be empty", "RS_ITEM_NAME_EMP"), 
	EMPTY_COMMUTE_PACKAGE_NAME("Commute package name can not be empty", "EMP_COMMUTE_PACKAGE_NAME"), 
	EMPTY_USER_TITLE("Empty user title!","EMP_TTL"), 
	
	//Timezone related
	INVALID_ADD_TIMEZONE_REQUEST("Invalid add timezone request!","INV_REQ"), 
	EMPTY_TIMEZONE("Timezone cannot be empty!","EMP_TZ"), 
	EMPTY_HOURS_DIFF("Timezone hours difference cannot be null or empty!","EMP_TZ_HRS_DIFF"), 
	TIMEZONE_ALREADY_EXISTS("This timezone details already exists!","DUP_TZ"),
	TIMEZONE_DETAILS_ADDED_SUCCESSFULLY("Timezone details added successfully!","0"),
	TIMEZONE_DETAILS_DELETED_SUCCESSFULLT("Timezone details deleted successfully!","0"),
	TIMEZONE_NOT_FOUND("Timezone details not found!","NF_TZ"),
	NO_TIMEZONE("No timezone details found!","NF_TZ"),
	
	// Loyalty programs 
	INVALID_ADD_LOYALTY_REQUEST("Add loyalty request is invalid", "INV_LOYALTY_REQ"), 
	EMPTY_LOYALTY_PROGRAM_NAME("Loyalty program name cannot be empty!","EMP_LOYALTY_NAME"),
	EMPTY_LOYALTY_PROGRAM_ICON("Loyalty program icon cannot be empty!","EMP_LOYALTY_ICON"),
	ADD_LOYALTY_FAILED("Add Loyalty program failed for current request", "ADD_LOYALTY_FAILED"), 
	GET_ALL_LOYALTY_FAILED("No loyalty programs found", "FN_LOYALTY_PROGRAMS"), 
	LOYALTY_PROGRAM_NOT_EXISTS("Loyalty program does not exists with current request", "LOYALTY_NO_EXISTS"), 
	LOYALTY_DELETE_FAILED("Loyalty deletion failed with current request", "LOYALTY_DEL_FAILED"), 
	EMPTY_LOYALTY_ID("Loyalty id can not be empty for updation", "EMP_LOYALTY_ID"), 
	UPDATE_LOYALTY_FAILED("Loyalty program update failed with current request", "LOYALTY_UPD_FAILED"),
	INVALID_LINK_LOYALTY_REQUEST("Invalid link loyalty program request!","INV_REQ"),
	LOYALTY_PROGRAM_NOT_FOUND("Loyalty Program not found!","NF_LOYALTY_PROGRAM"),
	LOYALTY_PROGRAM_ALREADY_EXIST("Loyalty Program allready exist!","ALLREADY_EXIST_LOYALTY_PROGRAM"),
	LOYALTY_PROGRAMS_ADDED_SUCCESSFULLY("Loyalty Programs added successfully!","0"),
	NO_LOYALTY_PROGRAMS_FOUND("No loyalty programs found!","NF_LP"),
	
	//Wings
	INVALID_ADD_WING_REQUEST("Add wing request is invalid", "INV_WING_REQ"), 
	EMPTY_WING_ID("Wing id cannot be empty!","EMP_WING_ID"),
	EMPTY_WING_NAME("Wing name cannot be empty!","EMP_WING_NAME"),
	EMPTY_WING_CODE("Wing code cannot be empty!","EMP_WING_CODE"),
	ADDED_WING_FAILED("Wing added failed!","ADD_WING_FAILED"),
	WING_NOT_FOUND("Wing not found!","NF_WNG"),
	GET_ALL_WINGS_FAILED("No wings found!", "NF_WINGS"), 
	WING_DELETE_FAILED("Wing deletion failed with current request", "WING_DEL_FAILED"), 
	WING_UPDATE_FAILED("Wing update failed with current request", "WING_UPDATE_FAILED"),
	WING_WITH_HOTEL_NOT_FOUND("Wing with hotel not found!","NF_WING"),
	
	//Floor
	INVAILID_ADD_FLOOR_REQUEST("Add floor request in invalid","INV_FLOOR_REQ"),
	EMPTY_FLOOR_ID("Floor id cannot be empty!","EMP_FLOOR_ID"),
	EMPTY_FLOOR_NAME("Floor name cannot be empty!","EMP_FLOOR_NAME"),
	EMPTY_FLOOR_CODE("Floor code cannot be empty!","EMP_FLOOR_CODE"),
	ADDED_FLOOR_FAILED("Floor added failed!","ADD_FLOOR_FAILED"),
	FLOOR_NOT_FOUND("Floor not found!","NF_FLOOR"),
	GET_ALL_FLOOR_FAILED("No floors found", "NF_FLOORS"), 
	FLOOR_DELETE_FAILED("Floor deletion failed with current request", "FLOOR_DEL_FAILED"), 
	FLOOR_UPDATE_FAILED("Floor update failed with current request", "FLOOR_UPDATE_FAILED"), 
	FLOOR_WITH_WING_NOT_FOUND("Floor with this wing id not found!","NF_FLOOR"),
	
	//Reservation category related
	RESERVATION_CATEGORY_ADDED_SUCCESSFULLY("Reservation category added successfully!","0"), 
	RESERVATION_CATEGORY_DELETED_SUCCESSFULLY("Reservation category deleted successfully!","0"), 
	RESERVATION_CATEGORY_UPDATED_SUCCESSFULLY("Reservation category updated successfully!","0"), 
	INVALID_ADD_RESERVATION_CAT_REQ("Add reservation category request cannot be null","INV_REQ"), 
	EMPTY_RES_CAT_NAME("Please provide category name!","EMP_RES_NAME"), 
	EMPTY_RES_CAT_IMAGE_URL("Please provide image url!","EMP_RES_CAT_IMG"), 
	EMPTY_RES_CAT_DESC("Please provide some description!","EMP_RES_CAT_DESC"), 
	RES_CAT_NOT_FOUND("Reservation category not found!","NF_RES_CAT"), 
	NO_RES_CAT_FOUND("No reservation category found for this request!","NO_RES_CAT"), 
	INVALID_UPDATE_RES_CAT_REQ("Invalid update reservation category request!","INV_REQ"), 
	EMPTY_RES_CAT_ID("Reservation category id cannot be empty!","EMP_RES_CAR_ID"), 
	EMPTY_RES_SUB_CAT("Please provide subCategory name","EMP_SUB_CAT"),
	EMPTY_DIRECTION_INFO("Please provide direction info","EMP_DIR_INFO"),
	
	//Picklist related
	INVALID_ADD_PICKLIST_REQUEST("Invalid add or modify picklist request","INV_REQ"),
	INVALID_PICKLIST_TYPE("Valid picklist types are : "+PickListType.getCodeList().toString(),"INV_PL_TYPE"), 
	EMPTY_PICKLIST_VALUES("Please provide picklist values","EMP_PL_VAL"),
	PL_UPDATED_SUCCESSFULLY("Picklist updated successfully!","0"),
	PL_DELETED_SUCCESSFULLY("Picklist deleted successfully!","0"), 
	PICKLIST_NOT_FOUND("Picklist not found!","NF_PL"), 
	NO_PL_FOUND("No picklist found","NO_PL"),
	
	
	//Arrival and Bookings 
	INVAILID_ALLOCATE_REQUEST("Invalid allocate request","INV_REQ"), 
	INVAILID_RFEEZE_REQUEST("Invalid freeze request","INV_REQ"), 
	INVAILID_ROOMS_AND_STAYS("Invalid Rooms And Stays request","INV_REQ"), 
	ROOM_ALREADY_ALLOTED("This Room is allready alloted !",""),
	EMPTY_BOOKING_IDS("Booking ids cannot be empty!","EMP_BOOKING_IDS"), 
	INVAILID_COMPLETE_CHECKIN_REQUEST("Invalid complete checkin request!","INV_REQ"), 
	EARLY_CHECKIN_NOT_REQUESTED("Early checking not requested by the user!","NOT_REQ_EARLY_CHECKIN"), 
	
	//Request
	NO_REQUEST("No Requests found","NF_REQUESTS"), 
	NO_ROOM_SERVICE_REQUESTS("No room services found!","NF_ROOM_SERVICES"), 
	
	//Service
	BELL_BOY_SERVICE_ADDED("Bell boy service added successfully!","0"), 
	INVALID_BELL_BOY_SERVICE_REQUEST("Invalid bell boy service request!","INV_REQ"), 
	BELL_BOY_SERVICE_NOT_FOUND("Bell boy service not found!","NF_BBS"), 
	BELL_BOY_SERVICE_UPDATED("Bell boy service added successfully!","0"), 
	NO_BELL_BOY_SERVICES_FOUND("No bell boy services created yet!","NF_BBS"), 
	
	//Places related
	INVALID_PLACE_REQUEST("Invalid place add/update request!","INV_REQ"),
	EMPTY_PLACE_NAME("Please provide place name!","EMP_PL_NAME"),
	INVALID_PLACE_TYPE("Valid place types are "+PlaceType.getTypeList().toString(),"INV_PL_TYPE"), 
	EMPTY_PLACE_ID("Please provide placeId!","EMP_PL_ID"), 
	EMPTY_PLACE_ICON("Please provide iconUrl!","EMP_PL_ICON"), 
	INVALID_PARENT_PLACE_ID("Invalid parent place id!","INV_PARENT_PLACE"), 
	PLACE_ADDED_SUCCESSFULLY("Place added successfully!","0"), 
	PLACE_UPDATED_SUCCESSFULLY("Place updated successfully!","0"), 
	PLACE_NOT_FOUND("Place not found!","NF_PL"),
	NO_PLACES_FOUND("No places found!","NF_PL"),
	PLACE_DELETE_SUCCESSFULLY("Place deleted successfully!","0"), 
	
	//Airport related
	INVALID_AIRPORT_REQUEST("Invalid airport request!","INV_REQ"), 
	EMPTY_AIRPORT_ID("Please provide airportId!","EMP_AIRPORT_ID"), 
	EMPTY_CITY_ID("Please provide cityId","EMP_CITY_ID"),
	EMPTY_AIRPORT_NAME("Airport name can not be empty", "EMP_AIRPORT_NAME"), 
	EMPTY_AIRPORT_DESCRIPTION("Please provide airport description!","EMP_AIRPORT_DESC"), 
	EMPTY_AIRPORT_TYPE("Please provide airport type!","EMP_AIRPORT_TYPE"), 
	AIRPORT_ADDED_SUCCESSFULLY("Airport added successfully!","0"), 
	AIRPORT_NOT_FOUND("Airport not found!","NF_AIRPORT"),
	AIRPORT_UPDATED_SUCCESSFULLY("Airport updated successfully!","0"), 
	AIRPORT_DELETED_SUCCESSFULLY("Airport deleted successfully!","0"), 
	NO_AIRPORT_FOUND("No airports found for this request!","NF_AIRPORT"),
	;
	



	
	private String message;
	private String code;
	private AboutstaysResponseCode(String message, String code) {
		this.message = message;
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public static AboutstaysResponseCode formatMessage(AboutstaysResponseCode aboutstaysResponseCode, String value){
		if(aboutstaysResponseCode!=null){
			AboutstaysResponseCode commonResponseCode = AboutstaysResponseCode.COMMON;
			commonResponseCode.setCode(aboutstaysResponseCode.code);
			commonResponseCode.setMessage(String.format(aboutstaysResponseCode.message, value));
			return commonResponseCode;
		}
		return aboutstaysResponseCode;
	}

}
