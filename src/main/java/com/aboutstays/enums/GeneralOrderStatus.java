package com.aboutstays.enums;

public enum GeneralOrderStatus {

	INITIATED(1, "INITIATED", "https://aboutstays.s3.amazonaws.com/images/ongoing.png"), 
	PROCESSING(2, "PROCESSING", "https://aboutstays.s3.amazonaws.com/images/processing.png"), 
	PENDING(3, "PROCESSING", "https://aboutstays.s3.amazonaws.com/images/processing.png"),
	PREPAIRING(4, "PROCESSING", "https://aboutstays.s3.amazonaws.com/images/processing.png"),
	UPDATED(5, "UPDATED", "https://aboutstays.s3.amazonaws.com/images/updated.png"),
	COMPLETED(6, "COMPLETED", "https://aboutstays.s3.amazonaws.com/images/completed.png"),
	CANCELLED(7, "CANCELLED", "https://aboutstays.s3.amazonaws.com/images/cancelled.png"),
	SCHEDULED(8,"SCHEDULED","https://aboutstays.s3.amazonaws.com/images/scheduled.png");
	
	private int code;
	private String message;
	private String iconUrl;
	
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}

	public String getIconUrl() {
		return iconUrl;
	}
	
	private GeneralOrderStatus(int code, String message, String iconUrl) {
		this.code = code;
		this.message = message;
		this.iconUrl = iconUrl;
	}
	
	public static boolean isClosed(GeneralOrderStatus generalOrderStatus){
		if(generalOrderStatus!=null){
			if(GeneralOrderStatus.COMPLETED.equals(generalOrderStatus)||GeneralOrderStatus.COMPLETED.equals(generalOrderStatus))
				return true;
		}
		return false;
	}

	public static GeneralOrderStatus findByCode(Integer code) {
		if(code!=null){
			for(GeneralOrderStatus generalOrderStatus : GeneralOrderStatus.values()){
				if(generalOrderStatus.code==code){
					return generalOrderStatus;
				}
			}
		}
		return null;
	}

	public static boolean isScheduled(GeneralOrderStatus generalOrderStatus) {
		if(generalOrderStatus!=null){
			if(GeneralOrderStatus.SCHEDULED.equals(generalOrderStatus)||GeneralOrderStatus.INITIATED.equals(generalOrderStatus))
				return true;
		}
		return false;
	}

	public static RequestDashboardStatus getByOrderStatus(GeneralOrderStatus generalOrderStatus) {
		if(generalOrderStatus==null)
			return null;
		if(GeneralOrderStatus.INITIATED.equals(generalOrderStatus)||GeneralOrderStatus.SCHEDULED.equals(generalOrderStatus)){
			return RequestDashboardStatus.SCHEDULED;
		}
		if(GeneralOrderStatus.PROCESSING.equals(generalOrderStatus)||GeneralOrderStatus.PREPAIRING.equals(generalOrderStatus)||
				GeneralOrderStatus.PENDING.equals(generalOrderStatus)||GeneralOrderStatus.UPDATED.equals(generalOrderStatus)){
			return RequestDashboardStatus.OPEN;
		}
		if(GeneralOrderStatus.CANCELLED.equals(generalOrderStatus)||GeneralOrderStatus.COMPLETED.equals(generalOrderStatus)){
			return RequestDashboardStatus.CLOSED;
		}
		return null;
	}
}
