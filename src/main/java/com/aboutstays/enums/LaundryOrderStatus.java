package com.aboutstays.enums;

public enum LaundryOrderStatus {

	INITIATED(1, "Initiated", ""), 
	PROCESSING(2, "Processing", ""), 
	PENDING(3, "Pending", ""),
	PREPAIRING(4, "Prepairing", ""),
	UPDATED(5, "Updated", ""),
	COMPLETED(6, "Compleated", ""),
	CANCELLED(7, "Cancelled", "");
	
	private int code;
	private String message;
	private String iconUrl;
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getIconUrl() {
		return iconUrl;
	}

	private LaundryOrderStatus(int code, String message, String iconUrl) {
		this.code = code;
		this.message = message;
		this.iconUrl = iconUrl;
	}
	
	public static LaundryOrderStatus findByCode(Integer code){
		if(code!=null){
			for(LaundryOrderStatus laundryOrderStatus : LaundryOrderStatus.values()){
				if(laundryOrderStatus.code==code)
					return laundryOrderStatus;
			}
		}
		return null;
	}
	
	public static RequestDashboardStatus getByLaundryOrderStatus(LaundryOrderStatus laundryOrderStatus){
		if(laundryOrderStatus==null)
			return null;
		if(LaundryOrderStatus.INITIATED.equals(laundryOrderStatus)||LaundryOrderStatus.UPDATED.equals(laundryOrderStatus)){
			return RequestDashboardStatus.SCHEDULED;
		}
		if(LaundryOrderStatus.PROCESSING.equals(laundryOrderStatus)||LaundryOrderStatus.PREPAIRING.equals(laundryOrderStatus)||
				LaundryOrderStatus.PENDING.equals(laundryOrderStatus)){
			return RequestDashboardStatus.OPEN;
		}
		if(LaundryOrderStatus.CANCELLED.equals(laundryOrderStatus)||LaundryOrderStatus.COMPLETED.equals(laundryOrderStatus)){
			return RequestDashboardStatus.CLOSED;
		}
		return null;
	}

}
