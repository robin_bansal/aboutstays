package com.aboutstays.enums;

public enum HotelUpdateBy {
	
	HOTELID;
	
	public static HotelUpdateBy findByValue(String value){
		for(HotelUpdateBy type : HotelUpdateBy.values()){
			if(type.name().equalsIgnoreCase(value)){
				return type;
			}
		}
		return null;
	}
}
