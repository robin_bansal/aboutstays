package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum LoginType{

	MANUAL(1), GMAIL(2), FACEBOOK(3);
	
	private Integer code;
	
	public Integer getCode() {
		return code;
	}
	
	private LoginType(Integer code) {
		this.code = code;
	}

	public static LoginType findLoginTypeByCode(Integer code){
		for(LoginType type : LoginType.values()){
			if(type.code==code){
				return type;
			}
		}
		return null;
	}

	public static LoginType findSignupTypeByValue(String value){
		for(LoginType type : LoginType.values()){
			if(type.name().equalsIgnoreCase(value)){
				return type;
			}
		}
		return null;
	}
	
	public static List<Integer> getTypeList(){
		List<Integer> typeList = new ArrayList<>();
		for(LoginType type : LoginType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}
	
	public static LoginType getDefaultValue(){
		return MANUAL;
	}
}
