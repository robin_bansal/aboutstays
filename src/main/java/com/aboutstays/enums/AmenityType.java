package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum AmenityType {

	HOTEL(1), 
	ROOM(2), 
	ESSENTIALS(3), 
	AWARDS(4), 
	CARD_ACCEPTED(5), 
	PREFERENCE_BASED_AMENITIES(6), 
	TRAVEL_OPTIONS(7),
	DIRECTIONS(8),
	CURRENCY(9),
	DISH_TYPE(10),
	FAVORITES(11),
	SPICE_LEVELS(12),
	STAY_STATS(13),
	HOTEL_INFO_SECTIONS(14),
	;

	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	private AmenityType(int type) {
		this.type = type;
	}

	public static AmenityType findbyType(Integer value) {
		if (value == null)
			return null;
		for (AmenityType type : AmenityType.values()) {
			if (type.getType() == value) {
				return type;
			}
		}
		return null;
	}

	public static List<Integer> getCodeList() {
		List<Integer> codeList = new ArrayList<>();
		for (AmenityType type : AmenityType.values()) {
			codeList.add(type.getType());
		}
		return codeList;
	}

}
