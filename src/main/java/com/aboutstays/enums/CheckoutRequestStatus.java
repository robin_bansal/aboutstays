package com.aboutstays.enums;

public enum CheckoutRequestStatus {

	USER_INITIATED(1), SYSTEM_INITIATED(2), USER_MODIFIED(3), SYSTEM_MODIFIED(4), APPROVED(5);
	
	private int code;
	
	public int getCode() {
		return code;
	}

	private CheckoutRequestStatus(int code) {
		this.code = code;
	}
	
	public static CheckoutRequestStatus findByCode(Integer code){
		if(code!=null){
			for(CheckoutRequestStatus checkoutRequest : CheckoutRequestStatus.values()){
				if(checkoutRequest.code==code)
					return checkoutRequest;
			}
		}
		return null;
	}
}
