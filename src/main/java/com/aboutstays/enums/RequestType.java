package com.aboutstays.enums;

public enum RequestType {
	
	OPEN(1), SCHEDULED(2), CLOSED(3);
	
	private Integer code;

	private RequestType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public static RequestType findValueByType(Integer value){
		for(RequestType type : RequestType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}

}
