package com.aboutstays.enums;

public enum FoodLabels {
	
	VEG(1), NON_VEG(2), SEA_FOOD(3), EGG(4), NONE(5);
	
	private Integer code;

	private FoodLabels(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public FoodLabels findStatusByCode(Integer value){
		for(FoodLabels status : FoodLabels.values()){
			if(status.code == value){
				return status;
			}
		}
		return null;
	}

}
