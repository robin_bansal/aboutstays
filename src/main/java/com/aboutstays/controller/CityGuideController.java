package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CityGuideItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.CityGuideItemByType;
import com.aboutstays.request.dto.AddCityGuideRequest;
import com.aboutstays.request.dto.UpdateCityGuideRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetCityGuideResponse;
import com.aboutstays.services.CityGuideService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CityGuideConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CityGuideConstants.BASE)
public class CityGuideController {
	
	@Autowired
	private CityGuideService cityGuideService;
	
	@Autowired
	private ValidateIdsService validateIdsService;

	@RequestMapping(value=RestMappingConstants.CityGuideConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddCityGuideRequest> getSample(){
		AddCityGuideRequest response = CityGuideConverter.getSampleRequest();
		return new BaseApiResponse<AddCityGuideRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddCityGuideRequest request) throws AboutstaysException{
		ValidateUtils.validateAddCityGuideRequest(request);
		validateIdsService.validateHotelId(request.getId());
		Iterator<CityGuideItemByType> iterator = request.getItemByTypeList().iterator();
		Set<Integer> cityGuideTypeSet = new HashSet<>(); 
		while (iterator.hasNext()) {
			CityGuideItemByType itemByType = iterator.next();
			CityGuideItemType itemType = CityGuideItemType.getByCode(itemByType.getType()); 
			if(itemType==null){
				throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_CITY_GUIDE_ITEM_TYPE);
			}
			else if(cityGuideTypeSet.contains(itemByType.getType())){
				iterator.remove();
			} else {
				cityGuideTypeSet.add(itemByType.getType());
			}
			itemByType.setTypeName(itemType.getDisplayName());
			itemByType.setImageUrl(itemType.getImageUrl());
		}
		
		String response = cityGuideService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_CITY_GUIDE_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCityGuideResponse>> getAll() throws AboutstaysException{
		List<GetCityGuideResponse> response = cityGuideService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_CITY_GUIDE_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetCityGuideResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetCityGuideResponse> get(@RequestParam(value=RestMappingConstants.CityGuideParameters.ID, required=true)String id) throws AboutstaysException{
		GetCityGuideResponse response = cityGuideService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_NOT_FOUND);
		}
		return new BaseApiResponse<GetCityGuideResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.CityGuideParameters.ID, required=true)String id) throws AboutstaysException{
		if(!cityGuideService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_NOT_FOUND);
		}
		Boolean response = cityGuideService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateCityGuideRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CITY_GUIDE_ID);
		}
		if(!cityGuideService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_NOT_FOUND);
		}
		if(CollectionUtils.isNotEmpty(request.getItemByTypeList())){
			for(CityGuideItemByType itemByType : request.getItemByTypeList()){
				CityGuideItemType itemType = CityGuideItemType.getByCode(itemByType.getType()); 
				if(itemType==null){
					throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_CITY_GUIDE_ITEM_TYPE);
				}
				itemByType.setTypeName(itemType.getDisplayName());
				itemByType.setImageUrl(itemType.getImageUrl());
			}
		}
		Boolean response = cityGuideService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	

}
