
package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.constants.StringConstants;
import com.aboutstays.entities.AirportPickup;
import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.entities.CheckinEntity;
import com.aboutstays.entities.FoodOrder;
import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.entities.InternetOrder;
import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.entities.ReservationItem;
import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CheckinRequestStatus;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.enums.RequestDashboardStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.pojos.GeneralDashboardItem;
import com.aboutstays.pojos.GeneralItemDetails;
import com.aboutstays.pojos.HousekeepingItemOrderDetails;
import com.aboutstays.pojos.LaundryItemOrderDetails;
import com.aboutstays.pojos.RequestDetailUIValues;
import com.aboutstays.pojos.RequestDetailsTableEntries;
import com.aboutstays.request.dto.CanceRequestByUosID;
import com.aboutstays.request.dto.CreateDashboardRequest;
import com.aboutstays.request.dto.UpdateRequestDashboardOrders;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.CreateDashboardResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;
import com.aboutstays.response.dto.GetHotelGeneralInfoResponse;
import com.aboutstays.response.dto.RequestDashboardItemDetails;
import com.aboutstays.services.AirportPickupService;
import com.aboutstays.services.BellBoyOrderService;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.CommuteOrderService;
import com.aboutstays.services.FoodOrdersService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HKOrderService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.InternetOrderService;
import com.aboutstays.services.LateCheckoutRequestService;
import com.aboutstays.services.LaundryOrderService;
import com.aboutstays.services.MaintenanceRequestService;
import com.aboutstays.services.ReservationItemService;
import com.aboutstays.services.ReservationOrderService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.UserService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.RequestConstants.BASE)
public class RequestDashboardController {

	
	@Autowired
	private UserOptedServicesService userOptedServicesService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StaysService staysService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private GeneralServicesService generalServicesService;
	
	@Autowired
	private FoodOrdersService foodOrdersService;

	@Autowired
	private LaundryOrderService laundryOrderService;
	
	@Autowired
	private HKOrderService hkOrderService;
	
	@Autowired
	private ValidateIdsService validateIdService;
	
	@Autowired
	private CommuteOrderService commuteOrderService;
	
	@Autowired
	private AirportPickupService apService;
	
	@Autowired
	private InternetOrderService ioService;
	
	@Autowired
	private LateCheckoutRequestService lcrService;
	
	@Autowired
	private ReservationOrderService rsService;
	
	@Autowired
	private CheckinService checkinService;
	
	@Autowired
	private MaintenanceRequestService mrService;
	
	@Autowired
	private BellBoyOrderService bellboyOrderService;
	
	@Autowired
	private ReservationItemService reservationItemService;
	
	
	@RequestMapping(value=RestMappingConstants.RequestConstants.CREATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<CreateDashboardResponse> createDashboard(@RequestBody(required=true)CreateDashboardRequest request) throws AboutstaysException{
		long currentTime = new Date().getTime();
		ValidateUtils.validateCreateDashboardRequest(request);
		validateIdsRequest(request.getUserId(), request.getHotelId(), request.getStaysId());
		List<UserOptedService> listUOS = userOptedServicesService.getByUserStaysAndHotel(request.getStaysId(),
				request.getUserId(), request.getHotelId());
		if (CollectionUtils.isEmpty(listUOS)) {
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		CreateDashboardResponse createDashboardResponse = new CreateDashboardResponse();
		List<GeneralDashboardItem> openDashboardItems = new ArrayList<>();
		List<GeneralDashboardItem> scheduledDashboardItems = new ArrayList<>();
		List<GeneralDashboardItem> closedDashboardItems = new ArrayList<>();
		createDashboardResponse.setOpenDashboardItems(openDashboardItems);
		createDashboardResponse.setScheduledDashboardItems(scheduledDashboardItems);
		createDashboardResponse.setClosedDashboardItems(closedDashboardItems);
		
		Map<String, GetGeneralServiceResponse> gsIdVsResponse = new HashMap<>();
		
		for(UserOptedService uos : listUOS){
			
			GeneralDashboardItem generalDashboardItem = new GeneralDashboardItem();
			
			RefCollectionType typeEnum = RefCollectionType.findValueByType(uos.getRefCollectionType());
			if (typeEnum == null) {
				continue;
			}
			
			String gsId = uos.getGsId();
			GetGeneralServiceResponse gsResponse = gsIdVsResponse.get(gsId);
			if (gsResponse == null) {
				gsResponse = generalServicesService.getById(gsId);
				if (gsResponse == null) {
					continue;
				}
				gsIdVsResponse.put(gsId, gsResponse);
			}
			String expenseGsId = gsResponse.getExpenseId();
			if(!StringUtils.isEmpty(expenseGsId)){
				GetGeneralServiceResponse expenseGsResponse = gsIdVsResponse.get(expenseGsId);
				if(expenseGsResponse==null){
					expenseGsResponse = generalServicesService.getById(expenseGsId);
				}
				if(expenseGsResponse!=null){
					gsIdVsResponse.put(expenseGsId, expenseGsResponse);
					gsResponse = expenseGsResponse;
					gsId = expenseGsId;
				}
			}

			generalDashboardItem.setUosId(uos.getId());
			generalDashboardItem.setTitle(gsResponse.getName());
			generalDashboardItem.setImageUrl(gsResponse.getActivatedImageUrl());
			
			String refId = uos.getRefId();
			String description = null;
			
			StringBuilder itemNameBuilder = new StringBuilder();
			RequestDashboardStatus requestDashboardStatus = null;
			GeneralOrderStatus generalOrderStatus = null;
			switch(typeEnum){
			case AIRPORT_PICKUP:
				AirportPickup airportResponse = apService.getEntity(refId);
				if(airportResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(airportResponse.getStatus());
					long deliveryTime = airportResponse.getPickupDateTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							apService.changeOrderStatus(refId, generalOrderStatus);
							airportResponse = apService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(airportResponse.getPickupDateTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(airportResponse.getUpdated());
						
					if(airportResponse.getDrop()){
						itemNameBuilder.append("Airport Drop");
					} else {
						itemNameBuilder.append("Airport Pickup");
					}
						
				} else {
					continue;
				}
					
				break;
			case COMMUTE:
				CommuteOrder commuteResponse = commuteOrderService.getEntity(refId);
				if(commuteResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(commuteResponse.getStatus());
					long deliveryTime = commuteResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							commuteOrderService.changeOrderStatus(refId, generalOrderStatus);
							commuteResponse = commuteOrderService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(commuteResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(commuteResponse.getUpdated());
						
					itemNameBuilder.append(commuteResponse.getPackageName());
				} else {
					continue;
				}
				
				break;
			case RESERVATION :
				ReservationOrder reservationResponse = rsService.getEntity(refId);
				if(reservationResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(reservationResponse.getStatus());
					long deliveryTime = reservationResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							rsService.changeOrderStatus(refId, generalOrderStatus);
							reservationResponse = rsService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(reservationResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(reservationResponse.getUpdated());	
					
					itemNameBuilder.append(reservationResponse.getName());
				} else {
					continue;
				}
				break;
			
			case INTERNET:
				InternetOrder internetResponse = ioService.getEntity(refId);
				
				if(internetResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(internetResponse.getStatus());
					long deliveryTime = internetResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							ioService.changeOrderStatus(refId, generalOrderStatus);
							internetResponse = ioService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(internetResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(internetResponse.getUpdated());
					if(internetResponse.getInternetPack() != null){
						itemNameBuilder.append(internetResponse.getInternetPack().getDescription());
					}
					
				} else {
					continue;
				}
				break;	
				
			case MAINTENANCE:
				MaintenanceRequestEntity maintenanceResponse = mrService.getEntity(refId);
				
				if(maintenanceResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(maintenanceResponse.getStatus());
					long deliveryTime = maintenanceResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							mrService.changeOrderStatus(refId, generalOrderStatus);
							maintenanceResponse = mrService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(maintenanceResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(maintenanceResponse.getUpdated());
					itemNameBuilder.append(maintenanceResponse.getItemName());
					
				} else {
					continue;
				}
				break;	
				
			case BELL_BOY:
				BellBoyOrder bellboyOrderResponse = bellboyOrderService.getEntity(refId);
				
				if(bellboyOrderResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(bellboyOrderResponse.getStatus());
					long deliveryTime = bellboyOrderResponse.getPreferredTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							bellboyOrderService.changeOrderStatus(refId, generalOrderStatus);
							bellboyOrderResponse = bellboyOrderService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(bellboyOrderResponse.getPreferredTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(bellboyOrderResponse.getUpdated());
					itemNameBuilder.append("Bell Boy");
					
				} else {
					continue;
				}
				break;	
				
			case LATE_CHECKOUT:
				LateCheckoutRequest lateCheckoutResponse = lcrService.getEntity(refId);
				
				if(lateCheckoutResponse != null){
					generalOrderStatus = GeneralOrderStatus.findByCode(lateCheckoutResponse.getStatus());
					long deliveryTime = lateCheckoutResponse.getCheckoutTime() + lateCheckoutResponse.getCheckoutDate();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							lcrService.changeOrderStatus(refId, generalOrderStatus);
							lateCheckoutResponse = lcrService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(lateCheckoutResponse.getCheckoutTime() + lateCheckoutResponse.getCheckoutDate());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(lateCheckoutResponse.getUpdated());
					itemNameBuilder.append("Late Checkout");
					
				} else {
					continue;
				}
				break;		
									
			case FOOD:
				FoodOrder foodResponse = foodOrdersService.getEntity(refId);
				if (foodResponse != null && CollectionUtils.isNotEmpty(foodResponse.getFoodItemOrders())) {
					generalOrderStatus = GeneralOrderStatus.findByCode(foodResponse.getStatus());
					long deliveryTime = foodResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							foodOrdersService.changeOrderStatus(refId, generalOrderStatus);
							foodResponse = foodOrdersService.getEntity(refId);
						}
					}
					
					generalDashboardItem.setOrderedTime(foodResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(foodResponse.getUpdated());

					Iterator<FoodItemOrderDetails> fIterator = foodResponse.getFoodItemOrders().iterator();
					while(fIterator.hasNext()){
						FoodItemOrderDetails foodItemOrderDetails = fIterator.next();
						itemNameBuilder.append(foodItemOrderDetails.getItemName());
						if(fIterator.hasNext()){
							itemNameBuilder.append(", ");
						}
					}

				} else {
					continue;
				}
				break;
			case HOUSEKEEPING:
				HousekeepingOrder hkOrderResponse = hkOrderService.getEntity(refId);
				if(hkOrderResponse != null && CollectionUtils.isNotEmpty(hkOrderResponse.getHkItemOrders())){
					
					generalOrderStatus = GeneralOrderStatus.findByCode(hkOrderResponse.getStatus());
					long deliveryTime = hkOrderResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							hkOrderService.changeOrderStatus(refId, generalOrderStatus);
							hkOrderResponse = hkOrderService.getEntity(refId);
						}
					}
					
					generalDashboardItem.setOrderedTime(hkOrderResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(hkOrderResponse.getUpdated());
					
					Iterator<HousekeepingItemOrderDetails> iterator = hkOrderResponse.getHkItemOrders().iterator();
					while(iterator.hasNext()){
						HousekeepingItemOrderDetails hkItemOrderDetails = iterator.next();
						itemNameBuilder.append(hkItemOrderDetails.getName());
						if(iterator.hasNext()){
							itemNameBuilder.append(", ");
						}
					}
				} else {
					continue;
				}
				break;
			case LAUNDRY:
				LaundryOrder laundryResponse = laundryOrderService.getEntity(refId);
				if (laundryResponse != null && CollectionUtils.isNotEmpty(laundryResponse.getLaundryItemOrders())) {
					
					generalOrderStatus = GeneralOrderStatus.findByCode(laundryResponse.getStatus());
					long deliveryTime = laundryResponse.getDeliveryTime();
					if(deliveryTime<=currentTime){
						if(GeneralOrderStatus.isScheduled(generalOrderStatus)){
							generalOrderStatus = GeneralOrderStatus.PROCESSING;
							laundryOrderService.changeOrderStatus(refId, generalOrderStatus);
							laundryResponse = laundryOrderService.getEntity(refId);
						}
					}
					generalDashboardItem.setOrderedTime(laundryResponse.getDeliveryTime());
					generalDashboardItem.setStatus(generalOrderStatus.getMessage());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					generalDashboardItem.setUpdatedTime(laundryResponse.getUpdated());
					
					Iterator<LaundryItemOrderDetails> iterator = laundryResponse.getLaundryItemOrders().iterator();
					while(iterator.hasNext()){
						LaundryItemOrderDetails laundryItemOrderDetails = iterator.next();
						itemNameBuilder.append(laundryItemOrderDetails.getItemName());
						if(iterator.hasNext()){
							itemNameBuilder.append(", ");
						}
					}
				} else {
					continue;
				}
				break;
				
			case EARLY_CHECK_IN:
				CheckinResponse checkinResponse = checkinService.getById(refId);
				
				if(checkinResponse != null){
					CheckinRequestStatus checkInStatus = CheckinRequestStatus.findByCode(checkinResponse.getStatus());
					generalOrderStatus = CheckinRequestStatus.getGeneralOrderStatus(checkInStatus);
					
					if(checkinResponse.getStatus() == CheckinRequestStatus.SYSTEM_INITIATED.getCode() 
							|| checkinResponse.getStatus() == CheckinRequestStatus.USER_INITIATED.getCode()){
						if(checkinResponse.getCreated() != null)
							generalDashboardItem.setOrderedTime(checkinResponse.getCreated());
					} else if(checkinResponse.getStatus() == CheckinRequestStatus.SYSTEM_MODIFIED.getCode() 
							|| checkinResponse.getStatus() == CheckinRequestStatus.USER_MODIFIED.getCode()
							|| checkinResponse.getStatus() == CheckinRequestStatus.APPROVED.getCode()){
						if(checkinResponse.getUpdated() != null)
							generalDashboardItem.setOrderedTime(checkinResponse.getUpdated());
					}
					generalDashboardItem.setUpdatedTime(checkinResponse.getUpdated());
					
					GetHotelGeneralInfoResponse hotelResponse = hotelService.getGeneralInfoById(request.getHotelId());
					if(hotelResponse != null && hotelResponse.getGeneralInformation() != null) {
						generalDashboardItem.setImageUrl(hotelResponse.getGeneralInformation().getLogoUrl());
					}
					
					generalDashboardItem.setStatus(checkInStatus.getDisplayName());
					generalDashboardItem.setStatusImageUrl(generalOrderStatus.getIconUrl());
					itemNameBuilder.append("early check-in");
					
				} else {
					continue;
				}
				break;		
			default:
				continue;
			
			}
			
			requestDashboardStatus = GeneralOrderStatus.getByOrderStatus(generalOrderStatus);
			description = itemNameBuilder.toString();
			generalDashboardItem.setDescription(description);
			if(requestDashboardStatus==null){
				//requestDashboardStatus = RequestDashboardStatus.SCHEDULED;
				continue;
			}
			switch (requestDashboardStatus) {
			case CLOSED:
				closedDashboardItems.add(generalDashboardItem);
				break;
			case OPEN:
				openDashboardItems.add(generalDashboardItem);
				break;
			case SCHEDULED:
				scheduledDashboardItems.add(generalDashboardItem);
				break;
			}
		}
		
		if(CollectionUtils.isNotEmpty(closedDashboardItems)) {
			Collections.sort(closedDashboardItems, new Comparator<GeneralDashboardItem>() {
				@Override
				public int compare(GeneralDashboardItem o1, GeneralDashboardItem o2) {
					Long updated1 = o1.getUpdatedTime();
					Long updated2 = o2.getUpdatedTime();
					return updated2.compareTo(updated1);
				}
			});
		}
		if(CollectionUtils.isNotEmpty(openDashboardItems)) {
			Collections.sort(openDashboardItems, new Comparator<GeneralDashboardItem>() {

				@Override
				public int compare(GeneralDashboardItem o1, GeneralDashboardItem o2) {
					Long updated1 = o1.getUpdatedTime();
					Long updated2 = o2.getUpdatedTime();
					return updated2.compareTo(updated1);
				}
			});
		}
		if(CollectionUtils.isEmpty(scheduledDashboardItems)) {
			Collections.sort(scheduledDashboardItems, new Comparator<GeneralDashboardItem>() {

				@Override
				public int compare(GeneralDashboardItem o1, GeneralDashboardItem o2) {
					Long updated1 = o1.getUpdatedTime();
					Long updated2 = o2.getUpdatedTime();
					return updated2.compareTo(updated1);
				}
			});
		}
		
//		// For early checkIn request
//		CheckinResponse checkinResponse = checkinService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
//		if(checkinResponse != null) {
//			if(checkinResponse.getEarlyCheckinRequest() != null) {
//				if(checkinResponse.getEarlyCheckinRequest().isEarlyCheckinRequested()) {
//					CheckinRequestStatus checkInStatus = CheckinRequestStatus.findByCode(checkinResponse.getStatus());
//					if(checkInStatus != null) {
//						RequestDashboardStatus status = CheckinRequestStatus.getByOrderStatus(checkInStatus);
//						if(status != null) {
//							GeneralDashboardItem dashboardItem = new GeneralDashboardItem();
//							GetHotelGeneralInfoResponse hotelResponse = hotelService.getGeneralInfoById(request.getHotelId());
//							if(hotelResponse != null && hotelResponse.getGeneralInformation() != null) {
//								dashboardItem.setImageUrl(hotelResponse.getGeneralInformation().getLogoUrl());
//							}
//							dashboardItem.setStatus(checkInStatus.getDisplayName());
//							if(checkinResponse.getStatus() == CheckinRequestStatus.SYSTEM_INITIATED.getCode() 
//									|| checkinResponse.getStatus() == CheckinRequestStatus.USER_INITIATED.getCode()){
//								if(checkinResponse.getCreated() != null)
//									dashboardItem.setOrderedTime(checkinResponse.getCreated());
//							} else if(checkinResponse.getStatus() == CheckinRequestStatus.SYSTEM_MODIFIED.getCode() 
//									|| checkinResponse.getStatus() == CheckinRequestStatus.USER_MODIFIED.getCode()
//									|| checkinResponse.getStatus() == CheckinRequestStatus.APPROVED.getCode()){
//								if(checkinResponse.getUpdated() != null)
//									dashboardItem.setOrderedTime(checkinResponse.getUpdated());
//							}
//							
//							dashboardItem.setTitle("Early Check-IN");
//							dashboardItem.setDescription("early check-in");
//							switch(status) {
//							case CLOSED:
//								closedDashboardItems.add(dashboardItem);
//								break;
//							case OPEN:
//								openDashboardItems.add(dashboardItem);
//								break;
//							case SCHEDULED:
//								scheduledDashboardItems.add(dashboardItem);
//								break;
//							}
//						}
//					}
//				}
//			}
//		}
		
		return new BaseApiResponse<CreateDashboardResponse>(false, createDashboardResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RequestConstants.GET_DETAILS, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<RequestDashboardItemDetails> getDetails(@RequestParam(value=RestMappingConstants.RequestParameters.uosId, required=true)String uosId) throws AboutstaysException{
		UserOptedService optedService = userOptedServicesService.getEntity(uosId);
		if(optedService == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		RequestDashboardItemDetails itemDetails = new RequestDashboardItemDetails();
		itemDetails.setUosId(optedService.getId());
		GetGeneralServiceResponse response = generalServicesService.getById(optedService.getGsId());
		if(response != null){
			itemDetails.setRequestType(response.getName());
		
			String requestId =  response.getExpenseId();
			if(!StringUtils.isEmpty(requestId)){
				response = generalServicesService.getById(requestId);
				if(response != null){
					itemDetails.setRequestType(response.getName());
				}
			}
		}
		
		RequestDetailsTableEntries tableEntries = new RequestDetailsTableEntries();
		RequestDetailUIValues titles = new RequestDetailUIValues();
		titles.setValue1(StringConstants.ITEM);
		titles.setValue2(StringConstants.QTY);
		titles.setValue3(StringConstants.RATE);
		
		List<RequestDetailUIValues> rows = new ArrayList<>();
		
		RefCollectionType typeEnum = RefCollectionType.findValueByType(optedService.getRefCollectionType());
		if(typeEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		String refId = optedService.getRefId();
		List<GeneralItemDetails> listGeneralItems = new ArrayList<>();
		double total = 0.0;
		switch(typeEnum){
		case FOOD: {
			FoodOrder foodResponse = foodOrdersService.getEntity(refId);
			if(foodResponse != null){
				itemDetails.setLastUpdatedTime(foodResponse.getUpdated());
				itemDetails.setRequestedTime(foodResponse.getCreated());
				itemDetails.setScheduledTime(foodResponse.getDeliveryTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(foodResponse.getStatus()).getMessage());
				itemDetails.setEditable(true);
				
				if(foodResponse.getFoodItemOrders() != null){
					listGeneralItems = new ArrayList<>();
					for(FoodItemOrderDetails order : foodResponse.getFoodItemOrders()){
						if(order == null){
							continue;
						}
						GeneralItemDetails generalItem = new GeneralItemDetails();
						generalItem.setName(order.getItemName());
						generalItem.setQuantity(order.getQuantity());
						generalItem.setPrice(order.getPrice());
						generalItem.setId(order.getFoodItemId());
						total = total + (order.getQuantity() * order.getPrice());
						listGeneralItems.add(generalItem);
						
						RequestDetailUIValues ithRow = new RequestDetailUIValues();
						ithRow.setValue1(generalItem.getName());
						ithRow.setValue2(String.valueOf(generalItem.getQuantity()));
						ithRow.setValue3(String.valueOf(generalItem.getPrice()));
						rows.add(ithRow);
					}
					itemDetails.setListGeneralItemDetails(listGeneralItems);
					itemDetails.setTotal(total);
				}
			}

			break;
		}
		case AIRPORT_PICKUP: {
			AirportPickup airportResponse = apService.getEntity(refId);
			if(airportResponse != null){
				itemDetails.setLastUpdatedTime(airportResponse.getUpdated());
				itemDetails.setRequestedTime(airportResponse.getCreated());
				itemDetails.setScheduledTime(airportResponse.getPickupDateTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(airportResponse.getStatus()).getMessage());
				itemDetails.setEditable(false);
				
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(airportResponse.getId());
				generalItem.setPrice(airportResponse.getPrice());
				if(airportResponse.getDrop()){
					generalItem.setName("Airport drop");
				} else {
					generalItem.setName("Airport pickup");
				}
				
				generalItem.setQuantity(1);
				total = total + airportResponse.getPrice();
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(StringConstants.PRICE);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				ithRow.setValue3(String.valueOf(generalItem.getPrice()));
				rows.add(ithRow);
			}
			break;
		}
		case COMMUTE: {
			CommuteOrder commuteResponse = commuteOrderService.getEntity(refId);
			if(commuteResponse != null){
				itemDetails.setLastUpdatedTime(commuteResponse.getUpdated());
				itemDetails.setRequestedTime(commuteResponse.getCreated());
				itemDetails.setScheduledTime(commuteResponse.getDeliveryTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(commuteResponse.getStatus()).getMessage());
				itemDetails.setEditable(false);
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(commuteResponse.getId());
				generalItem.setPrice(commuteResponse.getPrice());
				generalItem.setName(commuteResponse.getPackageName());
				generalItem.setQuantity(1);
				total = total + commuteResponse.getPrice();
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(StringConstants.PRICE);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				ithRow.setValue3(String.valueOf(generalItem.getPrice()));
				rows.add(ithRow);
			}

			break;
		}
		case RESERVATION : {
			ReservationOrder reservationResponse = rsService.getEntity(refId);
			if(reservationResponse != null){
				ReservationItem reservationItem = reservationItemService.getEntityById(reservationResponse.getRsItemId());
				boolean isAmountApplicable = reservationItem==null?false:reservationItem.isAmountApplicable();
				itemDetails.setLastUpdatedTime(reservationResponse.getUpdated());
				itemDetails.setRequestedTime(reservationResponse.getCreated());
				itemDetails.setScheduledTime(reservationResponse.getDeliveryTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(reservationResponse.getStatus()).getMessage());
				itemDetails.setEditable(false);
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(reservationResponse.getId());
				generalItem.setPrice(reservationResponse.getPrice());
				generalItem.setName(reservationResponse.getName());
				generalItem.setQuantity(reservationResponse.getPax());
				total = total + reservationResponse.getPrice();
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				if(isAmountApplicable){
					titles.setValue2(StringConstants.PAX);
					titles.setValue3(StringConstants.PRICE);
					ithRow.setValue2(String.valueOf(generalItem.getQuantity()));
					ithRow.setValue3(String.valueOf(generalItem.getPrice()));
				} else {
					titles.setValue2(null);
					titles.setValue3(StringConstants.PAX);
					ithRow.setValue3(String.valueOf(generalItem.getQuantity()));
				}
				rows.add(ithRow);
			}
			break;
		} 
		case INTERNET : {
			InternetOrder internetResponse = ioService.getEntity(refId);
			if(internetResponse != null){
				itemDetails.setLastUpdatedTime(internetResponse.getUpdated());
				itemDetails.setRequestedTime(internetResponse.getCreated());
				itemDetails.setScheduledTime(internetResponse.getDeliveryTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(internetResponse.getStatus()).getMessage());
				itemDetails.setEditable(false);
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				if(internetResponse.getInternetPack() != null){
					generalItem.setId(internetResponse.getId());
					generalItem.setPrice(internetResponse.getInternetPack().getPrice());
					generalItem.setName(internetResponse.getInternetPack().getDescription());
					total = total + internetResponse.getInternetPack().getPrice();
					generalItem.setQuantity(1);
				}
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(StringConstants.RATE);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				if(internetResponse.getInternetPack() != null && !internetResponse.getInternetPack().isComplementry()){
					ithRow.setValue3(String.valueOf(generalItem.getPrice()));
				} else {
					ithRow.setValue3("-");
				}
				rows.add(ithRow);
			}
			break;
		}
		
		case MAINTENANCE : {
			MaintenanceRequestEntity maintenanceResponse = mrService.getEntity(refId);
			if(maintenanceResponse != null){
				itemDetails.setLastUpdatedTime(maintenanceResponse.getUpdated());
				itemDetails.setRequestedTime(maintenanceResponse.getCreated());
				itemDetails.setScheduledTime(maintenanceResponse.getDeliveryTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(maintenanceResponse.getStatus()).getMessage());
				itemDetails.setEditable(false);
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(maintenanceResponse.getId());
				generalItem.setName(maintenanceResponse.getItemName());
				generalItem.setQuantity(1);
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(StringConstants.PROBLEM_TYPE);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				ithRow.setValue3(maintenanceResponse.getProblemType());
				rows.add(ithRow);
			}
			break;
		}
		
		case LATE_CHECKOUT : {
			LateCheckoutRequest checkoutRespones = lcrService.getEntity(refId);
			if(checkoutRespones != null){
				itemDetails.setLastUpdatedTime(checkoutRespones.getUpdated());
				itemDetails.setRequestedTime(checkoutRespones.getCreated());
				itemDetails.setScheduledTime(checkoutRespones.getDeliveryTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(checkoutRespones.getStatus()).getMessage());
				itemDetails.setEditable(false);
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(checkoutRespones.getId());
				generalItem.setName("Late Check-Out");
				generalItem.setQuantity(1);
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(StringConstants.PRICE);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				ithRow.setValue3(String.valueOf(generalItem.getPrice()));
				rows.add(ithRow);
			}
			break;
		}
		
		case BELL_BOY : {
			BellBoyOrder bellboyOrderResponse = bellboyOrderService.getEntity(refId);
			if(bellboyOrderResponse != null){
				itemDetails.setLastUpdatedTime(bellboyOrderResponse.getUpdated());
				itemDetails.setRequestedTime(bellboyOrderResponse.getCreated());
				itemDetails.setScheduledTime(bellboyOrderResponse.getPreferredTime());
				itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(bellboyOrderResponse.getStatus()).getMessage());
				itemDetails.setEditable(false);
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(bellboyOrderResponse.getId());
				generalItem.setName("Bell Boy");
				generalItem.setQuantity(1);
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(null);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				rows.add(ithRow);
			}
			break;
		}
		
		case EARLY_CHECK_IN : {
			CheckinEntity checkinEntity = checkinService.getEntity(refId);
			if(checkinEntity != null){
				itemDetails.setLastUpdatedTime(checkinEntity.getUpdated());
				itemDetails.setRequestedTime(checkinEntity.getCreated());
				itemDetails.setRequestStatus(CheckinRequestStatus.findByCode(checkinEntity.getStatus()).getDisplayName());
				itemDetails.setEditable(false);
				if(checkinEntity.getEarlyCheckinRequest() != null) {
					total = checkinEntity.getEarlyCheckinRequest().getCharges();
					String earlyTime = checkinEntity.getEarlyCheckinRequest().getRequestedCheckinTime();
					long scheduledTime = DateUtil.convertStringToLong(DateUtil.MENU_ITEM_TIME_FORMAT, earlyTime);
					itemDetails.setScheduledTime(scheduledTime);
				}
					
				GeneralItemDetails generalItem = new GeneralItemDetails();
				generalItem.setId(checkinEntity.getStaysId());
				generalItem.setName("Early Check-In");
				generalItem.setQuantity(1);
				generalItem.setPrice(total);
				listGeneralItems.add(generalItem);
				
				itemDetails.setListGeneralItemDetails(listGeneralItems);
				itemDetails.setTotal(total);
				
				titles.setValue2(null);
				titles.setValue3(StringConstants.PRICE);
				RequestDetailUIValues ithRow = new RequestDetailUIValues();
				ithRow.setValue1(generalItem.getName());
				ithRow.setValue3(String.valueOf(generalItem.getPrice()));
				rows.add(ithRow);
			}
			break;
		}
		
		case LAUNDRY:
			LaundryOrder laundryResponse = laundryOrderService.getEntity(refId);
			if(laundryResponse == null){
				throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ORDER_NOT_EXISTS);
			}
			itemDetails.setLastUpdatedTime(laundryResponse.getUpdated());
			itemDetails.setRequestedTime(laundryResponse.getCreated());
			itemDetails.setScheduledTime(laundryResponse.getDeliveryTime());
			itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(laundryResponse.getStatus()).getMessage());
			itemDetails.setEditable(true);
			
			if(laundryResponse.getLaundryItemOrders() != null){
				listGeneralItems = new ArrayList<>();
				for(LaundryItemOrderDetails order : laundryResponse.getLaundryItemOrders()){
					if(order == null){
						continue;
					}
					GeneralItemDetails generalItem = new GeneralItemDetails();
					generalItem.setName(order.getItemName());
					generalItem.setQuantity(order.getQuantity());
					generalItem.setPrice(order.getPrice());
					generalItem.setId(order.getLaundryItemId());
					total = total + (order.getQuantity() * order.getPrice());
					listGeneralItems.add(generalItem);
					
					RequestDetailUIValues ithRow = new RequestDetailUIValues();
					ithRow.setValue1(generalItem.getName());
					ithRow.setValue2(String.valueOf(generalItem.getQuantity()));
					ithRow.setValue3(String.valueOf(generalItem.getPrice()));
					rows.add(ithRow);
				}
				itemDetails.setTotal(total);
				itemDetails.setListGeneralItemDetails(listGeneralItems);
			}
			break;
		case HOUSEKEEPING:
			HousekeepingOrder hkOrderResponse = hkOrderService.getEntity(refId);
			if(hkOrderResponse == null){
				throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ORDER_NOT_EXISTS);
			}
			itemDetails.setLastUpdatedTime(hkOrderResponse.getUpdated());
			itemDetails.setRequestedTime(hkOrderResponse.getCreated());
			itemDetails.setScheduledTime(hkOrderResponse.getDeliveryTime());
			itemDetails.setRequestStatus(GeneralOrderStatus.findByCode(hkOrderResponse.getStatus()).getMessage());
			itemDetails.setEditable(true);
			
			if(hkOrderResponse.getHkItemOrders() != null){
				listGeneralItems = new ArrayList<>();
				for(HousekeepingItemOrderDetails order : hkOrderResponse.getHkItemOrders()){
					if(order == null){
						continue;
					}
					GeneralItemDetails generalItem = new GeneralItemDetails();
					generalItem.setName(order.getName());
					generalItem.setQuantity(order.getQuantity());
					//generalItem.setPrice(order.getPrice());
					generalItem.setId(order.getHkItemId());
					total = total + (order.getQuantity() * order.getPrice());
					listGeneralItems.add(generalItem);
					
					titles.setValue2(null);
					titles.setValue3(StringConstants.QTY);
					RequestDetailUIValues ithRow = new RequestDetailUIValues();
					ithRow.setValue1(generalItem.getName());
					ithRow.setValue3(String.valueOf(generalItem.getQuantity()));
					rows.add(ithRow);
				}
				//itemDetails.setTotal(total);
				itemDetails.setListGeneralItemDetails(listGeneralItems);
			}
			break;
		}
		tableEntries.setTitles(titles);
		tableEntries.setRows(rows);
		itemDetails.setTableEntries(tableEntries);
		return new BaseApiResponse<RequestDashboardItemDetails>(false, itemDetails, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RequestConstants.CANCEL_ORDER, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> cancelOrder(@RequestBody(required=true)CanceRequestByUosID request) throws AboutstaysException{
		ValidateUtils.validateCancelRequest(request);
		UserOptedService optedService = userOptedServicesService.getEntity(request.getUosId());
		if(optedService == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		String refId = optedService.getRefId();
		RefCollectionType refEnum = RefCollectionType.findValueByType(optedService.getRefCollectionType());
		if(refEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		Boolean response = false;
		switch (refEnum) {
		case FOOD:
			 response = foodOrdersService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case HOUSEKEEPING:
			response = hkOrderService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case LAUNDRY:
			response = laundryOrderService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case AIRPORT_PICKUP:
			response = apService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case COMMUTE :
			response = commuteOrderService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case RESERVATION : 
			response = rsService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case INTERNET:
			response = ioService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case MAINTENANCE:
			response = mrService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case BELL_BOY:
			response = bellboyOrderService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
			break;
		case LATE_CHECKOUT:
			response = lcrService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
		}
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.CANCEL_REQUEST_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RequestConstants.EDIT_ORDER, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> editOrder(@RequestBody(required=true)UpdateRequestDashboardOrders request) throws AboutstaysException{
		validateIdService.validateUOSId(request.getUosId());
		if(CollectionUtils.isMapEmpty(request.getMapItemIdVSQuantity())){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_REQUEST_DASHBOARD_ITEM_ORDER_INVALID);
		}
		UserOptedService optedService = userOptedServicesService.getEntity(request.getUosId());
		String refId = optedService.getRefId();
		RefCollectionType refEnum = RefCollectionType.findValueByType(optedService.getRefCollectionType());
		if(refEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		String response = "";
		switch(refEnum){
		case FOOD: {
			FoodOrder foodOrderResponse = foodOrdersService.getEntity(refId);
			
			boolean updateOrder = false;
			List<FoodItemOrderDetails> listFoodOrderDetails = new ArrayList<>();
			for(FoodItemOrderDetails itemDetails : foodOrderResponse.getFoodItemOrders()){
				if(request.getMapItemIdVSQuantity().containsKey(itemDetails.getFoodItemId())){
					FoodItemOrderDetails foodItemOrderDetails = new FoodItemOrderDetails();
					int quantity = request.getMapItemIdVSQuantity().get(itemDetails.getFoodItemId());
					if(quantity <= 0){
						continue;
					} else{
						updateOrder = true;
						foodItemOrderDetails.setFoodItemId(itemDetails.getFoodItemId());
						foodItemOrderDetails.setImageUrl(itemDetails.getImageUrl());
						foodItemOrderDetails.setItemName(itemDetails.getItemName());
						foodItemOrderDetails.setPrice(itemDetails.getPrice());
						foodItemOrderDetails.setQuantity(quantity);
						foodItemOrderDetails.setType(itemDetails.getType());
						foodItemOrderDetails.setTypeName(itemDetails.getTypeName());
						listFoodOrderDetails.add(foodItemOrderDetails);
					}
				}
			}
			
			if(updateOrder){
				foodOrderResponse.setFoodItemOrders(listFoodOrderDetails);
				foodOrderResponse.setUpdated(new Date().getTime());
				boolean updated = foodOrdersService.updateFoodOrderRequest(foodOrderResponse);
				foodOrdersService.changeOrderStatus(refId, GeneralOrderStatus.UPDATED);
				if(updated){
					response = "your order has been updated";
				} else{
					throw new AboutstaysException(AboutstaysResponseCode.UPDATE_FOOD_ORDER_FAILED);
				}
			} else {
				foodOrdersService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
				response = "your order has been cancelled";
			}
			break;
		}

		case HOUSEKEEPING: {
			HousekeepingOrder hkOrderResponse = hkOrderService.getEntity(refId);
			
			boolean updateOrder = false;
			List<HousekeepingItemOrderDetails> listHKOrderDetails = new ArrayList<>();
			for(HousekeepingItemOrderDetails itemDetails : hkOrderResponse.getHkItemOrders()){
				if(request.getMapItemIdVSQuantity().containsKey(itemDetails.getHkItemId())){
					HousekeepingItemOrderDetails hkItemOrderDetails = new HousekeepingItemOrderDetails();
					int quantity = request.getMapItemIdVSQuantity().get(itemDetails.getHkItemId());
					if(quantity <= 0){
						continue;
					} 
					else{
						updateOrder = true;
						hkItemOrderDetails.setHkItemId(itemDetails.getHkItemId());
						hkItemOrderDetails.setImageUrl(itemDetails.getImageUrl());
						hkItemOrderDetails.setName(itemDetails.getName());
						hkItemOrderDetails.setPrice(itemDetails.getPrice());
						hkItemOrderDetails.setQuantity(quantity);
						hkItemOrderDetails.setType(itemDetails.getType());
						hkItemOrderDetails.setTypeName(itemDetails.getTypeName());
						listHKOrderDetails.add(hkItemOrderDetails);
					}
				}
			}
			
			if(updateOrder){
				hkOrderResponse.setHkItemOrders(listHKOrderDetails);
				hkOrderResponse.setUpdated(new Date().getTime());
				boolean updated = hkOrderService.updateHKOrderRequest(hkOrderResponse);
				hkOrderService.changeOrderStatus(refId, GeneralOrderStatus.UPDATED);
				if(updated){
					response = "Your order has been updated";
				} else{
					throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HK_ORDER_FAILED);
				}
			} else {
				hkOrderService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
				response = "Your order has been cancelled";
			}
			break;
		}
		case LAUNDRY: {
			LaundryOrder laundryOrderResponse = laundryOrderService.getEntity(refId);
			
			boolean updateOrder = false;
			List<LaundryItemOrderDetails> listLaundryOrderDetails = new ArrayList<>();
			for(LaundryItemOrderDetails itemDetails : laundryOrderResponse.getLaundryItemOrders()){
				if(request.getMapItemIdVSQuantity().containsKey(itemDetails.getLaundryItemId())){
					LaundryItemOrderDetails laundryItemOrderDetails = new LaundryItemOrderDetails();
					int quantity = request.getMapItemIdVSQuantity().get(itemDetails.getLaundryItemId());
					if(quantity <= 0){
						continue;
					} else{
						updateOrder = true;
						laundryItemOrderDetails.setLaundryItemId(itemDetails.getLaundryItemId());
						laundryItemOrderDetails.setImageUrl(itemDetails.getImageUrl());
						laundryItemOrderDetails.setItemName(itemDetails.getItemName());
						laundryItemOrderDetails.setPrice(itemDetails.getPrice());
						laundryItemOrderDetails.setQuantity(quantity);
						laundryItemOrderDetails.setType(itemDetails.getType());
						laundryItemOrderDetails.setTypeName(itemDetails.getTypeName());
						listLaundryOrderDetails.add(laundryItemOrderDetails);
					}
				}
			}
			
			if(updateOrder){
				laundryOrderResponse.setLaundryItemOrders(listLaundryOrderDetails);
				laundryOrderResponse.setUpdated(new Date().getTime());
				boolean updated = laundryOrderService.updatLaundryOrderRequest(laundryOrderResponse);
				laundryOrderService.changeOrderStatus(refId, GeneralOrderStatus.UPDATED);
				if(updated){
					response = "your order has been updated";
				} else{
					throw new AboutstaysException(AboutstaysResponseCode.UPDATE_LAUNDRY_ORDER_FAILED);
				}
			} else {
				laundryOrderService.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
				response = "your order has been cancelled";
			}
			break;
		}
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

	public void validateIdsRequest(String userId, String hotelId, String staysId) throws AboutstaysException {
		if (!userService.existsByUserId(userId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		if (!hotelService.hotelExistsById(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if (!staysService.existsByStaysId(staysId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
	}
}
