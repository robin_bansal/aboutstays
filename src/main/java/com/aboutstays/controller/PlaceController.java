package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.PlaceType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.PlaceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.PlaceResponse;
import com.aboutstays.services.PlaceService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.PlaceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.PlaceConstants.BASE)
public class PlaceController {

	@Autowired
	private PlaceService placeService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<PlaceRequest> getSampleRequest(){
		PlaceRequest request = PlaceConverter.getSampleRequest();
		return new BaseApiResponse<PlaceRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.PlaceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addPlace(@RequestBody PlaceRequest request) throws AboutstaysException{
		ValidateUtils.validatePlaceRequest(request, false);
		PlaceType placeType = PlaceType.findByType(request.getPlaceType());
		switch(placeType){
		case CITY:{
			PlaceResponse parentPlace = placeService.getById(request.getParentPlaceId());
			if(parentPlace==null||!PlaceType.COUNTRY.equals(PlaceType.findByType(parentPlace.getPlaceType()))){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_PARENT_PLACE_ID);
			}
			break;
		}
		case CONTINENT:
			request.setParentPlaceId(null);
			break;
		case COUNTRY:{
			PlaceResponse parentPlace = placeService.getById(request.getParentPlaceId());
			if(parentPlace==null||!PlaceType.CONTINENT.equals(PlaceType.findByType(parentPlace.getPlaceType()))){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_PARENT_PLACE_ID);
			}
			break;
		}
		}
		String placeId = placeService.save(request);
		return new BaseApiResponse<String>(false, placeId, AboutstaysResponseCode.PLACE_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.PlaceConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updatePlace(@RequestBody PlaceRequest request) throws AboutstaysException{
		ValidateUtils.validatePlaceRequest(request, false);
		if(!placeService.existsByPlaceId(request.getPlaceId()))
			throw new AboutstaysException(AboutstaysResponseCode.PLACE_NOT_FOUND);
		PlaceType placeType = PlaceType.findByType(request.getPlaceType());
		switch(placeType){
		case CITY:{
			PlaceResponse parentPlace = placeService.getById(request.getParentPlaceId());
			if(parentPlace==null||!PlaceType.COUNTRY.equals(PlaceType.findByType(parentPlace.getPlaceType()))){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_PARENT_PLACE_ID);
			}
			break;
		}
		case CONTINENT:
			request.setParentPlaceId(null);
			break;
		case COUNTRY:{
			PlaceResponse parentPlace = placeService.getById(request.getParentPlaceId());
			if(parentPlace==null||!PlaceType.CONTINENT.equals(PlaceType.findByType(parentPlace.getPlaceType()))){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_PARENT_PLACE_ID);
			}
			break;
		}
		}
		placeService.updateNonNull(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.PLACE_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.PlaceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deletePlace(@RequestParam String placeId) throws AboutstaysException{
		if(!placeService.existsByPlaceId(placeId)){
			throw new AboutstaysException(AboutstaysResponseCode.PLACE_NOT_FOUND);
		}
		placeService.delete(placeId);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.PL_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.PlaceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<PlaceResponse>> getAll() throws AboutstaysException{
		List<PlaceResponse> response = placeService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_PLACES_FOUND);
		}
		return new BaseApiResponse<List<PlaceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
