package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddFloorRequest;
import com.aboutstays.request.dto.UpdateFloorRequest;
import com.aboutstays.response.dto.AddFloorResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetFloorResponse;
import com.aboutstays.services.FloorService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.services.WingService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.FloorConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.FloorConstants.BASE)
public class FloorController {

	@Autowired
	private FloorService floorService;

	@Autowired
	private WingService wingService;

	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private ValidateIdsService validateIdsService; 

	@RequestMapping(value = RestMappingConstants.FloorConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddFloorRequest> getSample() {
		AddFloorRequest response = FloorConverter.getRequestSample();
		return new BaseApiResponse<AddFloorRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FloorConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddFloorResponse> add(@RequestBody(required = true) AddFloorRequest request)
			throws AboutstaysException {
		ValidateUtils.validateAddFloorRequest(request);
		validateIdsService.validateWingId(request.getWingId(), request.getHotelId());
		if (StringUtils.isEmpty(request.getCode())) {
			request.setCode(request.getName());
		}

		AddFloorResponse response = floorService.add(request);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.ADDED_FLOOR_FAILED);
		}
		return new BaseApiResponse<AddFloorResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FloorConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFloorResponse>> getAll() throws AboutstaysException {
		List<GetFloorResponse> response = floorService.getAll();
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_FLOOR_FAILED);
		}
		return new BaseApiResponse<List<GetFloorResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FloorConstants.DELETE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(
			@RequestParam(value = RestMappingConstants.FloorParameters.ID, required = true) String id)
			throws AboutstaysException {
		if (!floorService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_NOT_FOUND);
		}
		Boolean response = floorService.deleteById(id);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FloorConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetFloorResponse> get(
			@RequestParam(value = RestMappingConstants.FloorParameters.ID, required = true) String id)
			throws AboutstaysException {
		GetFloorResponse response = floorService.getById(id);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_NOT_FOUND);
		}
		return new BaseApiResponse<GetFloorResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.WingsConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) UpdateFloorRequest request)
			throws AboutstaysException {
		if (StringUtils.isEmpty(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
		if (StringUtils.isEmpty(request.getWingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_ID);
		}
		if (StringUtils.isEmpty(request.getFloorId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_ID);
		}

		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if (!wingService.existById(request.getWingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_NOT_FOUND);
		}
		if (!floorService.existsById(request.getFloorId())) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_NOT_FOUND);
		}
		Boolean response = floorService.updateById(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FloorConstants.GET_BY_WING_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFloorResponse>> getByWingId(
			@RequestParam(value = RestMappingConstants.FloorParameters.WING_ID, required = true) String id)
			throws AboutstaysException {
		if (!wingService.existById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_NOT_FOUND);
		}
		List<GetFloorResponse> response = floorService.getFloorsByWingId(id);
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_FLOOR_FAILED);
		}
		return new BaseApiResponse<List<GetFloorResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FloorConstants.GET_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFloorResponse>> getByHotelId(
			@RequestParam(value = RestMappingConstants.FloorParameters.HOTEL_ID, required = true) String id)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<GetFloorResponse> response = floorService.getFloorsByHotelId(id);
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_FLOOR_FAILED);
		}
		return new BaseApiResponse<List<GetFloorResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
