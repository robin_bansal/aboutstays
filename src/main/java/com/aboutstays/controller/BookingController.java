package com.aboutstays.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.CreateBookingRequest;
import com.aboutstays.request.dto.DeleteBookingRequest;
import com.aboutstays.request.dto.GetBookingsRequest;
import com.aboutstays.request.dto.UpdateBookingRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CreateBookingResponse;
import com.aboutstays.response.dto.GetBookingReponse;
import com.aboutstays.services.BookingService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.BookingConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.BookingConstants.BASE)
public class BookingController {
	
	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
		
	
	@RequestMapping(value = RestMappingConstants.BookingConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<CreateBookingRequest> sampleReqeust(){
		CreateBookingRequest response = BookingConverter.getSampleRequest();
		return new BaseApiResponse<CreateBookingRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.BookingConstants.CREATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody             
	public BaseApiResponse<CreateBookingResponse> createBooking(@RequestBody(required=true)CreateBookingRequest createBookingRequest) throws AboutstaysException, ParseException{
		ValidateUtils.validateCreateBooking(createBookingRequest);
		//if(bookingService.existsByReservationNumber(createBookingRequest.getReservationNumber())){
		//	throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_EXISTS);
		//}
		validateIdsService.validateRoomCategoryId(createBookingRequest.getRoomCategoryId(), createBookingRequest.getHotelId());
		CreateBookingResponse response = bookingService.createBooking(createBookingRequest);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.BOOKING_CREATI0N_FAILED);
		}
		return new BaseApiResponse<CreateBookingResponse>(false, response, AboutstaysResponseCode.SUCCESS);		
	}
	
	@RequestMapping(value=RestMappingConstants.BookingConstants.GET, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetBookingReponse>> getBooking(@RequestBody(required=true)GetBookingsRequest getBookingsRequest) throws AboutstaysException{
		ValidateUtils.validateGetBookingsRequest(getBookingsRequest);
		validateIdsService.validateHotelUserIds(getBookingsRequest.getHotelId(), getBookingsRequest.getUserId());
		
		List<GetBookingReponse> response = bookingService.getBookings(getBookingsRequest);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_BOOKINGS);
		}
		return new BaseApiResponse<List<GetBookingReponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.BookingConstants.DELETE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteBooking(@RequestBody(required=true)DeleteBookingRequest request) throws AboutstaysException{
		ValidateUtils.validateDeleteBooking(request);
		Boolean response = false;
		if(!StringUtils.isEmpty(request.getBookingId()) && bookingService.existsByBookingId(request.getBookingId())){
			response = bookingService.deleteByBookingId(request.getBookingId());
		} else if(bookingService.existsByReservationNumber(request.getReservationNumber())){
			response = bookingService.deleteByReservationNumber(request.getReservationNumber());
		}
		
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS); 
	}
	
	@RequestMapping(value=RestMappingConstants.BookingConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateByBookingIdOrReservationNumber(@RequestBody(required=true)UpdateBookingRequest request) throws AboutstaysException{
		ValidateUtils.validateBookingUpdateRequest(request);
		Boolean response = false;
		if(bookingService.existsByBookingId(request.getBookingId())){
			response = bookingService.updateBookingByBookingId(request);
		} 
		
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.BOOKING_UPDATION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
