
package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.pojos.MenuItemsByType;
import com.aboutstays.request.dto.AddFoodOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.GetRoomServiceResponse;
import com.aboutstays.request.dto.UpdateFoodOrderRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.FoodItemResponse;
import com.aboutstays.response.dto.GetFoodOrderResponse;
import com.aboutstays.services.FoodItemService;
import com.aboutstays.services.FoodOrdersService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.RoomServicesService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.FoodOrderRequestConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.FoodOrderConstants.BASE)
public class FoodOrderController extends BaseSessionController{
	
	@Autowired
	private FoodOrdersService foodOrderService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private StaysService stayService;
		
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private FoodItemService foodItemService;
	
	@Autowired
	private RoomServicesService roomServicesService;
	
	
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddFoodOrderRequest> getSample(){
		AddFoodOrderRequest response = FoodOrderRequestConverter.getSampleRequest();
		return new BaseApiResponse<AddFoodOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	public BaseApiResponse<String> add(@RequestBody(required=true)AddFoodOrderRequest request, boolean skipValidations) throws AboutstaysException{
		logger.info(request.toString());
		if(!skipValidations){
			ValidateUtils.validateAddFoodOrderRequest(request);
			baseSessionService.validateBaseSessionSessionData(request);
		}
		
		if(CollectionUtils.isNotEmpty(request.getFoodItemOrders())) {
			boolean validItem = true;
			StringBuilder itemName = new StringBuilder();
			long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId()); 
			long requestedTime = 0;
			if(request.isRequestedNow()){
				request.setDeliveryTime(new Date().getTime()+timezoneDiff);
				requestedTime = request.getDeliveryTime() - timezoneDiff;
			} else {
				request.setDeliveryTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())+timezoneDiff);
				requestedTime = request.getDeliveryTime();
			}
			
			
			String requestTime = DateUtil.format(new Date(requestedTime), DateUtil.MENU_ITEM_TIME_FORMAT);
			long requestTimeInLong = DateUtil.convertStringToLong(DateUtil.MENU_ITEM_TIME_FORMAT, requestTime)-timezoneDiff;
			GetRoomServiceResponse roomServiceResponse = null;
			
			for(FoodItemOrderDetails itemDetails : request.getFoodItemOrders()) {
				if(itemDetails == null) {
					continue;
				}
				String foodId = itemDetails.getFoodItemId();
				FoodItemResponse itemResponse = foodItemService.getById(foodId);
				
				if(itemResponse != null) {
					if(roomServiceResponse==null){
						roomServiceResponse = roomServicesService.getById(itemResponse.getRoomServicesId());
					}
					if(roomServiceResponse != null && CollectionUtils.isNotEmpty(roomServiceResponse.getListMenuItem())) {
						for(MenuItemsByType menuItemByType : roomServiceResponse.getListMenuItem()) {
							if(menuItemByType.getType().equals(itemResponse.getType())) {
								if(menuItemByType.isAllDay()) {
									continue;
								}
								long startTime = DateUtil.convertStringToLong(DateUtil.MENU_ITEM_TIME_FORMAT, menuItemByType.getStartTime());
								long endTime = DateUtil.convertStringToLong(DateUtil.MENU_ITEM_TIME_FORMAT, menuItemByType.getEndTime());
								if((startTime < endTime && (requestTimeInLong < startTime || requestTimeInLong > endTime))||(endTime < startTime && (requestTimeInLong < startTime && requestTimeInLong > endTime))) {
									validItem = false;
									itemName.append(itemResponse.getName());
									itemName.append(", ");
									//throw new AboutstaysException(AboutstaysResponseCode.INVALID_FOOD_ORDER_TIME_REQUEST);
								}
							}
						}
					}
					
				}
			}
			if(!validItem) {
				if(itemName.length()>0){
					itemName.deleteCharAt(itemName.length()-1);
				}
				String errorResponse = itemName.toString();
				throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.INVALID_FOOD_ORDER_TIME_REQUEST, errorResponse));
			}
		}
		

		
		String response = foodOrderService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_FOOD_ORDER_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestByFoodOrderRequest(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null||uosResponse.isError()){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_FOOD_ORDER_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddFoodOrderRequest request) throws AboutstaysException{
		return add(request, false);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFoodOrderResponse>> getAll() throws AboutstaysException{
		List<GetFoodOrderResponse> response = foodOrderService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_FOOD_ORDERS_FAILED);
		}
		return new BaseApiResponse<List<GetFoodOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetFoodOrderResponse> get(@RequestParam(value=RestMappingConstants.FoodOrderParameters.ID, required=true)String id) throws AboutstaysException{
		GetFoodOrderResponse response = foodOrderService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ORDER_NOT_EXISTS);
		}
		return new BaseApiResponse<GetFoodOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.FoodOrderParameters.ID, required=true)String id) throws AboutstaysException{
		if(!foodOrderService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ORDER_NOT_EXISTS);
		}
		Boolean response = foodOrderService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateFoodOrderRequest request) throws AboutstaysException{
		logger.info(request.toString());
		ValidateUtils.validateUpdateFoodOrderRequest(request);
		baseSessionService.validateBaseSessionSessionData(request);
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FOOD_ORDER_ID);
		}
		if(!foodOrderService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ORDER_NOT_EXISTS);
		}
		Boolean response = foodOrderService.updateNonNullFieldsOfRequest(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_FOOD_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.GET_ALL_BY_USER_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFoodOrderResponse>> getAllByUserId(@RequestParam(value=RestMappingConstants.FoodOrderParameters.USER_ID, required=true)String userId) throws AboutstaysException{
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<GetFoodOrderResponse> listResponse = foodOrderService.getAllByUserId(userId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_FOOD_ORDER_BY_USER_ID_FAILED);
		}
		return new BaseApiResponse<List<GetFoodOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
			
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.GET_ALL_BY_HOTEL_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFoodOrderResponse>> getAllByHotelId(@RequestParam(value=RestMappingConstants.FoodOrderParameters.HOTEL_ID, required=true)String hotelId) throws AboutstaysException{
		if(!hotelService.hotelExistsById(hotelId)){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<GetFoodOrderResponse> listResponse = foodOrderService.getAllByHotelId(hotelId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_FOOD_ORDER_BY_HOTEL_ID_FAILED);
		}
		return new BaseApiResponse<List<GetFoodOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodOrderConstants.GET_ALL_BY_USER_ID_AND_STAY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetFoodOrderResponse>> getAllByUserIdAndStayId(@RequestParam(value=RestMappingConstants.FoodOrderParameters.USER_ID, required=true)String userId,
			@RequestParam(value=RestMappingConstants.FoodOrderParameters.STAY_ID, required=true)String stayId) throws AboutstaysException{
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if(!stayService.existsByStaysId(stayId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		List<GetFoodOrderResponse> listResponse = foodOrderService.getAllByUserAndStayId(userId, stayId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_BY_USERID_AND_STAYID_FAILED);
		}
		return new BaseApiResponse<List<GetFoodOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	
	

}
