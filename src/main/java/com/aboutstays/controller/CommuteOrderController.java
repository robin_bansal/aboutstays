package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CarType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddCommuteOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.UpdateCommuteOrderRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetCommuteOrderResponse;
import com.aboutstays.services.CommuteOrderService;
import com.aboutstays.services.HotelService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.CommuteOrderConverter;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.BASE)
public class CommuteOrderController extends BaseSessionController {

	@Autowired
	private CommuteOrderService coService;

	@Autowired
	private AutowireCapableBeanFactory beanFactory;


	@Autowired
	private HotelService hotelService;

	@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddCommuteOrderRequest> getSample() {
		AddCommuteOrderRequest response = CommuteOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddCommuteOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	public BaseApiResponse<String> add(@RequestBody(required = true) AddCommuteOrderRequest request,
			boolean skipValidations) throws AboutstaysException {
		if (!skipValidations) {
			ValidateUtils.validateCommuteOrder(request);
			baseSessionService.validateBaseSessionSessionData(request);
		}
		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId());
		if (request.isRequestedNow()) {
			request.setDeliveryTime(new Date().getTime() + timezoneDiff);
		} else {
			request.setDeliveryTime(
					DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())
							+ timezoneDiff);
		}
		if (request.getCarDetails() != null) {
			CarType type = CarType.findValueByCode(request.getCarDetails().getType());
			if (type == null) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
			}
			request.getCarDetails().setName(type.getMessage());
			request.getCarDetails().setImageUrl(type.getImageUrl());
		}
		String response = coService.save(request);
		if (StringUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.ADD_COMMUTE_ORDER_REQUEST);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSReqByCommuteOrder(request,
				response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if (uosResponse == null) {
			throw new AboutstaysException(AboutstaysResponseCode.ADD_COMMUTE_ORDER_REQUEST);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required = true) AddCommuteOrderRequest request)
			throws AboutstaysException {
		return add(request, false);
	}

	@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCommuteOrderResponse>> getAll() throws AboutstaysException {
		List<GetCommuteOrderResponse> response = coService.getAll();
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<List<GetCommuteOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetCommuteOrderResponse> get(
			@RequestParam(value = RestMappingConstants.CommuteOrderParameters.ID, required = true) String id)
			throws AboutstaysException {
		GetCommuteOrderResponse response = coService.getById(id);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<GetCommuteOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.DELETE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(
			@RequestParam(value = RestMappingConstants.CommuteOrderParameters.ID, required = true) String id)
			throws AboutstaysException {
		if (!coService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_ORDER_NOT_FOUND);
		}
		Boolean response = coService.delete(id);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.CommuteOrderConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) UpdateCommuteOrderRequest request)
			throws AboutstaysException {
		
		if (StringUtils.isEmpty(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_COMMUTE_ORDER_ID);
		}
		if (!coService.existsById(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_ORDER_NOT_FOUND);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		if (request.getCarDetails() != null) {
			CarType type = CarType.findValueByCode(request.getCarDetails().getType());
			if (type == null) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
			}
			request.getCarDetails().setName(type.getMessage());
			request.getCarDetails().setImageUrl(type.getImageUrl());
		}
		Boolean response = coService.updateNonNullFieldsOfRequest(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_COMMUTE_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
