package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.SupportedLaundryType;
import com.aboutstays.request.dto.LaundryServiceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.LaundryServiceResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.LaundryServicesService;
import com.aboutstays.utils.LaundryServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.LaundryServiceConstants.BASE)
public class LaundryServicesController {

	@Autowired
	private LaundryServicesService laundryServicesService;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LaundryServiceRequest> getSampleRequest(){
		LaundryServiceRequest request = LaundryServiceConverter.getSampleRequest();
		return new BaseApiResponse<LaundryServiceRequest>(false, request, AboutstaysResponseCode.SUCCESS);		
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addService(@RequestBody(required=true)LaundryServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddLaundryServiceRequest(request);
		if(!hotelService.hotelExistsById(request.getServiceId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		Iterator<SupportedLaundryType> iterator = request.getSupportedLaundryTypeList().iterator();
		Set<Integer> laundryTypeSet = new HashSet<>(); 
		while (iterator.hasNext()) {
			SupportedLaundryType supportedLaundryType = iterator.next();
			LaundryItemType laundryItemType = LaundryItemType.findByCode(supportedLaundryType.getType()); 
			if(laundryItemType==null)
				throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_LAUNDRY_TYPE);
			else if(laundryTypeSet.contains(supportedLaundryType.getType())){
				iterator.remove();
			} else {
				laundryTypeSet.add(supportedLaundryType.getType());
			}
			supportedLaundryType.setTypeName(laundryItemType.getDisplayName());
			supportedLaundryType.setImageUrl(laundryItemType.getImageUrl());
		}
		String id = laundryServicesService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.LAUNDRY_SERVICE_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LaundryServiceResponse> getById(@RequestParam(value=RestMappingConstants.LaundryServiceParameters.ID)String id) throws AboutstaysException{
		LaundryServiceResponse response = laundryServicesService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_SERVICE_NOT_FOUND);
		return new BaseApiResponse<LaundryServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
