package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddGeneralServiceRequest;
import com.aboutstays.request.dto.UpdateGeneralServiceRequest;
import com.aboutstays.response.dto.AddGeneralServiceResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.utils.GeneralServicesConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.BASE)
public class GeneralServiceController {

	@Autowired
	private GeneralServicesService generalServicesService;

	@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddGeneralServiceRequest> getSampleRequest() {
		AddGeneralServiceRequest response = GeneralServicesConverter.getSample();
		return new BaseApiResponse<AddGeneralServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddGeneralServiceResponse> add(
			@RequestBody(required = true) AddGeneralServiceRequest request) throws AboutstaysException {
		ValidateUtils.validateGeneralServiceRequest(request);
		GeneralServiceType typeEnum = GeneralServiceType.findGeneralServiceByType(request.getServiceType());
		StaysStatus statusEnum = StaysStatus.findStatusByCode(request.getStatus());
		if (typeEnum == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GENERAL_SERVICE_TYPE);
		}
		if (statusEnum == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_STAY_STATUS);
		}
		AddGeneralServiceResponse response = generalServicesService.add(request);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.ADD_GENERAL_SERVICE_FAILED);
		}
		return new BaseApiResponse<AddGeneralServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetGeneralServiceResponse>> getAllGeneralServices(
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.STATUS, required = false) Integer status)
			throws AboutstaysException {
		List<GetGeneralServiceResponse> response = null;
		if (status != null) {
			StaysStatus typeEnum = StaysStatus.findStatusByCode(status);
			if (typeEnum == null) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_STAY_STATUS);
			} else {
				response = generalServicesService.getAllByStaysStatus(status);
			}
		} else {
			response = generalServicesService.getAll();
		}
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_GENERAL_SERVICES_FAILED);
		}
		return new BaseApiResponse<List<GetGeneralServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.DELTE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.ID, required = true) String id)
			throws AboutstaysException {
		if (!generalServicesService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		}
		Boolean response = generalServicesService.deleteById(id);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.GS_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetGeneralServiceResponse> get(
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.ID, required = true) String id)
			throws AboutstaysException {
		GetGeneralServiceResponse response = generalServicesService.getById(id);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetGeneralServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.GeneralServicesConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) UpdateGeneralServiceRequest request)
			throws AboutstaysException {
		if (StringUtils.isEmpty(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GS_ID);
		}
		if (!generalServicesService.existsById(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		}
		Boolean response = generalServicesService.updateById(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_GS_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
