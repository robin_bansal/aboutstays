package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.GeneralNameImageInfo;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetCommonDataResponse;
import com.aboutstays.response.dto.GetMasterAmenityResponse;
import com.aboutstays.response.dto.StayPreferencesResponse;
import com.aboutstays.services.MasterAmenityService;
import com.aboutstays.services.ProcessUtilityService;
import com.aboutstays.utils.CollectionUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CommonConstants.BASE)
public class CommonController {
	
	//private static Logger logger = LoggerFactory.getLogger(CommonController.class);
	private static Logger logger = Logger.getLogger(CommonController.class);
	
	@Autowired
	private ProcessUtilityService processUtilityService; 
	
	@Autowired
	private MasterAmenityService masterAmenityService;
	
	@RequestMapping(value=RestMappingConstants.CommonConstants.GET_PREFERENCES, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<StayPreferencesResponse> getAllStayPreferences() throws AboutstaysException{
		logger.info(RestMappingConstants.CommonConstants.GET_PREFERENCES);
		List<GetMasterAmenityResponse> masterAmenities = masterAmenityService.getAll(AmenityType.PREFERENCE_BASED_AMENITIES);
		StayPreferencesResponse response = new StayPreferencesResponse();
		List<GeneralNameImageInfo> listPreferences = new ArrayList<>();
		
		if(CollectionUtils.isNotEmpty(masterAmenities)){
			for(GetMasterAmenityResponse amenity : masterAmenities) {
				GeneralNameImageInfo info = new GeneralNameImageInfo();
				info.setImageUrl(amenity.getIconUrl());
				info.setName(amenity.getName());
				listPreferences.add(info);
			}
		}
		response.setListPreferneces(listPreferences);
		return new BaseApiResponse<StayPreferencesResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommonConstants.GET_PROFILE_TITLES, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<String>> getProfileTitles() throws AboutstaysException{
		logger.info(RestMappingConstants.CommonConstants.GET_PROFILE_TITLES);
		List<String> titles = processUtilityService.getStringListProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_TITLES);
		if(CollectionUtils.isEmpty(titles)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_TITLES_FOUND);
		}
		return new BaseApiResponse<List<String>>(false, titles, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommonConstants.GET_IDENTITY_DOCS, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<String>> getIdentityDocs() throws AboutstaysException{
		logger.info(RestMappingConstants.CommonConstants.GET_IDENTITY_DOCS);
		List<String> identityDocs = processUtilityService.getStringListProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_IDENTITY_DOCS);
		if(CollectionUtils.isEmpty(identityDocs)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_DOCS_FOUND);
		}
		return new BaseApiResponse<List<String>>(false, identityDocs, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommonConstants.GET_COMMON_DATA, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetCommonDataResponse> getUserCommonData() throws AboutstaysException{
		logger.info(RestMappingConstants.CommonConstants.GET_COMMON_DATA);
		GetCommonDataResponse response = new GetCommonDataResponse();
		
		List<String> identityDocs = processUtilityService.getStringListProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_IDENTITY_DOCS);
		if(CollectionUtils.isNotEmpty(identityDocs)) {
			response.setListIdentityDocs(identityDocs);
		}
		
		List<String> titles = processUtilityService.getStringListProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_TITLES);
		if(CollectionUtils.isNotEmpty(titles)) {
			response.setListTitles(titles);
		}
		
		BaseApiResponse<StayPreferencesResponse> prefResponse = getAllStayPreferences();
		if(prefResponse != null && prefResponse.getData() != null) {
			StayPreferencesResponse stayPrefResponse = prefResponse.getData();
			List<GeneralNameImageInfo> listStayPrefs = stayPrefResponse.getListPreferneces();
			if(CollectionUtils.isNotEmpty(listStayPrefs)) {
				response.setListPreferneces(listStayPrefs);
			}
		}
		return new BaseApiResponse<GetCommonDataResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	
	
	
	

}
