package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddAmenitiesToRoomCategoryRequest;
import com.aboutstays.request.dto.AddRoomCategoryRequest;
import com.aboutstays.request.dto.SetEarlyCheckInRoomRequest;
import com.aboutstays.request.dto.SetLateCheckOutRoomRequest;
import com.aboutstays.request.dto.UpdateRoomCategoryRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetEarlyCheckinRoomResponse;
import com.aboutstays.response.dto.GetLateCheckoutRoomResponse;
import com.aboutstays.response.dto.GetRoomCategoryResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.RoomCategoryConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.BASE)
public class RoomCategoryController {

	@Autowired
	private RoomCategoryService roomCategoryService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private ValidateIdsService validateIdsService; 
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddRoomCategoryRequest> getSampleRequest(){
		AddRoomCategoryRequest request = RoomCategoryConverter.generateSampleRequest();
		return new BaseApiResponse<AddRoomCategoryRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.CREATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> createRoomCategory(@RequestBody(required=true)AddRoomCategoryRequest request) throws AboutstaysException{
		ValidateUtils.validateAddRoomCategoryRequest(request);
		if(!hotelService.hotelExistsById(request.getHotelId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		validateIdsService.validateAmenitiesList(request.getInRoomAmenities(), AmenityType.ROOM, AboutstaysResponseCode.AMENITY_NOT_FOUND);
		validateIdsService.validateAmenitiesList(request.getPreferenceBasedAmenities(), AmenityType.PREFERENCE_BASED_AMENITIES, AboutstaysResponseCode.AMENITY_NOT_FOUND);

		String roomCategoryId = roomCategoryService.addRoomCategory(request);
		return new BaseApiResponse<String>(false, roomCategoryId, AboutstaysResponseCode.ROOM_CATEGORY_ADDED_SUCCESSFULLY);
	}
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.GET_ALL,method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomCategoryResponse>> getAllRoomCategories() throws AboutstaysException{
		List<GetRoomCategoryResponse> responseList = roomCategoryService.getAll();
		if(CollectionUtils.isEmpty(responseList)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_CATEGORY);
		}
		return new BaseApiResponse<List<GetRoomCategoryResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.GET,method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetRoomCategoryResponse> getRoomCategoryById(@RequestParam(value=RestMappingConstants.RoomCategoryConstants.KEY_ID)String id) throws AboutstaysException{
		if(!roomCategoryService.exists(id)){
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
		}
		GetRoomCategoryResponse response = roomCategoryService.getById(id);
		return new BaseApiResponse<GetRoomCategoryResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.RoomCategoryConstants.UPDATE, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) UpdateRoomCategoryRequest request)
			throws AboutstaysException {
		ValidateUtils.validateUpdateRoomCategoryRequest(request);
		validateIdsService.validateRoomCategoryId(request.getRoomCategoryId(), request.getHotelId());
		validateIdsService.validateAmenitiesList(request.getInRoomAmenities(), AmenityType.ROOM, AboutstaysResponseCode.AMENITY_NOT_FOUND);
		validateIdsService.validateAmenitiesList(request.getPreferenceBasedAmenities(), AmenityType.PREFERENCE_BASED_AMENITIES, AboutstaysResponseCode.AMENITY_NOT_FOUND);

		Boolean response =roomCategoryService.update(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_FAILED_ROOM_CATEGORY);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteRoomCategory(@RequestParam(value=RestMappingConstants.RoomCategoryConstants.KEY_ID)String id) throws AboutstaysException{
		if(!roomCategoryService.exists(id)){
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
		}
		roomCategoryService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ROOM_CATEGORY_DELETED_SUCCESSFULLY);
	}

	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.SET_EARLY_CHECKIN_INFO,method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> setEarlyChecinInfo(@RequestBody(required=true)SetEarlyCheckInRoomRequest request) throws AboutstaysException{
		if(!roomCategoryService.exists(request.getRoomCategoryId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_CATEGORY);
		}
		if(request.getInfo() == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_EARLY_CHECK_IN_INFO);
		}
		Boolean response = roomCategoryService.setEarlyCheckinRoomInfo(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.EARLY_CHECKIN_INFO_SET_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.GET_EARLY_CHECKIN_INFO,method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetEarlyCheckinRoomResponse> getCheckinInfo(@RequestParam(value = RestMappingConstants.RoomCategoryParameters.ROOM_CATEGORY_ID)String roomId) throws AboutstaysException{
		if(!roomCategoryService.exists(roomId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_CATEGORY);
		}
		GetEarlyCheckinRoomResponse response = roomCategoryService.getEarlyCheckinRoomInfo(roomId);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GET_EARLY_CHECKIN_DETAILS_FAILED);
		}
		return new BaseApiResponse<GetEarlyCheckinRoomResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.SET_LATE_CHECKOUT_INFO, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> setLateChecoutInfo(@RequestBody(required=true)SetLateCheckOutRoomRequest request) throws AboutstaysException{
		if(!roomCategoryService.exists(request.getRoomCategoryId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_CATEGORY);
		}
		if(request.getCheckOutInfo() == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LATE_CHECKOUT_INFO_REQ);
		}
		Boolean response = roomCategoryService.setLateCheckoutRoomInfo(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_INFO_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.GET_LATE_CHECKOUT_INFO,method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetLateCheckoutRoomResponse> getCheckoutInfo(@RequestParam(value = RestMappingConstants.RoomCategoryParameters.ROOM_CATEGORY_ID)String roomId) throws AboutstaysException{
		if(!roomCategoryService.exists(roomId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_CATEGORY);
		}
		GetLateCheckoutRoomResponse response = roomCategoryService.getLateCheckoutRoomInfo(roomId);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GET_LATE_CHECKOUT_DETAILS_FAILED);
		}
		return new BaseApiResponse<GetLateCheckoutRoomResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.LINK_IN_ROOM_AMENITIES, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addInRoomAmenities(@RequestBody(required=true)AddAmenitiesToRoomCategoryRequest request) throws AboutstaysException{
		ValidateUtils.validateAddAmenitiesToRoomCategoryRequest(request);
		if(!roomCategoryService.exists(request.getRoomCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
		validateIdsService.validateAmenitiesList(request.getAmenityIds(), AmenityType.ROOM, AboutstaysResponseCode.AMENITY_NOT_FOUND);
		roomCategoryService.updateInRoomAmenities(request.getRoomCategoryId(), request.getAmenityIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AMENITY_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.LINK_PREFERENCE_BASED_AMENITIES, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addPreferenceBasedAmenities(@RequestBody(required=true)AddAmenitiesToRoomCategoryRequest request) throws AboutstaysException{
		ValidateUtils.validateAddAmenitiesToRoomCategoryRequest(request);
		if(!roomCategoryService.exists(request.getRoomCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
		validateIdsService.validateAmenitiesList(request.getAmenityIds(), AmenityType.PREFERENCE_BASED_AMENITIES, AboutstaysResponseCode.AMENITY_NOT_FOUND);
		roomCategoryService.updateInPreferenceBasedAmenities(request.getRoomCategoryId(), request.getAmenityIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AMENITY_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomCategoryConstants.GET_BY_HOTEL_ID,method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomCategoryResponse>> getAllRoomCategoriesByHotelId(@RequestParam(value=RestMappingConstants.RoomCategoryParameters.HOTEL_ID,required=true)String hotelId) throws AboutstaysException{
		if(!hotelService.hotelExistsById(hotelId)){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);			
		}
		List<GetRoomCategoryResponse> responseList = roomCategoryService.getAllByHotelId(hotelId);
		if(CollectionUtils.isEmpty(responseList)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_CATEGORY);
		}
		return new BaseApiResponse<List<GetRoomCategoryResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	
}
