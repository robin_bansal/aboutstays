package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.RoomMetaInfo;
import com.aboutstays.pojos.RoomWithAmenityIds;
import com.aboutstays.request.dto.AddAdditionalAmenitiesRequest;
import com.aboutstays.request.dto.AddRoomRequest;
import com.aboutstays.request.dto.BatchUpdateAmenitiesRequest;
import com.aboutstays.request.dto.BatchUpdateRoomMetaInfoRequest;
import com.aboutstays.request.dto.UpdateRoomRequest;
import com.aboutstays.response.dto.AddRoomResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetRoomResponse;
import com.aboutstays.services.FloorService;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.services.RoomService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.services.WingService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.RoomConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.RoomConstants.BASE)
public class RoomController {

	@Autowired
	RoomService roomService;

	@Autowired
	private RoomCategoryService roomCategoryService;

	@Autowired
	private FloorService floorService;

	@Autowired
	private WingService wingService;

	@Autowired
	private ValidateIdsService validateIdsService; 

	@RequestMapping(value = RestMappingConstants.REQUEST, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddRoomRequest> getSampleRequest() {
		AddRoomRequest request = RoomConverter.generateSampleRequest();
		return new BaseApiResponse<AddRoomRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.CREATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddRoomResponse> createRoom(@RequestBody(required = true) AddRoomRequest request)
			throws AboutstaysException {
		ValidateUtils.validateAddRoomRequest(request);
		if (!roomCategoryService.exists(request.getRoomCategoryId())) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
		}
		validateIdsService.validateFloorId(request.getFloorId(), request.getWingId(), request.getHotelId());
		String roomId = roomService.add(request);
		AddRoomResponse response = new AddRoomResponse();
		response.setRoomId(roomId);
		return new BaseApiResponse<AddRoomResponse>(false, response, AboutstaysResponseCode.ROOM_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomResponse>> getAllRooms() throws AboutstaysException {
		List<GetRoomResponse> responseList = roomService.getAll();
		if (CollectionUtils.isEmpty(responseList)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOMS);
		}
		return new BaseApiResponse<List<GetRoomResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetRoomResponse> getRoomCategoryById(
			@RequestParam(value = RestMappingConstants.RoomParameters.KEY_ID) String id) throws AboutstaysException {
		if (!roomService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_NOT_FOUND);
		}
		GetRoomResponse response = roomService.getById(id);
		return new BaseApiResponse<GetRoomResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.DELETE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteRoom(
			@RequestParam(value = RestMappingConstants.RoomParameters.KEY_ID) String id) throws AboutstaysException {
		if (!roomService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_NOT_FOUND);
		}
		roomService.deleteById(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ROOM_DELETED_SUCCESSFULLY);
	}
	@RequestMapping(value = RestMappingConstants.RoomConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) UpdateRoomRequest request)
			throws AboutstaysException {
		ValidateUtils.validateAddRoomRequest(request);
		validateIdsService.validateFloorId(request.getFloorId(), request.getWingId(), request.getHotelId());
		validateIdsService.validateRoomCategoryId(request.getRoomCategoryId(), request.getHotelId());
		Boolean response =roomService.updateById(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_FAIL_ROOM);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.LINK_ADDITIONAL_AMENITIES, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAdditionalAmenities(
			@RequestBody(required = true) AddAdditionalAmenitiesRequest request) throws AboutstaysException {
		ValidateUtils.validateAdditionalAmenitiesToRoomRequest(request);
		if (!roomService.existsById(request.getRoomId()))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.ROOM_WITH_ID_NOT_FOUND, request.getRoomId()));
		validateIdsService.validateAmenitiesList(request.getAmenityIds(), AmenityType.PREFERENCE_BASED_AMENITIES, AboutstaysResponseCode.AMENITY_NOT_FOUND);;
		roomService.addAdditionalAmenities(request.getRoomId(), request.getAmenityIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AMENITY_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.GET_BY_ROOM_CATEGORY_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomResponse>> getByRoomCategoryId(
			@RequestParam(value = RestMappingConstants.RoomParameters.ROOM_CATEGORY_ID) String id)
			throws AboutstaysException {
		if (StringUtils.isEmpty(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_CATEGORY_ID);
		}
		if (!roomCategoryService.exists(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
		}
		List<GetRoomResponse> responseList = roomService.getRoomsByRoomCategory(id);
		if (CollectionUtils.isEmpty(responseList)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOMS);
		}
		return new BaseApiResponse<List<GetRoomResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.GET_BY_FLOOR_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomResponse>> getByRoomFloorId(
			@RequestParam(value = RestMappingConstants.RoomParameters.FLOOR_ID) String id)
			throws AboutstaysException {
		if (StringUtils.isEmpty(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_ID);
		}
		if (!floorService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_NOT_FOUND);
		}
		List<GetRoomResponse> responseList = roomService.getRoomsByFloorId(id);
		if (CollectionUtils.isEmpty(responseList)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOMS);
		}
		return new BaseApiResponse<List<GetRoomResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.GET_BY_WING_Id, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomResponse>> getByRoomWingId(
			@RequestParam(value = RestMappingConstants.RoomParameters.WING_ID) String id)
			throws AboutstaysException {
		if (StringUtils.isEmpty(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_ID);
		}
		if (!wingService.existById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_NOT_FOUND);
		}
		List<GetRoomResponse> responseList = roomService.getRoomsByWingId(id);
		if (CollectionUtils.isEmpty(responseList)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOMS);
		}
		return new BaseApiResponse<List<GetRoomResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.RoomConstants.GET_BY_HOTEL_Id, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomResponse>> getByRoomHotelId(
			@RequestParam(value = RestMappingConstants.RoomParameters.HOTEL_ID) String id)
			throws AboutstaysException {
		if (StringUtils.isEmpty(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
		validateIdsService.validateHotelId(id);
		List<GetRoomResponse> responseList = roomService.getRoomsByHotelId(id);
		if (CollectionUtils.isEmpty(responseList)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOMS);
		}
		return new BaseApiResponse<List<GetRoomResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.RoomConstants.BATCH_DELETE_ROOMS, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteRoomsInBatch(@RequestParam List<String> roomIds, @RequestParam String hotelId) throws AboutstaysException{
		validateIdsService.validateHotelId(hotelId);
		roomService.deleteRooms(hotelId, roomIds);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ROOMS_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value = RestMappingConstants.RoomConstants.BATCH_UPDATE_METAINFO, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> batchUpdateMetaInfo(@RequestBody(required=true)BatchUpdateRoomMetaInfoRequest request) throws AboutstaysException{
		String hotelId = request.getHotelId();
		validateIdsService.validateHotelId(hotelId);
		boolean allUpdated = true;
		List<BaseApiResponse<Boolean>> responseList = null;
		if(CollectionUtils.isNotEmpty(request.getDataToUpdate())){
			responseList = new ArrayList<>();
			for(RoomMetaInfo roomInfo : request.getDataToUpdate()){
				if(!roomService.exists(hotelId, roomInfo.getRoomId())){
					allUpdated = false;
					responseList.add(new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.ROOM_WITH_ID_NOT_FOUND, roomInfo.getRoomId())));
				} else if(StringUtils.isEmpty(roomInfo.getRoomNumber())){
					allUpdated = false;
					responseList.add(new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.EMPTY_ROOM_NUMBER_WITH_ID, roomInfo.getRoomId())));
				} else if(!roomCategoryService.existsInHotel(hotelId, roomInfo.getRoomCategoryId())){
					allUpdated = false;
					responseList.add(new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.EMPTY_ROOM_CATEGORYID_ID_WITH_ID, roomInfo.getRoomId())));
				} else {
					if(roomInfo.getPanelUiOrder()==null){
						roomInfo.setPanelUiOrder(999);
					}
					roomService.updateRoomMetaInfo(roomInfo);
					responseList.add(new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.ROOM_WITH_NO_UPDATED_SUCCESSFULLY, roomInfo.getRoomNumber())));
				}
			}
		}
		if(!allUpdated){
			return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(true, responseList, AboutstaysResponseCode.SOME_ROOMS_NOT_UPDATED);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, responseList, AboutstaysResponseCode.ROOMS_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value = RestMappingConstants.RoomConstants.BATCH_LINK_ADDITIONAL_AMENITIES, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> batchUpdateAdditionalAmenities(@RequestBody(required=true)BatchUpdateAmenitiesRequest request) throws AboutstaysException{
		boolean allUpdated = true;
		List<BaseApiResponse<Boolean>> responseList = null;
		if(CollectionUtils.isNotEmpty(request.getDataToUpdate())){
			responseList = new ArrayList<>();
			for(RoomWithAmenityIds roomVsAmenities:request.getDataToUpdate()){
				try{
					AddAdditionalAmenitiesRequest addRequest = new AddAdditionalAmenitiesRequest();
					addRequest.setRoomId(roomVsAmenities.getRoomId());
					addRequest.setAmenityIds(roomVsAmenities.getAmentityIds());
					responseList.add(addAdditionalAmenities(addRequest));
				} catch(AboutstaysException ae){
					allUpdated = false;
					responseList.add(new BaseApiResponse<Boolean>(true, ae.getCode(), false,ae.getMessage()));
				} catch(Exception e){
					allUpdated = false;
					responseList.add(new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage()));
				}
			}
		}
		if(!allUpdated){
			return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(true, responseList, AboutstaysResponseCode.SOME_ROOMS_NOT_UPDATED);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, responseList, AboutstaysResponseCode.ROOMS_UPDATED_SUCCESSFULLY);
	}

}
