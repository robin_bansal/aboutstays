package com.aboutstays.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.GetAllStaysServiceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetStaysServiceListResponse;
import com.aboutstays.services.CurrentStayService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CurrentStaysConstants.BASE)
public class CurrentStaysController {

	@Autowired
	private CurrentStayService currentStayService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@RequestMapping(value=RestMappingConstants.CurrentStaysConstants.GET_ALL_SERVICES, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetStaysServiceListResponse> getAllStaysServices(@RequestBody(required=true)GetAllStaysServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateGetAllStaysServiceRequest(request);
		validateIdsService.validateUserHotelStaysIds(request);
		GetStaysServiceListResponse response = currentStayService.getAllCurrentStaysServices(request);
		if(response==null||CollectionUtils.isEmpty(response.getServicesList()))
			throw new AboutstaysException(AboutstaysResponseCode.NO_SERVICE_FOR_CURRENT_STAY);
		response.setHotelId(request.getHotelId());
		response.setStaysId(request.getStaysId());
		response.setUserId(request.getUserId());
		return new BaseApiResponse<GetStaysServiceListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
