package com.aboutstays.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.CheckoutEntity;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.InitiateCheckoutRequest;
import com.aboutstays.request.dto.ModifyCheckoutRequest;
import com.aboutstays.request.dto.RetreiveCheckoutRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.CheckoutResponse;
import com.aboutstays.response.dto.GetExpenseByCategoryResponse;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.CheckoutService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CheckoutConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CheckoutConstants.BASE)
public class CheckoutController {
	
	@Autowired
	private CheckoutService checkoutService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@Autowired
	private CheckinService checkinService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<InitiateCheckoutRequest> getSampleRequest(){
		InitiateCheckoutRequest initiateCheckoutRequest = CheckoutConverter.getSampleRequest();
		return new BaseApiResponse<InitiateCheckoutRequest>(false, initiateCheckoutRequest, AboutstaysResponseCode.SUCCESS);
	}
	
//	@RequestMapping(value=RestMappingConstants.CheckoutConstants.REQUEST_CHECKOUT, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public BaseApiResponse<String> initiateCheckout(@RequestBody(required=true)InitiateCheckoutRequest request) throws AboutstaysException{
//		ValidateUtils.validateCheckoutRequest(request);
//		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
//				
//		String checkoutId = checkoutService.save(request);
//		return new BaseApiResponse<String>(false, checkoutId, AboutstaysResponseCode.CHECKOUT_INITIATED_SUCCESSFULLY);
//	}
	
	@RequestMapping(value=RestMappingConstants.CheckoutConstants.MODIFY_CHECKOUT, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> modifyCheckout(@RequestBody(required=true)ModifyCheckoutRequest request) throws AboutstaysException{
		ValidateUtils.validateModifyCheckutRequest(request);
		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
		
		CheckoutResponse response = checkoutService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
		if(response==null){
			CheckinResponse checkinResponse = checkinService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
			response = CheckoutConverter.convertCheckinResponseToCheckoutResponse(checkinResponse, response);
			
			UserStayHotelIdsPojo basicIdsRequest = new UserStayHotelIdsPojo();
			basicIdsRequest.setUserId(request.getUserId());
			basicIdsRequest.setHotelId(request.getHotelId());
			basicIdsRequest.setStaysId(request.getStaysId());
			ExpenseDashboardController controller = new ExpenseDashboardController();
			beanFactory.autowireBean(controller);
			BaseApiResponse<GetExpenseByCategoryResponse> expense = controller.generate(basicIdsRequest);
			if(expense != null && expense.getData() != null) {
				GetExpenseByCategoryResponse categoryExpense = expense.getData();
				response.setGrandTotal(categoryExpense.getGrandTotal());
				response.setTotalAmount(categoryExpense.getGrandTotal());
			}
			response.setCheckedOutEarlier(false);
		} else {
			response.setCheckedOutEarlier(true);
		}
		response.setPaymentMethod(request.getPaymentMethod());
		
		CheckoutEntity checkoutEntity = CheckoutConverter.convertCheckoutResponseToEntity(response);
		checkoutService.update(checkoutEntity);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CHECKOUT_MODIFIED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.CheckoutConstants.RETREIVE_CHECKOUT, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<CheckoutResponse> retreiveCheckout(@RequestBody(required=true)RetreiveCheckoutRequest request) throws AboutstaysException{
		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
		CheckoutResponse response = checkoutService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
		
		if(response==null) {
			CheckinResponse checkinResponse = checkinService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
			response = CheckoutConverter.convertCheckinResponseToCheckoutResponse(checkinResponse, response);
			
			UserStayHotelIdsPojo basicIdsRequest = new UserStayHotelIdsPojo();
			basicIdsRequest.setUserId(request.getUserId());
			basicIdsRequest.setHotelId(request.getHotelId());
			basicIdsRequest.setStaysId(request.getStaysId());
			ExpenseDashboardController controller = new ExpenseDashboardController();
			beanFactory.autowireBean(controller);
			BaseApiResponse<GetExpenseByCategoryResponse> expense = controller.generate(basicIdsRequest);
			if(expense != null && expense.getData() != null) {
				GetExpenseByCategoryResponse categoryExpense = expense.getData();
				response.setGrandTotal(categoryExpense.getGrandTotal());
				response.setTotalAmount(categoryExpense.getGrandTotal());
			}
			response.setCheckedOutEarlier(false);
		} else {
			response.setCheckedOutEarlier(true);
		}

		return new BaseApiResponse<CheckoutResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

}
