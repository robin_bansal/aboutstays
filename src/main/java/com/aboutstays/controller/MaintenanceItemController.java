package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.MaintenanceItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.UpdateMaintenanceItemRequest;
import com.aboutstays.response.dto.AddMaintenanceItemRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.MaintenanceItemResponse;
import com.aboutstays.services.MaintenanceItemService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MaintenanceItemConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.MaintenanceItemConstants.BASE)
public class MaintenanceItemController {
	
	@Autowired
	private MaintenanceItemService mtItemService;
	
	@Autowired
	private ValidateIdsService validateIdService;
	
	
	@RequestMapping(value = RestMappingConstants.MaintenanceItemConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMaintenanceItemRequest> getSampleRequest() {
		AddMaintenanceItemRequest request = MaintenanceItemConverter.getSampleRequest();
		return new BaseApiResponse<AddMaintenanceItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.MaintenanceItemConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddMaintenanceItemRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMaintenanceItemRequest(request);
		if(MaintenanceItemType.getByCode(request.getItemType()) == null){
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MAINTENANCE_ITEM_TYPE);
		}
		validateIdService.validateHotelId(request.getHotelId());
		validateIdService.ValidateMaintenanceServiceId(request.getMaintenanceServiceId());
		String response = mtItemService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_MAINTENANCE_ITEM_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.MaintenanceItemConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<MaintenanceItemResponse>> getAll() throws AboutstaysException {
		List<MaintenanceItemResponse> responseList = mtItemService.getAll();
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_MAINTENANCE_ITEMS_FOUND);
		return new BaseApiResponse<List<MaintenanceItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceItemConstants.FETCH_BY_TYPE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<MaintenanceItemResponse>> fetchAllItemsByType(@RequestParam(value = RestMappingConstants.MaintenanceItemParameters.MT_SERVICE_ID) 
			String mtServicesId, @RequestParam(value = RestMappingConstants.MaintenanceItemParameters.TYPE)Integer type) throws AboutstaysException{
		MaintenanceItemType itemType = MaintenanceItemType.getByCode(type);
		if(itemType==null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MAINTENANCE_ITEM_TYPE);
		validateIdService.ValidateMaintenanceServiceId(mtServicesId);
		List<MaintenanceItemResponse> responseList = mtItemService.fetchByType(mtServicesId, itemType);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_MAINTENANCE_ITEMS_FOUND);
		return new BaseApiResponse<List<MaintenanceItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceItemConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.MaintenanceItemParameters.ID, required=true)String id) throws AboutstaysException{
		if(!mtItemService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_MAINTENANCE_ITEMS_FOUND);
		}
		Boolean response = mtItemService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_ITEM_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceItemConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<MaintenanceItemResponse> get(@RequestParam(value=RestMappingConstants.MaintenanceItemParameters.ID, required=true)String id) throws AboutstaysException{
		if(!mtItemService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_MAINTENANCE_ITEMS_FOUND);
		}
		MaintenanceItemResponse response = mtItemService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_MAINTENANCE_ITEMS_FOUND);
		}
		return new BaseApiResponse<MaintenanceItemResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.MaintenanceItemConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateMaintenanceItemRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MAINTENANCE_ITEM_ID);
		}
		if(MaintenanceItemType.getByCode(request.getItemType()) == null){
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MAINTENANCE_ITEM_TYPE);
		}
		validateIdService.validateHotelId(request.getHotelId());
		validateIdService.ValidateMaintenanceServiceId(request.getMaintenanceServiceId());
		Boolean response = mtItemService.updateNonNull(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_MAINTENANCE_ITEM_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	

	
	

}
