package com.aboutstays.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddEntertaimentItemRequest;
import com.aboutstays.request.dto.GetEntertaimentItemResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.services.EntertainmentItemService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.EntertainmentItemConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.BASE)
public class EntertainmentItemController {
	
	@Autowired 
	private ValidateIdsService validateIdsService;
	
	@Autowired
	private EntertainmentItemService eiService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddEntertaimentItemRequest> getSampleRequest(){
		AddEntertaimentItemRequest request = EntertainmentItemConverter.getSampleRequest();
		return new BaseApiResponse<AddEntertaimentItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddEntertaimentItemRequest request) throws AboutstaysException{
		validateAddItemRequest(request);
		String response = eiService.add(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_ENTERTAINMENT_ITEM_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	private void validateAddItemRequest(AddEntertaimentItemRequest request) throws AboutstaysException {
		ValidateUtils.validateEntertainmentItemRequest(request);
		validateIdsService.validateHotelId(request.getHotelId());
		validateIdsService.validateEntService(request.getServiceId());
		validateIdsService.validateMasterChannelId(request.getMasterItemId());
		if(eiService.existsByMasterItemId(request.getMasterItemId(), request.getHotelId(), request.getServiceId()))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.ITEM_ALREADY_EXISTS, request.getMasterItemId()));
	}
	
	
	@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.ADD_MULTIPLE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<String>> addMultiple(@RequestBody(required=true)List<AddEntertaimentItemRequest> listRequest) throws AboutstaysException{
		if(CollectionUtils.isEmpty(listRequest)){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ENTERTAINMENT_ITEM_REQUEST);
		}
		Set<String> masterItemIds = new HashSet<>();
		for(AddEntertaimentItemRequest request : listRequest){
			validateAddItemRequest(request);
			if(masterItemIds.remove(request.getMasterItemId())){
				throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.DUPLICATE_ENT_ITEM, request.getMasterItemId()));
			} else {
				masterItemIds.add(request.getMasterItemId());
			}
		}
		
		List<String> response = eiService.addMultiple(listRequest);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_ENTERTAINMENT_ITEM_FAILED);
		}
		return new BaseApiResponse<List<String>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetEntertaimentItemResponse>> getAll(@RequestParam(value=RestMappingConstants.EntertainmentItemParameters.ENT_SERVICE_ID, required=false)String entServiceId) throws AboutstaysException {
		List<GetEntertaimentItemResponse> responseList = null;
		if(!StringUtils.isEmpty(entServiceId)){
			validateIdsService.validateEntService(entServiceId);
			responseList = eiService.getAll(entServiceId);
		} else {
			responseList = eiService.getAll();
		}
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_ENTERTAIMENT_ITEM_FOUND);
		return new BaseApiResponse<List<GetEntertaimentItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.GET_ALL_BY_CATEGORY, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetEntertaimentItemResponse>> getAllByCategory(@RequestParam(value=RestMappingConstants.EntertainmentItemParameters.ENT_SERVICE_ID, required=true)String entServiceId,
			@RequestParam(value=RestMappingConstants.EntertainmentItemParameters.GENERE_ID, required=true)String genereId) throws AboutstaysException {
		validateIdsService.validateEntServiceAndGenereId(entServiceId, genereId);
		List<GetEntertaimentItemResponse> responseList = eiService.getAll(entServiceId, genereId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_ENTERTAIMENT_ITEM_FOUND);
		return new BaseApiResponse<List<GetEntertaimentItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetEntertaimentItemResponse> get(@RequestParam(value=RestMappingConstants.EntertainmentItemParameters.ID, required=true)String id) throws AboutstaysException {
		GetEntertaimentItemResponse response = eiService.getById(id);
		if (response == null)
			throw new AboutstaysException(AboutstaysResponseCode.NO_ENTERTAIMENT_ITEM_FOUND);
		return new BaseApiResponse<GetEntertaimentItemResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertainmentItemConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.EntertainmentItemParameters.ID, required=true)String id) throws AboutstaysException{
		if(!eiService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ENTERTAIMENT_ITEM_FOUND);
		}
		Boolean response = eiService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAINMENT_ITEM_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
//	@RequestMapping(value = RestMappingConstants.EntertainmentItemConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateEntertainmentRequest request) throws AboutstaysException{
//		if(StringUtils.isEmpty(request.getId())){
//			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ENTERTAINMENT_ITEM_ID);
//		}
//		validateAddItemRequest(request);
//		if(!eiService.existsById(request.getId()))
//			throw new AboutstaysException(AboutstaysResponseCode.NO_ENTERTAIMENT_ITEM_FOUND);
//		
//		boolean updated = eiService.update(request);
//		if(!updated){
//			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_ENTERTAINMENT_ITEM_FAILED);
//		}
//		return new BaseApiResponse<Boolean>(false, updated, AboutstaysResponseCode.SUCCESS);
//	}
	
}
