package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddSuggestedHotelRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.SuggestedHotelsResponse;
import com.aboutstays.services.SuggestedHotelsService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.SuggestedHotelsConstants.BASE)
public class SuggestedHotelController {
	
	@Autowired
	private SuggestedHotelsService suggestedHotelsService;
	
	@Autowired
	private ValidateIdsService validateIdService;
	
	@RequestMapping(value=RestMappingConstants.SuggestedHotelsConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddSuggestedHotelRequest request) throws AboutstaysException{
		ValidateUtils.validateSuggestedHotels(request);
		validateIdService.validateUserId(request.getUserId());
		String response = suggestedHotelsService.add(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_SUGGESTED_HOTELS_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.SuggestedHotelsConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<SuggestedHotelsResponse>> getAll() throws AboutstaysException{
		List<SuggestedHotelsResponse> response = suggestedHotelsService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_SUGGESTED_HOTELS_FOUND);
		}
		return new BaseApiResponse<List<SuggestedHotelsResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.SuggestedHotelsConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.SuggestedHotelsParameters.ID, required=true)String id) throws AboutstaysException{
		if(!suggestedHotelsService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_SUGGESTED_HOTELS_FOUND);
		}
		Boolean response = suggestedHotelsService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.SUGGESTED_HOTEL_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.SuggestedHotelsConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<SuggestedHotelsResponse> get(@RequestParam(value=RestMappingConstants.SuggestedHotelsParameters.ID, required=true)String id) throws AboutstaysException{
		SuggestedHotelsResponse response = suggestedHotelsService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_SUGGESTED_HOTELS_FOUND);
		}
		return new BaseApiResponse<SuggestedHotelsResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

}
