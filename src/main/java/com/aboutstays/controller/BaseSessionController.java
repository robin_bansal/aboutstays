package com.aboutstays.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aboutstays.services.BaseSessionService;
import com.aboutstays.services.ValidateIdsService;

public class BaseSessionController {
	@Autowired
	protected ValidateIdsService validateIdsService;

	@Autowired
	protected BaseSessionService baseSessionService;
	
	protected Logger logger = Logger.getLogger(getClass().getName());		
}
