package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.MessageStatus;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.MessageCreationType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddMessageRequest;
import com.aboutstays.request.dto.ChangeMessageStatusRequest;
import com.aboutstays.request.dto.GetStayMessagesRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetChatUsersResponse;
import com.aboutstays.response.dto.GetMessageListResponse;
import com.aboutstays.response.dto.GetMessageResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.MessageService;
import com.aboutstays.services.UserService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MessageConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.MessageConstants.BASE)
public class MessageController {

	@Autowired
	private MessageService messageService;

	@Autowired
	private UserService userService;

	@Autowired
	private HotelService hotelService;

	@Autowired
	private ValidateIdsService validateIdsService;

	@RequestMapping(value = RestMappingConstants.REQUEST, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMessageRequest> getSampleRequest() {
		AddMessageRequest request = MessageConverter.getSampleRequest();
		return new BaseApiResponse<AddMessageRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.MessageConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addMessage(@RequestBody(required = true) AddMessageRequest request)
			throws AboutstaysException {
		ValidateUtils.validateAddMessageRequest(request);
		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
		if (MessageCreationType.findByCode(request.getCreationType()) == null) {
			request.setCreationType(MessageCreationType.BY_USER.getCode());
		}
		String id = messageService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.MESSAGE_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.MessageConstants.GET_BY_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMessageResponse> getMessageById(
			@RequestParam(value = RestMappingConstants.MessageParameters.ID) String id) throws AboutstaysException {
		GetMessageResponse response = messageService.getMessageById(id);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.MESSAGE_NOT_FOUND);
		}
		return new BaseApiResponse<GetMessageResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.MessageConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMessageListResponse> getAllMessages() throws AboutstaysException {
		List<GetMessageResponse> messages = messageService.getAll();
		if (CollectionUtils.isEmpty(messages)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_MESSAGES_FOUND);
		}
		GetMessageListResponse response = new GetMessageListResponse();
		response.setMessages(messages);
		return new BaseApiResponse<GetMessageListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.MessageConstants.DELETE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteMessage(
			@RequestParam(value = RestMappingConstants.MessageParameters.ID) String id) throws AboutstaysException {
		if (!messageService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.MESSAGE_NOT_FOUND);
		messageService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.MESSAGE_DELETED_SUCCESSFULLT);
	}

	@RequestMapping(value = RestMappingConstants.MessageConstants.GET_STAY_MESSAGES, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMessageListResponse> getStayMessages(
			@RequestBody(required = true) GetStayMessagesRequest request) throws AboutstaysException {
		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
		List<GetMessageResponse> messages = messageService.getAll(request.getUserId(), request.getStaysId(),
				request.getHotelId());
		if (CollectionUtils.isEmpty(messages)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_MESSAGES_FOUND);
		}
		GetMessageListResponse response = new GetMessageListResponse();
		response.setMessages(messages);
		return new BaseApiResponse<GetMessageListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.MessageConstants.READ_MESSAGES, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> changeStatus(@RequestBody(required = true) ChangeMessageStatusRequest request)
			throws AboutstaysException {
		ValidateUtils.validateReadMessageRequest(request);
		if (!userService.existsByUserId(request.getUserId())) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		MessageCreationType messageCreationType = MessageCreationType.findByCode(request.getCreationType());
		if (messageCreationType == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CREATION_TYPE);
		}
		messageService.changeStatus(request.getUserId(), request.getHotelId(), messageCreationType, MessageStatus.READ);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.MESSAGE_STATUS_CHANGED_SUCCESSFULLY);
	}
	@RequestMapping(value=RestMappingConstants.MessageConstants.GET_CHAT_USERS, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetChatUsersResponse>> getChatUsers(@RequestParam(value=RestMappingConstants.MessageParameters.HOTEL_ID)String hotelId) throws AboutstaysException{
		validateIdsService.validateHotelId(hotelId);
		List<GetChatUsersResponse> response=messageService.getChatUsersByHotelId(hotelId);
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_CHAT_USERS);
		}
		return new BaseApiResponse<List<GetChatUsersResponse>>(false,response,AboutstaysResponseCode.SUCCESS);
	}
	
}
