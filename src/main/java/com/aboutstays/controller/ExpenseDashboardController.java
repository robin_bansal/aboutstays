package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.FoodOrder;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CheckinRequestStatus;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.CategoryExpenseItem;
import com.aboutstays.pojos.DateBasedExpenseItem;
import com.aboutstays.pojos.ExpenseItem;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.pojos.HousekeepingItemOrderDetails;
import com.aboutstays.pojos.LaundryItemOrderDetails;
import com.aboutstays.pojos.TimeBasedExpenseItem;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.GetAirportPickupResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.GetCommuteOrderResponse;
import com.aboutstays.response.dto.GetExpenseByCategoryResponse;
import com.aboutstays.response.dto.GetExpenseByDateResponse;
import com.aboutstays.response.dto.GetFoodOrderResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;
import com.aboutstays.response.dto.GetHKOrderResponse;
import com.aboutstays.response.dto.GetHotelGeneralInfoResponse;
import com.aboutstays.response.dto.GetLaundryOrderResponse;
import com.aboutstays.response.dto.GetReservationOrderResponse;
import com.aboutstays.response.dto.InternetOrderResponse;
import com.aboutstays.response.dto.LateCheckoutResposne;
import com.aboutstays.services.AirportPickupService;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.CommuteOrderService;
import com.aboutstays.services.FoodOrdersService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HKOrderService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.InternetOrderService;
import com.aboutstays.services.LateCheckoutRequestService;
import com.aboutstays.services.LaundryOrderService;
import com.aboutstays.services.ReservationOrderService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.ExpenseConstants.BASE)
public class ExpenseDashboardController {

	@Autowired
	private HotelService hotelService;

	@Autowired
	private GeneralServicesService generalServicesService;

	@Autowired
	private UserOptedServicesService userOptedServicesService;

	@Autowired
	private FoodOrdersService foodOrdersService;

	@Autowired
	private LaundryOrderService laundryOrderService;
	
	@Autowired
	private HKOrderService hkOrderService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@Autowired
	private CommuteOrderService coService;
	
	@Autowired
	private AirportPickupService apService;
	
	@Autowired
	private InternetOrderService ioService;
	
	@Autowired
	private LateCheckoutRequestService lcrService;
	
	@Autowired
	private ReservationOrderService rsService;
	
	@Autowired
	private CheckinService checkinService;

	@RequestMapping(value = RestMappingConstants.ExpenseConstants.GENERATE_CATEGORY, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetExpenseByCategoryResponse> generate(
			@RequestBody(required = true) UserStayHotelIdsPojo request) throws AboutstaysException {
		ValidateUtils.validateGenerateDashboardRequest(request);
		validateIdsRequest(request.getUserId(), request.getHotelId(), request.getStaysId());
		List<UserOptedService> listUOS = userOptedServicesService.getByUserStaysAndHotel(request.getStaysId(),
				request.getUserId(), request.getHotelId());

		GetExpenseByCategoryResponse response = new GetExpenseByCategoryResponse();
		Double grandTotal = 0.0;

		Map<String, CategoryExpenseItem> gsIdVsItemMap = new HashMap<>();
		Map<String, GetGeneralServiceResponse> gsIdVsResponse = new HashMap<>();
		for (UserOptedService userOptedService : listUOS) {

			RefCollectionType typeEnum = RefCollectionType.findValueByType(userOptedService.getRefCollectionType());
			if (typeEnum == null) {
				continue;
			}

			String gsId = userOptedService.getGsId();
			GetGeneralServiceResponse gsResponse = gsIdVsResponse.get(gsId);
			if (gsResponse == null) {
				gsResponse = generalServicesService.getById(gsId);
				if (gsResponse == null) {
					continue;
				}
				gsIdVsResponse.put(gsId, gsResponse);
			}
			String expenseGsId = gsResponse.getExpenseId();
			if(!StringUtils.isEmpty(expenseGsId)){
				GetGeneralServiceResponse expenseGsResponse = gsIdVsResponse.get(expenseGsId);
				if(expenseGsResponse==null){
					expenseGsResponse = generalServicesService.getById(expenseGsId);
				}
				if(expenseGsResponse!=null){
					gsIdVsResponse.put(expenseGsId, expenseGsResponse);
					gsResponse = expenseGsResponse;
					gsId = expenseGsId;
				}
			}

			Double subTotal = 0.0;

			CategoryExpenseItem expenseItem = gsIdVsItemMap.get(gsId);
			if (expenseItem == null) {
				expenseItem = new CategoryExpenseItem();
				expenseItem.setTotal(subTotal);
				expenseItem.setIconUrl(gsResponse.getActivatedImageUrl());
				expenseItem.setName(gsResponse.getName());
				gsIdVsItemMap.put(gsId, expenseItem);
			}

			String refId = userOptedService.getRefId();

			switch (typeEnum) {
			case FOOD: {
				GetFoodOrderResponse foodResponse = foodOrdersService.getById(refId);
				
				if (foodResponse != null && CollectionUtils.isNotEmpty(foodResponse.getFoodItemOrders())) {
					if(foodResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						for (FoodItemOrderDetails foodItemOrderDetails : foodResponse.getFoodItemOrders()) {
							subTotal = subTotal + foodItemOrderDetails.getPrice() * foodItemOrderDetails.getQuantity();
						}
					} 
				} else {
					continue;
				}
				break;
			}
			case AIRPORT_PICKUP: {
				GetAirportPickupResponse pickupResponse = apService.getById(refId);
				
				if (pickupResponse != null) {
					if(pickupResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						subTotal = subTotal + pickupResponse.getPrice();
					} 
				} else {
					continue;
				}
				break;
			}
			case COMMUTE: {
				GetCommuteOrderResponse commuteResponse = coService.getById(refId);
				
				if (commuteResponse != null) {
					if(commuteResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						subTotal = subTotal + commuteResponse.getPrice();
					} 
				} else {
					continue;
				}
				break;
			}
			case INTERNET: {
				InternetOrderResponse internetResponse = ioService.getById(refId);
				
				if (internetResponse != null) {
					if(internetResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						if(internetResponse.getInternetPack() != null){
							subTotal = subTotal + internetResponse.getInternetPack().getPrice();
						}
					} 
				} else {
					continue;
				}
				break;
			}
			case RESERVATION : {
				GetReservationOrderResponse reservationResponse = rsService.getById(refId);
				
				if (reservationResponse != null) {
					if(reservationResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						subTotal = subTotal + reservationResponse.getPrice();
					} 
				} else {
					continue;
				}
				break;
			}
			case LATE_CHECKOUT:{
				LateCheckoutResposne lcrResponse = lcrService.getById(refId);
				
				if (lcrResponse != null) {
					if(lcrResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						subTotal = subTotal + lcrResponse.getPrice();
					} 
				} else {
					continue;
				}
				break;
			}
			case LAUNDRY: {
				GetLaundryOrderResponse laundryResponse = laundryOrderService.getById(refId);
				if (laundryResponse != null && CollectionUtils.isNotEmpty(laundryResponse.getListOrders())) {
					if(laundryResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()) {
						for (LaundryItemOrderDetails laundryItemOrderDetails : laundryResponse.getListOrders()) {
							subTotal = subTotal
									+ laundryItemOrderDetails.getPrice() * laundryItemOrderDetails.getQuantity();
						}
					}
				} else {
					continue;
				}
				break;
			}
			case HOUSEKEEPING: {
				GetHKOrderResponse hkOrderResponse = hkOrderService.getById(refId);
				if(hkOrderResponse != null && CollectionUtils.isNotEmpty(hkOrderResponse.getListOrders())){
					if(hkOrderResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()) {
						for(HousekeepingItemOrderDetails hkItemOrderDetails : hkOrderResponse.getListOrders()) {
							subTotal = subTotal
									+ hkItemOrderDetails.getPrice() * hkItemOrderDetails.getQuantity();
						}
					}
				} else {
					continue;
				}
				break;
			}
			default:
				gsIdVsItemMap.remove(gsId);
				continue;
			}
			
			expenseItem.setTotal(expenseItem.getTotal() + subTotal);
			grandTotal = grandTotal + subTotal;

		}
//		if (CollectionUtils.isMapEmpty(gsIdVsItemMap)) {
//			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
//		}
		
		if(CollectionUtils.isMapNotEmpty(gsIdVsItemMap)) {
			Iterator<Map.Entry<String, CategoryExpenseItem>> iterator = gsIdVsItemMap.entrySet().iterator();
			while(iterator.hasNext()){
				Map.Entry<String, CategoryExpenseItem> entry = iterator.next();
				CategoryExpenseItem item = entry.getValue();
				if(item.getTotal() == null || item.getTotal() <= 0.0){
					iterator.remove();
				}
			}
		}

		
		List<CategoryExpenseItem> listCategoryItems = new ArrayList<>();
		listCategoryItems.addAll(gsIdVsItemMap.values());
		
		// For early checkIn request
		CheckinResponse checkinResponse = checkinService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
		if(checkinResponse != null) {
			if(checkinResponse.getEarlyCheckinRequest() != null) {
				if(checkinResponse.getEarlyCheckinRequested()) {
					CheckinRequestStatus checkInStatus = CheckinRequestStatus.findByCode(checkinResponse.getStatus());
					if(checkInStatus != null) {
						if(checkinResponse.getStatus() == CheckinRequestStatus.APPROVED.getCode()){
							CategoryExpenseItem earlyCheckinExpenseItem = new CategoryExpenseItem();
							GetHotelGeneralInfoResponse hotelResponse = hotelService.getGeneralInfoById(request.getHotelId());
							if(hotelResponse != null && hotelResponse.getGeneralInformation() != null) {
								earlyCheckinExpenseItem.setIconUrl(hotelResponse.getGeneralInformation().getLogoUrl());
								}
							earlyCheckinExpenseItem.setName("Early Checkin-In");
							earlyCheckinExpenseItem.setTotal(checkinResponse.getEarlyCheckinRequest().getCharges());
							grandTotal = grandTotal + checkinResponse.getEarlyCheckinRequest().getCharges();
							listCategoryItems.add(earlyCheckinExpenseItem);
						}
					}
				}
			}
		}

		//response.setListServiceExpense(new ArrayList<>(gsIdVsItemMap.values()));
		response.setGrandTotal(grandTotal);
		response.setListServiceExpense(listCategoryItems);
		return new BaseApiResponse<GetExpenseByCategoryResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.ExpenseConstants.GENERATE_TIME_BASED, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetExpenseByDateResponse> generateTimeBased(
			@RequestBody(required = true) UserStayHotelIdsPojo request) throws AboutstaysException {
		ValidateUtils.validateGenerateDashboardRequest(request);
		validateIdsRequest(request.getUserId(), request.getHotelId(), request.getStaysId());
		List<UserOptedService> listUOS = userOptedServicesService.getByUserStaysAndHotel(request.getStaysId(),
				request.getUserId(), request.getHotelId());
		if (CollectionUtils.isEmpty(listUOS)) {
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		GetExpenseByDateResponse response = new GetExpenseByDateResponse();

		Double grandTotal = 0.0;
		Double dateWiseTotal = 0.0;
		Map<String, GetGeneralServiceResponse> gsIdVsResponse = new HashMap<>();
		Map<String, DateBasedExpenseItem> dateVsExpenseItemsMap = new HashMap<>();

		for (UserOptedService userOptedService : listUOS) {
			Long createdTime = userOptedService.getCreated();
			String createdDate = DateUtil.format(new Date(createdTime), DateUtil.BASE_DATE_FORMAT);

			
			DateBasedExpenseItem dateBasedExpense = dateVsExpenseItemsMap.get(createdDate);
			if(dateBasedExpense == null){
				dateBasedExpense = new DateBasedExpenseItem();
				dateWiseTotal = 0.0; 
				dateVsExpenseItemsMap.put(createdDate, dateBasedExpense);
			}
			
			List<TimeBasedExpenseItem> expenseItemListByDate = dateBasedExpense.getListTimeBasedExpenseItem();
			if (expenseItemListByDate == null) {
				expenseItemListByDate = new ArrayList<>();
			}

			String gsId = userOptedService.getGsId();
			GetGeneralServiceResponse gsResponse = gsIdVsResponse.get(gsId);
			if (gsResponse == null) {
				gsResponse = generalServicesService.getById(gsId);
				if (gsResponse == null) {
					continue;
				}
				gsIdVsResponse.put(gsId, gsResponse);
			}
			String expenseGsId = gsResponse.getExpenseId();
			if(!StringUtils.isEmpty(expenseGsId)){
				GetGeneralServiceResponse expenseGsResponse = gsIdVsResponse.get(expenseGsId);
				if(expenseGsResponse==null){
					expenseGsResponse = generalServicesService.getById(expenseGsId);
				}
				if(expenseGsResponse!=null){
					gsIdVsResponse.put(expenseGsId, expenseGsResponse);
					gsResponse = expenseGsResponse;
					gsId = expenseGsId;
				}
			}

			
			Double timeWiseTotal = 0.0;
			TimeBasedExpenseItem timeBasedExpenseItem = new TimeBasedExpenseItem();
			timeBasedExpenseItem.setIconUrl(gsResponse.getActivatedImageUrl());
			timeBasedExpenseItem.setName(gsResponse.getName());

			String refId = userOptedService.getRefId();

			List<ExpenseItem> subItemList = new ArrayList<>();
			
			RefCollectionType typeEnum = RefCollectionType.findValueByType(userOptedService.getRefCollectionType());
			if (typeEnum == null) {
				continue;
			}
			switch (typeEnum) {
			case FOOD: {
				//GetFoodOrderResponse foodResponse = foodOrdersService.getById(refId);
				FoodOrder foodResponse = foodOrdersService.getEntity(refId);
				
				if (foodResponse != null && CollectionUtils.isNotEmpty(foodResponse.getFoodItemOrders())) {
					if(foodResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						for (FoodItemOrderDetails foodItemOrderDetails : foodResponse.getFoodItemOrders()) {
							Double subTotal = 0.0;
							subTotal = subTotal + foodItemOrderDetails.getPrice() * foodItemOrderDetails.getQuantity();
							timeWiseTotal+=subTotal;
							ExpenseItem expenseItem = new ExpenseItem();
							expenseItem.setTotal(subTotal);
							expenseItem.setName(MenuItemType.getByCode(foodItemOrderDetails.getType()).getDisplayName());
							subItemList.add(expenseItem);
						}
					} 
				} else {
					continue;
				}
				break;
			}
			case AIRPORT_PICKUP :{
				GetAirportPickupResponse pickupResponse = apService.getById(refId);
				
				if(pickupResponse != null){
					if(pickupResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						Double subTotal = 0.0;
						subTotal = subTotal + pickupResponse.getPrice();
						timeWiseTotal+=subTotal;
						ExpenseItem expenseItem = new ExpenseItem();
						expenseItem.setTotal(subTotal);
						if(pickupResponse.getDrop()){
							expenseItem.setName("Airport drop");
						} else {
							expenseItem.setName("Airport pickup");
						}
						subItemList.add(expenseItem);
					}
				} else {
					continue;
				}
				break;
			}
				
			case COMMUTE : {
				GetCommuteOrderResponse commuteResponse = coService.getById(refId);
				
				if(commuteResponse != null){
					if(commuteResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						Double subTotal = 0.0;
						subTotal = subTotal + commuteResponse.getPrice();
						timeWiseTotal+=subTotal;
						ExpenseItem expenseItem = new ExpenseItem();
						expenseItem.setTotal(subTotal);
						expenseItem.setName(commuteResponse.getPackageName());
						subItemList.add(expenseItem);
					}
				} else {
					continue;
				}
				break;
			}
			
			case RESERVATION : {
				GetReservationOrderResponse reservationResponse = rsService.getById(refId);
				
				if(reservationResponse != null){
					if(reservationResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						Double subTotal = 0.0;
						subTotal = subTotal + reservationResponse.getPrice();
						timeWiseTotal+=subTotal;
						ExpenseItem expenseItem = new ExpenseItem();
						expenseItem.setTotal(subTotal);
						expenseItem.setName(reservationResponse.getName());
						subItemList.add(expenseItem);
					}
				} else {
					continue;
				}
				break;
			}
			
			case LATE_CHECKOUT : {
				LateCheckoutResposne checkoutResponse = lcrService.getById(refId);
				
				if(checkoutResponse != null){
					if(checkoutResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						Double subTotal = 0.0;
						subTotal = subTotal + checkoutResponse.getPrice();
						timeWiseTotal+=subTotal;
						ExpenseItem expenseItem = new ExpenseItem();
						expenseItem.setTotal(subTotal);
						expenseItem.setName("Late checkout");
						subItemList.add(expenseItem);
					}
				} else {
					continue;
				}
				break;
			}
			
			case INTERNET: {
				InternetOrderResponse internetResponse = ioService.getById(refId);
				
				if(internetResponse != null){
					if(internetResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						Double subTotal = 0.0;
						subTotal = subTotal + internetResponse.getInternetPack().getPrice();
						timeWiseTotal+=subTotal;
						ExpenseItem expenseItem = new ExpenseItem();
						expenseItem.setTotal(subTotal);
						expenseItem.setName(internetResponse.getInternetPack().getDescription());
						subItemList.add(expenseItem);
					}
				} else {
					continue;
				}
				break;
			}
			
			case LAUNDRY: {
				GetLaundryOrderResponse laundryResponse = laundryOrderService.getById(refId);
				if (laundryResponse != null && CollectionUtils.isNotEmpty(laundryResponse.getListOrders())) {
					if(laundryResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()) {
						for (LaundryItemOrderDetails laundryItemOrderDetails : laundryResponse.getListOrders()) {
							Double subTotal = 0.0;
							subTotal = subTotal
									+ laundryItemOrderDetails.getPrice() * laundryItemOrderDetails.getQuantity();
							timeWiseTotal +=subTotal;
							ExpenseItem expenseItem = new ExpenseItem();
							expenseItem.setTotal(subTotal);
							expenseItem.setName(LaundryItemType.getNameByCode(laundryItemOrderDetails.getType()));
							subItemList.add(expenseItem);
						}
					}
				} else {
					continue;
				}
				break;
			}
			case HOUSEKEEPING: {
				GetHKOrderResponse hkOrderResponse = hkOrderService.getById(refId);
				if (hkOrderResponse != null && CollectionUtils.isNotEmpty(hkOrderResponse.getListOrders())) {
					if(hkOrderResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()){
						for (HousekeepingItemOrderDetails hkItemOrderDetails : hkOrderResponse.getListOrders()) {
							Double subTotal = 0.0;
							subTotal = subTotal
									+ hkItemOrderDetails.getPrice() * hkItemOrderDetails.getQuantity();
							timeWiseTotal +=subTotal;
							ExpenseItem expenseItem = new ExpenseItem();
							expenseItem.setTotal(subTotal);
							expenseItem.setName(HousekeepingItemType.getNameByCode(hkItemOrderDetails.getType()));
							subItemList.add(expenseItem);
						}
					}	
				} else {
					continue;
				}
				break;
			}
				
			}
			

			dateWiseTotal += timeWiseTotal;
			grandTotal +=timeWiseTotal;
				
			timeBasedExpenseItem.setCreated(createdTime);
			timeBasedExpenseItem.setTotal(timeWiseTotal);
			timeBasedExpenseItem.setSubExpenseItemList(subItemList);
			expenseItemListByDate.add(timeBasedExpenseItem);
			dateBasedExpense.setDateWiseTotal(dateWiseTotal);
			dateBasedExpense.setListTimeBasedExpenseItem(expenseItemListByDate);
			dateVsExpenseItemsMap.put(createdDate, dateBasedExpense);

		}
		
		Iterator<Map.Entry<String, DateBasedExpenseItem>> iterator = dateVsExpenseItemsMap.entrySet().iterator();
		while(iterator.hasNext()){
			Map.Entry<String, DateBasedExpenseItem> entry = iterator.next();
			DateBasedExpenseItem dateBasedExpenseItem = (DateBasedExpenseItem)entry.getValue();
			if(dateBasedExpenseItem != null){
				if(dateBasedExpenseItem.getDateWiseTotal() == null || dateBasedExpenseItem.getDateWiseTotal() == 0) {
					iterator.remove();
				} else {
					List<TimeBasedExpenseItem> timeBasedExpenseItemsList = dateBasedExpenseItem.getListTimeBasedExpenseItem();
					if(timeBasedExpenseItemsList != null){
						Iterator<TimeBasedExpenseItem> listIterator = timeBasedExpenseItemsList.iterator();
						while(listIterator.hasNext()){
							TimeBasedExpenseItem timeBasedExpenseItem = listIterator.next();
							if(timeBasedExpenseItem.getTotal() == null || timeBasedExpenseItem.getTotal() == 0.0){
								listIterator.remove();
							}
						}
					}
				}
			}

		}
		
		// For early checkIn request
		CheckinResponse checkinResponse = checkinService.getByUserStaysHotelId(request.getUserId(), request.getStaysId(), request.getHotelId());
		if(checkinResponse != null) {
			if(checkinResponse.getEarlyCheckinRequest() != null) {
				if(checkinResponse.getEarlyCheckinRequested()) {
					CheckinRequestStatus checkInStatus = CheckinRequestStatus.findByCode(checkinResponse.getStatus());
					if(checkInStatus != null) {
						if(checkinResponse.getStatus() == CheckinRequestStatus.APPROVED.getCode()){
							if(checkinResponse.getUpdated() != null){
								Long createdTime = checkinResponse.getUpdated();
								String createdDate = DateUtil.format(new Date(createdTime), DateUtil.BASE_DATE_FORMAT);
								
								DateBasedExpenseItem earlyCheckinExpenseItem =  dateVsExpenseItemsMap.get(createdDate);
								if(earlyCheckinExpenseItem == null) {
									earlyCheckinExpenseItem = new DateBasedExpenseItem();
									dateVsExpenseItemsMap.put(createdDate, earlyCheckinExpenseItem);
								}
								
								List<TimeBasedExpenseItem> listEarlyCheckinTimeBased = earlyCheckinExpenseItem.getListTimeBasedExpenseItem();
								if(listEarlyCheckinTimeBased == null) {
									listEarlyCheckinTimeBased = new ArrayList<>();
								}
								TimeBasedExpenseItem earlyCheckinTimeBased = new TimeBasedExpenseItem();
								
								earlyCheckinTimeBased.setCreated(checkinResponse.getUpdated());
								earlyCheckinTimeBased.setName("Early Check-IN");
								earlyCheckinTimeBased.setTotal(checkinResponse.getEarlyCheckinRequest().getCharges());
								String hotelIcon = "";
								GetHotelGeneralInfoResponse hotelResponse = hotelService.getGeneralInfoById(request.getHotelId());
								if(hotelResponse != null && hotelResponse.getGeneralInformation() != null) {
									hotelIcon = hotelResponse.getGeneralInformation().getLogoUrl();
								}
								earlyCheckinTimeBased.setIconUrl(hotelIcon);
								
								List<ExpenseItem> listEarlyCheckinExpenseItem = new ArrayList<>();
								ExpenseItem earlyCheckinSubListItem = new ExpenseItem();
								earlyCheckinSubListItem.setIconUrl(hotelIcon);
								earlyCheckinSubListItem.setName("early check-in");
								earlyCheckinSubListItem.setTotal(checkinResponse.getEarlyCheckinRequest().getCharges());
								listEarlyCheckinExpenseItem.add(earlyCheckinSubListItem);
								earlyCheckinTimeBased.setSubExpenseItemList(listEarlyCheckinExpenseItem);
								listEarlyCheckinTimeBased.add(earlyCheckinTimeBased);
								
								earlyCheckinExpenseItem.setDateWiseTotal(checkinResponse.getEarlyCheckinRequest().getCharges());								
								earlyCheckinExpenseItem.setListTimeBasedExpenseItem(listEarlyCheckinTimeBased);
								
								grandTotal = grandTotal + checkinResponse.getEarlyCheckinRequest().getCharges();
								dateVsExpenseItemsMap.put(createdDate, earlyCheckinExpenseItem);
							}
							
						}
					}
				}
			}
		}
			
		response.setDateVsExpenseItemList(dateVsExpenseItemsMap);
		response.setGrandTotal(grandTotal);
		return new BaseApiResponse<GetExpenseByDateResponse>(false, response, AboutstaysResponseCode.SUCCESS);

	}

	public void validateIdsRequest(String userId, String hotelId, String staysId) throws AboutstaysException {
		validateIdsService.validateUserHotelStaysIds(userId, hotelId, staysId);
	}

}
