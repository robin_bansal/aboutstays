package com.aboutstays.controller;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AirportRequest;
import com.aboutstays.response.dto.AirportResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.services.AirportService;
import com.aboutstays.services.PlaceService;
import com.aboutstays.utils.AirportConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value=RestMappingConstants.AirportConstants.BASE)
public class AirportController {

	@Autowired
	private AirportService airportService;
	
	@Autowired
	private PlaceService placeService; 
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AirportRequest> getSampleRequest(){
		AirportRequest request = AirportConverter.getSampleRequest();
		return new BaseApiResponse<AirportRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addAirport(@RequestBody AirportRequest request) throws AboutstaysException{
		ValidateUtils.validateAirportRequest(request, false);
		if(!placeService.existsByPlaceId(request.getCityId()))
			throw new AboutstaysException(AboutstaysResponseCode.CITY_NOT_FOUND);
		String airportId = airportService.addAirport(request);
		return new BaseApiResponse<String>(false, airportId, AboutstaysResponseCode.AIRPORT_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateAirport(@RequestBody AirportRequest request) throws AboutstaysException{
		ValidateUtils.validateAirportRequest(request, true);
		if(!airportService.existsById(request.getAirportId()))
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_NOT_FOUND);
		if(!placeService.existsByPlaceId(request.getCityId()))
			throw new AboutstaysException(AboutstaysResponseCode.CITY_NOT_FOUND);
		airportService.updateNonNullAirport(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AIRPORT_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> deleteAirport(@RequestParam String airportId) throws AboutstaysException{
		if(!airportService.existsById(airportId))
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_NOT_FOUND);
		airportService.deleteAirport(airportId);
		return new BaseApiResponse<String>(false, airportId, AboutstaysResponseCode.AIRPORT_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AirportResponse> getById(@RequestParam String airportId) throws AboutstaysException{
		AirportResponse response = airportService.getAirport(airportId);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_NOT_FOUND);
		return new BaseApiResponse<AirportResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<AirportResponse>> getAll(@RequestParam(required=false) String cityId) throws AboutstaysException{
		List<AirportResponse> responseList = null;
		if(!StringUtils.isEmpty(cityId)){
			if(!placeService.existsByPlaceId(cityId))
				throw new AboutstaysException(AboutstaysResponseCode.CITY_NOT_FOUND);
			responseList = airportService.getAllByCityId(cityId);
		} else {
			responseList = airportService.getAll();
		}
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_AIRPORT_FOUND);
		return new BaseApiResponse<List<AirportResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
}
