package com.aboutstays.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.InitiateCheckinRequest;
import com.aboutstays.request.dto.ModifyCheckinRequest;
import com.aboutstays.request.dto.RetreiveCheckinRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.ProcessUtilityService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CheckinConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CheckinConstants.BASE)
public class CheckinController extends BaseSessionController{

	@Autowired
	private CheckinService checkinService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private StaysService staysService;

	@Autowired
	private UserOptedServicesService uosService;
	
	@Autowired
	private GeneralServicesService gsService;
	
	@Autowired
	private ProcessUtilityService processUtilityService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<InitiateCheckinRequest> getSampleRequest(){
		InitiateCheckinRequest initiateCheckinRequest = CheckinConverter.getSampleCheckinRequest();
		return new BaseApiResponse<InitiateCheckinRequest>(false, initiateCheckinRequest, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CheckinConstants.REQUEST_CHECKIN, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> initiateCheckin(@RequestBody(required=true)InitiateCheckinRequest request) throws AboutstaysException{
		ValidateUtils.validateInitiateCheckinRequest(request);
		validateIdsService.validateHotelIdStaysId(request.getHotelId(),request.getStaysId());
		//To fill this validation
		ValidateUtils.validatePersonalInfo(request.getPersonalInformation());
		ValidateUtils.validateIdentityDocuments(request.getIdentityDocuments());		

		String checkinId = checkinService.save(request);
		String staysId = request.getStaysId();
		boolean isOfficial = request.isOfficialTrip();
		staysService.changeTripType(staysId, isOfficial);
		return new BaseApiResponse<String>(false, checkinId, AboutstaysResponseCode.CHECKIN_INITIATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.CheckinConstants.MODIFY_CHECKIN, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> modifyCheckin(@RequestBody(required=true)ModifyCheckinRequest request) throws AboutstaysException{
		//To do Apply validations on request
		ValidateUtils.ModifyCheckinRequest(request);
		validateIdsService.validateHotelIdStaysId(request.getHotelId(),request.getStaysId());
		String checkinId = checkinService.update(request);
		String staysId = request.getStaysId();
		boolean isOfficial = request.isOfficialTrip();
		staysService.changeTripType(staysId, isOfficial);
//		if(request.getEarlyCheckinRequest()  != null && request.getEarlyCheckinRequest().isEarlyCheckinRequested()) {
//			UserOptedService uos = uosService.getByRefId(checkinId);
//			if(uos == null) {
//				GeneralService generalService = gsService.getByType(GeneralServiceType.EARLY_CHECK_IN);
//				if(generalService != null) {
//					AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestEarlyCheckinOrder(request, checkinId, generalService.getId());
//					UserOptedServiceController controller = new UserOptedServiceController();
//					beanFactory.autowireBean(controller);
//					controller.add(optedRequest);
//				}
//			}			
//		}
//		if(!StringUtils.isEmpty(request.getCheckinId())){//Added for testing purpose
//			boolean approveCheckinAutomatically = processUtilityService.getBooleanProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_APPROVE_CHECKIN_AUTOMATICALLY, false);
//			if(approveCheckinAutomatically){
//				checkinService.changeStatus(checkinId, CheckinRequestStatus.APPROVED);
//				staysService.changeStaysStatus(request.getStaysId(), StaysStatus.STAYING);
//			}
//		}
		
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CHECKIN_MODIFIED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.CheckinConstants.RETREIVE_CHECKIN, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<CheckinResponse> retreiveCheckin(@RequestBody(required=true)RetreiveCheckinRequest request) throws AboutstaysException{
		validateIdsService.validateStaysId(request.getStaysId());
		CheckinResponse response = checkinService.getById(request.getStaysId());
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.CHECKIN_NOT_FOUND);
		return new BaseApiResponse<CheckinResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
}
