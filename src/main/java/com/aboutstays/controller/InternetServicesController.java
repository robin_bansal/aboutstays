package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddInternetServiceRequest;
import com.aboutstays.request.dto.UpdateInternetServiceRequest;
import com.aboutstays.response.dto.AddInternetServiceResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetInternetServiceResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.InternetServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.InternetServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.InternetServiceConstants.BASE)
public class InternetServicesController{
	
	@Autowired
	private InternetServicesService internetServicesService;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.InternetServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddInternetServiceRequest> getSample(){
		AddInternetServiceRequest response = InternetServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddInternetServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddInternetServiceResponse> add(@RequestBody(required=true)AddInternetServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddInternetPackRequest(request);
		if(!hotelService.hotelExistsById(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		AddInternetServiceResponse response = internetServicesService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_INTERNET_PACK_FAILED);	
		}
		return new BaseApiResponse<AddInternetServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetInternetServiceResponse>> getAll() throws AboutstaysException{
		List<GetInternetServiceResponse> response = internetServicesService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_INTERNET_PACKS_FAILED);
		}
		return new BaseApiResponse<List<GetInternetServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetInternetServiceResponse> get(@RequestParam(value=RestMappingConstants.InternetServiceParameters.ID, required=true)String id) throws AboutstaysException{
		GetInternetServiceResponse response = internetServicesService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_PACK_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetInternetServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.InternetServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.InternetServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!internetServicesService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_PACK_SERVICE_NOT_EXISTS);
		}
		Boolean response = internetServicesService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_PACK_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateInternetServiceRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_INTERNET_PACK_SERVICE_ID);
		}
		Boolean response = internetServicesService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_INTERNET_PACK_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
	

