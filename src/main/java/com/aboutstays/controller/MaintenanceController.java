package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.MaintenanceItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.MaintenanceItemByType;
import com.aboutstays.request.dto.AddMaintenanceRequest;
import com.aboutstays.request.dto.UpdateMaintenanceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetMaintenaneResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.MaintenanceService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MaintenanceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.MaintenanceConstants.BASE)
public class MaintenanceController {
	
	@Autowired
	private MaintenanceService maintenanceService;
	
	@Autowired
	private HotelService hotelService;

	@RequestMapping(value=RestMappingConstants.MaintenanceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMaintenanceRequest> getSample(){
		AddMaintenanceRequest response = MaintenanceConverter.getSampleRequest();
		return new BaseApiResponse<AddMaintenanceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddMaintenanceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMaintenanceRequest(request);
		if(!hotelService.hotelExistsById(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		Iterator<MaintenanceItemByType> iterator = request.getListMaintenanceItemType().iterator();
		Set<Integer> maintenanceTypeSet = new HashSet<>(); 
		while (iterator.hasNext()) {
			MaintenanceItemByType itemByType = iterator.next();
			MaintenanceItemType itemType = MaintenanceItemType.getByCode(itemByType.getType()); 
			if(itemType==null){
				throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MAINTENANCE_ITEM_TYPE);
			}
			else if(maintenanceTypeSet.contains(itemByType.getType())){
				iterator.remove();
			} else {
				maintenanceTypeSet.add(itemByType.getType());
			}
			itemByType.setTypeName(itemType.getDisplayName());
			itemByType.setImageUrl(itemType.getImageUrl());
		}
		
		String response = maintenanceService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_MAINTENANCE_BY_TYPE_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetMaintenaneResponse>> getAll() throws AboutstaysException{
		List<GetMaintenaneResponse> response = maintenanceService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_MAINTENANCE_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetMaintenaneResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMaintenaneResponse> get(@RequestParam(value=RestMappingConstants.MaintenanceParameters.ID, required=true)String id) throws AboutstaysException{
		GetMaintenaneResponse response = maintenanceService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_NOT_FOUND);
		}
		return new BaseApiResponse<GetMaintenaneResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.MaintenanceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!maintenanceService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_NOT_FOUND);
		}
		Boolean response = maintenanceService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateMaintenanceRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MAINTENANCE_ID);
		}
		if(!maintenanceService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_NOT_FOUND);
		}
		if(CollectionUtils.isNotEmpty(request.getListMaintenanceItemType())){
			for(MaintenanceItemByType itemByType : request.getListMaintenanceItemType()){
				MaintenanceItemType itemType = MaintenanceItemType.getByCode(itemByType.getType()); 
				if(itemType==null){
					throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MAINTENANCE_ITEM_TYPE);
				}
				itemByType.setTypeName(itemType.getDisplayName());
				itemByType.setImageUrl(itemType.getImageUrl());
			}
		}
		Boolean response = maintenanceService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
