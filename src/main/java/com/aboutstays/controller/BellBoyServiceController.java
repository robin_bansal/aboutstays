package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.BellBoyServiceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.BellBoyServiceResponse;
import com.aboutstays.services.BellBoyServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.BellBoyServiceConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;


@Controller
@RequestMapping(value=RestMappingConstants.BellBoyServiceConstants.BASE)
public class BellBoyServiceController {

	@Autowired
	private BellBoyServicesService bellBoyServicesService;
	
	@Autowired
	private ValidateIdsService validateIdsService; 
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<BellBoyServiceRequest> getSampleRequest(){
		BellBoyServiceRequest request = BellBoyServiceConverter.getSampleRequest();
		 return new BaseApiResponse<BellBoyServiceRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.BellBoyServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addService(@RequestBody BellBoyServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateBellBoyServiceRequest(request);
		validateIdsService.validateHotelId(request.getHotelId());
		String id = bellBoyServicesService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.BELL_BOY_SERVICE_ADDED);
	}
	
	@RequestMapping(value=RestMappingConstants.BellBoyServiceConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateService(@RequestBody BellBoyServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateBellBoyServiceRequest(request);
		validateIdsService.validateHotelId(request.getHotelId());
		if(!bellBoyServicesService.existsByHotelId(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.BELL_BOY_SERVICE_NOT_FOUND);
		bellBoyServicesService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.BELL_BOY_SERVICE_UPDATED);
	}
	
	@RequestMapping(value=RestMappingConstants.BellBoyServiceConstants.GET_BY_HOTEL_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<BellBoyServiceResponse> getBellBoyService(@RequestParam String hotelId) throws AboutstaysException{
		BellBoyServiceResponse response = bellBoyServicesService.getByHotelId(hotelId);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.BELL_BOY_SERVICE_NOT_FOUND);
		return new BaseApiResponse<BellBoyServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.BellBoyServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BellBoyServiceResponse>> getAll() throws AboutstaysException{
		List<BellBoyServiceResponse> response = bellBoyServicesService.getAll();
		if(CollectionUtils.isEmpty(response))
			throw new AboutstaysException(AboutstaysResponseCode.NO_BELL_BOY_SERVICES_FOUND);
		return new BaseApiResponse<List<BellBoyServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
}
