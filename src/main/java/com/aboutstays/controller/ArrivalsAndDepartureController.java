package com.aboutstays.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.Booking;
import com.aboutstays.entities.GeneralService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.BookingStatus;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.CheckinPojo;
import com.aboutstays.pojos.EarlyCheckinRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.AllocateRoomRequest;
import com.aboutstays.request.dto.ApproveEarlyCheckinRequest;
import com.aboutstays.request.dto.CompleteCheckinRequest;
import com.aboutstays.request.dto.FreezeBookingRequest;
import com.aboutstays.request.dto.InitiateCheckinRequest;
import com.aboutstays.request.dto.ModifyCheckinRequest;
import com.aboutstays.request.dto.RoomgsAndStaysRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.GetIncompleteBookingsResponce;
import com.aboutstays.response.dto.RoomAndStaysData;
import com.aboutstays.services.BookingService;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.FloorService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.RoomService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.ArrivalsAndDeparturesConstants.BASE)
public class ArrivalsAndDepartureController {
	@Autowired
	private BookingService bookingService;
	@Autowired
	private RoomService roomService;
	@Autowired
	private ValidateIdsService validationService;
	@Autowired
	private StaysService staysService;
	@Autowired
	private FloorService floorService;
	@Autowired
	private CheckinService checkinService;
	@Autowired
	private GeneralServicesService gsService;
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	@Autowired
	private UserOptedServicesService uosService;

	@RequestMapping(value = RestMappingConstants.ArrivalsAndDeparturesConstants.GET_UNCOMPLETE_BOOKINGS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetIncompleteBookingsResponce>> getUnAllocatedBookings(
			@RequestParam(value = RestMappingConstants.ArrivalsAndDeparturesParameters.HOTEL_ID, required = true) String hotelId,
			@RequestParam(value = RestMappingConstants.ArrivalsAndDeparturesParameters.BOOKING_DATE, required = true) String bookingDate)
			throws AboutstaysException {
		ValidateUtils.dateAndTimeValidation(bookingDate, DateUtil.BASE_DATE_FORMAT,
				AboutstaysResponseCode.INVALID_DATE_FORMAT);
		validationService.validateHotelId(hotelId);
		List<GetIncompleteBookingsResponce> response = bookingService.getUnCompleteBookings(hotelId, bookingDate);
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_BOOKINGS);
		}
		return new BaseApiResponse<List<GetIncompleteBookingsResponce>>(true, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.ArrivalsAndDeparturesConstants.ALLOCATE_ROOM, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> allocateRoom(@RequestBody(required = true) AllocateRoomRequest request)
			throws AboutstaysException {
		ValidateUtils.validateAllocateRoom(request);
		Booking booking = bookingService.getEntityById(request.getBookingId());
		if (booking == null) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_BOOKING_ID);
		}
		if (!roomService.existsById(request.getRoomId())) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_NOT_FOUND);
		}
		bookingService.allotRoom(request);
		staysService.setRoomId(booking.getDefaultStaysId(), request.getRoomId());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.ArrivalsAndDeparturesConstants.FREEZE_BOOKING, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> freezeBooking(@RequestBody(required = true) FreezeBookingRequest request)
			throws AboutstaysException {
		ValidateUtils.validateFreezeBooking(request);
		for (String bookingId : request.getBookingIds()) {
			if (!bookingService.existsByBookingId(bookingId)) {
				throw new AboutstaysException(
						String.format(AboutstaysResponseCode.BOOKING_NOT_FOUND.getMessage(), bookingId),
						AboutstaysResponseCode.BOOKING_NOT_FOUND_WITH_ID.getCode());
			}
		}
		bookingService.freezeBooking(request.getBookingIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.ArrivalsAndDeparturesConstants.COMPLETE_CHECKIN, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> completeCheckin(@RequestBody(required = true) CompleteCheckinRequest request)
			throws AboutstaysException {
		ValidateUtils.validateCompleteCheckin(request);
		Booking booking = bookingService.getEntityById(request.getBookingId());
		if (booking==null) {
			throw new AboutstaysException(AboutstaysResponseCode.BOOKING_NOT_FOUND);
		}		
		CheckinResponse checkinResponse = checkinService.getById(booking.getDefaultStaysId());
		if(checkinResponse==null){
			InitiateCheckinRequest checkinPojo=new InitiateCheckinRequest();
			checkinPojo.setHotelId(booking.getHotelId());
			checkinPojo.setStaysId(booking.getDefaultStaysId());
			checkinPojo.setCheckinFormCompleted(request.getCheckinFormCompleted());
			checkinPojo.setIdVerified(request.getIdVerified());
			checkinPojo.setIsEarlyCheckinFee(request.getIsEarlyCheckinFee());
			EarlyCheckinRequest checkinRequest=new EarlyCheckinRequest();
			checkinRequest.setCharges(request.getEarlyCheckinFee());
//			checkinRequest.setRequestedCheckinTime(requestedCheckinTime);
			checkinPojo.setEarlyCheckinRequest(checkinRequest);

			String CheckinId = checkinService.save(checkinPojo);
			checkinResponse=checkinService.getById(CheckinId);
		}else{
			ModifyCheckinRequest modifyCheckinRequest=new ModifyCheckinRequest();
			modifyCheckinRequest.setStaysId(booking.getDefaultStaysId());
			modifyCheckinRequest.setCheckinFormCompleted(request.getCheckinFormCompleted());
			modifyCheckinRequest.setIdVerified(request.getIdVerified());
			modifyCheckinRequest.setIsEarlyCheckinFee(request.getIsEarlyCheckinFee());
			EarlyCheckinRequest earlyCheckinRequest = checkinResponse.getEarlyCheckinRequest();
			earlyCheckinRequest.setCharges(request.getEarlyCheckinFee());
			modifyCheckinRequest.setEarlyCheckinRequest(earlyCheckinRequest);
			checkinService.updateNonNull(modifyCheckinRequest);
		}
		bookingService.changeBookingStatus(request.getBookingId(), BookingStatus.COMPLETE);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.ArrivalsAndDeparturesConstants.ROOMS_AND_STAYS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<RoomAndStaysData>> getRoomsAndStays(
			@RequestBody(required = true) RoomgsAndStaysRequest request) throws AboutstaysException {
		ValidateUtils.validateRoomAndStaysRequest(request);
		if (!floorService.existsById(request.getFloorId())) {
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_NOT_FOUND);
		}
		List<RoomAndStaysData> response = staysService.getRoomsAndStays(request);
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_NOT_FOUND);
		}
		return new BaseApiResponse<List<RoomAndStaysData>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = { RestMappingConstants.ArrivalsAndDeparturesConstants.APPROVE_EARLY_CHECKIN,
			RestMappingConstants.ArrivalsAndDeparturesConstants.REJECT_EARLY_CHECKIN }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> approveEarlyCheckin(
			@RequestBody(required = true) ApproveEarlyCheckinRequest request, HttpServletRequest httpServletRequest)
			throws AboutstaysException {
		ValidateUtils.validateApproveEarlyCheckin(request);
		Booking booking = bookingService.getEntityById(request.getBookingId());
		if (booking == null) {
			throw new AboutstaysException(AboutstaysResponseCode.BOOKING_NOT_FOUND);
		}
		CheckinResponse response = checkinService.getById(booking.getDefaultStaysId());
		if (response == null)
			throw new AboutstaysException(AboutstaysResponseCode.CHECKIN_NOT_FOUND);
		if (response.getEarlyCheckinRequest() == null || !response.getEarlyCheckinRequested()) {
			throw new AboutstaysException(AboutstaysResponseCode.EARLY_CHECKIN_NOT_REQUESTED);
		} else {
			String requestUri = httpServletRequest.getRequestURI().substring(httpServletRequest.getRequestURI().lastIndexOf('/'));
			if (!StringUtils.isEmpty(requestUri)) {
				switch (requestUri) {
				case RestMappingConstants.ArrivalsAndDeparturesConstants.APPROVE_EARLY_CHECKIN:
					checkinService.updatePriceAndIsApproved(true, request.getPrice(), booking.getDefaultStaysId());
					// UserOptedService uos =uosService.getByRefId(response.getStaysId());
					// if (uos == null) {
					GeneralService generalService = gsService.getByType(GeneralServiceType.EARLY_CHECK_IN);
					if (generalService != null) {
						AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter
								.createUOSRequestEarlyCheckinOrder(response, generalService.getId());
						UserOptedServiceController controller = new UserOptedServiceController();
						beanFactory.autowireBean(controller);
						controller.add(optedRequest);
					}
					// }
					return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.EARLY_CHECKIN_APPROVED);
				case RestMappingConstants.ArrivalsAndDeparturesConstants.REJECT_EARLY_CHECKIN:
					checkinService.updatePriceAndIsApproved(false, request.getPrice(), booking.getDefaultStaysId());
					return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.EARLY_CHECKIN_REJECTED);
				}
			}
		}
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.FAILED);
	}
}
