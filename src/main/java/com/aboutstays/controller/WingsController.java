package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddWingRequest;
import com.aboutstays.response.dto.AddWingResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetWingResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.WingService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;
import com.aboutstays.utils.WingsConverter;

@Controller
@RequestMapping(value = RestMappingConstants.WingsConstants.BASE)
public class WingsController {

	@Autowired
	private WingService wingService;

	@Autowired
	private HotelService hotelService;

	@RequestMapping(value = RestMappingConstants.WingsConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddWingRequest> getSample() {
		AddWingRequest response = WingsConverter.getSampleReuest();
		return new BaseApiResponse<AddWingRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.WingsConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddWingResponse> add(@RequestBody(required = true) AddWingRequest request)
			throws AboutstaysException {
		ValidateUtils.validateAddWingRequest(request);

		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if (StringUtils.isEmpty(request.getCode())) {
			request.setCode(request.getName());
		}

		AddWingResponse response = wingService.addWing(request);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.ADDED_WING_FAILED);
		}
		return new BaseApiResponse<AddWingResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.WingsConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetWingResponse>> getAll() throws AboutstaysException {
		List<GetWingResponse> response = wingService.getAllWings();
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_WINGS_FAILED);
		}
		return new BaseApiResponse<List<GetWingResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.WingsConstants.DELETE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(
			@RequestParam(value = RestMappingConstants.WingsParameters.ID, required = true) String id)
			throws AboutstaysException {
		if (!wingService.existById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_NOT_FOUND);
		}
		Boolean response = wingService.delete(id);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.WingsConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetWingResponse> get(
			@RequestParam(value = RestMappingConstants.WingsParameters.ID, required = true) String id)
			throws AboutstaysException {
		GetWingResponse response = wingService.getWingById(id);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_NOT_FOUND);
		}
		return new BaseApiResponse<GetWingResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.WingsConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) GetWingResponse request)
			throws AboutstaysException {
		if (StringUtils.isEmpty(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
		if (StringUtils.isEmpty(request.getWingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_ID);
		}

		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if (!wingService.existById(request.getWingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_NOT_FOUND);
		}
		Boolean response = wingService.updateWing(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.WING_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	@RequestMapping(value = RestMappingConstants.WingsConstants.GET_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetWingResponse>> getByHotelId(@RequestParam(value = RestMappingConstants.WingsParameters.HOTEL_ID, required = true) String id) throws AboutstaysException {
		
		if(!hotelService.hotelExistsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);			
		}
		List<GetWingResponse> response = wingService.getByHotelId(id);
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_WINGS_FAILED);
		}
		return new BaseApiResponse<List<GetWingResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}


}
