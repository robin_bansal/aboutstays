package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddReservationOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.UpdateReservationOrderRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetReservationOrderResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.ReservationOrderService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.ReservationOrderConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.BASE)
public class ReservationOrderController extends BaseSessionController{
	
	
	@Autowired
	private ReservationOrderService rsOrderService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddReservationOrderRequest> getSample(){
		AddReservationOrderRequest response = ReservationOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddReservationOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	public BaseApiResponse<String> add(@RequestBody(required=true)AddReservationOrderRequest request, boolean skipValidations) throws AboutstaysException{
		if(!skipValidations){
			ValidateUtils.validateReservationOrder(request);
			baseSessionService.validateBaseSessionSessionData(request);
			validateIdsService.validateReservationItemId(request.getRsItemId());
		}
		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId()); 
		if(request.isRequestedNow()){
			request.setDeliveryTime(new Date().getTime()+timezoneDiff);
		} else {
			request.setDeliveryTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())+timezoneDiff);
		}

		String response = rsOrderService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_RESERVATION_ORDER_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSReqByRSOrder(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_RESERVATION_ORDER_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddReservationOrderRequest request) throws AboutstaysException{
		return add(request, false);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetReservationOrderResponse>> getAll() throws AboutstaysException{
		List<GetReservationOrderResponse> response = rsOrderService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<List<GetReservationOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetReservationOrderResponse> get(@RequestParam(value=RestMappingConstants.ReservationOrderParameters.ID, required=true)String id) throws AboutstaysException{
		GetReservationOrderResponse response = rsOrderService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<GetReservationOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.ReservationOrderParameters.ID, required=true)String id) throws AboutstaysException{
		if(!rsOrderService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_ORDER_NOT_FOUND);
		}
		Boolean response = rsOrderService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationOrderConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateReservationOrderRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_ORDER_ID);
		}
		if(!rsOrderService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_ORDER_NOT_FOUND);
		}
		baseSessionService.validateBaseSessionSessionData(request);

		Boolean response = rsOrderService.updateNonNullFieldsOfRequest(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_RESERVATION_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
