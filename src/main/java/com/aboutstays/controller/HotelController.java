package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.enums.HotelType;
import com.aboutstays.enums.HotelUpdateBy;
import com.aboutstays.enums.PartnershipType;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddAmenitiesToHotelRequest;
import com.aboutstays.request.dto.AddAwardRequest;
import com.aboutstays.request.dto.AddCardAcceptedRequest;
import com.aboutstays.request.dto.AddEssentialToHotelRequest;
import com.aboutstays.request.dto.CreateHotelRequest;
import com.aboutstays.request.dto.DeleteLoyaltyRequest;
import com.aboutstays.request.dto.GetHotelGeneralServices;
import com.aboutstays.request.dto.GetHotelsByCityAndNamesRequest;
import com.aboutstays.request.dto.LinkLoyaltyProgramsRequest;
import com.aboutstays.request.dto.UpdateHotelRequest;
import com.aboutstays.request.dto.UpdateHotelServices;
import com.aboutstays.request.dto.UpdatePartnershipRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CreateHotelResponse;
import com.aboutstays.response.dto.GetHotelBasicInfoList;
import com.aboutstays.response.dto.GetHotelGeneralInfoResponse;
import com.aboutstays.response.dto.GetHotelListResponse;
import com.aboutstays.response.dto.GetHotelResponse;
import com.aboutstays.response.dto.GetHotelRoomsResponse;
import com.aboutstays.response.dto.GetRoomCategoryResponse;
import com.aboutstays.response.dto.HotelMetaDataResponce;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.LoyaltyProgramsService;
import com.aboutstays.services.MasterAmenityService;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HotelConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.HotelConstants.BASE)
public class HotelController {

	@Autowired
	private HotelService hotelService;

	@Autowired
	private RoomCategoryService roomService;

	@Autowired
	private GeneralServicesService generalServicesService;

	@Autowired
	private MasterAmenityService masterAmenityService;

	@Autowired
	private LoyaltyProgramsService loyaltyProgramService;
	
	@Autowired
	private ValidateIdsService validateIdsService; 

	@RequestMapping(value = RestMappingConstants.HotelConstants.SAMPLE_REQUEST, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<CreateHotelRequest> sampleRequest() {
		CreateHotelRequest request = HotelConverter.getSampleRequest();
		return new BaseApiResponse<CreateHotelRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.CREATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<CreateHotelResponse> addHotel(
			@RequestBody(required = true) CreateHotelRequest createHotelRequest) throws AboutstaysException {
		ValidateUtils.validateCreateHotelRequest(createHotelRequest);
		Integer hotelTypeValue = createHotelRequest.getHotelType();
		if (hotelTypeValue != null) {
			HotelType hotelType = HotelType.findHotelByType(hotelTypeValue);
			if (hotelType == null) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_TYPE);
			}
		} else {
			createHotelRequest.setHotelType(HotelType.getDefaultValue());
		}
		Integer partnershipTypeValue = createHotelRequest.getPartnershipType();
		if (partnershipTypeValue != null) {
			PartnershipType partnershipType = PartnershipType.findValueByCode(partnershipTypeValue);
			if (partnershipType == null) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_PARTNER_TYPE);
			}
		} else {
			createHotelRequest.setPartnershipType(PartnershipType.getDefaultParternshipType());
		}

		CreateHotelResponse response = hotelService.createHotel(createHotelRequest);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_CREATION_FAILED);
		}
		return new BaseApiResponse<CreateHotelResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.GET_HOTELS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHotelListResponse> getHotels(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_TYPE, required = false) Integer hotelType)
			throws AboutstaysException {
		if (hotelType != null) {
			ValidateUtils.validateHotelType(hotelType);
		}
		GetHotelListResponse response;
		if (HotelType.findHotelByType(hotelType) != null) {
			response = hotelService.getHotelListByType(hotelType);
		} else {
			response = hotelService.getAllHotels();
		}
		if (response == null)
			throw new AboutstaysException(AboutstaysResponseCode.GET_HOTEL_FAILED);

		return new BaseApiResponse<GetHotelListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.HOTEL_BY_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHotelResponse> getHotelById(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_ID, required = true) String hotelId)
			throws AboutstaysException {
		ValidateUtils.validateHotelId(hotelId);
		GetHotelResponse response = hotelService.getHotelById(hotelId);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_HOTEL_FAILED);
		}
		return new BaseApiResponse<GetHotelResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.GENERAL_INFO_BY_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHotelGeneralInfoResponse> getHotelGeneralInfoById(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_ID, required = true) String hotelId)
			throws AboutstaysException {
		ValidateUtils.validateHotelId(hotelId);
		GetHotelGeneralInfoResponse response = hotelService.getGeneralInfoById(hotelId);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_HOTEL_FAILED);
		}
		return new BaseApiResponse<GetHotelGeneralInfoResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.UPDATE_HOTEL_BY_TYPE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateByHotelId(@RequestBody(required = true) UpdateHotelRequest updateHotelRequest)
			throws AboutstaysException {
		HotelUpdateBy typeEnum = HotelUpdateBy.findByValue(updateHotelRequest.getType());
		ValidateUtils.validateHotelUpdateTypeValue(typeEnum, updateHotelRequest.getValue());
		switch (typeEnum) {
		case HOTELID:
			if (!hotelService.hotelExistsById(updateHotelRequest.getValue())) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_ID);
			}
			break;
		}
		Boolean response = hotelService.updateHotelByType(typeEnum, updateHotelRequest.getValue(), updateHotelRequest);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HOTEL_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.UPDATE_GENERAL_INFO, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateHotelGeneralInfo(
			@RequestBody(required = true) UpdateHotelRequest updateHotelRequest) throws AboutstaysException {
		ValidateUtils.validateHotelGeneralInfoUpdate(updateHotelRequest);
		HotelUpdateBy typeEnum = HotelUpdateBy.findByValue(updateHotelRequest.getType());
		ValidateUtils.validateHotelUpdateTypeValue(typeEnum, updateHotelRequest.getValue());
		switch (typeEnum) {
		case HOTELID:
			if (hotelService.hotelExistsById(updateHotelRequest.getValue()) == false) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_ID);
			}
			break;
		}
		updateHotelRequest.setHotelType(null);
		updateHotelRequest.setReviewsAndRatings(null);
		updateHotelRequest.setRoomsIds(null);
		Boolean response = hotelService.updateHotelByType(typeEnum, updateHotelRequest.getValue(), updateHotelRequest);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HOTEL_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.DELETE_HOTEL, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteById(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_ID, required = true) String hotelId)
			throws AboutstaysException {
		if (StringUtils.isEmpty(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_ID);
		}
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_ID);
		}
		Boolean response = hotelService.deleteHotelById(hotelId);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.DELETE_HOTEL_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.GET_ROOMS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHotelRoomsResponse> getAllRoomsOfHotel(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_ID) String hotelId)
			throws AboutstaysException {
		GetHotelResponse getHotelResponse = hotelService.getHotelById(hotelId);
		if (getHotelResponse == null)
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if (CollectionUtils.isEmpty(getHotelResponse.getRoomsIds()))
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_REGISTERED);
		List<GetRoomCategoryResponse> roomsList = roomService.getAllByRoomCategoryIds(getHotelResponse.getRoomsIds());
		if (CollectionUtils.isEmpty(roomsList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_REGISTERED);
		GetHotelRoomsResponse response = new GetHotelRoomsResponse();
		response.setHotelId(hotelId);
		response.setRoomsList(roomsList);
		return new BaseApiResponse<GetHotelRoomsResponse>(false, response, AboutstaysResponseCode.SUCCESS);

	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.CITY_AND_NAME, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHotelBasicInfoList> getHotelsByCityAndName(
			@RequestBody(required = true) GetHotelsByCityAndNamesRequest request) throws AboutstaysException {
		ValidateUtils.validateCityAndNameRequest(request);
		GetHotelBasicInfoList response = hotelService.getHotelByCityAndName(request.getCity(), request.getName());
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_HOTEL_FAILED);
		}
		return new BaseApiResponse<GetHotelBasicInfoList>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.UPDATE_PARTNERSHIP_TYPE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updatePartnership(@RequestBody(required = true) UpdatePartnershipRequest request)
			throws AboutstaysException {
		ValidateUtils.validateUpdatePartnership(request);
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_ID);
		}
		Boolean response = hotelService.updatePartnershipType(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.PARTNERSHIP_UPDATION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.GET_GENERAL_SERVICES, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHotelGeneralServices> getHotelGS(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_ID, required = true) String hotelId,
			@RequestParam(value = RestMappingConstants.HotelParameters.STAY_STATUS, required = true) Integer stayStatus,
			@RequestParam(value = RestMappingConstants.HotelParameters.IS_GI, required = false) Boolean isGI)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		StaysStatus statusEnum = StaysStatus.findStatusByCode(stayStatus);
		if (statusEnum == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_STAY_STATUS);
		}
		GetHotelGeneralServices response = hotelService.getGeneralServices(hotelId, statusEnum, isGI);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_HOTEL_GENERAL_SERVICES_FAILED);
		}
		return new BaseApiResponse<GetHotelGeneralServices>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.UPDATE_GENERAL_SERVICES, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateServiceDetails(@RequestBody(required = true) UpdateHotelServices request)
			throws AboutstaysException {
		ValidateUtils.validateUpdateHotelServicesRequest(request);
		StaysStatus statusEnum = StaysStatus.findStatusByCode(request.getStatus());
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}

		if (CollectionUtils.isNotEmpty(request.getListGSIds())) {
			for (String gsId : request.getListGSIds()) {
				if (!generalServicesService.existsById(gsId)) {
					throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
				}
			}
		}
		// if(CollectionUtils.isMapNotEmpty(request.getMapGSIdVsHSId())){
		// Map<String,String> mapGSIdVsHSId = request.getMapGSIdVsHSId();
		// for(Map.Entry<String, String> gsIdVsHSid : mapGSIdVsHSId.entrySet()){
		// GetGeneralServiceResponse gsResponse =
		// generalServicesService.getById(gsIdVsHSid.getKey());
		// if(gsResponse == null){
		// throw new
		// AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		// }
		// Integer gsService = gsResponse.getServiceType();
		// if(gsService == null){
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_TYPE);
		// }
		// GeneralServiceType gsType =
		// GeneralServiceType.findGeneralServiceByType(gsService);
		// GeneralServiceType.validate(gsType, gsIdVsHSid.getValue(),
		// beanFactory);
		// }
		// }
		Boolean response = hotelService.updateServiceDetails(request, statusEnum);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HOTEL_SERVICE_DETAILS_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.LINK_AMENITIES, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAllAmenities(@RequestBody(required = true) AddAmenitiesToHotelRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkAmenityToHotelRequest(request);
		Set<String> amenitySet = new HashSet<>(request.getAmenityIds());
		request.setAmenityIds(new ArrayList<>(amenitySet));
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		validateIdsService.validateAmenitiesList(request.getAmenityIds(), AmenityType.HOTEL, AboutstaysResponseCode.AMENITY_NOT_FOUND);
		hotelService.updateAmenities(request.getHotelId(), request.getAmenityIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AMENITY_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.LINK_ESSENTIALS, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAllEssentials(@RequestBody(required = true) AddEssentialToHotelRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkAssentialsToHotelRequest(request);
		Set<String> essentialsSet = new HashSet<>(request.getEssentialsIds());
		request.setEssentialsIds(new ArrayList<>(essentialsSet));
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		validateIdsService.validateAmenitiesList(request.getEssentialsIds(), AmenityType.ESSENTIALS, AboutstaysResponseCode.ESSENTIAL_NOT_FOUND);
		hotelService.updateEssentials(request.getHotelId(), request.getEssentialsIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ESSENTIAL_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.LINK_AWARD, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addHotelAward(@RequestBody(required = true) AddAwardRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkAwardToHotelRequest(request);
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		String awardId = request.getAwardData().getAwardId();
		if(!masterAmenityService.existsByIdAndType(awardId, AmenityType.AWARDS))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.AWARD_NOT_FOUND, awardId));
		hotelService.updateAwards(request.getHotelId(), request.getAwardData());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AWARD_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.LINK_CARD_ACCEPTED, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAllCardAccepted(@RequestBody(required = true) AddCardAcceptedRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkCardAcceptedToHotelRequest(request);
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		validateIdsService.validateAmenitiesList(request.getCardIds(), AmenityType.CARD_ACCEPTED, AboutstaysResponseCode.CARD_NOT_FOUND);
		hotelService.updateCardAccepted(request.getHotelId(), request.getCardIds());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CARD_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.DELETE_AWARD, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteAward(@RequestBody(required = true) AddAwardRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkAwardToHotelRequest(request);
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		boolean response = hotelService.deleteAwards(request.getHotelId(), request.getAwardData());
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.LINK_LOYALTY_PROGRAM, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> linkLoyaltyProgram(@RequestBody(required = true) LinkLoyaltyProgramsRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkLoyaltyProgramToHotelRequest(request);
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if (!loyaltyProgramService.existsById(request.getLoyaltyProgramsId())) {
			throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_PROGRAM_NOT_FOUND);
		}
		hotelService.updateLoyaltyPrograms(request.getHotelId(), request.getLoyaltyProgramsId());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.LOYALTY_PROGRAMS_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.DELETE_LOYALTY_PROGRAMS, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteLoyaltyProgram(@RequestBody(required = true) DeleteLoyaltyRequest request)
			throws AboutstaysException {
		ValidateUtils.validateLinkLoyaltyProgramToHotelRequest(request);
		if (!hotelService.hotelExistsById(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		Boolean response = hotelService.deleteLoyaltyProgram(request.getHotelId(), request.getLoyaltyProgramsId());
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HotelConstants.GET_HOTEL_META_DATA, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<HotelMetaDataResponce> getHotelMetaData(
			@RequestParam(value = RestMappingConstants.HotelParameters.HOTEL_ID, required = true) String hotelId)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		HotelMetaDataResponce responce = new HotelMetaDataResponce();
		responce.setHotelInfoMetaData(hotelService.getHotelInfoMetaData(hotelId));
		
		return new BaseApiResponse<HotelMetaDataResponce>(false, responce, AboutstaysResponseCode.SUCCESS);
	}

}
