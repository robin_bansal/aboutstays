package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddLaundryOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.UpdateLaundryOrderRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetLaundryOrderResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.LaundryOrderService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.LaundryOrderConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.BASE)
public class LaundryOrderController extends BaseSessionController{
	
	@Autowired
	private LaundryOrderService laundryOrderService;
	
	@Autowired
	private HotelService hotelService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private StaysService staysService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddLaundryOrderRequest> getSample(){
		AddLaundryOrderRequest response = LaundryOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddLaundryOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	public BaseApiResponse<String> add(@RequestBody(required=true)AddLaundryOrderRequest request, boolean skipValidation) throws AboutstaysException{
		if(!skipValidation){
			ValidateUtils.validateAddLaundryOrder(request);
			baseSessionService.validateBaseSessionSessionData(request);
		}
		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId()); 
		if(request.isRequestedNow()){
			request.setDeliveryTime(new Date().getTime()+timezoneDiff);
		} else {
			request.setDeliveryTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())+timezoneDiff);
		}
		String response = laundryOrderService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_LAUNDRY_ORDER_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestByLaundryOrderRequest(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_LAUNDRY_ORDER_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddLaundryOrderRequest request) throws AboutstaysException{
		return add(request, false);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetLaundryOrderResponse>> getAll() throws AboutstaysException{
		List<GetLaundryOrderResponse> response = laundryOrderService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_LAUNDRY_ORDERS_FAILED);
		}
		return new BaseApiResponse<List<GetLaundryOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetLaundryOrderResponse> get(@RequestParam(value=RestMappingConstants.LaundryOrderParameters.ID, required=true)String id) throws AboutstaysException{
		GetLaundryOrderResponse response = laundryOrderService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ORDER_NOT_EXISTS);
		}
		return new BaseApiResponse<GetLaundryOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.LaundryOrderParameters.ID, required=true)String id) throws AboutstaysException{
		if(!laundryOrderService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ORDER_NOT_EXISTS);
		}
		Boolean response = laundryOrderService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateLaundryOrderRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateLaundryOrderRequest(request);
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FOOD_ORDER_ID);
		}
		if(!laundryOrderService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ORDER_NOT_EXISTS);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		Boolean response = laundryOrderService.updateNonNullFieldsOfRequest(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_LAUNDRY_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.GET_ALL_BY_USER_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetLaundryOrderResponse>> getAllByUserId(@RequestParam(value=RestMappingConstants.LaundryOrderParameters.USER_ID, required=true)String userId) throws AboutstaysException{
		
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<GetLaundryOrderResponse> listResponse = laundryOrderService.getAllByUserId(userId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_LAUNDRY_ORDER_OF_USER_FAILED);
		}
		return new BaseApiResponse<List<GetLaundryOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.GET_ALL_BY_HOTEL_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetLaundryOrderResponse>> getAllByHotelId(@RequestParam(value=RestMappingConstants.LaundryOrderParameters.HOTEL_ID, required=true)String hotelId) throws AboutstaysException{
		if(!hotelService.hotelExistsById(hotelId)){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<GetLaundryOrderResponse> listResponse = laundryOrderService.getAllByHotelId(hotelId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_LAUNDRY_ORDERS_OF_HOTEL_FAILED);
		}
		return new BaseApiResponse<List<GetLaundryOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryOrderConstants.GET_ALL_BY_USER_ID_AND_STAY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetLaundryOrderResponse>> getAllByUserIdAndStayId(@RequestParam(value=RestMappingConstants.LaundryOrderParameters.USER_ID, required=true)String userId,
			@RequestParam(value=RestMappingConstants.LaundryOrderParameters.STAY_ID, required=true)String stayId) throws AboutstaysException{
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if(!staysService.existsByStaysId(stayId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		List<GetLaundryOrderResponse> listResponse = laundryOrderService.getAllByUserAndStayId(userId, stayId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_LAUNDRY_OF_USER_STAYS_FAILED);
		}
		return new BaseApiResponse<List<GetLaundryOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
}
