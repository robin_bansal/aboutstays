package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CarType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.CarDetails;
import com.aboutstays.pojos.CarDetailsAndPrice;
import com.aboutstays.pojos.SupportedAirports;
import com.aboutstays.request.dto.AddAirportServiceRequest;
import com.aboutstays.request.dto.UpdateAirportServiceRequest;
import com.aboutstays.response.dto.AddAirportServiceResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetAirportServiceResponse;
import com.aboutstays.services.AirportServicesService;
import com.aboutstays.services.HotelService;
import com.aboutstays.utils.AirportServiceConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.AirportServiceConstants.BASE)
public class AirportServiceController {
	
	@Autowired
	private AirportServicesService airportServicesService;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.AirportServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddAirportServiceRequest> getSample(){
		AddAirportServiceRequest response = AirportServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddAirportServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddAirportServiceResponse> add(@RequestBody(required=true)AddAirportServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddAirportServiceRequest(request);
		if(!hotelService.hotelExistsById(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		List<SupportedAirports> listSupportedAirports = request.getSupportedAirportList();
		for(SupportedAirports airports : listSupportedAirports){
			Iterator<CarDetailsAndPrice> iterator = airports.getCarDetailsAndPriceList().iterator();
			Set<Integer> carTypeSet = new HashSet<>();
			while(iterator.hasNext()){
				CarDetailsAndPrice carDetailsAndPrice = iterator.next();
				if(carDetailsAndPrice != null && carDetailsAndPrice.getCarDetails() != null){
					CarDetails carDetails = carDetailsAndPrice.getCarDetails();
					CarType carType = CarType.findValueByCode(carDetails.getType());
					if(carType == null){
						throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
					} else if(carTypeSet.contains(carDetails.getType())){
						iterator.remove();
					} else{
						carTypeSet.add(carDetails.getType());
					}
					carDetails.setName(carType.getMessage());
					carDetails.setImageUrl(carType.getImageUrl());
				}
			}
		}
			
		AddAirportServiceResponse response = airportServicesService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADDING_AIRPORT_SERVICE_FAILED);
		}
		return new BaseApiResponse<AddAirportServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetAirportServiceResponse>> getAll() throws AboutstaysException{
		List<GetAirportServiceResponse> response = airportServicesService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_AIRPORT_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetAirportServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.AirportServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!airportServicesService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_SERVICE_NOT_EXISTS);
		}
		Boolean response = airportServicesService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.AS_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetAirportServiceResponse> get(@RequestParam(value=RestMappingConstants.AirportServiceParameters.ID, required=true)String id) throws AboutstaysException{
		GetAirportServiceResponse response = airportServicesService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetAirportServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateAirportServiceRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_SERVICE_ID);
		}
		if(!airportServicesService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_SERVICE_NOT_EXISTS);
		}
		Boolean response = airportServicesService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_AIRPORT_SERVICE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

}
