package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddMasterChannelRequest;
import com.aboutstays.request.dto.AddMultipleMasterChannelRequest;
import com.aboutstays.request.dto.DeleteMasterChannelRequest;
import com.aboutstays.request.dto.UpdateMasterChannelRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetMasterChannelListResponse;
import com.aboutstays.response.dto.GetMasterChannelResponse;
import com.aboutstays.services.MasterChannelService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.ChannelConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.MasterChannelConstants.BASE)
public class MasterChannelController {

	@Autowired
	private MasterChannelService masterChannelService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMasterChannelRequest> getSampleRequest(){
		AddMasterChannelRequest request = ChannelConverter.getSampleRequest();
		return new BaseApiResponse<AddMasterChannelRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addMasterChannel(@RequestBody(required=true)AddMasterChannelRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMasterChannelRequest(request);
		validateIdsService.validateMasterGenereId(request.getGenereId());
		if(masterChannelService.existsByName(request.getName(), null)){
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.CHANNEL_ALREADY_EXISTS, request.getName()));
		}
		String id = masterChannelService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.CHANNEL_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.ADD_ALL, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAllMasterChannels(@RequestBody(required=true)AddMultipleMasterChannelRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMultipleMasterChannelRequest(request);
		for(AddMasterChannelRequest addMasterChannelRequest : request.getChannels()){
			validateIdsService.validateMasterGenereId(addMasterChannelRequest.getGenereId());
			if(masterChannelService.existsByName(addMasterChannelRequest.getName(), null)){
				throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.CHANNEL_ALREADY_EXISTS, addMasterChannelRequest.getName()));
			}
		}
		masterChannelService.saveAll(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ALL_CHANNEL_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateMasterChannelRequest(@RequestBody(required=true)UpdateMasterChannelRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateMasterChannelRequest(request);
		if(!masterChannelService.existsById(request.getChannelId()))
			throw new AboutstaysException(AboutstaysResponseCode.CHANNEL_NOT_FOUND);
		if(masterChannelService.existsByName(request.getName(), request.getChannelId())){
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.CHANNEL_ALREADY_EXISTS, request.getName()));
		}
		validateIdsService.validateMasterGenereId(request.getGenereId());
		masterChannelService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CHANNEL_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.DELETE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteMasterChannel(@RequestBody(required=true)DeleteMasterChannelRequest request) throws AboutstaysException{
		if(!masterChannelService.existsById(request.getChannelId()))
			throw new AboutstaysException(AboutstaysResponseCode.CHANNEL_NOT_FOUND);
		masterChannelService.delete(request.getChannelId());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CHANNEL_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMasterChannelResponse> getMasterChannel(@RequestParam(value=RestMappingConstants.MasterChannelParameters.ID)String id) throws AboutstaysException{
		GetMasterChannelResponse response = masterChannelService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.CHANNEL_NOT_FOUND);
		return new BaseApiResponse<GetMasterChannelResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMasterChannelListResponse> getAllMasterChannels() throws AboutstaysException{
		List<GetMasterChannelResponse> channels = masterChannelService.getAll();
		if(CollectionUtils.isEmpty(channels))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CHANNEL_FOUND);
		GetMasterChannelListResponse response = new GetMasterChannelListResponse();
		response.setChannels(channels);
		return new BaseApiResponse<GetMasterChannelListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterChannelConstants.GET_ALL_BY_GENERE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMasterChannelListResponse> getAllMasterChannelsByGenere(@RequestParam(value=RestMappingConstants.MasterChannelParameters.GENERE_ID)String genereId) throws AboutstaysException{
		validateIdsService.validateMasterGenereId(genereId);
		List<GetMasterChannelResponse> channels = masterChannelService.getAllByGenere(genereId);
		if(CollectionUtils.isEmpty(channels))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CHANNEL_FOUND);
		GetMasterChannelListResponse response = new GetMasterChannelListResponse();
		response.setChannels(channels);
		return new BaseApiResponse<GetMasterChannelListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
