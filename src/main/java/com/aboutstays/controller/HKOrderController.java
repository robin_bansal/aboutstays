package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddHKOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.UpdateHKOrderRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetHKOrderResponse;
import com.aboutstays.services.HKOrderService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.HKOrderConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.BASE)
public class HKOrderController extends BaseSessionController {

	@Autowired
	private HKOrderService hkOrderService;

	@Autowired
	private UserService userService;

	@Autowired
	private HotelService hotelService;

	@Autowired
	private StaysService stayService;

	@Autowired
	private AutowireCapableBeanFactory beanFactory;

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddHKOrderRequest> getSample() {
		AddHKOrderRequest response = HKOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddHKOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	public BaseApiResponse<String> add(@RequestBody(required = true) AddHKOrderRequest request, boolean skipValidations)
			throws AboutstaysException {
		if (!skipValidations) {
			ValidateUtils.validateAddHkOrderRequest(request);
			baseSessionService.validateBaseSessionSessionData(request);
		}

		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId());
		if (request.isRequestedNow()) {
			request.setDeliveryTime(new Date().getTime() + timezoneDiff);
		} else {
			request.setDeliveryTime(
					DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())
							+ timezoneDiff);
		}
		String response = hkOrderService.save(request);
		if (StringUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.ADD_HOUSEKEEPING_ORDER_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestByHKOrderRequest(request,
				response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if (uosResponse == null) {
			throw new AboutstaysException(AboutstaysResponseCode.ADD_LAUNDRY_ORDER_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required = true) AddHKOrderRequest request)
			throws AboutstaysException {
		return add(request, false);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetHKOrderResponse>> getAll() throws AboutstaysException {
		List<GetHKOrderResponse> response = hkOrderService.getAll();
		if (CollectionUtils.isEmpty(response)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_HK_ORDERS_FAILED);
		}
		return new BaseApiResponse<List<GetHKOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.GET, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHKOrderResponse> get(
			@RequestParam(value = RestMappingConstants.HousekeepingOrderParameters.ID, required = true) String id)
			throws AboutstaysException {
		GetHKOrderResponse response = hkOrderService.getById(id);
		if (response == null) {
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ORDER_NOT_EXISTS);
		}
		return new BaseApiResponse<GetHKOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.DELETE, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(
			@RequestParam(value = RestMappingConstants.HousekeepingOrderParameters.ID, required = true) String id)
			throws AboutstaysException {
		if (!hkOrderService.existsById(id)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ORDER_NOT_EXISTS);
		}
		Boolean response = hkOrderService.delete(id);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.UPDATE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required = true) UpdateHKOrderRequest request)
			throws AboutstaysException {
		ValidateUtils.validateUpdateHKOrderRequest(request);
		if (StringUtils.isEmpty(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FOOD_ORDER_ID);
		}
		if (!hkOrderService.existsById(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ORDER_NOT_EXISTS);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		Boolean response = hkOrderService.updateNonNullFieldsOfRequest(request);
		if (response == false) {
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HK_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.GET_ALL_BY_USER_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetHKOrderResponse>> getAllByUserId(
			@RequestParam(value = RestMappingConstants.HousekeepingOrderParameters.USER_ID, required = true) String userId)
			throws AboutstaysException {
		if (!userService.existsByUserId(userId)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<GetHKOrderResponse> listResponse = hkOrderService.getAllByUserId(userId);
		if (CollectionUtils.isEmpty(listResponse)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_HK_ORDER_OF_USER_FAILED);
		}
		return new BaseApiResponse<List<GetHKOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.GET_ALL_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetHKOrderResponse>> getAllByHotelId(
			@RequestParam(value = RestMappingConstants.HousekeepingOrderParameters.HOTEL_ID, required = true) String hotelId)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<GetHKOrderResponse> listResponse = hkOrderService.getAllByHotelId(hotelId);
		if (CollectionUtils.isEmpty(listResponse)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_HOUSEKEEPING_ORDERS_OF_HOTEL_FAILED);
		}
		return new BaseApiResponse<List<GetHKOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.HousekeepingOrderConstants.GET_ALL_BY_USER_ID_AND_STAY_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetHKOrderResponse>> getAllByUserIdAndStayId(
			@RequestParam(value = RestMappingConstants.HousekeepingOrderParameters.USER_ID, required = true) String userId,
			@RequestParam(value = RestMappingConstants.HousekeepingOrderParameters.STAY_ID, required = true) String stayId)
			throws AboutstaysException {
		if (!userService.existsByUserId(userId)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if (!stayService.existsByStaysId(stayId)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		List<GetHKOrderResponse> listResponse = hkOrderService.getAllByUserAndStayId(userId, stayId);
		if (CollectionUtils.isEmpty(listResponse)) {
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_HK_OF_USER_STAYS_FAILED);
		}
		return new BaseApiResponse<List<GetHKOrderResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}

}
