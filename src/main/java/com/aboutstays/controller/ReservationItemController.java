package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddReservationItemRequest;
import com.aboutstays.request.dto.BatchUpdateReservationItemRequest;
import com.aboutstays.request.dto.UpdateReservationItemRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.ReservationItemResponse;
import com.aboutstays.services.ReservationItemService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ReservationItemConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.ReservationItemConstants.BASE)
public class ReservationItemController {
	
	@Autowired
	private ReservationItemService rsItemService;
	
	@Autowired
	private ValidateIdsService validateIdService;
	
	@RequestMapping(value = RestMappingConstants.ReservationItemConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddReservationItemRequest> getSampleRequest() {
		AddReservationItemRequest request = ReservationItemConverter.getSampleRequest();
		return new BaseApiResponse<AddReservationItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.ReservationItemConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddReservationItemRequest request) throws AboutstaysException{
		ValidateUtils.validateReservationItemRequest(request);
		validateIdService.validateReservationCategoryId(request.getCategoryId(), request.getHotelId());
			
		String response = rsItemService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_RESERVATION_ITEM_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.ReservationItemConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<ReservationItemResponse>> getAll(@RequestParam(value=RestMappingConstants.ReservationItemParameters.HOTEL_ID, required=false)String hotelId) throws AboutstaysException {
		List<ReservationItemResponse> responseList = rsItemService.getAll();
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		return new BaseApiResponse<List<ReservationItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationItemConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<ReservationItemResponse> get(@RequestParam(value=RestMappingConstants.ReservationItemParameters.ID, required=true)String id) throws AboutstaysException{
		ReservationItemResponse response = rsItemService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		}
		return new BaseApiResponse<ReservationItemResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationItemConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.ReservationItemParameters.ID, required=true)String id) throws AboutstaysException{
		if(!rsItemService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		}
		Boolean response = rsItemService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_ITEM_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.ReservationItemConstants.UPDATE, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateReservationItemRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateReservationItemRequest(request);
		validateIdService.validateReservationItemId(request.getId(), request.getCategoryId(), request.getHotelId());
		
		rsItemService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.RS_ITEM_WITH_NAME_UPDATED, request.getName()));
	}
	
	@RequestMapping(value = RestMappingConstants.ReservationItemConstants.GET_ALL_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<ReservationItemResponse>> getAllByHotelId(@RequestParam(value=RestMappingConstants.ReservationItemParameters.HOTEL_ID)String hotelId,
			@RequestParam(value=RestMappingConstants.ReservationItemParameters.CATEGORY_ID, required=false)String categoryId) throws AboutstaysException {
		List<ReservationItemResponse> responseList = rsItemService.getAllByHotelId(hotelId, categoryId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		return new BaseApiResponse<List<ReservationItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	@RequestMapping(value = RestMappingConstants.ReservationItemConstants.BATCH_UPDATE, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> batchUpdate(@RequestBody BatchUpdateReservationItemRequest request){
		if(CollectionUtils.isNotEmpty(request.getItems())){
			List<BaseApiResponse<Boolean>> response = new ArrayList<>();
			boolean allUpdated = true;
			for(UpdateReservationItemRequest item : request.getItems()){
				BaseApiResponse<Boolean> updateItemResponse = null;
				try {
					updateItemResponse = update(item);
				} catch (AboutstaysException e) {
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, e.getCode(), false, e.getMessage());
				} catch (Exception e){
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
				}
				response.add(updateItemResponse);
			}
			if(!allUpdated){
				return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(true, response, AboutstaysResponseCode.SOME_RS_ITEMS_NOT_UPDATED);
			}
			return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, response, AboutstaysResponseCode.ALL_RS_ITEMS_UPDATED);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, null, AboutstaysResponseCode.SUCCESS);
	}
	
}
