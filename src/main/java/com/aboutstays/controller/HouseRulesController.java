package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddHouseRulesRequest;
import com.aboutstays.request.dto.UpdateHouseRulesRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetHouseRulesResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.HouseRulesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HouseRulesConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.HouseRulesConstants.BASE)
public class HouseRulesController {
	
	@Autowired
	private HouseRulesService hrService;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.HouseRulesConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddHouseRulesRequest> getSample(){
		AddHouseRulesRequest response = HouseRulesConverter.getSampleRequest();
		return new BaseApiResponse<AddHouseRulesRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HouseRulesConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddHouseRulesRequest request) throws AboutstaysException{
		ValidateUtils.validateAddHouseRuleRequest(request);
		if(!hotelService.hotelExistsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		String response = hrService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_HOUSERULES_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HouseRulesConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetHouseRulesResponse>> getAll() throws AboutstaysException{
		List<GetHouseRulesResponse> response = hrService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSE_RULES_ADD_YET);
		}
		return new BaseApiResponse<List<GetHouseRulesResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HouseRulesConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.HouseRulesParameters.ID, required=true)String id) throws AboutstaysException{
		if(!hrService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSERULES_FOUND);
		}
		Boolean response = hrService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSE_RULE_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HouseRulesConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHouseRulesResponse> get(@RequestParam(value=RestMappingConstants.HouseRulesParameters.ID, required=true)String id) throws AboutstaysException{
		GetHouseRulesResponse response = hrService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSERULES_FOUND);
		}
		return new BaseApiResponse<GetHouseRulesResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HouseRulesConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateHouseRulesRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOUSE_RULES_ID);
		}
		if(!hrService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSERULES_FOUND);
		}
		Boolean response = hrService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HOUSE_RULES_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
