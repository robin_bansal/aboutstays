package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddInternetOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.InternetOrderResponse;
import com.aboutstays.response.dto.UpdateInternetOrderRequest;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.InternetOrderService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.InternetOrderConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.InternetOrderConstants.BASE)
public class InternetOrderController extends BaseSessionController{
	
	@Autowired
	private InternetOrderService ioService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.InternetOrderConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddInternetOrderRequest> getSample(){
		AddInternetOrderRequest response = InternetOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddInternetOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}	

	
	public BaseApiResponse<String> add(@RequestBody(required=true)AddInternetOrderRequest request, boolean skipValidations) throws AboutstaysException{
		if(!skipValidations){
			ValidateUtils.validateInternetOrder(request);
			baseSessionService.validateBaseSessionSessionData(request);
		}
		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId()); 
		if(request.isRequestedNow()){
			request.setDeliveryTime(new Date().getTime()+timezoneDiff);
		} else {
			request.setDeliveryTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())+timezoneDiff);
		}
		String response = ioService.add(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_INTERNET_ORDER_REQUEST_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSReqByInternetOrder(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_INTERNET_ORDER_REQUEST_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetOrderConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddInternetOrderRequest request) throws AboutstaysException{
		return add(request, false);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetOrderConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<InternetOrderResponse>> getAll() throws AboutstaysException{
		List<InternetOrderResponse> response = ioService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<List<InternetOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetOrderConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<InternetOrderResponse> get(@RequestParam(value=RestMappingConstants.InternetOrderParameters.ID, required=true)String id) throws AboutstaysException{
		InternetOrderResponse response = ioService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<InternetOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetOrderConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.InternetOrderParameters.ID, required=true)String id) throws AboutstaysException{
		if(!ioService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_ORDER_NOT_FOUND);
		}
		Boolean response = ioService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.InternetOrderConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateInternetOrderRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_INTERNET_ORDER_ID);
		}
		if(!ioService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.INTERNET_ORDER_NOT_FOUND);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		Boolean response = ioService.updateNonNUll(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_INTERNET_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	
}
