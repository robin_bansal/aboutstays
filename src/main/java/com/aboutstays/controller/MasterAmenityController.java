package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddAllMasterAmenitiesRequest;
import com.aboutstays.request.dto.AddMasterAmenityRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetAllMasterAmenititesResponse;
import com.aboutstays.response.dto.GetMasterAmenityResponse;
import com.aboutstays.services.MasterAmenityService;
import com.aboutstays.utils.AmenityConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.MasterAmenityConstants.BASE)
public class MasterAmenityController {

	@Autowired
	private MasterAmenityService masterAmenityService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMasterAmenityRequest> getSampleRequest(){
		AddMasterAmenityRequest request = AmenityConverter.getSampleRequest();
		return new BaseApiResponse<AddMasterAmenityRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value="/requestAddAll", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddAllMasterAmenitiesRequest> getAddAllSampleRequest(){
		AddAllMasterAmenitiesRequest request = AmenityConverter.getAddAllSampleRequest();
		return new BaseApiResponse<AddAllMasterAmenitiesRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterAmenityConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addAmenity(@RequestBody(required=true)AddMasterAmenityRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMasterAmenityRequest(request);
		AmenityType amenityType = AmenityType.findbyType(request.getType());
		if(masterAmenityService.existsByName(request.getName(), amenityType)){
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.AMENITY_ALREADY_EXISTS, request.getName()));
		}
		String id = masterAmenityService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.AMENITY_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterAmenityConstants.ADD_ALL, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAllAmenities(@RequestBody(required=true)AddAllMasterAmenitiesRequest request) throws AboutstaysException{
		ValidateUtils.validateAddAllMasterAmenityRequest(request);
		for(AddMasterAmenityRequest addMasterAmenityRequest : request.getAmenities()){
			AmenityType amenityType = AmenityType.findbyType(addMasterAmenityRequest.getType());
			if(masterAmenityService.existsByName(addMasterAmenityRequest.getName(), amenityType)){
				throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.AMENITY_ALREADY_EXISTS, addMasterAmenityRequest.getName()));
			}
		}
		masterAmenityService.saveAll(request.getAmenities());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AMENITY_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterAmenityConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMasterAmenityResponse> getAmenityById(@RequestParam(value=RestMappingConstants.MasterAmenityParamaters.ID)String id) throws AboutstaysException{
		GetMasterAmenityResponse response = masterAmenityService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.AMENITY_NOT_FOUND, id));
		return new BaseApiResponse<GetMasterAmenityResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterAmenityConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetAllMasterAmenititesResponse> getAllAmenities(@RequestParam(required=false, value=RestMappingConstants.MasterAmenityParamaters.TYPE)Integer type) throws AboutstaysException{
		AmenityType amenityType=AmenityType.findbyType(type);
		List<GetMasterAmenityResponse> amenities=null;
		if(amenityType==null){
			amenities = masterAmenityService.getAll();			
		}else{
			amenities = masterAmenityService.getAll(amenityType);						
		}
		if(CollectionUtils.isEmpty(amenities)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_AMENITIES_FOUND);
		}
		GetAllMasterAmenititesResponse response = new GetAllMasterAmenititesResponse();
		response.setAmenities(amenities);
		return new BaseApiResponse<GetAllMasterAmenititesResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterAmenityConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteAmenity(@RequestParam(value=RestMappingConstants.MasterAmenityParamaters.ID)String id) throws AboutstaysException{
		if(!masterAmenityService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.AMENITY_NOT_FOUND, id));
		masterAmenityService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.AMENITY_DELETED_SUCCESSFULLY);
	}
}
