package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.HousekeepingItemsByType;
import com.aboutstays.request.dto.AddHousekeepingServiceRequest;
import com.aboutstays.response.dto.AddHousekeepingServiceResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetHousekeepingServiceResponse;
import com.aboutstays.response.dto.HousekeepingItemResponse;
import com.aboutstays.response.dto.UpdateHousekeepingServiceRequest;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.HousekeepingItemService;
import com.aboutstays.services.HousekeepingServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HousekeepingServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.BASE)
public class HousekeepingServiceController {
	
	@Autowired
	private HousekeepingServicesService hkServicesService;
	
	@Autowired
	private HousekeepingItemService hkItemService;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddHousekeepingServiceRequest> getSample(){
		AddHousekeepingServiceRequest response = HousekeepingServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddHousekeepingServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddHousekeepingServiceResponse> add(@RequestBody(required=true)AddHousekeepingServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddHousekeepingServiceRequest(request);
		if(!hotelService.hotelExistsById(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if(CollectionUtils.isNotEmpty(request.getListHKItems())){
			Set<Integer> setHKType = new HashSet<>();
			Iterator<HousekeepingItemsByType> iterator = request.getListHKItems().iterator();
			while(iterator.hasNext()){
				HousekeepingItemsByType items = iterator.next();
				
				HousekeepingItemType housekeepingItemType = HousekeepingItemType.getByCode(items.getType()); 
				if(housekeepingItemType==null){
					throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_HOUSEKEEPING_ITEM_TYPE);
				} else{
					if(setHKType.contains(items.getType())){
						iterator.remove();
					} else{
						setHKType.add(items.getType());
					}
				}
				items.setTypeName(housekeepingItemType.getDisplayName());
				items.setImageUrl(housekeepingItemType.getImageUrl());
			}
		}
		AddHousekeepingServiceResponse response = hkServicesService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_HOUSEKEEPING_SERVICE_FAILED);
		}
		return new BaseApiResponse<AddHousekeepingServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetHousekeepingServiceResponse>> getAll() throws AboutstaysException{
		List<GetHousekeepingServiceResponse> response = hkServicesService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_HOUSEKEEPING_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetHousekeepingServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetHousekeepingServiceResponse> get(@RequestParam(value=RestMappingConstants.HousekeepingServiceParameters.ID, required=true)String id) throws AboutstaysException{
		GetHousekeepingServiceResponse response = hkServicesService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetHousekeepingServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.HousekeepingServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!hkServicesService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		}
		Boolean response = hkServicesService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateHousekeepingServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddHousekeepingServiceRequest(request);
		if(!hkServicesService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		}
		if(CollectionUtils.isNotEmpty(request.getListHKItems())){
			Set<Integer> setHKType = new HashSet<>();
			Iterator<HousekeepingItemsByType> iterator = request.getListHKItems().iterator();
			while(iterator.hasNext()){
				HousekeepingItemsByType items = iterator.next();
				if(HousekeepingItemType.getByCode(items.getType())==null){
					throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_HOUSEKEEPING_ITEM_TYPE);
				} else{
					if(setHKType.contains(items.getType())){
						iterator.remove();
					} else{
						setHKType.add(items.getType());
					}
				}
			}
		}
		Boolean response = hkServicesService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_HOUSEKEEPING_SERVICE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingServiceConstants.SEARCH_HK_ITEM, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<HousekeepingItemResponse>> searchHKItems(@RequestParam(value=RestMappingConstants.HousekeepingServiceParameters.ID)String id,@RequestParam(value=RestMappingConstants.HousekeepingServiceParameters.QUERY)String query) throws AboutstaysException{
		if(!hkServicesService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		if(StringUtils.isEmpty(query))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SEARCH_QUERY);
		List<HousekeepingItemResponse> responseList = hkItemService.searchByQueryString(id, query);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		return new BaseApiResponse<List<HousekeepingItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

}
