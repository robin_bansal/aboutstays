package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.GetUOSByUserRequest;
import com.aboutstays.request.dto.UpdateUserOptedServiceRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetUserOptedServiceResponse;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.BASE)
public class UserOptedServiceController extends BaseSessionController{
	@Autowired
	private UserOptedServicesService userOptedServicesService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private StaysService staysService;
	
	@Autowired
	private GeneralServicesService generalServicesService;
	
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddUserOptedServiceRequest> getSample(){
		AddUserOptedServiceRequest response = UserOptedServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddUserOptedServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddUserOptedServiceResposne> add(@RequestBody(required=true)AddUserOptedServiceRequest request) throws AboutstaysException{
		logger.info(request.toString());
		ValidateUtils.validateAddUserOptedServiceRequest(request);
		baseSessionService.validateBaseSessionSessionData(request);
		Integer typeValue = request.getRefCollectionType();
		if(typeValue != null){
			if(RefCollectionType.findValueByType(typeValue) == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_REF_COLLECTION_TYPE);
			}
		} else{
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_REF_COLLECTION_TYPE);
		}
		AddUserOptedServiceResposne response = userOptedServicesService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_USER_OPTED_SERVICE_FAILED);
		}
		return new BaseApiResponse<AddUserOptedServiceResposne>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetUserOptedServiceResponse>> getAll(@RequestParam(value=RestMappingConstants.UserOptedServiceParameters.USER_ID, required=false)String userId) throws AboutstaysException{
		if(!StringUtils.isEmpty(userId) && !userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<GetUserOptedServiceResponse> response = userOptedServicesService.getAll(userId);
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_USER_OPTED_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetUserOptedServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetUserOptedServiceResponse> get(@RequestParam(value=RestMappingConstants.AirportPickupParameters.ID, required=true)String id) throws AboutstaysException{
		GetUserOptedServiceResponse response = userOptedServicesService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetUserOptedServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateUserOptedServiceRequest request) throws AboutstaysException{
		logger.info(request.toString());
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_OPTED_SERVICE_ID);
		}
		if(!userOptedServicesService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		if(!userService.existsByUserId(request.getUserId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if(!staysService.existsByStaysId(request.getStaysId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		if(!hotelService.hotelExistsById(request.getHotelId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if(!generalServicesService.existsById(request.getGsId())){
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		}
		Boolean response = userOptedServicesService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_USER_OPTED_SERVICE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.UserOptedServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!userOptedServicesService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		Boolean response = userOptedServicesService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.GET_BY_USER_AND_STAY, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetUserOptedServiceResponse>> getUserOptedServices(@RequestBody(required=true)GetUOSByUserRequest request) throws AboutstaysException{
		logger.info(request.toString());
		ValidateUtils.validateGetUOS(request);
		if(!userService.existsByUserId(request.getUserId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if(!staysService.existsByStaysId(request.getStayId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		List<GetUserOptedServiceResponse> listResponse = userOptedServicesService.getByUserAndStay(request);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_USER_OPTED_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetUserOptedServiceResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserOptedServiceConstants.GET_BY_USER_STAYS_HOTEL, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetUserOptedServiceResponse>> getByUserStaysHotel(@RequestBody(required=true)UserStayHotelIdsPojo request) throws AboutstaysException{
		validateIdsService.validateUserHotelStaysIds(request);
		List<GetUserOptedServiceResponse> listResponse = userOptedServicesService.getByUserStaysHotel(request);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_USER_OPTED_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetUserOptedServiceResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	

}
