package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.PickListType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddPickListRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetPickListResponse;
import com.aboutstays.services.PickListService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.PickListConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.PickListConstants.BASE)
public class PickListController {

	@Autowired
	private PickListService pickListService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddPickListRequest> getSampleRequest(){
		AddPickListRequest request = PickListConverter.getSampleRequest();
		return new BaseApiResponse<AddPickListRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.PickListConstants.ADD_MODIFY, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addOrModify(@RequestBody(required=true)AddPickListRequest request) throws AboutstaysException{
		ValidateUtils.validateAddPickListRequest(request);
		pickListService.addOrModify(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.PL_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.PickListConstants.DELETE_BY_TYPE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteByType(@RequestParam(value=RestMappingConstants.PickListParameters.TYPE)Integer type){
		pickListService.delete(type);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.PL_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.PickListConstants.GET_BY_TYPE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetPickListResponse> getByType(@RequestParam(value=RestMappingConstants.PickListParameters.TYPE)Integer type) throws AboutstaysException{
		GetPickListResponse response = pickListService.getByType(PickListType.findByCode(type));
		if(response==null){
			throw new AboutstaysException(AboutstaysResponseCode.PICKLIST_NOT_FOUND);
		}
		return new BaseApiResponse<GetPickListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.PickListConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetPickListResponse>> getAll() throws AboutstaysException{
		List<GetPickListResponse> response = pickListService.getAll();
		if(CollectionUtils.isEmpty(response))
			throw new AboutstaysException(AboutstaysResponseCode.NO_PL_FOUND);
		return new BaseApiResponse<List<GetPickListResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
}
