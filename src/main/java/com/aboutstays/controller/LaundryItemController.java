package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.BatchUpdateLaundryItemRequest;
import com.aboutstays.request.dto.LaundryItemRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.LaundryItemResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.LaundryItemService;
import com.aboutstays.services.LaundryServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.LaundryItemConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.LaundryItemConstants.BASE)
public class LaundryItemController {

	@Autowired
	private LaundryServicesService laundryServicesService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private LaundryItemService laundryItemService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LaundryItemRequest> getSampleRequest(){
		LaundryItemRequest request = LaundryItemConverter.getSampleRequest();
		return new BaseApiResponse<LaundryItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addLaundryItem(@RequestBody(required=true)LaundryItemRequest request) throws AboutstaysException{
		ValidateUtils.validateAddLaundryItemRequest(request);
		if(!laundryServicesService.existsById(request.getLaundryServicesId()))
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_SERVICE_NOT_FOUND);
		if(!hotelService.hotelExistsById(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		String id = laundryItemService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.LAUNDRY_ITEM_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LaundryItemResponse> getById(@RequestParam(value=RestMappingConstants.LaundryItemParameters.ID)String id) throws AboutstaysException{
		LaundryItemResponse response = laundryItemService.getById(id);
		if(response==null){
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ITEM_NOT_FOUND);
		}
		return new BaseApiResponse<LaundryItemResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<LaundryItemResponse>> getAll() throws AboutstaysException{
		List<LaundryItemResponse> responseList = laundryItemService.getAll();
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_LAUNDRY_ITEM_FOUND);
		return new BaseApiResponse<List<LaundryItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.GET_ALL_BY_HOTEL_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<LaundryItemResponse>> getAllByHotelId(@RequestParam(value=RestMappingConstants.LaundryItemParameters.HOTEL_ID)String hotelId) throws AboutstaysException{
		if(!hotelService.hotelExistsById(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		List<LaundryItemResponse> responseList = laundryItemService.getAllByHotelId(hotelId);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_LAUNDRY_ITEM_FOUND);
		return new BaseApiResponse<List<LaundryItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.GET_ALL_BY_LAUNDRY_SERVICE_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<LaundryItemResponse>> getAllByLaundryServicesId(@RequestParam(value=RestMappingConstants.LaundryItemParameters.LAUNDRY_SERVICES_ID)String laundryServicesId) throws AboutstaysException{
		if(!laundryServicesService.existsById(laundryServicesId))
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_SERVICE_NOT_FOUND);
		List<LaundryItemResponse> responseList = laundryItemService.getAllByServicesId(laundryServicesId);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_LAUNDRY_ITEM_FOUND);
		return new BaseApiResponse<List<LaundryItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.DELETE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteItem(@RequestParam(value=RestMappingConstants.LaundryItemParameters.ID)String id) throws AboutstaysException{
		if(!laundryItemService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ITEM_NOT_FOUND);
		laundryItemService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.LAUNDRY_ITEM_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.GET_BY_TYPE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<LaundryItemResponse>> fetchByType(@RequestParam(value=RestMappingConstants.LaundryItemParameters.LAUNDRY_SERVICES_ID)String laundryServicesId,
			@RequestParam(value=RestMappingConstants.LaundryItemParameters.LAUNDRY_TYPE)Integer laundryType) throws AboutstaysException{
		if(!laundryServicesService.existsById(laundryServicesId))
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_SERVICE_NOT_FOUND);
		LaundryItemType laundryItemType = LaundryItemType.findByCode(laundryType);
		if(laundryItemType==null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_LAUNDRY_TYPE);
		List<LaundryItemResponse> responseList = laundryItemService.getByType(laundryServicesId, laundryItemType);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_LAUNDRY_ITEM_FOUND);
		return new BaseApiResponse<List<LaundryItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.SEARCH, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<LaundryItemResponse>> searchByQueryString(@RequestParam(value=RestMappingConstants.LaundryItemParameters.LAUNDRY_SERVICES_ID)String laundryServicesId,
			@RequestParam(value=RestMappingConstants.LaundryItemParameters.QUERY)String query) throws AboutstaysException{
		if(!laundryServicesService.existsById(laundryServicesId))
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_SERVICE_NOT_FOUND);
		if(StringUtils.isEmpty(query))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SEARCH_QUERY);
		List<LaundryItemResponse> responseList = laundryItemService.searchLaundryItemsByQuery(laundryServicesId, query);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_LAUNDRY_ITEM_FOUND);
		return new BaseApiResponse<List<LaundryItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateLaundryItem(@RequestBody LaundryItemRequest request) throws AboutstaysException{
		if(!laundryItemService.existsById(request.getItemId()))
			throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ITEM_NOT_FOUND);
		laundryItemService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.LAUNDRY_ITEM_WITH_NAME_UPDATED, request.getName()));
	}
	
	@RequestMapping(value=RestMappingConstants.LaundryItemConstants.BATCH_UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> batchUpdate(@RequestBody BatchUpdateLaundryItemRequest request){
		if(CollectionUtils.isNotEmpty(request.getItems())){
			List<BaseApiResponse<Boolean>> response = new ArrayList<>();
			boolean allUpdated = true;
			for(LaundryItemRequest item : request.getItems()){
				BaseApiResponse<Boolean> updateItemResponse = null;
				try {
					updateItemResponse = updateLaundryItem(item);
				} catch (AboutstaysException e) {
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, e.getCode(), false, e.getMessage());
				} catch (Exception e){
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
				}
				response.add(updateItemResponse);
			}
			if(!allUpdated){
				return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(true, response, AboutstaysResponseCode.SOME_LAUNDRY_ITEMS_NOT_UPDATED);
			}
			return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, response, AboutstaysResponseCode.ALL_LAUNDRY_ITEMS_UPDATED);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, null, AboutstaysResponseCode.SUCCESS);
	}
}
