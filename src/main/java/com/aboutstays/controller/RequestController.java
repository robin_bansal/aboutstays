package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.AirportPickup;
import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.entities.Booking;
import com.aboutstays.entities.CheckinEntity;
import com.aboutstays.entities.FoodOrder;
import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.entities.InternetOrder;
import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.BookedFor;
import com.aboutstays.pojos.HotelRequestsPojo;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.GetRequestsResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetBookingReponse;
import com.aboutstays.response.dto.GetFoodOrderResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;
import com.aboutstays.response.dto.GetReservationCategoryResponse;
import com.aboutstays.response.dto.GetUserOptedServiceResponse;
import com.aboutstays.response.dto.ReservationItemResponse;
import com.aboutstays.response.dto.RoomServicesOrder;
import com.aboutstays.services.AirportPickupService;
import com.aboutstays.services.BellBoyOrderService;
import com.aboutstays.services.BookingService;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.CommuteOrderService;
import com.aboutstays.services.FoodOrdersService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HKOrderService;
import com.aboutstays.services.InternetOrderService;
import com.aboutstays.services.LateCheckoutRequestService;
import com.aboutstays.services.LaundryOrderService;
import com.aboutstays.services.MaintenanceRequestService;
import com.aboutstays.services.ReservationCategoryService;
import com.aboutstays.services.ReservationItemService;
import com.aboutstays.services.ReservationOrderService;
import com.aboutstays.services.RoomService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;

@Controller
@RequestMapping(value = RestMappingConstants.Request.BASE)
public class RequestController extends BaseSessionController {

	@Autowired
	private UserOptedServicesService userOptedServicesService;

	@Autowired
	private GeneralServicesService generalServicesService;

	@Autowired
	private FoodOrdersService foodOrdersService;

	@Autowired
	private LaundryOrderService laundryOrderService;

	@Autowired
	private HKOrderService hkOrderService;

	@Autowired
	private CommuteOrderService commuteOrderService;

	@Autowired
	private AirportPickupService apService;

	@Autowired
	private InternetOrderService ioService;

	@Autowired
	private LateCheckoutRequestService lcrService;

	@Autowired
	private ReservationOrderService rsService;

	@Autowired
	private CheckinService checkinService;

	@Autowired
	private MaintenanceRequestService mrService;

	@Autowired
	private BellBoyOrderService bellboyOrderService;

	@Autowired
	private ReservationItemService reservationItemService;

	@Autowired
	private ReservationCategoryService reservationCategoryService;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private StaysService staysService;

	@Autowired
	private RoomService roomService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = RestMappingConstants.Request.GET_ALL_REQUESTS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetRequestsResponse> getAllRequests(
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.HOTEL_ID, required = true) String hotelId,
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.DATE, required = true) String date)
			throws AboutstaysException {
		validateIdsService.validateHotelId(hotelId);
		try {
			DateUtil.parseDate(date, DateUtil.BASE_DATE_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_DATE_FORMAT, DateUtil.BASE_DATE_FORMAT));
		}
		Map<String, HotelRequestsPojo> gsIdVsRequestMap = new HashMap<>();
		List<GetUserOptedServiceResponse> getUserOptedServiceResponses = userOptedServicesService
				.getAllByHotelAndDate(hotelId, date);
		for (GetUserOptedServiceResponse optedServiceResponse : getUserOptedServiceResponses) {
			if (gsIdVsRequestMap.containsKey(optedServiceResponse.getGsId())) {
				setOrderCount(gsIdVsRequestMap.get(optedServiceResponse.getGsId()), optedServiceResponse);
			} else {
				HotelRequestsPojo hotelRequestsPojo = new HotelRequestsPojo();
				setOrderCount(hotelRequestsPojo, optedServiceResponse);
				gsIdVsRequestMap.put(optedServiceResponse.getGsId(), hotelRequestsPojo);
			}
		}
		GetRequestsResponse response = new GetRequestsResponse();
		List<HotelRequestsPojo> hotelRequestsPojos = new ArrayList<>();
		response.setRequestData(hotelRequestsPojos);
		for (Entry<String, HotelRequestsPojo> entry : gsIdVsRequestMap.entrySet()) {
			GetGeneralServiceResponse generalServiceResponse = generalServicesService.getById(entry.getKey());
			HotelRequestsPojo hotelRequestsPojo = entry.getValue();
			hotelRequestsPojo.setServiceName(generalServiceResponse.getName());
			hotelRequestsPojo.setIconUrl(generalServiceResponse.getActivatedImageUrl());
			hotelRequestsPojos.add(hotelRequestsPojo);
			response.updateCount(hotelRequestsPojo);
		}
		if (CollectionUtils.isEmpty(hotelRequestsPojos)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_REQUEST);
		}

		return new BaseApiResponse<GetRequestsResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	private void setOrderCount(HotelRequestsPojo hotelRequestsPojo, GetUserOptedServiceResponse optedServiceResponse) {
		RefCollectionType typeEnum = RefCollectionType.findValueByType(optedServiceResponse.getRefCollectionType());
		if (typeEnum == null) {
			return;
		}
		String refId = optedServiceResponse.getRefId();
		switch (typeEnum) {
		case FOOD: {
			FoodOrder foodResponse = foodOrdersService.getEntity(refId);
			if (foodResponse != null) {
				hotelRequestsPojo.updateStaus(foodResponse.getStatus());
			}
			break;
		}
		case AIRPORT_PICKUP:
			AirportPickup airportResponse = apService.getEntity(refId);
			if (airportResponse != null) {
				hotelRequestsPojo.updateStaus(airportResponse.getStatus());
			}
			break;
		case COMMUTE:
			CommuteOrder commuteResponse = commuteOrderService.getEntity(refId);
			if (commuteResponse != null) {
				hotelRequestsPojo.updateStaus(commuteResponse.getStatus());
			}
			break;
		case RESERVATION:
			ReservationOrder reservationResponse = rsService.getEntity(refId);
			if (reservationResponse != null) {
				hotelRequestsPojo.updateStaus(reservationResponse.getStatus());
			}
			break;
		case INTERNET:
			InternetOrder internetResponse = ioService.getEntity(refId);
			if (internetResponse != null) {
				hotelRequestsPojo.updateStaus(internetResponse.getStatus());
			}
			break;
		case MAINTENANCE:
			MaintenanceRequestEntity maintenanceResponse = mrService.getEntity(refId);
			if (maintenanceResponse != null) {
				hotelRequestsPojo.updateStaus(maintenanceResponse.getStatus());
			}
			break;
		case LATE_CHECKOUT:
			LateCheckoutRequest checkoutRespones = lcrService.getEntity(refId);
			if (checkoutRespones != null) {
				hotelRequestsPojo.updateStaus(checkoutRespones.getStatus());
			}
			break;
		case BELL_BOY:
			BellBoyOrder bellboyOrderResponse = bellboyOrderService.getEntity(refId);
			if (bellboyOrderResponse != null) {
				hotelRequestsPojo.updateStaus(bellboyOrderResponse.getStatus());
			}
			break;
		case EARLY_CHECK_IN:
			CheckinEntity checkinEntity = checkinService.getEntity(refId);
			if (checkinEntity != null) {
				hotelRequestsPojo.updateStaus(checkinEntity.getStatus());
			}
			break;
		case LAUNDRY:
			LaundryOrder laundryResponse = laundryOrderService.getEntity(refId);
			if (laundryResponse != null) {
				hotelRequestsPojo.updateStaus(laundryResponse.getStatus());
			}
			break;
		case HOUSEKEEPING:
			HousekeepingOrder hkOrderResponse = hkOrderService.getEntity(refId);
			if (hkOrderResponse != null) {
				hotelRequestsPojo.updateStaus(hkOrderResponse.getStatus());
			}
			break;
		default:
			break;
		}
	}

	@RequestMapping(value = RestMappingConstants.Request.GET_ALL_RESERVATIONS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<HotelRequestsPojo>> getAllReservation(
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.HOTEL_ID, required = true) String hotelId,
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.DATE, required = true) String date)
			throws AboutstaysException {
		validateIdsService.validateHotelId(hotelId);
		try {
			DateUtil.parseDate(date, DateUtil.BASE_DATE_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_DATE_FORMAT, DateUtil.BASE_DATE_FORMAT));
		}
		List<GetUserOptedServiceResponse> getUserOptedServiceResponses = userOptedServicesService
				.getAllByHotelAndDateAndStatus(hotelId, date, RefCollectionType.RESERVATION.getCode());
		if (CollectionUtils.isEmpty(getUserOptedServiceResponses)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		}
		Map<String, List<ReservationOrder>> map = new HashMap<>();
		for (GetUserOptedServiceResponse optedServiceResponse : getUserOptedServiceResponses) {
			ReservationOrder reservationResponse = rsService.getEntity(optedServiceResponse.getRefId());
			if (reservationResponse != null) {
				String rsItemId = reservationResponse.getRsItemId();
				if (!StringUtils.isEmpty(rsItemId)) {
					if (map.containsKey(rsItemId)) {
						map.get(rsItemId).add(reservationResponse);
					} else {
						List<ReservationOrder> list = new ArrayList<>();
						list.add(reservationResponse);
						map.put(rsItemId, list);
					}
				}
			}
		}
		List<HotelRequestsPojo> response = new ArrayList<>();
		for (Map.Entry<String, List<ReservationOrder>> entry : map.entrySet()) {
			ReservationItemResponse resevationItem = reservationItemService.getById(entry.getKey());
			if (resevationItem != null) {
				GetReservationCategoryResponse reservationCategory = reservationCategoryService
						.getById(resevationItem.getCategoryId());
				if (reservationCategory != null) {
					HotelRequestsPojo hotelRequestsPojo = new HotelRequestsPojo();
					hotelRequestsPojo.setServiceName(reservationCategory.getName());
					hotelRequestsPojo.setIconUrl(reservationCategory.getImageUrl());
					for (ReservationOrder reservationOrder : entry.getValue()) {
						hotelRequestsPojo.updateStaus(reservationOrder.getStatus());
					}
					response.add(hotelRequestsPojo);
				}
			}
		}

		return new BaseApiResponse<List<HotelRequestsPojo>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.Request.GET_ALL_ROOM_SERVICES, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<RoomServicesOrder>> getAllRoomServices(
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.HOTEL_ID, required = true) String hotelId,
			@RequestParam(value = RestMappingConstants.GeneralServiceParameters.DATE, required = true) String date)
			throws AboutstaysException {
		validateIdsService.validateHotelId(hotelId);
		try {
			DateUtil.parseDate(date, DateUtil.BASE_DATE_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_DATE_FORMAT, DateUtil.BASE_DATE_FORMAT));
		}
		List<GetUserOptedServiceResponse> getUserOptedServiceResponses = userOptedServicesService
				.getAllByHotelAndDateAndStatus(hotelId, date, RefCollectionType.FOOD.getCode());
		if (CollectionUtils.isEmpty(getUserOptedServiceResponses)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_ROOM_SERVICE_REQUESTS);
		}
		List<RoomServicesOrder> response = new ArrayList<>();
		for (GetUserOptedServiceResponse optedServiceResponse : getUserOptedServiceResponses) {
			GetFoodOrderResponse foodOrder = foodOrdersService.getById(optedServiceResponse.getRefId());
			RoomServicesOrder request = new RoomServicesOrder();
			response.add(request);
			request.setFoodOrderData(foodOrder);
			request.setStaus(foodOrder.getStatus());
			if (foodOrder != null) {
				GetStayResponse stayResponse = staysService.getStayByStayId(foodOrder.getStaysId());
				if (stayResponse != null) {
					request.setRoomData(roomService.getById(stayResponse.getRoomId()));
					Booking booking = bookingService.getEntityById(stayResponse.getBookingId());
					if (booking != null) {
						request.setBookingData(booking);
						if (StringUtils.isEmpty(stayResponse.getUserId())) {
							BookedFor bookedFor = booking.getBookedFor();
							if (bookedFor != null) {
								UserPojo userPojo = new UserPojo();
								userPojo.setFirstName(bookedFor.getName());
								request.setUserData(userPojo);
							}
						}
					}
					if (!StringUtils.isEmpty(stayResponse.getUserId())) {
						request.setUserData(userService.getById(stayResponse.getUserId()));
					}
				}
			}
		}
		return new BaseApiResponse<List<RoomServicesOrder>>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
