package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CartOperation;
import com.aboutstays.enums.CartType;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.enums.LaundryPreferences;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.FoodItemCartRequest;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.pojos.HousekeepingItemOrderDetails;
import com.aboutstays.pojos.LaundryItemOrderDetails;
import com.aboutstays.pojos.StaysIdVsCartData;
import com.aboutstays.pojos.UserCart;
import com.aboutstays.request.dto.AddFoodOrderRequest;
import com.aboutstays.request.dto.AddHKOrderRequest;
import com.aboutstays.request.dto.AddLaundryOrderRequest;
import com.aboutstays.request.dto.HKItemCartRequest;
import com.aboutstays.request.dto.LaundryItemCartRequest;
import com.aboutstays.request.dto.PlaceOrderRequest;
import com.aboutstays.request.dto.UserCartRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.FoodItemCartResponse;
import com.aboutstays.response.dto.FoodItemResponse;
import com.aboutstays.response.dto.HKItemCartResponse;
import com.aboutstays.response.dto.HousekeepingItemResponse;
import com.aboutstays.response.dto.LaundryItemCartResponse;
import com.aboutstays.response.dto.LaundryItemResponse;
import com.aboutstays.response.dto.UserCartResponse;
import com.aboutstays.services.FoodItemService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HousekeepingItemService;
import com.aboutstays.services.LaundryItemService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CartConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CartConstants.BASE)
public class CartController {
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private FoodItemService foodItemService;
	
	@Autowired
	private LaundryItemService laundryItemService;
	
	@Autowired
	private HousekeepingItemService hkItemService;
	
	@Autowired
	private GeneralServicesService generalServicesService;
	
	@Autowired
	private ValidateIdsService validateIdsService;

	private Map<String, UserCart<AddFoodOrderRequest>> userIdVsCartDetailsMap = new ConcurrentHashMap<>();
	private Map<String, UserCart<AddLaundryOrderRequest>> userIdVsLaundryCartDetailsMap = new ConcurrentHashMap<>();
	private Map<String, UserCart<AddHKOrderRequest>> userIdVsHKCartDetailsMap = new ConcurrentHashMap<>();
	
	@RequestMapping(value=RestMappingConstants.CartConstants.SAMPLE_FOOD_ITEM, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<UserCartRequest<FoodItemCartRequest>> getSampleFoodItemRequest(){
		UserCartRequest<FoodItemCartRequest> cartRequest = CartConverter.getSampleFoodCartRequest();
		return new BaseApiResponse<UserCartRequest<FoodItemCartRequest>>(false, cartRequest, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CartConstants.SAMPLE_FOOD_ITEM_LIST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<UserCartRequest<List<FoodItemCartRequest>>> getSampleFoodItemListRequest(){
		UserCartRequest<List<FoodItemCartRequest>> cartRequest = CartConverter.getSampleFoodCartListRequest();
		return new BaseApiResponse<UserCartRequest<List<FoodItemCartRequest>>>(false, cartRequest, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.CartConstants.ADD_OR_REMOVE_MULTIPLE_LAUNDRY_ITEM, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> addMultipleLaundryItems(@RequestBody(required=true)UserCartRequest<List<LaundryItemCartRequest>> cartRequest) throws AboutstaysException{
		ValidateUtils.validateCartRequest(cartRequest);
		validateIds(cartRequest.getUserId(), cartRequest.getHotelId(), cartRequest.getStaysId());
		if(CollectionUtils.isEmpty(cartRequest.getData())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CART_LIST);
		}
		List<BaseApiResponse<Boolean>> responseList = new ArrayList<>(cartRequest.getData().size());
		
		UserCart<AddLaundryOrderRequest> laundryOrderUserCart = userIdVsLaundryCartDetailsMap.get(cartRequest.getUserId());
		if(laundryOrderUserCart == null){
			laundryOrderUserCart = new UserCart<>();
			userIdVsLaundryCartDetailsMap.put(cartRequest.getUserId(), laundryOrderUserCart);
		}
		
		Map<Integer, StaysIdVsCartData<AddLaundryOrderRequest>> cartTypeVsDataMap = laundryOrderUserCart.getCartTypeVsDataMap();
		if(cartTypeVsDataMap == null){
			cartTypeVsDataMap = new HashMap<>();
			laundryOrderUserCart.setCartTypeVsDataMap(cartTypeVsDataMap);
		}
		
		StaysIdVsCartData<AddLaundryOrderRequest> staysIdVsCartData = cartTypeVsDataMap.get(CartType.LAUNDRY.getCode());
		if(staysIdVsCartData == null){
			staysIdVsCartData = new StaysIdVsCartData<>();
			cartTypeVsDataMap.put(CartType.LAUNDRY.getCode(), staysIdVsCartData);
		}
		
		Map<String, AddLaundryOrderRequest> staysIdVsCartDataMap = staysIdVsCartData.getStaysIdVsCartDataMap();
		if(staysIdVsCartDataMap == null){
			staysIdVsCartDataMap = new HashMap<>();
			staysIdVsCartData.setStaysIdVsCartDataMap(staysIdVsCartDataMap);
		}
		
		AddLaundryOrderRequest addLaundryOrderRequest = staysIdVsCartDataMap.get(cartRequest.getStaysId());
		if(addLaundryOrderRequest == null){
			addLaundryOrderRequest = new AddLaundryOrderRequest();
			addLaundryOrderRequest.setHotelId(cartRequest.getHotelId());
			addLaundryOrderRequest.setStaysId(cartRequest.getStaysId());
			addLaundryOrderRequest.setUserId(cartRequest.getUserId());
			staysIdVsCartDataMap.put(cartRequest.getStaysId(), addLaundryOrderRequest);
		}
		
		List<LaundryItemOrderDetails> listOrderDetails = addLaundryOrderRequest.getListOrders();
		for(LaundryItemCartRequest laundryItemCartRequest : cartRequest.getData()){
			BaseApiResponse<Boolean> response = null;
			try{
				
				if(laundryItemCartRequest == null){
					throw new AboutstaysException(AboutstaysResponseCode.CART_DATA_CANNOT_BE_NULL);
				}
				laundryItemCartRequest.setCartType(CartType.LAUNDRY.getCode());
				ValidateUtils.validateCartParams(laundryItemCartRequest.getCartOperation(), laundryItemCartRequest.getCartType());
				LaundryItemOrderDetails laundryItemOrderDetails = laundryItemCartRequest.getItemOrderDetails();
				ValidateUtils.validateLaundryItemOrderDetails(laundryItemOrderDetails);
				
				LaundryItemResponse laundryItemResponse = laundryItemService.getById(laundryItemOrderDetails.getLaundryItemId());
				if(laundryItemResponse == null){
					throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ITEM_NOT_FOUND);
				}
				laundryItemOrderDetails.setItemName(laundryItemResponse.getName());
				laundryItemOrderDetails.setTypeName(LaundryItemType.getNameByCode(laundryItemResponse.getType()));
				
				CartOperation cartOperation = CartOperation.getByCode(laundryItemCartRequest.getCartOperation());
				switch(cartOperation){
				case MODIFY:{
					if(CollectionUtils.isEmpty(listOrderDetails)){
						listOrderDetails = new ArrayList<>();
						addLaundryOrderRequest.setListOrders(listOrderDetails);
					}
					boolean itemFound = false;
					Iterator<LaundryItemOrderDetails> iterator = listOrderDetails.iterator();
					while(iterator.hasNext()){
						LaundryItemOrderDetails laundryItemInCart = iterator.next();
						if(laundryItemInCart.getLaundryItemId().equals(laundryItemOrderDetails.getLaundryItemId())&&laundryItemInCart.getType()==laundryItemOrderDetails.getType()
								&& laundryItemInCart.isSameDaySelected()==laundryItemOrderDetails.isSameDaySelected()){
							if(laundryItemOrderDetails.getQuantity()==0){
								iterator.remove();
							} else {
								laundryItemInCart.setQuantity(laundryItemOrderDetails.getQuantity());
								itemFound = true;
							}
							break;
						}
					}
					if(!itemFound&&laundryItemOrderDetails.getQuantity()>0){
						listOrderDetails.add(laundryItemOrderDetails);
					}
					System.out.println(laundryItemOrderDetails.getLaundryItemId());
					break;
				}
				case DELETE:{
					if(CollectionUtils.isEmpty(listOrderDetails)){
						throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ITEM_NOT_YET_ADDED);
					}
					boolean itemFound = false;
					Iterator<LaundryItemOrderDetails> iterator = listOrderDetails.iterator();
					while(iterator.hasNext()){
						LaundryItemOrderDetails laundryItemsInCart = iterator.next();
						if(laundryItemOrderDetails.getLaundryItemId().equals(laundryItemsInCart.getLaundryItemId())
								&& laundryItemOrderDetails.getType() == laundryItemsInCart.getType()){
							itemFound = true;
							iterator.remove();
							break;
						}
					}
					if(!itemFound){
						throw new AboutstaysException(AboutstaysResponseCode.LAUNDRY_ITEM_NOT_YET_ADDED);
					}
					break;
				}
				}
				response = new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.OPERATION_APPLIED_SUCCESSFULLY);
			} catch(AboutstaysException e){
				response = new BaseApiResponse<Boolean>(true, e.getCode(), false, e.getMessage());
			} catch(Exception e){
				response = new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
			}
			responseList.add(response);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.CartConstants.ADD_OR_REMOVE_MULTIPLE_FOOD_ITEM, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> addMultipleFoodItems(@RequestBody(required=true)UserCartRequest<List<FoodItemCartRequest>> cartRequest) throws AboutstaysException{
		ValidateUtils.validateCartRequest(cartRequest);
		validateIds(cartRequest.getUserId(), cartRequest.getHotelId(), cartRequest.getStaysId());
		if(CollectionUtils.isEmpty(cartRequest.getData())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CART_LIST);
		}
		List<BaseApiResponse<Boolean>> responseList = new ArrayList<>(cartRequest.getData().size());
		
		UserCart<AddFoodOrderRequest> foodOrderUserCart = userIdVsCartDetailsMap.get(cartRequest.getUserId());
		if(foodOrderUserCart==null){
			foodOrderUserCart = new UserCart<>();
			userIdVsCartDetailsMap.put(cartRequest.getUserId(), foodOrderUserCart);
		} 
		
		Map<Integer, StaysIdVsCartData<AddFoodOrderRequest>> cartTypeVsDataMap = foodOrderUserCart.getCartTypeVsDataMap();
		if(cartTypeVsDataMap==null){
			cartTypeVsDataMap = new HashMap<>();
			foodOrderUserCart.setCartTypeVsDataMap(cartTypeVsDataMap);
		}
		
		StaysIdVsCartData<AddFoodOrderRequest> staysIdVsCartData = cartTypeVsDataMap.get(CartType.FOOD_ITEM.getCode());
		if(staysIdVsCartData==null){
			staysIdVsCartData = new StaysIdVsCartData<>();
			cartTypeVsDataMap.put(CartType.FOOD_ITEM.getCode(), staysIdVsCartData);
		}
		
		Map<String, AddFoodOrderRequest> staysIdVsCartDataMap = staysIdVsCartData.getStaysIdVsCartDataMap();
		if(staysIdVsCartDataMap==null){
			staysIdVsCartDataMap = new HashMap<>();
			staysIdVsCartData.setStaysIdVsCartDataMap(staysIdVsCartDataMap);
		}
		
		AddFoodOrderRequest addFoodOrderRequest = staysIdVsCartDataMap.get(cartRequest.getStaysId());
		if(addFoodOrderRequest==null){
			addFoodOrderRequest = new AddFoodOrderRequest();
			addFoodOrderRequest.setHotelId(cartRequest.getHotelId());
			addFoodOrderRequest.setStaysId(cartRequest.getStaysId());
			addFoodOrderRequest.setUserId(cartRequest.getUserId());
			staysIdVsCartDataMap.put(cartRequest.getStaysId(), addFoodOrderRequest);
		}
		
		List<FoodItemOrderDetails> foodItemOrderDetailsList = addFoodOrderRequest.getFoodItemOrders();
		for(FoodItemCartRequest foodItemCartRequest : cartRequest.getData()){
			BaseApiResponse<Boolean> response = null;
			try{
				
				if(foodItemCartRequest==null){
					throw new AboutstaysException(AboutstaysResponseCode.CART_DATA_CANNOT_BE_NULL);
				} 
				foodItemCartRequest.setCartType(CartType.FOOD_ITEM.getCode());
				ValidateUtils.validateCartParams(foodItemCartRequest.getCartOperation(), foodItemCartRequest.getCartType());
				FoodItemOrderDetails foodItemOrderDetails = foodItemCartRequest.getFoodItemOrderDetails();
				ValidateUtils.validateFoodItemOrderDetails(foodItemOrderDetails);
				
				FoodItemResponse foodItemResponse = foodItemService.getById(foodItemOrderDetails.getFoodItemId());
				if(foodItemResponse==null)
					throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
				foodItemOrderDetails.setItemName(foodItemResponse.getName());
				MenuItemType menuItemType = MenuItemType.getByCode(foodItemOrderDetails.getType());
				foodItemOrderDetails.setTypeName(menuItemType.getDisplayName());
				foodItemOrderDetails.setImageUrl(menuItemType.getImageUrl());
				CartOperation cartOperation = CartOperation.getByCode(foodItemCartRequest.getCartOperation());
				switch (cartOperation) {
				case MODIFY:{
					if(CollectionUtils.isEmpty(foodItemOrderDetailsList)){
						foodItemOrderDetailsList = new ArrayList<>();
						addFoodOrderRequest.setFoodItemOrders(foodItemOrderDetailsList);
					}
					
					boolean itemFound = false;
					Iterator<FoodItemOrderDetails> iterator = foodItemOrderDetailsList.iterator();
					while(iterator.hasNext()){
						FoodItemOrderDetails foodItemsInCart = iterator.next();
						if(foodItemsInCart.getFoodItemId().equals(foodItemOrderDetails.getFoodItemId())&&foodItemsInCart.getType()==foodItemOrderDetails.getType()){
							if(foodItemOrderDetails.getQuantity()==0){
								iterator.remove();
							} else {
								foodItemsInCart.setQuantity(foodItemOrderDetails.getQuantity());
								itemFound = true;
							}
							break;
						}
					}
					if(!itemFound&&foodItemOrderDetails.getQuantity()>0){
						foodItemOrderDetailsList.add(foodItemOrderDetails);
					}
					
					System.out.println(foodItemOrderDetails.getFoodItemId());
					break;
				}
				case DELETE:{
					if(CollectionUtils.isEmpty(foodItemOrderDetailsList))
						throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_YET_ADDED);
					boolean itemFound = false;
					Iterator<FoodItemOrderDetails> iterator = foodItemOrderDetailsList.iterator();
					while(iterator.hasNext()){
						FoodItemOrderDetails foodItemsInCart = iterator.next();
						if(foodItemsInCart.getFoodItemId().equals(foodItemOrderDetails.getFoodItemId())&&foodItemsInCart.getType()==foodItemOrderDetails.getType()){
							itemFound = true;
							iterator.remove();
							break;
						}
					}
					if(!itemFound)
						throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_YET_ADDED);
					break;
				}
				}
				response = new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.OPERATION_APPLIED_SUCCESSFULLY);
			}catch(AboutstaysException e){
				response = new BaseApiResponse<Boolean>(true, e.getCode(), false, e.getMessage());
			} catch(Exception e){
				response = new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
			}
			responseList.add(response);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CartConstants.ADD_OR_REMOVE_MULTIPLE_HK_ITEM, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> addMultipleHKItems(@RequestBody(required=true)UserCartRequest<List<HKItemCartRequest>> cartRequest) throws AboutstaysException{
		ValidateUtils.validateCartRequest(cartRequest);
		validateIds(cartRequest.getUserId(), cartRequest.getHotelId(), cartRequest.getStaysId());
		if(CollectionUtils.isEmpty(cartRequest.getData())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CART_LIST);
		}
		List<BaseApiResponse<Boolean>> responseList = new ArrayList<>(cartRequest.getData().size());
		
		UserCart<AddHKOrderRequest> hkOrderUserCart = userIdVsHKCartDetailsMap.get(cartRequest.getUserId());
		if(hkOrderUserCart==null){
			hkOrderUserCart = new UserCart<>();
			userIdVsHKCartDetailsMap.put(cartRequest.getUserId(), hkOrderUserCart);
		} 
		
		Map<Integer, StaysIdVsCartData<AddHKOrderRequest>> cartTypeVsDataMap = hkOrderUserCart.getCartTypeVsDataMap();
		if(cartTypeVsDataMap==null){
			cartTypeVsDataMap = new HashMap<>();
			hkOrderUserCart.setCartTypeVsDataMap(cartTypeVsDataMap);
		}
		
		StaysIdVsCartData<AddHKOrderRequest> staysIdVsCartData = cartTypeVsDataMap.get(CartType.HOUSEKEEPING.getCode());
		if(staysIdVsCartData==null){
			staysIdVsCartData = new StaysIdVsCartData<>();
			cartTypeVsDataMap.put(CartType.HOUSEKEEPING.getCode(), staysIdVsCartData);
		}
		
		Map<String, AddHKOrderRequest> staysIdVsCartDataMap = staysIdVsCartData.getStaysIdVsCartDataMap();
		if(staysIdVsCartDataMap==null){
			staysIdVsCartDataMap = new HashMap<>();
			staysIdVsCartData.setStaysIdVsCartDataMap(staysIdVsCartDataMap);
		}
		
		AddHKOrderRequest addHKOrderRequest = staysIdVsCartDataMap.get(cartRequest.getStaysId());
		if(addHKOrderRequest==null){
			addHKOrderRequest = new AddHKOrderRequest();
			addHKOrderRequest.setHotelId(cartRequest.getHotelId());
			addHKOrderRequest.setStaysId(cartRequest.getStaysId());
			addHKOrderRequest.setUserId(cartRequest.getUserId());
			staysIdVsCartDataMap.put(cartRequest.getStaysId(), addHKOrderRequest);
		}
		
		List<HousekeepingItemOrderDetails> hkItemOrderDetailsList = addHKOrderRequest.getListOrders();
		for(HKItemCartRequest hkItemCartRequest : cartRequest.getData()){
			BaseApiResponse<Boolean> response = null;
			try{
				
				if(hkItemCartRequest==null){
					throw new AboutstaysException(AboutstaysResponseCode.CART_DATA_CANNOT_BE_NULL);
				} 
				hkItemCartRequest.setCartType(CartType.HOUSEKEEPING.getCode());
				ValidateUtils.validateCartParams(hkItemCartRequest.getCartOperation(), hkItemCartRequest.getCartType());
				HousekeepingItemOrderDetails hkItemOrderDetails = hkItemCartRequest.getHkItemOrderDetails();
				ValidateUtils.validateHKItemOrderDetails(hkItemOrderDetails);
				
				HousekeepingItemResponse hkItemResponse = hkItemService.getById(hkItemOrderDetails.getHkItemId());
				if(hkItemResponse == null){
					throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
				}
				hkItemOrderDetails.setName(hkItemResponse.getName());
				hkItemOrderDetails.setTypeName(HousekeepingItemType.getNameByCode(hkItemResponse.getType()));
				
				CartOperation cartOperation = CartOperation.getByCode(hkItemCartRequest.getCartOperation());
				switch (cartOperation) {
				case MODIFY:{
					if(CollectionUtils.isEmpty(hkItemOrderDetailsList)){
						hkItemOrderDetailsList = new ArrayList<>();
						addHKOrderRequest.setListOrders(hkItemOrderDetailsList);
					}
					boolean itemFound = false;
					Iterator<HousekeepingItemOrderDetails> iterator = hkItemOrderDetailsList.iterator();
					while(iterator.hasNext()){
						HousekeepingItemOrderDetails hkItemsInCart = iterator.next();
						if(hkItemsInCart.getHkItemId().equals(hkItemOrderDetails.getHkItemId())&&hkItemsInCart.getType()==hkItemOrderDetails.getType()){
							if(hkItemOrderDetails.getQuantity() == 0){
								iterator.remove();
							} else{
								itemFound = true;
								hkItemsInCart.setQuantity(hkItemOrderDetails.getQuantity());
							}
							break;
						}
					}
					if(!itemFound && hkItemOrderDetails.getQuantity() > 0){
						hkItemOrderDetailsList.add(hkItemOrderDetails);
					}
					System.out.println(hkItemOrderDetails.getHkItemId());
					break;
				}
				case DELETE:{
					if(CollectionUtils.isEmpty(hkItemOrderDetailsList))
						throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ITEM_NOT_ADDED_YET);
					boolean itemFound = false;
					Iterator<HousekeepingItemOrderDetails> iterator = hkItemOrderDetailsList.iterator();
					while(iterator.hasNext()){
						HousekeepingItemOrderDetails hkItemsInCart = iterator.next();
						if(hkItemsInCart.getHkItemId().equals(hkItemOrderDetails.getHkItemId())&&hkItemsInCart.getType()==hkItemOrderDetails.getType()){
							itemFound = true;
							iterator.remove();
							break;
						}
					}
					if(!itemFound)
						throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ITEM_NOT_ADDED_YET);
					break;
				}
				}
				response = new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.OPERATION_APPLIED_SUCCESSFULLY);
			}catch(AboutstaysException e){
				response = new BaseApiResponse<Boolean>(true, e.getCode(), false, e.getMessage());
			} catch(Exception e){
				response = new BaseApiResponse<Boolean>(true, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
			}
			responseList.add(response);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.CartConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<UserCartResponse> getUserCart(@RequestParam(value=RestMappingConstants.CartParameters.USER_ID)String userId,
			@RequestParam(value=RestMappingConstants.CartParameters.STAYS_ID)String staysId,
			@RequestParam(value=RestMappingConstants.CartParameters.CART_TYPE)Integer cartType,
			@RequestParam(value=RestMappingConstants.CartParameters.HOTEL_ID)String hotelId) throws AboutstaysException{
		validateIds(userId, hotelId, staysId);

		CartType cartTypeEnum = CartType.getByCode(cartType);
		if(cartTypeEnum==null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CART_TYPE);
		}
		switch (cartTypeEnum) {
		case FOOD_ITEM:{
			FoodItemCartResponse foodItemCartResponse = new FoodItemCartResponse();
			
			UserCart<AddFoodOrderRequest> userCart = userIdVsCartDetailsMap.get(userId);
			if(userCart==null)
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			StaysIdVsCartData<AddFoodOrderRequest> staysIdVsCartData = userCart.getCartTypeVsDataMap().get(cartType);
			if(staysIdVsCartData==null)
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			AddFoodOrderRequest addFoodOrderRequest = staysIdVsCartData.getStaysIdVsCartDataMap().get(staysId);
			if(addFoodOrderRequest==null||CollectionUtils.isEmpty(addFoodOrderRequest.getFoodItemOrders()))
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			
			foodItemCartResponse.setFoodItemOrderDetailList(addFoodOrderRequest.getFoodItemOrders());
			foodItemCartResponse.setCartType(cartType);
			foodItemCartResponse.setHotelId(hotelId);
			foodItemCartResponse.setStaysId(staysId);
			foodItemCartResponse.setUserId(userId);
			return new BaseApiResponse<UserCartResponse>(false, foodItemCartResponse, AboutstaysResponseCode.SUCCESS);
		}
		case HOUSEKEEPING:{
			HKItemCartResponse hkItemCartResponse = new HKItemCartResponse();
			
			UserCart<AddHKOrderRequest> userCart = userIdVsHKCartDetailsMap.get(userId);
			if(userCart==null)
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			StaysIdVsCartData<AddHKOrderRequest> staysIdVsCartData = userCart.getCartTypeVsDataMap().get(cartType);
			if(staysIdVsCartData==null)
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			AddHKOrderRequest addHKOrderRequest = staysIdVsCartData.getStaysIdVsCartDataMap().get(staysId);
			if(addHKOrderRequest==null||CollectionUtils.isEmpty(addHKOrderRequest.getListOrders()))
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			
			hkItemCartResponse.setHkItemOrderDetailsList(addHKOrderRequest.getListOrders());
			hkItemCartResponse.setCartType(cartType);
			hkItemCartResponse.setHotelId(hotelId);
			hkItemCartResponse.setStaysId(staysId);
			hkItemCartResponse.setUserId(userId);
			return new BaseApiResponse<UserCartResponse>(false, hkItemCartResponse, AboutstaysResponseCode.SUCCESS);
		}
		case INTERNET:
			break;
		case LAUNDRY:{
			LaundryItemCartResponse laundryItemCartResponse = new LaundryItemCartResponse();
			UserCart<AddLaundryOrderRequest> userCart = userIdVsLaundryCartDetailsMap.get(userId);
			if(userCart == null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			StaysIdVsCartData<AddLaundryOrderRequest> staysIdVsCartData = userCart.getCartTypeVsDataMap().get(cartType);
			if(staysIdVsCartData == null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			AddLaundryOrderRequest addLaundryOrderRequest = staysIdVsCartData.getStaysIdVsCartDataMap().get(staysId);
			if(addLaundryOrderRequest == null || CollectionUtils.isEmpty(addLaundryOrderRequest.getListOrders())){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			laundryItemCartResponse.setListLaundryItemOrderDetails(addLaundryOrderRequest.getListOrders());
			laundryItemCartResponse.setCartType(cartType);
			laundryItemCartResponse.setHotelId(hotelId);
			laundryItemCartResponse.setStaysId(staysId);
			laundryItemCartResponse.setUserId(userId);
			return new BaseApiResponse<UserCartResponse>(false, laundryItemCartResponse, AboutstaysResponseCode.SUCCESS);
		}
		}
		throw new AboutstaysException(AboutstaysResponseCode.UNIMPLEMENTED_CART_OPERATION);
		//return null;
	}
	
	@RequestMapping(value=RestMappingConstants.CartConstants.PLACE_ORDER, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> placeOrder(@RequestBody(required=true)PlaceOrderRequest request) throws AboutstaysException{
		ValidateUtils.validatePlaceOrderRequest(request);
		validateIds(request.getUserId(), request.getHotelId(), request.getStaysId());

		if(!generalServicesService.existsById(request.getGsId())){
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		}
		
		CartType cartType = CartType.getByCode(request.getCartType());
			if(cartType == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_CART_TYPE);
			}
		switch(cartType){
		case LAUNDRY:{
			UserCart<AddLaundryOrderRequest> laundryOrderUserCart = userIdVsLaundryCartDetailsMap.get(request.getUserId());
			if(laundryOrderUserCart == null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			Map<Integer, StaysIdVsCartData<AddLaundryOrderRequest>> cartTypeVsDataMap = laundryOrderUserCart.getCartTypeVsDataMap();
			if(cartTypeVsDataMap==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			StaysIdVsCartData<AddLaundryOrderRequest> staysIdVsCartData = cartTypeVsDataMap.get(CartType.LAUNDRY.getCode());
			if(staysIdVsCartData==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			Map<String, AddLaundryOrderRequest> staysIdVsCartDataMap = staysIdVsCartData.getStaysIdVsCartDataMap();
			if(staysIdVsCartDataMap==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			AddLaundryOrderRequest addLaundryOrderRequest = staysIdVsCartDataMap.get(request.getStaysId());
			if(addLaundryOrderRequest==null || CollectionUtils.isEmpty(addLaundryOrderRequest.getListOrders())){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			addLaundryOrderRequest.setComment(request.getComment());
			addLaundryOrderRequest.setDeliveryTime(request.getDeliveryTime());
			addLaundryOrderRequest.setDeliverTimeString(request.getDeliverTimeString());
			addLaundryOrderRequest.setGsId(request.getGsId());
			addLaundryOrderRequest.setHotelId(request.getHotelId());
			addLaundryOrderRequest.setRequestedNow(request.isRequestedNow());
			addLaundryOrderRequest.setStaysId(request.getStaysId());
			addLaundryOrderRequest.setUserId(request.getUserId());
			if(request.getLaundryPrefs() == 0 || LaundryPreferences.findLaundryByType(request.getLaundryPrefs()) == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_LAUNDRY_PREF_TYPE);
			}
			addLaundryOrderRequest.setLaundryPreference(request.getLaundryPrefs());
			
			LaundryOrderController laundryOrderController = new LaundryOrderController();
			beanFactory.autowireBean(laundryOrderController);
			BaseApiResponse<String> response = laundryOrderController.add(addLaundryOrderRequest);
			
			staysIdVsCartDataMap.remove(request.getStaysId());
			if(staysIdVsCartDataMap.isEmpty()){
				cartTypeVsDataMap.remove(request.getCartType());
			}
			if(cartTypeVsDataMap.isEmpty()){
				userIdVsCartDetailsMap.remove(request.getUserId());
			}
			
			return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS_ORDER_PLACED);
		}
		case FOOD_ITEM:{
			UserCart<AddFoodOrderRequest> foodOrderUserCart = userIdVsCartDetailsMap.get(request.getUserId());
			if(foodOrderUserCart==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			} 
			
			Map<Integer, StaysIdVsCartData<AddFoodOrderRequest>> cartTypeVsDataMap = foodOrderUserCart.getCartTypeVsDataMap();
			if(cartTypeVsDataMap==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			StaysIdVsCartData<AddFoodOrderRequest> staysIdVsCartData = cartTypeVsDataMap.get(CartType.FOOD_ITEM.getCode());
			if(staysIdVsCartData==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			Map<String, AddFoodOrderRequest> staysIdVsCartDataMap = staysIdVsCartData.getStaysIdVsCartDataMap();
			if(staysIdVsCartDataMap==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			AddFoodOrderRequest addFoodOrderRequest = staysIdVsCartDataMap.get(request.getStaysId());
			if(addFoodOrderRequest==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			addFoodOrderRequest.setHotelId(request.getHotelId());
			addFoodOrderRequest.setStaysId(request.getStaysId());
			addFoodOrderRequest.setUserId(request.getUserId());
			addFoodOrderRequest.setDeliverTimeString(request.getDeliverTimeString());
			addFoodOrderRequest.setComment(request.getComment());
			addFoodOrderRequest.setDeliveryTime(request.getDeliveryTime());
			addFoodOrderRequest.setGsId(request.getGsId());
			addFoodOrderRequest.setRequestedNow(request.isRequestedNow());
			
			FoodOrderController foodOrderController = new FoodOrderController();
			beanFactory.autowireBean(foodOrderController);
			BaseApiResponse<String> response = foodOrderController.add(addFoodOrderRequest, true);
			
			staysIdVsCartDataMap.remove(request.getStaysId());
			if(staysIdVsCartDataMap.isEmpty()){
				cartTypeVsDataMap.remove(request.getCartType());
			}
			if(cartTypeVsDataMap.isEmpty()){
				userIdVsCartDetailsMap.remove(request.getUserId());
			}
			
			return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS_ORDER_PLACED);
		}
		case HOUSEKEEPING:{
			UserCart<AddHKOrderRequest> hkOrderUserCart = userIdVsHKCartDetailsMap.get(request.getUserId());
			if(hkOrderUserCart==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			} 
			
			Map<Integer, StaysIdVsCartData<AddHKOrderRequest>> cartTypeVsDataMap = hkOrderUserCart.getCartTypeVsDataMap();
			if(cartTypeVsDataMap==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			StaysIdVsCartData<AddHKOrderRequest> staysIdVsCartData = cartTypeVsDataMap.get(CartType.HOUSEKEEPING.getCode());
			if(staysIdVsCartData==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			Map<String, AddHKOrderRequest> staysIdVsCartDataMap = staysIdVsCartData.getStaysIdVsCartDataMap();
			if(staysIdVsCartDataMap==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			AddHKOrderRequest addHKOrderRequest = staysIdVsCartDataMap.get(request.getStaysId());
			if(addHKOrderRequest==null){
				throw new AboutstaysException(AboutstaysResponseCode.CART_NOT_EXIST);
			}
			
			addHKOrderRequest.setHotelId(request.getHotelId());
			addHKOrderRequest.setStaysId(request.getStaysId());
			addHKOrderRequest.setUserId(request.getUserId());
			addHKOrderRequest.setComment(request.getComment());
			addHKOrderRequest.setDeliveryTime(request.getDeliveryTime());
			addHKOrderRequest.setDeliverTimeString(request.getDeliverTimeString());
			addHKOrderRequest.setGsId(request.getGsId());
			addHKOrderRequest.setRequestedNow(request.isRequestedNow());
			
			HKOrderController hkOrderController = new HKOrderController();
			beanFactory.autowireBean(hkOrderController);
			BaseApiResponse<String> response = hkOrderController.add(addHKOrderRequest, true);
			
			staysIdVsCartDataMap.remove(request.getStaysId());
			if(staysIdVsCartDataMap.isEmpty()){
				cartTypeVsDataMap.remove(request.getCartType());
			}
			if(cartTypeVsDataMap.isEmpty()){
				userIdVsCartDetailsMap.remove(request.getUserId());
			}
			
			return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS_ORDER_PLACED);
		}
		}
		return null;		
	}
	
	
	
	public void validateIds(String userId, String hotelId, String staysId) throws AboutstaysException{
		validateIdsService.validateUserHotelStaysIds(userId, hotelId, staysId);
	}
	
}
