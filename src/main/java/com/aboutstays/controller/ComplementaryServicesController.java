package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddComplementaryServicesRequest;
import com.aboutstays.request.dto.UpdateComplementaryServicesRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetComplementaryServicesResponse;
import com.aboutstays.services.ComplementaryServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.ComplementaryServicesConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.ComplementaryServicesConstants.BASE)
public class ComplementaryServicesController {

	@Autowired
	private ComplementaryServicesService complementaryServicesService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddComplementaryServicesRequest> getSampleRequest(){
		AddComplementaryServicesRequest request = ComplementaryServicesConverter.getSampleRequest();
		return new BaseApiResponse<AddComplementaryServicesRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ComplementaryServicesConstants.ADD, method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addService(@RequestBody(required=true)AddComplementaryServicesRequest request) throws AboutstaysException{
		ValidateUtils.validateAddComplementaryServicesRequest(request);
		validateIdsService.validateHotelId(request.getServicesId());
		String id = complementaryServicesService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.COMPLEMENTARY_SERVICE_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.ComplementaryServicesConstants.UPDATE, method = RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateService(@RequestBody(required=true)UpdateComplementaryServicesRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateComplementaryServicesRequest(request);
		if(!complementaryServicesService.existsById(request.getServicesId())){
			throw new AboutstaysException(AboutstaysResponseCode.COMPLEMENTARY_SERVICE_NOT_FOUND);
		}
		complementaryServicesService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.COMPLEMENTARY_SERVICE_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.ComplementaryServicesConstants.GET_BY_ID, method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetComplementaryServicesResponse> getService(@RequestParam(value=RestMappingConstants.ComplementaryServicesParameters.ID)String id) throws AboutstaysException{
		GetComplementaryServicesResponse response = complementaryServicesService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.COMPLEMENTARY_SERVICE_NOT_FOUND);
		return new BaseApiResponse<GetComplementaryServicesResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ComplementaryServicesConstants.GET_ALL, method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetComplementaryServicesResponse>> getAllServices() throws AboutstaysException{
		List<GetComplementaryServicesResponse> response = complementaryServicesService.getAll();
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.NO_COMPLEMENTARY_SERVICE);
		return new BaseApiResponse<List<GetComplementaryServicesResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ComplementaryServicesConstants.DELETE, method = RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteService(@RequestParam(value=RestMappingConstants.ComplementaryServicesParameters.ID)String id) throws AboutstaysException{
		if(!complementaryServicesService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.COMPLEMENTARY_SERVICE_NOT_FOUND);
		}
		complementaryServicesService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.COMPLEMENTARY_SERVICE_DELETED_SUCCESSFULLY);
	}
}
