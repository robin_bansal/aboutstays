package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.ReservationType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.ReservationItemTypeInfo;
import com.aboutstays.request.dto.UpdateReservationServiceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetReservationServiceResponse;
import com.aboutstays.services.ReservationServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.services.impl.AddReservationServiceRequest;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ReservationServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.BASE)
public class ReservationServiceController {
	
	@Autowired
	private ReservationServicesService rsService;
	
	@Autowired
	private ValidateIdsService validateIdsService; 
	
	@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddReservationServiceRequest> getSample(){
		AddReservationServiceRequest response = ReservationServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddReservationServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddReservationServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddReservationServiceRequest(request);
		validateIdsService.validateHotelId(request.getId());
		Iterator<ReservationItemTypeInfo> iterator = request.getTypeInfoList().iterator();
		Set<Integer> reservationTypeSet = new HashSet<>(); 
		while (iterator.hasNext()) {
			ReservationItemTypeInfo itemByType = iterator.next();
			ReservationType itemType = ReservationType.getByCode(itemByType.getType()); 
			if(itemType==null){
				throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_RESERVATION_TYPE);
			} else if(reservationTypeSet.contains(itemByType.getType())){
				iterator.remove();
			} else {
				reservationTypeSet.add(itemByType.getType());
			}
			itemByType.setTypeName(itemType.getDisplayName());
			itemByType.setImageUrl(itemType.getImageUrl());
		}
		
		String response = rsService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_RESERVATION_SERVICE_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetReservationServiceResponse>> getAll() throws AboutstaysException{
		List<GetReservationServiceResponse> response = rsService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_NOT_FOUND);
		}
		return new BaseApiResponse<List<GetReservationServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetReservationServiceResponse> get(@RequestParam(value=RestMappingConstants.ReservationServiceParameters.ID, required=true)String id) throws AboutstaysException{
		GetReservationServiceResponse response = rsService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_NOT_FOUND);
		}
		return new BaseApiResponse<GetReservationServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.ReservationServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!rsService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_NOT_FOUND);
		}
		Boolean response = rsService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateReservationServiceRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_SERVICE_ID);
		}
		if(!rsService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_NOT_FOUND);
		}
		if(CollectionUtils.isNotEmpty(request.getTypeInfoList())){
			for(ReservationItemTypeInfo itemByType : request.getTypeInfoList()){
				ReservationType itemType = ReservationType.getByCode(itemByType.getType()); 
				if(itemType==null){
					throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_RESERVATION_TYPE);
				}
				itemByType.setTypeName(itemType.getDisplayName());
				itemByType.setImageUrl(itemType.getImageUrl());
			}
		}
		Boolean response = rsService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
