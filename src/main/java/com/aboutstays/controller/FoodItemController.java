package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.FoodItem;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.GoesWellWithItem;
import com.aboutstays.request.dto.AddGoesWellItemsRequest;
import com.aboutstays.request.dto.BatchUpdateFoodItemsRequest;
import com.aboutstays.request.dto.FoodItemRequest;
import com.aboutstays.request.dto.UpdateFoodItemRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.FoodItemResponse;
import com.aboutstays.services.FoodItemService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.RoomServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.FoodItemConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.FoodItemConstants.BASE)
public class FoodItemController {

	@Autowired
	private FoodItemService foodItemService;

	@Autowired
	private HotelService hotelService;

	@Autowired
	private RoomServicesService roomService;
	
	@Autowired
	private ValidateIdsService validateIdsService;

	@RequestMapping(value = RestMappingConstants.REQUEST, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<FoodItemRequest> getSampleRequest() {
		FoodItemRequest request = FoodItemConverter.getSampleRequest();
		return new BaseApiResponse<FoodItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FoodItemConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addFoodItem(@RequestBody(required = true) FoodItemRequest request)
			throws AboutstaysException {
		ValidateUtils.validateFoodItemRequest(request);
		validateIdsService.validateHotelId(request.getHotelId());
		if (!roomService.existsById(request.getRoomServicesId())) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_SERVICE_NOT_EXISTS);
		}
		if (CollectionUtils.isNotEmpty(request.getListGoesWellItems())) {
			Iterator<GoesWellWithItem> iterator = request.getListGoesWellItems().iterator();
			while (iterator.hasNext()) {
				GoesWellWithItem item = iterator.next();
				if (StringUtils.isEmpty(item.getFoodItemId())) {
					iterator.remove();
					continue;
				} else if (!foodItemService.existsById(item.getFoodItemId())) {
					throw new AboutstaysException(String.format(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getMessage(),
							item.getFoodItemId()), AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getCode());
				}
			}
		}
		String id = foodItemService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.FOOD_ITEM_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.FoodItemConstants.ADD_ALL, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<String>>> addAllFoodItems(
			@RequestBody(required = true) List<FoodItemRequest> requestList) throws AboutstaysException {
		if (CollectionUtils.isEmpty(requestList)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_REQUEST_LIST);
		}
		List<BaseApiResponse<String>> responseList = new ArrayList<>();
		for (FoodItemRequest request : requestList) {
			responseList.add(addFoodItem(request));
		}
		return new BaseApiResponse<List<BaseApiResponse<String>>>(false, responseList,
				AboutstaysResponseCode.FOOD_ITEM_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value = RestMappingConstants.FoodItemConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<FoodItemResponse>> getAll() throws AboutstaysException {
		List<FoodItemResponse> responseList = foodItemService.getAllFoodItems();
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_FOOD_ITEM_ADDED_YET);
		return new BaseApiResponse<List<FoodItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FoodItemConstants.GET_ALL_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<FoodItemResponse>> getAllByHotelId(
			@RequestParam(value = RestMappingConstants.FoodItemParameters.HOTEL_ID) String hotelId)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<FoodItemResponse> responseList = foodItemService.getAllByHotelId(hotelId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_FOOD_ITEM_ADDED_YET);
		return new BaseApiResponse<List<FoodItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FoodItemConstants.GET_ALL_BY_ROOM_SERVICE_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<FoodItemResponse>> getAllByRoomServicesId(
			@RequestParam(value = RestMappingConstants.FoodItemParameters.ROOM_SERVICE_ID) String roomServicesId)
			throws AboutstaysException {
		if (!roomService.existsById(roomServicesId)) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_SERVICE_NOT_EXISTS);
		}
		List<FoodItemResponse> responseList = foodItemService.getAllByRoomServicesId(roomServicesId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_FOOD_ITEM_ADDED_YET);
		return new BaseApiResponse<List<FoodItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value = RestMappingConstants.FoodItemConstants.LINK_GOES_LIST, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addGoesWellWithItemsList(
			@RequestBody(required = true) AddGoesWellItemsRequest request) throws AboutstaysException {
		ValidateUtils.validateAddGoesWellWithItemRequest(request);
		FoodItem foodItem = foodItemService.getEntityById(request.getFoodItemId());
		if (foodItem == null)
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
		for (String foodItemId : request.getGoesWellWithItemIds()) {
			if (!foodItemService.existsById(foodItemId)) {
				throw new AboutstaysException(
						String.format(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getMessage(), foodItemId),
						AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getCode());
			}
		}
		boolean newItemFound = false;
		List<GoesWellWithItem> goesList = foodItem.getListGoesWellItems();
		if (CollectionUtils.isNotEmpty(goesList)) {

			Iterator<GoesWellWithItem> iterator = goesList.iterator();
			while (iterator.hasNext()) {
				GoesWellWithItem goesWellWithItem = iterator.next();
				if (!foodItemService.existsById(goesWellWithItem.getFoodItemId())) {
					iterator.remove();
					continue;
				}
				if (request.getGoesWellWithItemIds().contains(goesWellWithItem.getFoodItemId())) {
					newItemFound = true;
					request.getGoesWellWithItemIds().remove(goesWellWithItem.getFoodItemId());
				}
			}

		} else {
			goesList = new ArrayList<>();
			newItemFound = true;
		}
		if (newItemFound) {
			for (String foodItemId : request.getGoesWellWithItemIds()) {
				GoesWellWithItem goesWellWithItem = new GoesWellWithItem();
				goesWellWithItem.setFoodItemId(foodItemId);
				goesList.add(goesWellWithItem);
			}
			foodItem.setListGoesWellItems(goesList);
			foodItemService.updateEntity(foodItem);
		} else {
			throw new AboutstaysException(AboutstaysResponseCode.GOES_IDS_ALREADY_EXIST);
		}
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.GOES_ITEMS_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodItemConstants.FETCH_BY_TYPE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<FoodItemResponse>> fetchAllItemsByType(@RequestParam(value = RestMappingConstants.FoodItemParameters.ROOM_SERVICE_ID) 
			String roomServicesId, @RequestParam(value = RestMappingConstants.FoodItemParameters.TYPE)Integer type) throws AboutstaysException{
		MenuItemType menuItemType = MenuItemType.getByCode(type);
		if(menuItemType==null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MENU_ITEM_TYPE);
		if (!roomService.existsById(roomServicesId)) {
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_SERVICE_NOT_EXISTS);
		}
		List<FoodItemResponse> responseList = foodItemService.getAllByType(roomServicesId,menuItemType);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_FOOD_ITEM_ADDED_YET);
		return new BaseApiResponse<List<FoodItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodItemConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<FoodItemResponse> getItemById(@RequestParam(value=RestMappingConstants.FoodItemParameters.ID)String id) throws AboutstaysException{
		FoodItemResponse response = foodItemService.getById(id);
		if(response==null){
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
		}
		return new BaseApiResponse<FoodItemResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodItemConstants.DELETE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteFoodItem(@RequestParam(value=RestMappingConstants.FoodItemParameters.ID)String id) throws AboutstaysException{
		if(!foodItemService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
		foodItemService.deleteFoodItem(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.FOOD_ITEM_DELETED_SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.FoodItemConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateFoodItem(@RequestBody UpdateFoodItemRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateFoodItemRequest(request);
		if(!foodItemService.existsById(request.getFooditemId())){
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
		}
		if (CollectionUtils.isNotEmpty(request.getListGoesWellItems())) {
			Iterator<GoesWellWithItem> iterator = request.getListGoesWellItems().iterator();
			while (iterator.hasNext()) {
				GoesWellWithItem item = iterator.next();
				if (StringUtils.isEmpty(item.getFoodItemId())) {
					iterator.remove();
					continue;
				} else if (!foodItemService.existsById(item.getFoodItemId())) {
					throw new AboutstaysException(String.format(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getMessage(),
							item.getFoodItemId()), AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getCode());
				}
			}
		}
		foodItemService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.FOOD_ITEM_UPDATED_SUCCESSFULLY, request.getName()));
	}
	
	@RequestMapping(value=RestMappingConstants.FoodItemConstants.BATCH_UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> batchUpdateFoodItems(@RequestBody BatchUpdateFoodItemsRequest request){
		if(CollectionUtils.isNotEmpty(request.getItems())){
			List<BaseApiResponse<Boolean>> response = new ArrayList<>();
			boolean allUpdated = true;
			for(UpdateFoodItemRequest item : request.getItems()){
				BaseApiResponse<Boolean> updateItemResponse = null;
				try {
					updateItemResponse = updateFoodItem(item);
				} catch (AboutstaysException e) {
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, e.getCode(), false, e.getMessage());
				} catch (Exception e){
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
				}
				response.add(updateItemResponse);
			}
			if(!allUpdated){
				return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(true, response, AboutstaysResponseCode.SOME_FOOD_ITEMS_NOT_UPDATED);
			}
			return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, response, AboutstaysResponseCode.ALL_FOOD_ITEMS_UPDATED);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, null, AboutstaysResponseCode.SUCCESS);
	}

}
