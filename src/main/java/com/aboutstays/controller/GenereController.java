package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddMasterGenereRequest;
import com.aboutstays.request.dto.DeleteGenereRequest;
import com.aboutstays.request.dto.UpdateMasterGenereRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetMasterGenereResponse;
import com.aboutstays.services.MasterGenereService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MasterGenereConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.MasterGenereConstants.BASE)
public class GenereController {

	@Autowired
	private MasterGenereService masterGenereService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMasterGenereRequest> getSampleRequest(){
		AddMasterGenereRequest request = MasterGenereConverter.getSampleRequest();
		return new BaseApiResponse<AddMasterGenereRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterGenereConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addMasterGenere(@RequestBody(required=true)AddMasterGenereRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMasterGenereRequest(request);
		if(masterGenereService.existsByName(request.getName(),null)){
			throw new AboutstaysException(AboutstaysResponseCode.GENERE_ALREADY_EXISTS);
		}
		String id = masterGenereService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.GENERE_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterGenereConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateMasterGenere(@RequestBody(required=true)UpdateMasterGenereRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateMasterGenereRequest(request);
		if(!masterGenereService.existsById(request.getMasterGenereId()))
			throw new AboutstaysException(AboutstaysResponseCode.GENERE_NOT_FOUND);
		if(masterGenereService.existsByName(request.getName(),request.getMasterGenereId())){
			throw new AboutstaysException(AboutstaysResponseCode.GENERE_ALREADY_EXISTS);
		}
		masterGenereService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.GENERE_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterGenereConstants.DELETE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteMasterGenere(@RequestBody(required=true)DeleteGenereRequest request) throws AboutstaysException{
		ValidateUtils.validateDeleteGenereRequest(request);
		if(!masterGenereService.existsById(request.getMasterGenereId()))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.GENERE_NOT_FOUND, request.getMasterGenereId()));
		masterGenereService.delete(request.getMasterGenereId());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.GENERE_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterGenereConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMasterGenereResponse> getMasterGenere(@RequestParam(value=RestMappingConstants.MasterGenereParameters.ID)String id) throws AboutstaysException{
		GetMasterGenereResponse response = masterGenereService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.GENERE_NOT_FOUND);
		return new BaseApiResponse<GetMasterGenereResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterGenereConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetMasterGenereResponse>> getAllGeneres() throws AboutstaysException{
		List<GetMasterGenereResponse> responseList = masterGenereService.getAll();
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_GENERE_FOUND);
		return new BaseApiResponse<List<GetMasterGenereResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
}
