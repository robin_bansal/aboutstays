package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CarType;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.PickupStatus;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddAirportPickupRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.ApprovePickupRequest;
import com.aboutstays.request.dto.CanceRequestByUosID;
import com.aboutstays.request.dto.GetAirportPickupResponse;
import com.aboutstays.request.dto.UpdateAirportPickupRequest;
import com.aboutstays.response.dto.AddAirportPickupResponse;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.services.AirportPickupService;
import com.aboutstays.services.CarService;
import com.aboutstays.services.DriverService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.AirportPickupConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.AirportPickupConstants.BASE)
public class AirportPickupController extends BaseSessionController{
		
	@Autowired
	private AirportPickupService airportPickupServie;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private DriverService driverService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private UserOptedServicesService uosService;
		
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddAirportPickupRequest> getSample(){
		AddAirportPickupRequest response = AirportPickupConverter.getSampleRequest();
		return new BaseApiResponse<AddAirportPickupRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddAirportPickupResponse> add(@RequestBody(required=true)AddAirportPickupRequest request) throws AboutstaysException{
		logger.info(request.toString());
		baseSessionService.validateBaseSessionSessionData(request);
		ValidateUtils.validateAddAirportPickupRequest(request);

		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId());
		request.setPickupDateAndTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())+timezoneDiff);
		
		Integer typeValue = request.getCarType();
		if(typeValue != null){
			if(CarType.findValueByCode(typeValue) == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
			}
		} else{
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CAR_TYPE);
		}

		AddAirportPickupResponse addResponse = airportPickupServie.add(request);
		if(addResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_AIRPORT_PICKUP_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createAddUserOptedServiceRequest(request, addResponse);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_AIRPORT_PICKUP_FAILED);
		}
		return new BaseApiResponse<AddAirportPickupResponse>(false, addResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetAirportPickupResponse>> getAll(@RequestParam(value=RestMappingConstants.AirportPickupParameters.USER_ID, required=false)String userId) throws AboutstaysException{
		if(!StringUtils.isEmpty(userId) && !userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<GetAirportPickupResponse>	response = airportPickupServie.getAll(userId);
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_AIRPORT_PICKUPS_FAILED);
		}
		return new BaseApiResponse<List<GetAirportPickupResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetAirportPickupResponse> get(@RequestParam(value=RestMappingConstants.AirportPickupParameters.ID, required=true)String id) throws AboutstaysException{
		GetAirportPickupResponse response = airportPickupServie.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_PICKUP_NOT_EXISTS);
		}
		if(PickupStatus.findValueByType(response.getPickupStatus()) == PickupStatus.CANCELLED){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_PICKUP_CANCELLED);
		}
		return new BaseApiResponse<GetAirportPickupResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateAirportPickupRequest request) throws AboutstaysException{
		logger.info(request.toString());
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_PICKUP_ID);
		}
		if(!airportPickupServie.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_PICKUP_NOT_EXISTS);
		}
		
		if(StringUtils.isEmpty(request.getPickupStatus()) || PickupStatus.findValueByType(request.getPickupStatus()) == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PICKUP_STATUS);
		}
		Boolean response = airportPickupServie.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_AIRPORT_PICKUP_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.AirportPickupParameters.ID, required=true)String id) throws AboutstaysException{
		if(!airportPickupServie.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_PICKUP_NOT_EXISTS);
		}
		Boolean response = airportPickupServie.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_PICKUP_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.APPROVE_STATUS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> approvePickupRequest(@RequestBody(required=true)ApprovePickupRequest request) throws AboutstaysException{
		logger.info(request.toString());
		ValidateUtils.validateApprovePickupRequest(request);
		if(!carService.existsById(request.getCarId())){
			throw new AboutstaysException(AboutstaysResponseCode.CAR_NOT_EXISTS);
		}
		if(!driverService.existsById(request.getDriverId())){
			throw new AboutstaysException(AboutstaysResponseCode.DRIVER_NOT_EXISTS);
		}
		if(StringUtils.isEmpty(request.getStatus()) || PickupStatus.findValueByType(request.getStatus()) == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PICKUP_STATUS);
		}
		Boolean response = airportPickupServie.approvePickupRequest(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.APPROVE_PICKUP_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.AirportPickupConstants.CANCEL, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> cancelPickup(@RequestBody(required=true)CanceRequestByUosID request) throws AboutstaysException{
		ValidateUtils.validateCancelRequest(request);
		UserOptedService optedService = uosService.getEntity(request.getUosId());
		if(optedService == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		String refId = optedService.getRefId();
		RefCollectionType refEnum = RefCollectionType.findValueByType(optedService.getRefCollectionType());
		if(refEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
		Boolean response = false;
		if(refEnum == RefCollectionType.AIRPORT_PICKUP){
			response = airportPickupServie.changeOrderStatus(refId, GeneralOrderStatus.CANCELLED);
		}
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_PICKUP_CANCEL_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
