package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddLateCheckoutRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.RetreiveLateCheckoutRequest;
import com.aboutstays.request.dto.UpdateLateCheckoutRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.LateCheckoutResposne;
import com.aboutstays.services.LateCheckoutRequestService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.LateCheckoutRequestConverter;
import com.aboutstays.utils.UserOptedServiceConverter;

@Controller
@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.BASE)
public class LateCheckoutRequestController extends BaseSessionController {
	
	@Autowired
	private LateCheckoutRequestService lcrService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private UserOptedServicesService uosService;
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddLateCheckoutRequest> getSample(){
		AddLateCheckoutRequest response = LateCheckoutRequestConverter.getSampleRequest();
		return new BaseApiResponse<AddLateCheckoutRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	public BaseApiResponse<String> add(@RequestBody(required=true)AddLateCheckoutRequest request, boolean skipValidations) throws AboutstaysException{
		if(!skipValidations){
			if(request == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_LATE_CHECKOUT_REQUEST);
			}
			baseSessionService.validateBaseSessionSessionData(request);
		}

		String response = lcrService.add(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_LATE_CHECKOUT_REQUEST_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSReqByLateCheckoutRequest(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_LATE_CHECKOUT_REQUEST_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddLateCheckoutRequest request) throws AboutstaysException{
		return add(request, false);
	}
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<LateCheckoutResposne>> getAll() throws AboutstaysException{
		List<LateCheckoutResposne> response = lcrService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_NOT_FOUND);
		}
		return new BaseApiResponse<List<LateCheckoutResposne>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LateCheckoutResposne> get(@RequestParam(value=RestMappingConstants.LateCheckoutRequestParameters.ID, required=true)String id) throws AboutstaysException{
		LateCheckoutResposne response = lcrService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_NOT_FOUND);
		}
		return new BaseApiResponse<LateCheckoutResposne>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.LateCheckoutRequestParameters.ID, required=true)String id) throws AboutstaysException{
		if(!lcrService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_NOT_FOUND);
		}
		Boolean response = lcrService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_REQUEST_DEL_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateLateCheckoutRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LATE_CHECKOUT_REQUEST_ID);
		}
		if(!lcrService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_NOT_FOUND);
		}
		baseSessionService.validateBaseSessionSessionData(request);

		Boolean response = lcrService.updateNonNUll(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_LATE_CHECKOUT_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.MODIFY, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> modify(@RequestBody(required=true)UpdateLateCheckoutRequest request) throws AboutstaysException{
		baseSessionService.validateBaseSessionSessionData(request);

		String response = lcrService.modifyLateCheckoutRequest(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_LATE_CHECKOUT_FAILED);
		}
		
		UserOptedService uos = uosService.getByRefId(response);
		if(uos == null) {
			AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSReqByModifyLateCheckoutRequest(request, response);
			UserOptedServiceController controller = new UserOptedServiceController();
			beanFactory.autowireBean(controller);
			controller.add(optedRequest);
		}

		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.LateCheckoutRequestConstants.RETREIVE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LateCheckoutResposne> modify(@RequestBody(required=true)RetreiveLateCheckoutRequest request) throws AboutstaysException{
		baseSessionService.validateBaseSessionSessionData(request);

		LateCheckoutResposne response = lcrService.retrieveLateCheckoutRequest(request.getUserId(), request.getHotelId(), request.getStaysId());
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.LATE_CHECKOUT_NOT_FOUND);
		}
		return new BaseApiResponse<LateCheckoutResposne>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

}
