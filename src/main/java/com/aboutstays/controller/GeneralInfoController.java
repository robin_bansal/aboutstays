package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.GeneralInformationType;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddGeneralInfoRequest;
import com.aboutstays.request.dto.UpdateGeneralInfoRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetGeneralInfoResponse;
import com.aboutstays.services.GeneralInfoService;
import com.aboutstays.utils.GeneralInfoConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.BASE)
public class GeneralInfoController {
	
	@Autowired
	private GeneralInfoService generalInfoService;
	
	@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddGeneralInfoRequest> getSampleRequest(){
		AddGeneralInfoRequest response = GeneralInfoConverter.getSample();
		return new BaseApiResponse<AddGeneralInfoRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddGeneralInfoRequest request) throws AboutstaysException{
		ValidateUtils.validateGeneralInfoRequest(request);
		GeneralInformationType typeEnum = GeneralInformationType.findGeneralInfoByType(request.getInfoType());
		StaysStatus statusEnum = StaysStatus.findStatusByCode(request.getStatus()); 
		if(typeEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GENERAL_INFORMATION_TYPE);
		}
		if(statusEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_STAY_STATUS);
		}
		String response = generalInfoService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_GENERAL_INFORMATION_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS); 
	}
	
	@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetGeneralInfoResponse>> getAllGeneralInfo(@RequestParam(value=RestMappingConstants.GeneralInfoParameters.STATUS, required=false)Integer status) throws AboutstaysException{
		List<GetGeneralInfoResponse> response = null;
		if(status != null){
			StaysStatus typeEnum = StaysStatus.findStatusByCode(status);
			if(typeEnum == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_STAY_STATUS);
			} else{
				response = generalInfoService.getAllByStaysStatus(status);
			}
		} else{
			response = generalInfoService.getAll();
		}
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GET_GENERAL_INFO_FAILED);
		}
		return new BaseApiResponse<List<GetGeneralInfoResponse>>(false, response, AboutstaysResponseCode.SUCCESS) ;
	}
	
	@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.DELTE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.GeneralInfoParameters.ID, required=true)String id) throws AboutstaysException{
		if(!generalInfoService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_INFORMATION_NOT_FOUND);
		}
		Boolean response = generalInfoService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_INFO_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetGeneralInfoResponse> get(@RequestParam(value=RestMappingConstants.GeneralInfoParameters.ID, required=true)String id) throws AboutstaysException{
		GetGeneralInfoResponse response = generalInfoService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_INFORMATION_NOT_FOUND);
		}
		return new BaseApiResponse<GetGeneralInfoResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.GeneralInfoConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateGeneralInfoRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GI_ID);
		}
		if(!generalInfoService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
		}
		Boolean response = generalInfoService.updateById(request);
		if(response == false){
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
