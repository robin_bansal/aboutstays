package com.aboutstays.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.Booking;
import com.aboutstays.entities.Stays;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddStaysRequest;
import com.aboutstays.response.dto.AddStaysResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetHotelResponse;
import com.aboutstays.response.dto.GetStaysResponse;
import com.aboutstays.response.dto.GetStaysWithHotelInfoResponse;
import com.aboutstays.response.dto.TotalNightsAndStaysResponse;
import com.aboutstays.services.BookingService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.StaysConverter;

@Controller
@RequestMapping(value=RestMappingConstants.StaysConstants.BASE)
public class StaysController {
	
	@Autowired
	private StaysService staysService;
	
	@Autowired
	private BookingService bookingService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired ValidateIdsService validateIdsService;
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddStaysRequest> sampleReqeust(){
		AddStaysRequest response = StaysConverter.getSampleRequest();
		return new BaseApiResponse<AddStaysRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddStaysResponse> addStays(@RequestBody(required=true)AddStaysRequest request) throws AboutstaysException{
		Booking booking = bookingService.getEntityById(request.getBookingId(), request.getHotelId());
		if(booking==null){
			throw new AboutstaysException(AboutstaysResponseCode.BOOKING_NOT_FOUND);
		}
		GetHotelResponse hotelResponse = hotelService.getHotelById(request.getHotelId());
		if(hotelResponse == null) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if(!userService.existsByUserId(request.getUserId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		Stays stays = staysService.getEntityById(booking.getDefaultStaysId());
		if(stays == null){
			String staysId = staysService.addStayForBooking(booking);
			bookingService.setDefaultStaysId(booking.getBookingId(), staysId);
			booking.setDefaultStaysId(staysId);
			stays = staysService.getEntityById(staysId);
		}
		if(stays.isAddedInApp()){
			if(request.getUserId().equals(stays.getUserId())){
				throw new AboutstaysException(AboutstaysResponseCode.SAME_STAY_ALREADY_ADDED);
			} else {
				throw new AboutstaysException(AboutstaysResponseCode.OTHER_USER_ALREADY_ADDED_STAY);
			}
		}
		staysService.setUserId(stays.getStaysId(),request.getUserId());
		
//		PartnershipType partnershipType = PartnershipType.findValueByCode(hotelResponse.getPartnershipType());
//		ValidateUtils.validateAddStaysRequest(request, partnershipType);
//		if(PartnershipType.PARTNER.equals(partnershipType)){
//			if((!bookingService.existsByBookingId(request.getBookingId()))){
//				throw new AboutstaysException(AboutstaysResponseCode.NO_BOOKINGS_EXISTS);
//			}
//		} 
		
		
		// get stays by booking id and stay id
//		List<Stays> listStays = staysService.getStaysOfBooking(request.getBookingId());
//		if(CollectionUtils.isNotEmpty(listStays)){
//			boolean stayAddedByOtherUser = false;
//			for(Stays stays : listStays){
//				if(!request.getUserId().equals(stays.getUserId())){
//					stayAddedByOtherUser = true;
////					if(StaysStatus.findStatusByCode(stays.getStaysStatus()) != StaysStatus.CHECK_IN){
////						// send an otp to stays.getUserId;
////						//return and let call another api with matched otp boolean
////					}
//				} else{
//					throw new AboutstaysException(AboutstaysResponseCode.SAME_STAY_ALREADY_ADDED);
//				}
//			}
//			if(stayAddedByOtherUser){
//				throw new AboutstaysException(AboutstaysResponseCode.OTHER_USER_ALREADY_ADDED_STAY);
//			}
//		}
		
//		if(staysService.existsByBookingId(request.getBookingId())){
//			throw new AboutstaysException(AboutstaysResponseCode.STAY_ALREADY_ADDED);
//		}
//		AddStaysResponse response = staysService.addStays(request);
//		if(response == null){
//			throw new AboutstaysException(AboutstaysResponseCode.ADD_STAYS_FAILED);
//		}
		AddStaysResponse response = new AddStaysResponse();
		response.setStaysId(stays.getStaysId());
		return new BaseApiResponse<AddStaysResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteStays(@RequestParam(value=RestMappingConstants.StaysParameters.STAY_ID, required=true)String staysId) throws AboutstaysException{
		if(StringUtils.isEmpty(staysId)){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		}
		if(!staysService.existsByStaysId(staysId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		Boolean response = staysService.deleteStays(staysId);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.DELETE_STAYS_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetStaysResponse> getAllStays(@RequestParam(value=RestMappingConstants.StaysParameters.USER_ID, required=false)String userId) throws AboutstaysException{
		GetStaysResponse response;
		if(StringUtils.isEmpty(userId)){
			response = staysService.getAllStays();
		} else{
			response = staysService.getStaysByUserId(userId);
		}
		
		if(response == null||CollectionUtils.isEmpty(response.getListStays())){
			throw new AboutstaysException(AboutstaysResponseCode.GET_STAYS_FAILED);
		}
		
		return new BaseApiResponse<GetStaysResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetStayResponse> getStayByStayId(@RequestParam(value=RestMappingConstants.StaysParameters.STAY_ID, required=true)String stayId) throws AboutstaysException{
		if(StringUtils.isEmpty(stayId)){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		}
		GetStayResponse response = staysService.getStayByStayId(stayId);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GET_STAYS_FAILED);
		}
		return new BaseApiResponse<GetStayResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.GET_STAYS_WITH_HOTEL_INFO, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetStaysWithHotelInfoResponse> getAllStaysWithHotelInfo(@RequestParam(value=RestMappingConstants.StaysParameters.USER_ID, required=false)String userId) throws AboutstaysException{
		GetStaysWithHotelInfoResponse response;
		if(StringUtils.isEmpty(userId)){
			response = staysService.getAllStaysWithHotelInfo();
		} else{
			response = staysService.getStaysWithHotelInfo(userId);
		}
		
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GET_STAYS_FAILED);
		}
		return new BaseApiResponse<GetStaysWithHotelInfoResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysConstants.GET_ALL_STAYS_OF_USER, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<TotalNightsAndStaysResponse> getAllStaysOfUser(@RequestParam(value=RestMappingConstants.StaysParameters.USER_ID, required=true)String userId) throws AboutstaysException{
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		TotalNightsAndStaysResponse response = staysService.getTotalNightsAndStays(userId);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_STAYS_OF_USER_FAILED);
		}
		if(CollectionUtils.isEmpty(response.getHotelStaysList()))
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		
		return new BaseApiResponse<TotalNightsAndStaysResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
}
