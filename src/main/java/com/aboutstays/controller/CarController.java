package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CarType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddCarRequest;
import com.aboutstays.request.dto.UpdateCarRequest;
import com.aboutstays.response.dto.AddCarResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetCarResponse;
import com.aboutstays.services.CarService;
import com.aboutstays.utils.CarConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CarConstants.BASE)
public class CarController {
	
	@Autowired
	private CarService carService;

	@RequestMapping(value=RestMappingConstants.CarConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddCarRequest> getSample(){
		AddCarRequest response = CarConverter.getSampleRequest();
		return new BaseApiResponse<AddCarRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CarConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddCarResponse> add(@RequestBody(required=true)AddCarRequest request) throws AboutstaysException{
		ValidateUtils.validateAddCarRequest(request);
		Integer typeValue = request.getType();
		if(typeValue != null){
			if(CarType.findValueByCode(typeValue) == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
			}
		} else{
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CAR_TYPE);
		}
		if(carService.existsByNumberPlate(request.getNumberPlate())){
			throw new AboutstaysException(AboutstaysResponseCode.NUMBER_PLATE_ALREADY_EXISTS);
		}
		
		AddCarResponse response = carService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_CAR_FAILED);
		}
		return new BaseApiResponse<AddCarResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CarConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCarResponse>> getAll() throws AboutstaysException{
		List<GetCarResponse> response = carService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_CARS_FAILED);
		}
		return new BaseApiResponse<List<GetCarResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CarConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.CarParameters.ID, required=true)String id) throws AboutstaysException{
		if(!carService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.CAR_NOT_EXISTS);
		}
		Boolean response = carService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.CAR_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CarConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetCarResponse> get(@RequestParam(value=RestMappingConstants.CarParameters.ID, required=true)String id) throws AboutstaysException{
		GetCarResponse response = carService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.CAR_NOT_EXISTS);
		}
		return new BaseApiResponse<GetCarResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CarConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateCarRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CAR_ID);
		}
		if(!carService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.CAR_NOT_EXISTS);
		}
		Boolean response = carService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_CAR_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
