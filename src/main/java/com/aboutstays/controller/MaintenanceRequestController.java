package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddMaintenanceOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.UpdateMaintenanceOrderRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetMaintenanceOrderResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.MaintenanceRequestService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.MaintenanceOrderConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.BASE)
public class MaintenanceRequestController extends BaseSessionController{
	
	@Autowired
	private MaintenanceRequestService mtRequestService;
	
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMaintenanceOrderRequest> getSample(){
		AddMaintenanceOrderRequest response = MaintenanceOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddMaintenanceOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	public BaseApiResponse<String> add(@RequestBody(required=true)AddMaintenanceOrderRequest request, boolean skipValidations) throws AboutstaysException{
		if(!skipValidations){
			ValidateUtils.validateAddMaintenanceOrderRequst(request);
			baseSessionService.validateBaseSessionSessionData(request);
		}
		
		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId()); 
		if(request.isRequestedNow()){
			request.setDeliveryTime(new Date().getTime()+timezoneDiff);
		} else {
			request.setDeliveryTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString())+timezoneDiff);
		}
		String response = mtRequestService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_MAINTENANCE_ORDER_FAILED);
		}
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestByMTOrderRequest(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_MAINTENANCE_ORDER_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddMaintenanceOrderRequest request) throws AboutstaysException{
		return add(request, false);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetMaintenanceOrderResponse>> getAll() throws AboutstaysException{
		List<GetMaintenanceOrderResponse> response = mtRequestService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<List<GetMaintenanceOrderResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMaintenanceOrderResponse> get(@RequestParam(value=RestMappingConstants.MaintenanceRequestParameters.ID, required=true)String id) throws AboutstaysException{
		GetMaintenanceOrderResponse response = mtRequestService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_ORDER_NOT_FOUND);
		}
		return new BaseApiResponse<GetMaintenanceOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.MaintenanceRequestParameters.ID, required=true)String id) throws AboutstaysException{
		if(!mtRequestService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_ORDER_NOT_FOUND);
		}
		Boolean response = mtRequestService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_ORDER_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MaintenanceRequestConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateMaintenanceOrderRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MT_ORDER_ID);
		}
		if(!mtRequestService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_ORDER_NOT_FOUND);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		Boolean response = mtRequestService.updateNonNullFieldsOfRequest(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_MT_ORDER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
