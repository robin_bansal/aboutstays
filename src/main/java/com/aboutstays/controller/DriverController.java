package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddDriverRequest;
import com.aboutstays.request.dto.UpdateDriverRequest;
import com.aboutstays.response.dto.AddDriverResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetDriverResponse;
import com.aboutstays.services.DriverService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DriverConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.DriverConstants.BASE)
public class DriverController {
	
	@Autowired
	private DriverService driverService;
	
	@RequestMapping(value=RestMappingConstants.DriverConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddDriverRequest> getSample(){
		AddDriverRequest response = DriverConverter.getSampleRequest();
		return new BaseApiResponse<AddDriverRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.DriverConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddDriverResponse> add(@RequestBody(required=true)AddDriverRequest request) throws AboutstaysException{
		ValidateUtils.validateAddDriverRequest(request);
		AddDriverResponse response = driverService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_DRIVER_FAILED);
		}
		return new BaseApiResponse<AddDriverResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.DriverConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetDriverResponse>> getAll() throws AboutstaysException{
		List<GetDriverResponse> response = driverService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_DRIVERS_FAILED);
		}
		return new BaseApiResponse<List<GetDriverResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.DriverConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.DriverParameters.ID, required=true)String id) throws AboutstaysException{
		if(!driverService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.DRIVER_NOT_EXISTS);
		}
		Boolean response = driverService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.DRIVER_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.DriverConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetDriverResponse> get(@RequestParam(value=RestMappingConstants.DriverParameters.ID, required=true)String id) throws AboutstaysException{
		GetDriverResponse response = driverService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.DRIVER_NOT_EXISTS);
		}
		return new BaseApiResponse<GetDriverResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.DriverConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateDriverRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DRIVER_ID);
		}
		if(!driverService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.DRIVER_NOT_EXISTS);
		}
		Boolean response = driverService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_DRIVER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
