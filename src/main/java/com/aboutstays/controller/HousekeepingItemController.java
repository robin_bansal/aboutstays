package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.BatchUpdateHkItemRequest;
import com.aboutstays.request.dto.HousekeepingItemRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.HousekeepingItemResponse;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.HousekeepingItemService;
import com.aboutstays.services.HousekeepingServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HousekeepingItemConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.BASE)
public class HousekeepingItemController {
	
	@Autowired
	private HousekeepingItemService hkItemService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private HousekeepingServicesService hkServicesService;
	
	@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<HousekeepingItemRequest> getSampleRequest() {
		HousekeepingItemRequest request = HousekeepingItemConverter.getSampleRequest();
		return new BaseApiResponse<HousekeepingItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addHKItem(@RequestBody(required=true)HousekeepingItemRequest request) throws AboutstaysException{
		ValidateUtils.validateAddHousekeepingItemRequest(request);
		if(HousekeepingItemType.getByCode(request.getType()) == null){
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_HOUSEKEEPING_ITEM_TYPE);
		}
		if(!hotelService.hotelExistsById(request.getHotelId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if(!hkServicesService.existsById(request.getHousekeepingServicesId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		}
		String itemId = hkItemService.save(request);
		if(StringUtils.isEmpty(itemId)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_HOUSEKEEPING_ITEM_FAILED);
		}
		return new BaseApiResponse<String>(false, itemId, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.ADD_ALL, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<String>>> addAllHKItems(
			@RequestBody(required = true) List<HousekeepingItemRequest> requestList) throws AboutstaysException {
		if (CollectionUtils.isEmpty(requestList)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_REQUEST_LIST);
		}
		List<BaseApiResponse<String>> responseList = new ArrayList<>();
		for (HousekeepingItemRequest request : requestList) {
			responseList.add(addHKItem(request));
		}
		return new BaseApiResponse<List<BaseApiResponse<String>>>(false, responseList,
				AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<HousekeepingItemResponse>> getAll() throws AboutstaysException {
		List<HousekeepingItemResponse> responseList = hkItemService.getAllHousekeepingItems();
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		return new BaseApiResponse<List<HousekeepingItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.GET_ALL_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<HousekeepingItemResponse>> getAllByHotelId(
			@RequestParam(value = RestMappingConstants.HousekeepingItemParameters.HOTEL_ID) String hotelId)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<HousekeepingItemResponse> responseList = hkItemService.getAllByHotelId(hotelId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		return new BaseApiResponse<List<HousekeepingItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.HousekeepingItemConstants.GET_ALL_BY_HK_SERVICE_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<HousekeepingItemResponse>> getAllByHKServicesId(
			@RequestParam(value = RestMappingConstants.HousekeepingItemParameters.HK_SERVICE_ID) String hkServicesId)
			throws AboutstaysException {
		if (!hkServicesService.existsById(hkServicesId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		}
		List<HousekeepingItemResponse> responseList = hkItemService.getAllByHousekeepingServiceId(hkServicesId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		return new BaseApiResponse<List<HousekeepingItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingItemConstants.FETCH_BY_TYPE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<HousekeepingItemResponse>> fetchAllItemsByType(@RequestParam(value = RestMappingConstants.HousekeepingItemParameters.HK_SERVICE_ID) 
			String hkServicesId, @RequestParam(value = RestMappingConstants.HousekeepingItemParameters.TYPE)Integer type) throws AboutstaysException{
		HousekeepingItemType hkItemType = HousekeepingItemType.getByCode(type);
		if(hkItemType==null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_HOUSEKEEPING_ITEM_TYPE);
		if(!hkServicesService.existsById(hkServicesId)){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_SERVICE_NOT_EXISTS);
		}
		List<HousekeepingItemResponse> responseList = hkItemService.getAllByType(hkServicesId,hkItemType);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		return new BaseApiResponse<List<HousekeepingItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingItemConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.HousekeepingItemParameters.ID, required=true)String id) throws AboutstaysException{
		if(!hkItemService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		}
		Boolean response = hkItemService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.HOUSEKEEPING_ITEM_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingItemConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateItem(@RequestBody HousekeepingItemRequest request) throws AboutstaysException{
		if(!hkItemService.existsById(request.getItemId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_HOUSEKEEPING_ITEMS_FOUND);
		}
		hkItemService.update(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.HOUSEKEEPING_ITEM_WITH_NAME_UPDATED_SUCCESSFULLY, request.getName()));
	}
	
	@RequestMapping(value=RestMappingConstants.HousekeepingItemConstants.BATCH_UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<BaseApiResponse<Boolean>>> batchUpdate(@RequestBody BatchUpdateHkItemRequest request){
		if(CollectionUtils.isNotEmpty(request.getItems())){
			List<BaseApiResponse<Boolean>> response = new ArrayList<>();
			boolean allUpdated = true;
			for(HousekeepingItemRequest item : request.getItems()){
				BaseApiResponse<Boolean> updateItemResponse = null;
				try {
					updateItemResponse = updateItem(item);
				} catch (AboutstaysException e) {
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, e.getCode(), false, e.getMessage());
				} catch (Exception e){
					allUpdated = false;
					updateItemResponse = new BaseApiResponse<Boolean>(false, false, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
				}
				response.add(updateItemResponse);
			}
			if(!allUpdated){
				return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(true, response, AboutstaysResponseCode.SOME_HK_ITEMS_NOT_UPDATED);
			}
			return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, response, AboutstaysResponseCode.ALL_HK_ITEMS_UPDATED);
		}
		return new BaseApiResponse<List<BaseApiResponse<Boolean>>>(false, null, AboutstaysResponseCode.SUCCESS);
	}
}
