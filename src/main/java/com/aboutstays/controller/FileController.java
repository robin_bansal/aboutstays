package com.aboutstays.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.services.AWSService;
import com.aboutstays.services.ProcessUtilityService;

@Controller
@RequestMapping(value=RestMappingConstants.FileConstants.BASE)
public class FileController {
	
	@Autowired
	private ProcessUtilityService processUtilityService;
	
	@Autowired
	private AWSService awsService;
	
	@RequestMapping(value=RestMappingConstants.FileConstants.IMAGE_UPLOAD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> uploadImageFile(@RequestParam(value=RestMappingConstants.FileParameters.FILE, required=true)MultipartFile imageFile) throws AboutstaysException{
		if(imageFile == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_FILE);
		}
		try {
				byte[] bytes = imageFile.getBytes();
				
				String catalinaHomePath = System.getProperty("catalina.home");
				File tmpDir = new File(catalinaHomePath + File.separator + "tmpFiles");
				if(!tmpDir.exists()){
					tmpDir.mkdirs();
				}
				File file = new File(tmpDir.getAbsolutePath() + File.separator + imageFile.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
				stream.write(bytes);
				stream.close();
				System.out.println("" + file.getAbsolutePath());
				String bucketName = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.BUCKET_NAME);
				String awsImageFilePath = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.IMAGE_FILE_PATH);
				String imageUrl = awsService.uploadToS3(bucketName, file, awsImageFilePath);
				if(StringUtils.isEmpty(imageUrl)){
					throw new AboutstaysException(AboutstaysResponseCode.INTERENAL_SERVER_ERROR);
				}
				file.delete();
				return new BaseApiResponse<String>(false, imageUrl, AboutstaysResponseCode.SUCCESS);
				
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode.INTERENAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value=RestMappingConstants.FileConstants.UPLOAD_MULTIPLE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Map<String, String>> uploadMultipleDocsToS3(@RequestParam(value=RestMappingConstants.FileParameters.KEY_NAMES)String[] names,
			@RequestParam(value=RestMappingConstants.FileParameters.KEY_FILES)MultipartFile[] files) throws AboutstaysException{
		if(names.length!=files.length){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_FILE_UPLOAD_REQUEST_SIZE);
		}
		Map<String, String> namesVsFileUrlsMap = new HashMap<>();
		String catalinaHomePath = System.getProperty("catalina.home");
		File tmpDir = new File(catalinaHomePath + File.separator + "tmpFiles");
		if(!tmpDir.exists()){
			tmpDir.mkdirs();
		}
		for(int i=0;i<names.length;i++){
			try {
				byte[] bytes = files[i].getBytes();
				File file = new File(tmpDir.getAbsolutePath() + File.separator + files[i].getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(file));
				stream.write(bytes);
				stream.close();
				System.out.println("" + file.getAbsolutePath());
				String bucketName = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.BUCKET_NAME);
				String awsImageFilePath = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.IMAGE_FILE_PATH);
				String imageUrl = awsService.uploadToS3(bucketName, file, awsImageFilePath);
				if(!StringUtils.isEmpty(imageUrl)){
					namesVsFileUrlsMap.put(names[i], imageUrl);
				}
			} catch (Exception e) {
				throw new AboutstaysException(AboutstaysResponseCode.INTERENAL_SERVER_ERROR);
			}
		}
		return new BaseApiResponse<Map<String, String>>(false, namesVsFileUrlsMap, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.FileConstants.UPLOAD_DOC, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> uploadDocs(@RequestParam(value=RestMappingConstants.FileParameters.DOC_FILE)MultipartFile docFile) throws AboutstaysException{
		if(docFile == null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_FILE); 
		}
		try {
			byte[] bytes = docFile.getBytes();		
			
			String catalinaHome = System.getProperty("catalina.home");
			File tmpDir = new File(catalinaHome + File.separator + "tmpFiles");
			if(!tmpDir.exists()){
				tmpDir.mkdirs();
			}
			File file = new File(tmpDir.getAbsolutePath() + File.separator + docFile.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(file));
			stream.write(bytes);
			stream.close();
			String bucketName = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.BUCKET_NAME);
			String awsDocsFilePath = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.DOCS_FILE_PATH);
			String docUrl = awsService.uploadToS3(bucketName, file, awsDocsFilePath);
			if(docUrl == null){
				throw new AboutstaysException(AboutstaysResponseCode.INTERENAL_SERVER_ERROR);
			}
			return new BaseApiResponse<String>(false, docUrl, AboutstaysResponseCode.SUCCESS);
			
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode.INTERENAL_SERVER_ERROR);
		}
	}
}
