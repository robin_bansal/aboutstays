package com.aboutstays.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CarType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.CarDetails;
import com.aboutstays.pojos.CarDetailsAndPrice;
import com.aboutstays.pojos.CommutePackage;
import com.aboutstays.request.dto.AddCommuteServiceRequest;
import com.aboutstays.request.dto.UpdateCommuteServiceRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetCommuteServiceResponse;
import com.aboutstays.services.CommuteServicesService;
import com.aboutstays.services.HotelService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.CommuteServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.BASE)
public class CommuteServiceController {
	
	@Autowired
	private CommuteServicesService csService;
	
	@Autowired
	private HotelService hotelService;
	
	@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddCommuteServiceRequest> getSample(){
		AddCommuteServiceRequest response = CommuteServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddCommuteServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddCommuteServiceRequest request) throws AboutstaysException{
		ValidateUtils.vaildateCommuteServiceRequest(request);
		if(!hotelService.hotelExistsById(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		List<CommutePackage> listPackages = request.getListCommutePackage();
		for(CommutePackage cPackage : listPackages){
			Iterator<CarDetailsAndPrice> iterator = cPackage.getCarDetailsAndPriceList().iterator();
			Set<Integer> carTypeSet = new HashSet<>();
			while(iterator.hasNext()){
				CarDetailsAndPrice carDetailsAndPrice = iterator.next();
				if(carDetailsAndPrice != null && carDetailsAndPrice.getCarDetails() != null){
					CarDetails carDetails = carDetailsAndPrice.getCarDetails();
					CarType carType = CarType.findValueByCode(carDetails.getType());
					if(carType == null){
						throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
					} else if(carTypeSet.contains(carDetails.getType())){
						iterator.remove();
					} else{
						carTypeSet.add(carDetails.getType());
					}
					carDetails.setName(carType.getMessage());
					carDetails.setImageUrl(carType.getImageUrl());
				}
			}
		}

		String response = csService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADDING_COMMUTE_SERVICE_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCommuteServiceResponse>> getAll() throws AboutstaysException{
		List<GetCommuteServiceResponse> response = csService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_COMMUTE_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetCommuteServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.CommuteServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!csService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_SERVICE_NOT_EXISTS);
		}
		Boolean response = csService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_SERVICE_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetCommuteServiceResponse> get(@RequestParam(value=RestMappingConstants.CommuteServiceParameters.ID, required=true)String id) throws AboutstaysException{
		GetCommuteServiceResponse response = csService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetCommuteServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CommuteServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateCommuteServiceRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_COMMUTE_SERVICE_ID);
		}
		if(!csService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.COMMUTE_SERVICE_NOT_EXISTS);
		}
		Boolean response = csService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_COMMUTE_SERVICE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}

}
