package com.aboutstays.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;
import com.aboutstays.request.dto.AddBellBoyOrderRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetBellBoyOrderResponse;
import com.aboutstays.services.BellBoyOrderService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.utils.BellBoyOrderConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.BellBoyOrderConstants.BASE)
public class BellBoyOrderController extends BaseSessionController{
	
	@Autowired
	private BellBoyOrderService bellboyOrderService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
		
	@Autowired
	private UserOptedServicesService uosService;
	
	@RequestMapping(value=RestMappingConstants.BellBoyOrderConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddBellBoyOrderRequest> getSample(){
		AddBellBoyOrderRequest response = BellBoyOrderConverter.getSampleRequest();
		return new BaseApiResponse<AddBellBoyOrderRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.BellBoyOrderConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddBellBoyOrderRequest request) throws AboutstaysException{
		logger.info(request.toString());
		ValidateUtils.validateBellBoyOrderRequest(request);
		baseSessionService.validateBaseSessionSessionData(request);
		long timezoneDiff = hotelService.getTimezoneDifferenceInMillis(request.getHotelId()); 
		if(request.isRequestedNow()){
			request.setDeliveryTime(new Date().getTime()+timezoneDiff);
		} else {
			request.setDeliveryTime(DateUtil.convertStringToLong(DateUtil.BASE_DATE_TIME_FORMAT, request.getDeliverTimeString()));
		}
		
		String response = bellboyOrderService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_BELL_BOY_ORDER_FAILED);
		}
	
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestByBellBoyOrder(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_BELL_BOY_ORDER_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.BellBoyOrderConstants.GET, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetBellBoyOrderResponse> getRequestedBellBoy(@RequestBody(required=true)UserStaysHotelGSIdsPojo request) throws AboutstaysException{
		logger.info(request.toString());
		validateIdsService.validateUserHotelStaysGsIds(request);		
		GetBellBoyOrderResponse response = new GetBellBoyOrderResponse();
		List<UserOptedService> listOptedServices = uosService.getByUserStaysAndHotel(request.getStaysId(), request.getUserId(), request.getHotelId());
		if(CollectionUtils.isNotEmpty(listOptedServices)){
			for(UserOptedService uos : listOptedServices){
				if(uos == null){
					continue;
				}
				if(uos.getGsId().equals(request.getGsId())){
					String refId = uos.getRefId();
					BellBoyOrder bellboyOrder = bellboyOrderService.getEntity(refId);
					if(bellboyOrder == null){
						continue;
					}
					long currentTime = new Date().getTime();
					long deliveryTime = bellboyOrder.getPreferredTime();
					if(deliveryTime <= currentTime){
						bellboyOrderService.changeOrderStatus(refId, GeneralOrderStatus.PROCESSING);
						bellboyOrder = bellboyOrderService.getEntity(refId);
					}
					
					response.setStatus(bellboyOrder.getStatus());
					response.setId(bellboyOrder.getId());
					response.setDeliveryTime(bellboyOrder.getPreferredTime());
					response.setGsId(bellboyOrder.getGsId());
					response.setHotelId(bellboyOrder.getHotelId());
					response.setStaysId(bellboyOrder.getStaysId());
					response.setUserId(bellboyOrder.getUserId());
					String created = DateUtil.format(new Date(bellboyOrder.getCreated()), DateUtil.BASE_TIME_FORMAT);
					String preferredTime = DateUtil.format(new Date(bellboyOrder.getPreferredTime()), DateUtil.BASE_TIME_FORMAT);
					GeneralOrderStatus status = GeneralOrderStatus.findByCode(bellboyOrder.getStatus());
					String displayMsg = "";
					if(status == GeneralOrderStatus.CANCELLED || status == GeneralOrderStatus.COMPLETED){
						displayMsg = "Your last request has been over. You can request for a bell boy now.";
					} else if(status == GeneralOrderStatus.SCHEDULED){
						displayMsg = "You have requested for a bell boy at " + created + "scheduled for " + preferredTime;
					}
					response.setDisplayMsg(displayMsg);
				}
			}
		}
		return new BaseApiResponse<GetBellBoyOrderResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

}
