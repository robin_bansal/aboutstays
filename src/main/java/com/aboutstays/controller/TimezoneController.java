package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.TimeZoneDetails;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.services.TimezoneService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.TimezoneConstants.BASE)
public class TimezoneController {

	@Autowired
	private TimezoneService timezoneService;
	
	@RequestMapping(value=RestMappingConstants.TimezoneConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addTimezone(@RequestBody(required=true)TimeZoneDetails request) throws AboutstaysException{
		ValidateUtils.validateAddTimezone(request);
		if(timezoneService.existsByTimezone(request.getTimezone())){
			throw new AboutstaysException(AboutstaysResponseCode.TIMEZONE_ALREADY_EXISTS);
		}
		String id = timezoneService.saveTimezone(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.TIMEZONE_DETAILS_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.TimezoneConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<TimeZoneDetails>> getAll() throws AboutstaysException{
		List<TimeZoneDetails> responseList = timezoneService.getAll();
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_TIMEZONE);
		return new BaseApiResponse<List<TimeZoneDetails>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.TimezoneConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteTimezone(@RequestParam(value=RestMappingConstants.TimezoneConstants.KEY_ID)String id) throws AboutstaysException{
		if(!timezoneService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.TIMEZONE_NOT_FOUND);
		timezoneService.deleteTimezone(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.TIMEZONE_DETAILS_DELETED_SUCCESSFULLT);
	}
	
}
