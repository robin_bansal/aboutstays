package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.EntertaimentItemTypeInfo;
import com.aboutstays.request.dto.AddEntertaimentServiceRequest;
import com.aboutstays.request.dto.AddEntertaimentTypeRequest;
import com.aboutstays.request.dto.DeleteEntertaimentServiceRequest;
import com.aboutstays.request.dto.DeleteEntertaimentTypeRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetEntertaimentServiceResponse;
import com.aboutstays.services.EntertaimentServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.EntertaimentServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.BASE)
public class EntertaimentServiceController {

	@Autowired
	private EntertaimentServicesService entertaimentServicesService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddEntertaimentServiceRequest> getSampleRequest(){
		AddEntertaimentServiceRequest request = EntertaimentServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddEntertaimentServiceRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addEntertaimentService(@RequestBody(required=true)AddEntertaimentServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddEntertaimentServiceRequest(request);
		validateIdsService.validateHotelId(request.getServiceId());
		for(EntertaimentItemTypeInfo typeInfo : request.getTypeInfoList()){
			validateIdsService.validateMasterGenereId(typeInfo.getTypeId());
		}
		String id = entertaimentServicesService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.ENTERTAIMENT_SERVICE_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.ADD_TYPE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addTypeToEntertaimentService(@RequestBody(required=true)AddEntertaimentTypeRequest request) throws AboutstaysException{
		//Validations done in service below already
		entertaimentServicesService.addType(request.getServiceId(), request.getTypeInfo());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ENTERTAIMENT_SERVICE_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.REMOVE_TYPE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> removeTypeFromEntertaimentService(@RequestBody(required=true)DeleteEntertaimentTypeRequest request) throws AboutstaysException{
		//Validations done in service below already
		entertaimentServicesService.removeType(request.getServiceId(), request.getTypeInfo());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ENTERTAIMENT_SERVICE_UPDATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.DELETE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteEntertaimentService(@RequestBody(required=true)DeleteEntertaimentServiceRequest request) throws AboutstaysException{
		if(!entertaimentServicesService.existsById(request.getServiceId()))
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_SERVICE_NOT_FOUND);
		entertaimentServicesService.delete(request.getServiceId());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ENTERTAIMENT_SERVICE_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetEntertaimentServiceResponse> getEntertaimentService(@RequestParam(value=RestMappingConstants.EntertaimentServiceParameters.ID)String id) throws AboutstaysException{
		GetEntertaimentServiceResponse response = entertaimentServicesService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_SERVICE_NOT_FOUND);
		return new BaseApiResponse<GetEntertaimentServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.EntertaimentServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetEntertaimentServiceResponse>> getAllEntertaimentServices() throws AboutstaysException{
		List<GetEntertaimentServiceResponse> response = entertaimentServicesService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ENTERTAIMENT_SERVICES);
		}
		return new BaseApiResponse<List<GetEntertaimentServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
