package com.aboutstays.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.User;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.LoginType;
import com.aboutstays.enums.SignupType;
import com.aboutstays.enums.Type;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.DeleteIdentityDocRequest;
import com.aboutstays.request.dto.ForgotPasswordRequest;
import com.aboutstays.request.dto.GetStaysForBookingsRequest;
import com.aboutstays.request.dto.LoginRequest;
import com.aboutstays.request.dto.SignupRequest;
import com.aboutstays.request.dto.UpdateContactDetails;
import com.aboutstays.request.dto.UpdatePasswordRequest;
import com.aboutstays.request.dto.UpdatePersonalDetails;
import com.aboutstays.request.dto.UpdateStayPrefsRequest;
import com.aboutstays.request.dto.UpdateUserRequest;
import com.aboutstays.request.dto.ValidateOtpRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetBookedStaysResponse;
import com.aboutstays.response.dto.GetUserResponse;
import com.aboutstays.response.dto.LoginResponse;
import com.aboutstays.response.dto.OtpResponse;
import com.aboutstays.response.dto.SignupResponse;
import com.aboutstays.services.ProcessUtilityService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.UserConverter;
import com.aboutstays.utils.ValidateUtils;


@Controller
@RequestMapping(value=RestMappingConstants.UserConstants.BASE)
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private StaysService staysService;
	
	@Autowired
	private ProcessUtilityService processUtilityService;
	
	@RequestMapping(value=RestMappingConstants.UserConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<SignupRequest> sample(){
		SignupRequest request = UserConverter.getSampleRequest();
		return new BaseApiResponse<SignupRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.VALIDATE_SIGNUP, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> validateManualSignup(@RequestBody(required=true)SignupRequest signupReqeust) throws AboutstaysException{
		ValidateUtils.validateSignupRequest(signupReqeust);
		if(userService.existsByEmail(signupReqeust.getEmailId()))
			throw new AboutstaysException(AboutstaysResponseCode.DUPLICATE_EMAIL);
		if(userService.existsByNumber(signupReqeust.getNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.DUPLICATE_CONTACT);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.SUCCESS_SIGNUP_VALIDATION);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.SIGNUP, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<SignupResponse> signupUser(@RequestBody(required=true)SignupRequest signupRequest) throws AboutstaysException{
		SignupType typeEnum;
		Integer type = signupRequest.getSignupType();
		if(type!=null){
			typeEnum = SignupType.findSignupTypeByCode(type);
			if(typeEnum == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_SIGNUP_TYPE);
			}
		} else{
			signupRequest.setSignupType(SignupType.getDefaultValue());
		}
		ValidateUtils.validateSignupRequest(signupRequest);

		UserPojo userPojo = userService.getByEmailOrPhone(signupRequest.getEmailId(), signupRequest.getNumber());
		if (userPojo != null) {
			if (userPojo.getEmailId().equalsIgnoreCase(signupRequest.getEmailId())) {
				throw new AboutstaysException(AboutstaysResponseCode.DUPLICATE_EMAIL);
			} else if (userPojo.getNumber().equals(signupRequest.getNumber())) {
				throw new AboutstaysException(AboutstaysResponseCode.DUPLICATE_CONTACT);
			}
		}
		SignupResponse response = userService.signupUser(signupRequest);
		return new BaseApiResponse<SignupResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetUserResponse> getUserById(@RequestParam(value=RestMappingConstants.UserParameters.USER_ID)String userId) throws AboutstaysException{
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		User user = userService.getUserEntity(userId);
		GetUserResponse	response = UserConverter.convertUserToGetUser(user);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ACCOUNT);
		}
		return new BaseApiResponse<GetUserResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.LOGIN, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<LoginResponse> loginRespoonse(@RequestBody(required=true)LoginRequest loginRequest) throws AboutstaysException{
		Type typeEnum = Type.findTypeByCode(loginRequest.getType());
		ValidateUtils.validateTypeValue(typeEnum, loginRequest.getValue());  
		ValidateUtils.validateLoginRequest(loginRequest);
		Integer loginType = loginRequest.getLoginType();
		LoginType loginTypeEnum;
		if(loginType != null){
			loginTypeEnum = LoginType.findLoginTypeByCode(loginType);
			if(loginTypeEnum == null){
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_LOGIN_TYPE);
			}
		} else{
			loginTypeEnum = LoginType.getDefaultValue();
		}
		
		LoginResponse loginResponse = userService.matchCredentials(typeEnum, loginRequest.getValue());
		if(loginResponse == null){
			if(loginRequest.getSignupRequest() != null){
				SignupType type = SignupType.findSignupTypeByCode(loginRequest.getSignupRequest().getSignupType());
				if(type == SignupType.FACEBOOK || type == SignupType.GMAIL){
					if(!StringUtils.isEmpty(loginRequest.getSignupRequest().getNumber()) && 
							userService.existsByNumber(loginRequest.getSignupRequest().getNumber())){
						loginRequest.getSignupRequest().setNumber(loginRequest.getValue());
					}
					BaseApiResponse<SignupResponse> signUpResponse= signupUser(loginRequest.getSignupRequest());	
					if(signUpResponse!=null){
						if(!signUpResponse.isError()){
							loginResponse = userService.matchCredentials(typeEnum, loginRequest.getValue());
						}
					} else {
						throw new AboutstaysException(AboutstaysResponseCode.INTERENAL_SERVER_ERROR);
					}
				} 
			}  else{
				throw new AboutstaysException(AboutstaysResponseCode.NO_ACCOUNT);
			}
		} else if(!loginRequest.getPassword().equals(loginResponse.getPassword())){
			switch(loginTypeEnum){
			case MANUAL:
				throw new AboutstaysException(AboutstaysResponseCode.INV_PASS);
			case FACEBOOK:
				break;
			case GMAIL:
				break;
			}
		}
		String token = "123456"; // get from the token service;
		loginResponse.setToken(token);
		loginResponse.setPassword(null);
		return new BaseApiResponse<LoginResponse>(false, loginResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.GENERATE_OTP, method=RequestMethod.GET)
	@ResponseBody
	public BaseApiResponse<OtpResponse> otpRespnse(@RequestParam(value=RestMappingConstants.UserParameters.TYPE,required=true)Integer type,
			@RequestParam(value=RestMappingConstants.UserParameters.VALUE, required=true)String value) throws AboutstaysException{
		Type typeEnum = Type.findTypeByCode(type);
		ValidateUtils.validateTypeValue(typeEnum, value);
		OtpResponse otpResponse = new OtpResponse();
		boolean debugMode = processUtilityService.getBooleanProcessProperty(PropertyConstant.PROCESS_MESSAGE, PropertyConstant.PROPERT_DEBUG_MODE, true);
		if(Type.EMAIL.equals(typeEnum)){
			if(!userService.existsByEmail(value)){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER_WITH_EMAIL);
			}
			otpResponse.setOtp("1234");  // Generate otp by service and send to emailId			
		} else if(Type.PHONE.equals(typeEnum)){
			if(!userService.existsByNumber(value)){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER_WITH_PHONE);
			}
			otpResponse.setOtp("1234");   // Generate otp by service and send to phone
		}
		otpResponse.setDebugMode(debugMode);
		return new BaseApiResponse<OtpResponse>(false, otpResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.UserConstants.OTP_VALIDATED, method=RequestMethod.PATCH)
	@ResponseBody
	public BaseApiResponse<String> otpValidated(@RequestBody(required=true)ValidateOtpRequest validateOtpRequest) throws AboutstaysException{
		ValidateUtils.validateOtp(validateOtpRequest);
		if(userService.updateOtpByEmailOrPhone(validateOtpRequest) == false){
			throw new AboutstaysException(AboutstaysResponseCode.VERIFICATION_FAILED);
		}
		return new BaseApiResponse<String>(false, "Otp verified", AboutstaysResponseCode.SUCCESS);
	}
	

	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_BY_TYPE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateUser(@RequestBody(required=true)UpdateUserRequest updateUserRequest) throws AboutstaysException{
		ValidateUtils.validateUpdateUserRequest(updateUserRequest);
		Type typeEnum = Type.findTypeByCode(updateUserRequest.getType());
		ValidateUtils.validateTypeValue(typeEnum, updateUserRequest.getValue());
		switch(typeEnum){
		case PHONE:
			if(userService.existsByNumber(updateUserRequest.getValue()) == false){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
			}	
			break;
		case EMAIL:
			if(userService.existsByEmail(updateUserRequest.getValue()) == false){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
			}
			break;
		case USERID:
			if(userService.existsByUserId(updateUserRequest.getValue()) == false){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
			}
			break;
		default:
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REQUEST);
		}
		
		Boolean response = userService.updateUserByType(updateUserRequest, typeEnum, updateUserRequest.getValue());
		if(response == false)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REQUEST);
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.UPDATED);	
	}
	
	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_PASSWORD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updatePassword(@RequestBody(required=true)UpdatePasswordRequest updatePasswordRequest) throws AboutstaysException{
		Type typeEnum = Type.findTypeByCode(updatePasswordRequest.getType());
		ValidateUtils.validateTypeValue(typeEnum, updatePasswordRequest.getValue());
		ValidateUtils.ValidateUpdatePasswordRequest(updatePasswordRequest);
		switch(typeEnum){
		case PHONE:
			if(userService.existsByNumber(updatePasswordRequest.getValue()) == false){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
			}	
			break;
		case EMAIL:
			if(userService.existsByEmail(updatePasswordRequest.getValue()) == false){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
			}
			break;
		case USERID:
			if(userService.existsByUserId(updatePasswordRequest.getValue()) == false){
				throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
			}
			break;
		default:
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REQUEST);
		}	
		
		Boolean response = userService.updatePasswordByType(updatePasswordRequest, typeEnum, updatePasswordRequest.getValue());
		if(response == false)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REQUEST);
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.UPDATED);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.FORGOT_PASSWORD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> forgotPassword(@RequestBody(required=true)ForgotPasswordRequest forgotPasswordRequest) throws AboutstaysException{
		Type typeEnum = Type.findTypeByCode(forgotPasswordRequest.getType());
		ValidateUtils.validateTypeValue(typeEnum, forgotPasswordRequest.getValue());
		Boolean response = null;
		switch(typeEnum){
		case PHONE:
			response = userService.existsByNumber(forgotPasswordRequest.getValue());
			break;
		case EMAIL:
			response = userService.existsByEmail(forgotPasswordRequest.getValue());
			break;
		}
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_ACCOUNT);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);	
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_PERSONAL_DETAILS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updatePersonalDetails(@RequestBody(required=true)UpdatePersonalDetails request) throws AboutstaysException{
		ValidateUtils.validateUserId(request.getUserId());
		ValidateUtils.validatePersonalDetails(request);
		if(!userService.existsByUserId(request.getUserId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		Boolean response = userService.updatePersonalDetails(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_USER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_CONTACT_DETAILS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateContactDetails(@RequestBody(required=true)UpdateContactDetails request) throws AboutstaysException{
		ValidateUtils.validateUserId(request.getUserId());
		if(!userService.existsByUserId(request.getUserId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		Boolean response = userService.updateContactDetails(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_USER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_STAY_PREFS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateStayPreferences(@RequestBody(required=true)UpdateStayPrefsRequest request) throws AboutstaysException{
		ValidateUtils.validateUserId(request.getUserId());
		if(!userService.existsByUserId(request.getUserId())){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		Boolean response = userService.updateStayPrefs(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_USER_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.DELETE_IDENTITY_DOC, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteIdentityDoc(@RequestBody(required=true)DeleteIdentityDocRequest request) throws AboutstaysException{
		ValidateUtils.validateDeleteIdentityDocRequest(request);
		String userId = request.getUserId();
		User user = userService.getUserEntity(userId);
		if(user==null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<IdentityDoc> identityDocList = user.getListIdentityDocs();
		if(CollectionUtils.isEmpty(identityDocList)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_DOCS_FOUND);
		}
		IdentityDoc identityDocToDelete = request.getIdentityDoc();
		boolean docFound = false;
		Iterator<IdentityDoc> iterator = identityDocList.iterator();
		while(iterator.hasNext()){
			IdentityDoc identityDoc = iterator.next();
			if(identityDocToDelete.equals(identityDoc)){
				docFound = true;
				iterator.remove();
				break;
			}
		}
		if(!docFound){
			throw new AboutstaysException(AboutstaysResponseCode.NO_DOCS_FOUND);
		}
		Boolean response = userService.updateDocs(identityDocList, userId);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.DOCUMENTS_UPDATION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.DOCUMENT_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_IDENTITY_DOCS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateIdentityDocs(@RequestParam(value=RestMappingConstants.UserParameters.NAME_ON_DOCUMENT, required=true)String nameOnDoc,
			@RequestParam(value=RestMappingConstants.UserParameters.DOCUMENT_NUMBER, required=true)String docNumber,
			@RequestParam(value=RestMappingConstants.UserParameters.OVERWRITE)boolean overwrite,
			@RequestParam(value=RestMappingConstants.UserParameters.DOCUMENT_TYPE, required=true)String docType,
			@RequestParam(value=RestMappingConstants.UserParameters.SCANNED_COPY, required=true)MultipartFile scannedCopy,
			@RequestParam(value=RestMappingConstants.UserParameters.USER_ID, required=true)String userId) throws AboutstaysException{
		//DocumentType documentType = DocumentType.findStatusByCode(docType); 
//		if(documentType == null){
//			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DOCUMENT_TYPE);
//		}
		User user = userService.getUserEntity(userId);
		if(user==null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		List<IdentityDoc> identityDocList = user.getListIdentityDocs();
		if(CollectionUtils.isNotEmpty(identityDocList)){
			if(!overwrite){
				for(IdentityDoc doc : user.getListIdentityDocs()){
//					if(documentType.getCode().equals(doc.getDocType())){
//						throw new AboutstaysException(AboutstaysResponseCode.DOCUMENT_TYPE_EXISTS);
//					}
				}
			} else if(overwrite){
				Iterator<IdentityDoc> iterator = identityDocList.iterator();
				while(iterator.hasNext()){
					IdentityDoc doc = iterator.next();
					if(doc.getDocType().equalsIgnoreCase(docType)) {
						iterator.remove();
						break;
					}
//					if(documentType.getCode().equals(doc.getDocType())){
//						iterator.remove();
//						break;
//					}
				}
			}
		} else {
			identityDocList = new ArrayList<>();
		}

		
		FileController fileController = new FileController();
		beanFactory.autowireBean(fileController);
		BaseApiResponse<String> fileResponse = fileController.uploadDocs(scannedCopy);
		String docUrl = null;
		if(fileResponse!=null&&!fileResponse.isError()){
			docUrl = fileResponse.getData();
		} else {
			throw new AboutstaysException(AboutstaysResponseCode.DOCUMENT_UPLOAD_FAILED);
		}
		
		//identityDocList = new ArrayList<>();
		IdentityDoc identityDoc = new IdentityDoc();
		identityDoc.setDocNumber(docNumber);
		identityDoc.setDocType(docType);
		identityDoc.setDocUrl(docUrl);
		identityDoc.setNameOnDoc(nameOnDoc);
		identityDocList.add(identityDoc);
		user.setListIdentityDocs(identityDocList);
		Boolean response = userService.updateDocs(identityDocList, userId);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.DOCUMENTS_UPDATION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.UPDATE_PROFILE_PICTURE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateProfilePicture(@RequestParam(value=RestMappingConstants.UserParameters.DISPLAY_PICTURE)MultipartFile displayPicture,
			@RequestParam(value=RestMappingConstants.UserParameters.USER_ID)String userId) throws AboutstaysException{
		ValidateUtils.validateUserId(userId);
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		FileController fileController = new FileController();
		beanFactory.autowireBean(fileController);
		BaseApiResponse<String> fileResponse = fileController.uploadImageFile(displayPicture);
		String imageUrl = null;
		if(fileResponse !=  null && !fileResponse.isError()){
			imageUrl = fileResponse.getData();
		} else{
			throw new AboutstaysException(AboutstaysResponseCode.IMAGE_UPLOAD_FAILED);
		}
		
		Boolean response = userService.updateProfilePicture(userId, imageUrl);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.IMAGE_UPDATE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS); 
	}
	
	@RequestMapping(value=RestMappingConstants.UserConstants.CHECK_EXISTING_STAYS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetBookedStaysResponse> checkExistingStaysByBookingIds(@RequestBody(required=true)GetStaysForBookingsRequest request) throws AboutstaysException{
		ValidateUtils.validateCheckExistingStaysByBookingIds(request);
		if(!userService.existsByUserId(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		Map<String, Boolean> bookingIdsStaysExistingStatus = new HashMap<>();
		for(String bookingId : request.getBookingIds()){
			boolean staysExists = staysService.existsByUserIdAndBookingId(request.getUserId(), bookingId);
			bookingIdsStaysExistingStatus.put(bookingId, staysExists);
		}
		GetBookedStaysResponse getBookedStaysResponse = new GetBookedStaysResponse(bookingIdsStaysExistingStatus);
		return new BaseApiResponse<GetBookedStaysResponse>(false, getBookedStaysResponse, AboutstaysResponseCode.SUCCESS);
	}

}



