package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddAllMasterCitiesRequest;
import com.aboutstays.request.dto.AddMasterCityRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetAllMasterCityResponse;
import com.aboutstays.response.dto.GetMasterCityResponse;
import com.aboutstays.services.MasterCityService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MasterCityConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
public class MasterCityController {

	@Autowired
	private MasterCityService masterCityService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddMasterCityRequest> getSampleRequest(){
		AddMasterCityRequest request = MasterCityConverter.getSampleRequest();
		return new BaseApiResponse<AddMasterCityRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterCityConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addMasterCity(@RequestBody(required=true)AddMasterCityRequest request) throws AboutstaysException{
		ValidateUtils.validateAddMasterCityRequest(request);
		if(masterCityService.existsByName(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.CITY_BY_NAME_ALREADY_EXISTS, request.getName()));
		String id = masterCityService.save(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.CITY_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterCityConstants.ADD_ALL, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> addAllMasterCities(@RequestBody(required=true)AddAllMasterCitiesRequest request) throws AboutstaysException{
		ValidateUtils.validateAddAllMasterCitiesRequest(request);
		for(AddMasterCityRequest addRequest : request.getMasterCities()){
			if(masterCityService.existsByName(addRequest.getName()))
				throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.CITY_BY_NAME_ALREADY_EXISTS, addRequest.getName()));
		}
		masterCityService.saveAll(request.getMasterCities());
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.ALL_CITIES_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterCityConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteMasterCity(@RequestParam(value=RestMappingConstants.MasterCityParameters.ID)String id) throws AboutstaysException{
		if(masterCityService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.CITY_NOT_FOUND);
		masterCityService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CITY_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterCityConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetAllMasterCityResponse> getAllMasterCities() throws AboutstaysException{
		List<GetMasterCityResponse> getMasterCityResponseList = masterCityService.getAll();
		if(CollectionUtils.isEmpty(getMasterCityResponseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CITIES_FOUND);
		GetAllMasterCityResponse response = new GetAllMasterCityResponse();
		response.setCities(getMasterCityResponseList);
		return new BaseApiResponse<GetAllMasterCityResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.MasterCityConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetMasterCityResponse> addMasterCity(@RequestParam(value=RestMappingConstants.MasterCityParameters.ID)String id) throws AboutstaysException{
		GetMasterCityResponse response = masterCityService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.CITY_NOT_FOUND);
		return new BaseApiResponse<GetMasterCityResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
