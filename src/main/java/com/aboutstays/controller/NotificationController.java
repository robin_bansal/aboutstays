package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddNotificationRequest;
import com.aboutstays.request.dto.GetNotificationListResponse;
import com.aboutstays.request.dto.GetStayNotificationsRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetNotificationResponse;
import com.aboutstays.services.NotificationService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.NotificationConverter;
import com.aboutstays.utils.ValidateUtils;


@Controller
@RequestMapping(value=RestMappingConstants.NotificationConstants.BASE)
public class NotificationController {

	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddNotificationRequest> getSampleRequest(){
		AddNotificationRequest request = NotificationConverter.getSampleRequest();
		return new BaseApiResponse<AddNotificationRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.NotificationConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addNotification(@RequestBody(required=true)AddNotificationRequest request) throws AboutstaysException{
		ValidateUtils.validateAddNotificationRequest(request);
		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
		String id = notificationService.addNotification(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.NOTIFICATION_ADDED_SUCCESSFULLY);
	}

	@RequestMapping(value=RestMappingConstants.NotificationConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetNotificationResponse> getNotificationById(@RequestParam(value=RestMappingConstants.NotificationParameters.ID)String id) throws AboutstaysException{
		GetNotificationResponse response = notificationService.getNotificationById(id);
		if(response==null){
			throw new AboutstaysException(AboutstaysResponseCode.NOTIFICATION_NOT_FOUND);
		}
		return new BaseApiResponse<GetNotificationResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.NotificationConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetNotificationListResponse> getAllNotifications() throws AboutstaysException{
		List<GetNotificationResponse> notifications = notificationService.getAllNotifications();
		if(CollectionUtils.isEmpty(notifications)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_NOTIFICATIONS_FOUND);
		}
		GetNotificationListResponse response = new GetNotificationListResponse();
		response.setNotificationsList(notifications);
		return new BaseApiResponse<GetNotificationListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.NotificationConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteNotification(@RequestParam(value=RestMappingConstants.NotificationParameters.ID)String id) throws AboutstaysException{
		if(!notificationService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.NOTIFICATION_NOT_FOUND);
		notificationService.deleteNotification(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.NOTIFICATION_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.NotificationConstants.GET_STAY_NOTIFICATIONS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetNotificationListResponse> getAllNotificationsForStay(@RequestBody(required=true)GetStayNotificationsRequest request) throws AboutstaysException{
		ValidateUtils.validateGetStayNotificationsRequest(request);
		validateIdsService.validateUserHotelStaysIds(request.getUserId(), request.getHotelId(), request.getStaysId());
		List<GetNotificationResponse> notifications = notificationService.getAllNotifications(request.getUserId(), request.getStaysId(), request.getHotelId());
		if(CollectionUtils.isEmpty(notifications)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_NOTIFICATIONS_FOUND);
		}
		GetNotificationListResponse response = new GetNotificationListResponse();
		response.setNotificationsList(notifications);
		return new BaseApiResponse<GetNotificationListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	
}
