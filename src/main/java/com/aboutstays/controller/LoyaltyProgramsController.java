package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddLoyaltyRequest;
import com.aboutstays.response.dto.AddLoyaltyProgramResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetLoyaltyProgramResponse;
import com.aboutstays.response.dto.UpdateLoyaltyProgram;
import com.aboutstays.services.LoyaltyProgramsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.LoyaltyProgramsConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.BASE)
public class LoyaltyProgramsController {
	
	@Autowired
	private LoyaltyProgramsService loyaltyProgramsService;

	@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetLoyaltyProgramResponse> getSample(){
		GetLoyaltyProgramResponse response = LoyaltyProgramsConverter.getSampleRequest();
		return new BaseApiResponse<GetLoyaltyProgramResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddLoyaltyProgramResponse> add(@RequestBody(required=true)AddLoyaltyRequest request) throws AboutstaysException{
		ValidateUtils.validateLoyaltyRequest(request);
		AddLoyaltyProgramResponse response = loyaltyProgramsService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_LOYALTY_FAILED);
		}
		return new BaseApiResponse<AddLoyaltyProgramResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetLoyaltyProgramResponse>> getAll() throws AboutstaysException{
		List<GetLoyaltyProgramResponse> response = loyaltyProgramsService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_LOYALTY_PROGRAMS_FOUND);
		}
		return new BaseApiResponse<List<GetLoyaltyProgramResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.LoyaltyProgramsParameters.ID, required=true)String id) throws AboutstaysException{
		if(!loyaltyProgramsService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_PROGRAM_NOT_EXISTS);
		}
		Boolean response = loyaltyProgramsService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetLoyaltyProgramResponse> get(@RequestParam(value=RestMappingConstants.LoyaltyProgramsParameters.ID, required=true)String id) throws AboutstaysException{
		GetLoyaltyProgramResponse response = loyaltyProgramsService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_PROGRAM_NOT_EXISTS);
		}
		return new BaseApiResponse<GetLoyaltyProgramResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.LoyaltyProgramsConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateLoyaltyProgram request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LOYALTY_ID);
		}

		if(!loyaltyProgramsService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_PROGRAM_NOT_EXISTS);
		}
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LOYALTY_PROGRAM_NAME);

		Boolean response = loyaltyProgramsService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_LOYALTY_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
}
