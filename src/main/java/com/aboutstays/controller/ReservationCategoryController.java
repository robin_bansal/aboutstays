package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.enums.ReservationType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.Direction;
import com.aboutstays.request.dto.AddReservationCategoryRequest;
import com.aboutstays.request.dto.UpdateReservationCategoryRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetReservationCategoryListResponse;
import com.aboutstays.response.dto.GetReservationCategoryResponse;
import com.aboutstays.services.ReservationCategoryService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ReservationCategoryConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.ReservationCategoryConstants.BASE)
public class ReservationCategoryController {
	
	@Autowired
	private ReservationCategoryService reservationCategoryService;
	
	@Autowired
	private ValidateIdsService validateIdsService; 
	
	@RequestMapping(value=RestMappingConstants.REQUEST, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddReservationCategoryRequest> getSampleRequest(){
		AddReservationCategoryRequest request = ReservationCategoryConverter.getSampleRequest();
		return new BaseApiResponse<AddReservationCategoryRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationCategoryConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addCategory(@RequestBody(required=true)AddReservationCategoryRequest request) throws AboutstaysException{
		ValidateUtils.validateAddReservationCategoryRequest(request);
		if(ReservationType.RESTAURANTS_AND_CUISINES.getCode()!=request.getReservationType()){
			request.setPrimaryCuisine(null);
		}
		validateIdsService.validateFloorId(request.getFloorId(), request.getWingId(), request.getHotelId());
		if(CollectionUtils.isNotEmpty(request.getDirectionList())){
			for(Direction direction : request.getDirectionList()){
				validateIdsService.validateAmenityId(direction.getDirectionId(), AmenityType.DIRECTIONS, AboutstaysResponseCode.DIRECTION_NOT_FOUND);
				if(StringUtils.isEmpty(direction.getDirectionInfo()))
					throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DIRECTION_INFO);
			}
		}
		String id = reservationCategoryService.addCategory(request);
		return new BaseApiResponse<String>(false, id, AboutstaysResponseCode.RESERVATION_CATEGORY_ADDED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationCategoryConstants.GET_BY_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetReservationCategoryResponse> getById(@RequestParam(value=RestMappingConstants.ReservationCategoryParameters.ID)String id) throws AboutstaysException{
		GetReservationCategoryResponse response = reservationCategoryService.getById(id);
		if(response==null)
			throw new AboutstaysException(AboutstaysResponseCode.RES_CAT_NOT_FOUND);
		return new BaseApiResponse<GetReservationCategoryResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationCategoryConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetReservationCategoryListResponse> getAll(
			@RequestParam(value=RestMappingConstants.ReservationCategoryParameters.HOTEL_ID, required=false)String hotelId,
			@RequestParam(value=RestMappingConstants.ReservationCategoryParameters.RESERVATION_TYPE, required=false)Integer reservationType) throws AboutstaysException{
		List<GetReservationCategoryResponse> categories = reservationCategoryService.getAll(hotelId,ReservationType.getByCode(reservationType));
		if(CollectionUtils.isEmpty(categories))
			throw new AboutstaysException(AboutstaysResponseCode.NO_RES_CAT_FOUND);
		GetReservationCategoryListResponse response = new GetReservationCategoryListResponse();
		response.setCategories(categories);
		return new BaseApiResponse<GetReservationCategoryListResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationCategoryConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> deleteSubcategory(@RequestParam(value=RestMappingConstants.ReservationCategoryParameters.ID)String id) throws AboutstaysException{
		if(!reservationCategoryService.exists(id))
			throw new AboutstaysException(AboutstaysResponseCode.RES_CAT_NOT_FOUND);
		reservationCategoryService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.RESERVATION_CATEGORY_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value=RestMappingConstants.ReservationCategoryConstants.UPDATE, method=RequestMethod.PATCH, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateSubCategory(@RequestBody(required=true)UpdateReservationCategoryRequest request) throws AboutstaysException{
		ValidateUtils.validateUpdateReservationCatRequest(request);
		validateIdsService.validateFloorId(request.getFloorId(), request.getWingId(), request.getHotelId());
		reservationCategoryService.updateNonNull(request);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.RESERVATION_CATEGORY_UPDATED_SUCCESSFULLY);
	}

}
