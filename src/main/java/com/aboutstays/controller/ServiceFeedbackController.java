package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddServiceFeedbackRequest;
import com.aboutstays.request.dto.UpdateServiceFeedback;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetServiceFeedbackResponse;
import com.aboutstays.services.ServiceFeedbackService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.ServiceFeedbackConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.BASE)
public class ServiceFeedbackController {
	
	@Autowired
	private ServiceFeedbackService sfService;
	
	@Autowired
	private ValidateIdsService validateIdService;
	
	@Autowired
	private UserOptedServicesService uosService;
	
	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddServiceFeedbackRequest> getSample(){
		AddServiceFeedbackRequest response = ServiceFeedbackConverter.getSampleRequest();
		return new BaseApiResponse<AddServiceFeedbackRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}

	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddServiceFeedbackRequest request) throws AboutstaysException{
		ValidateUtils.validateServiceFeedbackRequest(request);
		validateIdService.validateUOSId(request.getUosId());
		
		String response = sfService.add(request);
		
		UserOptedService service = uosService.getEntity(request.getUosId());
		service.setSfId(response);
		uosService.updateById(service);
		
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_SERVICE_FEEDBACK_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.GET_BY_UOSID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetServiceFeedbackResponse> getByUosId(@RequestParam(value=RestMappingConstants.ServiceFeedbackParameters.UOS_ID, required=true)String uosId) throws AboutstaysException{
		validateIdService.validateUOSId(uosId);
		GetServiceFeedbackResponse response = sfService.getByUosId(uosId);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.SERVICE_FEEDBACK_NOT_FOUND);
		}
		return new BaseApiResponse<GetServiceFeedbackResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetServiceFeedbackResponse> get(@RequestParam(value=RestMappingConstants.ServiceFeedbackParameters.ID, required=true)String id) throws AboutstaysException{
		GetServiceFeedbackResponse response = sfService.get(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.SERVICE_FEEDBACK_NOT_FOUND);
		}
		return new BaseApiResponse<GetServiceFeedbackResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	
	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateServiceFeedback request) throws AboutstaysException{
		ValidateUtils.validateServiceFeedbackRequest(request);
		validateIdService.validateUOSId(request.getUosId());
		
		Boolean response = sfService.updateNonNUll(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_SERVICE_FEEDBACK_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetServiceFeedbackResponse>> getAll() throws AboutstaysException{
		List<GetServiceFeedbackResponse> response = sfService.getAll();
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.SERVICE_FEEDBACK_NOT_FOUND);
		}
		return new BaseApiResponse<List<GetServiceFeedbackResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.ServiceFeedbackConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.ServiceFeedbackParameters.ID, required=true)String id) throws AboutstaysException{
		if(!sfService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.SERVICE_FEEDBACK_NOT_FOUND);
		}
		Boolean response = sfService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.SERVICE_FEEDBACK_DEL_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
}