package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.MenuItemsByType;
import com.aboutstays.request.dto.AddRoomServiceRequest;
import com.aboutstays.request.dto.GetRoomServiceResponse;
import com.aboutstays.request.dto.UpdateRoomServiceRequest;
import com.aboutstays.response.dto.AddRoomServiceResponse;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.FoodItemResponse;
import com.aboutstays.services.FoodItemService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.RoomServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.RoomServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.RoomServiceConstants.BASE)
public class RoomServiceController {
	
	@Autowired
	private RoomServicesService roomServicesService;
	
	@Autowired
	private FoodItemService foodItemService;
	
	@Autowired
	private HotelService hotelService; 
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddRoomServiceRequest> getSample(){
		AddRoomServiceRequest response = RoomServiceConverter.getSampleRequest();
		return new BaseApiResponse<AddRoomServiceRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddRoomServiceResponse> add(@RequestBody(required=true)AddRoomServiceRequest request) throws AboutstaysException{
		ValidateUtils.validateAddRoomServiceRequest(request);
		if(!hotelService.hotelExistsById(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if(CollectionUtils.isNotEmpty(request.getListMenuItem())){
			for(MenuItemsByType menuItemsByType : request.getListMenuItem()){
				MenuItemType menuItemType = MenuItemType.getByCode(menuItemsByType.getType()); 
				if(menuItemType==null)
					throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MENU_ITEM_TYPE);
				
//				if(CollectionUtils.isNotEmpty(menuItemsByType.getFoodItemIds())){
//					for(String foodItemId : menuItemsByType.getFoodItemIds()){
//						if(!foodItemService.existsById(foodItemId)){
//							throw new AboutstaysException(String.format(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getMessage(), foodItemId), AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND.getCode());
//						}
//					}
//				}
				menuItemsByType.setTypeName(menuItemType.getDisplayName());
				menuItemsByType.setImageUrl(menuItemType.getImageUrl());
			}
		}
		
		AddRoomServiceResponse response = roomServicesService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_ROOM_SERVICE_FAILED);
		}
		return new BaseApiResponse<AddRoomServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetRoomServiceResponse>> getAll() throws AboutstaysException{
		List<GetRoomServiceResponse> response = roomServicesService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.GET_ALL_ROOM_SERVICE_FAILED);
		}
		return new BaseApiResponse<List<GetRoomServiceResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetRoomServiceResponse> get(@RequestParam(value=RestMappingConstants.RoomServiceParameters.ID, required=true)String id) throws AboutstaysException{
		GetRoomServiceResponse response = roomServicesService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_SERVICE_NOT_EXISTS);
		}
		return new BaseApiResponse<GetRoomServiceResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.RoomServiceParameters.ID, required=true)String id) throws AboutstaysException{
		if(!roomServicesService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_SERVICE_NOT_EXISTS);
		}
		Boolean response = roomServicesService.deleteById(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.RS_DELETE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateRoomServiceRequest request) throws AboutstaysException{
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_SERVICE_ID);
		}
		if(!roomServicesService.existsById(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.AIRPORT_SERVICE_NOT_EXISTS);
		}
		Boolean response = roomServicesService.updateById(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_ROOM_SERVICE_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.RoomServiceConstants.SEARCH_FOOD_ITEM, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<FoodItemResponse>> searchFoodItems(@RequestParam(value=RestMappingConstants.RoomServiceParameters.ID)String id,@RequestParam(value=RestMappingConstants.RoomServiceParameters.QUERY)String query) throws AboutstaysException{
		if(!roomServicesService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_SERVICE_NOT_EXISTS);
		if(StringUtils.isEmpty(query))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SEARCH_QUERY);
		List<FoodItemResponse> responseList = foodItemService.searchByQueryString(id, query);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
		return new BaseApiResponse<List<FoodItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
}
