package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.AddStaysFeedbackRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.UpdateStaysFeedbackRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.StaysFeedbackResponse;
import com.aboutstays.services.StaysFeedbackService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.StaysFeedbackConverter;
import com.aboutstays.utils.UserOptedServiceConverter;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.BASE)
public class StaysFeedbackController extends BaseSessionController{
	
	@Autowired
	private StaysFeedbackService sfService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.SAMPLE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddStaysFeedbackRequest> getSample(){
		AddStaysFeedbackRequest response = StaysFeedbackConverter.getSampleRequest();
		return new BaseApiResponse<AddStaysFeedbackRequest>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.ADD, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> add(@RequestBody(required=true)AddStaysFeedbackRequest request) throws AboutstaysException{
		ValidateUtils.validateAddStaysRequest(request);
		baseSessionService.validateBaseSessionSessionData(request);
		
		String response = sfService.add(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_STAYS_FEEDBACK_FAILED);
		}
		
		AddUserOptedServiceRequest optedRequest = UserOptedServiceConverter.createUOSRequestByStaysFeedback(request, response);
		UserOptedServiceController controller = new UserOptedServiceController();
		beanFactory.autowireBean(controller);
		BaseApiResponse<AddUserOptedServiceResposne> uosResponse = controller.add(optedRequest);
		if(uosResponse == null){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_STAYS_FEEDBACK_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.GET_ALL, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<StaysFeedbackResponse>> getAll() throws AboutstaysException{
		List<StaysFeedbackResponse> response = sfService.getAll();
		if(CollectionUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		return new BaseApiResponse<List<StaysFeedbackResponse>>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<StaysFeedbackResponse> get(@RequestParam(value=RestMappingConstants.StaysFeedbackParameters.ID, required=true)String id) throws AboutstaysException{
		StaysFeedbackResponse response = sfService.getById(id);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		return new BaseApiResponse<StaysFeedbackResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.StaysFeedbackParameters.ID, required=true)String id) throws AboutstaysException{
		if(!sfService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		Boolean response = sfService.delete(id);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.STAYS_FEEDBACK_DELETION_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.UPDATE_NEW, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> updateNew(@RequestBody(required=true)UpdateStaysFeedbackRequest request) throws AboutstaysException{
		if(request == null){
			throw new AboutstaysException(AboutstaysResponseCode.STAYS_FEEDBACK_REQUEST_INVALID);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		
		boolean response = sfService.update(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_STAYS_FEEDBACK_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.UPDATE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> update(@RequestBody(required=true)UpdateStaysFeedbackRequest request) throws AboutstaysException{
		if(request == null){
			throw new AboutstaysException(AboutstaysResponseCode.STAYS_FEEDBACK_REQUEST_INVALID);
		}
		if(StringUtils.isEmpty(request.getId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_FEEDBACK_ID);
		}
		baseSessionService.validateBaseSessionSessionData(request);
		
		boolean response = sfService.updateNonNull(request);
		if(response == false){
			throw new AboutstaysException(AboutstaysResponseCode.UPDATE_STAYS_FEEDBACK_FAILED);
		}
		return new BaseApiResponse<Boolean>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.GET_ALL_BY_USER_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<StaysFeedbackResponse>> getAllByUserId(@RequestParam(value=RestMappingConstants.StaysFeedbackParameters.USER_ID, required=true)String userId) throws AboutstaysException{
		validateIdsService.validateUserId(userId);
		List<StaysFeedbackResponse> listResponse = sfService.getAllByUser(userId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		return new BaseApiResponse<List<StaysFeedbackResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.GET_ALL_BY_HOTEL_ID, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<StaysFeedbackResponse>> getAllByHotelId(@RequestParam(value=RestMappingConstants.StaysFeedbackParameters.HOTEL_ID, required=true)String hotelId) throws AboutstaysException{
		validateIdsService.validateHotelId(hotelId);
		List<StaysFeedbackResponse> listResponse = sfService.getAllByHotel(hotelId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		return new BaseApiResponse<List<StaysFeedbackResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.GET_ALL_BY_USER_ID_AND_STAY_ID, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<StaysFeedbackResponse>> getAllByUserIdAndStayId(@RequestParam(value=RestMappingConstants.StaysFeedbackParameters.USER_ID, required=true)String userId,
			@RequestParam(value=RestMappingConstants.StaysFeedbackParameters.STAYS_ID, required=true)String staysId) throws AboutstaysException{
		validateIdsService.validateUserIdStaysId(userId, staysId);
		List<StaysFeedbackResponse> listResponse = sfService.getAllOfUserStay(userId, staysId);
		if(CollectionUtils.isEmpty(listResponse)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		return new BaseApiResponse<List<StaysFeedbackResponse>>(false, listResponse, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.StaysFeedbackConstants.GET_BY_BASIC_IDS, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<StaysFeedbackResponse> getAllByHotlStaysUser(@RequestBody(required=true)UserStayHotelIdsPojo request) throws AboutstaysException{
		baseSessionService.validateBaseSessionSessionData(request);
		StaysFeedbackResponse response = sfService.getAllByUserHotelStays(request);
		if(response == null){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_FEEDBACK_FOUND);
		}
		return new BaseApiResponse<StaysFeedbackResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	

}
