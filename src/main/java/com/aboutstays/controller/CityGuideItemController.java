package com.aboutstays.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.constants.RestMappingConstants;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CityGuideItemType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddCityGuideItemRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.GetCityGuideItemResponse;
import com.aboutstays.services.CityGuideItemService;
import com.aboutstays.services.CityGuideService;
import com.aboutstays.services.HotelService;
import com.aboutstays.utils.CityGuideItemConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ValidateUtils;

@Controller
@RequestMapping(value = RestMappingConstants.CityGuideItemConstants.BASE)
public class CityGuideItemController {
	
	@Autowired
	private CityGuideItemService cgItemService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private CityGuideService cgService;
	
	@RequestMapping(value = RestMappingConstants.CityGuideItemConstants.SAMPLE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<AddCityGuideItemRequest> getSampleRequest() {
		AddCityGuideItemRequest request = CityGuideItemConverter.getSampleRequest();
		return new BaseApiResponse<AddCityGuideItemRequest>(false, request, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.CityGuideItemConstants.ADD, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<String> addCGItem(@RequestBody(required = true) AddCityGuideItemRequest request) throws AboutstaysException {
		ValidateUtils.validateAddCGItemRequest(request);
		CityGuideItemType typeEnum = CityGuideItemType.getByCode(request.getCityGuideItemType());
		if(typeEnum == null){
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_CITY_GUIDE_ITEM_TYPE);
		}
		if(!hotelService.hotelExistsById(request.getHotelId())){
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		if(!cgService.existsById(request.getCityGuideId())){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_NOT_FOUND);
		}
		String response = cgItemService.save(request);
		if(StringUtils.isEmpty(response)){
			throw new AboutstaysException(AboutstaysResponseCode.ADD_CG_ITEM_FAILED);
		}
		return new BaseApiResponse<String>(false, response, AboutstaysResponseCode.SUCCESS);	
	}
	
	@RequestMapping(value = RestMappingConstants.CityGuideItemConstants.GET_ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCityGuideItemResponse>> getAll() throws AboutstaysException {
		List<GetCityGuideItemResponse> responseList = cgItemService.getAllCGItems();
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CITY_GUIDE_ITEM_ADDED_YET);
		return new BaseApiResponse<List<GetCityGuideItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.CityGuideItemConstants.GET_ALL_BY_HOTEL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCityGuideItemResponse>> getAllByHotelId(
			@RequestParam(value = RestMappingConstants.CityGuideItemParameters.HOTEL_ID) String hotelId)
			throws AboutstaysException {
		if (!hotelService.hotelExistsById(hotelId)) {
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		}
		List<GetCityGuideItemResponse> responseList = cgItemService.getAllByHotelId(hotelId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CITY_GUIDE_ITEM_ADDED_YET);
		return new BaseApiResponse<List<GetCityGuideItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value = RestMappingConstants.CityGuideItemConstants.GET_ALL_BY_CG_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCityGuideItemResponse>> getAllByRoomServicesId(
			@RequestParam(value = RestMappingConstants.CityGuideItemParameters.CG_ID) String cgId)
			throws AboutstaysException {
		if (!cgService.existsById(cgId)) {
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_NOT_FOUND);
		}
		List<GetCityGuideItemResponse> responseList = cgItemService.getAllByCGId(cgId);
		if (CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CITY_GUIDE_ITEM_ADDED_YET);
		return new BaseApiResponse<List<GetCityGuideItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideItemConstants.FETCH_BY_TYPE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<List<GetCityGuideItemResponse>> fetchAllItemsByType(@RequestParam(value = RestMappingConstants.CityGuideItemParameters.CG_ID) 
			String cgId, @RequestParam(value = RestMappingConstants.CityGuideItemParameters.TYPE)Integer type) throws AboutstaysException{
		CityGuideItemType itemType = CityGuideItemType.getByCode(type);
		if(itemType==null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_CITY_GUIDE_ITEM_TYPE);
		if (!cgService.existsById(cgId)) {
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_NOT_FOUND);
		}
		List<GetCityGuideItemResponse> responseList = cgItemService.getAllByType(cgId,itemType);
		if(CollectionUtils.isEmpty(responseList))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CITY_GUIDE_ITEM_ADDED_YET);
		return new BaseApiResponse<List<GetCityGuideItemResponse>>(false, responseList, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideItemConstants.GET, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<GetCityGuideItemResponse> getItemById(@RequestParam(value=RestMappingConstants.CityGuideItemParameters.ID)String id) throws AboutstaysException{
		GetCityGuideItemResponse response = cgItemService.getById(id);
		if(response==null){
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_ITEM_NOT_FOUND);
		}
		return new BaseApiResponse<GetCityGuideItemResponse>(false, response, AboutstaysResponseCode.SUCCESS);
	}
	
	@RequestMapping(value=RestMappingConstants.CityGuideItemConstants.DELETE, method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseApiResponse<Boolean> delete(@RequestParam(value=RestMappingConstants.CityGuideItemParameters.ID)String id) throws AboutstaysException{
		if(!cgItemService.existsById(id))
			throw new AboutstaysException(AboutstaysResponseCode.CITY_GUIDE_ITEM_NOT_FOUND);
		cgItemService.delete(id);
		return new BaseApiResponse<Boolean>(false, true, AboutstaysResponseCode.CITY_GUIDE_ITEM_DELETE_FAILED);
	}
		

}
