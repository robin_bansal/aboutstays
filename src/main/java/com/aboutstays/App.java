package com.aboutstays;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        long t = 1234;
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("IST"));
        long cTime = c.getTimeInMillis();
        System.out.println(new SimpleDateFormat("hh:mm a",Locale.CANADA).format(c.getTime()));
        c.setTimeInMillis(t);
        System.out.println(c.getTime());
        Calendar date = Calendar.getInstance();
        date.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        System.out.println(date.getTime());
        Date cDate = new Date();
        
    }
}
