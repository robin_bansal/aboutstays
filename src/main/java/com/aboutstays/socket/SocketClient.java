package com.aboutstays.socket;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import com.aboutstays.request.dto.SocketMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter.Listener;

/**
 * Hello world!
 *
 */
public class SocketClient extends WebSocketClient
{
	
    public SocketClient(URI serverURI) {
		super(serverURI);
	}
    

	public static void main( String[] args ) throws URISyntaxException, UnsupportedEncodingException
    {
    	Socket socket = IO.socket("http://localhost:3003");
    	socket.on(Socket.EVENT_DISCONNECT, new Listener() {
			  @Override
			  public void call(Object... args) {
				  System.out.println("disconect");
			  }
		}).on(Socket.EVENT_CONNECT, new Listener() {
			@Override
			public void call(Object... arg0) {
				System.out.println("here");
				System.out.println("socket connected");
				SocketMessageRequest messageRequest = new SocketMessageRequest();
				messageRequest.setFromId("5947b5b00364d332ae277f64");
				messageRequest.setToId("5947b5b00364d332ae277f64");
				messageRequest.setMessageId(System.currentTimeMillis());
				messageRequest.setMetaData("{\"text\":\"Hello\",\"stayId\":\"12345\"}");
				messageRequest.setSocketType(SocketManager.SocketTypes.HOTEL_CHAT.getType());
				try {
					socket.emit("addSocket", new ObjectMapper().writeValueAsString(messageRequest));
					messageRequest.setSocketType(SocketManager.SocketTypes.USER_CHAT.getType());
					socket.emit("message", new ObjectMapper().writeValueAsString(messageRequest));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).on(Socket.EVENT_CONNECT_ERROR, new Listener() {
			
			@Override
			public void call(Object... arg0) {
				System.out.println("error");
				System.out.println(arg0[0]);
			}
		}).on("receive", new Listener() {
			@Override
			public void call(Object... arg0) {
				System.out.println("receive message");
				System.out.println(arg0[0]);
				try {
					JSONObject jsonObject=new JSONObject(arg0[0].toString());
					jsonObject.put("socketType", SocketManager.SocketTypes.USER_CHAT.getType());
					socket.emit("changeStatus", jsonObject.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
    	socket.connect();
//    	
//    }
//
//	@Override
//	public void onClose(int arg0, String arg1, boolean arg2) {
//		System.out.println("close");
		
	}

	@Override
	public void onError(Exception arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0.getMessage());
		System.out.println("error");
	}

	@Override
	public void onMessage(String arg0) {
		// TODO Auto-generated method stub
		System.out.println("message"+arg0);
	}

	@Override
	public void onOpen(ServerHandshake arg0) {
		// TODO Auto-generated method stub
		send("test");
		System.out.println("open");
	}


	@Override
	public void onClose(int code, String reason, boolean remote) {
		// TODO Auto-generated method stub
		System.out.println("close");
	}
    
}
