package com.aboutstays.socket;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MessageStatus;
import com.aboutstays.enums.MessageCreationType;
import com.aboutstays.pojos.socketmetadatas.ChatMetaData;
import com.aboutstays.request.dto.AddMessageRequest;
import com.aboutstays.request.dto.SocketMessageRequest;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.MessageService;
import com.aboutstays.services.UserService;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SocketManager implements ConnectListener, DisconnectListener {

	private SocketIOServer socketIOServer;
	private Map<String, SocketIOClient> userIdVsClientMap;
	private Map<String, SocketIOClient> hotelIdVsClientMap;
	private ObjectMapper objectMapper;

	@Autowired
	private MessageService messageService;

	@Autowired
	private HotelService hotelService;

	@Autowired
	private UserService userService;

	@PostConstruct
	public void init() {
		userIdVsClientMap = new ConcurrentHashMap<>();
		hotelIdVsClientMap = new ConcurrentHashMap<>();
		Integer port = 3003;
		Configuration configuration = new Configuration();
		configuration.setPort(port);
		socketIOServer = new SocketIOServer(configuration);
		socketIOServer.addConnectListener(this);
		socketIOServer.addDisconnectListener(this);
		socketIOServer.addEventListener(SocketEvent.ADD_SOCKET.getEventName(), String.class, addSocketListener);
		socketIOServer.addEventListener(SocketEvent.RECEIVE_MESSAGE_FROM_CLIENT.getEventName(), String.class,
				messageListener);
		socketIOServer.addEventListener(SocketEvent.CHANGE_STATUS.getEventName(), String.class, onStatusChangeListener);
		socketIOServer.start();
		objectMapper = new ObjectMapper();
	}

	@Override
	public void onConnect(SocketIOClient client) {
		System.out.println("Socket connected " + client.getSessionId());
	}

	@Override
	public void onDisconnect(SocketIOClient client) {
		System.out.println("Socket disconnected" + client.getSessionId());
		Iterator<Map.Entry<String, SocketIOClient>> iterator = hotelIdVsClientMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, SocketIOClient> expertIdVsClientEntry = iterator.next();
			SocketIOClient registeredClient = expertIdVsClientEntry.getValue();
			if (registeredClient != null && client.getSessionId().equals(registeredClient.getSessionId())) {
				iterator.remove();
				client.disconnect();
				return;
			}
		}

		iterator = userIdVsClientMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, SocketIOClient> userIdVsClientEntry = iterator.next();
			SocketIOClient registeredClient = userIdVsClientEntry.getValue();
			if (registeredClient != null && client.getSessionId().equals(registeredClient.getSessionId())) {
				iterator.remove();
				client.disconnect();
				return;
			}
		}
	}

	private DataListener<String> addSocketListener = new DataListener<String>() {
		public void onData(SocketIOClient client, String response, AckRequest ackSender) throws Exception {
			System.out.println("In add Socket listener : " + response);
			SocketMessageRequest data = objectMapper.readValue(response, SocketMessageRequest.class);
			if (data != null) {
				SocketTypes socketTypes = SocketTypes.findByType(data.getSocketType());
				if (socketTypes != null) {
					switch (socketTypes) {
					case USER_CHAT:
						String userId = data.getFromId();
						if (!StringUtils.isEmpty(userId)) {
							if (userService.existsByUserId(userId)) {
								userIdVsClientMap.put(userId, client);
							}
						}
						break;
					case HOTEL_CHAT:
						String hotelId = data.getFromId();
						if (!StringUtils.isEmpty(hotelId)) {
							if (hotelService.hotelExistsById(hotelId)) {
								hotelIdVsClientMap.put(hotelId, client);
							}
						}
						break;
					}
				}
			}

		}
	};

	private DataListener<String> messageListener = new DataListener<String>() {
		@Override
		public void onData(SocketIOClient client, String response, AckRequest ackSender) throws Exception {
			System.out.println("In message listener");
			SocketMessageRequest data = objectMapper.readValue(response, SocketMessageRequest.class);
			if (data != null) {
				SocketTypes socketTypes = SocketTypes.findByType(data.getSocketType());
				if (socketTypes != null) {
					switch (socketTypes) {
					case USER_CHAT: {
						String userId = data.getFromId();
						String hotelId = data.getToId();
						if (hotelIdVsClientMap.containsKey(hotelId)) {
							SocketIOClient socketIOClient = hotelIdVsClientMap.get(hotelId);
							socketIOClient.sendEvent(SocketEvent.SEND_MESSAGE_TO_CLIENT.getEventName(), data);
							saveChatMessage(userId, hotelId, MessageCreationType.BY_USER.getCode(), data.getMetaData(),
									MessageStatus.READ);
						} else {
							saveChatMessage(userId, hotelId, MessageCreationType.BY_USER.getCode(), data.getMetaData(),
									MessageStatus.SENT);
						}
						break;
					}
					case HOTEL_CHAT: {
						String hotelId = data.getFromId();
						String userId = data.getToId();
						if (userIdVsClientMap.containsKey(userId)) {
							SocketIOClient socketIOClient = userIdVsClientMap.get(userId);
							socketIOClient.sendEvent(SocketEvent.SEND_MESSAGE_TO_CLIENT.getEventName(), data);
							saveChatMessage(userId, hotelId, MessageCreationType.BY_HOTEL.getCode(), data.getMetaData(),
									MessageStatus.READ);
						}else{
							saveChatMessage(userId, hotelId, MessageCreationType.BY_HOTEL.getCode(), data.getMetaData(),
									MessageStatus.SENT);							
						}
						break;
					}
					}
				}
			}
		}

		private void saveChatMessage(String userId, String hotelId, int code, String metaData,
				MessageStatus messageStatus) {
			AddMessageRequest addMessageRequest = new AddMessageRequest();
			addMessageRequest.setHotelId(hotelId);
			addMessageRequest.setUserId(userId);
			addMessageRequest.setCreationType(code);
			try {
				ChatMetaData chatMetaData = objectMapper.readValue(metaData, ChatMetaData.class);
				if (chatMetaData != null) {
					addMessageRequest.setMessageText(chatMetaData.getText());
					addMessageRequest.setStaysId(chatMetaData.getStayId());
					addMessageRequest.setStatus(messageStatus.getStatus());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			messageService.save(addMessageRequest);
		}
	};
	private DataListener<String> onStatusChangeListener = new DataListener<String>() {
		public void onData(SocketIOClient client, String response, AckRequest ackSender) throws Exception {
			System.out.println("On state change listener : " + response);
			SocketMessageRequest data = objectMapper.readValue(response, SocketMessageRequest.class);
			if (data != null) {
				SocketTypes socketTypes = SocketTypes.findByType(data.getSocketType());
				if (socketTypes != null) {
					switch (socketTypes) {
					case USER_CHAT:
						messageService.changeStatus(data.getFromId(), data.getToId(), MessageCreationType.BY_USER,
								MessageStatus.READ);
						break;
					case HOTEL_CHAT:
						messageService.changeStatus(data.getFromId(), data.getToId(), MessageCreationType.BY_HOTEL,
								MessageStatus.READ);
						break;
					}
				}
			}
		};
	};

	public static void main(String[] args) {
		new SocketManager().init();
	}

	private enum SocketEvent {
		ADD_SOCKET("addSocket", "addSocket"), RECEIVE_MESSAGE_FROM_CLIENT("message",
				"message"), SEND_MESSAGE_TO_CLIENT("receive", "receive"), CHANGE_STATUS("changeStatus", "changeStatus");

		private String massageType;
		private String eventName;

		public String getEventName() {
			return eventName;
		}

		private SocketEvent(String eventName, String massageType) {
			this.eventName = eventName;
			this.massageType = massageType;
		}

	}

	public enum SocketTypes {
		USER_CHAT(1), HOTEL_CHAT(2);
		int type;

		private SocketTypes(int type) {
			this.type = type;
		}

		public static SocketTypes findByType(int type) {
			for (SocketTypes socketTypes : values()) {
				if (socketTypes.getType() == type) {
					return socketTypes;
				}
			}
			return null;
		}

		public int getType() {
			return type;
		}
	}
}
