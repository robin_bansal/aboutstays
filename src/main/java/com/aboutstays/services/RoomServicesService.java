package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.RoomServicesEntity;
import com.aboutstays.request.dto.AddRoomServiceRequest;
import com.aboutstays.request.dto.GetRoomServiceResponse;
import com.aboutstays.request.dto.UpdateRoomServiceRequest;
import com.aboutstays.response.dto.AddRoomServiceResponse;

public interface RoomServicesService {
	
	public AddRoomServiceResponse add(AddRoomServiceRequest request);
	public List<GetRoomServiceResponse> getAll();
	public GetRoomServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateRoomServiceRequest request);
	public RoomServicesEntity getEntity(String roomServicesId);

}
