package com.aboutstays.services;


import java.util.List;

import com.aboutstays.entities.Booking;
import com.aboutstays.enums.BookingStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AllocateRoomRequest;
import com.aboutstays.request.dto.CreateBookingRequest;
import com.aboutstays.request.dto.GetBookingsRequest;
import com.aboutstays.request.dto.UpdateBookingRequest;
import com.aboutstays.response.dto.CreateBookingResponse;
import com.aboutstays.response.dto.GetBookingReponse;
import com.aboutstays.response.dto.GetIncompleteBookingsResponce;

public interface BookingService {	
	public boolean existsByReservationNumber(String reservationNumber);
	public boolean existsByBookingId(String bookingId);
	public CreateBookingResponse createBooking(CreateBookingRequest request) throws AboutstaysException;
	public List<GetBookingReponse> getBookings(GetBookingsRequest getBookingsRequest) throws AboutstaysException;
	public boolean deleteByBookingId(String bookingId);
	public boolean deleteByReservationNumber(String reservationNumber);
	public boolean updateBookingByBookingId(UpdateBookingRequest request) throws AboutstaysException;
	public List<GetBookingReponse> getBookingsByReservationNumber(String reservationNumber, String hotelId);
	public List<GetIncompleteBookingsResponce> getUnCompleteBookings(String hotelId, String bookingDate);
	public Boolean allotRoom(AllocateRoomRequest request) throws AboutstaysException;
	public void freezeBooking(List<String> bookingIds);
	public void changeBookingStatus(String bookingId, BookingStatus bookingStatus);
	public Booking getEntityById(String bookingId, String hotelId);
	public void setDefaultStaysId(String bookingId, String staysId);
	public Booking getEntityById(String bookingId);
}
