package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddLateCheckoutRequest;
import com.aboutstays.request.dto.UpdateLateCheckoutRequest;
import com.aboutstays.response.dto.LateCheckoutResposne;

public interface LateCheckoutRequestService {
	
	public String add(AddLateCheckoutRequest request);
	public LateCheckoutResposne getById(String id);
	public List<LateCheckoutResposne> getAll();
	public boolean delete(String id);
	public boolean updateNonNUll(UpdateLateCheckoutRequest request);
	public boolean existsById(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	public LateCheckoutRequest getEntity(String id);
	public String modifyLateCheckoutRequest(UpdateLateCheckoutRequest request);
	public LateCheckoutResposne retrieveLateCheckoutRequest(String userId, String hotelId, String staysId);

}
