package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.LaundryServiceRequest;
import com.aboutstays.response.dto.LaundryServiceResponse;

public interface LaundryServicesService {

	public String save(LaundryServiceRequest request);
	public LaundryServiceResponse getById(String servicesId);
	public boolean existsById(String servicesId);
	public List<LaundryServiceResponse> getAll();
	public void update(LaundryServiceRequest request);
	public void updateNonNull(LaundryServiceRequest request);
	
}
