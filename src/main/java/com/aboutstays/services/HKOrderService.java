package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddHKOrderRequest;
import com.aboutstays.request.dto.UpdateHKOrderRequest;
import com.aboutstays.response.dto.GetHKOrderResponse;

public interface HKOrderService {

	public String save(AddHKOrderRequest request);
	public GetHKOrderResponse getById(String id);
	public List<GetHKOrderResponse> getAll();
	public List<GetHKOrderResponse> getAllByUserId(String userId);
	public List<GetHKOrderResponse> getAllByHotelId(String hotelId);
	public List<GetHKOrderResponse> getAllByUserAndStayId(String userId, String stayId);
	public void updateRequest(UpdateHKOrderRequest request);
	public boolean updateNonNullFieldsOfRequest(UpdateHKOrderRequest request);
	public boolean delete(String id);
	public boolean existsById(String id);
	public HousekeepingOrder getEntity(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	public boolean updateHKOrderRequest(HousekeepingOrder hkOrder);
}
