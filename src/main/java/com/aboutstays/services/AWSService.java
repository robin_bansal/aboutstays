package com.aboutstays.services;

import java.io.File;

import com.aboutstays.exception.AWSAccessException;

public interface AWSService {
	
	public String uploadToS3(String bucketName, String fileName, String filePath, String fileType) throws AWSAccessException;
	public String uploadToS3(String bucketName, File file, String filePath) throws AWSAccessException;

}
