package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.PlaceType;
import com.aboutstays.request.dto.PlaceRequest;
import com.aboutstays.response.dto.PlaceResponse;

public interface PlaceService {

	public String save(PlaceRequest request);
	public void update(PlaceRequest request);
	public void updateNonNull(PlaceRequest request);
	public List<PlaceResponse> getByParentPlaceId(String parentPlaceId);
	public List<PlaceResponse> getByPlaceType(PlaceType placeType);
	public PlaceResponse getById(String placeId);
	public List<PlaceResponse> getAll();
	public void delete(String placeId);
	public boolean existsByPlaceId(String placeId);
	
}
