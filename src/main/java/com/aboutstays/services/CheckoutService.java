package com.aboutstays.services;

import com.aboutstays.entities.CheckoutEntity;
import com.aboutstays.enums.CheckoutRequestStatus;
import com.aboutstays.request.dto.InitiateCheckoutRequest;
import com.aboutstays.response.dto.CheckoutResponse;

public interface CheckoutService {
	
	public String save(InitiateCheckoutRequest request);
	public void update(CheckoutEntity request);
	public void updateNonNull(CheckoutEntity request);
	public CheckoutResponse getById(String id);
	public void delete(String id);
	public void changeStatus(String id, CheckoutRequestStatus checkoutRequestStatus);
	public CheckoutResponse getByUserStaysHotelId(String userId, String staysId, String hotelId);

}
