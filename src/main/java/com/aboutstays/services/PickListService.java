package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.PickListType;
import com.aboutstays.request.dto.AddPickListRequest;
import com.aboutstays.response.dto.GetPickListResponse;

public interface PickListService {

	public void addOrModify(AddPickListRequest request);
	public void delete(int type);
	public List<GetPickListResponse> getAll();
	public GetPickListResponse getByType(PickListType pickListType);
}
