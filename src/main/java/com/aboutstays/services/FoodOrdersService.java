package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.FoodOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddFoodOrderRequest;
import com.aboutstays.request.dto.UpdateFoodOrderRequest;
import com.aboutstays.response.dto.GetFoodOrderResponse;

public interface FoodOrdersService {

	public String save(AddFoodOrderRequest request);
	public GetFoodOrderResponse getById(String id);
	public List<GetFoodOrderResponse> getAll();
	public List<GetFoodOrderResponse> getAllByUserId(String userId);
	public List<GetFoodOrderResponse> getAllByHotelId(String hotelId);
	public List<GetFoodOrderResponse> getAllByUserAndStayId(String userId, String stayId);
	public void updateRequest(UpdateFoodOrderRequest request);
	public boolean updateNonNullFieldsOfRequest(UpdateFoodOrderRequest request);
	public boolean delete(String id);
	public boolean existsById(String id);
	public FoodOrder getEntity(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus foodOrderStatus);
	public boolean updateFoodOrderRequest(FoodOrder foodOrder);
}
