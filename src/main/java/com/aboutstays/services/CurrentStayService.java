package com.aboutstays.services;

import com.aboutstays.request.dto.GetAllStaysServiceRequest;
import com.aboutstays.response.dto.GetStaysServiceListResponse;

public interface CurrentStayService {

	public GetStaysServiceListResponse getAllCurrentStaysServices(GetAllStaysServiceRequest request);
	
}
