package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.AmenityType;
import com.aboutstays.request.dto.AddMasterAmenityRequest;
import com.aboutstays.response.dto.GetMasterAmenityResponse;

public interface MasterAmenityService {

	public String save(AddMasterAmenityRequest request);
	public GetMasterAmenityResponse getById(String id);
	public List<GetMasterAmenityResponse> getAll();
	public List<GetMasterAmenityResponse> getAll(AmenityType amenityType);
	public void delete(String id);
	public boolean existsById(String id);
	public boolean existsByName(String name, AmenityType amenityType);
	public void saveAll(List<AddMasterAmenityRequest> amenitites);
	public boolean existsByIdAndType(String amenityId, AmenityType amenityTpe);
}
