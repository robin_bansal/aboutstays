package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddAirportServiceRequest;
import com.aboutstays.request.dto.UpdateAirportServiceRequest;
import com.aboutstays.response.dto.AddAirportServiceResponse;
import com.aboutstays.response.dto.GetAirportServiceResponse;

public interface AirportServicesService {
	
	public AddAirportServiceResponse add(AddAirportServiceRequest request);
	public List<GetAirportServiceResponse> getAll();
	public GetAirportServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateAirportServiceRequest request);

}
