package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddCarRequest;
import com.aboutstays.request.dto.UpdateCarRequest;
import com.aboutstays.response.dto.AddCarResponse;
import com.aboutstays.response.dto.GetCarResponse;

public interface CarService {
	
	public AddCarResponse add(AddCarRequest request);
	public List<GetCarResponse> getAll();
	public GetCarResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateCarRequest request);
	public boolean existsByNumberPlate(String numberPlate);

}
