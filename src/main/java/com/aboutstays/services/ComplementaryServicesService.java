package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddComplementaryServicesRequest;
import com.aboutstays.request.dto.UpdateComplementaryServicesRequest;
import com.aboutstays.response.dto.GetComplementaryServicesResponse;

public interface ComplementaryServicesService {

	public String save(AddComplementaryServicesRequest request);
	public void update(UpdateComplementaryServicesRequest request);
	public GetComplementaryServicesResponse getById(String id);
	public List<GetComplementaryServicesResponse> getAll();
	public void delete(String id);
	public boolean existsById(String servicesId);
}
