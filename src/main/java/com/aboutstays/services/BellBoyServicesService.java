package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.BellBoyServiceRequest;
import com.aboutstays.response.dto.BellBoyServiceResponse;

public interface BellBoyServicesService {

	public String save(BellBoyServiceRequest request);
	public void update(BellBoyServiceRequest request);
	public void updateNonNull(BellBoyServiceRequest request);
	public void delete(String hotelId);
	public BellBoyServiceResponse getByHotelId(String hotelId);
	public boolean existsByHotelId(String hotelId);
	public List<BellBoyServiceResponse> getAll();
	
}
