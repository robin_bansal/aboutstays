package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddMasterGenereRequest;
import com.aboutstays.request.dto.UpdateMasterGenereRequest;
import com.aboutstays.response.dto.GetMasterGenereResponse;

public interface MasterGenereService {

	public String save(AddMasterGenereRequest request);
	public void delete(String id);
	public List<GetMasterGenereResponse> getAll();
	public void update(UpdateMasterGenereRequest request);
	public void updateNonNull(UpdateMasterGenereRequest request);
	public GetMasterGenereResponse getById(String id);
	public boolean existsByName(String name, String skipForChannelId);
	public GetMasterGenereResponse getByName(String name, String skipForChannelId);
	public boolean existsById(String masterGenereId);
}
