package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddReservationOrderRequest;
import com.aboutstays.request.dto.UpdateReservationOrderRequest;
import com.aboutstays.response.dto.GetReservationOrderResponse;

public interface ReservationOrderService {

	public String save(AddReservationOrderRequest request);
	public GetReservationOrderResponse getById(String id);
	public List<GetReservationOrderResponse> getAll();
	public boolean updateNonNullFieldsOfRequest(UpdateReservationOrderRequest request);
	public boolean delete(String id);
	public boolean existsById(String id);
	public ReservationOrder getEntity(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
}
 