package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.MaintenanceItem;
import com.aboutstays.enums.MaintenanceItemType;
import com.aboutstays.request.dto.UpdateMaintenanceItemRequest;
import com.aboutstays.response.dto.AddMaintenanceItemRequest;
import com.aboutstays.response.dto.MaintenanceItemResponse;

public interface MaintenanceItemService {
	
	public String save(AddMaintenanceItemRequest request);
	public MaintenanceItemResponse getById(String id);
	public List<MaintenanceItemResponse> getAll();
	public void update(UpdateMaintenanceItemRequest item);
	public boolean updateNonNull(UpdateMaintenanceItemRequest item);
	public List<MaintenanceItemResponse> getAllByHotelId(String hotelId);
	public List<MaintenanceItemResponse> getAllByMTServiceId(String serviceId);
	public boolean existsById(String id);
	public MaintenanceItem getEntityById(String id);
	public void updateEntity(MaintenanceItem item);
	public List<MaintenanceItemResponse> fetchByType(String mtServiceId, MaintenanceItemType itemType);
	public boolean deleteById(String id);

}
