package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddFloorRequest;
import com.aboutstays.request.dto.UpdateFloorRequest;
import com.aboutstays.response.dto.AddFloorResponse;
import com.aboutstays.response.dto.GetFloorResponse;

public interface FloorService {
	public AddFloorResponse add(AddFloorRequest request);
	public List<GetFloorResponse> getAll();
	public GetFloorResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateFloorRequest request);
	public List<GetFloorResponse> getFloorsByWingId(String wingId);
	public List<GetFloorResponse> getFloorsByHotelId(String hotelId);
	public boolean exists(String floorId, String wingId);
	public boolean exists(String floorId, String wingId, String hotelId);
}
