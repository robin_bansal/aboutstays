package com.aboutstays.services;

import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.BaseSessionPojo;

public interface BaseSessionService {
	void validateBaseSessionSessionData(BaseSessionPojo request) throws AboutstaysException;
}
