package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddRoomCategoryRequest;
import com.aboutstays.request.dto.SetEarlyCheckInRoomRequest;
import com.aboutstays.request.dto.SetLateCheckOutRoomRequest;
import com.aboutstays.request.dto.UpdateRoomCategoryRequest;
import com.aboutstays.response.dto.GetEarlyCheckinRoomResponse;
import com.aboutstays.response.dto.GetLateCheckoutRoomResponse;
import com.aboutstays.response.dto.GetRoomCategoryResponse;

public interface RoomCategoryService {
	public String addRoomCategory(AddRoomCategoryRequest addRoomCategoryRequest);
	public GetRoomCategoryResponse getById(String roomId);
	public List<GetRoomCategoryResponse> getAll();
	public List<GetRoomCategoryResponse> getAllByRoomCategoryIds(List<String> roomIds);
	public void delete(String roomCategoryId);
	public Boolean update(UpdateRoomCategoryRequest request);
	public Boolean updateNonNullFields(UpdateRoomCategoryRequest request);
	public boolean exists(String roomCategoryId);
	public boolean existsInHotel(String hotelId, String roomCategoryId);
	public boolean setEarlyCheckinRoomInfo(SetEarlyCheckInRoomRequest request);
	public GetEarlyCheckinRoomResponse getEarlyCheckinRoomInfo(String roomCategoryId);
	public boolean setLateCheckoutRoomInfo(SetLateCheckOutRoomRequest request);
	public GetLateCheckoutRoomResponse getLateCheckoutRoomInfo(String roomCategoryId);
	public void updateInRoomAmenities(String roomCategoryId, List<String> inRoomAmenityIds);
	public void updateInPreferenceBasedAmenities(String roomCategoryId, List<String> categoryBasedAmenityIds);
	public List<GetRoomCategoryResponse> getAllByHotelId(String hotelId);
}
