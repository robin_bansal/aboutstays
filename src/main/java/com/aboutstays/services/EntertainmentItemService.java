package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddEntertaimentItemRequest;
import com.aboutstays.request.dto.GetEntertaimentItemResponse;
import com.aboutstays.request.dto.UpdateEntertainmentRequest;

public interface EntertainmentItemService {
	
	public String add(AddEntertaimentItemRequest request);
	public List<String> addMultiple(List<AddEntertaimentItemRequest> request);
	public boolean existsById(String id);
	public GetEntertaimentItemResponse getById(String id);
	public List<GetEntertaimentItemResponse> getAll();
	public boolean delete(String id);
	public boolean updateNonNull(UpdateEntertainmentRequest request);
	public List<GetEntertaimentItemResponse> getAll(String entServiceId, String genereId);
	public boolean existsByMasterItemId(String masterItemId, String hotelId, String serviceId);
	public boolean update(UpdateEntertainmentRequest request);
	public List<GetEntertaimentItemResponse> getAll(String entServiceId);
	
}
