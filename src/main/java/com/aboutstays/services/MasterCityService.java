package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddMasterCityRequest;
import com.aboutstays.response.dto.GetMasterCityResponse;

public interface MasterCityService {

	public String save(AddMasterCityRequest request);
	public void saveAll(List<AddMasterCityRequest> requestList);
	public List<GetMasterCityResponse> getAll();
	public GetMasterCityResponse getById(String id);
	public void delete(String id);
	public GetMasterCityResponse getByName(String name);
	public boolean existsById(String id);
	public boolean existsByName(String name);
}
