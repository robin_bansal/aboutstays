package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.MessageStatus;
import com.aboutstays.enums.MessageCreationType;
import com.aboutstays.request.dto.AddMessageRequest;
import com.aboutstays.response.dto.GetChatUsersResponse;
import com.aboutstays.response.dto.GetMessageResponse;

public interface MessageService {
	public String save(AddMessageRequest request);
	public GetMessageResponse getMessageById(String id);
	public boolean existsById(String id);
	public List<GetMessageResponse> getAll();
	public List<GetMessageResponse> getAll(String userId, String staysId, String hotelId);
	public void delete(String id);
	public List<GetChatUsersResponse> getChatUsersByHotelId(String hotelId);
	public void changeStatus(String userId, String hotelId,MessageCreationType creationType,MessageStatus messageStatus);
}
