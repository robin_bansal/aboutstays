package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.Maintenance;
import com.aboutstays.request.dto.AddMaintenanceRequest;
import com.aboutstays.request.dto.UpdateMaintenanceRequest;
import com.aboutstays.response.dto.GetMaintenaneResponse;

public interface MaintenanceService {
	
	public String add(AddMaintenanceRequest request);
	public List<GetMaintenaneResponse> getAll();
	public GetMaintenaneResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateMaintenanceRequest request);
	public Maintenance getEntity(String id);

}
