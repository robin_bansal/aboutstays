package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddHouseRulesRequest;
import com.aboutstays.request.dto.UpdateHouseRulesRequest;
import com.aboutstays.response.dto.GetHouseRulesResponse;

public interface HouseRulesService {
	
	public String add(AddHouseRulesRequest request);
	public List<GetHouseRulesResponse> getAll();
	public GetHouseRulesResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateHouseRulesRequest request);
}
