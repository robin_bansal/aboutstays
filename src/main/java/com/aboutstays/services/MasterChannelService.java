package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddMasterChannelRequest;
import com.aboutstays.request.dto.AddMultipleMasterChannelRequest;
import com.aboutstays.request.dto.UpdateMasterChannelRequest;
import com.aboutstays.response.dto.GetMasterChannelResponse;

public interface MasterChannelService {

	public String save(AddMasterChannelRequest request);
	public void saveAll(AddMultipleMasterChannelRequest multipleRequests);
	public void update(UpdateMasterChannelRequest request);
	public GetMasterChannelResponse getById(String id);
	public List<GetMasterChannelResponse> getAll();
	public void delete(String id);
	public boolean existsByName(String name, String channelId);
	public boolean existsById(String channelId);
	public List<GetMasterChannelResponse> getAllByGenere(String genereId);
}
