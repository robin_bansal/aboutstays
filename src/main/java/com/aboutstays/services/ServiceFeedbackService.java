package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddServiceFeedbackRequest;
import com.aboutstays.request.dto.UpdateServiceFeedback;
import com.aboutstays.response.dto.GetServiceFeedbackResponse;

public interface ServiceFeedbackService {

	public String add(AddServiceFeedbackRequest request);
	public GetServiceFeedbackResponse get(String id);
	public List<GetServiceFeedbackResponse> getAll();
	public boolean delete(String id);
	public boolean update(UpdateServiceFeedback request);
	public boolean updateNonNUll(UpdateServiceFeedback request);
	public boolean existsById(String id);
	public GetServiceFeedbackResponse getByUosId(String uosId);
	
}
