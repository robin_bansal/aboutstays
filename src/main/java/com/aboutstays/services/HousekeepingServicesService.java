package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.HousekeepingServiceEntity;
import com.aboutstays.request.dto.AddHousekeepingServiceRequest;
import com.aboutstays.response.dto.AddHousekeepingServiceResponse;
import com.aboutstays.response.dto.GetHousekeepingServiceResponse;
import com.aboutstays.response.dto.UpdateHousekeepingServiceRequest;

public interface HousekeepingServicesService {
	
	public AddHousekeepingServiceResponse add(AddHousekeepingServiceRequest request);
	public List<GetHousekeepingServiceResponse> getAll();
	public GetHousekeepingServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateHousekeepingServiceRequest request);
	public HousekeepingServiceEntity getEntity(String hkId);

}
