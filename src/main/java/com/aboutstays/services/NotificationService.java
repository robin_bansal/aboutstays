package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddNotificationRequest;
import com.aboutstays.response.dto.GetNotificationResponse;

public interface NotificationService {

	public String addNotification(AddNotificationRequest request);
	public GetNotificationResponse getNotificationById(String id);
	public List<GetNotificationResponse> getAllNotifications();
	public List<GetNotificationResponse> getAllNotifications(String userId, String staysId, String hotelId);
	public boolean existsById(String id);
	public void deleteNotification(String id);
	
}
