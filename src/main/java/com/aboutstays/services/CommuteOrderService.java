package com.aboutstays.services;

import java.util.List;

import com.aboutstays.controller.CommuteOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddCommuteOrderRequest;
import com.aboutstays.request.dto.UpdateCommuteOrderRequest;
import com.aboutstays.response.dto.GetCommuteOrderResponse;

public interface CommuteOrderService {
	
	public String save(AddCommuteOrderRequest request);
	public GetCommuteOrderResponse getById(String id);
	public List<GetCommuteOrderResponse> getAll();
	public boolean updateNonNullFieldsOfRequest(UpdateCommuteOrderRequest request);
	public boolean delete(String id);
	public boolean existsById(String id);
	public CommuteOrder getEntity(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);

}
