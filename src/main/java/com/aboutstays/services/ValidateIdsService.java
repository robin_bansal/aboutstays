
package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;

public interface ValidateIdsService {

	public void validateHotelId(String hotelId) throws AboutstaysException;
	public void validateUserId(String userId) throws AboutstaysException;
	public void validateStaysId(String staysId) throws AboutstaysException;
	public void validateGSId(String gsId) throws AboutstaysException;
	public void validateHotelUserIds(String hotelId, String userId) throws AboutstaysException;
	public void validateUserHotelStaysIds(String userId, String hotelId, String staysId) throws AboutstaysException;
	public void validateUserHotelStaysIds(UserStayHotelIdsPojo userStayHotelIdsPojo) throws AboutstaysException;
	public void validateUserHotelStaysGsIds(UserStaysHotelGSIdsPojo userStaysHotelGSIdsPojo) throws AboutstaysException;
	public void validateUserHotelStaysGsIds(String userId, String hotelId, String staysId, String gsId) throws AboutstaysException;
	public void validateUOSId(String uosId) throws AboutstaysException;
	public void ValidateMaintenanceServiceId(String mtServiceId) throws AboutstaysException;
	public void validateMasterGenereId(String genereId) throws AboutstaysException;
	public void validateReservationServiceId(String rsId) throws AboutstaysException;
	public void validateMasterChannelId(String id) throws AboutstaysException;
	public void validateEntServiceAndGenereId(String entServiceId, String genereId) throws AboutstaysException;
	public void validateEntService(String serviceId) throws AboutstaysException;
	public void validateUserIdStaysId(String userId, String staysId) throws AboutstaysException;
	public void validateReservationItemId(String reservationItemId) throws AboutstaysException;
	public void validateWingId(String wingId,String hotelId) throws AboutstaysException;
	public void validateFloorId(String floorId,String wingId,String hotelId) throws AboutstaysException;
	public void validateRoomCategoryId(String roomCategoryId, String hotelId) throws AboutstaysException;
	public void validateReservationCategoryId(String categoryId, String rsId) throws AboutstaysException;
	public void validateReservationItemId(String id, String categoryId, String hotelId) throws AboutstaysException;
	public void validateAmenitiesList(List<String> amenityIds, AmenityType amenityType, AboutstaysResponseCode aboutstaysResponseCode) throws AboutstaysException;
	public void validateAmenityId(String amenityId, AmenityType amenityType, AboutstaysResponseCode aboutstaysResponseCode) throws AboutstaysException;
	public void validateHotelIdStaysId(String hotelId, String staysId) throws AboutstaysException;
}
