package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddInternetServiceRequest;
import com.aboutstays.request.dto.UpdateInternetServiceRequest;
import com.aboutstays.response.dto.AddInternetServiceResponse;
import com.aboutstays.response.dto.GetInternetServiceResponse;

public interface InternetServicesService {
	
	public AddInternetServiceResponse add(AddInternetServiceRequest request);
	public List<GetInternetServiceResponse> getAll();
	public GetInternetServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateInternetServiceRequest request);
}
