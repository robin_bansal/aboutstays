package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.ReservationServiceEntity;
import com.aboutstays.request.dto.UpdateReservationServiceRequest;
import com.aboutstays.response.dto.GetReservationServiceResponse;
import com.aboutstays.services.impl.AddReservationServiceRequest;

public interface ReservationServicesService {
	
	public String add(AddReservationServiceRequest request);
	public List<GetReservationServiceResponse> getAll();
	public GetReservationServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateReservationServiceRequest request);
	public ReservationServiceEntity getEntity(String cityGuideId);

}
