package com.aboutstays.services;

import com.aboutstays.entities.CheckinEntity;
import com.aboutstays.enums.CheckinRequestStatus;
import com.aboutstays.request.dto.InitiateCheckinRequest;
import com.aboutstays.request.dto.ModifyCheckinRequest;
import com.aboutstays.response.dto.CheckinResponse;

public interface CheckinService {

	public String save(InitiateCheckinRequest request);
	public String update(ModifyCheckinRequest request);
	public void updateNonNull(ModifyCheckinRequest request);
	public CheckinResponse getById(String id);
	public CheckinEntity getEntity(String id);
	public void delete(String id);
	public void changeStatus(String id, CheckinRequestStatus checkinRequestStatus);
	public CheckinResponse getByUserStaysHotelId(String userId, String staysId, String hotelId);
	public void updatePriceAndIsApproved(boolean isApproved, double price, String staysId);
}
