package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddWingRequest;
import com.aboutstays.response.dto.AddWingResponse;
import com.aboutstays.response.dto.GetWingResponse;

public interface WingService {
	public AddWingResponse addWing(AddWingRequest request);
	public GetWingResponse getWingById(String wingId);
	public List<GetWingResponse> getAllWings();
	public boolean existById(String id);
	public boolean delete(String wingId);
	public boolean updateWing(GetWingResponse request);
	public List<GetWingResponse> getByHotelId(String hotelId);
	public boolean exists(String wingId, String hotelId);
}
