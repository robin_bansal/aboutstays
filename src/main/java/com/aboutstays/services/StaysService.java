package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.Booking;
import com.aboutstays.entities.Stays;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.request.dto.AddStaysRequest;
import com.aboutstays.request.dto.RoomgsAndStaysRequest;
import com.aboutstays.response.dto.AddStaysResponse;
import com.aboutstays.response.dto.GetStaysResponse;
import com.aboutstays.response.dto.GetStaysWithHotelInfoResponse;
import com.aboutstays.response.dto.RoomAndStaysData;
import com.aboutstays.response.dto.TotalNightsAndStaysResponse;

public interface StaysService {

	public AddStaysResponse addStays(AddStaysRequest reqeust) throws AboutstaysException;
	public boolean existsByStaysId(String staysId);
	public boolean deleteStays(String staysId);
	public GetStaysResponse getAllStays() throws AboutstaysException;
	public GetStaysResponse getStaysByUserId(String userId) throws AboutstaysException;
	public GetStayResponse getStayByStayId(String stayId);
	public GetStayResponse getStayByStayBookingId(String bookingId);
	public GetStaysWithHotelInfoResponse getStaysWithHotelInfo(String userId);
	public GetStaysWithHotelInfoResponse getAllStaysWithHotelInfo();
	public boolean existsByBookingId(String bookingId);
	public TotalNightsAndStaysResponse getTotalNightsAndStays(String userId) throws AboutstaysException;
	public List<Stays> getStaysOfBooking(String bookingId);
	public boolean existsByUserIdAndBookingId(String userId, String bookingId);
	public void changeStaysStatus(String staysId, StaysStatus staying);
	public void changeTripType(String staysId, boolean isOfficial);
	public List<RoomAndStaysData> getRoomsAndStays(RoomgsAndStaysRequest request);
	public GetStayResponse getChatStayByStayId(String staysId);
	public String addStayForBooking(Booking booking);
	public void setUserId(String staysId, String userId);
	public Stays getEntityById(String staysId);
	public String getBookingIdByStaysId(String staysId) throws AboutstaysException;
	public void setRoomId(String defaultStaysId, String roomId);
}
