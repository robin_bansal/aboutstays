package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.AirportPickup;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddAirportPickupRequest;
import com.aboutstays.request.dto.ApprovePickupRequest;
import com.aboutstays.request.dto.GetAirportPickupResponse;
import com.aboutstays.request.dto.UpdateAirportPickupRequest;
import com.aboutstays.response.dto.AddAirportPickupResponse;

public interface AirportPickupService {
	
	public AddAirportPickupResponse add(AddAirportPickupRequest request);
	public List<GetAirportPickupResponse> getAll(String userId);
	public GetAirportPickupResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateAirportPickupRequest request);
	public boolean approvePickupRequest(ApprovePickupRequest request);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	public AirportPickup getEntity(String id);

}
