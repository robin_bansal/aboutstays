package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AirportRequest;
import com.aboutstays.response.dto.AirportResponse;

public interface AirportService {

	public String addAirport(AirportRequest request);
	public void updateAirport(AirportRequest request);
	public void updateNonNullAirport(AirportRequest request);
	public void deleteAirport(String id);
	public AirportResponse getAirport(String id);
	public List<AirportResponse> getAll();
	public List<AirportResponse> getAllByCityId(String cityId);
	public boolean existsById(String airportId);
}
