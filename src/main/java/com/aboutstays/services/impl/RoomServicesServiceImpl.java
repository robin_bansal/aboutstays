package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.RoomServicesEntity;
import com.aboutstays.mao.RoomServiceMao;
import com.aboutstays.request.dto.AddRoomServiceRequest;
import com.aboutstays.request.dto.GetRoomServiceResponse;
import com.aboutstays.request.dto.UpdateRoomServiceRequest;
import com.aboutstays.response.dto.AddRoomServiceResponse;
import com.aboutstays.services.RoomServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.RoomServiceConverter;

@Service
public class RoomServicesServiceImpl implements RoomServicesService{
	
	@Autowired
	private RoomServiceMao roomServiceMao;

	@Override
	public AddRoomServiceResponse add(AddRoomServiceRequest request) {
		RoomServicesEntity services = RoomServiceConverter.convertBeanToEntity(request);
		if(services != null){
			roomServiceMao.add(services);
			return new AddRoomServiceResponse(services.getHotelId());
		}
		return null;
	}

	@Override
	public List<GetRoomServiceResponse> getAll() {
		List<RoomServicesEntity> listServices = roomServiceMao.getAll();
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetRoomServiceResponse> listResponse = RoomServiceConverter.convertListEntityToListBean(listServices);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetRoomServiceResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			RoomServicesEntity service = roomServiceMao.getById(id);
			if(service != null){
				GetRoomServiceResponse response = RoomServiceConverter.convertEntityToBean(service);
				return response;
			}
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(roomServiceMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			roomServiceMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateRoomServiceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			RoomServicesEntity service = RoomServiceConverter.convertUpdateBeanToEntity(request);
			if(CollectionUtils.isEmpty(request.getListMenuItem())){
				service.setListMenuItem(new ArrayList<>());
			}
			roomServiceMao.update(service);
			return true;
		}		
		return false;
	}

	@Override
	public RoomServicesEntity getEntity(String roomServicesId) {
		return roomServiceMao.getById(roomServicesId);
	}
	
	

}
