package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Wing;
import com.aboutstays.mao.FloorMao;
import com.aboutstays.mao.RoomMao;
import com.aboutstays.mao.WingMao;
import com.aboutstays.request.dto.AddWingRequest;
import com.aboutstays.response.dto.AddWingResponse;
import com.aboutstays.response.dto.GetWingResponse;
import com.aboutstays.services.WingService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.WingsConverter;

@Service
public class WingServiceImpl implements WingService {

	@Autowired
	private WingMao wingsMao;
	
	@Autowired
	private FloorMao floorMao;
	
	@Autowired
	private RoomMao roomMao;

	@Override
	public AddWingResponse addWing(AddWingRequest request) {
		if (request != null) {
			Wing wing = WingsConverter.convertRequestToEntity(request);
			wingsMao.add(wing);
			return new AddWingResponse(wing.getId());
		}
		return null;
	}

	@Override
	public GetWingResponse getWingById(String wingId) {
		Wing wing = wingsMao.getWingById(wingId);
		return parseEntityToGetResponse(wing);
	}

	@Override
	public List<GetWingResponse> getAllWings() {
		List<Wing> wings = wingsMao.getAll();
		return parseEntityListToGetList(wings);
	}

	@Override
	public boolean existById(String id) {
		if (wingsMao.getWingById(id) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(String wingId) {
		if (!StringUtils.isEmpty(wingId)) {
			wingsMao.delete(wingId);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateWing(GetWingResponse request) {
		if (request != null && !StringUtils.isEmpty(request.getWingId())) {
			Wing wing = WingsConverter.convertRequestToEntity(request);
			if (wing != null) {
				wingsMao.updateById(wing, request.getWingId());
				return true;
			}
		}
		return false;
	}

	@Override
	public List<GetWingResponse> getByHotelId(String hotelId) {
		List<Wing> wings = wingsMao.getByHotelId(hotelId);
		return parseEntityListToGetList(wings);
	}
	
	private List<GetWingResponse> parseEntityListToGetList(List<Wing> wings){
		if(CollectionUtils.isEmpty(wings)){
			return null;
		}
		List<GetWingResponse> response = new ArrayList<>(wings.size());
		for(Wing wing : wings){
			response.add(parseEntityToGetResponse(wing));
		}
		return response;
	}

	private GetWingResponse parseEntityToGetResponse(Wing wing) {
		GetWingResponse response = WingsConverter.convertentityToResponse(wing);
		if(response!=null){
			int floorsCount = floorMao.getCountByWingId(wing.getId());
			response.setFloorsCount(floorsCount);
			int roomsCount = roomMao.getCountByWingId(wing.getId());
			response.setRoomsCount(roomsCount);
		}
		return response;
	}

	@Override
	public boolean exists(String wingId, String hotelId) {
		Wing wing = wingsMao.getByWingAndHotelId(wingId,hotelId);
		if(wing!=null)
			return true;
		return false;
	}

}
