package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Car;
import com.aboutstays.mao.CarMao;
import com.aboutstays.request.dto.AddCarRequest;
import com.aboutstays.request.dto.UpdateCarRequest;
import com.aboutstays.response.dto.AddCarResponse;
import com.aboutstays.response.dto.GetCarResponse;
import com.aboutstays.services.CarService;
import com.aboutstays.utils.CarConverter;
import com.aboutstays.utils.CollectionUtils;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarMao carMao;
	
	
	@Override
	public AddCarResponse add(AddCarRequest request) {
		if(request != null){
			Car car = CarConverter.convertBeanToEntity(request);
			carMao.add(car);
			return new AddCarResponse(car.getId());
		}
		return null;
	}

	@Override
	public List<GetCarResponse> getAll() {
		List<Car> listCar = carMao.getAll();
		if(CollectionUtils.isNotEmpty(listCar)){
			List<GetCarResponse> listResponse = CarConverter.convertListEntityToListBean(listCar);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetCarResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			Car car = carMao.getById(id);
			GetCarResponse response = CarConverter.convertEntityToBean(car);
			return response;
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			carMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateCarRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			Car car = CarConverter.convertUpdateRequestToEntity(request);
			carMao.updateNonNullById(car, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public boolean existsByNumberPlate(String numberPlate) {
		if(!StringUtils.isEmpty(numberPlate)){
			if(carMao.getByNumberPlate(numberPlate) != null){
				return true;
			}
		}
		return false;
	}

}
