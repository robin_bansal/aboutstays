package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.User;
import com.aboutstays.enums.StayPreferencesType;
import com.aboutstays.enums.Type;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.UserMao;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.PreferencesType;
import com.aboutstays.pojos.StayPreferences;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.SignupRequest;
import com.aboutstays.request.dto.UpdateContactDetails;
import com.aboutstays.request.dto.UpdatePasswordRequest;
import com.aboutstays.request.dto.UpdatePersonalDetails;
import com.aboutstays.request.dto.UpdateStayPrefsRequest;
import com.aboutstays.request.dto.UpdateUserRequest;
import com.aboutstays.request.dto.ValidateOtpRequest;
import com.aboutstays.response.dto.LoginResponse;
import com.aboutstays.response.dto.SignupResponse;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.UserConverter;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMao userMao;
	
	@Override
	public SignupResponse signupUser(SignupRequest signupRequest) {
		User user = UserConverter.convertSignupRequestToEntity(signupRequest);
		if(user!=null){
			userMao.save(user);
			return UserConverter.convertEntityToSignupResponse(user);
		}
		return null;
	}

	@Override
	public boolean existsByNumber(String number) {
		User user = userMao.getByPhoneNumber(number);
		if(user != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean existsByEmailOrPhone(String emailId, String number) {
		User user = userMao.getByEmailOrPhone(emailId, number);
		if(user != null){
			return true;
		}
		return false;
	}

	@Override
	public UserPojo getByEmailOrPhone(String emailId, String number) {
		User user = userMao.getByEmailOrPhone(emailId, number);
		return UserConverter.convertUserToUserPojo(user);
	}

	@Override
	public LoginResponse matchCredentials(Type type, String value) {
		User user = userMao.matchCredentials(type, value);
		return UserConverter.convertEntityToLoginResponse(user);
		
	}

	@Override
	public UserPojo getByEmail(String emailId) {
		User user = userMao.getByEmail(emailId);
		if(user!=null){
			return UserConverter.convertUserToUserPojo(user);
		}
		return null;
	}
	
	@Override
	public UserPojo getByNumber(String mobileNumber) {
		User user = userMao.getByPhoneNumber(mobileNumber);
		if(user != null){
			return UserConverter.convertUserToUserPojo(user);
		}
		return null;
	}

	@Override
	public boolean updateOtpByEmailOrPhone(ValidateOtpRequest validateOtpRequest) {
		if(validateOtpRequest == null)
			return false;
		if(Type.findTypeByCode(validateOtpRequest.getType()) == Type.EMAIL){
			userMao.updateOtpByEmail(validateOtpRequest.getValue());
			return true;
		} else if(Type.findTypeByCode(validateOtpRequest.getType()) == Type.PHONE){
			userMao.updateOtpByNumber(validateOtpRequest.getValue());
			return true;
		}
		return false;
		
	}

	@Override
	public boolean updateUserByType(UpdateUserRequest updateUserRequest, Type typeEnum, String value){
		if(updateUserRequest == null){
			return false;
		}
		User user = UserConverter.convertUpdateUserRequestToUser(updateUserRequest);
		user.setListStayPreferences(getPreferencesCode(updateUserRequest.getListStayPreferences()));
		userMao.updateNonNullFields(user,typeEnum, value);
		return true;
	}

	@Override
	public boolean existsByUserId(String userId) {
		if(userId == null){
			return false;
		}
		User user = userMao.getByUserId(userId);
		if(user != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean existsByEmail(String emailId) {
		if(emailId == null){
			return false;
		}
		User user = userMao.getByEmail(emailId);
		if(user != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean updatePasswordByType(UpdatePasswordRequest updatePasswordRequest, Type typeEnum, String value) {
		if(updatePasswordRequest == null){
			return false;
		}
		User user = UserConverter.convertUpdatePasswordRequestToUser(updatePasswordRequest);
		if(user == null){
			return false;
		}
		userMao.updatePasswordByType(user, typeEnum, value);
		return true;
	}

	@Override
	public boolean updatePersonalDetails(UpdatePersonalDetails request) throws AboutstaysException {
		if(request == null){
			return false;
		}
		User user = UserConverter.convertPersonalDetailsToEntity(request);
		if(user == null){
			return false;
		}
		userMao.editProfileByUserID(user, request.getUserId());
		return true;
	}

	@Override
	public boolean updateContactDetails(UpdateContactDetails request) {
		if(request == null){
			return false;
		}
		User user = UserConverter.convertContactDetailsToEntity(request);
		userMao.editProfileByUserID(user, request.getUserId());
		return true;
	}

	public List<PreferencesType> getPreferencesCode(List<StayPreferences> listStayPreferences){
		List<PreferencesType> listPreferencesCodes = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(listStayPreferences)) {
			for(StayPreferences prefs : listStayPreferences) {
				if(prefs == null) {
					continue;
				}
				StayPreferencesType type = StayPreferencesType.getByDisplayName(prefs.getName());
				if(type == null){
					continue;
				}
				PreferencesType prefsType = new PreferencesType();
				prefsType.setPreference(type.getCode());
				listPreferencesCodes.add(prefsType);
			}
		}
		return listPreferencesCodes;
	}
	
	@Override
	public boolean updateStayPrefs(UpdateStayPrefsRequest updateStayPrefsRequest) {
		if(CollectionUtils.isEmpty(updateStayPrefsRequest.getListStayPrefs())){
			userMao.update(updateStayPrefsRequest.getUserId(), null);
		} else
			userMao.update(updateStayPrefsRequest.getUserId(), getPreferencesCode(updateStayPrefsRequest.getListStayPrefs()));
		return true;
	}

	@Override
	public User getUserEntity(String userId) {
		return userMao.getById(userId);
	}

	@Override
	public boolean updateDocs(List<IdentityDoc> identityDocs, String userId) {
		if(StringUtils.isEmpty(userId)){
			return false;
		}
		userMao.updateDocs(identityDocs, userId);
		return true;
	}

	@Override
	public boolean updateProfilePicture(String userId, String imageUrl) {
		if(StringUtils.isEmpty(userId)){
			return false;
		}
		userMao.updateProfilePicture(userId, imageUrl);
		return true;
	}

	@Override
	public UserPojo getById(String userId) {
		User user = userMao.getById(userId);
		return UserConverter.convertUserToUserPojo(user);
	}

}
