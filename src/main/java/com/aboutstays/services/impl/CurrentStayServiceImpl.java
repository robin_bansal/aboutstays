package com.aboutstays.services.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.constants.StringConstants;
import com.aboutstays.controller.CheckinController;
import com.aboutstays.controller.CommuteOrder;
import com.aboutstays.controller.RequestDashboardController;
import com.aboutstays.entities.AirportPickup;
import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.entities.FoodOrder;
import com.aboutstays.entities.GeneralService;
import com.aboutstays.entities.Hotel;
import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.entities.InternetOrder;
import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.entities.Stays;
import com.aboutstays.entities.User;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.enums.PartnershipType;
import com.aboutstays.enums.StaysOrdering;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.AirportPickupMao;
import com.aboutstays.mao.BellBoyOrderMao;
import com.aboutstays.mao.CommuteOrderMao;
import com.aboutstays.mao.FoodRequestsMao;
import com.aboutstays.mao.GeneralServiceMao;
import com.aboutstays.mao.HKOrderMao;
import com.aboutstays.mao.HotelMao;
import com.aboutstays.mao.InternetOrderMao;
import com.aboutstays.mao.LateCheckoutRequestMao;
import com.aboutstays.mao.LaundryOrderMao;
import com.aboutstays.mao.MaintenanceRequestMao;
import com.aboutstays.mao.ReservationOrderMao;
import com.aboutstays.mao.StaysMao;
import com.aboutstays.mao.UserMao;
import com.aboutstays.mao.UserOptedServicesMao;
import com.aboutstays.request.dto.CreateDashboardRequest;
import com.aboutstays.request.dto.GetAllStaysServiceRequest;
import com.aboutstays.request.dto.RetreiveCheckinRequest;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CreateDashboardResponse;
import com.aboutstays.response.dto.GetStaysServiceListResponse;
import com.aboutstays.response.dto.GetStaysServiceResponse;
import com.aboutstays.services.CurrentStayService;
import com.aboutstays.services.ProcessUtilityService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.GeneralServicesConverter;
import com.aboutstays.utils.StaysConverter;

@Service
public class CurrentStayServiceImpl implements CurrentStayService {
	
	@Autowired
	private StaysMao staysMao;
	
	@Autowired
	private HotelMao hotelMao;
	
	@Autowired
	private GeneralServiceMao generalServiceMao;
	
	@Autowired
	private UserOptedServicesMao userOptedServicesMao;
	
	@Autowired
	private UserMao userMao;
	
	@Autowired
	private ProcessUtilityService processUtilityService;
	
	@Autowired
	private AirportPickupMao airportPickupMao;
	
	@Autowired
	private BellBoyOrderMao bellBoyOrderMao;
	
	@Autowired
	private CommuteOrderMao commuteOrderMao; 
	
	@Autowired
	private HKOrderMao housekeepingOrderMao;
	
	@Autowired
	private InternetOrderMao internetOrderMao;
	
	@Autowired
	private LateCheckoutRequestMao lateCheckoutRequestMao;
	
	@Autowired
	private LaundryOrderMao laundryOrderMao;
	
	@Autowired
	private MaintenanceRequestMao maintenanceRequestMao;
	
	@Autowired
	private ReservationOrderMao reservationOrderMao;
	
	@Autowired
	private FoodRequestsMao foodRequestsMao; 
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Override
	public GetStaysServiceListResponse getAllCurrentStaysServices(GetAllStaysServiceRequest request) {
		if(request==null)
			return null;
		long currentTime = new Date().getTime();
		GetStaysServiceListResponse response = new GetStaysServiceListResponse();
		Stays stays = staysMao.getByStaysId(request.getStaysId());
		Hotel hotel = hotelMao.getHotelById(request.getHotelId());
		User user = userMao.getById(request.getUserId());
		
		if(stays!=null&&hotel!=null&&user!=null){
			StaysStatus staysStatus = StaysStatus.findStatusByCode(stays.getStaysStatus());
			if(staysStatus!=null){
				int futureStayHours = processUtilityService.getIntegerProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_FUTURE_STAYLINE_HOURS, 24);
				StaysOrdering staysOrdering = StaysConverter.getStaysOrdering(stays, currentTime, futureStayHours);
				switch (staysStatus) {
				case CHECK_IN:
					if(StaysOrdering.CURRENT.equals(staysOrdering)
							&& hotel.getPartnershipType() == PartnershipType.PARTNER.getCode()){
						response.setShowCheckInButton(true);
						response.setShowCheckoutButton(false);
						response.setCheckinButtonText(StringConstants.INITIATE_CHECK_IN);
						try{
							CheckinController checkinController = new CheckinController();
							beanFactory.autowireBean(checkinController);
							RetreiveCheckinRequest retreiveCheckinRequest = new RetreiveCheckinRequest();
//							retreiveCheckinRequest.setHotelId(request.getHotelId());
//							retreiveCheckinRequest.setUserId(request.getUserId());
							retreiveCheckinRequest.setStaysId(request.getStaysId());
							checkinController.retreiveCheckin(retreiveCheckinRequest);
							response.setCheckinButtonText(StringConstants.MODIFY_CHECKIN);
						} catch(AboutstaysException ae){
							ae.printStackTrace();
							response.setCheckinButtonText(StringConstants.INITIATE_CHECK_IN);
						}
					} else {
						response.setShowCheckInButton(false);
						response.setShowCheckoutButton(false);
					}
					
					break;
				case CHECK_OUT:
					response.setShowCheckInButton(false);
					response.setShowCheckoutButton(true);
					break;
				case STAYING:
					response.setShowCheckInButton(false);
					response.setShowCheckoutButton(false);
					break;
				}
				
				List<String> hotelSupportedGsIds = new ArrayList<>();
//				Map<String, String> gsIdsVsSupportedServicesIdMap = new HashMap<>();
				if(hotel.getCheckInDetails()!=null&&CollectionUtils.isNotEmpty(hotel.getCheckInDetails().getListGSIds())){
					hotelSupportedGsIds.addAll(hotel.getCheckInDetails().getListGSIds());
//					if(CollectionUtils.isMapNotEmpty(hotel.getCheckInDetails().getMapGSIdVsHSId()))
//						gsIdsVsSupportedServicesIdMap.putAll(hotel.getCheckInDetails().getMapGSIdVsHSId());
				}
				if(hotel.getStaysDetails()!=null&&CollectionUtils.isNotEmpty(hotel.getStaysDetails().getListGSIds())){
					hotelSupportedGsIds.addAll(hotel.getStaysDetails().getListGSIds());
//					if(CollectionUtils.isMapNotEmpty(hotel.getStaysDetails().getMapGSIdVsHSId()))
//						gsIdsVsSupportedServicesIdMap.putAll(hotel.getStaysDetails().getMapGSIdVsHSId());
				}
				if(hotel.getCheckOutDetails()!=null&&CollectionUtils.isNotEmpty(hotel.getCheckOutDetails().getListGSIds())){
					hotelSupportedGsIds.addAll(hotel.getCheckOutDetails().getListGSIds());
//					if(CollectionUtils.isMapNotEmpty(hotel.getCheckOutDetails().getMapGSIdVsHSId()))
//						gsIdsVsSupportedServicesIdMap.putAll(hotel.getCheckOutDetails().getMapGSIdVsHSId());
				}
				
				if(CollectionUtils.isNotEmpty(hotelSupportedGsIds)){
					
					List<UserOptedService> userOptedServicesList = userOptedServicesMao.getByUserStayAndHotel(request.getStaysId(), request.getUserId(), request.getHotelId());
					Map<String, List<UserOptedService>> gsIdVsUserOptedServicesMap = new HashMap<>();
					if(CollectionUtils.isNotEmpty(userOptedServicesList)){
						for(UserOptedService userOptedService : userOptedServicesList){
							String gsId = userOptedService.getGsId();
							List<UserOptedService> uosListByGsId = gsIdVsUserOptedServicesMap.get(gsId);
							if(uosListByGsId==null){
								uosListByGsId = new ArrayList<>();
							}
							uosListByGsId.add(userOptedService);
							gsIdVsUserOptedServicesMap.put(gsId, uosListByGsId);
						}
					}
					
					List<GetStaysServiceResponse> responseList = new ArrayList<>();
					boolean showUnactivatedService = processUtilityService.getBooleanProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_SHOW_UNACTIVATED_SERVICES, false);
					int enableCheckoutServiceHours = processUtilityService.getIntegerProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_ENABLE_CHECKOUT_SERVICES_HOURS, 24);
					for(String gsId : hotelSupportedGsIds){
						GeneralService generalService = generalServiceMao.getById(gsId);
						if(generalService!=null&&GeneralServiceType.findGeneralServiceByType(generalService.getServiceType())!=null){
							GeneralServiceType generalServiceType = GeneralServiceType.findGeneralServiceByType(generalService.getServiceType());
							if(!StaysOrdering.isValid(staysOrdering,generalService)){
								continue;
							}
							if(!PartnershipType.isValid(hotel.getPartnershipType(),generalService)){
								continue;
							}
							GetStaysServiceResponse getStaysServiceResponse = GeneralServicesConverter.convertEntityToGetStaysServiceResponse(generalService);
														
							PartnershipType partnershipType = PartnershipType.findValueByCode(hotel.getPartnershipType());
							if(partnershipType == null) {
								continue;
							}
							
							switch(partnershipType) {
							case NOT_PARTNER:
								getStaysServiceResponse.setClickable(true);
								getStaysServiceResponse.setServiceImageUrl(generalService.getActivatedImageUrl());
								break;
							case PARTNER:
								if(stays.getStaysStatus().equals(generalService.getStatus())){
									getStaysServiceResponse.setClickable(true);
									getStaysServiceResponse.setServiceImageUrl(generalService.getActivatedImageUrl());
								} else {
									if(StaysStatus.STAYING.equals(staysStatus)){
										
										try {
											Date date = DateUtil.parseDate(hotel.getGeneralInformation().getCheckOutTime(), "HH:mm");
											Calendar c = Calendar.getInstance();
											c.setTime(date);
											Date checkoutDateTime = DateUtil.modifyDate(stays.getCheckOutDate(), c.get(Calendar.HOUR_OF_DAY), Calendar.MINUTE, Calendar.SECOND); 
											long timeBeforeCheckout = checkoutDateTime.getTime() - currentTime;
											if(timeBeforeCheckout<enableCheckoutServiceHours*3600*1000&&!StaysStatus.CHECK_IN.equals(StaysStatus.findStatusByCode(generalService.getStatus()))){
												getStaysServiceResponse.setClickable(true);
												getStaysServiceResponse.setServiceImageUrl(generalService.getActivatedImageUrl());
												response.setShowCheckInButton(false);
												response.setShowCheckoutButton(true);
											} else {
												response.setShowCheckInButton(false);
												response.setShowCheckoutButton(false);
												if(!showUnactivatedService){
													continue;
												}
												getStaysServiceResponse.setClickable(false);
												getStaysServiceResponse.setServiceImageUrl(generalService.getUnactivatedImageUrl());
											}
										} catch (ParseException e) {
											e.printStackTrace();
											if(!showUnactivatedService){
												continue;
											}
											getStaysServiceResponse.setClickable(false);
											getStaysServiceResponse.setServiceImageUrl(generalService.getUnactivatedImageUrl());
										}
									} else if(StaysStatus.CHECK_OUT.equals(staysStatus)) {
										getStaysServiceResponse.setClickable(true);
										getStaysServiceResponse.setServiceImageUrl(generalService.getActivatedImageUrl());
										
									} else {
										if(!showUnactivatedService){
											continue;
										}
										getStaysServiceResponse.setClickable(false);
										getStaysServiceResponse.setServiceImageUrl(generalService.getUnactivatedImageUrl());
									}
									
								}
								break;
							}
							
							int countToShow = 0;
							boolean showTick = false;
							boolean showNumber = false;
							String serviceImageUrl = null;
							if(generalService.getGeneralInfo()){//If it is general information type service
								showTick = false;
								showNumber = false;
								countToShow = 0;
								serviceImageUrl = generalService.getActivatedImageUrl();
							} else {
								List<UserOptedService> uosListByGsId = gsIdVsUserOptedServicesMap.get(gsId);
								if(CollectionUtils.isEmpty(uosListByGsId)){
									showTick = false;
									showNumber = false;
									countToShow = 0;
									serviceImageUrl = generalService.getUnactivatedImageUrl();
								}
								switch(generalServiceType){
								case AIRPORT_DROP:
								case AIRPORT_PICKUP:
									serviceImageUrl = generalService.getActivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											AirportPickup airportPickup = airportPickupMao.getById(refId);
											if(airportPickup!=null&&!GeneralOrderStatus.CANCELLED.
													equals(GeneralOrderStatus.findByCode(airportPickup.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												if(showNumber){
													countToShow++;
												}
												getStaysServiceResponse.setRelevantUosId(userOptedService.getId());
												getStaysServiceResponse.setReferenceId(userOptedService.getRefId());
												serviceImageUrl = generalService.getActivatedImageUrl();
											} 
										}
									}
									break;
								case BELL_BOY:
									showTick = generalService.isTickable();
									showNumber = generalService.isNumerable();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									} 
									if(showNumber){
										if(CollectionUtils.isNotEmpty(uosListByGsId)){
											for(UserOptedService userOptedService : uosListByGsId){
												String refId = userOptedService.getRefId();
												BellBoyOrder bBoyOrder = bellBoyOrderMao.getById(refId);
												if(bBoyOrder!=null&&!GeneralOrderStatus.CANCELLED.
														equals(GeneralOrderStatus.findByCode(bBoyOrder.getStatus()))){
													countToShow++;
												} 
											}
										}
									} 
									serviceImageUrl = generalService.getActivatedImageUrl();
									break;
								case COMMUTE:
									showTick = generalService.isTickable();
									showNumber = generalService.isNumerable();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(showNumber){
										if(CollectionUtils.isNotEmpty(uosListByGsId)){
											for(UserOptedService userOptedService : uosListByGsId){
												String refId = userOptedService.getRefId();
												CommuteOrder commuteOrder = commuteOrderMao.getById(refId);
												if(commuteOrder!=null&&!GeneralOrderStatus.CANCELLED.
														equals(GeneralOrderStatus.findByCode(commuteOrder.getStatus()))){
													countToShow++;
												} 
											}
										}
									}
									serviceImageUrl = generalService.getActivatedImageUrl();
									break;
								case HOUSEKEEPING:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											HousekeepingOrder order = housekeepingOrderMao.getById(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.
														equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case INTERNET:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											InternetOrder order = internetOrderMao.get(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.
													equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case LATE_CHECKOUT:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											LateCheckoutRequest order = lateCheckoutRequestMao.get(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.
													equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case LAUNDRY:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											LaundryOrder order = laundryOrderMao.getById(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.
													equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case MAINTENANCE:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											MaintenanceRequestEntity order = maintenanceRequestMao.getById(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.
													equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case REQUEST:
								case REQUESTS_CHECKIN:	
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									RequestDashboardController requestDashboardController = new RequestDashboardController();
									beanFactory.autowireBean(requestDashboardController);
									
									try {
										CreateDashboardRequest createDashboardRequest = new CreateDashboardRequest();
										createDashboardRequest.setHotelId(request.getHotelId());
										createDashboardRequest.setUserId(request.getUserId());
										createDashboardRequest.setStaysId(request.getStaysId());
										BaseApiResponse<CreateDashboardResponse> createDashboardResponse = requestDashboardController.createDashboard(createDashboardRequest);
										showTick = generalService.isTickable();
										showNumber = generalService.isNumerable();
										serviceImageUrl = generalService.getActivatedImageUrl();
										if(CollectionUtils.isNotEmpty(createDashboardResponse.getData().getOpenDashboardItems())){
											countToShow = createDashboardResponse.getData().getOpenDashboardItems().size();
										} 
										if(CollectionUtils.isNotEmpty(createDashboardResponse.getData().getScheduledDashboardItems())){
											countToShow += createDashboardResponse.getData().getScheduledDashboardItems().size();
										}
										if(countToShow==0) {
											showNumber = false;
											serviceImageUrl = generalService.getUnactivatedImageUrl();
										}
										
									} catch (AboutstaysException e) {
										showNumber = false;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									break;
								case RESERVATION:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									showTick = generalService.isTickable();
									showNumber = generalService.isNumerable();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(showNumber&&CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											ReservationOrder order = reservationOrderMao.getById(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case ROOM_SERVICE:
									serviceImageUrl = generalService.getUnactivatedImageUrl();
									showTick = generalService.isTickable();
									showNumber = generalService.isNumerable();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									if(showNumber&&CollectionUtils.isNotEmpty(uosListByGsId)){
										for(UserOptedService userOptedService : uosListByGsId){
											String refId = userOptedService.getRefId();
											FoodOrder order = foodRequestsMao.getById(refId);
											if(order!=null&&!GeneralOrderStatus.CANCELLED.equals(GeneralOrderStatus.findByCode(order.getStatus()))){
												showTick = generalService.isTickable();
												showNumber = generalService.isNumerable();
												serviceImageUrl = generalService.getActivatedImageUrl();
												if(showNumber){
													countToShow++;
												} else {
													break;
												}
											}
										}
									}
									break;
								case CITY_GUIDE:
								case COMPLEMENTRY_SERVICE:
								case EXPENSE:
								case EXPENSE_CHECKOUT:
								case FEEDBACK_AND_SUGGESTION:
								case HOTEL_INFO:
								case HOTEL_INFO_STAYS:
								case IN_HOTEL_NAVIGATION:
								case HOUSE_RULES:
								case TV_ENTERTAINMENT:
								case MESSAGE:
								case MESSAGE_CURRENT_STAY:	
								case NOTIFICATION:
								case NOTIFICATIONS_CHECKIN:
									showTick = generalService.isTickable();
									showNumber = generalService.isNumerable();
									countToShow = 0;
									serviceImageUrl = generalService.getActivatedImageUrl();
									if(CollectionUtils.isEmpty(uosListByGsId)){
										showTick = false;
										showNumber = false;
										countToShow = 0;
										serviceImageUrl = generalService.getUnactivatedImageUrl();
									}
									break;
								default:
									continue;	
								}
							}
							
							getStaysServiceResponse.setShowTick(showTick);
							getStaysServiceResponse.setShowNumber(showNumber);
							getStaysServiceResponse.setCount(countToShow);
//							getStaysServiceResponse.setServiceId(gsIdsVsSupportedServicesIdMap.get(gsId));
							getStaysServiceResponse.setServiceId(hotel.getId());
							getStaysServiceResponse.setServiceImageUrl(serviceImageUrl);
							responseList.add(getStaysServiceResponse);
						}
					}
					Collections.sort(responseList, new Comparator<GetStaysServiceResponse>() {
						@Override
						public int compare(GetStaysServiceResponse o1, GetStaysServiceResponse o2) {
							return o1.getUiOrder()-o2.getUiOrder();
						}
					});
					response.setServicesList(responseList);
					boolean showCheckoutButton = processUtilityService.getBooleanProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_SHOW_CHECKOUT_BUTTON, false);
					response.setShowCheckoutButton(showCheckoutButton);
					return response;
				}
			}
		}
		return null;
	}

}
