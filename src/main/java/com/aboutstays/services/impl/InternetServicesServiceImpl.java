package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.InternetService;
import com.aboutstays.mao.InternetServiceMao;
import com.aboutstays.request.dto.AddInternetServiceRequest;
import com.aboutstays.request.dto.UpdateInternetServiceRequest;
import com.aboutstays.response.dto.AddInternetServiceResponse;
import com.aboutstays.response.dto.GetInternetServiceResponse;
import com.aboutstays.services.InternetServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.InternetServiceConverter;

@Service
public class InternetServicesServiceImpl implements InternetServicesService{

	@Autowired
	private InternetServiceMao ineternetServicesMao;
	
	@Override
	public AddInternetServiceResponse add(AddInternetServiceRequest request) {
		InternetService packs = InternetServiceConverter.convertBeanToEntity(request);
		if(packs != null){
			ineternetServicesMao.add(packs);
			return new AddInternetServiceResponse(packs.getHotelId());
		}
		return null;
	}

	@Override
	public List<GetInternetServiceResponse> getAll() {
		List<InternetService> listServices = ineternetServicesMao.getAll();
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetInternetServiceResponse> listResponse = InternetServiceConverter.convertListEntityToListBean(listServices);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetInternetServiceResponse getById(String id) {
		InternetService service = ineternetServicesMao.getById(id);
		GetInternetServiceResponse response = InternetServiceConverter.convertEntityToBean(service);
		return response;
	}

	@Override
	public boolean existsById(String id) {
		if(ineternetServicesMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			ineternetServicesMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateInternetServiceRequest request) {
		InternetService service = InternetServiceConverter.convertUpdateRequestToEntity(request);
		if(service != null){
			if(CollectionUtils.isEmpty(service.getListPacks()))
				service.setListPacks(new ArrayList<>());
			ineternetServicesMao.update(service);
			return true;
		}
		return false;
	}

}
