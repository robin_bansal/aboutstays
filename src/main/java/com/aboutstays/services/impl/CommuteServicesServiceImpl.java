package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.CommuteService;
import com.aboutstays.mao.CommuteServiceMao;
import com.aboutstays.request.dto.AddCommuteServiceRequest;
import com.aboutstays.request.dto.UpdateCommuteServiceRequest;
import com.aboutstays.response.dto.GetCommuteServiceResponse;
import com.aboutstays.services.CommuteServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.CommuteServiceConverter;

@Service
public class CommuteServicesServiceImpl implements CommuteServicesService{

	@Autowired
	private CommuteServiceMao csMao;
	
	@Override
	public String add(AddCommuteServiceRequest request) {
		CommuteService service = CommuteServiceConverter.convertBeanToEntity(request);
		if(service != null){
			csMao.add(service);
			return service.getHotelId();
		}
		return null;
	}

	@Override
	public List<GetCommuteServiceResponse> getAll() {
		List<CommuteService> listService = csMao.getAll();
		return CommuteServiceConverter.convertListEntityToListBean(listService);
	}

	@Override
	public GetCommuteServiceResponse getById(String id) {
		CommuteService service = csMao.getById(id);
		return CommuteServiceConverter.convertEntityToBean(service);
	}

	@Override
	public boolean existsById(String id) {
		if(csMao.getById(id) == null){
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteById(String id) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		csMao.deletyById(id);
		return true;
	}

	@Override
	public boolean updateById(UpdateCommuteServiceRequest request) {
		CommuteService service = CommuteServiceConverter.convertUpdateRequestToEntity(request);
		if(service != null){
			if(CollectionUtils.isEmpty(service.getListCommutePackage())){
				service.setListCommutePackage(new ArrayList<>());
			}
			csMao.updateNonNullById(service, request.getId());
			return true;
		}
		return false;
	}

}
