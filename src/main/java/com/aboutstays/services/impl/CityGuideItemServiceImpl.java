package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.CityGuideItemEntity;
import com.aboutstays.enums.CityGuideItemType;
import com.aboutstays.mao.CityGuideItemMao;
import com.aboutstays.request.dto.AddCityGuideItemRequest;
import com.aboutstays.request.dto.UpdateCGItemRequest;
import com.aboutstays.response.dto.GetCityGuideItemResponse;
import com.aboutstays.services.CityGuideItemService;
import com.aboutstays.utils.CityGuideItemConverter;

@Service
public class CityGuideItemServiceImpl implements CityGuideItemService{

	@Autowired
	private CityGuideItemMao cgItemMao;
	
	@Override
	public String save(AddCityGuideItemRequest request) {
		CityGuideItemEntity entity = CityGuideItemConverter.convertBeanToEntity(request);
		if(entity != null){
			entity.setId(null);
			cgItemMao.save(entity);
			return entity.getId();
		}
		return null;
	}

	@Override
	public GetCityGuideItemResponse getById(String id) {
		CityGuideItemEntity entity = cgItemMao.getById(id);
		return CityGuideItemConverter.convertEntityToBean(entity);
	}

	@Override
	public List<GetCityGuideItemResponse> getAllCGItems() {
		List<CityGuideItemEntity> listEntity = cgItemMao.getAll();
		return CityGuideItemConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public void update(UpdateCGItemRequest request) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean updateNonNull(UpdateCGItemRequest request) {
		if(request != null && StringUtils.isEmpty(request.getId())){
			CityGuideItemEntity entity = CityGuideItemConverter.convertUpdateBeanToEntity(request);
			cgItemMao.updateNonNull(entity);
			return true;
		}
		return false;
	}

	@Override
	public List<GetCityGuideItemResponse> getAllByHotelId(String hotelId) {
		List<CityGuideItemEntity> listEntity = cgItemMao.getAllByHotelId(hotelId);
		return CityGuideItemConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public List<GetCityGuideItemResponse> getAllByCGId(String cgId) {
		List<CityGuideItemEntity> listEntity = cgItemMao.getAllByCityGuideId(cgId);
		return CityGuideItemConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public boolean existsById(String id) {
		if(cgItemMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public CityGuideItemEntity getEntityById(String id) {
		return cgItemMao.getById(id);
	}

	@Override
	public void updateEntity(CityGuideItemEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<GetCityGuideItemResponse> getAllByType(String cgId, CityGuideItemType cgItemType) {
		if(StringUtils.isEmpty(cgId) || cgItemType == null){
			return null;
		}
		List<CityGuideItemEntity> listEntity = cgItemMao.getAllByCityGuideId(cgId, cgItemType.getCode());
		return CityGuideItemConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			cgItemMao.delete(id);
			return true;
		}
		return false;
		
	}
	
	

}
