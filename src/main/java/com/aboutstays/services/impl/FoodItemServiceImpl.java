package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.FoodItem;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.mao.FoodItemMao;
import com.aboutstays.pojos.GoesWellWithItem;
import com.aboutstays.pojos.GoesWellWithItemPojo;
import com.aboutstays.request.dto.FoodItemRequest;
import com.aboutstays.request.dto.UpdateFoodItemRequest;
import com.aboutstays.response.dto.FoodItemResponse;
import com.aboutstays.services.FoodItemService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.FoodItemConverter;

@Service
public class FoodItemServiceImpl implements FoodItemService {

	@Autowired
	private FoodItemMao foodItemMao;
	
	@Override
	public String save(FoodItemRequest foodItemRequest) {
		FoodItem foodItem = FoodItemConverter.convertRequestToEntity(foodItemRequest);
		if(foodItem!=null){
			foodItem.setId(null);
			foodItemMao.save(foodItem);
			return foodItem.getId();
		}
		return null;
	}

	@Override
	public FoodItemResponse getById(String id) {
		FoodItem foodItem = foodItemMao.getById(id);
		return parseEntityToGetResponse(foodItem);
	}

	@Override
	public List<FoodItemResponse> getAllFoodItems() {
		List<FoodItem> foodItemList = foodItemMao.getAll();
		return parseEntityList(foodItemList);
	}

	@Override
	public void update(UpdateFoodItemRequest foodItemRequest) {
		FoodItem foodItem = FoodItemConverter.convertUpdateRequestToEntity(foodItemRequest);
		if(foodItem!=null){
			foodItemMao.update(foodItem);
		}
	}

	@Override
	public void updateNonNull(UpdateFoodItemRequest foodItemRequest) {
//		FoodItem foodItem = FoodItemConverter.convertUpdateRequestToEntity(foodItemRequest);
//		if(foodItem!=null){
//			foodItemMao.updateNonNull(foodItem);
//		}
	}

	@Override
	public List<FoodItemResponse> getAllByHotelId(String hotelId) {
		List<FoodItem> foodItemList = foodItemMao.getAllByHotelId(hotelId);
		return parseEntityList(foodItemList);
	}

	@Override
	public List<FoodItemResponse> getAllByRoomServicesId(String roomServicesId) {
		List<FoodItem> foodItemList = foodItemMao.getAllByRoomServicesId(roomServicesId);
		return parseEntityList(foodItemList);
	}
	
	public List<FoodItemResponse> parseEntityList(List<FoodItem> foodItemList){
		if(CollectionUtils.isNotEmpty(foodItemList)){
			List<FoodItemResponse> responseList = new ArrayList<>(foodItemList.size());
			for(FoodItem foodItem : foodItemList){
				responseList.add(parseEntityToGetResponse(foodItem));
			}
			return responseList;
		}
		return null;
	}
	
	public FoodItemResponse parseEntityToGetResponse(FoodItem foodItem){
		FoodItemResponse response = FoodItemConverter.convertEntityToResponse(foodItem);
		if(response!=null&&CollectionUtils.isNotEmpty(foodItem.getListGoesWellItems())){
			Iterator<GoesWellWithItem> iterator = foodItem.getListGoesWellItems().iterator();
			List<GoesWellWithItemPojo> goesList = new ArrayList<>();
			
			boolean updateEntity = false;
			while(iterator.hasNext()){
				GoesWellWithItem goesWellWithItem = iterator.next();
				String foodItemId = goesWellWithItem.getFoodItemId();
				GoesWellWithItemPojo goesWellWithItemPojo = FoodItemConverter.convertEntityToGWWIP(foodItemMao.getById(foodItemId));
				if(goesWellWithItemPojo==null){
					iterator.remove();
					updateEntity = true;
					continue;
				}
				goesList.add(goesWellWithItemPojo);
			}
			if(CollectionUtils.isNotEmpty(goesList)){
				response.setListGoesWellItems(goesList);
			}
			if(updateEntity){
				foodItemMao.update(foodItem);
			}
		}
		return response;
	}

	@Override
	public boolean existsById(String foodItemId) {
		FoodItem foodItem = foodItemMao.getById(foodItemId);
		if(foodItem!=null)
			return true;
		return false;
	}

	@Override
	public List<FoodItemResponse> searchByQueryString(String id, String query) {
		if(StringUtils.isEmpty(query))
			return null;
		query = query.toLowerCase();
		List<FoodItemResponse> responseList = getAllByRoomServicesId(id);
		if(CollectionUtils.isNotEmpty(responseList)){
			Iterator<FoodItemResponse> iterator = responseList.iterator();
			while(iterator.hasNext()){
				FoodItemResponse response = iterator.next();
				if(!response.getName().toLowerCase().contains(query)&&!response.getCuisineType().toLowerCase().contains(query)&&
					!response.getDescription().toLowerCase().contains(query)){
					iterator.remove();
				}
			}
		}
		return responseList;
	}

	@Override
	public FoodItem getEntityById(String foodItemId) {
		FoodItem foodItem = foodItemMao.getById(foodItemId);
		return foodItem;
	}

	@Override
	public void updateEntity(FoodItem foodItem) {
		foodItemMao.update(foodItem);
	}

	@Override
	public List<FoodItemResponse> getAllByType(String roomServicesId, MenuItemType menuItemType) {
		if(StringUtils.isEmpty(roomServicesId)||menuItemType==null)
			return null;
		List<FoodItem> foodItemList = foodItemMao.getAllByRoomServicesId(roomServicesId, menuItemType.getCode());
		return parseEntityList(foodItemList);
	}

	@Override
	public void deleteFoodItem(String id) {
		foodItemMao.delete(id);
	}

}
