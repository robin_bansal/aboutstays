package com.aboutstays.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aboutstays.entities.MasterCity;
import com.aboutstays.mao.MasterCityMao;
import com.aboutstays.request.dto.AddMasterCityRequest;
import com.aboutstays.response.dto.GetMasterCityResponse;
import com.aboutstays.services.MasterCityService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MasterCityConverter;

@Service
public class MasterCityServiceImpl implements MasterCityService {

	@Autowired
	private MasterCityMao masterCityMao;
	
	@Override
	public String save(AddMasterCityRequest request) {
		MasterCity masterCity = MasterCityConverter.convertRequestToEntity(request);
		if(masterCity!=null){
			masterCityMao.save(masterCity);
			return masterCity.getId();
		}
		return null;
	}

	@Override
	public void saveAll(List<AddMasterCityRequest> requestList) {
		List<MasterCity> entityList = MasterCityConverter.convertRequestListToEntityList(requestList);
		if(CollectionUtils.isNotEmpty(requestList)){
			masterCityMao.saveAll(entityList);
		}
	}

	@Override
	public List<GetMasterCityResponse> getAll() {
		List<MasterCity> entityList = masterCityMao.getAll();
		return MasterCityConverter.convertEntityListToGetList(entityList);
	}

	@Override
	public GetMasterCityResponse getById(String id) {
		MasterCity masterCity = masterCityMao.getById(id);
		return MasterCityConverter.convertEntityToGetResponse(masterCity);
	}

	@Override
	public void delete(String id) {
		masterCityMao.delete(id);
	}

	@Override
	public GetMasterCityResponse getByName(String name) {
		List<MasterCity> masterCities = masterCityMao.getAll();
		if(CollectionUtils.isEmpty(masterCities)){
			for(MasterCity masterCity : masterCities){
				if(masterCity.getName().toLowerCase().equalsIgnoreCase(name)){
					return MasterCityConverter.convertEntityToGetResponse(masterCity);
				}
			}
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		MasterCity masterCity = masterCityMao.getById(id);
		if(masterCity!=null)
			return true;
		return false;
	}

	@Override
	public boolean existsByName(String name) {
		GetMasterCityResponse getMasterCityResponse = getByName(name);
		if(getMasterCityResponse!=null)
			return true;
		return false;
	}

}
