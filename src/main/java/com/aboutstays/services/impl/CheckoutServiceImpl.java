package com.aboutstays.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.CheckoutEntity;
import com.aboutstays.enums.CheckoutRequestStatus;
import com.aboutstays.mao.CheckoutMao;
import com.aboutstays.request.dto.InitiateCheckoutRequest;
import com.aboutstays.response.dto.CheckoutResponse;
import com.aboutstays.services.CheckoutService;
import com.aboutstays.utils.CheckoutConverter;

@Service
public class CheckoutServiceImpl implements CheckoutService{

	@Autowired
	private CheckoutMao checkoutMao;
	
	@Override
	public String save(InitiateCheckoutRequest request) {
		CheckoutEntity checkoutEntity = CheckoutConverter.convertInitiateRequestToEntity(request);
		if(checkoutEntity != null){
			checkoutEntity.setId(null);
			checkoutMao.save(checkoutEntity);
			return checkoutEntity.getId();
		}
		return null;
	}

	@Override
	public void update(CheckoutEntity checkoutEntity) {
//		CheckoutEntity checkoutEntity = CheckoutConverter.convertModifyRequestToEntity(request);
		if(checkoutEntity != null) {
			checkoutMao.update(checkoutEntity);
		}
	}

	@Override
	public void updateNonNull(CheckoutEntity checkoutEntity) {
//		CheckoutEntity checkoutEntity = CheckoutConverter.convertModifyRequestToEntity(request);
		if(checkoutEntity != null) {
			checkoutMao.updateNonNull(checkoutEntity);
		}
	}

	@Override
	public CheckoutResponse getById(String id) {
		CheckoutEntity checkoutEntity = checkoutMao.getById(id);
		return CheckoutConverter.convertEntityToResponse(checkoutEntity);
	}

	@Override
	public void delete(String id) {
		checkoutMao.delete(id);
		
	}

	@Override
	public void changeStatus(String id, CheckoutRequestStatus checkoutRequestStatus) {
		if(StringUtils.isEmpty(id)||checkoutRequestStatus==null)
			return;
		checkoutMao.changeStatus(id, checkoutRequestStatus.getCode());
		
	}

	@Override
	public CheckoutResponse getByUserStaysHotelId(String userId, String staysId, String hotelId) {
		CheckoutEntity checkoutEntity = checkoutMao.getByUserStaysHotelId(userId, staysId, hotelId);
		return CheckoutConverter.convertEntityToResponse(checkoutEntity);
	}
	
	

}
