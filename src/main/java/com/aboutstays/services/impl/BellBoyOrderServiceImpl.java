package com.aboutstays.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.BellBoyOrderMao;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;
import com.aboutstays.request.dto.AddBellBoyOrderRequest;
import com.aboutstays.response.dto.GetBellBoyOrderResponse;
import com.aboutstays.services.BellBoyOrderService;
import com.aboutstays.utils.BellBoyOrderConverter;

@Service
public class BellBoyOrderServiceImpl implements BellBoyOrderService{

	@Autowired
	private BellBoyOrderMao bellBoyOrderMao;
	
	@Override
	public String save(AddBellBoyOrderRequest request) {
		BellBoyOrder order = BellBoyOrderConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			bellBoyOrderMao.save(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public GetBellBoyOrderResponse getRequestedBellBoy(UserStaysHotelGSIdsPojo request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BellBoyOrder getEntity(String id) {
		if(!StringUtils.isEmpty(id)){
			return bellBoyOrderMao.getById(id);
		}
		return null;
	}
	
	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(StringUtils.isEmpty(id)||status==null){
			return false;
		}
		bellBoyOrderMao.changeOrderStatus(id, status.getCode());
		return true;
	}


	
	
}
