package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.MaintenanceRequestMao;
import com.aboutstays.request.dto.AddMaintenanceOrderRequest;
import com.aboutstays.request.dto.UpdateMaintenanceOrderRequest;
import com.aboutstays.response.dto.GetMaintenanceOrderResponse;
import com.aboutstays.services.MaintenanceRequestService;
import com.aboutstays.utils.MaintenanceOrderConverter;

@Service
public class MaintenanceRequestServiceImpl implements MaintenanceRequestService{

	@Autowired
	private MaintenanceRequestMao mtRequestMao;
	
	@Override
	public String save(AddMaintenanceOrderRequest request) {
		MaintenanceRequestEntity entity = MaintenanceOrderConverter.convertBeanToEntity(request);
		if(entity != null){
			entity.setId(null);
			mtRequestMao.save(entity);
			return entity.getId();
		}
		return null;
	}

	@Override
	public GetMaintenanceOrderResponse getById(String id) {
		MaintenanceRequestEntity entity = mtRequestMao.getById(id);
		return MaintenanceOrderConverter.convertEntityToBean(entity);
	}

	@Override
	public List<GetMaintenanceOrderResponse> getAll() {
		List<MaintenanceRequestEntity> listEntity = mtRequestMao.getAll();
		return MaintenanceOrderConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public boolean updateNonNullFieldsOfRequest(UpdateMaintenanceOrderRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			MaintenanceRequestEntity entity = MaintenanceOrderConverter.convertUpdateRequestToEntity(request);
			mtRequestMao.updateNonNull(entity);
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			mtRequestMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(mtRequestMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(StringUtils.isEmpty(id)||status==null){
			return false;
		}
		mtRequestMao.changeOrderStatus(id, status.getCode());
		return true;
	}

	@Override
	public MaintenanceRequestEntity getEntity(String id) {
		return mtRequestMao.getById(id);
	}
	
	

}
