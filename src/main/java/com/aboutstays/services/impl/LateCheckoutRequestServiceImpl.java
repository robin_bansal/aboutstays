package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.LateCheckoutRequestMao;
import com.aboutstays.request.dto.AddLateCheckoutRequest;
import com.aboutstays.request.dto.UpdateLateCheckoutRequest;
import com.aboutstays.response.dto.LateCheckoutResposne;
import com.aboutstays.services.LateCheckoutRequestService;
import com.aboutstays.utils.LateCheckoutRequestConverter;

@Service
public class LateCheckoutRequestServiceImpl implements LateCheckoutRequestService{

	@Autowired
	private LateCheckoutRequestMao lcrMao;
	
	@Override
	public String add(AddLateCheckoutRequest request) {
		LateCheckoutRequest checkout = LateCheckoutRequestConverter.convertBeanToEntity(request);
		if(checkout != null){
			checkout.setId(null);
			lcrMao.add(checkout);
			return checkout.getId();
		}
		return null;
	}

	@Override
	public LateCheckoutResposne getById(String id) {
		LateCheckoutRequest request = lcrMao.get(id);
		return LateCheckoutRequestConverter.convertEntityToBean(request);
	}

	@Override
	public List<LateCheckoutResposne> getAll() {
		List<LateCheckoutRequest> listRequest = lcrMao.getAll();
		return LateCheckoutRequestConverter.convertListEntityToListBean(listRequest);
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			lcrMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateNonNUll(UpdateLateCheckoutRequest request) {
		if(!StringUtils.isEmpty(request.getId())){
			LateCheckoutRequest order = LateCheckoutRequestConverter.convertUpdateEntityToBean(request);
			if(order != null){
				lcrMao.updateNonNull(order);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(lcrMao.get(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(status != null){
			lcrMao.changeOrderStatus(id, status.getCode());
			return true;
		}
		return false;
	}

	@Override
	public LateCheckoutRequest getEntity(String id) {
		return lcrMao.get(id);
	}

	@Override
	public String modifyLateCheckoutRequest(UpdateLateCheckoutRequest request) {
		LateCheckoutRequest lateCheckoutRequest = LateCheckoutRequestConverter.convertModifyRequest(request);
		if(lateCheckoutRequest != null) {
			lcrMao.update(lateCheckoutRequest);
			return lateCheckoutRequest.getId();
		}
		return null;
	}

	@Override
	public LateCheckoutResposne retrieveLateCheckoutRequest(String userId, String hotelId, String staysId) {
		LateCheckoutRequest lcrRequest = lcrMao.retreiveLateCheckout(userId, hotelId, staysId);
		return LateCheckoutRequestConverter.convertEntityToBean(lcrRequest);
	}

}
