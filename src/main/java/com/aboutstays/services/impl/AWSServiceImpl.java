package com.aboutstays.services.impl;

import java.io.File;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.exception.AWSAccessException;
import com.aboutstays.exception.CommonExceptionCode;
import com.aboutstays.services.AWSService;
import com.aboutstays.services.ProcessUtilityService;
import com.aboutstays.utils.AWSUploader;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;

@Service
public class AWSServiceImpl implements AWSService{

	private static Logger logger = LoggerFactory.getLogger(AWSServiceImpl.class);
	
	@Autowired
	private ProcessUtilityService processUtilityService;
	
	private AmazonS3Client s3client;
	
	@PostConstruct
	public void init(){
		String accessKey = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.PROPERTY_IAM_ACCESS_KEY);
		String secretKey = processUtilityService.getStringProcessProperty(PropertyConstant.PROCESS_AWS, PropertyConstant.PROPERTY_IAM_SECRET_KEY);
		BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
		s3client = new AmazonS3Client(awsCredentials);
	}
	
	@Override
	public String uploadToS3(String bucketName, String fileName, String filePath, String fileType) throws AWSAccessException {
		if(s3client == null){
			logger.error("s3 client is found null");
			throw new AWSAccessException(CommonExceptionCode.S3_CLIENT_UNINTRESTED);
		}
		return AWSUploader.uploadToS3(s3client, bucketName, fileName, filePath, fileType);
	}

	@Override
	public String uploadToS3(String bucketName, File file, String filePath) throws AWSAccessException {
		if(s3client==null){
			System.out.println("s3 client is found null");
			logger.error("s3 client is found null");
			throw new AWSAccessException(CommonExceptionCode.S3_CLIENT_UNINTRESTED);
		}
		return AWSUploader.uploadToS3(s3client, bucketName, file, filePath);
	}

}
