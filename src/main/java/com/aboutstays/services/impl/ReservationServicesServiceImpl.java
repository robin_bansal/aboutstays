package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.ReservationServiceEntity;
import com.aboutstays.mao.ReservationServiceMao;
import com.aboutstays.request.dto.UpdateReservationServiceRequest;
import com.aboutstays.response.dto.GetReservationServiceResponse;
import com.aboutstays.services.ReservationServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ReservationServiceConverter;

@Service
public class ReservationServicesServiceImpl implements ReservationServicesService{

	@Autowired
	private ReservationServiceMao rsMao;
	
	@Override
	public String add(AddReservationServiceRequest request) {
		ReservationServiceEntity entity = ReservationServiceConverter.convertBeanToEntity(request);
		if(entity != null){
			rsMao.add(entity);
			return entity.getHotelId();
		}
		return null;
	}

	@Override
	public List<GetReservationServiceResponse> getAll() {
		List<ReservationServiceEntity> listEntity = rsMao.getAll();
		return ReservationServiceConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public GetReservationServiceResponse getById(String id) {
		ReservationServiceEntity entity = rsMao.getById(id);
		return ReservationServiceConverter.convertEntityToBean(entity);
	}

	@Override
	public boolean existsById(String id) {
		if(!StringUtils.isEmpty(id)){
			if(rsMao.getById(id) != null){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			rsMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateReservationServiceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			ReservationServiceEntity entity = ReservationServiceConverter.convertUpdateBeanToEntity(request);
			if(entity != null){
				if(CollectionUtils.isEmpty(entity.getTypeInfoList()))
					entity.setTypeInfoList(new ArrayList<>());
				rsMao.updateNonNullById(entity, request.getId());
				return true;
			}
		}
		return false;
	}

	@Override
	public ReservationServiceEntity getEntity(String id) {
		return rsMao.getById(id);
	}

}
