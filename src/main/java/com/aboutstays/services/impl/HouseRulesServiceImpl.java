package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.HouseRules;
import com.aboutstays.mao.HouseRulesMao;
import com.aboutstays.request.dto.AddHouseRulesRequest;
import com.aboutstays.request.dto.UpdateHouseRulesRequest;
import com.aboutstays.response.dto.GetHouseRulesResponse;
import com.aboutstays.services.HouseRulesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HouseRulesConverter;

@Service
public class HouseRulesServiceImpl implements HouseRulesService{

	@Autowired
	private HouseRulesMao hrMao;
	
	
	@Override
	public String add(AddHouseRulesRequest request) {
		HouseRules rules = HouseRulesConverter.convertBeanToEntity(request);
		if(rules != null){
			hrMao.add(rules);
			return rules.getHotelId();
		}
		return null;
	}

	@Override
	public List<GetHouseRulesResponse> getAll() {
		List<HouseRules> listRules = hrMao.getAll();
		return HouseRulesConverter.convertListEntityToListBean(listRules);
	}

	@Override
	public GetHouseRulesResponse getById(String id) {
		HouseRules rules = hrMao.getById(id);
		return HouseRulesConverter.convertEntityToBean(rules);
	}

	@Override
	public boolean existsById(String id) {
		if(hrMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			hrMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateHouseRulesRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			HouseRules rules = HouseRulesConverter.convertUpdateRequestToEntity(request);
			if(CollectionUtils.isEmpty(rules.getListPGItem()))
				rules.setListPGItem(new ArrayList<>());
			hrMao.updateNonNullById(rules);
			return true;
		}
		return false;
	}
}
