package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.AirportPickup;
import com.aboutstays.entities.Car;
import com.aboutstays.entities.Driver;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.PickupStatus;
import com.aboutstays.mao.AirportPickupMao;
import com.aboutstays.mao.CarMao;
import com.aboutstays.mao.DriverMao;
import com.aboutstays.request.dto.AddAirportPickupRequest;
import com.aboutstays.request.dto.ApprovePickupRequest;
import com.aboutstays.request.dto.GetAirportPickupResponse;
import com.aboutstays.request.dto.UpdateAirportPickupRequest;
import com.aboutstays.response.dto.AddAirportPickupResponse;
import com.aboutstays.response.dto.GetCarResponse;
import com.aboutstays.response.dto.GetDriverResponse;
import com.aboutstays.services.AirportPickupService;
import com.aboutstays.utils.AirportPickupConverter;
import com.aboutstays.utils.CarConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DriverConverter;

@Service
public class AirportPickupServiceImpl implements AirportPickupService{

	@Autowired
	private AirportPickupMao airportPickupMao;
	
	@Autowired
	private CarMao carMao;
	
	@Autowired
	private DriverMao driverMao;
	
	@Override
	public AddAirportPickupResponse add(AddAirportPickupRequest request) {
		if(request != null){
			AirportPickup pickup = AirportPickupConverter.convertBeanToEntity(request);
			airportPickupMao.add(pickup);
			return new AddAirportPickupResponse(pickup.getId());
		}
		return null;
	}

	@Override
	public List<GetAirportPickupResponse> getAll(String userId) {
		List<AirportPickup> listPickup = null;
		if(StringUtils.isEmpty(userId)){
			listPickup = airportPickupMao.getAll();	
		} else{
			listPickup = airportPickupMao.getAllByUserId(userId);
		}
		List<GetAirportPickupResponse> listResponse = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(listPickup)){
			for(AirportPickup pickup : listPickup){
				GetAirportPickupResponse response = AirportPickupConverter.convertEntityToBean(pickup);
				if(pickup.getPickupStatus() == PickupStatus.APPROVED.getCode()){
					if(!StringUtils.isEmpty(pickup.getCarId())){
						Car car = carMao.getById(pickup.getCarId());
						GetCarResponse carResponse = CarConverter.convertEntityToBean(car);
						response.setCar(carResponse);
					}
					if(!StringUtils.isEmpty(pickup.getDriverId())){
						Driver driver = driverMao.getById(pickup.getDriverId());
						GetDriverResponse driverResponse = DriverConverter.convertEntityToBean(driver);
						response.setDriver(driverResponse);
					}
				}
				listResponse.add(response);
			}			
			return listResponse;
		}
		return null;
	}

	@Override
	public GetAirportPickupResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			AirportPickup pickup = airportPickupMao.getById(id);
			if(pickup != null){
				GetAirportPickupResponse response = AirportPickupConverter.convertEntityToBean(pickup);
				if(pickup.getPickupStatus() == PickupStatus.APPROVED.getCode()){
					if(!StringUtils.isEmpty(pickup.getCarId())){
						Car car = carMao.getById(pickup.getCarId());
						GetCarResponse carResponse = CarConverter.convertEntityToBean(car);
						response.setCar(carResponse);
					}
					if(!StringUtils.isEmpty(pickup.getDriverId())){
						Driver driver = driverMao.getById(pickup.getDriverId());
						GetDriverResponse driverResponse = DriverConverter.convertEntityToBean(driver);
						response.setDriver(driverResponse);
					}
				}
				return response;
			}			
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			airportPickupMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateAirportPickupRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			AirportPickup pickup = AirportPickupConverter.convertUpdateRequestToEntity(request);
			airportPickupMao.updateNonNullById(pickup, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public boolean approvePickupRequest(ApprovePickupRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			AirportPickup pickup = AirportPickupConverter.convertApprovePickupRequestToEntity(request);
			airportPickupMao.updateNonNullById(pickup, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		airportPickupMao.changeOrderStatus(id, status);
		return true;
	}

	@Override
	public AirportPickup getEntity(String id) {
		return airportPickupMao.getById(id);
	}



}
