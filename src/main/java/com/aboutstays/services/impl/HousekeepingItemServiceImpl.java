package com.aboutstays.services.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.HousekeepingItem;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.mao.HousekeepingItemMao;
import com.aboutstays.request.dto.HousekeepingItemRequest;
import com.aboutstays.response.dto.HousekeepingItemResponse;
import com.aboutstays.services.HousekeepingItemService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HousekeepingItemConverter;

@Service
public class HousekeepingItemServiceImpl implements HousekeepingItemService{

	@Autowired
	private HousekeepingItemMao hkItemMao;
	
	@Override
	public String save(HousekeepingItemRequest request) {
		HousekeepingItem item = HousekeepingItemConverter.convertRequestToEntity(request);
		if(item!=null){
			item.setId(null);
			hkItemMao.save(item);
			return item.getId();
		}
		return null;
	}

	@Override
	public HousekeepingItemResponse getById(String id) {
		HousekeepingItem item = hkItemMao.getById(id);
		HousekeepingItemResponse response = HousekeepingItemConverter.convertEntityToResponse(item);
		return response;
	}

	@Override
	public List<HousekeepingItemResponse> getAllHousekeepingItems() {
		List<HousekeepingItem> listHKItems = hkItemMao.getAll();
		if(CollectionUtils.isNotEmpty(listHKItems)){
			List<HousekeepingItemResponse> listResponse = HousekeepingItemConverter.convertListEntityToListResponse(listHKItems);
			return listResponse;
		}
		return null;
	}

	@Override
	public void update(HousekeepingItemRequest request) {
		HousekeepingItem item = HousekeepingItemConverter.convertUpdateRequestToEntity(request);
		if(item != null){
			hkItemMao.update(item);
		}
	}

	@Override
	public void updateNonNull(HousekeepingItemRequest request) {
		HousekeepingItem item = HousekeepingItemConverter.convertUpdateRequestToEntity(request);
		if(item != null){
			hkItemMao.updateNonNull(item);
		}
	}

	@Override
	public List<HousekeepingItemResponse> getAllByHotelId(String hotelId) {
		List<HousekeepingItem> hkItemList = hkItemMao.getAllByHotelId(hotelId);
		if(CollectionUtils.isNotEmpty(hkItemList)){
			List<HousekeepingItemResponse> listResponse = HousekeepingItemConverter.convertListEntityToListResponse(hkItemList);
			return listResponse;
		}
		return null;
	}

	@Override
	public List<HousekeepingItemResponse> getAllByHousekeepingServiceId(String hkServiceId) {
		List<HousekeepingItem> hkItemList = hkItemMao.getAllByHKServicesId(hkServiceId);
		if(CollectionUtils.isNotEmpty(hkItemList)){
			List<HousekeepingItemResponse> listResponse = HousekeepingItemConverter.convertListEntityToListResponse(hkItemList);
			return listResponse;
		}
		return null;
	}

	@Override
	public boolean existsById(String hkItemId) {
		HousekeepingItem item = hkItemMao.getById(hkItemId);
		if(item!=null)
			return true;
		return false;
	}

	@Override
	public List<HousekeepingItemResponse> searchByQueryString(String hkServiceId, String query) {
		if(StringUtils.isEmpty(query)){
			return null;
		}
		query = query.toLowerCase();
		List<HousekeepingItemResponse> listResponse = getAllByHousekeepingServiceId(hkServiceId);
		if(CollectionUtils.isNotEmpty(listResponse)){
			Iterator<HousekeepingItemResponse> iterator = listResponse.iterator();
			while(iterator.hasNext()){
				HousekeepingItemResponse response = iterator.next();
				if(!response.getName().toLowerCase().contains(query)){
					iterator.remove();
				}
			}
		}
		return listResponse;
	}

	@Override
	public HousekeepingItem getEntityById(String hkItemId) {
		HousekeepingItem item = hkItemMao.getById(hkItemId);
		return item;
	}

	@Override
	public void updateEntity(HousekeepingItem hkItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HousekeepingItemResponse> getAllByType(String hkServiceId,
			HousekeepingItemType hkItemType) {
		if(StringUtils.isEmpty(hkServiceId) || hkItemType == null){
			return null;
		}
		List<HousekeepingItem> hkItemList = hkItemMao.getAllByHKServicesId(hkServiceId, hkItemType.getCode());
		if(CollectionUtils.isNotEmpty(hkItemList)){
			List<HousekeepingItemResponse> listResponse = HousekeepingItemConverter.convertListEntityToListResponse(hkItemList);
			return listResponse;
		}
		return null;
	}

	@Override
	public boolean deleteById(String id) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		hkItemMao.delete(id);
		return true;
	}

		


}
