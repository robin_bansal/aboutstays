package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.BellBoyServiceEntity;
import com.aboutstays.mao.BellBoyServiceMao;
import com.aboutstays.request.dto.BellBoyServiceRequest;
import com.aboutstays.response.dto.BellBoyServiceResponse;
import com.aboutstays.services.BellBoyServicesService;
import com.aboutstays.utils.BellBoyServiceConverter;

@Service
public class BellBoyServicesServiceImpl implements BellBoyServicesService {

	@Autowired
	private BellBoyServiceMao bellBoyServiceMao;
	
	@Override
	public String save(BellBoyServiceRequest request) {
		BellBoyServiceEntity entity = BellBoyServiceConverter.convertRequestToEntity(request, false);
		if(entity!=null){
			bellBoyServiceMao.save(entity);
			return entity.getHotelId();
		}
		return null;
	}

	@Override
	public void update(BellBoyServiceRequest request) {
		BellBoyServiceEntity entity = BellBoyServiceConverter.convertRequestToEntity(request, true);
		if(entity!=null){
			bellBoyServiceMao.update(entity);
		}
	}

	@Override
	public void updateNonNull(BellBoyServiceRequest request) {
		BellBoyServiceEntity entity = BellBoyServiceConverter.convertRequestToEntity(request, true);
		if(entity!=null){
			bellBoyServiceMao.updateNonNull(entity);
		}
	}

	@Override
	public void delete(String hotelId) {
		bellBoyServiceMao.delete(hotelId);
	}

	@Override
	public BellBoyServiceResponse getByHotelId(String hotelId) {
		BellBoyServiceEntity entity = bellBoyServiceMao.getByHotelId(hotelId);
		return BellBoyServiceConverter.convertEntityToResponse(entity);
	}

	@Override
	public boolean existsByHotelId(String hotelId) {
		BellBoyServiceEntity entity = bellBoyServiceMao.getByHotelId(hotelId);
		if(entity!=null){
			return true;
		}
		return false;
	}

	@Override
	public List<BellBoyServiceResponse> getAll() {
		List<BellBoyServiceEntity> entities = bellBoyServiceMao.getAll();
		return BellBoyServiceConverter.convertEntityListToResponseList(entities);
	}

}
