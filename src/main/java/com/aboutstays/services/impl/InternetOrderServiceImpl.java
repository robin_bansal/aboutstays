package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.InternetOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.InternetOrderMao;
import com.aboutstays.request.dto.AddInternetOrderRequest;
import com.aboutstays.response.dto.InternetOrderResponse;
import com.aboutstays.response.dto.UpdateInternetOrderRequest;
import com.aboutstays.services.InternetOrderService;
import com.aboutstays.utils.InternetOrderConverter;

@Service
public class InternetOrderServiceImpl implements InternetOrderService{

	@Autowired
	private InternetOrderMao ioMao;
	
	@Override
	public String add(AddInternetOrderRequest request) {
		InternetOrder order = InternetOrderConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			ioMao.add(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public InternetOrderResponse getById(String id) {
		InternetOrder order = ioMao.get(id);
		return InternetOrderConverter.convertEntityToBean(order);
	}

	@Override
	public List<InternetOrderResponse> getAll() {
		List<InternetOrder> listOrder = ioMao.getAll();
		return InternetOrderConverter.convertListEntityToListBean(listOrder);
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			ioMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateNonNUll(UpdateInternetOrderRequest request) {
		if(!StringUtils.isEmpty(request.getId())){
			InternetOrder order = InternetOrderConverter.convertUpdateEntityToBean(request);
			if(order != null){
				ioMao.updateNonNull(order);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(ioMao.get(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(status != null){
			ioMao.changeOrderStatus(id, status.getCode());
			return true;
		}
		return false;
	}

	@Override
	public InternetOrder getEntity(String id) {
		return ioMao.get(id);
	}
}
