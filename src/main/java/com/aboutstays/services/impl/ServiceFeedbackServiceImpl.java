package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.ServiceFeedback;
import com.aboutstays.mao.ServiceFeedbackMao;
import com.aboutstays.request.dto.AddServiceFeedbackRequest;
import com.aboutstays.request.dto.UpdateServiceFeedback;
import com.aboutstays.response.dto.GetServiceFeedbackResponse;
import com.aboutstays.services.ServiceFeedbackService;
import com.aboutstays.utils.ServiceFeedbackConverter;

@Service
public class ServiceFeedbackServiceImpl implements ServiceFeedbackService{

	@Autowired
	private ServiceFeedbackMao sfMao;
	
	@Override
	public String add(AddServiceFeedbackRequest request) {
		ServiceFeedback feedback = ServiceFeedbackConverter.convertBeanToEntity(request);
		if(feedback != null){
			feedback.setId(null);
			sfMao.add(feedback);
			return feedback.getId();
		}
		return null;
	}

	@Override
	public GetServiceFeedbackResponse get(String id) {
		ServiceFeedback feedback = sfMao.get(id);
		return ServiceFeedbackConverter.convertEntityToBean(feedback);
	}

	@Override
	public List<GetServiceFeedbackResponse> getAll() {
		List<ServiceFeedback> listFeedback = sfMao.getAll();
		return ServiceFeedbackConverter.convertListEntityToListBean(listFeedback);
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			sfMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean update(UpdateServiceFeedback request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateNonNUll(UpdateServiceFeedback request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			ServiceFeedback feedback = ServiceFeedbackConverter.convertUpdateBeanToEntity(request);
			sfMao.updateNonNull(feedback);
			return true;
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(sfMao.get(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public GetServiceFeedbackResponse getByUosId(String uosId) {
		if(!StringUtils.isEmpty(uosId)){
			ServiceFeedback feedback = sfMao.getByUosId(uosId);
			return ServiceFeedbackConverter.convertEntityToBean(feedback);
		}
		return null;
	}

}
