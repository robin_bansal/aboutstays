package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MasterGenereItem;
import com.aboutstays.mao.MasterGenereMao;
import com.aboutstays.request.dto.AddMasterGenereRequest;
import com.aboutstays.request.dto.UpdateMasterGenereRequest;
import com.aboutstays.response.dto.GetMasterGenereResponse;
import com.aboutstays.services.MasterGenereService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MasterGenereConverter;

@Service
public class MasterGenereServiceImpl implements MasterGenereService {

	@Autowired
	private MasterGenereMao masterGenereMao;
	
	@Override
	public String save(AddMasterGenereRequest request) {
		MasterGenereItem masterGenereItem = MasterGenereConverter.convertAddRequestToEntity(request);
		if(masterGenereItem!=null){
			masterGenereMao.save(masterGenereItem);
			return masterGenereItem.getId();
		}
		return null;
	}

	@Override
	public void delete(String id) {
		masterGenereMao.delete(id);
	}

	@Override
	public List<GetMasterGenereResponse> getAll() {
		List<MasterGenereItem> masterGenereItems = masterGenereMao.getAll();
		return MasterGenereConverter.convertEntityListToGetList(masterGenereItems);
	}

	@Override
	public void update(UpdateMasterGenereRequest request) {
		MasterGenereItem masterGenereItem = MasterGenereConverter.convertUpdateRequestToEntity(request);
		if(masterGenereItem!=null){
			masterGenereMao.update(masterGenereItem);
		}
	}

	@Override
	public void updateNonNull(UpdateMasterGenereRequest request) {
		MasterGenereItem masterGenereItem = MasterGenereConverter.convertUpdateRequestToEntity(request);
		if(masterGenereItem!=null){
			masterGenereMao.updateNonNull(masterGenereItem);
		}
	}

	@Override
	public GetMasterGenereResponse getById(String id) {
		MasterGenereItem masterGenereItem = masterGenereMao.getById(id);
		return MasterGenereConverter.convertEntityToGetResponse(masterGenereItem);
	}

	@Override
	public boolean existsByName(String name, String skipForChannelId) {
		GetMasterGenereResponse masterGenereItem = getByName(name, skipForChannelId);
		if(masterGenereItem!=null){
			return true;
		}
		return false;
	}

	@Override
	public GetMasterGenereResponse getByName(String name, String skipForChannelId) {
		List<MasterGenereItem> masterGenereItemList = masterGenereMao.getAll();
		if(CollectionUtils.isNotEmpty(masterGenereItemList)){
			for(MasterGenereItem masterGenereItem : masterGenereItemList){
				String savedName = masterGenereItem.getName();
				if((!StringUtils.isEmpty(savedName)&&savedName.equalsIgnoreCase(name))){
					if(masterGenereItem.getId().equals(skipForChannelId)){
						continue;
					}
					return MasterGenereConverter.convertEntityToGetResponse(masterGenereItem);
				}
			}
		}
		return null;
	}

	@Override
	public boolean existsById(String masterGenereId) {
		MasterGenereItem masterGenereItem = masterGenereMao.getById(masterGenereId);
		if(masterGenereItem!=null)
			return true;
		return false;
	}

}
