package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MasterAmenity;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.mao.MasterAmenityMao;
import com.aboutstays.request.dto.AddMasterAmenityRequest;
import com.aboutstays.response.dto.GetMasterAmenityResponse;
import com.aboutstays.services.MasterAmenityService;
import com.aboutstays.utils.AmenityConverter;
import com.aboutstays.utils.CollectionUtils;

@Service
public class MasterAmenityServiceImpl implements MasterAmenityService {

	@Autowired
	private MasterAmenityMao masterAmenityMao;
	
	@Override
	public String save(AddMasterAmenityRequest request) {
		MasterAmenity masterAmenity = AmenityConverter.convertAddRequestToEntity(request);
		if(masterAmenity!=null){
			masterAmenityMao.save(masterAmenity);
			return masterAmenity.getId();
		}
		return null;
	}

	@Override
	public GetMasterAmenityResponse getById(String id) {
		MasterAmenity masterAmenity = masterAmenityMao.getById(id);
		return AmenityConverter.convertEntityToGetResponse(masterAmenity);
	}

	@Override
	public List<GetMasterAmenityResponse> getAll() {
		List<MasterAmenity> amenities = masterAmenityMao.getAll();
		return AmenityConverter.convertEntityListToGetList(amenities);
	}

	@Override
	public List<GetMasterAmenityResponse> getAll(AmenityType amenityType) {
		if(amenityType==null)
			return null;
		List<MasterAmenity> amenities = masterAmenityMao.getAll(amenityType.getType());
		return AmenityConverter.convertEntityListToGetList(amenities);
	}

	@Override
	public void delete(String id) {
		masterAmenityMao.delete(id);
	}

	@Override
	public boolean existsById(String id) {
		MasterAmenity masterAmenity = masterAmenityMao.getById(id);
		if(masterAmenity!=null)
			return true;
		return false;
	}

	@Override
	public boolean existsByName(String name, AmenityType amenityType) {
		if(StringUtils.isEmpty(name)||amenityType==null)
			return false;
		List<MasterAmenity> amenities = masterAmenityMao.getAll(amenityType.getType());
		if(CollectionUtils.isNotEmpty(amenities)){
			for(MasterAmenity masterAmenity : amenities){
				if(masterAmenity.getName().equalsIgnoreCase(name)){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void saveAll(List<AddMasterAmenityRequest> amenitites) {
		List<MasterAmenity> masterAmenities = AmenityConverter.convertAddLisToEntityList(amenitites);
		if(CollectionUtils.isNotEmpty(masterAmenities)){
			masterAmenityMao.saveAll(masterAmenities);
		}
	}

	@Override
	public boolean existsByIdAndType(String amenityId, AmenityType amenityType) {
		if(amenityType==null)
			return false;
		MasterAmenity masterAmenity = masterAmenityMao.getByIdAndType(amenityId, amenityType.getType());
		if(masterAmenity!=null)
			return true;
		return false;
	}

}
