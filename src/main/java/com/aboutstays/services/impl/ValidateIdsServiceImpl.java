package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;
import com.aboutstays.services.EntertaimentServicesService;
import com.aboutstays.services.FloorService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.MaintenanceService;
import com.aboutstays.services.MasterAmenityService;
import com.aboutstays.services.MasterChannelService;
import com.aboutstays.services.MasterGenereService;
import com.aboutstays.services.ReservationCategoryService;
import com.aboutstays.services.ReservationItemService;
import com.aboutstays.services.ReservationServicesService;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.services.UserService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.services.WingService;
import com.aboutstays.utils.CollectionUtils;

@Service
public class ValidateIdsServiceImpl implements ValidateIdsService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private StaysService staysService;
	
	@Autowired
	private GeneralServicesService generalServicesService;
	
	@Autowired
	private UserOptedServicesService uosService;
	
	@Autowired
	private MaintenanceService mtService;
	
	@Autowired
	private MasterGenereService masterGenereService;
	
	@Autowired
	private ReservationServicesService rsService;
	
	@Autowired
	private MasterChannelService mcService;
	
	@Autowired
	private EntertaimentServicesService esService;
	
	@Autowired
	private MasterGenereService mgService;
	
	@Autowired
	private ReservationItemService rsItemService;
	
	@Autowired
	private FloorService floorService;
	
	@Autowired
	private WingService wingService;
	
	@Autowired
	private RoomCategoryService roomCategoryService; 
	
	@Autowired
	private ReservationCategoryService reservationCategoryService;
	
	@Autowired
	private MasterAmenityService masterAmenityService;
	
	@Override
	public void validateHotelId(String hotelId) throws AboutstaysException {
		if(StringUtils.isEmpty(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if(!hotelService.hotelExistsById(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
	}

	@Override
	public void validateUserId(String userId) throws AboutstaysException {
		if(!userService.existsByUserId(userId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
	}

	@Override
	public void validateStaysId(String staysId) throws AboutstaysException {
		if(!staysService.existsByStaysId(staysId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
	}

	@Override
	public void validateGSId(String gsId) throws AboutstaysException {
		if (StringUtils.isEmpty(gsId))
		throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_ID);
		if(!generalServicesService.existsById(gsId))
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
	}

	@Override
	public void validateHotelUserIds(String hotelId, String userId) throws AboutstaysException {
		if(!userService.existsByUserId(userId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		if(!hotelService.hotelExistsById(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
	}

	@Override
	public void validateUserHotelStaysIds(String userId, String hotelId, String staysId) throws AboutstaysException {
		if(StringUtils.isEmpty(userId))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if(StringUtils.isEmpty(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if(StringUtils.isEmpty(staysId))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		if(!userService.existsByUserId(userId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		if(!hotelService.hotelExistsById(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if(!staysService.existsByStaysId(staysId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
	}

	@Override
	public void validateUserHotelStaysIds(UserStayHotelIdsPojo userStayHotelIdsPojo) throws AboutstaysException {
		if(userStayHotelIdsPojo!=null){
			validateUserHotelStaysIds(userStayHotelIdsPojo.getUserId(), userStayHotelIdsPojo.getHotelId(), userStayHotelIdsPojo.getStaysId());
		}
	}

	@Override
	public void validateUserHotelStaysGsIds(UserStaysHotelGSIdsPojo userStaysHotelGSIdsPojo)
			throws AboutstaysException {
		if(userStaysHotelGSIdsPojo!=null){
			validateUserHotelStaysGsIds(userStaysHotelGSIdsPojo.getUserId(), userStaysHotelGSIdsPojo.getHotelId(), 
					userStaysHotelGSIdsPojo.getStaysId(), userStaysHotelGSIdsPojo.getGsId());
		}
	}

	@Override
	public void validateUserHotelStaysGsIds(String userId, String hotelId, String staysId, String gsId)
			throws AboutstaysException {
		if(!userService.existsByUserId(userId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		if(!hotelService.hotelExistsById(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if(!staysService.existsByStaysId(staysId))
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		if(!generalServicesService.existsById(gsId))
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);
	}

	@Override
	public void validateUOSId(String uosId) throws AboutstaysException {
		if(StringUtils.isEmpty(uosId)){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_OPTED_SERVICE_ID);
		}
		if(!uosService.existsById(uosId)){
			throw new AboutstaysException(AboutstaysResponseCode.USER_OPTED_SERVICE_NOT_EXISTS);
		}
	}

	@Override
	public void ValidateMaintenanceServiceId(String mtServiceId) throws AboutstaysException {
		if(StringUtils.isEmpty(mtServiceId)){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MAINTENANCE_ID);
		}
		if(!mtService.existsById(mtServiceId)){
			throw new AboutstaysException(AboutstaysResponseCode.MAINTENANCE_NOT_FOUND);
		}
	}

	@Override
	public void validateMasterGenereId(String genereId) throws AboutstaysException {
		if(!masterGenereService.existsById(genereId))
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.GENERE_NOT_FOUND, genereId));
	}
	public void validateReservationServiceId(String rsId) throws AboutstaysException {
		validateHotelId(rsId);
		if(!rsService.existsById(rsId)){
			throw new AboutstaysException(AboutstaysResponseCode.RESERVATION_SERVICE_NOT_FOUND);
		}
	}

	@Override
	public void validateMasterChannelId(String id) throws AboutstaysException {
		if(!mcService.existsById(id)){
			throw new AboutstaysException(AboutstaysResponseCode.MASTER_CHANNEL_NOT_FOUND);
		}
		
	}

	@Override
	public void validateEntServiceAndGenereId(String entServiceId, String genereId) throws AboutstaysException {
		if(!mgService.existsById(genereId)){
			throw new AboutstaysException(AboutstaysResponseCode.MASTER_GENERE_NOT_FOUND);
		}
		if(!esService.existsById(entServiceId)){
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_SERVICE_NOT_FOUND);
		}
	}

	@Override
	public void validateEntService(String serviceId) throws AboutstaysException {
		if(!esService.existsById(serviceId)){
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_SERVICE_NOT_FOUND);
		}
	}

	@Override
	public void validateUserIdStaysId(String userId, String staysId) throws AboutstaysException {
		if(!userService.existsByUserId(userId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		}
		if(!staysService.existsByStaysId(staysId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		}
		
	}

	@Override
	public void validateReservationItemId(String reservationItemId) throws AboutstaysException {
		if(!rsItemService.existsById(reservationItemId)) {
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		}
		
	}

	@Override
	public void validateFloorId(String floorId, String wingId, String hotelId) throws AboutstaysException {
		validateWingId(wingId, hotelId);
		if(!floorService.exists(floorId, wingId, hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.FLOOR_WITH_WING_NOT_FOUND);
	}

	@Override
	public void validateWingId(String wingId, String hotelId) throws AboutstaysException {
		validateHotelId(hotelId);
		if(!wingService.exists(wingId, hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.WING_WITH_HOTEL_NOT_FOUND);
	}

	@Override
	public void validateRoomCategoryId(String roomCategoryId, String hotelId) throws AboutstaysException {
		validateHotelId(hotelId);
		if(!roomCategoryService.existsInHotel(hotelId, roomCategoryId))
			throw new AboutstaysException(AboutstaysResponseCode.ROOM_CATEGORY_NOT_FOUND);
	}

	@Override
	public void validateReservationCategoryId(String categoryId, String hotelId) throws AboutstaysException {
		validateReservationServiceId(hotelId);
		if(!reservationCategoryService.exists(categoryId, hotelId)){
			throw new AboutstaysException(AboutstaysResponseCode.RES_CAT_NOT_FOUND);
		}
	}

	@Override
	public void validateReservationItemId(String id, String categoryId, String hotelId) throws AboutstaysException {
		validateReservationCategoryId(categoryId, hotelId);
		if(!rsItemService.exists(id, categoryId, hotelId)){
			throw new AboutstaysException(AboutstaysResponseCode.NO_RESERVATION_ITEM_FOUND);
		}
	}

	@Override
	public void validateAmenitiesList(List<String> amenityIds, AmenityType amenityType, AboutstaysResponseCode aboutstaysResponseCode) throws AboutstaysException {
		if(CollectionUtils.isNotEmpty(amenityIds)&&amenityType!=null&&aboutstaysResponseCode!=null){
			for(String amenityId : amenityIds){
				validateAmenityId(amenityId, amenityType, aboutstaysResponseCode);
			}
		}
	}

	@Override
	public void validateAmenityId(String amenityId, AmenityType amenityType,
			AboutstaysResponseCode aboutstaysResponseCode) throws AboutstaysException {
		if(!masterAmenityService.existsByIdAndType(amenityId, amenityType)){
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(aboutstaysResponseCode, amenityId));
		}
	}

	@Override
	public void validateHotelIdStaysId(String hotelId, String staysId) throws AboutstaysException {
		if(StringUtils.isEmpty(hotelId))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if(StringUtils.isEmpty(staysId))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
	}

}
