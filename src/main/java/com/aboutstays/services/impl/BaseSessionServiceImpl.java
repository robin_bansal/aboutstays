package com.aboutstays.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.BaseSessionPojo;
import com.aboutstays.services.BaseSessionService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.StaysService;

@Service
public class BaseSessionServiceImpl implements BaseSessionService{
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private StaysService staysService;
	
	@Autowired GeneralServicesService generalServicesService;

	@Override
	public void validateBaseSessionSessionData(BaseSessionPojo baseSessionPojo) throws AboutstaysException {
//		if(StringUtils.isEmpty(baseSessionPojo.getUserId()))
//			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);

		if(StringUtils.isEmpty(baseSessionPojo.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if(StringUtils.isEmpty(baseSessionPojo.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		
//		if(!userService.existsByUserId(baseSessionPojo.getUserId()))
//			throw new AboutstaysException(AboutstaysResponseCode.NO_USER);
		if (StringUtils.isEmpty(baseSessionPojo.getGsId()))
		throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_ID);

		if(!hotelService.hotelExistsById(baseSessionPojo.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.HOTEL_NOT_FOUND);
		if(!staysService.existsByStaysId(baseSessionPojo.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.NO_STAYS_EXISTS);
		if(!generalServicesService.existsById(baseSessionPojo.getGsId()))
			throw new AboutstaysException(AboutstaysResponseCode.GENERAL_SERVICE_NOT_EXISTS);

	}
}
