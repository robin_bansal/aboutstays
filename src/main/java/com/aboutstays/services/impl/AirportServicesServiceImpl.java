package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.AirportServices;
import com.aboutstays.mao.AirportServiceMao;
import com.aboutstays.request.dto.AddAirportServiceRequest;
import com.aboutstays.request.dto.UpdateAirportServiceRequest;
import com.aboutstays.response.dto.AddAirportServiceResponse;
import com.aboutstays.response.dto.GetAirportServiceResponse;
import com.aboutstays.services.AirportServicesService;
import com.aboutstays.utils.AirportServiceConverter;
import com.aboutstays.utils.CollectionUtils;

@Service
public class AirportServicesServiceImpl implements AirportServicesService{

	@Autowired
	private AirportServiceMao airportServiceMao;
	
	@Override
	public AddAirportServiceResponse add(AddAirportServiceRequest request) {
		AirportServices services = AirportServiceConverter.convertBeanToEntity(request);
		if(services != null){
			airportServiceMao.add(services);
			return new AddAirportServiceResponse(services.getHotelId());
		}
		return null;
	}

	@Override
	public List<GetAirportServiceResponse> getAll() {
		List<AirportServices> listServices = airportServiceMao.getAll();
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetAirportServiceResponse> listResponse = AirportServiceConverter.convertListEntityToListBean(listServices);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetAirportServiceResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			AirportServices service = airportServiceMao.getById(id);
			if(service != null){
				GetAirportServiceResponse response = AirportServiceConverter.convertEntityToBean(service);
				return response;
			}
		}
		return null;
	}
	

	@Override
	public boolean existsById(String id) {
		if(airportServiceMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			airportServiceMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateAirportServiceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			AirportServices service = AirportServiceConverter.convertUpdateBeanToEntity(request);
			if(service.getSupportedAirports()==null){//As updateNonNull is used so if we want to set list to empty we should pass it as empty list but not null
				service.setSupportedAirports(new ArrayList<>());
			}
			airportServiceMao.updateNonNullById(service, request.getId());
			return true;
		}
		return false;
	}

}
