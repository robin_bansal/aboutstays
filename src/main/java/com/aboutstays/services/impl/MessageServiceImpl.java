package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.entities.Message;
import com.aboutstays.entities.MessageStatus;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.enums.MessageCreationType;
import com.aboutstays.mao.MessageMao;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.AddMessageRequest;
import com.aboutstays.response.dto.GetChatUsersResponse;
import com.aboutstays.response.dto.GetMessageResponse;
import com.aboutstays.services.MessageService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MessageConverter;
import com.aboutstays.utils.UserConverter;

@Service
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private MessageMao messageMao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StaysService staysService;

	@Override
	public String save(AddMessageRequest request) {
		Message message = MessageConverter.convertAddRequestToEntity(request);
		if(message!=null){
			messageMao.save(message);
			return message.getId();
		}
		return null;
	}

	@Override
	public GetMessageResponse getMessageById(String id) {
		Message message = messageMao.getById(id);
		return MessageConverter.convertEntityToGetResponse(message);
	}

	@Override
	public boolean existsById(String id) {
		Message message = messageMao.getById(id);
		if(message!=null)
			return true;
		return false;
	}

	@Override
	public List<GetMessageResponse> getAll() {
		List<Message> messages = messageMao.getAll();
		return MessageConverter.convertEntityListToGetList(messages);
	}

	@Override
	public List<GetMessageResponse> getAll(String userId, String staysId, String hotelId) {
		List<Message> messages = messageMao.getAll(userId, staysId, hotelId);
		return MessageConverter.convertEntityListToGetList(messages);
	}

	@Override
	public void delete(String id) {
		messageMao.delete(id);
	}

	@Override
	public void changeStatus(String userId, String hotelId,MessageCreationType creationType,MessageStatus messageStatus) {
		if(!StringUtils.isEmpty(userId)&&!StringUtils.isEmpty(hotelId)&&creationType!=null){
			messageMao.updateMessageStatus(userId,hotelId,creationType.getCode(),new Date(),messageStatus.getStatus());
		}
	}

	@Override
	public List<GetChatUsersResponse> getChatUsersByHotelId(String hotelId) {
		List<Message> messages = messageMao.getAllByHotelId(hotelId);
		if(CollectionUtils.isNotEmpty(messages)){
			Map<String,Integer> chatUserMap= new HashMap<String,Integer>();
			for(Message message:messages){
				String staysId = message.getStaysId();
				int status=0;
				if(!message.getStatus().equals(MessageStatus.READ.getStatus())){
					status=1;
				}
				if(!StringUtils.isEmpty(staysId)){
					if(chatUserMap.containsKey(staysId)){
						chatUserMap.put(staysId, chatUserMap.get(staysId)+status);
					}else{
						chatUserMap.put(staysId, status);
					}					
				}
			}
			List<GetChatUsersResponse> responses=new ArrayList<>();
			for(Entry<String, Integer> entry:chatUserMap.entrySet()){
				GetStayResponse stayData = staysService.getChatStayByStayId(entry.getKey());
				if(stayData!=null){
					UserPojo userPojo = UserConverter.convertUserToUserPojo(userService.getUserEntity(stayData.getUserId()));
					if(userPojo!=null){
						GetChatUsersResponse chatUsersResponse = new GetChatUsersResponse(userPojo, stayData,entry.getValue());
						responses.add(chatUsersResponse);											
					}
				}
			}
			return responses;
		}
		return null;
	}
}
