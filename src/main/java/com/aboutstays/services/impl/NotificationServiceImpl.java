package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.Notification;
import com.aboutstays.mao.NotificationMao;
import com.aboutstays.request.dto.AddNotificationRequest;
import com.aboutstays.response.dto.GetNotificationResponse;
import com.aboutstays.services.NotificationService;
import com.aboutstays.utils.NotificationConverter;

@Service
public class NotificationServiceImpl implements NotificationService{
	
	@Autowired
	private NotificationMao notificationMao;

	@Override
	public String addNotification(AddNotificationRequest request) {
		Notification notification = NotificationConverter.convertAddRequestToEntity(request);
		if(notification!=null){
			notificationMao.save(notification);
			return notification.getId();
		}
		return null;
	}

	@Override
	public GetNotificationResponse getNotificationById(String id) {
		Notification notification = notificationMao.getById(id);
		return NotificationConverter.convertEntityToGetRequest(notification);
	}

	@Override
	public List<GetNotificationResponse> getAllNotifications() {
		List<Notification> notifications = notificationMao.getAll(); 
		return NotificationConverter.convertEntityListToGetList(notifications);
	}

	@Override
	public List<GetNotificationResponse> getAllNotifications(String userId, String staysId, String hotelId) {
		List<Notification> notifications = notificationMao.getAll(userId, staysId, hotelId); 
		return NotificationConverter.convertEntityListToGetList(notifications);
	}

	@Override
	public boolean existsById(String id) {
		Notification notification = notificationMao.getById(id);
		if(notification!=null)
			return true;
		return false;
	}

	@Override
	public void deleteNotification(String id) {
		notificationMao.delete(id);
	}

}
