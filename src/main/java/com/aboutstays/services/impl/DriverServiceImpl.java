package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Driver;
import com.aboutstays.mao.DriverMao;
import com.aboutstays.request.dto.AddDriverRequest;
import com.aboutstays.request.dto.UpdateDriverRequest;
import com.aboutstays.response.dto.AddDriverResponse;
import com.aboutstays.response.dto.GetDriverResponse;
import com.aboutstays.services.DriverService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DriverConverter;

@Service
public class DriverServiceImpl implements DriverService {

	@Autowired
	private DriverMao driverMao;
	
	@Override
	public AddDriverResponse add(AddDriverRequest request) {
		if(request != null){
			Driver driver = DriverConverter.convertBeanToEntity(request);
			driverMao.add(driver);
			return new AddDriverResponse(driver.getId());
		}
		return null;
	}

	@Override
	public List<GetDriverResponse> getAll() {
		List<Driver> listDriver = driverMao.getAll();
		if(CollectionUtils.isNotEmpty(listDriver)){
			List<GetDriverResponse> listResponse = DriverConverter.convertListEntityToListBean(listDriver);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetDriverResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			Driver driver = driverMao.getById(id);
			GetDriverResponse response = DriverConverter.convertEntityToBean(driver);
			return response;
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			driverMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateDriverRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			Driver driver = DriverConverter.convertUpdateRequestToEntity(request);
			driverMao.updateNonNullById(driver, request.getId());
			return true;
		}
		return false;
	}

}
