package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.ReservationOrderMao;
import com.aboutstays.request.dto.AddReservationOrderRequest;
import com.aboutstays.request.dto.UpdateReservationOrderRequest;
import com.aboutstays.response.dto.GetReservationOrderResponse;
import com.aboutstays.services.ReservationOrderService;
import com.aboutstays.utils.ReservationOrderConverter;

@Service
public class ReservationOrderServiceImpl implements ReservationOrderService{

	@Autowired
	private ReservationOrderMao rsOrderMao;
	
	@Override
	public String save(AddReservationOrderRequest request) {
		ReservationOrder order = ReservationOrderConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			rsOrderMao.save(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public GetReservationOrderResponse getById(String id) {
		ReservationOrder order = rsOrderMao.getById(id);
		return ReservationOrderConverter.convertEntityToBean(order);
	}

	@Override
	public List<GetReservationOrderResponse> getAll() {
		List<ReservationOrder> listOrder = rsOrderMao.getAll();
		return ReservationOrderConverter.convertListEntityToListBean(listOrder);
	}

	@Override
	public boolean updateNonNullFieldsOfRequest(UpdateReservationOrderRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			ReservationOrder order = ReservationOrderConverter.convertUpdateBeanToEntity(request);
			if(order != null){
				rsOrderMao.updateNonNull(order);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			rsOrderMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(rsOrderMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public ReservationOrder getEntity(String id) {
		return rsOrderMao.getById(id);
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(status ==null){
			return false;
		}
		rsOrderMao.changeOrderStatus(id, status.getCode());
		return true;
	}
	
	

}
