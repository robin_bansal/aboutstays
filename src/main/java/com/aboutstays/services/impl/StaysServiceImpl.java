package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.constants.PropertyConstant;
import com.aboutstays.entities.Booking;
import com.aboutstays.entities.Hotel;
import com.aboutstays.entities.RoomCategory;
import com.aboutstays.entities.Stays;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.CheckinRequestStatus;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.enums.PartnershipType;
import com.aboutstays.enums.StaysOrdering;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.HotelMao;
import com.aboutstays.mao.RoomCategoryMao;
import com.aboutstays.mao.StaysMao;
import com.aboutstays.pojos.HotelGeneralInformation;
import com.aboutstays.pojos.StaysHotelPojo;
import com.aboutstays.request.dto.AddStaysRequest;
import com.aboutstays.request.dto.RoomgsAndStaysRequest;
import com.aboutstays.response.dto.AddStaysResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.GetHotelResponse;
import com.aboutstays.response.dto.GetRoomCategoryResponse;
import com.aboutstays.response.dto.GetRoomResponse;
import com.aboutstays.response.dto.GetStaysResponse;
import com.aboutstays.response.dto.GetStaysWithHotelInfoResponse;
import com.aboutstays.response.dto.LateCheckoutResposne;
import com.aboutstays.response.dto.RoomAndStaysData;
import com.aboutstays.response.dto.TotalNightsAndStaysResponse;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.CheckoutService;
import com.aboutstays.services.LateCheckoutRequestService;
import com.aboutstays.services.ProcessUtilityService;
import com.aboutstays.services.RoomService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.ValidateIdsService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.HotelConverter;
import com.aboutstays.utils.RoomCategoryConverter;
import com.aboutstays.utils.StaysConverter;

@Service
public class StaysServiceImpl implements StaysService{

	@Autowired
	private StaysMao staysMao;
	
	@Autowired
	private HotelMao hotelMao;
	
	@Autowired
	private RoomCategoryMao roomMao;  
	
	@Autowired
	private CheckinService checkinService;
	
	@Autowired
	private ValidateIdsService validateIdsService;
	
	@Autowired
	private CheckoutService checkoutService;
	
	@Autowired
	private ProcessUtilityService processUtilityService;

	@Autowired
	private LateCheckoutRequestService lcrService;
	
	@Autowired
	private RoomService roomService;
	
	@Override
	public AddStaysResponse addStays(AddStaysRequest reqeust) throws AboutstaysException {
		Stays stays = StaysConverter.convertBeanToEntity(reqeust);
		if(stays != null){
			staysMao.addStays(stays);
			return new AddStaysResponse(stays.getStaysId());
		}
		return null;
	}

	@Override
	public boolean existsByStaysId(String staysId) {
		if(StringUtils.isEmpty(staysId)){
			return false;
		}
		Stays stays = staysMao.getByStaysId(staysId);
		if(stays == null){
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteStays(String staysId) {
		if(StringUtils.isEmpty(staysId)){
			return false;
		}
		staysMao.deleteStays(staysId);
		return true;
	}

	@Override
	public GetStaysResponse getAllStays() throws AboutstaysException {
		List<Stays> listStays = staysMao.getAllStays();
		if(CollectionUtils.isEmpty(listStays)){
			return null;
		}
		List<GetStayResponse> listStaysPojo = StaysConverter.convertListEntityToListGetResponse(listStays);
		if(CollectionUtils.isEmpty(listStaysPojo)){
			return null;
		}
		return new GetStaysResponse(listStaysPojo);
	}

	@Override
	public GetStaysResponse getStaysByUserId(String userId) throws AboutstaysException {
		if(StringUtils.isEmpty(userId)){
			return null;
		}
		List<Stays> listStays = staysMao.getStaysByUserId(userId);
		List<GetStayResponse> listStasyPojo = StaysConverter.convertListEntityToListGetResponse(listStays);		
		if(CollectionUtils.isEmpty(listStasyPojo)){
			return null;
		}
		return new GetStaysResponse(listStasyPojo);
	}

	@Override
	public GetStayResponse getStayByStayId(String stayId){
		if(StringUtils.isEmpty(stayId)){
			return null;
		}
		return StaysConverter.convertEntityToStayResponse(staysMao.getByStaysId(stayId));
	}
	@Override
	public GetStayResponse getStayByStayBookingId(String bookingId) {
		if(StringUtils.isEmpty(bookingId)){
			return null;
		}
		Stays stays = staysMao.getByBookingId(bookingId);
		GetStayResponse response = StaysConverter.convertEntityToStayResponse(stays);
		if(response != null){
			return response;
		}
		return null;
	}

	@Override
	public GetStaysWithHotelInfoResponse getStaysWithHotelInfo(String userId) {
		if(StringUtils.isEmpty(userId)){
			return null;
		}
		List<Stays> listStays = staysMao.getStaysByUserId(userId);
		Iterator<Stays> iterator = listStays.iterator();
		List<StaysHotelPojo> hotelStaysList = new ArrayList<>();
		while(iterator.hasNext()){
			Stays stays = iterator.next();
			Hotel hotel = hotelMao.getHotelById(stays.getHotelId());
			if(hotel!=null&&hotel.getGeneralInformation()!=null){
				StaysHotelPojo hotelStay = new StaysHotelPojo();
				hotelStay.setStaysPojo(StaysConverter.convertEntityToStayResponse(stays));
				hotelStay.setGeneralInfo(hotel.getGeneralInformation());
				hotelStaysList.add(hotelStay);
			}
		}
		return new GetStaysWithHotelInfoResponse(hotelStaysList);
	}

	@Override
	public GetStaysWithHotelInfoResponse getAllStaysWithHotelInfo() {
		List<Stays> listStays = staysMao.getAllStays();
		List<StaysHotelPojo> hotelStaysList = new ArrayList<>();
		for(Stays stays : listStays){
			StaysHotelPojo hotelStay = new StaysHotelPojo();
			RoomCategory room = roomMao.getById(stays.getRoomId());
			if(room != null){
				hotelStay.setRoomPojo(RoomCategoryConverter.convertEntityToGetResponse(room));
			}
			Hotel hotel = hotelMao.getHotelById(stays.getHotelId());
			if(hotel != null && hotel.getGeneralInformation() != null){
				hotelStay.setStaysPojo(StaysConverter.convertEntityToStayResponse(stays));
				hotelStay.setGeneralInfo(hotel.getGeneralInformation());
			}
			hotelStaysList.add(hotelStay);
		}
		return new GetStaysWithHotelInfoResponse(hotelStaysList);
	}

	@Override
	public boolean existsByBookingId(String bookingId) {
		Stays stays = staysMao.getByBookingId(bookingId);
		if(stays!=null)
			return true;
		return false;
	}

	@Override
	public TotalNightsAndStaysResponse getTotalNightsAndStays(String userId) throws AboutstaysException {
		TotalNightsAndStaysResponse response = null;
		long currentTime = new Date().getTime();
		if(!StringUtils.isEmpty(userId)){
			List<Stays> listStays = staysMao.getStaysByUserId(userId);
			List<StaysHotelPojo> hotelStaysList = new ArrayList<>();
			List<StaysHotelPojo> pastStaysList = new ArrayList<>();
			
			Map<String, GetRoomCategoryResponse> roomIdVsPojo = new HashMap<>();
			Map<String, GetHotelResponse> hotelIdVsGeneralInfo = new HashMap<>();
			
			int totalNights = 0;
			if(CollectionUtils.isNotEmpty(listStays)){
				int futureStayHours = processUtilityService.getIntegerProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_FUTURE_STAYLINE_HOURS, 24);
				
				for(Stays stays : listStays){
					StaysHotelPojo hotelStay = new StaysHotelPojo();
					GetStayResponse staysPojo = StaysConverter.convertEntityToStayResponse(stays);
					
					hotelStay.setStaysPojo(staysPojo);
					
					HotelGeneralInformation hotelGeneralInformation = null;
					GetHotelResponse hotelResponse = hotelIdVsGeneralInfo.get(stays.getHotelId());
					if(hotelResponse==null){
						hotelResponse = HotelConverter.convertHotelToGetHotelResponse(hotelMao.getHotelById(stays.getHotelId()));
						if(hotelResponse==null||hotelResponse.getGeneralInformation()==null){
							continue;
						}
						hotelGeneralInformation = hotelResponse.getGeneralInformation();
//						hotelStay.setHotelType(hotelResponse.getHotelType());
//						hotelStay.setPartnershipType(hotelResponse.getPartnershipType());
						hotelIdVsGeneralInfo.put(stays.getHotelId(), hotelResponse);
					} else {
						hotelGeneralInformation = hotelResponse.getGeneralInformation();
					}
					hotelStay.setGeneralInfo(hotelGeneralInformation);
					
					if(PartnershipType.PARTNER.equals(PartnershipType.findValueByCode(hotelResponse.getPartnershipType()))){
						GetRoomCategoryResponse roomResponse = roomIdVsPojo.get(stays.getRoomId());
						if(roomResponse==null){
							roomResponse = RoomCategoryConverter.convertEntityToGetResponse(roomMao.getById(stays.getRoomId()));
							if(roomResponse == null){
								continue;
							} 
							roomIdVsPojo.put(stays.getRoomId(), roomResponse);
						}
						hotelStay.setRoomPojo(roomResponse);
					}
					hotelStay.setHotelType(hotelResponse.getHotelType());
					hotelStay.setPartnershipType(hotelResponse.getPartnershipType());	
					

					staysPojo.setStaysOrdering(StaysConverter.getStaysOrdering(stays, currentTime, futureStayHours).getCode());
					if(StaysOrdering.PAST.equals(StaysOrdering.findByCode(staysPojo.getStaysOrdering()))){
						staysMao.changeStatus(stays.getStaysId(), StaysStatus.CHECK_OUT.getCode());
					}
					
					long diffInMilis = stays.getCheckOutDate().getTime() - stays.getCheckInDate().getTime();
					totalNights += TimeUnit.DAYS.convert(diffInMilis, TimeUnit.MILLISECONDS);
					
					// Getting actual CheckIn Time
					String actualCheckedIn = getActualCheckinTime(userId, stays.getHotelId(), stays.getStaysId());
					if(StringUtils.isEmpty(actualCheckedIn)) {
						actualCheckedIn = stays.getCheckInTime();
					}
//					if(actualCheckedIn == 0 && stays.getCreated() != null) {
//						actualCheckedIn = stays.getCreated();
//					}
					hotelStay.setActualCheckedIn(actualCheckedIn);
					
					// Getting actual checked out Time
					String actualCheckedOut = getActualCheckoutTime(userId, stays.getHotelId(), stays.getStaysId());
					if(StringUtils.isEmpty(actualCheckedOut)) {
						actualCheckedOut = stays.getCheckOutTime();
					}
					hotelStay.setActualCheckedOut(actualCheckedOut);
					
					if(hotelStay.getStaysPojo() != null) {
						if(hotelStay.getStaysPojo().getStaysOrdering() == StaysOrdering.PAST.getCode()) {
							pastStaysList.add(hotelStay);
						} else {
							hotelStaysList.add(hotelStay);
						}
					}
				}
				
				if(CollectionUtils.isNotEmpty(hotelStaysList)){
					Collections.sort(hotelStaysList, new Comparator<StaysHotelPojo>() {
						@Override
						public int compare(StaysHotelPojo o1, StaysHotelPojo o2) {
							GetStayResponse stays1 = o1.getStaysPojo();
							GetStayResponse stays2 = o2.getStaysPojo();
							String checkin1 = stays1.getCheckInDate();
							String checkin2 = stays2.getCheckInDate();
							String checkout1 = stays1.getCheckOutDate();
							String checkout2 = stays2.getCheckOutDate();
							try{
								Date checkin1Date = DateUtil.parseDate(checkin1, DateUtil.BASE_DATE_FORMAT);
								Date checkin2Date = DateUtil.parseDate(checkin2, DateUtil.BASE_DATE_FORMAT);

								if(checkin1Date.compareTo(checkin2Date) == 0) {
									Date checkout1Date = DateUtil.parseDate(checkout1, DateUtil.BASE_DATE_FORMAT);
									Date checkout2Date = DateUtil.parseDate(checkout2, DateUtil.BASE_DATE_FORMAT);
									return checkout1Date.compareTo(checkout2Date);
								}
								return checkin1Date.compareTo(checkin2Date);
							} catch (Exception e){
								
							}
							return 0;
						}
					});
				}
				
				if(CollectionUtils.isNotEmpty(pastStaysList)) {
					Collections.sort(pastStaysList, new Comparator<StaysHotelPojo>() {

						@Override
						public int compare(StaysHotelPojo o1, StaysHotelPojo o2) {
							GetStayResponse stays1 = o1.getStaysPojo();
							GetStayResponse stays2 = o2.getStaysPojo();
							String checkin1 = stays1.getCheckInDate();
							String checkin2 = stays2.getCheckInDate();
							String checkout1 = stays1.getCheckOutDate();
							String checkout2 = stays2.getCheckOutDate();
							try{
								Date checkin1Date = DateUtil.parseDate(checkin1, DateUtil.BASE_DATE_FORMAT);
								Date checkin2Date = DateUtil.parseDate(checkin2, DateUtil.BASE_DATE_FORMAT);
								if(checkin1Date.compareTo(checkin2Date) == 0) {
									Date checkout1Date = DateUtil.parseDate(checkout1, DateUtil.BASE_DATE_FORMAT);
									Date checkout2Date = DateUtil.parseDate(checkout2, DateUtil.BASE_DATE_FORMAT);
									return checkout2Date.compareTo(checkout1Date);
								}
								return checkin2Date.compareTo(checkin1Date);
							} catch (Exception e){
								
							}
							return 0;
						}
					});
				}
				hotelStaysList.addAll(pastStaysList);
				response = new TotalNightsAndStaysResponse();
				response.setHotels(hotelIdVsGeneralInfo.size());
				response.setNights(totalNights);
				response.setHotelStaysList(hotelStaysList);
			}
		}
		return response;
	}
	
	public String getActualCheckinTime(String userId, String hotelId, String staysId) throws AboutstaysException {
		String actualCheckedIn = "";
		validateIdsService.validateUserHotelStaysIds(userId, hotelId, staysId);
		CheckinResponse response = checkinService.getByUserStaysHotelId(userId, staysId, hotelId);
		if(response != null) {
//			if(response.getCreated() != null) {
//				actualCheckedIn = response.getCreated();
//			}
			if(response.getEarlyCheckinRequest() != null && response.getEarlyCheckinRequested()
					&& response.getStatus() == CheckinRequestStatus.APPROVED.getCode()) {
				if(!StringUtils.isEmpty(response.getEarlyCheckinRequest().getRequestedCheckinTime())) {
					actualCheckedIn = response.getEarlyCheckinRequest().getRequestedCheckinTime();
					//actualCheckedIn = DateUtil.convertStringToLong(DateUtil.MENU_ITEM_TIME_FORMAT, response.getEarlyCheckinRequest().getRequestedCheckinTime());
				}
			}
		}
		return actualCheckedIn;
	}
	
	private String getActualCheckoutTime(String userId, String hotelId, String staysId) throws AboutstaysException {
		String actualCheckedOut = "";
		validateIdsService.validateUserHotelStaysIds(userId, hotelId, staysId);
//		CheckoutResponse response = checkoutService.getByUserStaysHotelId(userId, staysId, hotelId);
//		if(response != null) {
//			if(response.getUpdated() != null) {
//				actualCheckedOut = response.getUpdated();
//			}
//		}
		LateCheckoutResposne lateResponse = lcrService.retrieveLateCheckoutRequest(userId, hotelId, staysId);
		if(lateResponse != null && lateResponse.getStatus() == GeneralOrderStatus.COMPLETED.getCode()) {
			long checkoutTime = lateResponse.getCheckoutTime();
			actualCheckedOut = DateUtil.format(new Date(checkoutTime), DateUtil.BASE_DATE_FORMAT);
		}
		return actualCheckedOut;
	}
	

	@Override
	public List<Stays> getStaysOfBooking(String bookingId) {
		if(StringUtils.isEmpty(bookingId)){
			return null;
		}
		return staysMao.getStaysOfBooking(bookingId);
	}

	public boolean existsByUserIdAndBookingId(String userId, String bookingId) {
		Stays stays = staysMao.getStaysByUserAndBookingId(userId, bookingId);
		if(stays!=null)
			return true;
		return false;
	}

	@Override
	public void changeStaysStatus(String staysId, StaysStatus staysStatus) {
		if(StringUtils.isEmpty(staysId)||staysStatus==null)
			return;
		staysMao.changeStatus(staysId, staysStatus.getCode());
	}

	@Override
	public void changeTripType(String staysId, boolean isOfficial) {
		staysMao.changeTripType(staysId, isOfficial);
	}

	@Override
	public List<RoomAndStaysData> getRoomsAndStays(RoomgsAndStaysRequest request) {
		List<RoomAndStaysData> list=new ArrayList<>();
		List<GetRoomResponse> roomsList = roomService.getRoomsByFloorId(request.getFloorId());
		if(CollectionUtils.isNotEmpty(roomsList)){
			for(GetRoomResponse getRoomResponse:roomsList){
				RoomAndStaysData roomAndStaysData=new RoomAndStaysData();
				roomAndStaysData.setRoomData(getRoomResponse);
				try {
					roomAndStaysData.setStayList(getStaysByRoomIdAndDate(getRoomResponse.getRoomId(),request.getDate()));
				} catch (AboutstaysException e) {
					e.printStackTrace();
				}
				list.add(roomAndStaysData);
			}
		}
		return list;
	}

	private List<GetStayResponse> getStaysByRoomIdAndDate(String roomId, String date) throws AboutstaysException {
		List<Stays> listStays = staysMao.getStaysByRoomId(roomId);
		if(CollectionUtils.isNotEmpty(listStays)){
			return StaysConverter.convertListEntityToListGetResponse(listStays);
		}
		return null;
	}

	@Override
	public GetStayResponse getChatStayByStayId(String staysId) {
		Stays stays = staysMao.getByStaysId(staysId);
		if(stays != null){
			int futureStayHours = processUtilityService.getIntegerProcessProperty(PropertyConstant.PROCESS_STAYS, PropertyConstant.PROPERTY_FUTURE_STAYLINE_HOURS, 24);
			StaysOrdering staysOrdering = StaysConverter.getStaysOrdering(stays, System.currentTimeMillis(),futureStayHours);
			if(staysOrdering!=null){
				switch (staysOrdering) {
				case CURRENT:
				case FUTURE:
					return StaysConverter.convertEntityToStayResponse(stays);
				}
			}
		}
		return null;
	}

	@Override
	public String addStayForBooking(Booking booking) {
		if(booking==null)
			return null;
		Hotel hotel = hotelMao.getHotelById(booking.getHotelId());
		if(hotel==null||hotel.getGeneralInformation()==null)
			return null;
		long currentTime = System.currentTimeMillis();
		Stays stays = new Stays();
		stays.setCreated(currentTime);
		stays.setUpdated(currentTime);
		stays.setBookingId(booking.getBookingId());
		stays.setCheckInDate(booking.getCheckInDate());
		stays.setCheckOutDate(booking.getCheckOutDate());
		stays.setCheckInTime(hotel.getGeneralInformation().getCheckInTime());
		stays.setCheckOutTime(hotel.getGeneralInformation().getCheckOutTime());
		stays.setHotelId(booking.getHotelId());
		stays.setOfficial(false);
		stays.setRoomId(booking.getRoomId());
		stays.setStaysStatus(StaysStatus.CHECK_IN.getCode());
		staysMao.addStays(stays);
		return stays.getStaysId();
	}

	@Override
	public void setUserId(String staysId, String userId) {
		Stays stays = staysMao.getByStaysId(staysId);
		if(stays!=null){
			stays.setUserId(userId);
			stays.setAddedInApp(true);
			stays.setUpdated(System.currentTimeMillis());
			staysMao.update(stays);
		}
	}

	@Override
	public Stays getEntityById(String staysId) {
		return staysMao.getByStaysId(staysId);
	}

	@Override
	public String getBookingIdByStaysId(String staysId) throws AboutstaysException {
		Stays stays = staysMao.getByStaysId(staysId);
		if(stays==null){
			throw new AboutstaysException(AboutstaysResponseCode.STAYS_NOT_FOUND);
		}
		return null;
	}

	@Override
	public void setRoomId(String defaultStaysId, String roomId) {
		staysMao.setRoomId(defaultStaysId, roomId);
	}

}
