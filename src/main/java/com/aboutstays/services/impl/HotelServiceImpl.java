package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.GeneralService;
import com.aboutstays.entities.Hotel;
import com.aboutstays.entities.MasterAmenity;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.HotelType;
import com.aboutstays.enums.HotelUpdateBy;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.GeneralServiceMao;
import com.aboutstays.mao.HotelMao;
import com.aboutstays.mao.MasterAmenityMao;
import com.aboutstays.pojos.Amenity;
import com.aboutstays.pojos.AwardPojo;
import com.aboutstays.pojos.CardAccepted;
import com.aboutstays.pojos.CheckInDetails;
import com.aboutstays.pojos.CheckOutDetails;
import com.aboutstays.pojos.Essential;
import com.aboutstays.pojos.HotelAward;
import com.aboutstays.pojos.HotelAwardResponse;
import com.aboutstays.pojos.HotelInfoMetaData;
import com.aboutstays.pojos.HotelPojo;
import com.aboutstays.pojos.ReservationServicePojo;
import com.aboutstays.pojos.StaysDetails;
import com.aboutstays.request.dto.CreateHotelRequest;
import com.aboutstays.request.dto.GetHotelGeneralServices;
import com.aboutstays.request.dto.UpdateHotelRequest;
import com.aboutstays.request.dto.UpdateHotelServices;
import com.aboutstays.request.dto.UpdatePartnershipRequest;
import com.aboutstays.response.dto.CreateHotelResponse;
import com.aboutstays.response.dto.GetGSResponseWithHS;
import com.aboutstays.response.dto.GetHotelBasicInfoList;
import com.aboutstays.response.dto.GetHotelGeneralInfoResponse;
import com.aboutstays.response.dto.GetHotelListResponse;
import com.aboutstays.response.dto.GetHotelResponse;
import com.aboutstays.response.dto.GetLoyaltyProgramResponse;
import com.aboutstays.response.dto.HotelBasicInfoPojo;
import com.aboutstays.services.HotelService;
import com.aboutstays.services.HouseRulesService;
import com.aboutstays.services.LoyaltyProgramsService;
import com.aboutstays.services.ReservationServicesService;
import com.aboutstays.services.TimezoneService;
import com.aboutstays.utils.AmenityConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.GeneralServicesConverter;
import com.aboutstays.utils.HotelConverter;

@Service
public class HotelServiceImpl implements HotelService {

	@Autowired
	private HotelMao hotelMao;

	@Autowired
	private AutowireCapableBeanFactory beanFactory;

	@Autowired
	private GeneralServiceMao generalServiceMao;

	@Autowired
	private MasterAmenityMao masterAmenityMao;

	@Autowired
	private ReservationServicesService rsService;

	@Autowired
	private TimezoneService timezoneService;

	@Autowired
	private LoyaltyProgramsService loyaltyProgramsService;

	@Autowired
	private HouseRulesService houseRulesService;

	private Map<String, String> hotelIdVsTimezone = new ConcurrentHashMap<>();

	@Override
	public CreateHotelResponse createHotel(CreateHotelRequest createHotelRequest) {
		Hotel hotel = HotelConverter.convertCreateHotelRequestToEntity(createHotelRequest);
		if (hotel != null) {
			hotelMao.save(hotel);
			return new CreateHotelResponse(hotel.getId());
		}
		return null;
	}

	public GetHotelListResponse getAllHotels() {
		List<Hotel> listHotel = hotelMao.getAllHotels();
		List<HotelPojo> listHotelPojo = getHotelsWithReservationService(listHotel);
		return new GetHotelListResponse(listHotelPojo);
	}

	// public String getReservationServiceIdFromHotel(Hotel hotel) {
	// String reservationServiceId = "";
	// if (hotel != null) {
	// if (hotel.getStaysDetails() != null) {
	// if
	// (CollectionUtils.isMapNotEmpty(hotel.getStaysDetails().getMapGSIdVsHSId()))
	// {
	// for (Map.Entry<String, String> mapGsIdVsSIds :
	// hotel.getStaysDetails().getMapGSIdVsHSId()
	// .entrySet()) {
	// GeneralService gs = gsMao.getById(mapGsIdVsSIds.getKey());
	// if (gs != null) {
	// GeneralServiceType serviceType = GeneralServiceType
	// .findGeneralServiceByType(gs.getServiceType());
	// if (serviceType == null) {
	// continue;
	// }
	// if (serviceType == GeneralServiceType.RESERVATION) {
	// reservationServiceId = mapGsIdVsSIds.getValue();
	// break;
	// }
	// }
	// }
	// }
	// }
	// }
	// return reservationServiceId;
	// }

	public List<HotelPojo> getHotelsWithReservationService(List<Hotel> listHotels) {
		List<HotelPojo> listHotelPojo = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(listHotels)) {
			for (Hotel hotel : listHotels) {
				// if(hotel == null) {
				// continue;
				// }
				// String reservationGsId = "";
				// String reservationServiceId = "";
				// if(hotel.getStaysDetails() != null) {
				// if(CollectionUtils.isMapNotEmpty(hotel.getStaysDetails().getMapGSIdVsHSId()))
				// {
				// for(Map.Entry<String, String> mapGsIdVsSIds :
				// hotel.getStaysDetails().getMapGSIdVsHSId().entrySet()) {
				// GeneralService gs = gsMao.getById(mapGsIdVsSIds.getKey());
				// if(gs != null) {
				// GeneralServiceType serviceType =
				// GeneralServiceType.findGeneralServiceByType(gs.getServiceType());
				// if(serviceType == null) {
				// continue;
				// }
				// if(serviceType == GeneralServiceType.RESERVATION) {
				// reservationGsId = mapGsIdVsSIds.getKey();
				// reservationServiceId = mapGsIdVsSIds.getValue();
				// break;
				// }
				// }
				// }
				// }
				// }

				// String reservationServiceId =
				// getReservationServiceIdFromHotel(hotel);
				String reservationServiceId = hotel.getId();
				HotelPojo hotelPojo = HotelConverter.convertHotelToHotelPojo(hotel);

				if (!StringUtils.isEmpty(reservationServiceId)) {
					ReservationServicePojo rsPojo = rsService.getById(reservationServiceId);
					hotelPojo.setReservationServicePojo(rsPojo);
				}

				// GetHotelOtherInfoResponse otherInfoPojo =
				// hotelOtherInfoService.get(hotel.getId());
				// hotelPojo.setHotelOtherInfo(otherInfoPojo);

				listHotelPojo.add(hotelPojo);
			}
		}
		return listHotelPojo;
	}

	public List<HotelPojo> getHotelsListWithOtherInfo(List<Hotel> listHotel) {
		List<HotelPojo> hotelListWithOtherInfo = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(listHotel)) {
			for (Hotel hotel : listHotel) {
				if (hotel == null) {
					continue;
				}
				HotelPojo hotelPojo = HotelConverter.convertHotelToHotelPojo(hotel);
				// HotelOtherInfo hotelOtherInfo =
				// otherInfoMao.get(hotel.getId());
				// if (hotelOtherInfo != null) {
				// HotelOtherInfoPojo otherInfoPojo = new HotelOtherInfoPojo();
				// otherInfoPojo.setId(hotelOtherInfo.getId());
				// otherInfoPojo.setListAwards(hotelOtherInfo.getListAwards());
				// otherInfoPojo.setListCardAccepted(hotelOtherInfo.getListCardAccepted());
				// hotelPojo.setHotelOtherInfo(otherInfoPojo);
				// }
				hotelListWithOtherInfo.add(hotelPojo);
			}
		}
		return hotelListWithOtherInfo;
	}

	@Override
	public GetHotelListResponse getHotelListByType(int type) {
		if (HotelType.findHotelByType(type) == null) {
			return null;
		}
		List<Hotel> listHotel = hotelMao.getHotelsByType(type);
		List<HotelPojo> listHotelPojo = getHotelsWithReservationService(listHotel);
		return new GetHotelListResponse(listHotelPojo);
	}

	@Override
	public GetHotelResponse getHotelById(String hotelId) {
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			return parseEntityToGetResponse(hotel);
		}
		return null;
	}

	public GetHotelResponse parseEntityToGetResponse(Hotel hotel) {
		if (hotel == null)
			return null;
		GetHotelResponse response = HotelConverter.convertHotelToGetHotelResponse(hotel);
		List<String> amenities = hotel.getAmenities();
		if (CollectionUtils.isNotEmpty(amenities)) {
			List<Amenity> amenitiyList = new ArrayList<>();
			for (String amenityId : amenities) {
				MasterAmenity masterAmenity = masterAmenityMao.getById(amenityId);
				amenitiyList.add(AmenityConverter.convertMasterEntityToAmenity(masterAmenity));
			}
			response.setAmenities(amenitiyList);
		}
		List<String> essentialIds = hotel.getEssentials();
		if (CollectionUtils.isNotEmpty(essentialIds)) {
			List<Essential> listEssentials = new ArrayList<>();
			for (String id : essentialIds) {
				MasterAmenity masterAmenity = masterAmenityMao.getById(id);
				Essential essential = AmenityConverter.convertMasterEntityToEssensial(masterAmenity);
				listEssentials.add(essential);
			}
			response.setEssentials(listEssentials);
		}
		List<HotelAward> awards = hotel.getAwards();
		if (CollectionUtils.isNotEmpty(awards)) {
			List<HotelAwardResponse> awardResponces = new ArrayList<>();
			response.setListAwards(awardResponces);
			for (HotelAward awardsPojo : awards) {
				MasterAmenity masterAmenity = masterAmenityMao.getById(awardsPojo.getAwardId());
				AwardPojo award = AmenityConverter.convertMasterEntityToAwards(masterAmenity);

				HotelAwardResponse awardResponce = new HotelAwardResponse();
				awardResponce.setAward(award);
				awardResponce.setYear(awardsPojo.getYear());
				awardResponces.add(awardResponce);
			}
		}
		List<String> cardsAccepted = hotel.getCardsAccepted();
		if (CollectionUtils.isNotEmpty(cardsAccepted)) {
			List<CardAccepted> cardsList = new ArrayList<>();
			for (String cardId : cardsAccepted) {
				MasterAmenity masterAmenity = masterAmenityMao.getById(cardId);
				cardsList.add(AmenityConverter.convertMasterEntityToCard(masterAmenity));
			}
			response.setListCardAccepted(cardsList);
		}
		List<String> loyaltyProgramsList = hotel.getLoyaltyPrograms();
		if (CollectionUtils.isNotEmpty(loyaltyProgramsList)) {
			List<GetLoyaltyProgramResponse> loyaltyList = new ArrayList<>();
			for (String loyaltyPrograId : loyaltyProgramsList) {
				GetLoyaltyProgramResponse getLoyaltyProgramResponse = loyaltyProgramsService.getById(loyaltyPrograId);
				if (getLoyaltyProgramResponse != null) {
					loyaltyList.add(getLoyaltyProgramResponse);
				}
			}
			response.setListLoyaltyPrograms(loyaltyList);
		}

		// String reservationServiceId =
		// getReservationServiceIdFromHotel(hotel);
		String reservationServiceId = hotel.getId();
		if (!StringUtils.isEmpty(reservationServiceId)) {
			ReservationServicePojo rsPojo = rsService.getById(reservationServiceId);
			response.setReservationServicePojo(rsPojo);
		}
		return response;
	}

	@Override
	public boolean hotelExistsById(String hotelId) {
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateHotelByType(HotelUpdateBy type, String value, UpdateHotelRequest updateHotelRequest) {
		if (updateHotelRequest == null) {
			return false;
		}
		Hotel hotel = HotelConverter.convertUpdateHotelRequestToEntity(updateHotelRequest);
		hotelMao.hotelUpdateNonNullFields(type, value, hotel);
		return true;
	}

	@Override
	public boolean deleteHotelById(String hotelId) {
		if (StringUtils.isEmpty(hotelId)) {
			return false;
		}
		hotelMao.deleteHotelById(hotelId);
		return true;
	}

	public void removeRoom(String hotelId, String roomId) {
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null && !CollectionUtils.isEmpty(hotel.getRoomsIds())) {
			Iterator<String> iterator = hotel.getRoomsIds().iterator();
			while (iterator.hasNext()) {
				if (iterator.next().equals(roomId))
					iterator.remove();
			}
			hotelMao.update(hotel);
		}
	}

	@Override
	public void addRoomToHotel(String hotelId, String roomId) {
		hotelMao.addRoomToHotel(hotelId, roomId);
	}

	@Override
	public GetHotelBasicInfoList getHotelByCityAndName(String city, String name) {
		if (StringUtils.isEmpty(city) && StringUtils.isEmpty(name)) {
			return null;
		}
		List<Hotel> listHotels = new ArrayList<Hotel>();
		List<Hotel> allHotels = hotelMao.getHotelListWithBasicInfo();
		for (Hotel hotel : allHotels) {
			if (hotel.getGeneralInformation() != null && hotel.getGeneralInformation().getAddress() != null
					&& !StringUtils.isEmpty(hotel.getGeneralInformation().getAddress().getCity())
					&& !StringUtils.isEmpty(hotel.getGeneralInformation().getName())) {
				if (StringUtils.isEmpty(city) && !StringUtils.isEmpty(name)) {
					if (hotel.getGeneralInformation().getName().toLowerCase().startsWith(name.toLowerCase())) {
						listHotels.add(hotel);
					}
				} else if (StringUtils.isEmpty(name) && !StringUtils.isEmpty(city)) {
					if (hotel.getGeneralInformation().getAddress().getCity().toLowerCase()
							.startsWith(city.toLowerCase())) {
						listHotels.add(hotel);
					}
				} else {
					if (hotel.getGeneralInformation().getName().toLowerCase().startsWith(name.toLowerCase())
							&& hotel.getGeneralInformation().getAddress().getCity().toLowerCase()
									.startsWith(city.toLowerCase())) {
						listHotels.add(hotel);
					}
				}
			}
		}
		List<HotelBasicInfoPojo> listHotelsBasicInfo = HotelConverter.convertBasicInfoListToPojoList(listHotels);
		if (listHotelsBasicInfo == null) {
			return null;
		}
		return new GetHotelBasicInfoList(listHotelsBasicInfo);
	}

	@Override
	public boolean updatePartnershipType(UpdatePartnershipRequest request) {
		if (StringUtils.isEmpty(request.getHotelId()) || StringUtils.isEmpty(request.getPartnershipType())) {
			return false;
		}
		hotelMao.UpdatePartnershipType(request.getHotelId(), request.getPartnershipType());
		return true;
	}

	@Override
	public GetHotelGeneralServices getGeneralServices(String hotelId, StaysStatus statusEnum, Boolean isGI) {
		if (!StringUtils.isEmpty(hotelId)) {
			Hotel hotel = hotelMao.getHotelById(hotelId);
			if (hotel != null) {
				GeneralServicesServiceImpl gsImpl = new GeneralServicesServiceImpl();
				beanFactory.autowireBean(gsImpl);
				List<GetGSResponseWithHS> listGS = new ArrayList<>();
				// Map<String, String> mapGSIdVsHSId = new HashMap<>();
				List<String> listGSIds = new ArrayList<>();
				switch (statusEnum) {
				case CHECK_IN:
					if (hotel.getCheckInDetails() != null) {
						if (CollectionUtils.isNotEmpty(hotel.getCheckInDetails().getListGSIds())) {
							listGSIds = hotel.getCheckInDetails().getListGSIds();
						}
						// if
						// (CollectionUtils.isMapNotEmpty(hotel.getCheckInDetails().getMapGSIdVsHSId()))
						// {
						// mapGSIdVsHSId =
						// hotel.getCheckInDetails().getMapGSIdVsHSId();
						// }
					}
					break;
				case STAYING:
					if (hotel.getStaysDetails() != null) {
						if (CollectionUtils.isNotEmpty(hotel.getStaysDetails().getListGSIds())) {
							listGSIds = hotel.getStaysDetails().getListGSIds();
						}
						// if
						// (CollectionUtils.isMapNotEmpty(hotel.getStaysDetails().getMapGSIdVsHSId()))
						// {
						// mapGSIdVsHSId =
						// hotel.getStaysDetails().getMapGSIdVsHSId();
						// }
					}
					break;
				case CHECK_OUT:
					if (hotel.getCheckOutDetails() != null) {
						if (CollectionUtils.isNotEmpty(hotel.getCheckOutDetails().getListGSIds())) {
							listGSIds = hotel.getCheckOutDetails().getListGSIds();
						}
						// if
						// (CollectionUtils.isMapNotEmpty(hotel.getCheckOutDetails().getMapGSIdVsHSId()))
						// {
						// mapGSIdVsHSId =
						// hotel.getCheckOutDetails().getMapGSIdVsHSId();
						// }
					}
					break;
				}

				for (String gsId : listGSIds) {
					GeneralService service = generalServiceMao.getById(gsId, isGI);
					if (service == null) {
						continue;
					}
					GetGSResponseWithHS response = GeneralServicesConverter.convertEntityToGetGSResponseWithHS(service);
					response.setHsId(hotelId);
					// if (mapGSIdVsHSId.containsKey(gsId)) {
					// response.setHsId(mapGSIdVsHSId.get(gsId));
					// }
					listGS.add(response);
				}

				GetHotelGeneralServices response = new GetHotelGeneralServices();
				response.setListGS(listGS);
				return response;
			}
		}
		return null;
	}

	@Override
	public Boolean updateServiceDetails(UpdateHotelServices request, StaysStatus statusEnum) {
		Hotel hotel = hotelMao.getHotelById(request.getHotelId());
		if (hotel != null) {
			// Map<String, String> mapGSIdVsHSId = new HashMap<>();
			// Map<String, String> reqMap = request.getMapGSIdVsHSId();
			// if (reqMap == null) {
			// reqMap = new HashMap<>();
			// }
			List<String> reqList = request.getListGSIds();
			if (reqList == null) {
				reqList = new ArrayList<>();
			}
			List<String> listGSIds = new ArrayList<>();
			switch (statusEnum) {
			case CHECK_IN:
				if (hotel.getCheckInDetails() != null) {
					listGSIds = hotel.getCheckInDetails().getListGSIds();
					// mapGSIdVsHSId =
					// hotel.getCheckInDetails().getMapGSIdVsHSId();
				} else {
					CheckInDetails checkInDetails = new CheckInDetails();
					hotel.setCheckInDetails(checkInDetails);
					// hotel.getCheckInDetails().setMapGSIdVsHSId(reqMap);
					hotel.getCheckInDetails().setListGSIds(reqList);
				}
				break;
			case STAYING:
				if (hotel.getStaysDetails() != null) {
					listGSIds = hotel.getStaysDetails().getListGSIds();
					// mapGSIdVsHSId =
					// hotel.getStaysDetails().getMapGSIdVsHSId();
				} else {
					StaysDetails staysDetails = new StaysDetails();
					hotel.setStaysDetails(staysDetails);
					// hotel.getStaysDetails().setMapGSIdVsHSId(reqMap);
					hotel.getStaysDetails().setListGSIds(reqList);
				}
				break;
			case CHECK_OUT:
				if (hotel.getCheckOutDetails() != null) {
					listGSIds = hotel.getCheckOutDetails().getListGSIds();
					// mapGSIdVsHSId =
					// hotel.getCheckOutDetails().getMapGSIdVsHSId();
				} else {
					CheckOutDetails checkOutDetails = new CheckOutDetails();
					hotel.setCheckOutDetails(checkOutDetails);
					// hotel.getCheckOutDetails().setMapGSIdVsHSId(reqMap);
					hotel.getCheckOutDetails().setListGSIds(listGSIds);
				}
				break;
			}

			if (CollectionUtils.isEmpty(listGSIds)) {
				if (CollectionUtils.isNotEmpty(reqList)) {
					listGSIds = new ArrayList<>();
					listGSIds.addAll(reqList);
				}
			} else if (CollectionUtils.isNotEmpty(reqList)) {
				for (String reqId : reqList) {
					boolean alreadyExists = false;
					for (String gsId : listGSIds) {
						if (reqId.equals(gsId)) {
							alreadyExists = true;
							break;
						}
					}
					if (!alreadyExists) {
						listGSIds.add(reqId);
					}
				}
			}

			// if (CollectionUtils.isMapEmpty(mapGSIdVsHSId)) {
			// if (CollectionUtils.isMapNotEmpty(reqMap)) {
			// mapGSIdVsHSId = new HashMap<>();
			// mapGSIdVsHSId.putAll(reqMap);
			// }
			// } else if (CollectionUtils.isMapNotEmpty(reqMap)) {
			// Map<String, String> iterationMap = new HashMap<>();
			// iterationMap.putAll(mapGSIdVsHSId);
			// for (Map.Entry<String, String> gsVsHsID :
			// iterationMap.entrySet()) {
			// for (Map.Entry<String, String> reqGSVSHSId : reqMap.entrySet()) {
			// if (!reqGSVSHSId.getKey().equals(gsVsHsID.getKey())) {
			// mapGSIdVsHSId.put(reqGSVSHSId.getKey(), reqGSVSHSId.getValue());
			// }
			// }
			// }
			// }

			// for (Map.Entry<String, String> gsVsHSId :
			// mapGSIdVsHSId.entrySet()) {
			// if (!listGSIds.contains(gsVsHSId.getKey())) {
			// listGSIds.add(gsVsHSId.getKey());
			// }
			// }
			// hotelMao.updateHotelServiceDetails(mapGSIdVsHSId, listGSIds,
			// request.getHotelId(), statusEnum);
			hotelMao.updateHotelServiceDetails(listGSIds, request.getHotelId(), statusEnum);
			return true;
		}
		return false;
	}

	@Override
	public GetHotelGeneralInfoResponse getGeneralInfoById(String hotelId) {
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			return HotelConverter.convertHotelToGetGeneralInfoResponse(hotel);
		}
		return null;
	}

	@Override
	public void updateAmenities(String hotelId, List<String> amenityIds) {
		if (StringUtils.isEmpty(hotelId) || amenityIds == null)
			return;
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {

			List<String> savedAmenities = getUpdatedList(hotel.getAmenities(), amenityIds);
			;
			hotel.setAmenities(savedAmenities);
			hotelMao.update(hotel);
		}
	}

	private List<String> getUpdatedList(List<String> oldList, List<String> newList) {
		Set<String> set = new HashSet<>(newList);
		set.addAll(newList);
		newList.clear();
		newList.addAll(set);
		return newList;
	}

	@Override
	public void updateEssentials(String hotelId, List<String> assentialsIds) {
		if (StringUtils.isEmpty(hotelId) || assentialsIds == null)
			return;
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			List<String> savedEssentials = getUpdatedList(hotel.getEssentials(), assentialsIds);
			hotel.setEssentials(savedEssentials);
			hotelMao.update(hotel);
		}
	}

	@Override
	public long getTimezoneDifferenceInMillis(String hotelId) {
		String timezone = hotelIdVsTimezone.get(hotelId);
		if (StringUtils.isEmpty(timezone)) {
			timezone = hotelMao.getTimezone(hotelId);
			if (!StringUtils.isEmpty(timezone)) {
				hotelIdVsTimezone.put(hotelId, timezone);
			}
		}
		return timezoneService.getTimeDifferenceInMillis(timezone);
	}

	@Override
	public void updateAwards(String hotelId, HotelAward awardsPojo) throws AboutstaysException {
		if (StringUtils.isEmpty(hotelId) || awardsPojo == null)
			return;
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			List<HotelAward> awards = hotel.getAwards();
			if (awards == null) {
				awards = new ArrayList<>();
			}
			for (HotelAward pojo : awards) {
				if (pojo.getAwardId().equals(awardsPojo.getAwardId()) && pojo.getYear().equals(awardsPojo.getYear())) {
					throw new AboutstaysException(AboutstaysResponseCode.AWARD_ALREADY_EXIST);
				}
			}
			awards.add(awardsPojo);
			hotel.setAwards(awards);
			hotelMao.update(hotel);
		}
	}

	@Override
	public void updateCardAccepted(String hotelId, List<String> cardIds) {
		if (StringUtils.isEmpty(hotelId) || cardIds == null)
			return;
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			List<String> savedCards = getUpdatedList(hotel.getEssentials(), cardIds);
			hotel.setCardsAccepted(savedCards);
			hotelMao.update(hotel);
		}
	}

	@Override
	public boolean deleteAwards(String hotelId, HotelAward awardsPojo) throws AboutstaysException {
		if (StringUtils.isEmpty(hotelId) || awardsPojo == null)
			return false;
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			List<HotelAward> awards = hotel.getAwards();
			if (CollectionUtils.isNotEmpty(awards)) {
				HotelAward awardSelected = null;
				for (HotelAward award : awards) {
					if (award.getAwardId().equals(awardsPojo.getAwardId())
							&& award.getYear().equals(awardsPojo.getYear())) {
						awardSelected = award;
						break;
					}
				}
				if (awardSelected != null) {
					awards.remove(awardSelected);
				} else {
					throw new AboutstaysException(AboutstaysResponseCode.AWARD_NOT_EXIST);
				}
			}
			hotelMao.update(hotel);
		}
		return true;
	}

	@Override
	public void updateLoyaltyPrograms(String hotelId, String loyaltyProgramsId) throws AboutstaysException {
		if (StringUtils.isEmpty(hotelId) || loyaltyProgramsId == null)
			return;
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if (hotel != null) {
			List<String> savedLoyaltyPrograms = hotel.getLoyaltyPrograms();
			if (savedLoyaltyPrograms == null) {
				savedLoyaltyPrograms = new ArrayList<>();
			}
			if (savedLoyaltyPrograms.contains(loyaltyProgramsId)) {
				throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_PROGRAM_ALREADY_EXIST);
			}
			savedLoyaltyPrograms.add(loyaltyProgramsId);
			hotel.setLoyaltyPrograms(savedLoyaltyPrograms);
			hotelMao.update(hotel);
		}
	}

	@Override
	public boolean deleteLoyaltyProgram(String hotelId, String loyaltyProgramsId) throws AboutstaysException {
		if (!StringUtils.isEmpty(hotelId)) {
			Hotel hotel = hotelMao.getHotelById(hotelId);
			List<String> loyaltyPrograms = hotel.getLoyaltyPrograms();
			if (loyaltyPrograms == null || !loyaltyPrograms.contains(loyaltyProgramsId)) {
				throw new AboutstaysException(AboutstaysResponseCode.LOYALTY_PROGRAM_NOT_FOUND);
			}
			loyaltyPrograms.remove(loyaltyProgramsId);
			hotelMao.update(hotel);
			return true;
		}
		return false;
	}

	@Override
	public boolean existsInHotel(String hotelId, String roomId) {
		if(StringUtils.isEmpty(hotelId) || StringUtils.isEmpty(roomId)){
			return false;
		}
		Hotel hotel = hotelMao.getHotelById(hotelId);
		if(hotel!=null && !CollectionUtils.isEmpty(hotel.getRoomsIds())){
			if(hotel.getRoomsIds().contains(roomId)){
				return true;
			}
		}
		return false;
	}

	@Override
	public HotelInfoMetaData getHotelInfoMetaData(String hotelId) {
		GetHotelResponse getHotelResponse = getHotelById(hotelId);
		if (getHotelResponse != null) {
			HotelInfoMetaData data = new HotelInfoMetaData();
			data.setTotalItems(8);
			int count = 0;
			if (isHotelInfoExist(getHotelResponse)) {
				count++;
			}
			if (isLocationAndContactsExist(getHotelResponse)) {
				count++;
			}
			if (isAmenitiesExist(getHotelResponse)) {
				count++;
			}
			if (isEssentilasExist(getHotelResponse)) {
				count++;
			}
			if (isAwardsExist(getHotelResponse)) {
				count++;
			}
			if (isLoyaltyProgramExist(getHotelResponse)) {
				count++;
			}
			if (isCardsAcceptedExist(getHotelResponse)) {
				count++;
			}
			if (isHouseRulesExist(hotelId)) {
				count++;
			}
			data.setItems(count);
			return data;
		}
		return null;
	}

	@Override
	public boolean isHotelInfoExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (getHotelResponse.getGeneralInformation() != null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isLocationAndContactsExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (getHotelResponse.getGeneralInformation() != null) {
				if (getHotelResponse.getGeneralInformation().getAddress() != null) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isAmenitiesExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (CollectionUtils.isNotEmpty(getHotelResponse.getAmenities())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEssentilasExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (CollectionUtils.isNotEmpty(getHotelResponse.getEssentials())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isAwardsExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (CollectionUtils.isNotEmpty(getHotelResponse.getListAwards())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isCardsAcceptedExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (CollectionUtils.isNotEmpty(getHotelResponse.getListCardAccepted())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isLoyaltyProgramExist(GetHotelResponse getHotelResponse) {
		if (getHotelResponse != null) {
			if (CollectionUtils.isNotEmpty(getHotelResponse.getListLoyaltyPrograms())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isHouseRulesExist(String hotelId) {
		if(!StringUtils.isEmpty(hotelId)){
			if(houseRulesService.getById(hotelId)!=null){
				return true;
			}
		}
		return false;
	}
}