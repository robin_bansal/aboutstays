package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.CityGuide;
import com.aboutstays.mao.CityGuideMao;
import com.aboutstays.request.dto.AddCityGuideRequest;
import com.aboutstays.request.dto.UpdateCityGuideRequest;
import com.aboutstays.response.dto.GetCityGuideResponse;
import com.aboutstays.services.CityGuideService;
import com.aboutstays.utils.CityGuideConverter;
import com.aboutstays.utils.CollectionUtils;

@Service
public class CityGuideServiceImpl implements CityGuideService{

	@Autowired
	private CityGuideMao cityGuideMao;
	
	@Override
	public String add(AddCityGuideRequest request) {
		CityGuide cityGuide = CityGuideConverter.convertBeanToEntity(request);
		if(cityGuide != null){
			cityGuideMao.add(cityGuide);
			return cityGuide.getHotelId();
		}
		return null;
	}

	@Override
	public List<GetCityGuideResponse> getAll() {
		List<CityGuide> listCityGuide = cityGuideMao.getAll();
		return CityGuideConverter.convertListEntityToListBean(listCityGuide);
	}

	@Override
	public GetCityGuideResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			CityGuide cityGuide = cityGuideMao.getById(id);
			return CityGuideConverter.convertEntityToBean(cityGuide);
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(cityGuideMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			cityGuideMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateCityGuideRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			CityGuide cityGuide = CityGuideConverter.convertUpdateBeanToEntity(request);
			if(CollectionUtils.isEmpty(cityGuide.getListCityGuideItems())){
				cityGuide.setListCityGuideItems(new ArrayList<>());
			}
			cityGuideMao.updateNonNullById(cityGuide, request.getId());
			return true;
		}		
		return false;
	}

	@Override
	public CityGuide getEntity(String cityGuideId) {
		return cityGuideMao.getById(cityGuideId);
	}

}
