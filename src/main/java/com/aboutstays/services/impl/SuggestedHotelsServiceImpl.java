package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.SuggestedHotels;
import com.aboutstays.mao.SuggestedHotelsMao;
import com.aboutstays.request.dto.AddSuggestedHotelRequest;
import com.aboutstays.response.dto.SuggestedHotelsResponse;
import com.aboutstays.services.SuggestedHotelsService;
import com.aboutstays.utils.SuggestedHotelsConverter;

@Service
public class SuggestedHotelsServiceImpl implements SuggestedHotelsService{

	@Autowired
	private SuggestedHotelsMao shMao;

	@Override
	public String add(AddSuggestedHotelRequest request) {
		SuggestedHotels response = SuggestedHotelsConverter.convertBeanToEntity(request);
		if(response != null) {
			response.setId(null);
			shMao.add(response);
			return response.getId();
		}
		return null;
	}

	@Override
	public List<SuggestedHotelsResponse> getAll() {
		List<SuggestedHotels> listRequest = shMao.getAll();
		return SuggestedHotelsConverter.convertListEntityToListBean(listRequest);
	}

	@Override
	public SuggestedHotelsResponse getById(String id) {
		SuggestedHotels hotels = shMao.getById(id);
		return SuggestedHotelsConverter.convertEntityToBean(hotels);
	}

	@Override
	public boolean existsById(String id) {
		if(shMao.getById(id) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)) {
			shMao.deletyById(id);
			return true;
		}
		return false;
	} 
	
	
}
