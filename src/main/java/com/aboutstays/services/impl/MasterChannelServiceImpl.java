package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MasterChannelItem;
import com.aboutstays.mao.MasterChannelMao;
import com.aboutstays.request.dto.AddMasterChannelRequest;
import com.aboutstays.request.dto.AddMultipleMasterChannelRequest;
import com.aboutstays.request.dto.UpdateMasterChannelRequest;
import com.aboutstays.response.dto.GetMasterChannelResponse;
import com.aboutstays.response.dto.GetMasterGenereResponse;
import com.aboutstays.services.MasterChannelService;
import com.aboutstays.services.MasterGenereService;
import com.aboutstays.utils.ChannelConverter;
import com.aboutstays.utils.CollectionUtils;

@Service
public class MasterChannelServiceImpl implements MasterChannelService {
	
	@Autowired
	private MasterChannelMao masterChannelMao;
	
	@Autowired
	private MasterGenereService masterGenereService; 

	@Override
	public String save(AddMasterChannelRequest request) {
		MasterChannelItem masterChannelItem = ChannelConverter.convertAddRequestToEntity(request);
		if(masterChannelItem!=null){
			masterChannelMao.save(masterChannelItem);
			return masterChannelItem.getId();
		}
		return null;
	}

	@Override
	public void saveAll(AddMultipleMasterChannelRequest multipleRequests) {
		List<MasterChannelItem> masterChannelItems = ChannelConverter.convertAddListToEntityList(multipleRequests);
		if(CollectionUtils.isNotEmpty(masterChannelItems)){
			masterChannelMao.saveAll(masterChannelItems);
		}
	}

	@Override
	public void update(UpdateMasterChannelRequest request) {
		MasterChannelItem masterChannelItem = ChannelConverter.convertUpdateRequestToEntity(request);
		if(masterChannelItem!=null){
			masterChannelMao.update(masterChannelItem);
		}
	}

	@Override
	public GetMasterChannelResponse getById(String id) {
		MasterChannelItem masterChannelItem = masterChannelMao.getById(id);
		if(masterChannelItem!=null){
			GetMasterGenereResponse genereDetails = masterGenereService.getById(masterChannelItem.getGenereId());
			return ChannelConverter.convertEntityToGetResponse(masterChannelItem, genereDetails);
		}
		return null;
	}

	@Override
	public List<GetMasterChannelResponse> getAll() {
		List<MasterChannelItem> masterChannelItemList = masterChannelMao.getAll();
		if(CollectionUtils.isNotEmpty(masterChannelItemList)){
			List<GetMasterChannelResponse> responseList = new ArrayList<>(masterChannelItemList.size());
			Map<String, GetMasterGenereResponse> genereIdVsDetails = new HashMap<>();
			for(MasterChannelItem masterChannelItem : masterChannelItemList){
				GetMasterGenereResponse genereDetails = genereIdVsDetails.get(masterChannelItem.getGenereId());
				if(genereDetails==null){
					genereDetails = masterGenereService.getById(masterChannelItem.getGenereId());
					genereIdVsDetails.put(masterChannelItem.getGenereId(), genereDetails);
				}
				responseList.add(ChannelConverter.convertEntityToGetResponse(masterChannelItem, genereDetails));
			}
			return responseList;
		}
		return null;
	}

	@Override
	public void delete(String id) {
		masterChannelMao.delete(id);
	}

	@Override
	public boolean existsByName(String name, String skipForChannelId) {
		List<MasterChannelItem> masterChannelItems = masterChannelMao.getAll();
		if(CollectionUtils.isNotEmpty(masterChannelItems)){
			for(MasterChannelItem masterChannelItem : masterChannelItems){
				String savedName = masterChannelItem.getName();
				if(!StringUtils.isEmpty(savedName)&&savedName.equalsIgnoreCase(name)){
					if(masterChannelItem.getId().equals(skipForChannelId)){
						continue;
					}
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean existsById(String channelId) {
		MasterChannelItem masterChannelItem = masterChannelMao.getById(channelId);
		if(masterChannelItem!=null){
			return true;
		}
		return false;
	}

	@Override
	public List<GetMasterChannelResponse> getAllByGenere(String genereId) {
		List<MasterChannelItem> masterChannelItemList = masterChannelMao.getAllByGenereId(genereId);
		if(CollectionUtils.isNotEmpty(masterChannelItemList)){
			List<GetMasterChannelResponse> responseList = new ArrayList<>(masterChannelItemList.size());
			GetMasterGenereResponse genereDetails = masterGenereService.getById(genereId);
			for(MasterChannelItem masterChannelItem : masterChannelItemList){
				responseList.add(ChannelConverter.convertEntityToGetResponse(masterChannelItem, genereDetails));
			}
			return responseList;
		}
		return null;
	}

}
