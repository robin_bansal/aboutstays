package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.GeneralService;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.mao.GeneralServiceMao;
import com.aboutstays.request.dto.AddGeneralServiceRequest;
import com.aboutstays.request.dto.UpdateGeneralServiceRequest;
import com.aboutstays.response.dto.AddGeneralServiceResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.GeneralServicesConverter;

@Service
public class GeneralServicesServiceImpl implements GeneralServicesService{

	@Autowired
	private GeneralServiceMao generalServiceMao;
	

	
	@Override
	public AddGeneralServiceResponse add(AddGeneralServiceRequest request) {
		if(request == null){
			return null;
		}
		GeneralService service = GeneralServicesConverter.convertBeanToEntity(request);
		if(service != null){
			generalServiceMao.add(service);
			return new AddGeneralServiceResponse(service.getId());
		}
		return null;
	}

	@Override
	public List<GetGeneralServiceResponse> getAll() {
		List<GeneralService> listGS = generalServiceMao.getAll();
		if(CollectionUtils.isNotEmpty(listGS)){
			List<GetGeneralServiceResponse> listResponse = GeneralServicesConverter.convertListEntityToListBean(listGS);
			return listResponse;
		}
		return null;
	}

	@Override
	public List<GetGeneralServiceResponse> getAllByStaysStatus(Integer status) {
		List<GeneralService> listGS = generalServiceMao.getAllByStaysStatus(status);
		if(CollectionUtils.isNotEmpty(listGS)){
			List<GetGeneralServiceResponse> listResponse = GeneralServicesConverter.convertListEntityToListBean(listGS);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetGeneralServiceResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			GeneralService service = generalServiceMao.getById(id);
			GetGeneralServiceResponse response = GeneralServicesConverter.convertEntityToBean(service);
			return response;
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(generalServiceMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			generalServiceMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateGeneralServiceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			GeneralService service = GeneralServicesConverter.convertUpdateBeanToEntity(request);
			generalServiceMao.updateNonNullById(service, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public GeneralService getByType(GeneralServiceType type) {
		if(type != null) {
			return generalServiceMao.getByType(type.getCode());
		}
		return null;
	}

}
