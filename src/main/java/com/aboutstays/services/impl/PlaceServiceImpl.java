package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.Place;
import com.aboutstays.enums.PlaceType;
import com.aboutstays.mao.PlaceMao;
import com.aboutstays.request.dto.PlaceRequest;
import com.aboutstays.response.dto.PlaceResponse;
import com.aboutstays.services.PlaceService;
import com.aboutstays.utils.PlaceConverter;

@Service
public class PlaceServiceImpl implements PlaceService{

	@Autowired
	private PlaceMao placeMao;
	
	@Override
	public String save(PlaceRequest request) {
		Place place = PlaceConverter.convertRequestToEntity(request, false);
		if(place!=null){
			placeMao.save(place);
			return place.getPlaceId();
		}
		return null;
	}

	@Override
	public void update(PlaceRequest request) {
		Place place = PlaceConverter.convertRequestToEntity(request, true);
		if(place!=null){
			Place savedPlace = placeMao.getById(place.getPlaceId());
			if(savedPlace!=null){
				place.setCreated(savedPlace.getCreated());
			}
			placeMao.update(place);
		}
	}

	@Override
	public void updateNonNull(PlaceRequest request) {
		Place place = PlaceConverter.convertRequestToEntity(request, true);
		if(place!=null){
			Place savedPlace = placeMao.getById(place.getPlaceId());
			if(savedPlace!=null){
				place.setCreated(savedPlace.getCreated());
			}
			placeMao.updateNonNull(place);
		}
	}

	@Override
	public List<PlaceResponse> getByParentPlaceId(String parentPlaceId) {
		List<Place> places = placeMao.getByParentId(parentPlaceId);
		return PlaceConverter.convertEntityListToResponseList(places);
	}

	@Override
	public List<PlaceResponse> getByPlaceType(PlaceType placeType) {
		if(placeType==null)
			return null;
		List<Place> places = placeMao.getByType(placeType.getType());
		return PlaceConverter.convertEntityListToResponseList(places);
	}

	@Override
	public PlaceResponse getById(String placeId) {
		Place place = placeMao.getById(placeId);
		return PlaceConverter.convertEntityToResponse(place);
	}

	@Override
	public List<PlaceResponse> getAll() {
		List<Place> places = placeMao.getAll();
		return PlaceConverter.convertEntityListToResponseList(places);
	}

	@Override
	public void delete(String placeId) {
		placeMao.delete(placeId);
	}

	@Override
	public boolean existsByPlaceId(String placeId) {
		Place place = placeMao.getById(placeId);
		if(place!=null){
			return true;
		}
		return false;
	}

	
}
