package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Maintenance;
import com.aboutstays.mao.MaintenanceMao;
import com.aboutstays.request.dto.AddMaintenanceRequest;
import com.aboutstays.request.dto.UpdateMaintenanceRequest;
import com.aboutstays.response.dto.GetMaintenaneResponse;
import com.aboutstays.services.MaintenanceService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.MaintenanceConverter;

@Service
public class MaintenanceServiceImpl implements MaintenanceService {

	@Autowired
	private MaintenanceMao maintenanceMao;
	
	@Override
	public String add(AddMaintenanceRequest request) {
		Maintenance maintenance = MaintenanceConverter.convertBeanToEntity(request);
		if(maintenance != null){
			maintenanceMao.add(maintenance);
			return maintenance.getHotelId();
		}
		return null;
	}

	@Override
	public List<GetMaintenaneResponse> getAll() {
		List<Maintenance> listMaintenance = maintenanceMao.getAll();
		return MaintenanceConverter.convertListEntityToListBean(listMaintenance);
	}

	@Override
	public GetMaintenaneResponse getById(String id) {
		Maintenance maintenance = maintenanceMao.getById(id);
		return MaintenanceConverter.converteEntityToBean(maintenance);
	}

	@Override
	public boolean existsById(String id) {
		if(maintenanceMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			maintenanceMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateMaintenanceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			Maintenance maintenance = MaintenanceConverter.convertUpdateBeanToEntity(request);
			if(CollectionUtils.isEmpty(maintenance.getListItemType())){
				maintenance.setListItemType(new ArrayList<>());
			}
			maintenanceMao.updateNonNullById(maintenance, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public Maintenance getEntity(String id) {
		return maintenanceMao.getById(id);
	}

}
