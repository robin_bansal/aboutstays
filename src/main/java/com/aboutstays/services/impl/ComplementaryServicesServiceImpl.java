package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.ComplementaryServicesEntity;
import com.aboutstays.mao.ComplementaryServicesMao;
import com.aboutstays.request.dto.AddComplementaryServicesRequest;
import com.aboutstays.request.dto.UpdateComplementaryServicesRequest;
import com.aboutstays.response.dto.GetComplementaryServicesResponse;
import com.aboutstays.services.ComplementaryServicesService;
import com.aboutstays.utils.ComplementaryServicesConverter;

@Service
public class ComplementaryServicesServiceImpl implements ComplementaryServicesService {
	
	@Autowired
	private ComplementaryServicesMao complementaryServicesMao; 

	@Override
	public String save(AddComplementaryServicesRequest request) {
		ComplementaryServicesEntity entity = ComplementaryServicesConverter.convertAddRequestToEntity(request);
		if(entity!=null){
			complementaryServicesMao.save(entity);
			return entity.getHotelId();
		}
		return null;
	}

	@Override
	public void update(UpdateComplementaryServicesRequest request) {
		ComplementaryServicesEntity entity = ComplementaryServicesConverter.convertUpdateRequestToEntity(request);
		if(entity!=null){
			complementaryServicesMao.update(entity);
		}
	}

	@Override
	public GetComplementaryServicesResponse getById(String id) {
		ComplementaryServicesEntity entity = complementaryServicesMao.getById(id);
		return ComplementaryServicesConverter.convertEntityToGetResponse(entity);
	}

	@Override
	public List<GetComplementaryServicesResponse> getAll() {
		List<ComplementaryServicesEntity> entities = complementaryServicesMao.getAll();
		return ComplementaryServicesConverter.convertEntityListToGetList(entities);
	}

	@Override
	public void delete(String id) {
		complementaryServicesMao.delete(id);
	}

	@Override
	public boolean existsById(String servicesId) {
		ComplementaryServicesEntity entity = complementaryServicesMao.getById(servicesId);
		if(entity!=null)
			return true;
		return false;
	}

}
