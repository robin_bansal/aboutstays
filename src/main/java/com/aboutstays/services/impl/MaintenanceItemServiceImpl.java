package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MaintenanceItem;
import com.aboutstays.enums.MaintenanceItemType;
import com.aboutstays.mao.MaintenanceItemMao;
import com.aboutstays.request.dto.UpdateMaintenanceItemRequest;
import com.aboutstays.response.dto.AddMaintenanceItemRequest;
import com.aboutstays.response.dto.MaintenanceItemResponse;
import com.aboutstays.services.MaintenanceItemService;
import com.aboutstays.utils.MaintenanceItemConverter;

@Service
public class MaintenanceItemServiceImpl implements MaintenanceItemService {

	@Autowired
	private MaintenanceItemMao mtItemMao;
	
	@Override
	public String save(AddMaintenanceItemRequest request) {
		MaintenanceItem item = MaintenanceItemConverter.convertBeanToEntity(request);
		if(item != null){
			item.setId(null);
			mtItemMao.save(item);
			return item.getId();
		}
		return null;
	}

	@Override
	public MaintenanceItemResponse getById(String id) {
		MaintenanceItem item = mtItemMao.getById(id);
		return MaintenanceItemConverter.convertEntityToBean(item);
	}

	@Override
	public List<MaintenanceItemResponse> getAll() {
		List<MaintenanceItem> listItem = mtItemMao.getAll();
		return MaintenanceItemConverter.convertListEntityToListBean(listItem);
	}

	@Override
	public void update(UpdateMaintenanceItemRequest item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean updateNonNull(UpdateMaintenanceItemRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			MaintenanceItem item = MaintenanceItemConverter.convertUpdateBeanToEntity(request);
			mtItemMao.updateNonNull(item, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public List<MaintenanceItemResponse> getAllByHotelId(String hotelId) {
		List<MaintenanceItem> listItem = mtItemMao.getAllByHotelId(hotelId);
		return MaintenanceItemConverter.convertListEntityToListBean(listItem);
	}

	@Override
	public List<MaintenanceItemResponse> getAllByMTServiceId(String serviceId) {
		List<MaintenanceItem> listItem = mtItemMao.getAllByMTServicesId(serviceId);
		return MaintenanceItemConverter.convertListEntityToListBean(listItem);
	}

	@Override
	public boolean existsById(String id) {
		if(mtItemMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public MaintenanceItem getEntityById(String id) {
		return mtItemMao.getById(id);
	}

	@Override
	public void updateEntity(MaintenanceItem item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MaintenanceItemResponse> fetchByType(String mtServiceId, MaintenanceItemType itemType) {
		if(StringUtils.isEmpty(mtServiceId) || itemType == null){
			return null;
		}
		List<MaintenanceItem> listItem = mtItemMao.fetchByType(mtServiceId, itemType.getCode());
		return MaintenanceItemConverter.convertListEntityToListBean(listItem);
	}

	@Override
	public boolean deleteById(String id) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		mtItemMao.delete(id);
		return true;
	}

}
