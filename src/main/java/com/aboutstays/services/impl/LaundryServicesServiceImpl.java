package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.LaundryServiceEntity;
import com.aboutstays.mao.LaundryServicesMao;
import com.aboutstays.request.dto.LaundryServiceRequest;
import com.aboutstays.response.dto.LaundryServiceResponse;
import com.aboutstays.services.LaundryServicesService;
import com.aboutstays.utils.LaundryServiceConverter;

@Service
public class LaundryServicesServiceImpl implements LaundryServicesService {
	
	@Autowired
	private LaundryServicesMao laundryServicesMao;

	@Override
	public String save(LaundryServiceRequest request) {
		LaundryServiceEntity entity = LaundryServiceConverter.convertAddRequestToEntity(request);
		if(entity!=null){
			laundryServicesMao.save(entity);
			return entity.getHotelId();
		}
		return null;
	}

	@Override
	public LaundryServiceResponse getById(String servicesId) {
		LaundryServiceEntity entity = laundryServicesMao.getById(servicesId);
		return LaundryServiceConverter.convertEntityToGetResponse(entity);
	}

	@Override
	public boolean existsById(String servicesId) {
		LaundryServiceEntity entity = laundryServicesMao.getById(servicesId);
		if(entity!=null)
			return true;
		return false;
	}

	@Override
	public List<LaundryServiceResponse> getAll() {
		List<LaundryServiceEntity> entitieList = laundryServicesMao.getAll();
		return LaundryServiceConverter.convertEntityListToGetList(entitieList);
	}

	@Override
	public void update(LaundryServiceRequest request) {
		LaundryServiceEntity entity = LaundryServiceConverter.convertUpdateRequestToEntity(request);
		if(entity!=null)
			laundryServicesMao.update(entity);
	}

	@Override
	public void updateNonNull(LaundryServiceRequest request) {
		LaundryServiceEntity entity = LaundryServiceConverter.convertUpdateRequestToEntity(request);
		if(entity!=null)
			laundryServicesMao.updateNonNull(entity);
	}

}
