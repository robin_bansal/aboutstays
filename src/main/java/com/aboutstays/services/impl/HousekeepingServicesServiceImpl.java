package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.HousekeepingServiceEntity;
import com.aboutstays.mao.HousekeepingServiceMao;
import com.aboutstays.request.dto.AddHousekeepingServiceRequest;
import com.aboutstays.response.dto.AddHousekeepingServiceResponse;
import com.aboutstays.response.dto.GetHousekeepingServiceResponse;
import com.aboutstays.response.dto.UpdateHousekeepingServiceRequest;
import com.aboutstays.services.HousekeepingServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.HousekeepingServiceConverter;

@Service
public class HousekeepingServicesServiceImpl implements HousekeepingServicesService {

	@Autowired
	private HousekeepingServiceMao hkServiceMao;
	
	@Override
	public AddHousekeepingServiceResponse add(AddHousekeepingServiceRequest request) {
		HousekeepingServiceEntity services = HousekeepingServiceConverter.convertBeanToEntity(request);
		if(services != null){
			hkServiceMao.add(services);
			return new AddHousekeepingServiceResponse(services.getHotelId());
		}
		return null;
	}

	@Override
	public List<GetHousekeepingServiceResponse> getAll() {
		List<HousekeepingServiceEntity> listServices = hkServiceMao.getAll();
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetHousekeepingServiceResponse> listResponse = HousekeepingServiceConverter.convertListEntityToListBean(listServices);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetHousekeepingServiceResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			HousekeepingServiceEntity service = hkServiceMao.getById(id);
			if(service != null){
				GetHousekeepingServiceResponse response = HousekeepingServiceConverter.convertEntityToBean(service);
				return response;
			}
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(hkServiceMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			hkServiceMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateHousekeepingServiceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			HousekeepingServiceEntity service = HousekeepingServiceConverter.convertUpdateBeanToEntity(request);
			if(service != null){
				if(CollectionUtils.isEmpty(request.getListHKItems()))
					service.setListHKItems(new ArrayList<>());
				hkServiceMao.update(service);
				return true;				
			}
		}		
		return false;
	}

	@Override
	public HousekeepingServiceEntity getEntity(String hkId) {
		return hkServiceMao.getById(hkId);
	}

}
