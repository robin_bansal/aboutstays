package com.aboutstays.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.TimeZoneDetails;
import com.aboutstays.mao.TimeZoneDetailsMao;
import com.aboutstays.services.TimezoneService;

@Service
public class TimezoneServiceImpl implements TimezoneService {

	@Autowired
	private TimeZoneDetailsMao timeZoneDetailsMao; 
	
	private Map<String, Long> timezoneVsTimezoneDifference = new ConcurrentHashMap<>();
	
	@Override
	public String saveTimezone(TimeZoneDetails timeZoneDetails) {
		if(timeZoneDetails!=null){
			timeZoneDetails.setId(null);
			long currentTime = new Date().getTime();
			timeZoneDetails.setCreated(currentTime);
			timeZoneDetails.setUpdated(currentTime);
			timeZoneDetailsMao.saveT(timeZoneDetails);
			return timeZoneDetails.getId();
		}
		return null;
	}

	@Override
	public TimeZoneDetails getTimezoneById(String id) {
		return timeZoneDetailsMao.getTById(id);
	}

	@Override
	public TimeZoneDetails getTimezoneDetailsByTimezone(String timezone) {
		return timeZoneDetailsMao.getByTimezone(timezone);
	}

	@Override
	public List<TimeZoneDetails> getAll() {
		return timeZoneDetailsMao.getAllT();
	}

	@Override
	public long getTimeDifferenceInMillis(String timezone) {
		if(StringUtils.isEmpty(timezone)){
			return 0;
		}
		Long value = timezoneVsTimezoneDifference.get(timezone);
		if(value==null){
			TimeZoneDetails timeZoneDetails = timeZoneDetailsMao.getByTimezone(timezone);
			if(timeZoneDetails!=null){
				value = (long) (timeZoneDetails.getHoursDifference()*60*60*1000);
				timezoneVsTimezoneDifference.put(timezone, value);
				return value;
			} else {
				value = 0l;
			}
		}
		return value;
	}

	@Override
	public boolean existsByTimezone(String timezone) {
		TimeZoneDetails timeZoneDetails = timeZoneDetailsMao.getByTimezone(timezone);
		if(timeZoneDetails!=null)
			return true;
		return false;
	}

	@Override
	public boolean existsById(String id) {
		TimeZoneDetails timeZoneDetails = timeZoneDetailsMao.getTById(id);
		if(timeZoneDetails!=null)
			return true;
		return false;
	}

	@Override
	public void deleteTimezone(String id) {
		timeZoneDetailsMao.deleteT(id);
	}

}
