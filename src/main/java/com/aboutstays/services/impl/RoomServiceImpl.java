package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MasterAmenity;
import com.aboutstays.entities.Room;
import com.aboutstays.mao.MasterAmenityMao;
import com.aboutstays.mao.RoomMao;
import com.aboutstays.pojos.Amenity;
import com.aboutstays.pojos.RoomMetaInfo;
import com.aboutstays.request.dto.AddRoomRequest;
import com.aboutstays.request.dto.UpdateRoomRequest;
import com.aboutstays.response.dto.GetRoomResponse;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.services.RoomService;
import com.aboutstays.utils.AmenityConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.RoomConverter;

@Service
public class RoomServiceImpl implements RoomService {
	
	@Autowired
	private RoomMao roomMao;
	@Autowired
	private MasterAmenityMao masterAmenityMao;
	
	@Autowired
	private RoomCategoryService roomCategoryService;

	@Override
	public String add(AddRoomRequest request) {
		Room room = RoomConverter.getEntityFromAddRequest(request);
		if (room != null) {
			roomMao.add(room);
			return room.getId();
		}
		return null;
	}

	@Override
	public List<GetRoomResponse> getAll() {
		List<Room> all = roomMao.getAll();
		return convertEntityListToResponseList(all);
	}

	private List<GetRoomResponse> convertEntityListToResponseList(List<Room> all) {
		if (all == null) {
			return null;
		}
		List<GetRoomResponse> getRoomResponses = new ArrayList<>();
		for (Room room : all) {
			GetRoomResponse getRoomResponse = convertEntityToRoomResponse(room);
			if(getRoomResponse!=null)
				getRoomResponses.add(getRoomResponse);
		}
		Collections.sort(getRoomResponses, new Comparator<GetRoomResponse>() {
			@Override
			public int compare(GetRoomResponse o1, GetRoomResponse o2) {
				if(o1.getPanelUiOrder()==null&&o2.getPanelUiOrder()==null)
					return 0;
				if(o1.getPanelUiOrder()==null){
					return o2.getPanelUiOrder();
				}
				if(o2.getPanelUiOrder()==null)
					return o1.getPanelUiOrder();
				return o1.getPanelUiOrder().compareTo(o2.getPanelUiOrder());
			}
		});
		return getRoomResponses;
	}

	@Override
	public GetRoomResponse getById(String id) {
		if (!StringUtils.isEmpty(id)) {
			Room room = roomMao.getById(id);
			return convertEntityToRoomResponse(room);
		}
		return null;
	}

	private GetRoomResponse convertEntityToRoomResponse(Room room) {
		if (room == null) {
			return null;
		}
		GetRoomResponse getRoomResponse = RoomConverter.getResponseFromEntity(room);
		if (getRoomResponse != null) {
			List<String> additionalAmenityIds = room.getAdditionalAmenityIds();
			if (additionalAmenityIds != null) {
				List<Amenity> amenities = new ArrayList<>();
				getRoomResponse.setAdditionalAmenities(amenities);
				for (String amenityId : additionalAmenityIds) {
					MasterAmenity masterAmenity = masterAmenityMao.getById(amenityId);
					Amenity amenity = AmenityConverter.convertMasterEntityToAmenity(masterAmenity);
					amenities.add(amenity);
				}
			}
			getRoomResponse.setRoomCategoryData(roomCategoryService.getById(room.getRoomCategoryId()));
		}
		return getRoomResponse;
	}

	@Override
	public boolean existsById(String id) {
		if (!StringUtils.isEmpty(id)) {
			if (roomMao.getById(id) != null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if (!StringUtils.isEmpty(id)) {
			roomMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateRoomRequest request) {
		if (request != null) {
			Room room = RoomConverter.getEntityFromUpdateRequest(request);
			if (room != null) {
				roomMao.updateNonNullById(room, request.getRoomId());
				return true;
			}
		}
		return false;
	}

	private List<String> getUpdatedListFromOld(List<String> oldList, List<String> newList) {
		Set<String> set = new HashSet<>(oldList);
		set.addAll(newList);
		oldList.clear();
		oldList.addAll(set);
		return oldList;
	}

	@Override
	public boolean addAdditionalAmenities(String roomId, List<String> amenityIds) {
		Room room = roomMao.getById(roomId);
		room.setAdditionalAmenityIds(amenityIds);
		roomMao.update(room);
		return true;
//		if (!StringUtils.isEmpty(roomId) && CollectionUtils.isNotEmpty(amenityIds)) {
//			Room room = roomMao.getById(roomId);
//			if (room != null && CollectionUtils.isNotEmpty(room.getAdditionalAmenityIds())) {
//				List<String> updatedListFromOld = getUpdatedListFromOld(room.getAdditionalAmenityIds(), amenityIds);
//				room.setAdditionalAmenityIds(updatedListFromOld);
//				room.setAdditionalAmenityIds(amenityIds);
//				roomMao.update(room);
//				return true;
//			}
//		}
//		return false;
	}

	@Override
	public List<GetRoomResponse> getRoomsByRoomCategory(String categoryId) {
		if(!StringUtils.isEmpty(categoryId)){
			List<Room> roomsList = roomMao.getByRoomCategory(categoryId);
			if(roomsList!=null){
				return convertEntityListToResponseList(roomsList);
			}
		}
		return null;
	}

	@Override
	public List<GetRoomResponse> getRoomsByFloorId(String floorId) {
		if(!StringUtils.isEmpty(floorId)){
			List<Room> roomsList = roomMao.getByFloorId(floorId);
			if(roomsList!=null){
				return convertEntityListToResponseList(roomsList);
			}
		}
		return null;
	}

	@Override
	public List<GetRoomResponse> getRoomsByWingId(String wingId) {
		if(!StringUtils.isEmpty(wingId)){
			List<Room> roomsList = roomMao.getByWingId(wingId);
			if(roomsList!=null){
				return convertEntityListToResponseList(roomsList);
			}
		}
		return null;
	}

	@Override
	public List<GetRoomResponse> getRoomsByHotelId(String hotelsId) {
		if(!StringUtils.isEmpty(hotelsId)){
			List<Room> roomsList = roomMao.getByHotelId(hotelsId);
			if(roomsList!=null){
				return convertEntityListToResponseList(roomsList);
			}
		}
		return null;
	}

	@Override
	public void deleteRooms(String hotelId, List<String> roomIds) {
		roomMao.deleteRooms(hotelId, roomIds);
	}

	@Override
	public boolean exists(String hotelId, String roomId) {
		Room room = roomMao.getById(hotelId, roomId);
		if(room!=null)
			return true;
		return false;
	}

	@Override
	public void updateRoomMetaInfo(RoomMetaInfo roomInfo) {
		roomMao.updateRoomFields(roomInfo.getRoomId(), roomInfo.getRoomCategoryId(), roomInfo.getRoomNumber(), roomInfo.getPanelUiOrder());
	}

}
