package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.EntertaimentServiceEntity;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.EntertaimentServiceMao;
import com.aboutstays.pojos.EntertaimentItemTypeInfo;
import com.aboutstays.pojos.EntertaimentItemTypeInfoPojo;
import com.aboutstays.request.dto.AddEntertaimentServiceRequest;
import com.aboutstays.response.dto.GetEntertaimentServiceResponse;
import com.aboutstays.response.dto.GetMasterGenereResponse;
import com.aboutstays.services.EntertaimentServicesService;
import com.aboutstays.services.MasterGenereService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.EntertaimentServiceConverter;

@Service
public class EntertaimentServicesServiceImpl implements EntertaimentServicesService {
	
	@Autowired
	private EntertaimentServiceMao entertaimentServiceMao; 
	
	@Autowired
	private MasterGenereService masterGenereService;

	@Override
	public String save(AddEntertaimentServiceRequest request) {
		EntertaimentServiceEntity entertaimentServiceEntity = EntertaimentServiceConverter.convertAddRequestToEntity(request);
		if(entertaimentServiceEntity!=null){
			entertaimentServiceMao.save(entertaimentServiceEntity);
			return entertaimentServiceEntity.getHotelId();
		}
		return null;
	}

	@Override
	public void addType(String id, EntertaimentItemTypeInfo newTypeInfo) throws AboutstaysException {
		if(newTypeInfo==null||StringUtils.isEmpty(newTypeInfo.getTypeId()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_TYPE_INFO_REQUEST);
		EntertaimentServiceEntity entertaimentServiceEntity = entertaimentServiceMao.getById(id);
		if(entertaimentServiceEntity==null){
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_SERVICE_NOT_FOUND);
		}
		if(!masterGenereService.existsById(newTypeInfo.getTypeId())){
			throw new AboutstaysException(AboutstaysResponseCode.formatMessage(AboutstaysResponseCode.GENERE_NOT_FOUND, newTypeInfo.getTypeId()));
		}
		List<EntertaimentItemTypeInfo> saveTypeList = entertaimentServiceEntity.getTypeInfoList();
		if(saveTypeList==null){
			saveTypeList = new ArrayList<>();
		}
		Iterator<EntertaimentItemTypeInfo> iterator = saveTypeList.iterator();
		while(iterator.hasNext()){
			EntertaimentItemTypeInfo entertaimentItemTypeInfo = iterator.next();
			if(entertaimentItemTypeInfo==null||StringUtils.isEmpty(entertaimentItemTypeInfo.getTypeId())){
				iterator.remove();
				continue;
			}
			if(entertaimentItemTypeInfo.getTypeId().equals(newTypeInfo.getTypeId())){
				entertaimentServiceEntity.setTypeInfoList(saveTypeList);
				entertaimentServiceEntity.setUpdated(new Date().getTime());
				entertaimentServiceMao.update(entertaimentServiceEntity);
				throw new AboutstaysException(AboutstaysResponseCode.DUP_ENTERTAIMENT_TYPE);
			}
		}
		saveTypeList.add(newTypeInfo);
		entertaimentServiceEntity.setTypeInfoList(saveTypeList);
		entertaimentServiceEntity.setUpdated(new Date().getTime());
		entertaimentServiceMao.update(entertaimentServiceEntity);
	}

	@Override
	public void removeType(String id, EntertaimentItemTypeInfo newTypeInfo) throws AboutstaysException {
		if(newTypeInfo==null||StringUtils.isEmpty(newTypeInfo.getTypeId()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REMOVE_TYPE_INFO_REQUEST);
		EntertaimentServiceEntity entertaimentServiceEntity = entertaimentServiceMao.getById(id);
		if(entertaimentServiceEntity==null){
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_SERVICE_NOT_FOUND);
		}
		List<EntertaimentItemTypeInfo> savedTypeList = entertaimentServiceEntity.getTypeInfoList();
		if(CollectionUtils.isEmpty(savedTypeList)){
			throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_TYPE_NOT_FOUND);
		}
		Iterator<EntertaimentItemTypeInfo> iterator = savedTypeList.iterator();
		while(iterator.hasNext()){
			EntertaimentItemTypeInfo entertaimentItemTypeInfo = iterator.next();
			if(entertaimentItemTypeInfo==null||StringUtils.isEmpty(entertaimentItemTypeInfo.getTypeId())){
				iterator.remove();
				continue;
			}
			if(entertaimentItemTypeInfo.getTypeId().equals(newTypeInfo.getTypeId())){
				iterator.remove();
				entertaimentServiceEntity.setTypeInfoList(savedTypeList);
				entertaimentServiceEntity.setUpdated(new Date().getTime());
				entertaimentServiceMao.update(entertaimentServiceEntity);
				return;
			}
		}
		throw new AboutstaysException(AboutstaysResponseCode.ENTERTAIMENT_TYPE_NOT_FOUND);
		
	}

	@Override
	public List<GetEntertaimentServiceResponse> getAll() {
		List<EntertaimentServiceEntity> entityList = entertaimentServiceMao.getAll();
		return parseEntityListToGetResponseList(entityList);
	}

	private List<GetEntertaimentServiceResponse> parseEntityListToGetResponseList(
			List<EntertaimentServiceEntity> entityList) {
		if(CollectionUtils.isEmpty(entityList)){
			return null;
		}
		List<GetEntertaimentServiceResponse> responseList = new ArrayList<>(entityList.size());
		for(EntertaimentServiceEntity entity : entityList){
			responseList.add(parseEntityToGetResponse(entity));
		}
		return responseList;
	}

	@Override
	public GetEntertaimentServiceResponse getById(String id) {
		EntertaimentServiceEntity entity = entertaimentServiceMao.getById(id);
		return parseEntityToGetResponse(entity);
	}

	private GetEntertaimentServiceResponse parseEntityToGetResponse(EntertaimentServiceEntity entity) {
		if(entity==null)
			return null;
		GetEntertaimentServiceResponse response = new GetEntertaimentServiceResponse();
		response.setServiceId(entity.getHotelId());
		if(CollectionUtils.isNotEmpty(entity.getTypeInfoList())){
			List<EntertaimentItemTypeInfoPojo> typeInfoList = new ArrayList<>();
			for(EntertaimentItemTypeInfo typeInfo : entity.getTypeInfoList()){
				if(typeInfo==null||StringUtils.isEmpty(typeInfo.getTypeId())){
					continue;
				}
				GetMasterGenereResponse genereDetails = masterGenereService.getById(typeInfo.getTypeId());
				if(genereDetails==null){
					continue;
				}
				EntertaimentItemTypeInfoPojo typeInfoPojo = new EntertaimentItemTypeInfoPojo();
				typeInfoPojo.setGenereId(typeInfo.getTypeId());
				typeInfoPojo.setImageUrl(genereDetails.getLogo());
				typeInfoPojo.setTypeName(genereDetails.getName());
				typeInfoList.add(typeInfoPojo);
			}
			response.setTypeInfoList(typeInfoList);
		}
		return response;
	}

	@Override
	public void delete(String id) {
		entertaimentServiceMao.delete(id);
	}

	@Override
	public boolean existsById(String id) {
		EntertaimentServiceEntity entertaimentServiceEntity = entertaimentServiceMao.getById(id);
		if(entertaimentServiceEntity!=null){
			return true;
		}
		return false;
	}

}
