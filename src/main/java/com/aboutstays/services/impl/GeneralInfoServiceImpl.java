package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.GeneralInformation;
import com.aboutstays.mao.GeneralInfoMao;
import com.aboutstays.request.dto.AddGeneralInfoRequest;
import com.aboutstays.request.dto.UpdateGeneralInfoRequest;
import com.aboutstays.response.dto.GetGeneralInfoResponse;
import com.aboutstays.services.GeneralInfoService;
import com.aboutstays.utils.GeneralInfoConverter;

@Service
public class GeneralInfoServiceImpl implements GeneralInfoService{

	@Autowired
	private GeneralInfoMao generalInfoMao;

	@Override
	public String add(AddGeneralInfoRequest request) {
		GeneralInformation information = GeneralInfoConverter.convertBeanToEntity(request);
		if(information != null){
			information.setId(null);
			generalInfoMao.add(information);
			return information.getId();
		}
		return null;
	}

	@Override
	public List<GetGeneralInfoResponse> getAll() {
		List<GeneralInformation> listGInfo = generalInfoMao.getAll();
		List<GetGeneralInfoResponse> listResponse = GeneralInfoConverter.convertListEntityToListBean(listGInfo);
		return listResponse;
	}

	@Override
	public List<GetGeneralInfoResponse> getAllByStaysStatus(Integer status) {
		List<GeneralInformation> listGInfo = generalInfoMao.getAllByStaysStatus(status);
		List<GetGeneralInfoResponse> listResponse = GeneralInfoConverter.convertListEntityToListBean(listGInfo);
		return listResponse;
	}

	@Override
	public GetGeneralInfoResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			GeneralInformation info = generalInfoMao.getById(id);
			GetGeneralInfoResponse response = GeneralInfoConverter.convertEntityToBean(info);
			return response;
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(generalInfoMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			generalInfoMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateGeneralInfoRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			GeneralInformation info = GeneralInfoConverter.convertUpdateBeanToEntity(request);
			generalInfoMao.updateNonNullById(info, request.getId());
			return true;
		}
		return false;
	}
}
