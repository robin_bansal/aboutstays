package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.HKOrderMao;
import com.aboutstays.request.dto.AddHKOrderRequest;
import com.aboutstays.request.dto.UpdateHKOrderRequest;
import com.aboutstays.response.dto.GetHKOrderResponse;
import com.aboutstays.services.HKOrderService;
import com.aboutstays.utils.HKOrderConverter;

@Service
public class HKOrderServiceImpl implements HKOrderService{

	@Autowired
	private HKOrderMao hkOrderMao;
	
	@Override
	public String save(AddHKOrderRequest request) {
		HousekeepingOrder order = HKOrderConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			hkOrderMao.save(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public GetHKOrderResponse getById(String id) {
		HousekeepingOrder order = hkOrderMao.getById(id);
		return HKOrderConverter.convertEntityToBean(order);
	}

	@Override
	public List<GetHKOrderResponse> getAll() {
		List<HousekeepingOrder> listOrders = hkOrderMao.getAll();
		return HKOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public List<GetHKOrderResponse> getAllByUserId(String userId) {
		List<HousekeepingOrder> listOrders = hkOrderMao.getAllByUserId(userId);
		return HKOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public List<GetHKOrderResponse> getAllByHotelId(String hotelId) {
		List<HousekeepingOrder> listOrders = hkOrderMao.getAllByHotelId(hotelId);
		return HKOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public List<GetHKOrderResponse> getAllByUserAndStayId(String userId, String stayId) {
		List<HousekeepingOrder> listOrders = hkOrderMao.getAllByUserAndStayId(userId, stayId);
		return HKOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public void updateRequest(UpdateHKOrderRequest request) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean updateNonNullFieldsOfRequest(UpdateHKOrderRequest request) {
		HousekeepingOrder order = HKOrderConverter.convertUpdateRequestToEntity(request);
		if (order != null) {
			hkOrderMao.updateNonNull(order);
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			hkOrderMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(hkOrderMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public HousekeepingOrder getEntity(String id) {
		return hkOrderMao.getById(id);
	}
	
	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(StringUtils.isEmpty(id)||status==null){
			return false;
		}
		hkOrderMao.changeOrderStatus(id, status.getCode());
		return true;
	}

	@Override
	public boolean updateHKOrderRequest(HousekeepingOrder hkOrder) {
		if(hkOrder == null){
			return false;
		}
		hkOrderMao.updateNonNull(hkOrder);
		return true;
	}

}
