package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.PickList;
import com.aboutstays.enums.PickListType;
import com.aboutstays.mao.PickListMao;
import com.aboutstays.request.dto.AddPickListRequest;
import com.aboutstays.response.dto.GetPickListResponse;
import com.aboutstays.services.PickListService;
import com.aboutstays.utils.PickListConverter;

@Service
public class PickListServiceImpl implements PickListService {

	@Autowired
	private PickListMao pickListMao;
	
	@Override
	public void addOrModify(AddPickListRequest request) {
		PickList pickList = PickListConverter.convertAddRequestToEntity(request);
		if(pickList!=null){
			pickListMao.addOrModify(pickList);
		}
	}

	@Override
	public void delete(int type) {
		pickListMao.deleteByType(type);
	}

	@Override
	public List<GetPickListResponse> getAll() {
		List<PickList> pickLists = pickListMao.getAllT();
		return PickListConverter.convertEntityListToGetList(pickLists);
	}

	@Override
	public GetPickListResponse getByType(PickListType pickListType) {
		if(pickListType==null)
			return null;
		PickList pickList = pickListMao.getByType(pickListType.getCode());
		return PickListConverter.convertEntityToGetResponse(pickList);
	}

}
