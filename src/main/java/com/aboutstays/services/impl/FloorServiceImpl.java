package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Floor;
import com.aboutstays.mao.FloorMao;
import com.aboutstays.mao.RoomMao;
import com.aboutstays.request.dto.AddFloorRequest;
import com.aboutstays.request.dto.UpdateFloorRequest;
import com.aboutstays.response.dto.AddFloorResponse;
import com.aboutstays.response.dto.GetFloorResponse;
import com.aboutstays.services.FloorService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.FloorConverter;

@Service
public class FloorServiceImpl implements FloorService {
	
	@Autowired
	private FloorMao floorMao;
	
	@Autowired
	private RoomMao roomMao;

	@Override
	public AddFloorResponse add(AddFloorRequest request) {
		if (request != null) {
			Floor floor = FloorConverter.convertRequestToEntity(request);
			if (floor != null) {
				floorMao.add(floor);
				return new AddFloorResponse(floor.getId());
			}
		}
		return null;
	}

	@Override
	public List<GetFloorResponse> getAll() {
		List<Floor> floors = floorMao.getAll();
		return FloorConverter.getResponseListFromEntityList(floors);
	}

	@Override
	public GetFloorResponse getById(String id) {
		Floor floor = floorMao.getById(id);
		return parseEntityToGetResponse(floor);
	}

	@Override
	public boolean existsById(String id) {
		if (!StringUtils.isEmpty(id) && floorMao.getById(id) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			floorMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateFloorRequest request) {
		if(request!=null&&!StringUtils.isEmpty(request.getFloorId())){
			Floor floor = FloorConverter.convertRequestToEntity(request);
			if(floor!=null){
				floorMao.updateById(floor, request.getFloorId());
				return true;
			}
		}
		return false;
	}

	@Override
	public List<GetFloorResponse> getFloorsByWingId(String wingId) {
		List<Floor> floors = floorMao.getByWingId(wingId);
		return parseEntityListToGetList(floors);
	}

	@Override
	public List<GetFloorResponse> getFloorsByHotelId(String hotelId) {
		List<Floor> floors = floorMao.getByHotelId(hotelId);
		return parseEntityListToGetList(floors);
	}
	
	private List<GetFloorResponse> parseEntityListToGetList(List<Floor> floors){
		if(CollectionUtils.isEmpty(floors))
			return null;
		List<GetFloorResponse> response = new ArrayList<>(floors.size());
		for(Floor floor : floors){
			response.add(parseEntityToGetResponse(floor));
		}
		return response;
	}

	private GetFloorResponse parseEntityToGetResponse(Floor floor) {
		GetFloorResponse response = FloorConverter.convertEntiityToResponse(floor);
		if(response!=null){
			int roomsCount = roomMao.getCountByFloorId(floor.getId());
			response.setRoomsCount(roomsCount);
		}
		return response;
	}

	@Override
	public boolean exists(String floorId, String wingId) {
		Floor floor = floorMao.getByFloorAndWingId(floorId,wingId);
		if(floor!=null)
			return true;
		return false;
	}

	@Override
	public boolean exists(String floorId, String wingId, String hotelId) {
		Floor floor = floorMao.getByFloorWingAndHotelId(floorId,wingId, hotelId);
		if(floor!=null)
			return true;
		return false;
	}

}
