package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.LoyaltyPrograms;
import com.aboutstays.mao.LoyaltyProgramsMao;
import com.aboutstays.request.dto.AddLoyaltyRequest;
import com.aboutstays.response.dto.AddLoyaltyProgramResponse;
import com.aboutstays.response.dto.GetLoyaltyProgramResponse;
import com.aboutstays.response.dto.UpdateLoyaltyProgram;
import com.aboutstays.services.LoyaltyProgramsService;
import com.aboutstays.utils.LoyaltyProgramsConverter;
@Service
public class LoyaltyProgramsServiceImpl implements LoyaltyProgramsService{

	@Autowired
	LoyaltyProgramsMao loyaltyProgramMao;
	
	@Override
	public AddLoyaltyProgramResponse add(AddLoyaltyRequest request) {
		if(request==null){
			return null;
		}
		LoyaltyPrograms loyalyPrograms=LoyaltyProgramsConverter.conterBeanToEntity(request);
		loyaltyProgramMao.add(loyalyPrograms);
		AddLoyaltyProgramResponse addLoyaltyProgramResponse=new AddLoyaltyProgramResponse();
		addLoyaltyProgramResponse.setId(loyalyPrograms.getId());
		return addLoyaltyProgramResponse;
	}

	@Override
	public List<GetLoyaltyProgramResponse> getAll() {
		return LoyaltyProgramsConverter.conterEntityListToBeanList(loyaltyProgramMao.getAll());
	}

	@Override
	public GetLoyaltyProgramResponse getById(String id) {
		if(StringUtils.isEmpty(id)){
			return null;
		}
		return LoyaltyProgramsConverter.convertEntityToBean(loyaltyProgramMao.getById(id));
	}

	@Override
	public boolean existsById(String id) {
		if(getById(id)!=null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(existsById(id)){
			loyaltyProgramMao.deletyById(id);			
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateLoyaltyProgram request) {
		if(request==null ){
			return false;
		}
		if(StringUtils.isEmpty(request.getId())){
			return false;
		}
		LoyaltyPrograms loyalyPrograms = LoyaltyProgramsConverter.conterBeanToEntity(request);
		loyaltyProgramMao.update(loyalyPrograms);
		return true;
	}

}
