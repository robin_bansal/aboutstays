package com.aboutstays.services.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.controller.RequestDashboardController;
import com.aboutstays.entities.AirportPickup;
import com.aboutstays.entities.GeneralService;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.AirportPickupMao;
import com.aboutstays.mao.UserOptedServicesMao;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.CreateDashboardRequest;
import com.aboutstays.request.dto.GetUOSByUserRequest;
import com.aboutstays.request.dto.UpdateUserOptedServiceRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.BaseApiResponse;
import com.aboutstays.response.dto.CreateDashboardResponse;
import com.aboutstays.response.dto.GetCommuteOrderResponse;
import com.aboutstays.response.dto.GetFoodOrderResponse;
import com.aboutstays.response.dto.GetHKOrderResponse;
import com.aboutstays.response.dto.GetLaundryOrderResponse;
import com.aboutstays.response.dto.GetReservationOrderResponse;
import com.aboutstays.response.dto.GetUserOptedServiceResponse;
import com.aboutstays.response.dto.InternetOrderResponse;
import com.aboutstays.services.CommuteOrderService;
import com.aboutstays.services.FoodOrdersService;
import com.aboutstays.services.GeneralServicesService;
import com.aboutstays.services.HKOrderService;
import com.aboutstays.services.InternetOrderService;
import com.aboutstays.services.LaundryOrderService;
import com.aboutstays.services.ReservationOrderService;
import com.aboutstays.services.UserOptedServicesService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.DateUtil;
import com.aboutstays.utils.UserOptedServiceConverter;

@Service
public class UserOptedServicesServiceImpl implements UserOptedServicesService{

	@Autowired
	private UserOptedServicesMao userOptedServicesMao;
	
	@Autowired
	private AirportPickupMao apMao;
	
	@Autowired
	private FoodOrdersService foodOrdersService;
	
	@Autowired
	private HKOrderService hkOrderService;
	
	@Autowired
	private LaundryOrderService laundryOrderService;
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;
	
	@Autowired
	private CommuteOrderService coService;
	
	@Autowired
	private InternetOrderService ioService;
	
	@Autowired
	private ReservationOrderService roService;
	
	@Autowired
	private GeneralServicesService generalServicesService;
	
	@Override
	public AddUserOptedServiceResposne add(AddUserOptedServiceRequest request) {
		if(request != null){
			UserOptedService optedService = UserOptedServiceConverter.convertBeanToEntity(request);
			userOptedServicesMao.add(optedService);
			return new AddUserOptedServiceResposne(optedService.getId());
		}
		return null;
	}

	@Override
	public List<GetUserOptedServiceResponse> getAll(String userId) {
		List<UserOptedService> listOptedServices = null;
		if(StringUtils.isEmpty(userId)){
			listOptedServices = userOptedServicesMao.getAll();
		} else{
			listOptedServices = userOptedServicesMao.getAllByUserId(userId);
		}
		if(CollectionUtils.isNotEmpty(listOptedServices)){
			List<GetUserOptedServiceResponse> listResponse = UserOptedServiceConverter.convertListEntityToListBean(listOptedServices);
			return listResponse;
		}
		return null;
	}

	@Override
	public GetUserOptedServiceResponse getById(String id) {
		if(!StringUtils.isEmpty(id)){
			UserOptedService optedService = userOptedServicesMao.getById(id);
			if(optedService != null){
				GetUserOptedServiceResponse response = UserOptedServiceConverter.convertEntityToBean(optedService);
				return response;
			}
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			userOptedServicesMao.deletyById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateById(UpdateUserOptedServiceRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			UserOptedService optedService = UserOptedServiceConverter.convertUpdateRequestToEntity(request);
			userOptedServicesMao.updateNonNullById(optedService, request.getId());
			return true;
		}
		return false;
	}

	@Override
	public List<GetUserOptedServiceResponse> getByUserAndStay(GetUOSByUserRequest request) {
		if(!StringUtils.isEmpty(request.getStayId()) && !StringUtils.isEmpty(request.getUserId())){
			List<UserOptedService> listOptedServices = userOptedServicesMao.getByUserAndStay(request);
			if(CollectionUtils.isNotEmpty(listOptedServices)){
				List<GetUserOptedServiceResponse> listResponse = UserOptedServiceConverter.convertListEntityToListBean(listOptedServices);
				return listResponse;
			}
		}
		return null;
	}
	
	

	@Override
	public List<UserOptedService> getByUserStaysAndHotel(String staysId, String userId, String hotelId) {
		if(!StringUtils.isEmpty(userId) && !StringUtils.isEmpty(staysId) && !StringUtils.isEmpty(hotelId)){
			List<UserOptedService> listOptedServices = userOptedServicesMao.getByUserStayAndHotel(staysId, userId, hotelId);
			return listOptedServices;
		}
		return null;
	}

	@Override
	public UserOptedService getEntity(String id) {
		return userOptedServicesMao.getById(id);
	}

	@Override
	public boolean updateById(UserOptedService request) {
		if(!StringUtils.isEmpty(request.getId())){
			userOptedServicesMao.update(request);
			return true;
		}
		return false;
	}

	@Override
	public List<GetUserOptedServiceResponse> getByUserStaysHotel(UserStayHotelIdsPojo request) throws AboutstaysException {
		List<UserOptedService> listOptedServices = userOptedServicesMao.getByUserStayAndHotel(request.getStaysId(), request.getUserId(), request.getHotelId());
		
		List<GetUserOptedServiceResponse> listResponse = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(listOptedServices)){
			listResponse = UserOptedServiceConverter.convertListEntityToListBean(listOptedServices);
		}
		
		if(CollectionUtils.isNotEmpty(listResponse)){
			Iterator<GetUserOptedServiceResponse> iterator = listResponse.iterator();
			while(iterator.hasNext()) {
				GetUserOptedServiceResponse uosResponse = iterator.next();
				if(uosResponse == null){
					iterator.remove();
					continue;
				}
				
				String refId = uosResponse.getRefId();
				RefCollectionType typeEnum = RefCollectionType.findValueByType(uosResponse.getRefCollectionType());
				if(typeEnum == null){
					continue;
				}
				
				switch(typeEnum){
				case AIRPORT_PICKUP:{
					AirportPickup response =  apMao.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
				
				case FOOD:{
					GetFoodOrderResponse response = foodOrdersService.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
				case HOUSEKEEPING:{
					GetHKOrderResponse response = hkOrderService.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
				case LAUNDRY: {
					GetLaundryOrderResponse response = laundryOrderService.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
				case COMMUTE: {
					GetCommuteOrderResponse response = coService.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
				case INTERNET: {
					InternetOrderResponse response = ioService.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
				case RESERVATION: {
					GetReservationOrderResponse response = roService.getById(refId);
					if(response == null){
						iterator.remove();
						continue;
					}
					if(response.getStatus() == GeneralOrderStatus.CANCELLED.getCode()){
						iterator.remove();
						continue;
					}
					uosResponse.setShowTick(true);
					break;
				}
//				case EARLY_CHECK_IN: {
//					CheckinResponse checkinResponse = checkinService.getById(refId);
//					if(checkinResponse == null){
//						iterator.remove();
//						continue;
//					}
//					CheckinRequestStatus status = CheckinRequestStatus.findByCode(checkinResponse.getStatus());
//					if(CheckinRequestStatus.getGeneralOrderStatus(status) == GeneralOrderStatus.CANCELLED){
//						iterator.remove();
//						continue;
//					}
//					uosResponse.setShowTick(true);
//					break;
//				}
				default:
					break;
			}
			
		  }
		}
		
		RequestDashboardController controller = new RequestDashboardController();
		beanFactory.autowireBean(controller);
		CreateDashboardRequest dashboardRequest = new CreateDashboardRequest();
		dashboardRequest.setHotelId(request.getHotelId());
		dashboardRequest.setStaysId(request.getStaysId());
		dashboardRequest.setUserId(request.getUserId());
		BaseApiResponse<CreateDashboardResponse> dashboardResponse = controller.createDashboard(dashboardRequest);
		
		if(dashboardResponse != null && !dashboardResponse.isError()){
			if(dashboardResponse.getData() != null && 
			    CollectionUtils.isNotEmpty(dashboardResponse.getData().getOpenDashboardItems())) {
				GetUserOptedServiceResponse countResponse = new GetUserOptedServiceResponse();
				//countResponse.setGsId(gsId); we can make dashboard response return gsId
				// General Service Type for request
				GeneralService generalService = generalServicesService.getByType(GeneralServiceType.REQUEST);
				if(generalService != null){
					countResponse.setGsId(generalService.getId());
				}
				countResponse.setShowTick(true);
				countResponse.setCountToShow(dashboardResponse.getData().getOpenDashboardItems().size());
				listResponse.add(countResponse);
			}
		}
		return listResponse;
	}

	@Override
	public UserOptedService getByRefId(String refId) {
		if(!StringUtils.isEmpty(refId)) {
			return userOptedServicesMao.getByRefId(refId);
		}
		return null;
	}

	@Override
	public List<GetUserOptedServiceResponse> getAllByHotelAndDate(String hotelId, String date) {
		try {
			List<UserOptedService> all = userOptedServicesMao.getAllByHotelIdAndCreated(hotelId,DateUtil.parseDate(date, DateUtil.BASE_DATE_FORMAT).getTime());
			if(CollectionUtils.isNotEmpty(all)){
				return UserOptedServiceConverter.convertListEntityToListBean(all);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<GetUserOptedServiceResponse> getAllByHotelAndDateAndStatus(String hotelId, String date, Integer code) {
		try {
			List<UserOptedService> all = userOptedServicesMao.getAllByHotelIdAndCreatedAndStatus(hotelId,DateUtil.parseDate(date, DateUtil.BASE_DATE_FORMAT).getTime(),code);
			if(CollectionUtils.isNotEmpty(all)){
				return UserOptedServiceConverter.convertListEntityToListBean(all);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
