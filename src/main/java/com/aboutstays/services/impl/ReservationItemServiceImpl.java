package com.aboutstays.services.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.ReservationItem;
import com.aboutstays.mao.ReservationItemMao;
import com.aboutstays.request.dto.AddReservationItemRequest;
import com.aboutstays.request.dto.UpdateReservationItemRequest;
import com.aboutstays.response.dto.ReservationItemResponse;
import com.aboutstays.services.ReservationItemService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ReservationItemConverter;

@Service
public class ReservationItemServiceImpl implements ReservationItemService{

	@Autowired
	private ReservationItemMao rsItemMao;
	
	
	@Override
	public String save(AddReservationItemRequest request) {
		ReservationItem item = ReservationItemConverter.convertBeanToEntity(request);
		if(item != null){
			item.setId(null);
			rsItemMao.save(item);
			return item.getId();
		}
		return null;
	}

	@Override
	public ReservationItemResponse getById(String id) {
		ReservationItem item = rsItemMao.getById(id);
		return ReservationItemConverter.convertEntityToBean(item);
	}

	@Override
	public List<ReservationItemResponse> getAll() {
		List<ReservationItem> listEntity = rsItemMao.getAll();
		return ReservationItemConverter.convertListEntityToListBean(listEntity);
	}

	@Override
	public void update(UpdateReservationItemRequest request) {
		ReservationItem item = ReservationItemConverter.convertUpdateEntityToBean(request);
		if(item != null){
			rsItemMao.update(item);
		}
	}

	@Override
	public boolean updateNonNull(UpdateReservationItemRequest request) {
		ReservationItem item = ReservationItemConverter.convertUpdateEntityToBean(request);
		if(item != null){
			rsItemMao.updateNonNull(item);
			return true;
		}
		return false;
	}

	@Override
	public List<ReservationItemResponse> getAllByHotelId(String hotelId, String categoryId) {
		List<ReservationItem> items = rsItemMao.getByHotelId(hotelId);
		if(!StringUtils.isEmpty(categoryId)&&CollectionUtils.isNotEmpty(items)){
			Iterator<ReservationItem> iterator = items.iterator();
			while(iterator.hasNext()){
				ReservationItem item = iterator.next();
				if(!categoryId.equals(item.getCategoryId())){
					iterator.remove();
				}
			}
		}
		return ReservationItemConverter.convertListEntityToListBean(items);
	}

	@Override
	public boolean existsById(String id) {
		if(rsItemMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public ReservationItem getEntityById(String id) {
		return rsItemMao.getById(id);
	}

	@Override
	public void updateEntity(ReservationItem item) {
		rsItemMao.update(item);
	}


	@Override
	public boolean deleteById(String id) {
		if(!StringUtils.isEmpty(id)){
			rsItemMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean exists(String id, String categoryId, String hotelId) {
		ReservationItem reservationItem = rsItemMao.getByIdCategoryAndHotelId(id, categoryId, hotelId);
		if(reservationItem!=null){
			return true;
		}
		return false;
	}

}
