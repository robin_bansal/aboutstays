package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.ProcessPropertyInfo;
import com.aboutstays.mao.ProcessPropertyInfoMao;
import com.aboutstays.services.ProcessUtilityService;

@Service
public class ProcessUtilityServiceImpl implements ProcessUtilityService {

	@Autowired
	private ProcessPropertyInfoMao processMao;
	
	
	@Override
	public String getStringProcessProperty(String processName, String propertyName) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return null;
		}
		ProcessPropertyInfo propertyInfo = processMao.getProcessPropertyValue(processName, propertyName);
		if(propertyInfo != null){
			return propertyInfo.getPropertyValue();
		}
		return null;
	}

	@Override
	public Integer getIntegerProcessProperty(String processName, String propertyName) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return null;
		}
		ProcessPropertyInfo propertyInfo = processMao.getProcessPropertyValue(processName, propertyName);
		if(propertyInfo != null){
			try{
				return Integer.parseInt(propertyInfo.getPropertyValue());
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Long getLongProcessProperty(String processName, String propertyName) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return null;
		}
		ProcessPropertyInfo propertyInfo = processMao.getProcessPropertyValue(processName, propertyName);
		if(propertyInfo != null){
			try{
				return Long.parseLong(propertyInfo.getPropertyValue());
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Boolean getBooleanProcessProperty(String processName, String propertyName) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return null;
		}
		ProcessPropertyInfo propertyInfo = processMao.getProcessPropertyValue(processName, propertyName);
		if(propertyInfo != null){
			try{
				return Boolean.parseBoolean(propertyInfo.getPropertyValue());
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Boolean getBooleanProcessProperty(String processName, String propertyName, boolean defaultValue) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return null;
		}
		Boolean value = getBooleanProcessProperty(processName, propertyName);
		if(value == null){
			return defaultValue;
		}
		return value;
	}

	@Override
	public int getIntegerProcessProperty(String processName, String propertyName, int defaultValue) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return defaultValue;
		}
		Integer value = getIntegerProcessProperty(processName, propertyName);
		if(value==null){
			value = defaultValue;
		}
		return defaultValue;
	}

	@Override
	public List<String> getStringListProperty(String processName, String propertyName) {
		if(StringUtils.isEmpty(processName) || StringUtils.isEmpty(propertyName)){
			return null;
		}
		ProcessPropertyInfo propertyInfo = processMao.getProcessPropertyValue(processName, propertyName);
		if(propertyInfo != null){
			String valuesString = propertyInfo.getPropertyValue();
			if(!StringUtils.isEmpty(valuesString)){
				List<String> valuesList = new ArrayList<>();
				String[] values = valuesString.split(",");
				for(String value : values){
					valuesList.add(value.trim());
				}
				return valuesList;
			}
		}
		return null;
	}

}
