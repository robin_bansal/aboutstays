package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.RoomCategory;
import com.aboutstays.mao.MasterAmenityMao;
import com.aboutstays.mao.RoomCategoryMao;
import com.aboutstays.pojos.Amenity;
import com.aboutstays.request.dto.AddRoomCategoryRequest;
import com.aboutstays.request.dto.SetEarlyCheckInRoomRequest;
import com.aboutstays.request.dto.SetLateCheckOutRoomRequest;
import com.aboutstays.request.dto.UpdateRoomCategoryRequest;
import com.aboutstays.response.dto.GetEarlyCheckinRoomResponse;
import com.aboutstays.response.dto.GetLateCheckoutRoomResponse;
import com.aboutstays.response.dto.GetRoomCategoryResponse;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.utils.AmenityConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.RoomCategoryConverter;

@Service
public class RoomCategoryServiceImpl implements RoomCategoryService {
	
	@Autowired
	private RoomCategoryMao roomCategoryMao;
	
	@Autowired
	private MasterAmenityMao masterAmenityMao;

	@Override
	public String addRoomCategory(AddRoomCategoryRequest request) {
		RoomCategory room = RoomCategoryConverter.convertAddRoomRequestToEntity(request);
		if(room!=null){
			roomCategoryMao.save(room);
			return room.getRoomCategegoryId();
		}
		return null;
	}

	@Override
	public GetRoomCategoryResponse getById(String roomId) {
		RoomCategory room = roomCategoryMao.getById(roomId);
		return parseEnityToGetResponse(room);
	}

	@Override
	public List<GetRoomCategoryResponse> getAll() {
		List<RoomCategory> roomList = roomCategoryMao.getAll();
		return parseEntityListToGetList(roomList);
	}
	
	public GetRoomCategoryResponse parseEnityToGetResponse(RoomCategory room){
		if(room==null){
			return null;
		}
		GetRoomCategoryResponse roomResponse = RoomCategoryConverter.convertEntityToGetResponse(room);
		List<String> inRoomAminities = room.getInRoomAmenities();
		if(CollectionUtils.isNotEmpty(inRoomAminities)){
			List<Amenity> amenities = new ArrayList<>();
			for(String amenityId : inRoomAminities){
				Amenity amenity = AmenityConverter.convertMasterEntityToAmenity(masterAmenityMao.getById(amenityId));
				if(amenity==null){
					continue;
				}
				amenities.add(amenity);
			}
			roomResponse.setInRoomAmenities(amenities);
		}
		List<String> preferenceBasedAmenities = room.getPreferenceBasedAmenities();
		if(CollectionUtils.isNotEmpty(preferenceBasedAmenities)){
			List<Amenity> amenities = new ArrayList<>();
			for(String amenityId:preferenceBasedAmenities){
				Amenity amenity = AmenityConverter.convertMasterEntityToAmenity(masterAmenityMao.getById(amenityId));
				if(amenity==null){
					continue;
				}
				amenities.add(amenity);
			}
			roomResponse.setPreferenceBasedAmenities(amenities);
		}
		return roomResponse;
	}
	
	private List<GetRoomCategoryResponse> parseEntityListToGetList(List<RoomCategory> roomList){
		if(CollectionUtils.isNotEmpty(roomList)){
			List<GetRoomCategoryResponse> getList = new ArrayList<>();
			for(RoomCategory room : roomList){
				GetRoomCategoryResponse roomResponse = parseEnityToGetResponse(room);
				if(roomResponse!=null){
					getList.add(roomResponse);					
				}
			}
			return getList;
		}
		return null;
	}

	@Override
	public List<GetRoomCategoryResponse> getAllByRoomCategoryIds(List<String> roomIds) {
		List<RoomCategory> roomList = roomCategoryMao.getAllByRoomCategoryIds(roomIds);
		return parseEntityListToGetList(roomList);
	}

	@Override
	public void delete(String roomId) {
		roomCategoryMao.delete(roomId);
	}

	@Override
	public Boolean update(UpdateRoomCategoryRequest request) {
		if(request!=null){
			RoomCategory room = RoomCategoryConverter.convertUpdateRequestToEntity(request);
			if(room!=null){
				roomCategoryMao.update(room);			
				return true;
			}
		}
		return false;
	}

	@Override
	public Boolean updateNonNullFields(UpdateRoomCategoryRequest request) {
		if(request!=null){
			RoomCategory room = RoomCategoryConverter.convertUpdateRequestToEntity(request);
			if(room!=null){
				roomCategoryMao.updateNonNullFields(room);
				return true;
			}			
		}
		return false;
	}

	@Override
	public boolean exists(String roomId) {
		RoomCategory room = roomCategoryMao.getById(roomId);
		if(room!=null)
			return true;
		return false;
	}

	@Override
	public boolean existsInHotel(String hotelId, String roomId) {
		RoomCategory roomCategory = roomCategoryMao.getByHotelAndCategoryId(hotelId, roomId);
		if(roomCategory!=null)
			return true;
		return false;
	}

	@Override
	public boolean setEarlyCheckinRoomInfo(SetEarlyCheckInRoomRequest request) {
		if(!StringUtils.isEmpty(request.getRoomCategoryId()) && request.getInfo() != null){
			roomCategoryMao.setEarlyCheckinInfo(request);
			return true;
		}
		return false;
		
	}

	@Override
	public GetEarlyCheckinRoomResponse getEarlyCheckinRoomInfo(String roomId) {
		RoomCategory room = roomCategoryMao.getEarlyCheckinRoomInfo(roomId);
		return RoomCategoryConverter.convertEarlyCheckInInfo(room);
	}

	@Override
	public boolean setLateCheckoutRoomInfo(SetLateCheckOutRoomRequest request) {
		if(!StringUtils.isEmpty(request.getRoomCategoryId()) && request.getCheckOutInfo() != null){
			roomCategoryMao.setLateCheckoutInfo(request);
			return true;
		}
		return false;
	}

	@Override
	public GetLateCheckoutRoomResponse getLateCheckoutRoomInfo(String roomId) {
		RoomCategory room = roomCategoryMao.getLateCheckoutInfo(roomId);
		return RoomCategoryConverter.convertLateCheckoutInfo(room);
	}
	

	@Override
	public void updateInRoomAmenities(String roomCategoryId, List<String> amenityIds) {
		if(StringUtils.isEmpty(roomCategoryId)||CollectionUtils.isEmpty(amenityIds))
			return;
		RoomCategory room = roomCategoryMao.getById(roomCategoryId);
		if(room!=null){
			List<String> savedInRoomAmenities = room.getInRoomAmenities();
			if(savedInRoomAmenities==null)
				savedInRoomAmenities = new ArrayList<>();
			room.setInRoomAmenities(getUpdatedListFromOld(savedInRoomAmenities,amenityIds));
			roomCategoryMao.update(room);
		}
	}

	private List<String> getUpdatedListFromOld(List<String> oldList, List<String> newList) {
		Set<String> set = new HashSet<>(oldList);
		set.addAll(newList);
		oldList.clear();
		oldList.addAll(set);
		return oldList;
	}

	@Override
	public void updateInPreferenceBasedAmenities(String roomCategoryId, List<String> preferenceBasedAmenityIds) {
		if(StringUtils.isEmpty(roomCategoryId)||CollectionUtils.isEmpty(preferenceBasedAmenityIds))
			return;
		RoomCategory room = roomCategoryMao.getById(roomCategoryId);
		if(room!=null){
			List<String> savedPrefenceBasedAmenities = room.getPreferenceBasedAmenities();
			if(savedPrefenceBasedAmenities==null)
				savedPrefenceBasedAmenities = new ArrayList<>();
			room.setPreferenceBasedAmenities(getUpdatedListFromOld(savedPrefenceBasedAmenities,preferenceBasedAmenityIds));
			roomCategoryMao.update(room);
		}
	}

	@Override
	public List<GetRoomCategoryResponse> getAllByHotelId(String hotelId) {
		if(!StringUtils.isEmpty(hotelId)){
			List<RoomCategory> roomList = roomCategoryMao.getByHotelId(hotelId);
			return parseEntityListToGetList(roomList);			
		}
		return null;
	}


}
