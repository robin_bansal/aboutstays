package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.controller.CommuteOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.CommuteOrderMao;
import com.aboutstays.request.dto.AddCommuteOrderRequest;
import com.aboutstays.request.dto.UpdateCommuteOrderRequest;
import com.aboutstays.response.dto.GetCommuteOrderResponse;
import com.aboutstays.services.CommuteOrderService;
import com.aboutstays.utils.CommuteOrderConverter;

@Service
public class CommuteOrderServiceImpl implements CommuteOrderService{

	@Autowired
	private CommuteOrderMao coMao;
	
	@Override
	public String save(AddCommuteOrderRequest request) {
		CommuteOrder order = CommuteOrderConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			coMao.save(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public GetCommuteOrderResponse getById(String id) {
		CommuteOrder order = coMao.getById(id);
		return CommuteOrderConverter.convertEntityToBean(order);
	}

	@Override
	public List<GetCommuteOrderResponse> getAll() {
		List<CommuteOrder> listOrder = coMao.getAll();
		return CommuteOrderConverter.convertListEntityToListBean(listOrder);		
	}

	@Override
	public boolean updateNonNullFieldsOfRequest(UpdateCommuteOrderRequest request) {
		CommuteOrder order = CommuteOrderConverter.convertUpdateBeanToEntity(request);
		if(order != null){
			coMao.updateNonNull(order);
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(String id) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		coMao.delete(id);
		return true;
	}

	@Override
	public boolean existsById(String id) {
		if(coMao.getById(id) == null){
			return false;
		}
		return true;
	}

	@Override
	public CommuteOrder getEntity(String id) {
		return coMao.getById(id);
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(status != null){
			coMao.changeOrderStatus(id, status.getCode());
			return true;
		}
		return false;
	}

}
