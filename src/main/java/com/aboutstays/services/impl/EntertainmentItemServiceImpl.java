package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.EntertaimentItem;
import com.aboutstays.entities.MasterChannelItem;
import com.aboutstays.mao.EntertainmentItemMao;
import com.aboutstays.mao.MasterChannelMao;
import com.aboutstays.request.dto.AddEntertaimentItemRequest;
import com.aboutstays.request.dto.GetEntertaimentItemResponse;
import com.aboutstays.request.dto.UpdateEntertainmentRequest;
import com.aboutstays.response.dto.GetMasterChannelResponse;
import com.aboutstays.services.EntertainmentItemService;
import com.aboutstays.services.MasterChannelService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.EntertainmentItemConverter;

@Service
public class EntertainmentItemServiceImpl implements EntertainmentItemService{

	@Autowired
	private MasterChannelMao mcMao;
	
	@Autowired
	private EntertainmentItemMao eiMao;
	
	@Autowired
	private MasterChannelService masterChannelService;
	
	@Override
	public String add(AddEntertaimentItemRequest request) {
		if(request != null){
			MasterChannelItem channelItem =  mcMao.getById(request.getMasterItemId());
			EntertaimentItem entertaimentItem = EntertainmentItemConverter.convertBeanToEntity(request, channelItem.getGenereId());
			if(entertaimentItem!=null){
				eiMao.add(entertaimentItem);
				return entertaimentItem.getId();
			}
		}
		return null;
	}
	
	@Override
	public List<String> addMultiple(List<AddEntertaimentItemRequest> listRequest) {
		if(CollectionUtils.isNotEmpty(listRequest)){
			List<String> listResponse = new ArrayList<>();
			for(AddEntertaimentItemRequest request : listRequest){
				if(request == null){
					continue;
				}
				String id = add(request);
				listResponse.add(id);
			}
			return listResponse;
		}
		return null;
	}

	@Override
	public boolean existsById(String id) {
		if(eiMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public GetEntertaimentItemResponse getById(String id) {
		EntertaimentItem item = eiMao.getById(id);
		if(item!=null){
			GetMasterChannelResponse masterChannelDetails = masterChannelService.getById(item.getMasterItemId());
			return EntertainmentItemConverter.convertEntityToBean(item, masterChannelDetails);
		}
		return null;
	}


	@Override
	public List<GetEntertaimentItemResponse> getAll() {
		List<EntertaimentItem> listItem = eiMao.getAll();
		List<GetEntertaimentItemResponse> listResponse = new ArrayList<>();
		
		if(CollectionUtils.isNotEmpty(listItem)){
			for(EntertaimentItem item : listItem){
				if(item == null){
					continue;
				}
				GetMasterChannelResponse masterChannelDetails = masterChannelService.getById(item.getMasterItemId());
				GetEntertaimentItemResponse response = EntertainmentItemConverter.convertEntityToBean(item, masterChannelDetails);
				if(response != null){
					listResponse.add(response);
				}
			}
			return listResponse;
		}
		return null;
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			eiMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateNonNull(UpdateEntertainmentRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			MasterChannelItem channelItem =  mcMao.getById(request.getMasterItemId());
			EntertaimentItem entertaimentItem = EntertainmentItemConverter.convertUpdateBeanToEntity(request, channelItem.getGenereId());
			if(entertaimentItem!=null){
				eiMao.updateNonNull(entertaimentItem);
				return true;
			}
		}
		return false;
	}

	@Override
	public List<GetEntertaimentItemResponse> getAll(String entServiceId, String genereId) {
		List<EntertaimentItem> listEntertainmentItem = eiMao.getAll(entServiceId);
		List<GetEntertaimentItemResponse> listResponse = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(listEntertainmentItem)){
			Iterator<EntertaimentItem> iterator = listEntertainmentItem.iterator();
			while(iterator.hasNext()){
				EntertaimentItem item = iterator.next();
				if(!item.getCategory().equalsIgnoreCase(genereId)){
					iterator.remove();
				} else {
					GetMasterChannelResponse masterChannelDetails = masterChannelService.getById(item.getMasterItemId());
					GetEntertaimentItemResponse response = EntertainmentItemConverter.convertEntityToBean(item, masterChannelDetails);
					if(response != null){
						listResponse.add(response);
					}
				}
				
			}
		}
		return listResponse;
	}

	@Override
	public boolean existsByMasterItemId(String masterItemId, String hotelId, String serviceId) {
		EntertaimentItem entertaimentItem = eiMao.getByMasterItemId(masterItemId, hotelId, serviceId);
		if(entertaimentItem!=null)
			return true;
		return false;
	}

	@Override
	public boolean update(UpdateEntertainmentRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			MasterChannelItem channelItem =  mcMao.getById(request.getMasterItemId());
			EntertaimentItem entertaimentItem = EntertainmentItemConverter.convertUpdateBeanToEntity(request, channelItem.getGenereId());
			if(entertaimentItem!=null){
				eiMao.update(entertaimentItem);
				return true;
			}
		}
		return false;
	}

	@Override
	public List<GetEntertaimentItemResponse> getAll(String entServiceId) {
		List<EntertaimentItem> listEntertainmentItem = eiMao.getAll(entServiceId);
		List<GetEntertaimentItemResponse> listResponse = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(listEntertainmentItem)){
			Iterator<EntertaimentItem> iterator = listEntertainmentItem.iterator();
			while(iterator.hasNext()){
				EntertaimentItem item = iterator.next();
				GetMasterChannelResponse masterChannelDetails = masterChannelService.getById(item.getMasterItemId());
				GetEntertaimentItemResponse response = EntertainmentItemConverter.convertEntityToBean(item, masterChannelDetails);
				if(response != null){
					listResponse.add(response);
				}
			}
		}
		return listResponse;
	}

}
