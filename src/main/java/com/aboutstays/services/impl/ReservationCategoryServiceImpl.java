package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.MasterAmenity;
import com.aboutstays.entities.ReservationCategory;
import com.aboutstays.enums.ReservationType;
import com.aboutstays.mao.MasterAmenityMao;
import com.aboutstays.mao.ReservationCategoryMao;
import com.aboutstays.pojos.Direction;
import com.aboutstays.pojos.DirectionDetail;
import com.aboutstays.request.dto.AddReservationCategoryRequest;
import com.aboutstays.request.dto.UpdateReservationCategoryRequest;
import com.aboutstays.response.dto.GetReservationCategoryResponse;
import com.aboutstays.services.ReservationCategoryService;
import com.aboutstays.utils.AmenityConverter;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.ReservationCategoryConverter;

@Service
public class ReservationCategoryServiceImpl implements ReservationCategoryService {
	
	@Autowired
	private ReservationCategoryMao reservationCategoryMao; 
	
	@Autowired
	private MasterAmenityMao masterAmenityMao;

	@Override
	public String addCategory(AddReservationCategoryRequest request) {
		ReservationCategory reservationCategory = ReservationCategoryConverter.convertAddRequestToEntity(request);
		if(reservationCategory!=null){
			reservationCategoryMao.saveT(reservationCategory);
			return reservationCategory.getId();
		}
		return null;
	}

	@Override
	public boolean exists(String categoryId) {
		ReservationCategory reservationCategory = reservationCategoryMao.getTById(categoryId);
		if(reservationCategory!=null){
			return true;
		}
		return false;
	}

	@Override
	public GetReservationCategoryResponse getById(String categoryId) {
		ReservationCategory reservationCategory = reservationCategoryMao.getTById(categoryId);
		return parseEntityToGetResponse(reservationCategory);
	}

	@Override
	public void update(UpdateReservationCategoryRequest request) {
		ReservationCategory reservationSubCategory = ReservationCategoryConverter.convertUpdateRequestToEntity(request);
		if(reservationSubCategory!=null){
			reservationCategoryMao.updateT(reservationSubCategory);
		}
	}

	@Override
	public void updateNonNull(UpdateReservationCategoryRequest request) {
		ReservationCategory reservationCategory = ReservationCategoryConverter.convertUpdateRequestToEntity(request);
		if(reservationCategory!=null){
			reservationCategoryMao.updateNonNullFieldsOfT(reservationCategory, reservationCategory.getId());
		}
	}

	@Override
	public List<GetReservationCategoryResponse> getAll(String hotelId, ReservationType reservationType) {
		List<ReservationCategory> reservationCategories = null;
		if(!StringUtils.isEmpty(hotelId)&&reservationType!=null){
			reservationCategories = reservationCategoryMao.getByHotelIdAndType(hotelId, reservationType.getCode());
		} else if(reservationType!=null){
			reservationCategories = reservationCategoryMao.getByType(reservationType.getCode());
		} else if(!StringUtils.isEmpty(hotelId)){
			reservationCategories = reservationCategoryMao.getByHotelId(hotelId);
		} else {
			reservationCategories = reservationCategoryMao.getAllT();
		}
		return parseEntityListToGetList(reservationCategories);
	}

	@Override
	public void delete(String categoryId) {
		reservationCategoryMao.deleteT(categoryId);
	}

	@Override
	public boolean exists(String categoryId, String hotelId) {
		ReservationCategory reservationCategory = reservationCategoryMao.getByIdAndHotelId(categoryId, hotelId);
		if(reservationCategory!=null)
			return true;
		return false;
	}
	
	private List<GetReservationCategoryResponse> parseEntityListToGetList(List<ReservationCategory> requestList){
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<GetReservationCategoryResponse> responseList = new ArrayList<>(requestList.size());
		for(ReservationCategory reservationCategory : requestList){
			responseList.add(parseEntityToGetResponse(reservationCategory));
		}
		return responseList;
	}
	
	private GetReservationCategoryResponse parseEntityToGetResponse(ReservationCategory reservationCategory){
		GetReservationCategoryResponse response = ReservationCategoryConverter.convertEntityToGetResponse(reservationCategory);
		if(response!=null){
			if(CollectionUtils.isNotEmpty(reservationCategory.getDirectionList())){
				List<DirectionDetail> directionList = new ArrayList<>(reservationCategory.getDirectionList().size());
				for(Direction direction : reservationCategory.getDirectionList()){
					MasterAmenity masterAmenity = masterAmenityMao.getById(direction.getDirectionId());
					DirectionDetail directionDetail = AmenityConverter.convertMasterAmentityToDirectionDetail(masterAmenity, direction.getDirectionInfo());
					if(directionDetail!=null){
						directionList.add(directionDetail);
					}
				}
				response.setDirectionList(directionList);
			}
		}
		return response;
	}

}
