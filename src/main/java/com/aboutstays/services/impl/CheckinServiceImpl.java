package com.aboutstays.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.CheckinEntity;
import com.aboutstays.enums.CheckinRequestStatus;
import com.aboutstays.enums.StayPreferencesType;
import com.aboutstays.mao.CheckinMao;
import com.aboutstays.pojos.EarlyCheckinRequest;
import com.aboutstays.pojos.PreferencesType;
import com.aboutstays.pojos.StayPreferences;
import com.aboutstays.request.dto.InitiateCheckinRequest;
import com.aboutstays.request.dto.ModifyCheckinRequest;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.services.CheckinService;
import com.aboutstays.utils.CheckinConverter;
import com.aboutstays.utils.CollectionUtils;

@Service
public class CheckinServiceImpl implements CheckinService {

	@Autowired
	private CheckinMao checkinMao;
	
	@Override
	public String save(InitiateCheckinRequest request) {
		CheckinEntity checkinEntity = CheckinConverter.convertInitiateRequestToEntity(request);
		if(checkinEntity != null) {
			checkinEntity.setListStayPreferences(getPreferencesCode(request.getListStayPreferences()));
			if(checkinEntity!=null){
				checkinMao.save(checkinEntity);
				return checkinEntity.getStaysId();
			}
		}
		return null;
	}

	public List<PreferencesType> getPreferencesCode(List<StayPreferences> listStayPreferences){
		List<PreferencesType> listPreferencesCodes = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(listStayPreferences)) {
			for(StayPreferences prefs : listStayPreferences) {
				if(prefs == null) {
					continue;
				}
				StayPreferencesType type = StayPreferencesType.getByDisplayName(prefs.getName());
				if(type == null){
					continue;
				}
				PreferencesType prefType = new PreferencesType();
				prefType.setPreference(type.getCode());
				listPreferencesCodes.add(prefType);
			}
		}
		return listPreferencesCodes;
	}
	
	@Override
	public String update(ModifyCheckinRequest request) {
		CheckinEntity checkinEntity = CheckinConverter.convertModifyRequestToEntity(request);
		if(checkinEntity!=null){
			checkinEntity.setListStayPreferences(getPreferencesCode(request.getListStayPreferences()));
			checkinMao.update(checkinEntity);
			return checkinEntity.getStaysId();
		}
		return null;
	}

	@Override
	public void updateNonNull(ModifyCheckinRequest request) {
		CheckinEntity checkinEntity = CheckinConverter.convertModifyRequestToEntity(request);
		if(checkinEntity!=null){
			checkinEntity.setListStayPreferences(getPreferencesCode(request.getListStayPreferences()));
			checkinMao.updateNonNull(checkinEntity);
		}	
	}

	@Override
	public CheckinResponse getById(String id) {
		CheckinEntity checkinEntity = checkinMao.getById(id);
		return CheckinConverter.convertEntityToResponse(checkinEntity);
	}

	@Override
	public void delete(String id) {
		checkinMao.delete(id);
	}

	@Override
	public void changeStatus(String id, CheckinRequestStatus checkinRequestStatus) {
		if(StringUtils.isEmpty(id)||checkinRequestStatus==null)
			return;
		checkinMao.changeStatus(id, checkinRequestStatus.getCode());
	}

	@Override
	public CheckinResponse getByUserStaysHotelId(String userId, String staysId, String hotelId) {
		CheckinEntity checkinEntity = checkinMao.getByUserStaysHotelId(userId, staysId, hotelId);
		return CheckinConverter.convertEntityToResponse(checkinEntity);
	}

	@Override
	public CheckinEntity getEntity(String id) {
		return checkinMao.getById(id);
	}

	@Override
	public void updatePriceAndIsApproved(boolean isApproved, double charges, String staysId) {
		if(!StringUtils.isEmpty(staysId)){
			CheckinEntity checkingEntity = checkinMao.getById(staysId);
			if(checkingEntity!=null){
				checkingEntity.setEarlyCheckinApproved(isApproved);
				EarlyCheckinRequest earlyCheckinRequest = checkingEntity.getEarlyCheckinRequest();
				if(earlyCheckinRequest!=null){
					earlyCheckinRequest.setCharges(charges);
					checkinMao.updateNonNull(checkingEntity);
				}				
			}
		}
	}

}
