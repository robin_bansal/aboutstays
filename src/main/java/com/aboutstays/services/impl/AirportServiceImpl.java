package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboutstays.entities.Airport;
import com.aboutstays.mao.AirportMao;
import com.aboutstays.request.dto.AirportRequest;
import com.aboutstays.response.dto.AirportResponse;
import com.aboutstays.services.AirportService;
import com.aboutstays.utils.AirportConverter;

@Service
public class AirportServiceImpl implements AirportService{

	@Autowired
	private AirportMao airportMao;
	
	@Override
	public String addAirport(AirportRequest request) {
		Airport airport = AirportConverter.convertRequestToEntity(request, false);
		if(airport!=null){
			airportMao.save(airport);
			return airport.getAirportId();
		}
		return null;
	}

	@Override
	public void updateAirport(AirportRequest request) {
		Airport airport = AirportConverter.convertRequestToEntity(request, true);
		if(airport!=null){
			airportMao.update(airport);
		}
	}

	@Override
	public void updateNonNullAirport(AirportRequest request) {
		Airport airport = AirportConverter.convertRequestToEntity(request, true);
		if(airport!=null){
			airportMao.updateNonNull(airport);
		}
	}

	@Override
	public void deleteAirport(String id) {
		airportMao.delete(id);
	}

	@Override
	public AirportResponse getAirport(String id) {
		Airport airport = airportMao.getById(id);
		return AirportConverter.convertEntityToResponse(airport);
	}

	@Override
	public List<AirportResponse> getAll() {
		List<Airport> airports = airportMao.getAll();
		return AirportConverter.convertEntityListToResponseList(airports);
	}

	@Override
	public List<AirportResponse> getAllByCityId(String cityId) {
		List<Airport> airports = airportMao.getByCityId(cityId);
		return AirportConverter.convertEntityListToResponseList(airports);
	}

	@Override
	public boolean existsById(String airportId) {
		Airport airport = airportMao.getById(airportId);
		if(airport!=null)
			return true;
		return false;
	}

}
