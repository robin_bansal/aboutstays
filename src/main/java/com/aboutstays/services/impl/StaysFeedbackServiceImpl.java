package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.StaysFeedback;
import com.aboutstays.mao.StaysFeedbackMao;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.AddStaysFeedbackRequest;
import com.aboutstays.request.dto.UpdateStaysFeedbackRequest;
import com.aboutstays.response.dto.StaysFeedbackResponse;
import com.aboutstays.services.StaysFeedbackService;
import com.aboutstays.utils.StaysFeedbackConverter;

@Service
public class StaysFeedbackServiceImpl implements StaysFeedbackService{

	@Autowired
	private StaysFeedbackMao sfMao;
	
	@Override
	public String add(AddStaysFeedbackRequest request) {
		StaysFeedback feedback = StaysFeedbackConverter.convertBeanToEntity(request);
		if(feedback != null){
			feedback.setId(null);
			sfMao.add(feedback);
			return feedback.getId();
		}
		return null;
	}

	@Override
	public StaysFeedbackResponse getById(String id) {
		StaysFeedback feedback = sfMao.getById(id); 
		return StaysFeedbackConverter.convertEntityToBean(feedback);
	}

	@Override
	public List<StaysFeedbackResponse> getAll() {
		List<StaysFeedback> listFeedback = sfMao.getAll();
		return StaysFeedbackConverter.convertListEntityToListBean(listFeedback);
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			sfMao.delete(id);
			return true;
		}
		return false;
	}



	@Override
	public boolean existsById(String id) {
		if(sfMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public List<StaysFeedbackResponse> getAllByUser(String userId) {
		List<StaysFeedback> listFeedback = sfMao.getAllOfUser(userId);
		return StaysFeedbackConverter.convertListEntityToListBean(listFeedback);
	}

	@Override
	public List<StaysFeedbackResponse> getAllByHotel(String hotelId) {
		List<StaysFeedback> listFeedback = sfMao.getAllOfHotel(hotelId);
		return StaysFeedbackConverter.convertListEntityToListBean(listFeedback);
	}

	@Override
	public List<StaysFeedbackResponse> getAllOfUserStay(String userId, String staysId) {
		List<StaysFeedback> listFeedback = sfMao.getAllOfUserStay(userId, staysId);
		return StaysFeedbackConverter.convertListEntityToListBean(listFeedback);
	}

	@Override
	public boolean updateNonNull(UpdateStaysFeedbackRequest request) {
		if(request != null && !StringUtils.isEmpty(request.getId())){
			StaysFeedback feedback = StaysFeedbackConverter.convertUpdateBeanToEntity(request);
			if(feedback != null){
				sfMao.updateNonNull(feedback);
				return true;
			}
			
		}
		return false;
	}

	@Override
	public StaysFeedbackResponse getAllByUserHotelStays(UserStayHotelIdsPojo request) {
		if(request != null){
			StaysFeedback feedback = sfMao.getByUserHotelStays(request);
			return StaysFeedbackConverter.convertEntityToBean(feedback); 
		}
		return null;
	}

	@Override
	public boolean update(UpdateStaysFeedbackRequest request) {
		StaysFeedback feedback = StaysFeedbackConverter.convertUpdateBeanToEntity(request);
		if(feedback != null){
			sfMao.update(feedback);
			return true;
		}
		return false;
	}




}
