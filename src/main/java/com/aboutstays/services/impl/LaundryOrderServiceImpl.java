package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.LaundryOrderMao;
import com.aboutstays.request.dto.AddLaundryOrderRequest;
import com.aboutstays.request.dto.UpdateLaundryOrderRequest;
import com.aboutstays.response.dto.GetLaundryOrderResponse;
import com.aboutstays.services.LaundryOrderService;
import com.aboutstays.utils.LaundryOrderConverter;

@Service
public class LaundryOrderServiceImpl implements LaundryOrderService{

	@Autowired
	private LaundryOrderMao laundryOrderMao;
	
	@Override
	public String save(AddLaundryOrderRequest request) {
		LaundryOrder order = LaundryOrderConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			laundryOrderMao.save(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public GetLaundryOrderResponse getById(String id) {
		LaundryOrder order = laundryOrderMao.getById(id);
		return LaundryOrderConverter.convertEntityToBean(order);
	}

	@Override
	public List<GetLaundryOrderResponse> getAll() {
		List<LaundryOrder> listOrders = laundryOrderMao.getAll();
		return LaundryOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public List<GetLaundryOrderResponse> getAllByUserId(String userId) {
		List<LaundryOrder> listOrders = laundryOrderMao.getAllByUserId(userId);
		return LaundryOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public List<GetLaundryOrderResponse> getAllByHotelId(String hotelId) {
		List<LaundryOrder> listOrders = laundryOrderMao.getAllByHotelId(hotelId);
		return LaundryOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public List<GetLaundryOrderResponse> getAllByUserAndStayId(String userId, String stayId) {
		List<LaundryOrder> listOrders = laundryOrderMao.getAllByUserAndStayId(userId, stayId);
		return LaundryOrderConverter.convertListEntityToListBean(listOrders);
	}

	@Override
	public void updateRequest(UpdateLaundryOrderRequest request) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean updateNonNullFieldsOfRequest(UpdateLaundryOrderRequest request) {
		LaundryOrder order = LaundryOrderConverter.convertUpdateRequestToEntity(request);
		if (order != null) {
			laundryOrderMao.updateNonNull(order);
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			laundryOrderMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean existsById(String id) {
		if(laundryOrderMao.getById(id) != null){
			return true;
		}
		return false;
	}

	@Override
	public LaundryOrder getEntity(String id) {
		return laundryOrderMao.getById(id);
	}
	
	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus status) {
		if(StringUtils.isEmpty(id)||status==null){
			return false;
		}
		laundryOrderMao.changeOrderStatus(id, status.getCode());
		return true;
	}

	@Override
	public boolean updatLaundryOrderRequest(LaundryOrder order) {
		if(order != null){
			laundryOrderMao.updateNonNull(order);
			return true;
		}
		return false;
	}

}
