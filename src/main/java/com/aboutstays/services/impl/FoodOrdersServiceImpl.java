package com.aboutstays.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.FoodOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.mao.FoodRequestsMao;
import com.aboutstays.request.dto.AddFoodOrderRequest;
import com.aboutstays.request.dto.UpdateFoodOrderRequest;
import com.aboutstays.response.dto.GetFoodOrderResponse;
import com.aboutstays.services.FoodOrdersService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.FoodOrderRequestConverter;

@Service
public class FoodOrdersServiceImpl implements FoodOrdersService {
	
	@Autowired
	private FoodRequestsMao foodRequestMao;

	@Override
	public String save(AddFoodOrderRequest request) {
		FoodOrder order = FoodOrderRequestConverter.convertBeanToEntity(request);
		if(order != null){
			order.setId(null);
			foodRequestMao.save(order);
			return order.getId();
		}
		return null;
	}

	@Override
	public boolean updateNonNullFieldsOfRequest(UpdateFoodOrderRequest request) {
		FoodOrder order = FoodOrderRequestConverter.convertUpdateRequestToEntity(request);
		if (order != null) {
			foodRequestMao.updateNonNull(order);
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(String id) {
		if(!StringUtils.isEmpty(id)){
			foodRequestMao.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public GetFoodOrderResponse getById(String id) {
		FoodOrder order = foodRequestMao.getById(id);
		return FoodOrderRequestConverter.convertEntityToBean(order);
	}

	@Override
	public List<GetFoodOrderResponse> getAll() {
		List<FoodOrder> listOrder = foodRequestMao.getAll();
		if(CollectionUtils.isNotEmpty(listOrder)){
			List<GetFoodOrderResponse> listResponse = FoodOrderRequestConverter.convertListEntityToListBean(listOrder);
			return listResponse;
		}
		return null;
	}

	@Override
	public List<GetFoodOrderResponse> getAllByUserId(String userId) {
		List<FoodOrder> listOrder = foodRequestMao.getAllByUserId(userId);
		if(CollectionUtils.isNotEmpty(listOrder)){
			List<GetFoodOrderResponse> listResponse = FoodOrderRequestConverter.convertListEntityToListBean(listOrder);
			return listResponse;
		}	
		return null;
	}


	@Override
	public List<GetFoodOrderResponse> getAllByHotelId(String hotelId) {
		List<FoodOrder> listOrder = foodRequestMao.getAllByHotelId(hotelId);
		if(CollectionUtils.isNotEmpty(listOrder)){
			List<GetFoodOrderResponse> listResponse = FoodOrderRequestConverter.convertListEntityToListBean(listOrder);
			return listResponse;
		}	
		return null;
	}



	@Override
	public List<GetFoodOrderResponse> getAllByUserAndStayId(String userId, String stayId) {
		List<FoodOrder> listOrder = foodRequestMao.getAllByUserAndStayId(userId, stayId);
		if(CollectionUtils.isNotEmpty(listOrder)){
			List<GetFoodOrderResponse> listResponse = FoodOrderRequestConverter.convertListEntityToListBean(listOrder);
			return listResponse;
		}			
		return null;
	}



	@Override
	public void updateRequest(UpdateFoodOrderRequest request) {
		if(request != null){
			FoodOrder order = FoodOrderRequestConverter.convertUpdateRequestToEntity(request);
			if(order != null){
				foodRequestMao.update(order);
			}
		}
	}

	@Override
	public boolean existsById(String id) {
		if(foodRequestMao.getById(id) == null){
			return false;
		}
		return true;
	}

	@Override
	public FoodOrder getEntity(String id) {
		return foodRequestMao.getById(id);
	}

	@Override
	public boolean changeOrderStatus(String id, GeneralOrderStatus foodOrderStatus) {
		if(StringUtils.isEmpty(id)||foodOrderStatus==null){
			return false;
		}
		foodRequestMao.changeOrderStatus(id, foodOrderStatus.getCode());
		return true;
	}

	@Override
	public boolean updateFoodOrderRequest(FoodOrder foodOrder) {
		if(foodOrder != null){
			foodRequestMao.updateNonNull(foodOrder);
			return true;
		}
		return false;
	}

}
