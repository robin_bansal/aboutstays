package com.aboutstays.services.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.LaundryItem;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.mao.LaundryItemMao;
import com.aboutstays.request.dto.LaundryItemRequest;
import com.aboutstays.response.dto.LaundryItemResponse;
import com.aboutstays.services.LaundryItemService;
import com.aboutstays.utils.CollectionUtils;
import com.aboutstays.utils.LaundryItemConverter;

@Service
public class LaundryItemServiceImpl implements LaundryItemService {
	
	@Autowired
	private LaundryItemMao laundryItemMao;

	@Override
	public String save(LaundryItemRequest request) {
		LaundryItem item = LaundryItemConverter.convertAddRequestToEntity(request);
		if(request!=null){
			laundryItemMao.save(item);
			return item.getId();
		}
		return null;
	}

	@Override
	public LaundryItemResponse getById(String id) {
		return LaundryItemConverter.convertEntityToGetResponse(laundryItemMao.getById(id));
	}

	@Override
	public List<LaundryItemResponse> getAll() {
		return LaundryItemConverter.convertEntityListToResponseList(laundryItemMao.getAll());
	}

	@Override
	public List<LaundryItemResponse> getAllByServicesId(String servicesId) {
		return LaundryItemConverter.convertEntityListToResponseList(laundryItemMao.getAllByServicesId(servicesId));
	}

	@Override
	public List<LaundryItemResponse> getAllByHotelId(String hotelId) {
		return LaundryItemConverter.convertEntityListToResponseList(laundryItemMao.getAllByHotelId(hotelId));
	}

	@Override
	public void update(LaundryItemRequest request) {
		LaundryItem item = LaundryItemConverter.converUpdateRequestToEntity(request);
		if(request!=null){
			laundryItemMao.update(item);
		}
	}

	@Override
	public void updateNonNull(LaundryItemRequest request) {
		LaundryItem item = LaundryItemConverter.converUpdateRequestToEntity(request);
		if(request!=null){
			laundryItemMao.save(item);
		}
	}

	@Override
	public void delete(String id) {
		laundryItemMao.delete(id);
	}

	@Override
	public boolean existsById(String id) {
		LaundryItem item = laundryItemMao.getById(id);
		if(item!=null)
			return true;
		return false;
	}

	@Override
	public List<LaundryItemResponse> getByType(String laundryServicesId, LaundryItemType laundryItemType) {
		if(StringUtils.isEmpty(laundryServicesId)||laundryItemType==null)
			return null;
		return LaundryItemConverter.convertEntityListToResponseList(laundryItemMao.getByType(laundryServicesId, laundryItemType.getCode()));
	}

	@Override
	public List<LaundryItemResponse> searchLaundryItemsByQuery(String laundryServicesId, String query) {
		if(StringUtils.isEmpty(laundryServicesId)||StringUtils.isEmpty(query))
			return null;
		query = query.toLowerCase();
		List<LaundryItemResponse> responseList = LaundryItemConverter.convertEntityListToResponseList(laundryItemMao.getAllByServicesId(laundryServicesId));
		if(CollectionUtils.isNotEmpty(responseList)){
			Iterator<LaundryItemResponse> iterator = responseList.iterator();
			while(iterator.hasNext()){
				LaundryItemResponse item = iterator.next();
				if(!item.getName().toLowerCase().contains(query))
					iterator.remove();
			}
		}
		return responseList;
	}

}
