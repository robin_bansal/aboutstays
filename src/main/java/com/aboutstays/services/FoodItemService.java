package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.FoodItem;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.request.dto.FoodItemRequest;
import com.aboutstays.request.dto.UpdateFoodItemRequest;
import com.aboutstays.response.dto.FoodItemResponse;

public interface FoodItemService {

	public String save(FoodItemRequest foodItemRequest);
	public FoodItemResponse getById(String id);
	public List<FoodItemResponse> getAllFoodItems();
	public void update(UpdateFoodItemRequest foodItemRequest);
	public void updateNonNull(UpdateFoodItemRequest foodItemRequest);
	public List<FoodItemResponse> getAllByHotelId(String hotelId);
	public List<FoodItemResponse> getAllByRoomServicesId(String roomServicesId);
	public boolean existsById(String foodItemId);
	public List<FoodItemResponse> searchByQueryString(String id, String query);
	public FoodItem getEntityById(String foodItemId);
	public void updateEntity(FoodItem foodItem);
	public List<FoodItemResponse> getAllByType(String roomServicesId, MenuItemType menuItemType);
	public void deleteFoodItem(String id);
}
