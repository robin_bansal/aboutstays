package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.CityGuide;
import com.aboutstays.request.dto.AddCityGuideRequest;
import com.aboutstays.request.dto.UpdateCityGuideRequest;
import com.aboutstays.response.dto.GetCityGuideResponse;

public interface CityGuideService {

	public String add(AddCityGuideRequest request);
	public List<GetCityGuideResponse> getAll();
	public GetCityGuideResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateCityGuideRequest request);
	public CityGuide getEntity(String cityGuideId);
}
