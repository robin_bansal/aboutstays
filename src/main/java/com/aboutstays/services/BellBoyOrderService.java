package com.aboutstays.services;

import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;
import com.aboutstays.request.dto.AddBellBoyOrderRequest;
import com.aboutstays.response.dto.GetBellBoyOrderResponse;

public interface BellBoyOrderService {
	
	public String save(AddBellBoyOrderRequest request);
	public GetBellBoyOrderResponse getRequestedBellBoy(UserStaysHotelGSIdsPojo request);
	public boolean delete(String id);
	public BellBoyOrder getEntity(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	

}
