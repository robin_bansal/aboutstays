package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.User;
import com.aboutstays.enums.Type;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.SignupRequest;
import com.aboutstays.request.dto.UpdateContactDetails;
import com.aboutstays.request.dto.UpdatePasswordRequest;
import com.aboutstays.request.dto.UpdatePersonalDetails;
import com.aboutstays.request.dto.UpdateStayPrefsRequest;
import com.aboutstays.request.dto.UpdateUserRequest;
import com.aboutstays.request.dto.ValidateOtpRequest;
import com.aboutstays.response.dto.LoginResponse;
import com.aboutstays.response.dto.SignupResponse;

public interface UserService {
	public SignupResponse signupUser(SignupRequest signupRequest);
	public boolean existsByNumber(String number);
	public boolean existsByUserId(String userId);
	public boolean existsByEmail(String emailId);
	public boolean existsByEmailOrPhone(String emailId, String number);
	public UserPojo getByEmailOrPhone(String emailId, String number);
	public UserPojo getByEmail(String emailId);
	public UserPojo getByNumber(String mobileNumber);
	public boolean updateOtpByEmailOrPhone(ValidateOtpRequest validateOtpRequest);
	public boolean updateUserByType(UpdateUserRequest updateUserRequest, Type type, String value);
	public boolean updatePasswordByType(UpdatePasswordRequest updatePasswordRequest, Type typeEnum, String value);
	public LoginResponse matchCredentials(Type type, String value);	
	public boolean updatePersonalDetails(UpdatePersonalDetails request) throws AboutstaysException;
	public boolean updateContactDetails(UpdateContactDetails request);
	public boolean updateStayPrefs(UpdateStayPrefsRequest prefList);
	public User getUserEntity(String userId);
	public boolean updateDocs(List<IdentityDoc> identityDocs, String userId);
	public boolean updateProfilePicture(String userId, String imageUrl);
	public UserPojo getById(String userId);
}
