package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.request.dto.LaundryItemRequest;
import com.aboutstays.response.dto.LaundryItemResponse;

public interface LaundryItemService {

	public String save(LaundryItemRequest request);
	public LaundryItemResponse getById(String id);
	public List<LaundryItemResponse> getAll();
	public List<LaundryItemResponse> getAllByServicesId(String servicesId);
	public List<LaundryItemResponse> getAllByHotelId(String hotelId);
	public void update(LaundryItemRequest request);
	public void updateNonNull(LaundryItemRequest request);
	public void delete(String id);
	public boolean existsById(String id);
	public List<LaundryItemResponse> getByType(String laundryServicesId, LaundryItemType laundryItemType);
	public List<LaundryItemResponse> searchLaundryItemsByQuery(String laundryServicesId, String query);
}
