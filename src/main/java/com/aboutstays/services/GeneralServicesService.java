package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.GeneralService;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.request.dto.AddGeneralServiceRequest;
import com.aboutstays.request.dto.UpdateGeneralServiceRequest;
import com.aboutstays.response.dto.AddGeneralServiceResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;

public interface GeneralServicesService {
	
	public AddGeneralServiceResponse add(AddGeneralServiceRequest request);
	public List<GetGeneralServiceResponse> getAll();
	public List<GetGeneralServiceResponse> getAllByStaysStatus(Integer status);
	public GetGeneralServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateGeneralServiceRequest request);
	public GeneralService getByType(GeneralServiceType type);
}
