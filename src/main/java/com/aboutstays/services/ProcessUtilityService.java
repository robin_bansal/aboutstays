package com.aboutstays.services;

import java.util.List;

public interface ProcessUtilityService {
	
	public String getStringProcessProperty(String processName, String propertyName);
	public Integer getIntegerProcessProperty(String processName, String propertyName);
	public Long getLongProcessProperty(String processName, String propertyName);
	public Boolean getBooleanProcessProperty(String processName, String propertyName);
	public Boolean getBooleanProcessProperty(String processName, String propertyName, boolean defaultValue);
	public int getIntegerProcessProperty(String processName, String propertyName, int defaultValue);
	public List<String> getStringListProperty(String processStays, String propertyTitles);
}
