package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.InternetOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddInternetOrderRequest;
import com.aboutstays.response.dto.InternetOrderResponse;
import com.aboutstays.response.dto.UpdateInternetOrderRequest;

public interface InternetOrderService {
	
	public String add(AddInternetOrderRequest request);
	public InternetOrderResponse getById(String id);
	public List<InternetOrderResponse> getAll();
	public boolean delete(String id);
	public boolean updateNonNUll(UpdateInternetOrderRequest request);
	public boolean existsById(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	public InternetOrder getEntity(String id);
}
