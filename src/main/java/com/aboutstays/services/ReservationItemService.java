package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.ReservationItem;
import com.aboutstays.request.dto.AddReservationItemRequest;
import com.aboutstays.request.dto.UpdateReservationItemRequest;
import com.aboutstays.response.dto.ReservationItemResponse;

public interface ReservationItemService {
	
	public String save(AddReservationItemRequest request);
	public ReservationItemResponse getById(String id);
	public List<ReservationItemResponse> getAll();
	public void update(UpdateReservationItemRequest item);
	public boolean updateNonNull(UpdateReservationItemRequest item);
	public List<ReservationItemResponse> getAllByHotelId(String hotelId, String categoryId);
	public boolean existsById(String id);
	public ReservationItem getEntityById(String id);
	public void updateEntity(ReservationItem item);
	public boolean deleteById(String id);
	public boolean exists(String id, String categoryId, String hotelId);
}
