package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddLaundryOrderRequest;
import com.aboutstays.request.dto.UpdateLaundryOrderRequest;
import com.aboutstays.response.dto.GetLaundryOrderResponse;

public interface LaundryOrderService {

	public String save(AddLaundryOrderRequest request);
	public GetLaundryOrderResponse getById(String id);
	public List<GetLaundryOrderResponse> getAll();
	public List<GetLaundryOrderResponse> getAllByUserId(String userId);
	public List<GetLaundryOrderResponse> getAllByHotelId(String hotelId);
	public List<GetLaundryOrderResponse> getAllByUserAndStayId(String userId, String stayId);
	public void updateRequest(UpdateLaundryOrderRequest request);
	public boolean updateNonNullFieldsOfRequest(UpdateLaundryOrderRequest request);
	public boolean delete(String id);
	public boolean existsById(String id);
	public LaundryOrder getEntity(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	public boolean updatLaundryOrderRequest(LaundryOrder order);
}
