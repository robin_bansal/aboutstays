package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddMaintenanceOrderRequest;
import com.aboutstays.request.dto.UpdateMaintenanceOrderRequest;
import com.aboutstays.response.dto.GetMaintenanceOrderResponse;

public interface MaintenanceRequestService {

	public String save(AddMaintenanceOrderRequest request);
	public GetMaintenanceOrderResponse getById(String id);
	public List<GetMaintenanceOrderResponse> getAll();
	public boolean updateNonNullFieldsOfRequest(UpdateMaintenanceOrderRequest request);
	public boolean delete(String id);
	public boolean existsById(String id);
	public boolean changeOrderStatus(String id, GeneralOrderStatus status);
	public MaintenanceRequestEntity getEntity(String id);
}
