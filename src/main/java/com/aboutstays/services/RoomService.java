package com.aboutstays.services;

import java.util.List;

import com.aboutstays.pojos.RoomMetaInfo;
import com.aboutstays.request.dto.AddRoomRequest;
import com.aboutstays.request.dto.UpdateRoomRequest;
import com.aboutstays.response.dto.GetRoomResponse;

public interface RoomService {
	public String add(AddRoomRequest request);
	public List<GetRoomResponse> getAll();
	public GetRoomResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateRoomRequest request);
	public boolean addAdditionalAmenities(String roomId,List<String> amenityIds);
	public List<GetRoomResponse> getRoomsByRoomCategory(String categoryId);
	public List<GetRoomResponse> getRoomsByFloorId(String floorId);
	public List<GetRoomResponse> getRoomsByWingId(String wingId);
	public List<GetRoomResponse> getRoomsByHotelId(String hotelsId);
	public void deleteRooms(String hotelId, List<String> roomIds);
	public boolean exists(String hotelId, String roomId);
	public void updateRoomMetaInfo(RoomMetaInfo roomInfo);
}
