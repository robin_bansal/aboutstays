package com.aboutstays.services;

import java.util.List;

import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.EntertaimentItemTypeInfo;
import com.aboutstays.request.dto.AddEntertaimentServiceRequest;
import com.aboutstays.response.dto.GetEntertaimentServiceResponse;

public interface EntertaimentServicesService {

	public String save(AddEntertaimentServiceRequest request);
	public void addType(String id, EntertaimentItemTypeInfo typeInfo) throws AboutstaysException;
	public void removeType(String id, EntertaimentItemTypeInfo typeInfo) throws AboutstaysException;
	public List<GetEntertaimentServiceResponse> getAll();
	public GetEntertaimentServiceResponse getById(String id);
	public void delete(String id);
	public boolean existsById(String id);
}
