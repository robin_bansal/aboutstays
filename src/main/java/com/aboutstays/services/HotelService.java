package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.HotelUpdateBy;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.HotelAward;
import com.aboutstays.pojos.HotelInfoMetaData;
import com.aboutstays.request.dto.CreateHotelRequest;
import com.aboutstays.request.dto.GetHotelGeneralServices;
import com.aboutstays.request.dto.UpdateHotelRequest;
import com.aboutstays.request.dto.UpdateHotelServices;
import com.aboutstays.request.dto.UpdatePartnershipRequest;
import com.aboutstays.response.dto.CreateHotelResponse;
import com.aboutstays.response.dto.GetHotelBasicInfoList;
import com.aboutstays.response.dto.GetHotelGeneralInfoResponse;
import com.aboutstays.response.dto.GetHotelListResponse;
import com.aboutstays.response.dto.GetHotelResponse;

public interface HotelService {
	
	public CreateHotelResponse createHotel(CreateHotelRequest createHotelRequest);
	public GetHotelListResponse getAllHotels();
	public GetHotelListResponse getHotelListByType(int type);
	public GetHotelResponse getHotelById(String hotelId);
	public GetHotelGeneralInfoResponse getGeneralInfoById(String hotelId);
	public boolean hotelExistsById(String hotelId);
	public boolean updateHotelByType(HotelUpdateBy type, String value, UpdateHotelRequest updateHotelRequest);
	public boolean deleteHotelById(String hotelId);
	public void removeRoom(String hotelId, String roomId);
	public void addRoomToHotel(String hotelId, String roomId);
	public GetHotelBasicInfoList getHotelByCityAndName(String city, String name);
	public boolean updatePartnershipType(UpdatePartnershipRequest request);
	public GetHotelGeneralServices getGeneralServices(String hotelId, StaysStatus statusEnum, Boolean isGs);
	public Boolean updateServiceDetails(UpdateHotelServices request, StaysStatus statusEnum);
	public void updateAmenities(String hotelId, List<String> amenityIds);
	public void updateEssentials(String hotelId, List<String> assentialIds);
	public void updateAwards(String hotelId, HotelAward awardRequestPojo)throws AboutstaysException;
	public boolean deleteAwards(String hotelId, HotelAward awardRequestPojo)throws AboutstaysException;
	public void updateCardAccepted(String hotelId, List<String> cardIds);
	public void updateLoyaltyPrograms(String hotelId, String loyaltyProgramsId)throws AboutstaysException;
	public boolean deleteLoyaltyProgram(String hotelId, String loyaltyProgramsId)throws AboutstaysException;
	public long getTimezoneDifferenceInMillis(String hotelId);
	public boolean existsInHotel(String hotelId, String roomId);
	
	public HotelInfoMetaData getHotelInfoMetaData(String hotelId);
	public boolean isHotelInfoExist(GetHotelResponse hotelResponse);
	public boolean isLocationAndContactsExist(GetHotelResponse hotelResponse);
	public boolean isAmenitiesExist(GetHotelResponse hotelResponse);
	public boolean isEssentilasExist(GetHotelResponse hotelResponse);
	public boolean isAwardsExist(GetHotelResponse hotelResponse);
	public boolean isCardsAcceptedExist(GetHotelResponse hotelResponse);
	public boolean isLoyaltyProgramExist(GetHotelResponse getHotelResponse);
	public boolean isHouseRulesExist(String hotelId);
}
