package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.CityGuideItemEntity;
import com.aboutstays.enums.CityGuideItemType;
import com.aboutstays.request.dto.AddCityGuideItemRequest;
import com.aboutstays.request.dto.UpdateCGItemRequest;
import com.aboutstays.response.dto.GetCityGuideItemResponse;

public interface CityGuideItemService {

	public String save(AddCityGuideItemRequest request);
	public GetCityGuideItemResponse getById(String id);
	public List<GetCityGuideItemResponse> getAllCGItems();
	public void update(UpdateCGItemRequest request);
	public boolean updateNonNull(UpdateCGItemRequest request);
	public List<GetCityGuideItemResponse> getAllByHotelId(String hotelId);
	public List<GetCityGuideItemResponse> getAllByCGId(String cgId);
	public boolean existsById(String id);
	public CityGuideItemEntity getEntityById(String id);
	public void updateEntity(CityGuideItemEntity entity);
	public List<GetCityGuideItemResponse> getAllByType(String cgId, CityGuideItemType cgItemType);
	public boolean delete(String id);
}
