package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.TimeZoneDetails;

public interface TimezoneService {

	public String saveTimezone(TimeZoneDetails timeZoneDetails);
	public TimeZoneDetails getTimezoneById(String id);
	public TimeZoneDetails getTimezoneDetailsByTimezone(String timezone);
	public List<TimeZoneDetails> getAll();
	public long getTimeDifferenceInMillis(String timezone);
	public boolean existsByTimezone(String timezone);
	public boolean existsById(String id);
	public void deleteTimezone(String id);
	
}
