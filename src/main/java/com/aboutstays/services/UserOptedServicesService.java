package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.UserOptedService;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.GetUOSByUserRequest;
import com.aboutstays.request.dto.UpdateUserOptedServiceRequest;
import com.aboutstays.response.dto.AddUserOptedServiceResposne;
import com.aboutstays.response.dto.GetUserOptedServiceResponse;

public interface UserOptedServicesService {
	
	public AddUserOptedServiceResposne add(AddUserOptedServiceRequest request);
	public List<GetUserOptedServiceResponse> getAll(String userId);
	public GetUserOptedServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateUserOptedServiceRequest request);
	public boolean updateById(UserOptedService request);
	public List<GetUserOptedServiceResponse> getByUserAndStay(GetUOSByUserRequest request);
	public List<GetUserOptedServiceResponse> getByUserStaysHotel(UserStayHotelIdsPojo request) throws AboutstaysException;
	public List<UserOptedService> getByUserStaysAndHotel(String staysId, String userId, String hotelId);
	public UserOptedService getEntity(String id);
	public UserOptedService getByRefId(String refId);
	public List<GetUserOptedServiceResponse> getAllByHotelAndDate(String hotelId, String date);
	public List<GetUserOptedServiceResponse> getAllByHotelAndDateAndStatus(String hotelId, String date, Integer code);
}
