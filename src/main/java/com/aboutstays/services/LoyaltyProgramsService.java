package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddLoyaltyRequest;
import com.aboutstays.response.dto.AddLoyaltyProgramResponse;
import com.aboutstays.response.dto.GetLoyaltyProgramResponse;
import com.aboutstays.response.dto.UpdateLoyaltyProgram;

public interface LoyaltyProgramsService {
	public AddLoyaltyProgramResponse add(AddLoyaltyRequest request);
	public List<GetLoyaltyProgramResponse> getAll();
	public GetLoyaltyProgramResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateLoyaltyProgram request);
}
