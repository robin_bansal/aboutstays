package com.aboutstays.services;

import java.util.List;

import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.AddStaysFeedbackRequest;
import com.aboutstays.request.dto.UpdateStaysFeedbackRequest;
import com.aboutstays.response.dto.StaysFeedbackResponse;

public interface StaysFeedbackService {
	
	public String add(AddStaysFeedbackRequest request);
	public StaysFeedbackResponse getById(String id);
	public List<StaysFeedbackResponse> getAll();
	public List<StaysFeedbackResponse> getAllByUser(String userId);
	public List<StaysFeedbackResponse> getAllByHotel(String hotelId);
	public List<StaysFeedbackResponse> getAllOfUserStay(String userId, String staysId);
	public boolean updateNonNull(UpdateStaysFeedbackRequest request);
	public boolean update(UpdateStaysFeedbackRequest request);
	public boolean existsById(String id);
	public boolean delete(String id);
	public StaysFeedbackResponse getAllByUserHotelStays(UserStayHotelIdsPojo request);
}
