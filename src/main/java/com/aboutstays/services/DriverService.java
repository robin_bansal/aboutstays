package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddDriverRequest;
import com.aboutstays.request.dto.UpdateDriverRequest;
import com.aboutstays.response.dto.AddDriverResponse;
import com.aboutstays.response.dto.GetDriverResponse;

public interface DriverService {
	
	public AddDriverResponse add(AddDriverRequest request);
	public List<GetDriverResponse> getAll();
	public GetDriverResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateDriverRequest request);

}
