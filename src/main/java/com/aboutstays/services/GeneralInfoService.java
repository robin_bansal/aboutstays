package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddGeneralInfoRequest;
import com.aboutstays.request.dto.UpdateGeneralInfoRequest;
import com.aboutstays.response.dto.GetGeneralInfoResponse;

public interface GeneralInfoService {

	public String add(AddGeneralInfoRequest request);
	public List<GetGeneralInfoResponse> getAll();
	public List<GetGeneralInfoResponse> getAllByStaysStatus(Integer status);
	public GetGeneralInfoResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateGeneralInfoRequest request);
}
