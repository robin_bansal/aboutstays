package com.aboutstays.services;

import java.util.List;

import com.aboutstays.entities.HousekeepingItem;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.request.dto.HousekeepingItemRequest;
import com.aboutstays.response.dto.HousekeepingItemResponse;

public interface HousekeepingItemService {
	
	public String save(HousekeepingItemRequest request);
	public HousekeepingItemResponse getById(String id);
	public List<HousekeepingItemResponse> getAllHousekeepingItems();
	public void update(HousekeepingItemRequest foodItemRequest);
	public void updateNonNull(HousekeepingItemRequest foodItemRequest);
	public List<HousekeepingItemResponse> getAllByHotelId(String hotelId);
	public List<HousekeepingItemResponse> getAllByHousekeepingServiceId(String hkServicesId);
	public boolean existsById(String hkItemId);
	public List<HousekeepingItemResponse> searchByQueryString(String id, String query);
	public HousekeepingItem getEntityById(String hkItemId);
	public void updateEntity(HousekeepingItem hkItem);
	public List<HousekeepingItemResponse> getAllByType(String hkServiceId, HousekeepingItemType hkItemType);
	public boolean deleteById(String id);

}
