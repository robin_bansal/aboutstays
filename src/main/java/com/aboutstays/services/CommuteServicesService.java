package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddCommuteServiceRequest;
import com.aboutstays.request.dto.UpdateCommuteServiceRequest;
import com.aboutstays.response.dto.GetCommuteServiceResponse;

public interface CommuteServicesService {
	
	public String add(AddCommuteServiceRequest request);
	public List<GetCommuteServiceResponse> getAll();
	public GetCommuteServiceResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);
	public boolean updateById(UpdateCommuteServiceRequest request);

}
