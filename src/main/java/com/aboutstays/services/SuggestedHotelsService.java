package com.aboutstays.services;

import java.util.List;

import com.aboutstays.request.dto.AddSuggestedHotelRequest;
import com.aboutstays.response.dto.SuggestedHotelsResponse;

public interface SuggestedHotelsService {
	
	public String add(AddSuggestedHotelRequest request);
	public List<SuggestedHotelsResponse> getAll();
	public SuggestedHotelsResponse getById(String id);
	public boolean existsById(String id);
	public boolean deleteById(String id);

}
