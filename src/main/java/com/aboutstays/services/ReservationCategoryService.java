package com.aboutstays.services;

import java.util.List;

import com.aboutstays.enums.ReservationType;
import com.aboutstays.request.dto.AddReservationCategoryRequest;
import com.aboutstays.request.dto.UpdateReservationCategoryRequest;
import com.aboutstays.response.dto.GetReservationCategoryResponse;

public interface ReservationCategoryService {

	public String addCategory(AddReservationCategoryRequest request);
	public boolean exists(String subCategoryId);
	public GetReservationCategoryResponse getById(String subCategoryId);
	public void update(UpdateReservationCategoryRequest request);
	public void updateNonNull(UpdateReservationCategoryRequest request);
	public List<GetReservationCategoryResponse> getAll(String hotelId, ReservationType reservationType);
	public void delete(String subCategoryId);
	public boolean exists(String categoryId, String hotelId);
	
}
