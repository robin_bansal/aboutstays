package com.aboutstays.exception;

public enum CommonExceptionCode {
	
	//AWS related
	S3_CLIENT_UNINTRESTED("S3 Client is found null", "NULL_S3_CLI");
	
	private String message;
	private String code;
	
	private CommonExceptionCode(String message, String code) {
		this.message = message;
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public String getCode() {
		return code;
	}
	
	

}
