package com.aboutstays.exception;

public class AWSAccessException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;

	public String getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public AWSAccessException(){
		super();
	}
	
	public AWSAccessException(String message){
		super(message);
	}
	
	public AWSAccessException(Throwable cause){
		super(cause);
	}

	public AWSAccessException(CommonExceptionCode commonExceptionCode){
		super(commonExceptionCode.getMessage());
		this.code = commonExceptionCode.getCode();
	}
}
