package com.aboutstays.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.response.dto.BaseApiResponse;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(value=AboutstaysException.class)
	@ResponseBody
	public <T> BaseApiResponse<T> handleAboutstaysException(AboutstaysException ce){
		ce.printStackTrace();
		return new BaseApiResponse<T>(true, ce.getCode(), null, ce.getMessage());
	}
	
	@ExceptionHandler(value=Exception.class)
	@ResponseBody
	public <T> BaseApiResponse<T> handleAboutstaysException(Exception e){
		e.printStackTrace();
		return new BaseApiResponse<T>(true, null, AboutstaysResponseCode.INTERENAL_SERVER_ERROR, e.getMessage());
	}

}
