package com.aboutstays.exception;

import com.aboutstays.enums.AboutstaysResponseCode;

public class AboutstaysException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String detailMessage;
	
	public AboutstaysException(){};
	
	public AboutstaysException(String message, String code, String detailMessage) {
		super(message);
		this.code = code;
		this.detailMessage = detailMessage;
	}

	public AboutstaysException(String message, String code) {
		super(message);
		this.code = code;
	}
	
	public AboutstaysException(AboutstaysResponseCode aboutstaysResponseCode){
		super(aboutstaysResponseCode.getMessage());
		this.code = aboutstaysResponseCode.getCode();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}
	
	
	
	
	

}
