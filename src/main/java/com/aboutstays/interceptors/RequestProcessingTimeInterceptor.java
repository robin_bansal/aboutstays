package com.aboutstays.interceptors;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.aboutstays.utils.CollectionUtils;


public class RequestProcessingTimeInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = Logger.getLogger(RequestProcessingTimeInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		long startTime = System.currentTimeMillis();
		logger.info("Request URL::" + request.getRequestURL().toString()
				+ ":: Start Time=" + System.currentTimeMillis());
		ResettableStreamHttpServletRequest myRequestWrapper = new ResettableStreamHttpServletRequest((HttpServletRequest) request);

		String clientIp = myRequestWrapper.getRemoteHost();
		int clientPort = myRequestWrapper.getRemotePort();
		logger.info("Client Address:: "+clientIp+":"+clientPort);
		
		myRequestWrapper.setAttribute("startTime", startTime);
		try{
			if(CollectionUtils.isMapNotEmpty(myRequestWrapper.getParameterMap())){
				StringBuilder sb = new StringBuilder("Request params:: {");
				for(Map.Entry<String, String[]> paramEntry : myRequestWrapper.getParameterMap().entrySet()){
					sb.append(paramEntry.getKey()+":"+Arrays.asList(paramEntry.getValue()).toString()+"; ");
				}
				sb.append("}");
				logger.info(sb.toString());
			}
//			String body = IOUtils.toString(myRequestWrapper.getReader());
//			logger.info("Request Body:: "+body);
			
//			InputStreamReader isr = new InputStreamReader(request.getInputStream());
//			BufferedReader br = new BufferedReader(isr);
//			String line = "";
//			StringBuilder sb = new StringBuilder();
//			while((line=br.readLine())!=null){
//				sb.append(line);
//			}
//			logger.info("Request body::" + sb.toString());
		} catch (Exception e){
			
		}
		//if returned false, we need to make sure 'response' is sent
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		long startTime = (Long) request.getAttribute("startTime");
		logger.info("Request URL::" + request.getRequestURL().toString()
				+ ":: End Time=" + System.currentTimeMillis());
		logger.info("Request URL::" + request.getRequestURL().toString()
				+ ":: Time Taken=" + (System.currentTimeMillis() - startTime));
	}

}