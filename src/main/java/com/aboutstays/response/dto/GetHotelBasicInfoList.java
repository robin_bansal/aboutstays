package com.aboutstays.response.dto;

import java.util.List;

public class GetHotelBasicInfoList {
	
	private List<HotelBasicInfoPojo> hotelBasicInfoList;

	public GetHotelBasicInfoList(){};
	
	public GetHotelBasicInfoList(List<HotelBasicInfoPojo> hotelBasicInfoList) {
		this.hotelBasicInfoList = hotelBasicInfoList;
	}

	public List<HotelBasicInfoPojo> getHotelBasicInfoList() {
		return hotelBasicInfoList;
	}

	public void setHotelBasicInfoList(List<HotelBasicInfoPojo> hotelBasicInfoList) {
		this.hotelBasicInfoList = hotelBasicInfoList;
	}
	
	
	
	

}
