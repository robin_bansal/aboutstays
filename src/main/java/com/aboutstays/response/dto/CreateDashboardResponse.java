package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.GeneralDashboardItem;

public class CreateDashboardResponse {

	private List<GeneralDashboardItem> openDashboardItems;
	private List<GeneralDashboardItem> scheduledDashboardItems;
	private List<GeneralDashboardItem> closedDashboardItems;
	public List<GeneralDashboardItem> getOpenDashboardItems() {
		return openDashboardItems;
	}
	public void setOpenDashboardItems(List<GeneralDashboardItem> openDashboardItems) {
		this.openDashboardItems = openDashboardItems;
	}
	public List<GeneralDashboardItem> getScheduledDashboardItems() {
		return scheduledDashboardItems;
	}
	public void setScheduledDashboardItems(List<GeneralDashboardItem> scheduledDashboardItems) {
		this.scheduledDashboardItems = scheduledDashboardItems;
	}
	public List<GeneralDashboardItem> getClosedDashboardItems() {
		return closedDashboardItems;
	}
	public void setClosedDashboardItems(List<GeneralDashboardItem> closedDashboardItems) {
		this.closedDashboardItems = closedDashboardItems;
	}

	
	
	
	
}
