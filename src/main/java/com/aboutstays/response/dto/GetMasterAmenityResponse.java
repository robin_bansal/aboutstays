package com.aboutstays.response.dto;

import com.aboutstays.pojos.MasterAmenityPojo;

public class GetMasterAmenityResponse extends MasterAmenityPojo {

	private String amenityId;
	
	public String getAmenityId() {
		return amenityId;
	}
	
	public void setAmenityId(String amenityId) {
		this.amenityId = amenityId;
	}
}
