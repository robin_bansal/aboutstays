package com.aboutstays.response.dto;

import com.aboutstays.pojos.CheckoutPojo;

public class CheckoutResponse extends CheckoutPojo{

	private String id;
	private int paymentMethod;
	private boolean checkedOutEarlier;
	private Long created;
	private Long updated;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public int getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public boolean isCheckedOutEarlier() {
		return checkedOutEarlier;
	}

	public void setCheckedOutEarlier(boolean checkedOutEarlier) {
		this.checkedOutEarlier = checkedOutEarlier;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}


	
	
}
