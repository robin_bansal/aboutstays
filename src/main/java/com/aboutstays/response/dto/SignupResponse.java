package com.aboutstays.response.dto;

public class SignupResponse {
	
	private String userID;

	public SignupResponse(String userID) {
		this.userID = userID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}


}
