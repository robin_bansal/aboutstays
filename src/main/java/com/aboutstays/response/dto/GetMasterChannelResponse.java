package com.aboutstays.response.dto;

import com.aboutstays.pojos.MasterChannelItemPojo;

public class GetMasterChannelResponse extends MasterChannelItemPojo {

	private String channelId;
	private GetMasterGenereResponse genereDetails;
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public GetMasterGenereResponse getGenereDetails() {
		return genereDetails;
	}
	public void setGenereDetails(GetMasterGenereResponse genereDetails) {
		this.genereDetails = genereDetails;
	}
	
}
