package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.LaundryItemOrderDetails;

public class LaundryItemCartResponse extends UserCartResponse{
	
	private List<LaundryItemOrderDetails> listLaundryItemOrderDetails;

	public List<LaundryItemOrderDetails> getListLaundryItemOrderDetails() {
		return listLaundryItemOrderDetails;
	}

	public void setListLaundryItemOrderDetails(List<LaundryItemOrderDetails> listLaundryItemOrderDetails) {
		this.listLaundryItemOrderDetails = listLaundryItemOrderDetails;
	}
	
	

}
