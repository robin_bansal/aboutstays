package com.aboutstays.response.dto;

import com.aboutstays.pojos.UserStayHotelIdsPojo;

public class UserCartResponse extends UserStayHotelIdsPojo{

	private int cartType;
	public int getCartType() {
		return cartType;
	}
	public void setCartType(int cartType) {
		this.cartType = cartType;
	}
	
}
