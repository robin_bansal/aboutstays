package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.CategoryExpenseItem;

public class GetExpenseByCategoryResponse {
	
	private List<CategoryExpenseItem> listServiceExpense;
	private Double grandTotal;
	public List<CategoryExpenseItem> getListServiceExpense() {
		return listServiceExpense;
	}
	public void setListServiceExpense(List<CategoryExpenseItem> listServiceExpense) {
		this.listServiceExpense = listServiceExpense;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
	

}
