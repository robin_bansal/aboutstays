package com.aboutstays.response.dto;

import com.aboutstays.enums.GetStayResponse;

public class GetIncompleteBookingsResponce extends GetBookingReponse{
	
	private GetStayResponse staysData;
	private CheckinResponse checkinData;
	
	public GetStayResponse getStaysData() {
		return staysData;
	}
	public void setStaysData(GetStayResponse staysData) {
		this.staysData = staysData;
	}
	public CheckinResponse getCheckinData() {
		return checkinData;
	}
	public void setCheckinData(CheckinResponse checkinData) {
		this.checkinData = checkinData;
	}
	
}
