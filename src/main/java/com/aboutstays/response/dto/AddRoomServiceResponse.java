package com.aboutstays.response.dto;

public class AddRoomServiceResponse {
	
	private String id;
	
	public AddRoomServiceResponse() {}


	public AddRoomServiceResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	

}
