package com.aboutstays.response.dto;

import com.aboutstays.pojos.PlacePojo;

public class PlaceResponse extends PlacePojo {

	private String placeId;
	
	public String getPlaceId() {
		return placeId;
	}
	
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
}
