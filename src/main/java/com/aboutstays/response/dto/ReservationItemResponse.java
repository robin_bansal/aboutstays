package com.aboutstays.response.dto;

import com.aboutstays.pojos.ReservationItemPojo;

public class ReservationItemResponse extends ReservationItemPojo{

	private String id;
	private String categoryId;
	private String hotelId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	
	
}
