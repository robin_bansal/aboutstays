package com.aboutstays.response.dto;

import com.aboutstays.pojos.CommuteOrderPojo;

public class GetCommuteOrderResponse extends CommuteOrderPojo{

	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
