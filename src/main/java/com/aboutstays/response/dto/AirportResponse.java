package com.aboutstays.response.dto;

import com.aboutstays.pojos.AirportPojo;

public class AirportResponse extends AirportPojo {

	private String airportId;
	private String cityId;
	
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
	public String getAirportId() {
		return airportId;
	}
	public void setAirportId(String airportId) {
		this.airportId = airportId;
	}
	
}
