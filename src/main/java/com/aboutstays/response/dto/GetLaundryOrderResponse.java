package com.aboutstays.response.dto;

import com.aboutstays.pojos.LaundryOrderPojo;

public class GetLaundryOrderResponse extends LaundryOrderPojo{
	
	private String id;
	private int status;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	

}
