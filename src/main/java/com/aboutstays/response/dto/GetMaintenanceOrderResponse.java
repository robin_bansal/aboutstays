package com.aboutstays.response.dto;

import com.aboutstays.pojos.MaintenanceRequestPojo;

public class GetMaintenanceOrderResponse extends MaintenanceRequestPojo{
	
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
