package com.aboutstays.response.dto;

import java.util.List;

public class GetAllMasterCityResponse {

	private List<GetMasterCityResponse> cities;
	
	public List<GetMasterCityResponse> getCities() {
		return cities;
	}
	
	public void setCities(List<GetMasterCityResponse> cities) {
		this.cities = cities;
	}
}
