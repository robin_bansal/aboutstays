package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.Amenity;
import com.aboutstays.pojos.RoomPojo;

public class GetRoomResponse extends RoomPojo {
	
	private String roomId;
	private List<Amenity> additionalAmenities;
	private GetRoomCategoryResponse roomCategoryData;
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public List<Amenity> getAdditionalAmenities() {
		return additionalAmenities;
	}
	public void setAdditionalAmenities(List<Amenity> additionalAmenities) {
		this.additionalAmenities = additionalAmenities;
	}
	public GetRoomCategoryResponse getRoomCategoryData() {
		return roomCategoryData;
	}
	public void setRoomCategoryData(GetRoomCategoryResponse roomCategoryData) {
		this.roomCategoryData = roomCategoryData;
	}
}
