package com.aboutstays.response.dto;

public class OtpResponse {
		
	private String otp;
	private boolean debugMode;
	
	public OtpResponse(){};
	public OtpResponse(String otp) {
		this.otp = otp;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	public boolean isDebugMode() {
		return debugMode;
	}
	
	public void setDebugMode(boolean debugMode) {
		this.debugMode = debugMode;
	}
	
}
