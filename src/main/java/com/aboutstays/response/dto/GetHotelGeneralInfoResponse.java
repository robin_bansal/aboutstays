package com.aboutstays.response.dto;

import com.aboutstays.pojos.HotelGeneralInformation;

public class GetHotelGeneralInfoResponse {
	
	private String hotelId;
	private Integer hotelType;
	private HotelGeneralInformation generalInformation;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getHotelType() {
		return hotelType;
	}
	public void setHotelType(Integer hotelType) {
		this.hotelType = hotelType;
	}
	public HotelGeneralInformation getGeneralInformation() {
		return generalInformation;
	}
	public void setGeneralInformation(HotelGeneralInformation generalInformation) {
		this.generalInformation = generalInformation;
	}
	
	

}
