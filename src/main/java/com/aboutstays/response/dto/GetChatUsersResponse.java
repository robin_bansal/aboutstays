package com.aboutstays.response.dto;

import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.pojos.UserPojo;

public class GetChatUsersResponse {
	private UserPojo userData;
	private GetStayResponse staysData;
	private int unreadMessageCount;

	public GetChatUsersResponse() {
	}

	public GetChatUsersResponse(UserPojo userData, GetStayResponse staysData, int unreadMessageCount) {
		super();
		this.userData = userData;
		this.staysData = staysData;
		this.unreadMessageCount = unreadMessageCount;
	}

	public UserPojo getUserData() {
		return userData;
	}

	public void setUserData(UserPojo userData) {
		this.userData = userData;
	}

	public int getUnreadMessageCount() {
		return unreadMessageCount;
	}

	public void setUnreadMessageCount(int unreadMessageCount) {
		this.unreadMessageCount = unreadMessageCount;
	}

	public GetStayResponse getStaysData() {
		return staysData;
	}

	public void setStaysData(GetStayResponse staysData) {
		this.staysData = staysData;
	}
}
