package com.aboutstays.response.dto;

import com.aboutstays.pojos.HotelGeneralInformation;

public class HotelBasicInfoPojo {
	
	private String hotelId;
	private Integer partnershipType;
	private HotelGeneralInformation generalInformation;
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getPartnershipType() {
		return partnershipType;
	}
	public void setPartnershipType(Integer partnershipType) {
		this.partnershipType = partnershipType;
	}
	public HotelGeneralInformation getGeneralInformation() {
		return generalInformation;
	}
	public void setGeneralInformation(HotelGeneralInformation generalInformation) {
		this.generalInformation = generalInformation;
	}
	
	

}
