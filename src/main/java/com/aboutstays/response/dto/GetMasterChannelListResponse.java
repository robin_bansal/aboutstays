package com.aboutstays.response.dto;

import java.util.List;

public class GetMasterChannelListResponse {

	private List<GetMasterChannelResponse> channels;
	
	public List<GetMasterChannelResponse> getChannels() {
		return channels;
	}
	
	public void setChannels(List<GetMasterChannelResponse> channels) {
		this.channels = channels;
	}
}
