package com.aboutstays.response.dto;

import com.aboutstays.request.dto.AddFloorRequest;

public class GetFloorResponse extends AddFloorRequest{
	
	private String floorId;
	private int roomsCount;
	
	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
	
	public int getRoomsCount() {
		return roomsCount;
	}
	
	public void setRoomsCount(int roomsCount) {
		this.roomsCount = roomsCount;
	}

}
