package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.Amenity;
import com.aboutstays.pojos.RoomCategoryPojo;

public class GetRoomCategoryResponse extends RoomCategoryPojo{
	private String roomCategoryId;
	private List<Amenity> inRoomAmenities;
	private List<Amenity> preferenceBasedAmenities;
	public String getRoomCategoryId() {
		return roomCategoryId;
	}
	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}
	public List<Amenity> getInRoomAmenities() {
		return inRoomAmenities;
	}
	public void setInRoomAmenities(List<Amenity> inRoomAmenities) {
		this.inRoomAmenities = inRoomAmenities;
	}
	public List<Amenity> getPreferenceBasedAmenities() {
		return preferenceBasedAmenities;
	}
	public void setPreferenceBasedAmenities(List<Amenity> preferenceBasedAmenities) {
		this.preferenceBasedAmenities = preferenceBasedAmenities;
	}

}
