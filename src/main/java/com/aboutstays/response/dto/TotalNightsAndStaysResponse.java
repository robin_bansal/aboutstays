package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.StaysHotelPojo;

public class TotalNightsAndStaysResponse {
	
	private int nights;
	private int hotels;
	private List<StaysHotelPojo> hotelStaysList;
	

	public int getNights() {
		return nights;
	}
	public void setNights(int nights) {
		this.nights = nights;
	}
	public int getHotels() {
		return hotels;
	}
	public void setHotels(int hotels) {
		this.hotels = hotels;
	}
	public List<StaysHotelPojo> getHotelStaysList() {
		return hotelStaysList;
	}
	public void setHotelStaysList(List<StaysHotelPojo> hotelStaysList) {
		this.hotelStaysList = hotelStaysList;
	}
	
}
