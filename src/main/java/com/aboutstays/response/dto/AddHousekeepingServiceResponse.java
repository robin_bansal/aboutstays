package com.aboutstays.response.dto;

public class AddHousekeepingServiceResponse {

	private String id;

	public AddHousekeepingServiceResponse() {}
	
	public AddHousekeepingServiceResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
}
