package com.aboutstays.response.dto;

import com.aboutstays.request.dto.AddWingRequest;

public class GetWingResponse extends AddWingRequest {
	
	private String wingId;
	private int floorsCount;
	private int roomsCount;

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	
	public int getFloorsCount() {
		return floorsCount;
	}
	
	public void setFloorsCount(int floorsCount) {
		this.floorsCount = floorsCount;
	}
	
	public void setRoomsCount(int roomsCount) {
		this.roomsCount = roomsCount;
	}
	
	public int getRoomsCount() {
		return roomsCount;
	}
}
