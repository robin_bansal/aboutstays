package com.aboutstays.response.dto;

import java.util.Map;

public class GetBookedStaysResponse {

	private Map<String, Boolean> bookingIdsStaysExistingStatus;
	
	public GetBookedStaysResponse(Map<String, Boolean> bookingIdsStaysExistingStatus) {
		this.bookingIdsStaysExistingStatus = bookingIdsStaysExistingStatus;
	}

	public Map<String, Boolean> getBookingIdsStaysExistingStatus() {
		return bookingIdsStaysExistingStatus;
	}
	
	public void setBookingIdsStaysExistingStatus(Map<String, Boolean> bookingIdsStaysExistingStatus) {
		this.bookingIdsStaysExistingStatus = bookingIdsStaysExistingStatus;
	}
}
