package com.aboutstays.response.dto;

import com.aboutstays.pojos.MasterCityPojo;

public class GetMasterCityResponse extends MasterCityPojo{

	private String cityId;
	
	public String getCityId() {
		return cityId;
	}
	
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
}
