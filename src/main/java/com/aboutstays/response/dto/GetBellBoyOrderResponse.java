package com.aboutstays.response.dto;

import com.aboutstays.pojos.BellBoyOrderPojo;

public class GetBellBoyOrderResponse extends BellBoyOrderPojo {
	
	private String id;
	private String displayMsg;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDisplayMsg() {
		return displayMsg;
	}
	public void setDisplayMsg(String displayMsg) {
		this.displayMsg = displayMsg;
	}
	
	

}
