package com.aboutstays.response.dto;

public class AddAirportPickupResponse {
	
	private String id;

	public AddAirportPickupResponse() {}
	
	public AddAirportPickupResponse(String id) {
		this.id = id;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
	

}
