package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.GeneralNameImageInfo;

public class GetCommonDataResponse {
	
	List<GeneralNameImageInfo> listPreferneces;
	List<String> listTitles;
	List<String> listIdentityDocs;
	
	public List<GeneralNameImageInfo> getListPreferneces() {
		return listPreferneces;
	}
	public void setListPreferneces(List<GeneralNameImageInfo> listPreferneces) {
		this.listPreferneces = listPreferneces;
	}
	public List<String> getListTitles() {
		return listTitles;
	}
	public void setListTitles(List<String> listTitles) {
		this.listTitles = listTitles;
	}
	public List<String> getListIdentityDocs() {
		return listIdentityDocs;
	}
	public void setListIdentityDocs(List<String> listIdentityDocs) {
		this.listIdentityDocs = listIdentityDocs;
	}
	
	

}
