package com.aboutstays.response.dto;

public class CreateHotelResponse {
	
	public String hotelId;
	

	public CreateHotelResponse(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	
	

}
