package com.aboutstays.response.dto;

public class AddRoomResponse {
	private String roomId;

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

}
