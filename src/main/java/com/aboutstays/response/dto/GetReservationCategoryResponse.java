package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.DirectionDetail;
import com.aboutstays.pojos.ReservationCategoryPojo;

public class GetReservationCategoryResponse extends ReservationCategoryPojo {

	private String categoryId;
	private String reservationCategoryName;
	private List<DirectionDetail> directionList;
	
	public String getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getReservationCategoryName() {
		return reservationCategoryName;
	}
	
	public List<DirectionDetail> getDirectionList() {
		return directionList;
	}
	
	public void setDirectionList(List<DirectionDetail> directionList) {
		this.directionList = directionList;
	}
	
	public void setReservationCategoryName(String reservationCategoryName) {
		this.reservationCategoryName = reservationCategoryName;
	}
}
