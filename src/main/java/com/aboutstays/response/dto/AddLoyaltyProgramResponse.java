package com.aboutstays.response.dto;

public class AddLoyaltyProgramResponse {
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
