package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.Amenity;
import com.aboutstays.pojos.CardAccepted;
import com.aboutstays.pojos.Essential;
import com.aboutstays.pojos.HotelAwardResponse;
import com.aboutstays.pojos.HotelPojo;

public class GetHotelResponse extends HotelPojo {
	
	private List<Amenity> amenities;
	private List<HotelAwardResponse> listAwards;
	private List<Essential> essentials;
	private List<CardAccepted> listCardAccepted;
	private List<GetLoyaltyProgramResponse> listLoyaltyPrograms;
	
	public List<Essential> getEssentials() {
		return essentials;
	}
	public void setEssentials(List<Essential> essentials) {
		this.essentials = essentials;
	}
	public List<Amenity> getAmenities() {
		return amenities;
	}
	
	public void setAmenities(List<Amenity> amenities) {
		this.amenities = amenities;
	}

	public List<HotelAwardResponse> getListAwards() {
		return listAwards;
	}

	public void setListAwards(List<HotelAwardResponse> awards) {
		this.listAwards = awards;
	}
	public List<CardAccepted> getListCardAccepted() {
		return listCardAccepted;
	}
	public void setListCardAccepted(List<CardAccepted> cardsAccepted) {
		this.listCardAccepted = cardsAccepted;
	}
	public List<GetLoyaltyProgramResponse> getListLoyaltyPrograms() {
		return listLoyaltyPrograms;
	}
	public void setListLoyaltyPrograms(List<GetLoyaltyProgramResponse> listLoyaltyPrograms) {
		this.listLoyaltyPrograms = listLoyaltyPrograms;
	}
}
