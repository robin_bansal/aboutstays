package com.aboutstays.response.dto;

public class AddGeneralServiceResponse {

	private String id;
	
	public AddGeneralServiceResponse() {}

	public AddGeneralServiceResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
