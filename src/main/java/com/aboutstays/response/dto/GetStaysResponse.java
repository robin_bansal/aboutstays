package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.enums.GetStayResponse;

public class GetStaysResponse {
	
	private List<GetStayResponse> listStays;


	public GetStaysResponse(){};
	
	public GetStaysResponse(List<GetStayResponse> listStays) {
		this.listStays = listStays;
	}

	public List<GetStayResponse> getListStays() {
		return listStays;
	}

	public void setListStays(List<GetStayResponse> listStays) {
		this.listStays = listStays;
	}


	
	
	
	

}
