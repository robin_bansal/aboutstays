package com.aboutstays.response.dto;

import com.aboutstays.enums.AboutstaysResponseCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class BaseApiResponse<T> {
	
	private boolean error;
	private String code;
	private T data;
	private String message;
	private String detailMessage;
	
	public BaseApiResponse(){};
	
	public BaseApiResponse(boolean error, String code, T data, String message) {
		this.error = error;
		this.code = code;
		this.data = data;
		this.message = message;
	}
	
	public BaseApiResponse(boolean error, T data, AboutstaysResponseCode aboutstaysResponseCode){
		this.error = error;
		this.code = aboutstaysResponseCode.getCode();
		this.data = data;
		this.message = aboutstaysResponseCode.getMessage();
	}

	public BaseApiResponse(boolean error, T data, AboutstaysResponseCode aboutstaysResponseCode, String detailMessage){
		this.error = error;
		this.code = aboutstaysResponseCode.getCode();
		this.data = data;
		this.message = aboutstaysResponseCode.getMessage();
		this.detailMessage = detailMessage;
	}
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}

	
	
	

}
