package com.aboutstays.response.dto;

public class AddDriverResponse {

	private String id;

	public AddDriverResponse() {}
	
	public AddDriverResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
}
