package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.StaysHotelPojo;

public class GetStaysWithHotelInfoResponse{
	
	private List<StaysHotelPojo> hotelStaysList;
	
	public GetStaysWithHotelInfoResponse(List<StaysHotelPojo> hotelStaysList) {
		this.hotelStaysList = hotelStaysList;
	}
	public List<StaysHotelPojo> getHotelStaysList() {
		return hotelStaysList;
	}
	public void setHotelStaysList(List<StaysHotelPojo> hotelStaysList) {
		this.hotelStaysList = hotelStaysList;
	}
}
