package com.aboutstays.response.dto;

import com.aboutstays.pojos.UserPasswordPojo;

public class LoginResponse extends UserPasswordPojo {
	
	private String referralCode;
	private boolean otpValidated;
	private String token;
	private long created;
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public boolean isOtpValidated() {
		return otpValidated;
	}

	public void setOtpValidated(boolean otpValidated) {
		this.otpValidated = otpValidated;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}
	
	

}
