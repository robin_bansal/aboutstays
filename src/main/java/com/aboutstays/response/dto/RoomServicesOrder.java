package com.aboutstays.response.dto;

import com.aboutstays.entities.Booking;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.PanelStatus;
import com.aboutstays.pojos.UserPojo;

public class RoomServicesOrder {
	private GetRoomResponse roomData;
	private Booking bookingData;
	private UserPojo userData;
	private GetFoodOrderResponse foodOrderData;
	private int panelStatusType;
	private String panelStatusName;

	public GetRoomResponse getRoomData() {
		return roomData;
	}

	public void setRoomData(GetRoomResponse roomData) {
		this.roomData = roomData;
	}

	public Booking getBookingData() {
		return bookingData;
	}

	public void setBookingData(Booking bookingData) {
		this.bookingData = bookingData;
	}

	public UserPojo getUserData() {
		return userData;
	}

	public void setUserData(UserPojo userData) {
		this.userData = userData;
	}

	public GetFoodOrderResponse getFoodOrderData() {
		return foodOrderData;
	}

	public void setFoodOrderData(GetFoodOrderResponse foodOrderData) {
		this.foodOrderData = foodOrderData;
	}

	public int getPanelStatusType() {
		return panelStatusType;
	}

	public void setPanelStatusType(int panelStatusType) {
		this.panelStatusType = panelStatusType;
	}

	public String getPanelStatusName() {
		return panelStatusName;
	}

	public void setPanelStatusName(String panelStatusName) {
		this.panelStatusName = panelStatusName;
	}
	
	public void setStaus(Integer status) {
		GeneralOrderStatus orderStatus = GeneralOrderStatus.findByCode(status);
		if (orderStatus != null) {
			switch (orderStatus) {
			case INITIATED:
			case PENDING:
				panelStatusName=PanelStatus.PENDING.getType();
				panelStatusType=PanelStatus.PENDING.getCode();
				break;
			case COMPLETED:
			case CANCELLED:
				panelStatusName=PanelStatus.COMPLETED.getType();
				panelStatusType=PanelStatus.COMPLETED.getCode();
				break;
			case PROCESSING:
			case PREPAIRING:
			case UPDATED:
				panelStatusName=PanelStatus.ONGOING.getType();
				panelStatusType=PanelStatus.ONGOING.getCode();
				break;
			case SCHEDULED:
				panelStatusName=PanelStatus.SCHEDULED.getType();
				panelStatusType=PanelStatus.SCHEDULED.getCode();
				break;
			}
		}
	}
}
