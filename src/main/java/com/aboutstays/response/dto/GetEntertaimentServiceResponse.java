package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.EntertaimentItemTypeInfoPojo;
import com.aboutstays.pojos.EntertaimentServicePojo;

public class GetEntertaimentServiceResponse extends EntertaimentServicePojo {

	private List<EntertaimentItemTypeInfoPojo> typeInfoList;
	
	public List<EntertaimentItemTypeInfoPojo> getTypeInfoList() {
		return typeInfoList;
	}
	public void setTypeInfoList(List<EntertaimentItemTypeInfoPojo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}
	
}
