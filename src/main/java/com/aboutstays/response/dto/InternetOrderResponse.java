package com.aboutstays.response.dto;

import com.aboutstays.pojos.InternetOrderPojo;

public class InternetOrderResponse extends InternetOrderPojo{
	
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
