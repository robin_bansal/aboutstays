package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.UserStayHotelIdsPojo;

public class GetStaysServiceListResponse extends UserStayHotelIdsPojo{

	private List<GetStaysServiceResponse> servicesList;
	private String checkinButtonText;
	private boolean showCheckInButton;
	private boolean showCheckoutButton;
	
	public String getCheckinButtonText() {
		return checkinButtonText;
	}
	
	public void setCheckinButtonText(String checkinButtonText) {
		this.checkinButtonText = checkinButtonText;
	}
	
	public List<GetStaysServiceResponse> getServicesList() {
		return servicesList;
	}
	
	public void setServicesList(List<GetStaysServiceResponse> servicesList) {
		this.servicesList = servicesList;
	}

	public boolean isShowCheckInButton() {
		return showCheckInButton;
	}

	public void setShowCheckInButton(boolean showCheckInButton) {
		this.showCheckInButton = showCheckInButton;
	}

	public boolean isShowCheckoutButton() {
		return showCheckoutButton;
	}

	public void setShowCheckoutButton(boolean showCheckoutButton) {
		this.showCheckoutButton = showCheckoutButton;
	}
	
}
