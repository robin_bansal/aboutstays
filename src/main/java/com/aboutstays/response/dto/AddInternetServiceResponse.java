package com.aboutstays.response.dto;

public class AddInternetServiceResponse {
	
	private String id;
	
	public AddInternetServiceResponse() {}

	public AddInternetServiceResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	

}
