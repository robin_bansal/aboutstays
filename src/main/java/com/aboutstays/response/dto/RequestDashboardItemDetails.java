package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.GeneralItemDetails;
import com.aboutstays.pojos.RequestDetailsTableEntries;

public class RequestDashboardItemDetails {
	
	private String uosId;
	private long requestedTime;
	private long lastUpdatedTime;
	private String requestType;
	private String requestStatus;
	private List<GeneralItemDetails> listGeneralItemDetails;
	private double total;
	private double serviceCharge;
	private long scheduledTime;
	private boolean editable;
	private RequestDetailsTableEntries tableEntries;
	
	
	public String getUosId() {
		return uosId;
	}
	public void setUosId(String uosId) {
		this.uosId = uosId;
	}
	public long getRequestedTime() {
		return requestedTime;
	}
	public void setRequestedTime(long requestedTime) {
		this.requestedTime = requestedTime;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public List<GeneralItemDetails> getListGeneralItemDetails() {
		return listGeneralItemDetails;
	}
	public void setListGeneralItemDetails(List<GeneralItemDetails> listGeneralItemDetails) {
		this.listGeneralItemDetails = listGeneralItemDetails;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public long getScheduledTime() {
		return scheduledTime;
	}
	public void setScheduledTime(long scheduledTime) {
		this.scheduledTime = scheduledTime;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	public RequestDetailsTableEntries getTableEntries() {
		return tableEntries;
	}
	
	public void setTableEntries(RequestDetailsTableEntries tableEntries) {
		this.tableEntries = tableEntries;
	}

}
