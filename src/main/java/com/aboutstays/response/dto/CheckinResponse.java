package com.aboutstays.response.dto;

import com.aboutstays.pojos.CheckinPojo;

public class CheckinResponse extends CheckinPojo {

	private Long created;
	private Long updated;
	private int status;
	
	public Long getCreated() {
		return created;
	}
	
	public void setCreated(Long created) {
		this.created = created;
	}
	
	public Long getUpdated() {
		return updated;
	}
	
	public void setUpdated(Long updated) {
		this.updated = updated;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
