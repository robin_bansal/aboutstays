package com.aboutstays.response.dto;

import com.aboutstays.pojos.LoyaltyProgramPojo;

public class UpdateLoyaltyProgram extends LoyaltyProgramPojo{
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
