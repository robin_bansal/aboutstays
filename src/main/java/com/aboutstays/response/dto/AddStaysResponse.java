package com.aboutstays.response.dto;

public class AddStaysResponse {

	private String staysId;
	
	public AddStaysResponse(){};

	public AddStaysResponse(String staysId) {
		this.staysId = staysId;
	}

	public String getStaysId() {
		return staysId;
	}

	public void setStaysId(String staysId) {
		this.staysId = staysId;
	}
	
	
}
