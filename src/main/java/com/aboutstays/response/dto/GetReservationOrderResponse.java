package com.aboutstays.response.dto;

import com.aboutstays.pojos.ReservationOrderPojo;

public class GetReservationOrderResponse extends ReservationOrderPojo{
	
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
