package com.aboutstays.response.dto;

import java.util.List;

public class GetMessageListResponse {

	private List<GetMessageResponse> messages;
	
	public List<GetMessageResponse> getMessages() {
		return messages;
	}
	
	public void setMessages(List<GetMessageResponse> messages) {
		this.messages = messages;
	}
}
