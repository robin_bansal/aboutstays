package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.HousekeepingItemOrderDetails;

public class HKItemCartResponse extends UserCartResponse{
	
	private List<HousekeepingItemOrderDetails> hkItemOrderDetailsList;

	public List<HousekeepingItemOrderDetails> getHkItemOrderDetailsList() {
		return hkItemOrderDetailsList;
	}

	public void setHkItemOrderDetailsList(List<HousekeepingItemOrderDetails> hkItemOrderDetailsList) {
		this.hkItemOrderDetailsList = hkItemOrderDetailsList;
	}
	
	

}
