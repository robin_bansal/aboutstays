package com.aboutstays.response.dto;

import com.aboutstays.pojos.MasterGenereItemPojo;

public class GetMasterGenereResponse extends MasterGenereItemPojo {

	private String masterGenereId;
	
	public String getMasterGenereId() {
		return masterGenereId;
	}
	
	public void setMasterGenereId(String masterGenereId) {
		this.masterGenereId = masterGenereId;
	}
}
