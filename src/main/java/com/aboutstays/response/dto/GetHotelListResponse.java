package com.aboutstays.response.dto;

import java.util.List;
import com.aboutstays.pojos.HotelPojo;

public class GetHotelListResponse {
	
	private List<HotelPojo> listHotels;
	
	public GetHotelListResponse(){};
	
	public GetHotelListResponse(List<HotelPojo> listHotels) {
		this.listHotels = listHotels;
	}

	public List<HotelPojo> getListHotels() {
		return listHotels;
	}

	public void setListHotels(List<HotelPojo> listHotels) {
		this.listHotels = listHotels;
	}

}
