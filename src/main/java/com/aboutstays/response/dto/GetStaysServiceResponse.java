package com.aboutstays.response.dto;

public class GetStaysServiceResponse{

	private String gsId;
	private String serviceName;
	private String serviceImageUrl;
	private Integer serviceType;
	private boolean clickable;
	private boolean showTick;
	private boolean showNumber;
	private int count;
	private boolean generalInfo;
	private int uiOrder;
	private int staysStatus;
	private String serviceId;
	private String relevantUosId;
	private String referenceId;
	
	public String getGsId() {
		return gsId;
	}
	public void setGsId(String gsId) {
		this.gsId = gsId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceImageUrl() {
		return serviceImageUrl;
	}
	public void setServiceImageUrl(String serviceImageUrl) {
		this.serviceImageUrl = serviceImageUrl;
	}
	public Integer getServiceType() {
		return serviceType;
	}
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}
	public boolean isClickable() {
		return clickable;
	}
	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}
	public boolean isShowTick() {
		return showTick;
	}
	public void setShowTick(boolean showTick) {
		this.showTick = showTick;
	}
	public boolean isShowNumber() {
		return showNumber;
	}
	public void setShowNumber(boolean showNumber) {
		this.showNumber = showNumber;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public boolean isGeneralInfo() {
		return generalInfo;
	}
	public void setGeneralInfo(boolean generalInfo) {
		this.generalInfo = generalInfo;
	}
	public int getUiOrder() {
		return uiOrder;
	}
	public void setUiOrder(int uiOrder) {
		this.uiOrder = uiOrder;
	}
	public int getStaysStatus() {
		return staysStatus;
	}
	public void setStaysStatus(int staysStatus) {
		this.staysStatus = staysStatus;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getRelevantUosId() {
		return relevantUosId;
	}
	public void setRelevantUosId(String relevantUosId) {
		this.relevantUosId = relevantUosId;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	
}
