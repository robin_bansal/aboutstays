package com.aboutstays.response.dto;

import com.aboutstays.pojos.GeneralServicePojo;

public class GetGSResponseWithHS extends GeneralServicePojo{
	
	private String hsId;

	public String getHsId() {
		return hsId;
	}

	public void setHsId(String hsId) {
		this.hsId = hsId;
	}
	
	

}
