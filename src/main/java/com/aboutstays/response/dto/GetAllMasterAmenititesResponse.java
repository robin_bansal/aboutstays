package com.aboutstays.response.dto;

import java.util.List;

public class GetAllMasterAmenititesResponse {

	private List<GetMasterAmenityResponse> amenities;
	
	public List<GetMasterAmenityResponse> getAmenities() {
		return amenities;
	}
	
	public void setAmenities(List<GetMasterAmenityResponse> amenities) {
		this.amenities = amenities;
	}
}
