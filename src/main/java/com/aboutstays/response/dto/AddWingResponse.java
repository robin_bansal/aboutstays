package com.aboutstays.response.dto;

public class AddWingResponse {
	private String wingId;

	public AddWingResponse() {
	}

	public AddWingResponse(String wingId) {
		super();
		this.wingId = wingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
}
