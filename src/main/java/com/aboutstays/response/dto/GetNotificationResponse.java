package com.aboutstays.response.dto;

import com.aboutstays.pojos.NotificationPojo;

public class GetNotificationResponse extends NotificationPojo {

	private String notificationId;
	private long created;
	
	public String getNotificationId() {
		return notificationId;
	}
	
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	
	public long getCreated() {
		return created;
	}
	
	public void setCreated(long created) {
		this.created = created;
	}
}
