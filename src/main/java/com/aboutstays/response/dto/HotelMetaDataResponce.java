package com.aboutstays.response.dto;

import com.aboutstays.pojos.AmenityMetaData;
import com.aboutstays.pojos.FloorMapMetaData;
import com.aboutstays.pojos.HotelInfoMetaData;
import com.aboutstays.pojos.MessagesMetaData;
import com.aboutstays.pojos.ServiceMenuMetaData;

public class HotelMetaDataResponce {
	private HotelInfoMetaData hotelInfoMetaData;
	private FloorMapMetaData floorMapMetaData;
	private AmenityMetaData amenityMetaData;
	private MessagesMetaData messagesMetaData;
	private ServiceMenuMetaData serviceMenuMetaData;

	public HotelInfoMetaData getHotelInfoMetaData() {
		return hotelInfoMetaData;
	}

	public void setHotelInfoMetaData(HotelInfoMetaData hotelInfoMetaData) {
		this.hotelInfoMetaData = hotelInfoMetaData;
	}

	public FloorMapMetaData getFloorMapMetaData() {
		return floorMapMetaData;
	}

	public void setFloorMapMetaData(FloorMapMetaData floorMapMetaData) {
		this.floorMapMetaData = floorMapMetaData;
	}

	public AmenityMetaData getAmenityMetaData() {
		return amenityMetaData;
	}

	public void setAmenityMetaData(AmenityMetaData amenityMetaData) {
		this.amenityMetaData = amenityMetaData;
	}

	public MessagesMetaData getMessagesMetaData() {
		return messagesMetaData;
	}

	public void setMessagesMetaData(MessagesMetaData messagesMetaData) {
		this.messagesMetaData = messagesMetaData;
	}

	public ServiceMenuMetaData getServiceMenuMetaData() {
		return serviceMenuMetaData;
	}

	public void setServiceMenuMetaData(ServiceMenuMetaData serviceMenuMetaData) {
		this.serviceMenuMetaData = serviceMenuMetaData;
	}

}
