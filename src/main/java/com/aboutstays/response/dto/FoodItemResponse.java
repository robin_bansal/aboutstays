package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.FoodItemPojo;
import com.aboutstays.pojos.GoesWellWithItemPojo;

public class FoodItemResponse extends FoodItemPojo{

	private String fooditemId;
	private List<GoesWellWithItemPojo> listGoesWellItems;
	
	public String getFooditemId() {
		return fooditemId;
	}
	public void setFooditemId(String fooditemId) {
		this.fooditemId = fooditemId;
	}
	public List<GoesWellWithItemPojo> getListGoesWellItems() {
		return listGoesWellItems;
	}
	public void setListGoesWellItems(List<GoesWellWithItemPojo> listGoesWellItems) {
		this.listGoesWellItems = listGoesWellItems;
	}
}
