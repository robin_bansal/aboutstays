package com.aboutstays.response.dto;

public class AddCarResponse {

	private String id;
	
	public AddCarResponse() {}
	
	public AddCarResponse(String id){
		this.id = id;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
