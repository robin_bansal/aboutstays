package com.aboutstays.response.dto;

import com.aboutstays.pojos.UserOptedServicePojo;

public class GetUserOptedServiceResponse extends UserOptedServicePojo{
	
	private boolean showTick;
	private int countToShow;
	
	public boolean isShowTick() {
		return showTick;
	}
	public void setShowTick(boolean showTick) {
		this.showTick = showTick;
	}
	public int getCountToShow() {
		return countToShow;
	}
	public void setCountToShow(int countToShow) {
		this.countToShow = countToShow;
	}
	
	

}
