package com.aboutstays.response.dto;

import java.util.List;

public class GetHotelRoomsResponse {

	private String hotelId;
	private List<GetRoomCategoryResponse> roomsList;
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public List<GetRoomCategoryResponse> getRoomsList() {
		return roomsList;
	}
	public void setRoomsList(List<GetRoomCategoryResponse> roomsList) {
		this.roomsList = roomsList;
	}
	
}
