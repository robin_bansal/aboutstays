package com.aboutstays.response.dto;

import com.aboutstays.pojos.BookingPojo;
import com.aboutstays.pojos.UserPojo;

public class GetBookingReponse extends BookingPojo{
	private String bookingId;
	private int bookingStatus;
	private GetRoomCategoryResponse roomCategoryData;
	private GetRoomResponse roomData;
	private UserPojo userData;
	private boolean addedToStayline;
	
	public boolean isAddedToStayline() {
		return addedToStayline;
	}
	
	public void setAddedToStayline(boolean addedToStayline) {
		this.addedToStayline = addedToStayline;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public int getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(int bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public GetRoomCategoryResponse getRoomCategoryData() {
		return roomCategoryData;
	}

	public void setRoomCategoryData(GetRoomCategoryResponse roomCategoryData) {
		this.roomCategoryData = roomCategoryData;
	}

	public GetRoomResponse getRoomData() {
		return roomData;
	}

	public void setRoomData(GetRoomResponse roomData) {
		this.roomData = roomData;
	}

	public UserPojo getUserData() {
		return userData;
	}

	public void setUserData(UserPojo userData) {
		this.userData = userData;
	}
	
	
}
