package com.aboutstays.response.dto;

public class AddServiceResponse {
	
	private String serviceId;
	
	public AddServiceResponse(){};
	
	public AddServiceResponse(String serviceId){
		this.serviceId = serviceId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	

}
