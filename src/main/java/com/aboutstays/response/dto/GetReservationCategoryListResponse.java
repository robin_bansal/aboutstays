package com.aboutstays.response.dto;

import java.util.List;

public class GetReservationCategoryListResponse {

	private List<GetReservationCategoryResponse> categories;
	
	public List<GetReservationCategoryResponse> getCategories() {
		return categories;
	}
	public void setCategories(List<GetReservationCategoryResponse> categories) {
		this.categories = categories;
	}
}
