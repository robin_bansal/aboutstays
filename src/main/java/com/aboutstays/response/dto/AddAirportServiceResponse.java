package com.aboutstays.response.dto;

public class AddAirportServiceResponse {

	private String id;

	public AddAirportServiceResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	
}
