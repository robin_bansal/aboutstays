package com.aboutstays.response.dto;

public class AddFloorResponse {
	private String floorId;

	public AddFloorResponse(String floorId) {
		this.floorId = floorId;
	}

	public AddFloorResponse() {
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
}
