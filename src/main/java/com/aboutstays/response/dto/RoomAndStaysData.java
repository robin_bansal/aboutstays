package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.enums.GetStayResponse;

public class RoomAndStaysData {
	private GetRoomResponse roomData;
	private List<GetStayResponse> stayList;
	public GetRoomResponse getRoomData() {
		return roomData;
	}
	public void setRoomData(GetRoomResponse roomData) {
		this.roomData = roomData;
	}
	public List<GetStayResponse> getStayList() {
		return stayList;
	}
	public void setStayList(List<GetStayResponse> stayList) {
		this.stayList = stayList;
	}
	
}
