package com.aboutstays.response.dto;

import java.util.Map;

import com.aboutstays.pojos.DateBasedExpenseItem;

public class GetExpenseByDateResponse {

	private Map<String, DateBasedExpenseItem> dateVsExpenseItemList;
	private Double grandTotal;
	public Map<String, DateBasedExpenseItem> getDateVsExpenseItemList() {
		return dateVsExpenseItemList;
	}
	public void setDateVsExpenseItemList(Map<String, DateBasedExpenseItem> dateVsExpenseItemList) {
		this.dateVsExpenseItemList = dateVsExpenseItemList;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
	
}
