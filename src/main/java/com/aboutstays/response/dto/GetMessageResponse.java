package com.aboutstays.response.dto;

import com.aboutstays.pojos.MessagePojo;

public class GetMessageResponse extends MessagePojo {

	private String messageId;
	private boolean byUser;
	private long created;
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public boolean isByUser() {
		return byUser;
	}
	public void setByUser(boolean byUser) {
		this.byUser = byUser;
	}
	
	public long getCreated() {
		return created;
	}
	
	public void setCreated(long created) {
		this.created = created;
	}
}
