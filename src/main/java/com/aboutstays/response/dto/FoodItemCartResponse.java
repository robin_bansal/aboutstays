package com.aboutstays.response.dto;

import java.util.List;

import com.aboutstays.pojos.FoodItemOrderDetails;

public class FoodItemCartResponse extends UserCartResponse{

	private List<FoodItemOrderDetails> foodItemOrderDetailList;
	
	public List<FoodItemOrderDetails> getFoodItemOrderDetailList() {
		return foodItemOrderDetailList;
	}
	
	public void setFoodItemOrderDetailList(List<FoodItemOrderDetails> foodItemOrderDetailList) {
		this.foodItemOrderDetailList = foodItemOrderDetailList;
	}
}
