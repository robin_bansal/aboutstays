package com.aboutstays.response.dto;

import com.aboutstays.pojos.EarlyCheckinInfo;

public class GetEarlyCheckinRoomResponse {

	private String roomCategoryId;
	private EarlyCheckinInfo checkInInfo;

	public String getRoomCategoryId() {
		return roomCategoryId;
	}

	public void setRoomCategoryId(String roomCategoryId) {
		this.roomCategoryId = roomCategoryId;
	}

	public EarlyCheckinInfo getCheckInInfo() {
		return checkInInfo;
	}

	public void setCheckInInfo(EarlyCheckinInfo checkInInfo) {
		this.checkInInfo = checkInInfo;
	}

}
