package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.GeneralService;

public interface GeneralServiceMao {
	
	public void add(GeneralService generalService);
	public List<GeneralService> getAll();
	public List<GeneralService> getAllByStaysStatus(Integer stayStatus);
	public GeneralService getById(String id);
	public GeneralService getById(String id, Boolean isGI);
	public void deletyById(String id);
	public void updateNonNullById(GeneralService service, String id);
	public GeneralService getByType(int type);
}
