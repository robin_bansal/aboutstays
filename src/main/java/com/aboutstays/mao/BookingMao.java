package com.aboutstays.mao;

import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Booking;

public interface BookingMao {
	
	public Booking getByReservationNumber(String reservationNumber);
	public Booking getByBookingId(String bookingId);
	public void createBooking(Booking booking);
	public List<Booking> getBookingsByReservationNumber(String reservationNumber, String hotelId);
	public List<Booking> getBookingsByCheckInDate(Date checkInDate, String hotelId);
	public void deleteByBookingId(String bookingId);
	public void deleteByReservationNumber(String reservationNumber);
	public void updateByBookingId(Booking booking);
	public List<Booking> getUnCompleteBookings(String hotelId, Date bookingDate);
	public List<Booking> getByRoomId(String roomId);
	public void setDefaultStaysId(String bookingId, String staysId);
	public Booking getByBookingId(String bookingId, String hotelId);
}
