package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.StaysFeedback;
import com.aboutstays.pojos.UserStayHotelIdsPojo;

public interface StaysFeedbackMao {

	public void add(StaysFeedback feedback);
	public StaysFeedback getById(String id);
	public List<StaysFeedback> getAll();
	public List<StaysFeedback> getAllOfHotel(String hotelId);
	public List<StaysFeedback> getAllOfUser(String userId);
	public List<StaysFeedback> getAllOfUserStay(String userId, String stayId);
	public void delete(String id);
	public void updateNonNull(StaysFeedback feedback);
	public StaysFeedback getByUserHotelStays(UserStayHotelIdsPojo request);
	public void update(StaysFeedback feedback);
	
	
	
}
