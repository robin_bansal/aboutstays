package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.User;
import com.aboutstays.enums.Type;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.PreferencesType;

public interface UserMao {

	public void save(User user);
	public User getById(String id);
	public void update(User user);
	public List<User> getAll();
	public User getByEmailOrPhone(String emailId, String phoneNumber);
	public User getByPhoneNumber(String number);
	public User matchCredentials(Type typeEnum, String value);
	public User getByEmail(String email);
	public void updateOtpByEmail(String emailId);
	public void updateOtpByNumber(String number);
	public void updateNonNullFields(User user, Type typeEnum, String value);
	public User getByUserId(String userId);
	public void updatePasswordByType(User user, Type typeEnum, String value);
	public void editProfileByUserID(User user, String usreId);
	public void updateDocs(List<IdentityDoc> identityDocs, String userId);
	public void updateProfilePicture(String userId, String imageUrl);
	public void update(String userId, List<PreferencesType> preferences);
	
}
