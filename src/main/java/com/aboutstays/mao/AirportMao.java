package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Airport;

public interface AirportMao {

	public void save(Airport airport);
	public void update(Airport airport);
	public void updateNonNull(Airport airport);
	public void delete(String id);
	public Airport getById(String id);
	public List<Airport> getAll();
	public List<Airport> getByCityId(String cityId);
}
