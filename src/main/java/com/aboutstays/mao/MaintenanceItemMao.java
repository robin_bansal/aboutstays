package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.MaintenanceItem;

public interface MaintenanceItemMao {
	
	public void save(MaintenanceItem item);
	public MaintenanceItem getById(String id);
	public List<MaintenanceItem> getAll();
	public void update(MaintenanceItem foodItem);
	public void updateNonNull(MaintenanceItem item, String id);
	public List<MaintenanceItem> getAllByHotelId(String hotelId);
	public List<MaintenanceItem> getAllByMTServicesId(String mtServiceId);
	public List<MaintenanceItem> fetchByType(String mtServiceId, int code);
	public void delete(String id);

}
