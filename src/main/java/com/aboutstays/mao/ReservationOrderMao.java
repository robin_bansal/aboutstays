package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.ReservationOrder;

public interface ReservationOrderMao {
	
	public void save(ReservationOrder entity);
	public ReservationOrder getById(String id);
	public List<ReservationOrder> getAll();
	public void updateNonNull(ReservationOrder request);
	public void delete(String id);
	public void changeOrderStatus(String id, int status);

}
