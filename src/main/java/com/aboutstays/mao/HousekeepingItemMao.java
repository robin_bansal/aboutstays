package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.HousekeepingItem;

public interface HousekeepingItemMao {
	
	public void save(HousekeepingItem item);
	public HousekeepingItem getById(String id);
	public List<HousekeepingItem> getAll();
	public void update(HousekeepingItem foodItem);
	public void updateNonNull(HousekeepingItem foodItem);
	public List<HousekeepingItem> getAllByHotelId(String hotelId);
	public List<HousekeepingItem> getAllByHKServicesId(String hkServiceId);
	public List<HousekeepingItem> getAllByHKServicesId(String hkServiceId, int code);
	public void delete(String id);

}
