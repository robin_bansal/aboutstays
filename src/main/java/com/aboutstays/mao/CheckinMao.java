package com.aboutstays.mao;

import com.aboutstays.entities.CheckinEntity;

public interface CheckinMao {

	public void save(CheckinEntity checkinEntity);
	public void update(CheckinEntity checkinEntity);
	public void updateNonNull(CheckinEntity checkinEntity);
	public CheckinEntity getById(String id);
	public void delete(String id);
	public void changeStatus(String id, int status);
	public CheckinEntity getByUserStaysHotelId(String userId, String staysId, String hotelId);
}
