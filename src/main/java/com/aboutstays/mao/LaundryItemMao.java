package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.LaundryItem;

public interface LaundryItemMao {

	public void save(LaundryItem laundryItem);
	public LaundryItem getById(String id);
	public List<LaundryItem> getAll();
	public List<LaundryItem> getAllByServicesId(String servicesId);
	public List<LaundryItem> getAllByHotelId(String hotelId);
	public void update(LaundryItem laundryItem);
	public void updateNonNull(LaundryItem laundryItem);
	public void delete(String id);
	public List<LaundryItem> getByType(String laundryServicesId, int laundryItemType);
}
