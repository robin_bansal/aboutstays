package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.CityGuideItemEntity;

public interface CityGuideItemMao {

	public void save(CityGuideItemEntity item);
	public CityGuideItemEntity getById(String id);
	public List<CityGuideItemEntity> getAll();
	public void update(CityGuideItemEntity item);
	public void updateNonNull(CityGuideItemEntity item);
	public List<CityGuideItemEntity> getAllByHotelId(String hotelId);
	public List<CityGuideItemEntity> getAllByCityGuideId(String cityGuideId);
	public List<CityGuideItemEntity> getAllByCityGuideId(String cityGuideId, int code);
	public void delete(String id);
}
