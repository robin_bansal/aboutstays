package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.CommuteService;

public interface CommuteServiceMao {
	
	public void add(CommuteService commuteService);
	public List<CommuteService> getAll();
	public CommuteService getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(CommuteService service, String id);

}
