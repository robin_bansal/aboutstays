package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.BellBoyServiceEntity;

public interface BellBoyServiceMao {

	public void save(BellBoyServiceEntity entity);
	public void update(BellBoyServiceEntity entity);
	public void updateNonNull(BellBoyServiceEntity entity);
	public void delete(String id);
	public List<BellBoyServiceEntity> getAll();
	public BellBoyServiceEntity getByHotelId(String hotelId);
	
}
