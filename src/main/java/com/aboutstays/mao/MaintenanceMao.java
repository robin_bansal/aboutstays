package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Maintenance;

public interface MaintenanceMao {
	
	public void add(Maintenance maintenance);
	public List<Maintenance> getAll();
	public Maintenance getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(Maintenance maintenance, String id);

}
