package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Car;

public interface CarMao {

	public void add(Car car);
	public List<Car> getAll();
	public Car getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(Car car, String id);
	public Car getByNumberPlate(String numberPlate);
}
