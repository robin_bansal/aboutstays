package com.aboutstays.mao;

import java.util.List;
import java.util.Map;

import com.aboutstays.entities.Hotel;
import com.aboutstays.enums.HotelUpdateBy;
import com.aboutstays.enums.StaysStatus;

public interface HotelMao {
	
	public void save(Hotel hotel);
	public List<Hotel> getAllHotels();
	public List<Hotel> getHotelsByType(int type);
	public Hotel getHotelById(String hotelId);  
	public void hotelUpdateNonNullFields(HotelUpdateBy type, String value, Hotel hotel);
	public void deleteHotelById(String id);
	public void update(Hotel hotel);
	public void addRoomToHotel(String hotelId, String roomId);
	public List<Hotel> getHotelsByCityAndName(String city, String name);
	public void UpdatePartnershipType(String hotelId, Integer partnershipType);
	public List<Hotel> getHotelListWithBasicInfo();
	public void updateHotelServiceDetails(Map<String, String> mapGSIdVsHSId, List<String> listGSIds, String hotelId, StaysStatus statusEnum);
	public String getTimezone(String hotelId);
	public void updateHotelServiceDetails(List<String> listGSIds, String hotelId, StaysStatus statusEnum);
	
}
