package com.aboutstays.mao;

import com.aboutstays.entities.PickList;

public interface PickListMao extends AbstractMao<PickList> {

	public PickList getByType(int type);

	public void deleteByType(int type);

	public void addOrModify(PickList pickList);
}
