package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Floor;

public interface FloorMao {
	
	public void add(Floor floor);

	public List<Floor> getAll();

	public Floor getById(String id);
	
	public void updateById(Floor floor,String id);

	public void delete(String id);

	public List<Floor> getByWingId(String wingId);

	public List<Floor> getByHotelId(String hotelId);

	public int getCountByWingId(String id);

	public Floor getByFloorAndWingId(String floorId, String wingId);

	public Floor getByFloorWingAndHotelId(String floorId, String wingId, String hotelId);
}
