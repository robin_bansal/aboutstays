package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.InternetService;

public interface InternetServiceMao {
	
	public void add(InternetService service);
	public List<InternetService> getAll();
	public InternetService getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(InternetService packs, String id);
	public void update(InternetService service);

}
