package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.FoodOrder;

public interface FoodRequestsMao {

	public void save(FoodOrder foodOrderRequest);
	public FoodOrder getById(String id);
	public List<FoodOrder> getAll();
	public List<FoodOrder> getAllByUserId(String userId);
	public List<FoodOrder> getAllByHotelId(String hotelId);
	public List<FoodOrder> getAllByUserAndStayId(String userId, String stayId);
	public void update(FoodOrder request);
	public void updateNonNull(FoodOrder request);
	public void delete(String id);
	public void changeOrderStatus(String id, int status);
	
}
