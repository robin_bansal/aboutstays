 package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.UserOptedService;
import com.aboutstays.request.dto.GetUOSByUserRequest;

public interface UserOptedServicesMao {

	public void add(UserOptedService service);
	public List<UserOptedService> getAll();
	public List<UserOptedService> getAllByUserId(String userId);
	public UserOptedService getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(UserOptedService service, String id);
	public List<UserOptedService> getByUserAndStay(GetUOSByUserRequest request);
	public List<UserOptedService> getByUserStayAndHotel(String staysId, String userId, String hotelId);
	public void update(UserOptedService request);
	public UserOptedService getByRefId(String refId);
	public List<UserOptedService> getAllByHotelIdAndCreated(String hotelId, long time);
	public List<UserOptedService> getAllByHotelIdAndCreatedAndStatus(String hotelId, long time,Integer status);
}
