package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.FoodItem;

public interface FoodItemMao {

	public void save(FoodItem foodItem);
	public FoodItem getById(String id);
	public List<FoodItem> getAll();
	public void update(FoodItem foodItem);
	public void updateNonNull(FoodItem foodItem);
	public List<FoodItem> getAllByHotelId(String hotelId);
	public List<FoodItem> getAllByRoomServicesId(String roomServicesId);
	public List<FoodItem> getAllByRoomServicesId(String roomServicesId, int code);
	public void delete(String id);
}
