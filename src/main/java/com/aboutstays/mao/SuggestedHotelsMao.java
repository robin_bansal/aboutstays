package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.SuggestedHotels;

public interface SuggestedHotelsMao {
	
	public void add(SuggestedHotels request);
	public List<SuggestedHotels> getAll();
	public SuggestedHotels getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(SuggestedHotels request, String id);

}
