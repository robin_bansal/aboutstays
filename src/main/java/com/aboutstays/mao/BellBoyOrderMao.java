package com.aboutstays.mao;

import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;

public interface BellBoyOrderMao {
	
	public void save(BellBoyOrder order);
	public void getRequestedBellBoys(UserStaysHotelGSIdsPojo request);
	public void delete(String id);
	public BellBoyOrder getById(String id);
	public void changeOrderStatus(String id, int status);
	
	

}
