package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.LaundryServiceEntity;

public interface LaundryServicesMao {

	public void save(LaundryServiceEntity laundryServiceEntity);
	public LaundryServiceEntity getById(String id);
	public List<LaundryServiceEntity> getAll();
	public void delete(String id);
	public void update(LaundryServiceEntity entity);
	public void updateNonNull(LaundryServiceEntity entity);
	
}
