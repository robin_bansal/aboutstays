package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.ServiceFeedback;

public interface ServiceFeedbackMao {

	public void add(ServiceFeedback request);
	public ServiceFeedback get(String id);
	public List<ServiceFeedback> getAll();
	public void update(ServiceFeedback request);
	public void delete(String id);
	public void updateNonNull(ServiceFeedback request);
	public ServiceFeedback getByUosId(String uosId);
	
}
