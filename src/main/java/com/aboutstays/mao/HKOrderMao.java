package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.HousekeepingOrder;

public interface HKOrderMao {
	
	public void save(HousekeepingOrder hkOrder);
	public HousekeepingOrder getById(String id);
	public List<HousekeepingOrder> getAll();
	public List<HousekeepingOrder> getAllByUserId(String userId);
	public List<HousekeepingOrder> getAllByHotelId(String hotelId);
	public List<HousekeepingOrder> getAllByUserAndStayId(String userId, String stayId);
	public void update(HousekeepingOrder request);
	public void updateNonNull(HousekeepingOrder request);
	public void delete(String id);
	public void changeOrderStatus(String id, int status);

}
