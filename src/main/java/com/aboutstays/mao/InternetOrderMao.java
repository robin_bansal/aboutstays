package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.InternetOrder;

public interface InternetOrderMao {

	public void add(InternetOrder order);
	public InternetOrder get(String id);
	public List<InternetOrder> getAll();
	public void delete(String id);
	public void updateNonNull(InternetOrder order);
	public void changeOrderStatus(String id, int status);
	

}
