package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.EntertaimentItem;

public interface EntertainmentItemMao {
	
	public void add(EntertaimentItem item);
	public void addMultiple(List<EntertaimentItem> listItem);
	public EntertaimentItem getById(String id);
	public List<EntertaimentItem> getAll();
	public List<EntertaimentItem> getAll(String entServiceId);
	public void delete(String id);
	public void updateNonNull(EntertaimentItem item);
	public EntertaimentItem getByMasterItemId(String masterItemId, String hotelId, String serviceId);
	public void update(EntertaimentItem entertaimentItem);

}
