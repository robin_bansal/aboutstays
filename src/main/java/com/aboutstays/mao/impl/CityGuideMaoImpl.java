package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.CityGuide;
import com.aboutstays.mao.CityGuideMao;

@Repository
public class CityGuideMaoImpl extends AbstractMaoImpl<CityGuide> implements CityGuideMao{

	public CityGuideMaoImpl() {
		super(CityGuide.class);
	}
	
	@Override
	public void add(CityGuide cityGuide) {
		saveT(cityGuide);
	}

	@Override
	public List<CityGuide> getAll() {
		return getAllT();
	}

	@Override
	public CityGuide getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(CityGuide cityGuide, String id) {
		updateNonNullFieldsOfT(cityGuide, id);
	}

}
