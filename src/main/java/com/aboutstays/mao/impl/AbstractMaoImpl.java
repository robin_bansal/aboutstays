package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.aboutstays.mao.AbstractMao;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class AbstractMaoImpl<T> implements AbstractMao<T> {
	
	@Autowired
	@Qualifier(value="mongoTemplate")
	protected MongoTemplate mongoTemplate;
	
	protected final Class<T> entityClass;

	public AbstractMaoImpl(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public void saveT(T t) {
		mongoTemplate.save(t);
	}

	@Override
	public T getTById(String id) {
		return mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)), entityClass);
	}

	@Override
	public List<T> getAllT() {
		return mongoTemplate.find(new Query(), entityClass);
	}

	@Override
	public void deleteT(String id) {
		mongoTemplate.remove(new Query(Criteria.where("_id").is(id)), entityClass);
	}

	@Override
	public void updateT(T t) {
		saveT(t);
		
	}

	@Override
	public T findOne(Criteria criteria) {
		return mongoTemplate.findOne(new Query(criteria), entityClass);
	}
	
	

	@Override
	public List<T> find(Criteria criteria) {
		return mongoTemplate.find(new Query(criteria), entityClass);
	}
	
	

	@Override
	public void updateT(Criteria criteria, Update update) {
		 mongoTemplate.updateFirst(new Query(criteria), update, entityClass);
	}

	@Override
	public void updateAllT(Criteria criteria, Update update) {
		 mongoTemplate.updateMulti(new Query(criteria), update, entityClass);
	}

	@Override
	public void upsert(Criteria criteria, Update update) {
		mongoTemplate.upsert(new Query(criteria), update, entityClass);
		
	}

	@Override
	public Update fromDBObjectExcludeNullFields(DBObject dbObject) {
		Update update = new Update();       
	    for (String key : dbObject.keySet()) {
	        Object value = dbObject.get(key);
	        if(value!=null){
	            update.set(key, value);
	        }
	    }
	    return update;
	}

	@Override
	public void updateNonNullFieldsOfT(T t, String id) {
		Query query = new Query(Criteria.where(Fields.UNDERSCORE_ID).is(id));
		DBObject dbObject = new BasicDBObject();
		mongoTemplate.getConverter().write(t, dbObject);
		Update update = fromDBObjectExcludeNullFields(dbObject);
		mongoTemplate.updateFirst(query, update, entityClass);
	}
	@Override
	public void updateNonNullFieldsOfT(T t, Query query) {
		DBObject dbObject = new BasicDBObject();
		mongoTemplate.getConverter().write(t, dbObject);
		Update update = fromDBObjectExcludeNullFields(dbObject);
		mongoTemplate.updateMulti(query, update, entityClass);
	}

	@Override
	public void deleteT(Criteria criteria) {
		mongoTemplate.remove(new Query(criteria), entityClass);
	}

	@Override
	public List<T> find(Query query) {
		return mongoTemplate.find(query, entityClass);
	}

	@Override
	public T findOne(Query query) {
		return mongoTemplate.findOne(query, entityClass);
	}
	
	@Override
	public long count(Criteria criteria) {
		if(criteria!=null)
			return mongoTemplate.count(new Query(criteria), entityClass);
		return mongoTemplate.count(new Query(), entityClass);
	}
	

}
