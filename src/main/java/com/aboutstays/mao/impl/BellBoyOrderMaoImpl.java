package com.aboutstays.mao.impl;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.mao.BellBoyOrderMao;
import com.aboutstays.pojos.UserStaysHotelGSIdsPojo;

@Repository
public class BellBoyOrderMaoImpl extends AbstractMaoImpl<BellBoyOrder> implements BellBoyOrderMao{

	
	public BellBoyOrderMaoImpl() {
		super(BellBoyOrder.class);
	}

	@Override
	public void save(BellBoyOrder order) {
		saveT(order);
	}

	@Override
	public void getRequestedBellBoys(UserStaysHotelGSIdsPojo request) {
		
		
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public BellBoyOrder getById(String id) {
		return getTById(id);
	}

	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(BellBoyOrder.FIELDS.STATUS, status));
	}

}
