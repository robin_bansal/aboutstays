package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ReservationItem;
import com.aboutstays.mao.ReservationItemMao;

@Repository
public class ReservationItemMaoImpl extends AbstractMaoImpl<ReservationItem> implements ReservationItemMao{

	public ReservationItemMaoImpl() {
		super(ReservationItem.class);
	}

	@Override
	public void save(ReservationItem item) {
		saveT(item);
	}

	@Override
	public ReservationItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<ReservationItem> getAll() {
		return getAllT();
	}

	@Override
	public void updateNonNull(ReservationItem item) {
		updateNonNullFieldsOfT(item, item.getId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void update(ReservationItem item) {
		updateT(item);
	}

	@Override
	public List<ReservationItem> getByHotelId(String hotelId) {
		return find(Criteria.where(ReservationItem.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public ReservationItem getByIdCategoryAndHotelId(String id, String categoryId, String hotelId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(id).and(ReservationItem.FIELDS.CATEGORY_ID).is(categoryId).and(ReservationItem.FIELDS.HOTEL_ID).is(hotelId));
	}

}
