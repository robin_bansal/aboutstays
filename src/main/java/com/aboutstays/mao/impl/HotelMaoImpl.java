package com.aboutstays.mao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Hotel;
import com.aboutstays.enums.HotelUpdateBy;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.mao.HotelMao;
import com.aboutstays.mao.utils.MaoUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Repository
public class HotelMaoImpl extends AbstractMaoImpl<Hotel> implements HotelMao{

	public HotelMaoImpl() {
		super(Hotel.class);
	}
	
	@Override
	public void save(Hotel hotel) {
		saveT(hotel);
	}

	@Override
	public List<Hotel> getAllHotels() {
		return getAllT();
	}

	@Override
	public List<Hotel> getHotelsByType(int type) {
		return find(Criteria.where(Hotel.FIELDS.HOTEL_TYPE).is(type));
	}

	@Override
	public Hotel getHotelById(String hotelId) {
		return findOne(Criteria.where(Hotel.FIELDS._ID).is(hotelId));
	}


	@Override
	public void hotelUpdateNonNullFields(HotelUpdateBy type, String value, Hotel hotel) {
		DBObject dbObject = new BasicDBObject();
		mongoTemplate.getConverter().write(hotel, dbObject);
		Update update = MaoUtility.fromDBObjectExcludeNonNullFields(dbObject);
		switch(type){
		case HOTELID:
			upsert(Criteria.where(Hotel.FIELDS._ID).is(value), update);
			break;
		}
	}

	@Override
	public void deleteHotelById(String id) {
		deleteT(id);	
	}
	
	
	public void update(Hotel hotel) {
		updateT(hotel);
	}

	@Override
	public void addRoomToHotel(String hotelId, String roomId) {
		mongoTemplate.updateFirst(
	            Query.query(Criteria.where(Fields.UNDERSCORE_ID).is(hotelId)), 
	            new Update().addToSet(Hotel.FIELDS.ROOMS_IDS, roomId), entityClass);
	}

	@Override
	public List<Hotel> getHotelsByCityAndName(String city, String name) {
		if(!StringUtils.isEmpty(city) && !StringUtils.isEmpty(name)){
			return find(Criteria.where("generalInformation.address.city").is(city).and("generalInformation.name").is(name));
		} else if(!StringUtils.isEmpty(city) && StringUtils.isEmpty(name)){
			return find(Criteria.where("generalInformation.address.city").is(city));
		} else if(StringUtils.isEmpty(city) && !StringUtils.isEmpty(name)){
			return find(Criteria.where("generalInformation.name").is(name));
		} 
		return null;	
	}

	
	@Override
	public void UpdatePartnershipType(String hotelId, Integer partnershipType) {
		Update update = new Update();
		update.set(Hotel.FIELDS.PARTNERSHIPT_TYPE, partnershipType);
		updateT(Criteria.where(Hotel.FIELDS._ID).is(hotelId), update);
	}

	@Override
	public List<Hotel> getHotelListWithBasicInfo() {
		Query query = new Query();
		query.fields().include(Hotel.FIELDS.GENERAL_INFO).include(Hotel.FIELDS.PARTNERSHIPT_TYPE).include(Hotel.FIELDS._ID);
		return find(query);		
	}

	@Override
	public void updateHotelServiceDetails(Map<String, String> mapGSIdVSHSId, List<String> listGSIds, String hotelId, StaysStatus statusEnum) {
		Update update = new Update();
		switch(statusEnum){
		case CHECK_IN:
			update.set("checkInDetails.mapGSIdVsHSId", mapGSIdVSHSId);
			update.set("checkInDetails.listGSIds", listGSIds);
			break;
		case STAYING:
			update.set("stayDetails.mapGSIdVsHSId", mapGSIdVSHSId);
			update.set("stayDetails.listGSIds", listGSIds);
			break;
		case CHECK_OUT:
			update.set("checkOutDetails.mapGSIdVsHSId", mapGSIdVSHSId);
			update.set("checkOutDetails.listGSIds", listGSIds);
			break;
		}
		updateT(Criteria.where(Hotel.FIELDS._ID).is(hotelId), update);
	}

	@Override
	public String getTimezone(String hotelId) {
		Hotel hotel = getTById(hotelId);
		if(hotel!=null){
			return hotel.getTimezone();
		}
		return null;
	}

	@Override
	public void updateHotelServiceDetails(List<String> listGSIds, String hotelId, StaysStatus statusEnum) {
		Update update = new Update();
		switch(statusEnum){
		case CHECK_IN:
			update.set("checkInDetails.listGSIds", listGSIds);
			break;
		case STAYING:
			update.set("stayDetails.listGSIds", listGSIds);
			break;
		case CHECK_OUT:
			update.set("checkOutDetails.listGSIds", listGSIds);
			break;
		}
		updateT(Criteria.where(Hotel.FIELDS._ID).is(hotelId), update);
	}
}
