package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Maintenance;
import com.aboutstays.mao.MaintenanceMao;

@Repository
public class MaintenanceMaoImpl extends AbstractMaoImpl<Maintenance> implements MaintenanceMao{

	public MaintenanceMaoImpl() {
		super(Maintenance.class);
	}

	@Override
	public void add(Maintenance maintenance) {
		saveT(maintenance);
		
	}

	@Override
	public List<Maintenance> getAll() {
		return getAllT();
	}

	@Override
	public Maintenance getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(Maintenance maintenance, String id) {
		updateNonNullFieldsOfT(maintenance, id);
	}

}
