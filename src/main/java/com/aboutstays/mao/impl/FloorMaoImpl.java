package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Floor;
import com.aboutstays.mao.FloorMao;

@Repository
public class FloorMaoImpl extends AbstractMaoImpl<Floor> implements FloorMao {

	public FloorMaoImpl() {
		super(Floor.class);
	}

	@Override
	public void add(Floor floor) {
		saveT(floor);
	}

	@Override
	public List<Floor> getAll() {
		return getAllT();
	}

	@Override
	public Floor getById(String id) {
		return getTById(id);
	}

	@Override
	public void updateById(Floor floor, String id) {
		updateNonNullFieldsOfT(floor, id);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public List<Floor> getByWingId(String wingId) {
		return find(Criteria.where(Floor.FIELDS.WING_ID).is(wingId));
	}

	@Override
	public List<Floor> getByHotelId(String hotelId) {
		return find(Criteria.where(Floor.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public int getCountByWingId(String wingId) {
		long count = count(Criteria.where(Floor.FIELDS.WING_ID).is(wingId));
		return (int) count;
	}

	@Override
	public Floor getByFloorAndWingId(String floorId, String wingId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(floorId).and(Floor.FIELDS.WING_ID).is(wingId));
	}

	@Override
	public Floor getByFloorWingAndHotelId(String floorId, String wingId, String hotelId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(floorId).and(Floor.FIELDS.WING_ID).is(wingId).and(Floor.FIELDS.HOTEL_ID).is(hotelId));
	}

}
