package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.RoomCategory;
import com.aboutstays.mao.RoomCategoryMao;
import com.aboutstays.request.dto.SetEarlyCheckInRoomRequest;
import com.aboutstays.request.dto.SetLateCheckOutRoomRequest;

@Repository
public class RoomCategoryMaoImpl extends AbstractMaoImpl<RoomCategory> implements RoomCategoryMao {

	public RoomCategoryMaoImpl() {
		super(RoomCategory.class);
	}

	@Override
	public void save(RoomCategory room) {
		saveT(room);
	}

	@Override
	public RoomCategory getById(String roomId) {
		return getTById(roomId);
	}

	@Override
	public List<RoomCategory> getAll() {
		return getAllT();
	}

	@Override
	public List<RoomCategory> getAllByRoomCategoryIds(List<String> roomIds) {
		return find(Criteria.where(Fields.UNDERSCORE_ID).in(roomIds));
	}

	@Override
	public void delete(String roomId) {
		deleteT(roomId);
	}

	@Override
	public void update(RoomCategory room) {
		updateT(room);
	}

	@Override
	public void updateNonNullFields(RoomCategory room) {
		updateNonNullFieldsOfT(room, room.getRoomCategegoryId());
	}

	@Override
	public void setEarlyCheckinInfo(SetEarlyCheckInRoomRequest request) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(request.getRoomCategoryId()), new Update().set(RoomCategory.FIELDS.EARLY_CHECKIN_INFO, request.getInfo()));
	}

	@Override
	public RoomCategory getEarlyCheckinRoomInfo(String roomId) {
		Query query = new Query(Criteria.where(Fields.UNDERSCORE_ID).is(roomId));
		query.fields().include(RoomCategory.FIELDS.EARLY_CHECKIN_INFO).include(Fields.UNDERSCORE_ID);
		return findOne(query);
	}

	@Override
	public void setLateCheckoutInfo(SetLateCheckOutRoomRequest request) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(request.getRoomCategoryId()), new Update().set(RoomCategory.FIELDS.LATE_CHECKOUT_INFO, request.getCheckOutInfo()));
	}

	@Override
	public RoomCategory getLateCheckoutInfo(String roomId) {
		Query query = new Query(Criteria.where(Fields.UNDERSCORE_ID).is(roomId));
		query.fields().include(RoomCategory.FIELDS.LATE_CHECKOUT_INFO).include(Fields.UNDERSCORE_ID);
		return findOne(query);
	}

	@Override
	public List<RoomCategory> getByHotelId(String hotelId) {
		return find(new Query(Criteria.where(RoomCategory.FIELDS.HOTEL_ID).is(hotelId)));
	}

	@Override
	public RoomCategory getByHotelAndCategoryId(String hotelId, String roomId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(roomId).and(RoomCategory.FIELDS.HOTEL_ID).is(hotelId));
	}

}
