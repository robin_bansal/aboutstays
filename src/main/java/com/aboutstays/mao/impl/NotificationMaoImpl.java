package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Notification;
import com.aboutstays.entities.Notification.FIELDS;
import com.aboutstays.mao.NotificationMao;

@Repository
public class NotificationMaoImpl extends AbstractMaoImpl<Notification> implements NotificationMao {

	public NotificationMaoImpl() {
		super(Notification.class);
	}

	@Override
	public void save(Notification notification) {
		saveT(notification);
	}

	@Override
	public void update(Notification notification) {
		updateT(notification);
	}

	@Override
	public void updateNonNull(Notification notification) {
		updateNonNullFieldsOfT(notification, notification.getId());
	}

	@Override
	public Notification getById(String id) {
		return getTById(id);
	}

	@Override
	public List<Notification> getAll() {
		return getAllT();
	}

	@Override
	public List<Notification> getAll(String userId, String staysId, String hotelId) {
		return find(Criteria.where(FIELDS.USER_ID).is(userId).and(FIELDS.STAYS_ID).is(staysId).and(FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

}
