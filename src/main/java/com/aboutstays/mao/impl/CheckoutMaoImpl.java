package com.aboutstays.mao.impl;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.CheckoutEntity;
import com.aboutstays.entities.CheckoutEntity.FIELDS;
import com.aboutstays.mao.CheckoutMao;

@Repository
public class CheckoutMaoImpl extends AbstractMaoImpl<CheckoutEntity> implements CheckoutMao {

	public CheckoutMaoImpl() {
		super(CheckoutEntity.class);
	}

	@Override
	public void save(CheckoutEntity checkoutEntity) {
		saveT(checkoutEntity);
	}

	@Override
	public void update(CheckoutEntity checkinEntity) {
		updateT(checkinEntity);
	}

	@Override
	public void updateNonNull(CheckoutEntity checkoutEntity) {
		updateNonNullFieldsOfT(checkoutEntity, checkoutEntity.getId());
		
	}

	@Override
	public CheckoutEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
		
	}

	@Override
	public void changeStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(FIELDS.STATUS, status));
	}

	@Override
	public CheckoutEntity getByUserStaysHotelId(String userId, String staysId, String hotelId) {
		return findOne(Criteria.where(FIELDS.USER_ID).is(userId).and(FIELDS.STAYS_ID).is(staysId).and(FIELDS.HOTEL_ID).is(hotelId));
	}

	
}
