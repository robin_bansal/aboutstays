package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.MasterCity;
import com.aboutstays.mao.MasterCityMao;

@Repository
public class MasterCityMaoImpl extends AbstractMaoImpl<MasterCity> implements MasterCityMao {

	public MasterCityMaoImpl() {
		super(MasterCity.class);
	}

	@Override
	public void save(MasterCity masterCity) {
		saveT(masterCity);
	}

	@Override
	public void saveAll(List<MasterCity> masterCities) {
		mongoTemplate.insertAll(masterCities);
	}

	@Override
	public List<MasterCity> getAll() {
		return getAllT();
	}

	@Override
	public MasterCity getById(String id) {
		return getTById(id);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public MasterCity getByName(String name) {
		return findOne(Criteria.where(MasterCity.FIELDS.NAME).is(name));
	}

}
