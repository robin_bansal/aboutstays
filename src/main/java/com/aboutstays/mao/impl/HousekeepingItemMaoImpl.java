package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.HousekeepingItem;
import com.aboutstays.mao.HousekeepingItemMao;

@Repository
public class HousekeepingItemMaoImpl extends AbstractMaoImpl<HousekeepingItem> implements HousekeepingItemMao {

	public HousekeepingItemMaoImpl() {
		super(HousekeepingItem.class);
	}
	
	@Override
	public void save(HousekeepingItem item) {
		saveT(item);
		
	}

	@Override
	public HousekeepingItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<HousekeepingItem> getAll() {
		return getAllT();
	}

	@Override
	public void update(HousekeepingItem foodItem) {
		updateT(foodItem);
		
	}

	@Override
	public void updateNonNull(HousekeepingItem item) {
		updateNonNullFieldsOfT(item, item.getId());
	}

	@Override
	public List<HousekeepingItem> getAllByHotelId(String hotelId) {
		return find(Criteria.where(HousekeepingItem.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<HousekeepingItem> getAllByHKServicesId(String hkServiceId) {
		return find(Criteria.where(HousekeepingItem.FIELDS.HOUSEKEEPING_SERVICES_ID).is(hkServiceId));
	}

	@Override
	public List<HousekeepingItem> getAllByHKServicesId(String hkServiceId, int code) {
		return find(Criteria.where(HousekeepingItem.FIELDS.HOUSEKEEPING_SERVICES_ID).is(hkServiceId).and(HousekeepingItem.FIELDS.ITEM_TYPE).is(code));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
		
	}

}
