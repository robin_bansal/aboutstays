package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.entities.StaysFeedback;
import com.aboutstays.mao.StaysFeedbackMao;
import com.aboutstays.pojos.UserStayHotelIdsPojo;

@Repository
public class StaysFeedbackMaoImpl extends AbstractMaoImpl<StaysFeedback> implements StaysFeedbackMao{

	public StaysFeedbackMaoImpl() {
		super(StaysFeedback.class);
	}

	@Override
	public void add(StaysFeedback feedback) {
		saveT(feedback);
		
	}

	@Override
	public StaysFeedback getById(String id) {
		return getTById(id);
	}

	@Override
	public List<StaysFeedback> getAll() {
		return getAllT();
	}

	@Override
	public List<StaysFeedback> getAllOfHotel(String hotelId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<StaysFeedback> getAllOfUser(String userId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId));
	}

	@Override
	public List<StaysFeedback> getAllOfUserStay(String userId, String stayId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId).and(BaseSessionEntity.FIELDS.STAYS_ID).is(stayId)); 
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNull(StaysFeedback feedback) {
		updateNonNullFieldsOfT(feedback, feedback.getId());
	}

	@Override
	public StaysFeedback getByUserHotelStays(UserStayHotelIdsPojo request) {
		return findOne(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(request.getHotelId()).and(BaseSessionEntity.FIELDS.USER_ID).is(request.getUserId()).and(BaseSessionEntity.FIELDS.STAYS_ID).is(request.getStaysId()));
	}

	@Override
	public void update(StaysFeedback feedback) {
		updateT(feedback);
		
	}

}
