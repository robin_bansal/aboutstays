package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.controller.CommuteOrder;
import com.aboutstays.mao.CommuteOrderMao;

@Repository
public class CommuteOrderMaoImpl extends AbstractMaoImpl<CommuteOrder> implements CommuteOrderMao{

	public CommuteOrderMaoImpl() {
		super(CommuteOrder.class);
	}

	@Override
	public void save(CommuteOrder entity) {
		saveT(entity);
	}

	@Override
	public CommuteOrder getById(String id) {
		return getTById(id);
	}

	@Override
	public List<CommuteOrder> getAll() {
		return getAllT();
	}

	@Override
	public void updateNonNull(CommuteOrder request) {
		updateNonNullFieldsOfT(request, request.getId());
		
	}

	@Override
	public void delete(String id) {
		deleteT(id);
		
	}

	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(CommuteOrder.FIELDS.STATUS, status));
		
	}

}
