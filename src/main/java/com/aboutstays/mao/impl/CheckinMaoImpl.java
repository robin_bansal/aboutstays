package com.aboutstays.mao.impl;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.CheckinEntity;
import com.aboutstays.entities.CheckinEntity.FIELDS;
import com.aboutstays.mao.CheckinMao;

@Repository
public class CheckinMaoImpl extends AbstractMaoImpl<CheckinEntity> implements CheckinMao {

	public CheckinMaoImpl() {
		super(CheckinEntity.class);
	}

	@Override
	public void save(CheckinEntity checkinEntity) {
		saveT(checkinEntity);
	}

	@Override
	public void update(CheckinEntity checkinEntity) {
		updateT(checkinEntity);
	}

	@Override
	public void updateNonNull(CheckinEntity checkinEntity) {
		updateNonNullFieldsOfT(checkinEntity, checkinEntity.getStaysId());
	}

	@Override
	public CheckinEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void changeStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(FIELDS.STATUS, status));
	}

	@Override
	public CheckinEntity getByUserStaysHotelId(String userId, String staysId, String hotelId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(staysId).and(FIELDS.USER_ID).is(userId).and(FIELDS.HOTEL_ID).is(hotelId));
	}
}
