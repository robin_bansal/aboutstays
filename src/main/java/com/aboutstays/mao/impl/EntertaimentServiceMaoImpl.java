package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.EntertaimentServiceEntity;
import com.aboutstays.mao.EntertaimentServiceMao;

@Repository
public class EntertaimentServiceMaoImpl extends AbstractMaoImpl<EntertaimentServiceEntity>
		implements EntertaimentServiceMao {

	public EntertaimentServiceMaoImpl() {
		super(EntertaimentServiceEntity.class);
	}

	@Override
	public void save(EntertaimentServiceEntity entity) {
		saveT(entity);
	}

	@Override
	public void update(EntertaimentServiceEntity entity) {
		updateT(entity);
	}

	@Override
	public List<EntertaimentServiceEntity> getAll() {
		return getAllT();
	}

	@Override
	public EntertaimentServiceEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

}
