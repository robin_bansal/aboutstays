package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.GeneralService;
import com.aboutstays.mao.GeneralServiceMao;

@Repository
public class GeneralServiceMaoImpl extends AbstractMaoImpl<GeneralService> implements GeneralServiceMao{

	public GeneralServiceMaoImpl() {
		super(GeneralService.class);
	}
	
	@Override
	public void add(GeneralService generalService) {
		saveT(generalService);	
	}

	@Override
	public List<GeneralService> getAll() {
		return getAllT();
	}

	@Override
	public List<GeneralService> getAllByStaysStatus(Integer stayStatus) {
		return find(Criteria.where(GeneralService.FIELDS.STATUS).is(stayStatus));
	}

	@Override
	public GeneralService getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(GeneralService service, String id) {
		updateNonNullFieldsOfT(service, id);
	}

	@Override
	public GeneralService getById(String id, Boolean isGI) {
		return findOne(Criteria.where(GeneralService.FIELDS._ID).is(id).and(GeneralService.FIELDS.GENERAL_INFO).is(isGI));
	}

	@Override
	public GeneralService getByType(int type) {
		return findOne(Criteria.where(GeneralService.FIELDS.SERVICE_TYPE).is(type));
	}
	
	

	
}
