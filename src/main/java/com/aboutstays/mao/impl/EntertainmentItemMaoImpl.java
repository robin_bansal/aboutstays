package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.EntertaimentItem;
import com.aboutstays.entities.EntertaimentItem.FIELDS;
import com.aboutstays.mao.EntertainmentItemMao;

@Repository
public class EntertainmentItemMaoImpl extends AbstractMaoImpl<EntertaimentItem> implements EntertainmentItemMao{

	public EntertainmentItemMaoImpl() {
		super(EntertaimentItem.class);
	}

	@Override
	public void add(EntertaimentItem item) {
		saveT(item);
	}


	@Override
	public EntertaimentItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<EntertaimentItem> getAll() {
		return getAllT();
	}



	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNull(EntertaimentItem item) {
		updateNonNullFieldsOfT(item, item.getId());
	}

	@Override
	public void addMultiple(List<EntertaimentItem> listItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntertaimentItem> getAll(String entServiceId) {
		return find(Criteria.where(EntertaimentItem.FIELDS.ENT_SERVICE_ID).is(entServiceId));
	}

	@Override
	public EntertaimentItem getByMasterItemId(String masterItemId, String hotelId, String serviceId) {
		return findOne(Criteria.where(EntertaimentItem.FIELDS.MASTER_ITEM_ID).is(masterItemId).and(FIELDS.HOTEL_ID).is(hotelId).and(FIELDS.ENT_SERVICE_ID).is(serviceId));
	}

	@Override
	public void update(EntertaimentItem entertaimentItem) {
		updateT(entertaimentItem);
	}

}
