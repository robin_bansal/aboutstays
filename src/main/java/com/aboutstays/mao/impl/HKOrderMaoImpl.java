package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.mao.HKOrderMao;

@Repository
public class HKOrderMaoImpl extends AbstractMaoImpl<HousekeepingOrder> implements HKOrderMao {

	public HKOrderMaoImpl() {
		super(HousekeepingOrder.class);
	}
	
	@Override
	public void save(HousekeepingOrder hkOrder) {
		saveT(hkOrder);
	}

	@Override
	public HousekeepingOrder getById(String id) {
		return getTById(id);
	}

	@Override
	public List<HousekeepingOrder> getAll() {
		return getAllT();
	}

	@Override
	public List<HousekeepingOrder> getAllByUserId(String userId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId));
	}

	@Override
	public List<HousekeepingOrder> getAllByHotelId(String hotelId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<HousekeepingOrder> getAllByUserAndStayId(String userId, String stayId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId).and(BaseSessionEntity.FIELDS.STAYS_ID).is(stayId));
	}

	@Override
	public void update(HousekeepingOrder request) {
		updateT(request);	
	}

	@Override
	public void updateNonNull(HousekeepingOrder request) {
		updateNonNullFieldsOfT(request, request.getId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);	
	}
	
	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(HousekeepingOrder.FIELDS.STATUS, status));
	}

}
