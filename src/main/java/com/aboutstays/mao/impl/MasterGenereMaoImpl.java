package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.MasterGenereItem;
import com.aboutstays.mao.MasterGenereMao;

@Repository
public class MasterGenereMaoImpl extends AbstractMaoImpl<MasterGenereItem> implements MasterGenereMao {

	public MasterGenereMaoImpl() {
		super(MasterGenereItem.class);
	}

	@Override
	public void save(MasterGenereItem masterGenereItem) {
		saveT(masterGenereItem);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public List<MasterGenereItem> getAll() {
		return getAllT();
	}

	@Override
	public void update(MasterGenereItem masterGenereItem) {
		updateT(masterGenereItem);
	}

	@Override
	public void updateNonNull(MasterGenereItem masterGenereItem) {
		updateNonNullFieldsOfT(masterGenereItem, masterGenereItem.getId());
	}

	@Override
	public MasterGenereItem getById(String id) {
		return getTById(id);
	}

}
