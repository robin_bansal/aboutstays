package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.CityGuideItemEntity;
import com.aboutstays.mao.CityGuideItemMao;

@Repository
public class CityGuideItemMaoImpl extends AbstractMaoImpl<CityGuideItemEntity> implements CityGuideItemMao {

	public CityGuideItemMaoImpl() {
		super(CityGuideItemEntity.class);
	}
	
	@Override
	public void save(CityGuideItemEntity item) {
		saveT(item);
	}

	@Override
	public CityGuideItemEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public List<CityGuideItemEntity> getAll() {
		return getAllT();
	}

	@Override
	public void update(CityGuideItemEntity foodItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateNonNull(CityGuideItemEntity item) {
		updateNonNullFieldsOfT(item, item.getId());
	}

	@Override
	public List<CityGuideItemEntity> getAllByHotelId(String hotelId) {
		return find(Criteria.where(CityGuideItemEntity.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<CityGuideItemEntity> getAllByCityGuideId(String cityGuideId) {
		return find(Criteria.where(CityGuideItemEntity.FIELDS.CITY_GUIDE_ID).is(cityGuideId));
	}

	@Override
	public List<CityGuideItemEntity> getAllByCityGuideId(String cityGuideId, int code) {
		return find(Criteria.where(CityGuideItemEntity.FIELDS.CITY_GUIDE_ID).is(cityGuideId).and(CityGuideItemEntity.FIELDS.CITY_GUIDE_ITEM_TYPE).is(code));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}
	
	

}
