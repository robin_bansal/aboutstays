package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.RoomServicesEntity;
import com.aboutstays.mao.RoomServiceMao;

@Repository
public class RoomServiceMaoImpl extends AbstractMaoImpl<RoomServicesEntity> implements RoomServiceMao {

	public RoomServiceMaoImpl() {
		super(RoomServicesEntity.class);
	}

	@Override
	public void add(RoomServicesEntity roomService) {
		saveT(roomService);
	}

	@Override
	public List<RoomServicesEntity> getAll() {
		return getAllT();
	}

	@Override
	public RoomServicesEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(RoomServicesEntity service, String id) {
		updateNonNullFieldsOfT(service, id);
	}

	@Override
	public void update(RoomServicesEntity service) {
		update(service);
	}

}
