package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Driver;
import com.aboutstays.mao.DriverMao;

@Repository
public class DriverMaoImpl extends AbstractMaoImpl<Driver> implements DriverMao{

	public DriverMaoImpl(){
		super(Driver.class);
	}
	
	@Override
	public void add(Driver driver) {
		saveT(driver);	
	}

	@Override
	public List<Driver> getAll() {
		return getAllT();
	}

	@Override
	public Driver getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(Driver driver, String id) {
		updateNonNullFieldsOfT(driver, id);
	}

	
}
