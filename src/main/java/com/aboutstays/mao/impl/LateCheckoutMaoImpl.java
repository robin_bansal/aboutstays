package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.mao.LateCheckoutRequestMao;

@Repository
public class LateCheckoutMaoImpl extends AbstractMaoImpl<LateCheckoutRequest> implements LateCheckoutRequestMao{

	public LateCheckoutMaoImpl() {
		super(LateCheckoutRequest.class);
	}

	@Override
	public void add(LateCheckoutRequest order) {
		saveT(order);
		
	}

	@Override
	public LateCheckoutRequest get(String id) {
		return getTById(id);
	}

	@Override
	public List<LateCheckoutRequest> getAll() {
		return getAllT();
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNull(LateCheckoutRequest order) {
		updateNonNullFieldsOfT(order, order.getId());
		
	}

	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(LateCheckoutRequest.FIELDS.STATUS, status));
		
	}

	@Override
	public void update(LateCheckoutRequest request) {
		updateT(request);
		
	}

	@Override
	public LateCheckoutRequest retreiveLateCheckout(String userId, String hotelId, String staysId) {
		return findOne(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId).and(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId).and(BaseSessionEntity.FIELDS.STAYS_ID).is(staysId));
	}

}
