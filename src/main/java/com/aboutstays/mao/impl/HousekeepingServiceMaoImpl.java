package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.HousekeepingServiceEntity;
import com.aboutstays.mao.HousekeepingServiceMao;

@Repository
public class HousekeepingServiceMaoImpl extends AbstractMaoImpl<HousekeepingServiceEntity> implements HousekeepingServiceMao{

	public HousekeepingServiceMaoImpl() {
		super(HousekeepingServiceEntity.class);
	}
	
	@Override
	public void add(HousekeepingServiceEntity hkService) {
		saveT(hkService);
	}

	@Override
	public List<HousekeepingServiceEntity> getAll() {
		return getAllT();
	}

	@Override
	public HousekeepingServiceEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
		
	}

	@Override
	public void updateNonNullById(HousekeepingServiceEntity service, String id) {
		updateNonNullFieldsOfT(service, id);
	}

	@Override
	public void update(HousekeepingServiceEntity service) {
		updateT(service);
	}

}
