package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.GeneralInformation;
import com.aboutstays.mao.GeneralInfoMao;

@Repository
public class GeneralInfoMaoImpl extends AbstractMaoImpl<GeneralInformation> implements GeneralInfoMao{

	public GeneralInfoMaoImpl() {
		super(GeneralInformation.class);
	}
	
	@Override
	public void add(GeneralInformation generalInfo) {
		saveT(generalInfo);		
	}

	@Override
	public List<GeneralInformation> getAll() {
		return getAllT();
	}

	@Override
	public List<GeneralInformation> getAllByStaysStatus(Integer stayStatus) {
		return find(Criteria.where(GeneralInformation.FIELDS.STATUS).is(stayStatus));
	}

	@Override
	public GeneralInformation getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(GeneralInformation information, String id) {
		updateNonNullFieldsOfT(information, id);
	}

}
