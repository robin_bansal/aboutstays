package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.SuggestedHotels;
import com.aboutstays.mao.SuggestedHotelsMao;

@Repository
public class SuggestedHotelsMaoImpl extends AbstractMaoImpl<SuggestedHotels> implements SuggestedHotelsMao{

	public SuggestedHotelsMaoImpl() {
		super(SuggestedHotels.class);
	}

	@Override
	public void add(SuggestedHotels request) {
		saveT(request);
		
	}

	@Override
	public List<SuggestedHotels> getAll() {
		return getAllT();
	}

	@Override
	public SuggestedHotels getById(String id) {
		return getTById(id);
				
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
		
	}

	@Override
	public void updateNonNullById(SuggestedHotels request, String id) {
		updateNonNullFieldsOfT(request, id);
		
	}

}
