package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.FoodItem;
import com.aboutstays.mao.FoodItemMao;

@Repository
public class FoodItemMaoImpl extends AbstractMaoImpl<FoodItem> implements FoodItemMao {

	public FoodItemMaoImpl() {
		super(FoodItem.class);
	}

	@Override
	public void save(FoodItem foodItem) {
		saveT(foodItem);
	}

	@Override
	public FoodItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<FoodItem> getAll() {
		return getAllT();
	}

	@Override
	public void update(FoodItem foodItem) {
		updateT(foodItem);
	}

	@Override
	public void updateNonNull(FoodItem foodItem) {
		updateNonNullFieldsOfT(foodItem, foodItem.getId());
	}

	@Override
	public List<FoodItem> getAllByHotelId(String hotelId) {
		return find(Criteria.where(FoodItem.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<FoodItem> getAllByRoomServicesId(String roomServicesId) {
		return find(Criteria.where(FoodItem.FIELDS.ROOM_SERVICES_ID).is(roomServicesId));
	}

	@Override
	public List<FoodItem> getAllByRoomServicesId(String roomServicesId, int code) {
		return find(Criteria.where(FoodItem.FIELDS.ROOM_SERVICES_ID).is(roomServicesId).and(FoodItem.FIELDS.MENU_ITEM_TYPE).is(code));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

}
