package com.aboutstays.mao.impl;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.PickList;
import com.aboutstays.mao.PickListMao;

@Repository
public class PickListMaoImpl extends AbstractMaoImpl<PickList> implements PickListMao {

	public PickListMaoImpl() {
		super(PickList.class);
	}

	@Override
	public PickList getByType(int type) {
		return findOne(Criteria.where(PickList.FIELDS.TYPE).is(type));
	}

	@Override
	public void deleteByType(int type) {
		deleteT(Criteria.where(PickList.FIELDS.TYPE).is(type));
	}

	@Override
	public void addOrModify(PickList pickList) {
		upsert(Criteria.where(PickList.FIELDS.TYPE).is(pickList.getType()), new Update().set(PickList.FIELDS.TYPE, pickList.getType()).set(PickList.FIELDS.VALUES, pickList.getValues()));
	}

}
