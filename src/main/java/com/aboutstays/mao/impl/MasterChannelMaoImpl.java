package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.MasterChannelItem;
import com.aboutstays.mao.MasterChannelMao;

@Repository
public class MasterChannelMaoImpl extends AbstractMaoImpl<MasterChannelItem> implements MasterChannelMao {

	public MasterChannelMaoImpl() {
		super(MasterChannelItem.class);
	}

	@Override
	public void save(MasterChannelItem masterChannelItem) {
		saveT(masterChannelItem);
	}

	@Override
	public void saveAll(List<MasterChannelItem> masterChannelItems) {
		mongoTemplate.insertAll(masterChannelItems);
	}

	@Override
	public void update(MasterChannelItem masterChannelItem) {
		updateT(masterChannelItem);
	}

	@Override
	public MasterChannelItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<MasterChannelItem> getAll() {
		return getAllT();
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public List<MasterChannelItem> getAllByGenereId(String genereId) {
		return find(Criteria.where(MasterChannelItem.FIELDS.GENERE_ID).is(genereId));
	}

}
