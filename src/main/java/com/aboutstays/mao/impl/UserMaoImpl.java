package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.User;
import com.aboutstays.enums.Type;
import com.aboutstays.mao.UserMao;
import com.aboutstays.mao.utils.MaoUtility;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.PreferencesType;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Repository
public class UserMaoImpl extends AbstractMaoImpl<User> implements UserMao {

	public UserMaoImpl() {
		super(User.class);
	}

	@Override
	public void save(User user) {
		saveT(user);
		
	}

	@Override
	public User getById(String id) {
		return getTById(id);
	}

	@Override
	public void update(User user) {
		saveT(user);
	}

	@Override
	public List<User> getAll() {
		return getAllT();
	}

	@Override
	public User getByEmailOrPhone(String emailId, String phoneNumber) {
		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where(User.FIELDS.EMAIL_ID).is(emailId), Criteria.where(User.FIELDS.MOBILE_NUMBER).is(phoneNumber));
		return findOne(criteria);
	}

	@Override
	public User getByPhoneNumber(String number) {
		return findOne(Criteria.where(User.FIELDS.MOBILE_NUMBER).is(number));
	}

	@Override
	public User matchCredentials(Type typeEnum, String value) {
		switch(typeEnum){
			case PHONE:
				return getByPhoneNumber(value);
			case EMAIL:
				return getByEmail(value);
			case USERID:
				return getByUserId(value);
			default:
				return null;
		}
	}

	@Override
	public User getByEmail(String email) {
		return findOne(Criteria.where(User.FIELDS.EMAIL_ID).is(email));
	}

	@Override
	public void updateOtpByEmail(String emailId) {
		Update update = new Update();
		update.set(User.FIELDS.OTP_VALIDATED, true);
		updateT(Criteria.where(User.FIELDS.EMAIL_ID).is(emailId), update);
	}

	@Override
	public void updateOtpByNumber(String number) {
		Update update = new Update();
		update.set(User.FIELDS.OTP_VALIDATED, true);
		updateT(Criteria.where(User.FIELDS.MOBILE_NUMBER).is(number), update);
	}

	@Override
	public void updateNonNullFields(User user, Type typeEnum, String value) {
		DBObject dbObject = new BasicDBObject();
		mongoTemplate.getConverter().write(user,dbObject);
		Update update = MaoUtility.fromDBObjectExcludeNonNullFields(dbObject);
		
		switch(typeEnum){
			case PHONE:
				upsert(Criteria.where(User.FIELDS.MOBILE_NUMBER).is(value), update);
				break;
			case EMAIL:
				upsert(Criteria.where(User.FIELDS.EMAIL_ID).is(value), update);
				break;
			case USERID:
				upsert(Criteria.where(User.FIELDS._ID).is(value), update);
				break;
		}
	}

	@Override
	public User getByUserId(String userId) {
		return findOne(Criteria.where(User.FIELDS._ID).is(userId));
	}

	@Override
	public void updatePasswordByType(User user, Type typeEnum, String value) {
		Update update = new Update();
		switch(typeEnum){
		case PHONE:
			update.set(User.FIELDS.PASSWORD, user.getPassword());
			updateT(Criteria.where(User.FIELDS.MOBILE_NUMBER).is(value), update);
			break;
		case EMAIL:
			update.set(User.FIELDS.PASSWORD, user.getPassword());
			updateT(Criteria.where(User.FIELDS.EMAIL_ID).is(value), update);
			break;
		case USERID:
			update.set(User.FIELDS.PASSWORD, user.getPassword());
			updateT(Criteria.where(User.FIELDS._ID).is(value), update);
			break;
		}	
	}


	@Override
	public void editProfileByUserID(User user, String userId) {
		updateNonNullFieldsOfT(user, userId);
	}

	@Override
	public void updateDocs(List<IdentityDoc> identityDocs, String userId) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(userId), new Update().set(User.FIELDS.IDENTITY_DOCS, identityDocs));
	}

	@Override
	public void updateProfilePicture(String userId, String imageUrl) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(userId), new Update().set(User.FIELDS.IMAGE_URL, imageUrl));
	}

	@Override
	public void update(String userId, List<PreferencesType> listStayPrefs) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(userId), new Update().set(User.FIELDS.STAY_PREFERENCES, listStayPrefs));
	}

}
