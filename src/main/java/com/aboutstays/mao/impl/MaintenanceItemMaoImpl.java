package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.MaintenanceItem;
import com.aboutstays.mao.MaintenanceItemMao;

@Repository
public class MaintenanceItemMaoImpl extends AbstractMaoImpl<MaintenanceItem> implements MaintenanceItemMao{

	public MaintenanceItemMaoImpl() {
		super(MaintenanceItem.class);
	}

	@Override
	public void save(MaintenanceItem item) {
		saveT(item);
		
	}

	@Override
	public MaintenanceItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<MaintenanceItem> getAll() {
		return getAllT();
	}

	@Override
	public void update(MaintenanceItem foodItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateNonNull(MaintenanceItem item, String id) {
		updateNonNullFieldsOfT(item, id);
	}

	@Override
	public List<MaintenanceItem> getAllByHotelId(String hotelId) {
		return find(Criteria.where(MaintenanceItem.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<MaintenanceItem> getAllByMTServicesId(String mtServiceId) {
		return find(Criteria.where(MaintenanceItem.FIELDS.MAINTENANCE_SERVICE_ID).is(mtServiceId));
	}

	@Override
	public List<MaintenanceItem> fetchByType(String mtServiceId, int code) {
		return find(Criteria.where(MaintenanceItem.FIELDS.MAINTENANCE_SERVICE_ID).is(mtServiceId).and(MaintenanceItem.FIELDS.ITEM_TYPE).is(code));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

}
