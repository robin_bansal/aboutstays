package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Wing;
import com.aboutstays.mao.WingMao;

@Repository
public class WingMaoImpl extends AbstractMaoImpl<Wing> implements WingMao {

	public WingMaoImpl() {
		super(Wing.class);
	}

	@Override
	public void add(Wing wing) {
		saveT(wing);
	}

	@Override
	public List<Wing> getAll() {
		return getAllT();
	}

	@Override
	public Wing getWingById(String wingId) {
		return getTById(wingId);
	}

	@Override
	public void updateById(Wing wing,String wingId) {
		updateNonNullFieldsOfT(wing, wingId);
	}

	@Override
	public void delete(String wingId) {
		deleteT(wingId);
	}

	@Override
	public List<Wing> getByHotelId(String hotelId) {
		return find(Criteria.where(Wing.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public Wing getByWingAndHotelId(String wingId, String hotelId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(wingId).and(Wing.FIELDS.HOTEL_ID).is(hotelId));
	}
}
