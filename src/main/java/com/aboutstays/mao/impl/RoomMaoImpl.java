package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Room;
import com.aboutstays.mao.RoomMao;

@Repository
public class RoomMaoImpl extends AbstractMaoImpl<Room> implements RoomMao {

	public RoomMaoImpl() {
		super(Room.class);
	}

	@Override
	public void add(Room room) {
		saveT(room);
	}

	@Override
	public List<Room> getAll() {
		return getAllT();
	}

	@Override
	public Room getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(Room room, String id) {
		updateNonNullFieldsOfT(room, id);
	}

	@Override
	public void update(Room room) {
		updateT(room);
	}

	@Override
	public List<Room> getByRoomCategory(String roomCategoryId) {
		return find(Criteria.where(Room.FIELDS.ROOM_CATEGORY_ID).is(roomCategoryId));
	}

	@Override
	public List<Room> getByFloorId(String floorId) {
		return find(Criteria.where(Room.FIELDS.FLOOOR_ID).is(floorId));
	}

	@Override
	public List<Room> getByWingId(String wingId) {
		return find(Criteria.where(Room.FIELDS.WING_ID).is(wingId));
	}

	@Override
	public List<Room> getByHotelId(String hotelId) {
		return find(Criteria.where(Room.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public int getCountByWingId(String wingId) {
		return (int) count(Criteria.where(Room.FIELDS.WING_ID).is(wingId));
	}

	@Override
	public int getCountByFloorId(String floorId) {
		return (int) count(Criteria.where(Room.FIELDS.FLOOOR_ID).is(floorId));
	}

	@Override
	public void deleteRooms(String hotelId, List<String> roomIds) {
		deleteT(Criteria.where(Fields.UNDERSCORE_ID).in(roomIds).and(Room.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public Room getById(String hotelId, String roomId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(roomId).and(Room.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public void updateRoomFields(String roomId, String roomCategoryId, String roomNumber, Integer panelUiOrder) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(roomId),new Update().set(Room.FIELDS.ROOM_CATEGORY_ID, roomCategoryId).set(Room.FIELDS.ROOM_NUMBER, roomNumber).set(Room.FIELDS.PANEL_UI_ORDER, panelUiOrder));
	}

}
