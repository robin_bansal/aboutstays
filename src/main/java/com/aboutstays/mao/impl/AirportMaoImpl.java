package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Airport;
import com.aboutstays.entities.Airport.FIELDS;
import com.aboutstays.mao.AirportMao;

@Repository
public class AirportMaoImpl extends AbstractMaoImpl<Airport> implements AirportMao {

	public AirportMaoImpl() {
		super(Airport.class);
	}

	@Override
	public void save(Airport airport) {
		saveT(airport);
	}

	@Override
	public void update(Airport airport) {
		updateT(airport);
	}

	@Override
	public void updateNonNull(Airport airport) {
		updateNonNullFieldsOfT(airport, airport.getAirportId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public Airport getById(String id) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(id));
	}

	@Override
	public List<Airport> getAll() {
		return find(new Query());
	}

	@Override
	public List<Airport> getByCityId(String cityId) {
		return find(Criteria.where(FIELDS.CITY_ID).is(cityId));
	}

}
