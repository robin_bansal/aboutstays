package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ServiceFeedback;
import com.aboutstays.mao.ServiceFeedbackMao;

@Repository
public class ServiceFeedbackMaoImpl extends AbstractMaoImpl<ServiceFeedback> implements ServiceFeedbackMao{

	public ServiceFeedbackMaoImpl() {
		super(ServiceFeedback.class);
	}

	@Override
	public void add(ServiceFeedback request) {
		saveT(request);
	}

	@Override
	public ServiceFeedback get(String id) {
		return getTById(id);
	}

	@Override
	public List<ServiceFeedback> getAll() {
		return getAllT();
	}

	@Override
	public void update(ServiceFeedback request) {
		updateT(request);
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNull(ServiceFeedback request) {
		updateNonNullFieldsOfT(request, request.getId());
	}

	@Override
	public ServiceFeedback getByUosId(String uosId) {
		return findOne(Criteria.where(ServiceFeedback.FIELDS.UOS_ID).is(uosId));
		
	}
	
	

}
