package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.LaundryServiceEntity;
import com.aboutstays.mao.LaundryServicesMao;

@Repository
public class LaundryServicesMaoImpl extends AbstractMaoImpl<LaundryServiceEntity> implements LaundryServicesMao {

	public LaundryServicesMaoImpl() {
		super(LaundryServiceEntity.class);
	}

	@Override
	public void save(LaundryServiceEntity laundryServiceEntity) {
		saveT(laundryServiceEntity);
	}

	@Override
	public LaundryServiceEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public List<LaundryServiceEntity> getAll() {
		return getAllT();
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void update(LaundryServiceEntity entity) {
		updateT(entity);
	}

	@Override
	public void updateNonNull(LaundryServiceEntity entity) {
		updateNonNullFieldsOfT(entity, entity.getHotelId());
	}

}
