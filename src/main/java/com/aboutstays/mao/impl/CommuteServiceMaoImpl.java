package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.CommuteService;
import com.aboutstays.mao.CommuteServiceMao;

@Repository
public class CommuteServiceMaoImpl extends AbstractMaoImpl<CommuteService> implements CommuteServiceMao{

	public CommuteServiceMaoImpl() {
		super(CommuteService.class);
	}

	@Override
	public void add(CommuteService commuteService) {
		saveT(commuteService);
	}

	@Override
	public List<CommuteService> getAll() {
		return getAllT();
	}

	@Override
	public CommuteService getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(CommuteService service, String id) {
		updateNonNullFieldsOfT(service, id);
	}

}
