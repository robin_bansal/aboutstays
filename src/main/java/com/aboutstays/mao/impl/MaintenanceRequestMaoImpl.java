package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.mao.MaintenanceRequestMao;

@Repository
public class MaintenanceRequestMaoImpl extends AbstractMaoImpl<MaintenanceRequestEntity> implements MaintenanceRequestMao{

	public MaintenanceRequestMaoImpl() {
		super(MaintenanceRequestEntity.class);
	}

	@Override
	public void save(MaintenanceRequestEntity entity) {
		saveT(entity);
	}

	@Override
	public MaintenanceRequestEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public List<MaintenanceRequestEntity> getAll() {
		return getAllT();
	}

	@Override
	public void updateNonNull(MaintenanceRequestEntity request) {
		updateNonNullFieldsOfT(request, request.getId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(MaintenanceRequestEntity.FIELDS.STATUS, status));
	}

}
