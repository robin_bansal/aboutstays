package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.mao.LaundryOrderMao;

@Repository
public class LaundryOrderMaoImpl extends AbstractMaoImpl<LaundryOrder> implements LaundryOrderMao {

	public LaundryOrderMaoImpl() {
		super(LaundryOrder.class);
	}
	
	@Override
	public void save(LaundryOrder laundryOrder) {
		saveT(laundryOrder);
		
	}

	@Override
	public LaundryOrder getById(String id) {
		return getTById(id);
	}

	@Override
	public List<LaundryOrder> getAll() {
		return getAllT();
	}

	@Override
	public List<LaundryOrder> getAllByUserId(String userId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId));
	}

	@Override
	public List<LaundryOrder> getAllByHotelId(String hotelId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<LaundryOrder> getAllByUserAndStayId(String userId, String stayId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId).and(BaseSessionEntity.FIELDS.STAYS_ID).is(stayId));
	}

	@Override
	public void update(LaundryOrder request) {
		updateT(request);
	}

	@Override
	public void updateNonNull(LaundryOrder request) {
		updateNonNullFieldsOfT(request, request.getId());		
	}

	@Override
	public void delete(String id) {
		deleteT(id);		
	}
	
	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(LaundryOrder.FIELDS.STATUS, status));
	}



}
