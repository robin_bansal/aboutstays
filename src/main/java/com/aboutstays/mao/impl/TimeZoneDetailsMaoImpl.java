package com.aboutstays.mao.impl;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.TimeZoneDetails;
import com.aboutstays.mao.TimeZoneDetailsMao;

@Repository
public class TimeZoneDetailsMaoImpl extends AbstractMaoImpl<TimeZoneDetails> implements TimeZoneDetailsMao {

	public TimeZoneDetailsMaoImpl() {
		super(TimeZoneDetails.class);
	}

	public TimeZoneDetails getByTimezone(String timezone) {
		return findOne(Criteria.where(TimeZoneDetails.FIELDS.TIMEZONE).is(timezone));
	}

}
