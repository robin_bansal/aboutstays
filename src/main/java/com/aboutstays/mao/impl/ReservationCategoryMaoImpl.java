package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ReservationCategory;
import com.aboutstays.mao.ReservationCategoryMao;

@Repository
public class ReservationCategoryMaoImpl extends AbstractMaoImpl<ReservationCategory>
		implements ReservationCategoryMao {

	public ReservationCategoryMaoImpl() {
		super(ReservationCategory.class);
	}

	@Override
	public List<ReservationCategory> getByType(int reservationType) {
		return find(Criteria.where(ReservationCategory.FIELDS.RESERVATION_TYPE).is(reservationType));
	}

	@Override
	public List<ReservationCategory> getByHotelIdAndType(String hotelId, int code) {
		return find(Criteria.where(ReservationCategory.FIELDS.HOTEL_ID).is(hotelId).and(ReservationCategory.FIELDS.RESERVATION_TYPE).is(code));
	}

	@Override
	public List<ReservationCategory> getByHotelId(String hotelId) {
		return find(Criteria.where(ReservationCategory.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public ReservationCategory getByIdAndHotelId(String categoryId, String hotelId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(categoryId).and(ReservationCategory.FIELDS.HOTEL_ID).is(hotelId));
	}

}
