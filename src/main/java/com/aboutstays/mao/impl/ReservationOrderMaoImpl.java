package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.mao.ReservationOrderMao;

@Repository
public class ReservationOrderMaoImpl extends AbstractMaoImpl<ReservationOrder> implements ReservationOrderMao{

	public ReservationOrderMaoImpl() {
		super(ReservationOrder.class);
	}

	@Override
	public void save(ReservationOrder entity) {
		saveT(entity);
	}

	@Override
	public ReservationOrder getById(String id) {
		return getTById(id);
	}

	@Override
	public List<ReservationOrder> getAll() {
		return getAllT();
	}

	@Override
	public void updateNonNull(ReservationOrder request) {
		updateNonNullFieldsOfT(request, request.getId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(ReservationOrder.FIELDS.STATUS, status));
	}
	

}
