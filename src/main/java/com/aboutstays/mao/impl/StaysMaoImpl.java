package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Stays;
import com.aboutstays.mao.StaysMao;

@Repository
public class StaysMaoImpl extends AbstractMaoImpl<Stays> implements StaysMao {

	public StaysMaoImpl(){
		super(Stays.class);
	}
	
	@Override
	public void addStays(Stays stays) {
		saveT(stays);
	}

	@Override
	public Stays getByStaysId(String staysId) {
		if(StringUtils.isEmpty(staysId)){
			return null;
		}
		return findOne(Criteria.where(Stays.FIELDS._ID).is(staysId));
	}

	@Override
	public void deleteStays(String staysId) {
		deleteT(staysId);
	}

	@Override
	public List<Stays> getAllStays() {
		return getAllT();
	}

	@Override
	public List<Stays> getStaysByUserId(String userId) {
		return find(Criteria.where(Stays.FIELDS.USER_ID).is(userId));
	}

	@Override
	public Stays getByBookingId(String bookingId) {
		return findOne(Criteria.where(Stays.FIELDS.BOOKING_ID).is(bookingId));
	}

	@Override
	public List<Stays> getStaysOfBooking(String bookingId) {
		return find(Criteria.where(Stays.FIELDS.BOOKING_ID).is(bookingId));
	}
	
	public Stays getStaysByUserAndBookingId(String userId, String bookingId) {
		return findOne(Criteria.where(Stays.FIELDS.BOOKING_ID).is(bookingId).and(Stays.FIELDS.USER_ID).is(userId));
	}

	@Override
	public void changeStatus(String staysId, Integer code) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(staysId), new Update().set(Stays.FIELDS.STAYS_STATUS, code));
	}

	@Override
	public void changeTripType(String staysId, boolean isOfficial) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(staysId), new Update().set(Stays.FIELDS.OFFICIAL, isOfficial).set(Stays.FIELDS.UPDATED, System.currentTimeMillis()));
	}

	@Override
	public List<Stays> getStaysByRoomId(String roomId) {
		return find(Criteria.where(Stays.FIELDS.ROOM_ID).is(roomId));
	}

	@Override
	public void update(Stays stays) {
		updateT(stays);
	}

	@Override
	public void setRoomId(String staysId, String roomId) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(staysId), new Update().set(Stays.FIELDS.ROOM_ID, roomId).set(Stays.FIELDS.UPDATED, System.currentTimeMillis()));
	}
}
