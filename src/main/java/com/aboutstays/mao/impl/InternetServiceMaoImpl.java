package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.InternetService;
import com.aboutstays.mao.InternetServiceMao;

@Repository
public class InternetServiceMaoImpl extends AbstractMaoImpl<InternetService> implements InternetServiceMao{

	public InternetServiceMaoImpl() {
		super(InternetService.class);
	}
	
	@Override
	public void add(InternetService packs) {
		saveT(packs);
	}

	@Override
	public List<InternetService> getAll() {
		return getAllT();
	}


	@Override
	public InternetService getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(InternetService packs, String id) {
		updateNonNullFieldsOfT(packs, id);
	}

	@Override
	public void update(InternetService service) {
		updateT(service);
	}

}
