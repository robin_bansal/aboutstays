package com.aboutstays.mao.impl;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ProcessPropertyInfo;
import com.aboutstays.mao.ProcessPropertyInfoMao;

@Repository
public class ProcessPropertyInfoMaoImpl extends AbstractMaoImpl<ProcessPropertyInfo> implements ProcessPropertyInfoMao{

	public ProcessPropertyInfoMaoImpl() {
		super(ProcessPropertyInfo.class); 
	}
	
	@Override
	public ProcessPropertyInfo getProcessPropertyValue(String processName, String propertyaName) {
		return findOne(Criteria.where(ProcessPropertyInfo.FIELDS.PROCESS_NAME).is(processName).and(ProcessPropertyInfo.FIELDS.PROPERTY_NAME).is(propertyaName));
	}

}
