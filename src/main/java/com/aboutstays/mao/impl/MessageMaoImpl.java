package com.aboutstays.mao.impl;

import java.util.Date;
import java.util.List;


import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Message;
import com.aboutstays.entities.MessageStatus;
import com.aboutstays.entities.Notification.FIELDS;
import com.aboutstays.mao.MessageMao;

@Repository
public class MessageMaoImpl extends AbstractMaoImpl<Message> implements MessageMao {

	public MessageMaoImpl() {
		super(Message.class);
	}

	@Override
	public void save(Message message) {
		saveT(message);
	}

	@Override
	public void update(Message message) {
		updateT(message);
	}

	@Override
	public void updateNonNull(Message message) {
		updateNonNullFieldsOfT(message, message.getId());
	}

	@Override
	public Message getById(String id) {
		return getTById(id);
	}

	@Override
	public List<Message> getAll() {
		return getAllT();
	}

	@Override
	public List<Message> getAll(String userId, String staysId, String hotelId) {
		return find(Criteria.where(FIELDS.USER_ID).is(userId).and(FIELDS.STAYS_ID).is(staysId).and(FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void updateMessageStatus(String userId, String hotelId, int creationType, Date date, int messageStatus) {
		updateAllT(Criteria.where(Message.FIELDS.USER_ID).is(userId).and(Message.FIELDS.HOTEL_ID).is(hotelId).and(Message.FIELDS.CREATION_TYPE).is(creationType).and(Message.FIELDS.STATUS).ne(MessageStatus.READ.getStatus()).and(Message.FIELDS.CREATED).lte(date.getTime()), new Update().set(Message.FIELDS.STATUS,messageStatus));
	}

	@Override
	public List<Message> getAllByHotelId(String hotelId) {
		return find(Criteria.where(FIELDS.HOTEL_ID).is(hotelId));
	}

}
