package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.LoyaltyPrograms;
import com.aboutstays.mao.LoyaltyProgramsMao;

@Repository
public class LoyaltyProgramMaoImp extends AbstractMaoImpl<LoyaltyPrograms> implements LoyaltyProgramsMao{

	public LoyaltyProgramMaoImp() {
		super(LoyaltyPrograms.class);
	}

	@Override
	public void add(LoyaltyPrograms loyalyPrograms) {
		saveT(loyalyPrograms);
	}

	@Override
	public void update(LoyaltyPrograms loyalyPrograms) {
		updateNonNullFieldsOfT(loyalyPrograms, loyalyPrograms.getId());
	}

	@Override
	public List<LoyaltyPrograms> getAll() {
		return getAllT();
	}

	@Override
	public LoyaltyPrograms getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

}
