package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BellBoyServiceEntity;
import com.aboutstays.mao.BellBoyServiceMao;

@Repository
public class BellBoyServiceMaoImpl extends AbstractMaoImpl<BellBoyServiceEntity> implements BellBoyServiceMao {

	public BellBoyServiceMaoImpl() {
		super(BellBoyServiceEntity.class);
	}

	@Override
	public void save(BellBoyServiceEntity entity) {
		saveT(entity);
	}

	@Override
	public void update(BellBoyServiceEntity entity) {
		updateT(entity);
	}

	@Override
	public void updateNonNull(BellBoyServiceEntity entity) {
		updateNonNullFieldsOfT(entity, entity.getHotelId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public List<BellBoyServiceEntity> getAll() {
		return find(new Query());
	}

	@Override
	public BellBoyServiceEntity getByHotelId(String hotelId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(hotelId));
	}

}
