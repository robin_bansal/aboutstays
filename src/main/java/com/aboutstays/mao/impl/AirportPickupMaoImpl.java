package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.AirportPickup;
import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.PickupStatus;
import com.aboutstays.mao.AirportPickupMao;

@Repository
public class AirportPickupMaoImpl extends AbstractMaoImpl<AirportPickup> implements AirportPickupMao{

	public AirportPickupMaoImpl(){
		super(AirportPickup.class);
	}
	
	@Override
	public void add(AirportPickup airportPickup) {
		saveT(airportPickup);
	}

	@Override
	public List<AirportPickup> getAll() {
		return getAllT();   
	}

	@Override
	public AirportPickup getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(AirportPickup airportPickup, String id) {
		updateNonNullFieldsOfT(airportPickup, id);
	}

	@Override
	public List<AirportPickup> getAllByUserId(String userId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId));
	}

	@Override
	public void changeOrderStatus(String id, GeneralOrderStatus status) {
		Update update = new Update();
		if(status == GeneralOrderStatus.CANCELLED){
			update.set(AirportPickup.FIELDS.PICKUP_STATUS, PickupStatus.CANCELLED.getCode());
		}
		update.set(AirportPickup.FIELDS.STATUS, status.getCode());
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), update);
	}
	
	

}
