package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ComplementaryServicesEntity;
import com.aboutstays.mao.ComplementaryServicesMao;

@Repository
public class ComplementaryServicesMaoImpl extends AbstractMaoImpl<ComplementaryServicesEntity>
		implements ComplementaryServicesMao {

	public ComplementaryServicesMaoImpl() {
		super(ComplementaryServicesEntity.class);
	}

	@Override
	public void save(ComplementaryServicesEntity entity) {
		saveT(entity);
	}

	@Override
	public void update(ComplementaryServicesEntity entity) {
		updateT(entity);
	}

	@Override
	public ComplementaryServicesEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public List<ComplementaryServicesEntity> getAll() {
		return getAllT();
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

}
