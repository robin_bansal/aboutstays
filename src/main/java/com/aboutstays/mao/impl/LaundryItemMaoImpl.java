package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.LaundryItem;
import com.aboutstays.mao.LaundryItemMao;

@Repository
public class LaundryItemMaoImpl extends AbstractMaoImpl<LaundryItem> implements LaundryItemMao {

	public LaundryItemMaoImpl() {
		super(LaundryItem.class);
	}

	@Override
	public void save(LaundryItem laundryItem) {
		saveT(laundryItem);
	}

	@Override
	public LaundryItem getById(String id) {
		return getTById(id);
	}

	@Override
	public List<LaundryItem> getAll() {
		return getAllT();
	}

	@Override
	public List<LaundryItem> getAllByServicesId(String servicesId) {
		return find(Criteria.where(LaundryItem.FIELDS.LAUNDRY_SERVICES_ID).is(servicesId));
	}

	@Override
	public List<LaundryItem> getAllByHotelId(String hotelId) {
		return find(Criteria.where(LaundryItem.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public void update(LaundryItem laundryItem) {
		updateT(laundryItem);
	}

	@Override
	public void updateNonNull(LaundryItem laundryItem) {
		updateNonNullFieldsOfT(laundryItem, laundryItem.getId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public List<LaundryItem> getByType(String laundryServicesId, int laundryItemType) {
		return find(Criteria.where(LaundryItem.FIELDS.LAUNDRY_SERVICES_ID).is(laundryServicesId).and(LaundryItem.FIELDS.ITEM_TYPE).is(laundryItemType));
	}

}
