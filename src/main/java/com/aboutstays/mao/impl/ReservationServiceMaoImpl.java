package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.ReservationServiceEntity;
import com.aboutstays.mao.ReservationServiceMao;

@Repository
public class ReservationServiceMaoImpl extends AbstractMaoImpl<ReservationServiceEntity> implements ReservationServiceMao{

	public ReservationServiceMaoImpl() {
		super(ReservationServiceEntity.class);
	}

	@Override
	public void add(ReservationServiceEntity entity) {
		saveT(entity);
		
	}

	@Override
	public List<ReservationServiceEntity> getAll() {
		return getAllT();
	}

	@Override
	public ReservationServiceEntity getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(ReservationServiceEntity entity, String id) {
		updateNonNullFieldsOfT(entity, id);
	}
	
}
