package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.InternetOrder;
import com.aboutstays.mao.InternetOrderMao;

@Repository
public class InternetOrderMaoImpl extends AbstractMaoImpl<InternetOrder> implements InternetOrderMao{

	public InternetOrderMaoImpl() {
		super(InternetOrder.class);
	}

	@Override
	public void add(InternetOrder order) {
		saveT(order);
	}

	@Override
	public InternetOrder get(String id) {
		return getTById(id);
	}

	@Override
	public List<InternetOrder> getAll() {
		return getAllT();
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNull(InternetOrder order) {
		updateNonNullFieldsOfT(order, order.getId());
	}
	
	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(InternetOrder.FIELDS.STATUS, status));
		
	}

}
