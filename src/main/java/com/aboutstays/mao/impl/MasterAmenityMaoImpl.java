package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.MasterAmenity;
import com.aboutstays.mao.MasterAmenityMao;

@Repository
public class MasterAmenityMaoImpl extends AbstractMaoImpl<MasterAmenity> implements MasterAmenityMao {

	public MasterAmenityMaoImpl() {
		super(MasterAmenity.class);
	}

	@Override
	public void save(MasterAmenity masterAmenity) {
		saveT(masterAmenity);
	}

	@Override
	public void update(MasterAmenity masterAmenity) {
		updateT(masterAmenity);
	}

	@Override
	public MasterAmenity getById(String id) {
		return getTById(id);
	}

	@Override
	public List<MasterAmenity> getAll() {
		return getAllT();
	}

	@Override
	public List<MasterAmenity> getAll(int type) {
		return find(Criteria.where(MasterAmenity.FIELDS.TYPE).is(type));
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void saveAll(List<MasterAmenity> amenitites) {
		mongoTemplate.insertAll(amenitites);
	}

	@Override
	public MasterAmenity getByIdAndType(String amenityId, int type) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(amenityId).and(MasterAmenity.FIELDS.TYPE).is(type));
	}

}
