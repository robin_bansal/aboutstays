package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Car;
import com.aboutstays.mao.CarMao;

@Repository
public class CarMaoImpl extends AbstractMaoImpl<Car> implements CarMao{

	public CarMaoImpl(){
		super(Car.class);
	}
	
	@Override
	public void add(Car car) {
		saveT(car);		
	}

	@Override
	public List<Car> getAll() {
		return getAllT();
	}

	@Override
	public Car getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);		
	}

	@Override
	public void updateNonNullById(Car car, String id) {
		updateNonNullFieldsOfT(car, id);
	}

	@Override
	public Car getByNumberPlate(String numberPlate) {
		return findOne(Criteria.where(Car.FIELDS.NUMBER_PLATE).is(numberPlate));
	}

}
