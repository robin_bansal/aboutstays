package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.entities.UserOptedService;
import com.aboutstays.mao.UserOptedServicesMao;
import com.aboutstays.request.dto.GetUOSByUserRequest;

@Repository
public class UserOptedServicesMaoImpl extends AbstractMaoImpl<UserOptedService> implements UserOptedServicesMao{

	public UserOptedServicesMaoImpl() {
		super(UserOptedService.class);
	}
	
	@Override
	public void add(UserOptedService service) {
		saveT(service);
	}

	@Override
	public List<UserOptedService> getAll() {
		return getAllT();
	}

	@Override
	public UserOptedService getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
	}

	@Override
	public void updateNonNullById(UserOptedService service, String id) {
		updateNonNullFieldsOfT(service, id);
	}

	@Override
	public List<UserOptedService> getAllByUserId(String userId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId));
	}

	@Override
	public List<UserOptedService> getByUserAndStay(GetUOSByUserRequest request) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(request.getUserId()).and(BaseSessionEntity.FIELDS.STAYS_ID).is(request.getStayId()));
	}

	@Override
	public List<UserOptedService> getByUserStayAndHotel(String staysId, String userId, String hotelId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.STAYS_ID).is(staysId).and(BaseSessionEntity.FIELDS.USER_ID).is(userId).and(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public void update(UserOptedService request) {
		updateT(request);
	}

	@Override
	public UserOptedService getByRefId(String refId) {
		return findOne(Criteria.where(UserOptedService.FIELDS.REF_ID).is(refId));
	}

	@Override
	public List<UserOptedService> getAllByHotelIdAndCreated(String hotelId, long time) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId).and(UserOptedService.FIELDS.CREATED).gt(time));
	}

	@Override
	public List<UserOptedService> getAllByHotelIdAndCreatedAndStatus(String hotelId, long time,Integer status) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId).and(UserOptedService.FIELDS.REF_COLLECTION_TYPE).is(status).and(UserOptedService.FIELDS.CREATED).gt(time));
	}



}
