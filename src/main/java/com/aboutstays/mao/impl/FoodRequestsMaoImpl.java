package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.entities.FoodOrder;
import com.aboutstays.mao.FoodRequestsMao;

@Repository
public class FoodRequestsMaoImpl extends AbstractMaoImpl<FoodOrder> implements FoodRequestsMao{

	public FoodRequestsMaoImpl() {
		super(FoodOrder.class);
	}

	@Override
	public void save(FoodOrder foodOrderRequest) {
		saveT(foodOrderRequest);
	}

	@Override
	public FoodOrder getById(String id) {
		return getTById(id);
	}

	@Override
	public List<FoodOrder> getAll() {
		return getAllT();
	}

	@Override
	public List<FoodOrder> getAllByUserId(String userId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId));
	}

	@Override
	public List<FoodOrder> getAllByHotelId(String hotelId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<FoodOrder> getAllByUserAndStayId(String userId, String stayId) {
		return find(Criteria.where(BaseSessionEntity.FIELDS.USER_ID).is(userId).and(BaseSessionEntity.FIELDS.STAYS_ID).is(stayId));
	}

	@Override
	public void update(FoodOrder request) {
		updateT(request);
	}

	@Override
	public void updateNonNull(FoodOrder request) {
		updateNonNullFieldsOfT(request, request.getId());
	}

	@Override
	public void delete(String id) {
		deleteT(id);
	}

	@Override
	public void changeOrderStatus(String id, int status) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(id), new Update().set(FoodOrder.FIELDS.STATUS, status));
	}

}
