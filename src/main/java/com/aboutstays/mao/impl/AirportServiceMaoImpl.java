package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.AirportServices;
import com.aboutstays.mao.AirportServiceMao;

@Repository
public class AirportServiceMaoImpl extends AbstractMaoImpl<AirportServices> implements AirportServiceMao{

	public AirportServiceMaoImpl(){
		super(AirportServices.class);
	}
	
	@Override
	public void add(AirportServices airportServices) {
		saveT(airportServices);
		
	}

	@Override
	public List<AirportServices> getAll() {
		return getAllT();
	}

	@Override
	public AirportServices getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
		
	}

	@Override
	public void updateNonNullById(AirportServices service, String id) {
		updateNonNullFieldsOfT(service, id);
		
	}

}
