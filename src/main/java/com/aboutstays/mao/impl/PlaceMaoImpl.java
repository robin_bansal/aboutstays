package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Place;
import com.aboutstays.mao.PlaceMao;

@Repository
public class PlaceMaoImpl extends AbstractMaoImpl<Place> implements PlaceMao {

	public PlaceMaoImpl() {
		super(Place.class);
	}

	@Override
	public void save(Place place) {
		saveT(place);
	}

	@Override
	public void update(Place place) {
		updateT(place);
	}

	@Override
	public void updateNonNull(Place place) {
		updateNonNullFieldsOfT(place, place.getPlaceId());
	}

	@Override
	public Place getById(String placeId) {
		return findOne(Criteria.where(Fields.UNDERSCORE_ID).is(placeId));
	}

	@Override
	public List<Place> getByParentId(String parentPlaceId) {
		return find(Criteria.where(Place.FIELDS.PARENT_PLACE_ID).is(parentPlaceId));
	}

	@Override
	public List<Place> getAll() {
		return find(new Query());
	}

	@Override
	public void delete(String placeId) {
		deleteT(placeId);
	}

	@Override
	public List<Place> getByType(int placeType) {
		return find(Criteria.where(Place.FIELDS.PLACE_TYPE).is(placeType));
	}

}
