package com.aboutstays.mao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aboutstays.entities.HouseRules;
import com.aboutstays.mao.HouseRulesMao;

@Repository
public class HouseRulesMaoImpl extends AbstractMaoImpl<HouseRules> implements HouseRulesMao {

	public HouseRulesMaoImpl() {
		super(HouseRules.class);
	}
	
	@Override
	public void add(HouseRules rules) {
		saveT(rules);
	}

	@Override
	public List<HouseRules> getAll() {
		return getAllT();
	}

	@Override
	public HouseRules getById(String id) {
		return getTById(id);
	}

	@Override
	public void deletyById(String id) {
		deleteT(id);
		
	}

	@Override
	public void updateNonNullById(HouseRules rules) {
		updateNonNullFieldsOfT(rules, rules.getHotelId());
		
	}
}
