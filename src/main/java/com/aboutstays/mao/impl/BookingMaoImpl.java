package com.aboutstays.mao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.aboutstays.entities.Booking;
import com.aboutstays.entities.Booking.FIELDS;
import com.aboutstays.enums.BookingStatus;
import com.aboutstays.mao.BookingMao;

@Repository
public class BookingMaoImpl extends AbstractMaoImpl<Booking> implements BookingMao {

	
	public BookingMaoImpl(){
		super(Booking.class);
	}
	
	@Override
	public Booking getByReservationNumber(String reservationNumber) {
		return findOne(Criteria.where(Booking.FIELDS.RESERVATION_NUMBER).is(reservationNumber));
	}

	@Override
	public Booking getByBookingId(String bookingId) {
		return findOne(Criteria.where(Booking.FIELDS._ID).is(bookingId));
	}

	@Override
	public void createBooking(Booking booking) {
		saveT(booking);
	}

	@Override
	public List<Booking> getBookingsByReservationNumber(String reservationNumber, String hotelId) {
		return find(Criteria.where(Booking.FIELDS.RESERVATION_NUMBER).is(reservationNumber).and(Booking.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public List<Booking> getBookingsByCheckInDate(Date checkInDate, String hotelId) {
		return find(Criteria.where(Booking.FIELDS.CHECK_IN_DATE).is(checkInDate).and(Booking.FIELDS.HOTEL_ID).is(hotelId));
	}

	@Override
	public void deleteByBookingId(String bookingId) {
		deleteT(bookingId);
//		find(Criteria.where(Booking.FIELDS.ROOM_ID).is("").and(Booking.FIELDS.CHECK_OUT_DATE).gt("2").and(Booking.FIELDS.BOOKING_STATUS).is("completed").and(Booking.FIELDS.CHECK_IN_DATE).lte("2"));
	}

	@Override
	public void deleteByReservationNumber(String reservationNumber) {
		deleteT(Criteria.where(Booking.FIELDS.RESERVATION_NUMBER).is(reservationNumber));
	}

	@Override
	public void updateByBookingId(Booking booking) {
		updateNonNullFieldsOfT(booking, booking.getBookingId());
	}

	@Override
	public List<Booking> getUnCompleteBookings(String hotelId, Date bookingDate) {
		return find(Criteria.where(Booking.FIELDS.CHECK_IN_DATE).is(bookingDate).and(Booking.FIELDS.HOTEL_ID).is(hotelId).and(Booking.FIELDS.BOOKING_STATUS).ne(BookingStatus.COMPLETE.getStatus()));
	}

	@Override
	public List<Booking> getByRoomId(String roomId) {
		return find(Criteria.where(Booking.FIELDS.ROOM_ID).is(roomId));
	}

	@Override
	public void setDefaultStaysId(String bookingId, String staysId) {
		updateT(Criteria.where(Fields.UNDERSCORE_ID).is(bookingId), new Update().set(FIELDS.DEFAULT_STAYS_ID, staysId));
	}

	@Override
	public Booking getByBookingId(String bookingId, String hotelId) {
		return findOne(Criteria.where(Booking.FIELDS._ID).is(bookingId).and(FIELDS.HOTEL_ID).is(hotelId));
	}

}
