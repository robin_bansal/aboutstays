package com.aboutstays.mao;

import com.aboutstays.entities.TimeZoneDetails;

public interface TimeZoneDetailsMao extends AbstractMao<TimeZoneDetails>{
	
	public TimeZoneDetails getByTimezone(String timezone);

}
