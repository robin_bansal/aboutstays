package com.aboutstays.mao;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.DBObject;

public interface AbstractMao<T> {

	public void saveT(T t);
	public T getTById(String id);
	public List<T> getAllT();
	public void deleteT(String id);
	public void updateT(T t);
	public T findOne(Criteria criteria);
	public List<T> find(Criteria criteria);
	public void updateT(Criteria criteria, Update update);
	public void updateNonNullFieldsOfT(T t, Query query);
	public void updateAllT(Criteria criteria, Update update);
	public void upsert(Criteria criteria, Update update);
	public void updateNonNullFieldsOfT(T t, String id);
	public Update fromDBObjectExcludeNullFields(DBObject dbObject);
	public void deleteT(Criteria criteria);
	public List<T> find(Query query);
	public T findOne(Query query);
	public long count(Criteria criteria);
}
