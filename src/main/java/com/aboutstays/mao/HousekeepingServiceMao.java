package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.HousekeepingServiceEntity;

public interface HousekeepingServiceMao {
	
	public void add(HousekeepingServiceEntity hkService);
	public List<HousekeepingServiceEntity> getAll();
	public HousekeepingServiceEntity getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(HousekeepingServiceEntity service, String id);
	public void update(HousekeepingServiceEntity service);

}
