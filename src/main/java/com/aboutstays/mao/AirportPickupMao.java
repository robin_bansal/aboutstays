package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.AirportPickup;
import com.aboutstays.enums.GeneralOrderStatus;

public interface AirportPickupMao {

	public void add(AirportPickup airportPickup);
	public List<AirportPickup> getAll();
	public List<AirportPickup> getAllByUserId(String userId);
	public AirportPickup getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(AirportPickup airportPickup, String id);
	public void changeOrderStatus(String id, GeneralOrderStatus status);
}
