package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.ComplementaryServicesEntity;

public interface ComplementaryServicesMao {

	public void save(ComplementaryServicesEntity entity);
	public void update(ComplementaryServicesEntity entity);
	public ComplementaryServicesEntity getById(String id);
	public List<ComplementaryServicesEntity> getAll();
	public void delete(String id);
}
