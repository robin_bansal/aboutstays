package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Place;

public interface PlaceMao {

	public void save(Place place);
	public void update(Place place);
	public void updateNonNull(Place place);
	public Place getById(String placeId);
	public List<Place> getByParentId(String parentPlaceId);
	public List<Place> getAll();
	public void delete(String placeId);
	public List<Place> getByType(int placeType);
}
