package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Stays;

public interface StaysMao {
	
	public void addStays(Stays stays);
	public Stays getByStaysId(String staysId);
	public void deleteStays(String staysId);
	public List<Stays> getAllStays();
	public List<Stays> getStaysByUserId(String userId);
	public Stays getByBookingId(String bookingId);
	public List<Stays> getStaysOfBooking(String bookingId);
	public Stays getStaysByUserAndBookingId(String userId, String bookingId);
	public void changeStatus(String staysId, Integer code);
	public void changeTripType(String staysId, boolean isOfficial);
	public List<Stays> getStaysByRoomId(String roomId);
	public void update(Stays stays);
	public void setRoomId(String staysId, String roomId);
}
