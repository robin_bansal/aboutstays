package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.HouseRules;

public interface HouseRulesMao {
	
	public void add(HouseRules rules);
	public List<HouseRules> getAll();
	public HouseRules getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(HouseRules rules);
}
