package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Driver;


public interface DriverMao {
	
	public void add(Driver driver);
	public List<Driver> getAll();
	public Driver getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(Driver driver, String id);
}
