package com.aboutstays.mao;

import java.util.List;
import com.aboutstays.entities.LoyaltyPrograms;

public interface LoyaltyProgramsMao {
	void add(LoyaltyPrograms loyalyPrograms);

	void update(LoyaltyPrograms loyalyPrograms);

	List<LoyaltyPrograms> getAll();

	LoyaltyPrograms getById(String id);

	void deletyById(String id);
}
