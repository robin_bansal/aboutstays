package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Notification;

public interface NotificationMao {

	public void save(Notification notification);
	public void update(Notification notification);
	public void updateNonNull(Notification notification);
	public Notification getById(String id);
	public List<Notification> getAll();
	public List<Notification> getAll(String userId, String staysId, String hotelId);
	public void delete(String id);
}
