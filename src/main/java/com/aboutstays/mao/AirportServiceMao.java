package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.AirportServices;

public interface AirportServiceMao {
	
	public void add(AirportServices airportServices);
	public List<AirportServices> getAll();
	public AirportServices getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(AirportServices service, String id);

}
