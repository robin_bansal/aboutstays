package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.ReservationCategory;

public interface ReservationCategoryMao extends AbstractMao<ReservationCategory> {

	public List<ReservationCategory> getByType(int reservationType);
	public List<ReservationCategory> getByHotelIdAndType(String hotelId, int code);
	public List<ReservationCategory> getByHotelId(String hotelId);
	public ReservationCategory getByIdAndHotelId(String categoryId, String hotelId);
	
}
