package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.RoomCategory;
import com.aboutstays.request.dto.SetEarlyCheckInRoomRequest;
import com.aboutstays.request.dto.SetLateCheckOutRoomRequest;

public interface RoomCategoryMao {

	public void save(RoomCategory room);
	public RoomCategory getById(String roomId);
	public List<RoomCategory> getAll();
	public List<RoomCategory> getAllByRoomCategoryIds(List<String> roomIds);
	public void delete(String roomCategoryId);
	public void update(RoomCategory roomCategory);
	public void updateNonNullFields(RoomCategory roomCategory);
	public void setEarlyCheckinInfo(SetEarlyCheckInRoomRequest request);
	public RoomCategory getEarlyCheckinRoomInfo(String roomCategoryId);
	public void setLateCheckoutInfo(SetLateCheckOutRoomRequest request);
	public RoomCategory getLateCheckoutInfo(String roomCategoryId);
	public List<RoomCategory> getByHotelId(String hotelId);
	public RoomCategory getByHotelAndCategoryId(String hotelId, String roomId);
	
}
