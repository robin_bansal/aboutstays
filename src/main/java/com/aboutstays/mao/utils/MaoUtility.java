package com.aboutstays.mao.utils;

import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.DBObject;

public class MaoUtility {
	
	public static Update fromDBObjectExcludeNonNullFields(DBObject dbObject){
		Update update = new Update();
		for(String key : dbObject.keySet()){
			Object value = dbObject.get(key);
			if(value != null){
				update.set(key, value);
			}
		}
		return update;
		
	}

}
