package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.MasterGenereItem;

public interface MasterGenereMao {

	public void save(MasterGenereItem masterGenereItem);
	public void delete(String id);
	public List<MasterGenereItem> getAll();
	public void update(MasterGenereItem masterGenereItem);
	public void updateNonNull(MasterGenereItem masterGenereItem);
	public MasterGenereItem getById(String id);
}
