package com.aboutstays.mao;

import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Message;

public interface MessageMao {

	public void save(Message message);
	public void update(Message message);
	public void updateNonNull(Message message);
	public Message getById(String id);
	public List<Message> getAll();
	public List<Message> getAll(String userId,String staysId,String hotelId);
	public void delete(String id);
	public void updateMessageStatus(String userId, String hotelId, int creationType, Date date, int messageStatus);
	public List<Message> getAllByHotelId(String hotelId);
}
