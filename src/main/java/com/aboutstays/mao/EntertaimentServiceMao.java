package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.EntertaimentServiceEntity;

public interface EntertaimentServiceMao {

	public void save(EntertaimentServiceEntity entity);
	public void update(EntertaimentServiceEntity entity);
	public List<EntertaimentServiceEntity> getAll();
	public EntertaimentServiceEntity getById(String id);
	public void delete(String id);
}
