package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.RoomServicesEntity;

public interface RoomServiceMao {
	
	public void add(RoomServicesEntity roomService);
	public List<RoomServicesEntity> getAll();
	public RoomServicesEntity getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(RoomServicesEntity service, String id);
	public void update(RoomServicesEntity service);

}
