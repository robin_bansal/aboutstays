package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.MasterCity;

public interface MasterCityMao {

	public void save(MasterCity masterCity);
	public void saveAll(List<MasterCity> masterCities);
	public List<MasterCity> getAll();
	public MasterCity getById(String id);
	public void delete(String id);
	public MasterCity getByName(String name); 
}
