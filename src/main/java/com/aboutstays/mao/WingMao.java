package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Wing;

public interface WingMao {
	
	public void add(Wing wing);

	public List<Wing> getAll();

	public Wing getWingById(String wingId);
	
	public void updateById(Wing wing,String wingId);

	public void delete(String wingId);
	
	public List<Wing> getByHotelId(String hotelId);

	public Wing getByWingAndHotelId(String wingId, String hotelId);

	
}
