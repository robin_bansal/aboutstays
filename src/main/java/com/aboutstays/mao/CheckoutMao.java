package com.aboutstays.mao;

import com.aboutstays.entities.CheckoutEntity;

public interface CheckoutMao {

	public void save(CheckoutEntity checkoutEntity);
	public void update(CheckoutEntity checkinEntity);
	public void updateNonNull(CheckoutEntity checkinEntity);
	public CheckoutEntity getById(String id);
	public void delete(String id);
	public void changeStatus(String id, int status);
	public CheckoutEntity getByUserStaysHotelId(String userId, String staysId, String hotelId);
}
