package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.CityGuide;



public interface CityGuideMao {

	public void add(CityGuide cityGuide);
	public List<CityGuide> getAll();
	public CityGuide getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(CityGuide cityGuide, String id);
}
