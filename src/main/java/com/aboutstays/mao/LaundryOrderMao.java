package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.LaundryOrder;

public interface LaundryOrderMao {

	public void save(LaundryOrder laundryOrder);
	public LaundryOrder getById(String id);
	public List<LaundryOrder> getAll();
	public List<LaundryOrder> getAllByUserId(String userId);
	public List<LaundryOrder> getAllByHotelId(String hotelId);
	public List<LaundryOrder> getAllByUserAndStayId(String userId, String stayId);
	public void update(LaundryOrder request);
	public void updateNonNull(LaundryOrder request);
	public void delete(String id);
	public void changeOrderStatus(String id, int status);
}
