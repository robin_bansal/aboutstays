package com.aboutstays.mao;

import com.aboutstays.entities.ProcessPropertyInfo;

public interface ProcessPropertyInfoMao {
	
	public ProcessPropertyInfo getProcessPropertyValue(String processName, String propertyaName);

}
