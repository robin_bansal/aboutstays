package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.LateCheckoutRequest;

public interface LateCheckoutRequestMao {
	
	public void add(LateCheckoutRequest order);
	public LateCheckoutRequest get(String id);
	public List<LateCheckoutRequest> getAll();
	public void delete(String id);
	public void updateNonNull(LateCheckoutRequest order);
	public void changeOrderStatus(String id, int status);
	public void update(LateCheckoutRequest request);
	public LateCheckoutRequest retreiveLateCheckout(String userId, String hotelId, String staysId);

}
