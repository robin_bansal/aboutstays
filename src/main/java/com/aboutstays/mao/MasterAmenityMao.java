package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.MasterAmenity;

public interface MasterAmenityMao {

	public void save(MasterAmenity masterAmenity);
	public void update(MasterAmenity masterAmenity);
	public MasterAmenity getById(String id);
	public List<MasterAmenity> getAll();
	public List<MasterAmenity> getAll(int type);
	public void delete(String id);
	public void saveAll(List<MasterAmenity> amenitites);
	public MasterAmenity getByIdAndType(String amenityId, int type);
}
