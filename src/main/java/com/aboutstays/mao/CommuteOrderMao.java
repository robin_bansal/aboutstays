package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.controller.CommuteOrder;

public interface CommuteOrderMao {
	
	public void save(CommuteOrder entity);
	public CommuteOrder getById(String id);
	public List<CommuteOrder> getAll();
	public void updateNonNull(CommuteOrder request);
	public void delete(String id);
	public void changeOrderStatus(String id, int status);

}
