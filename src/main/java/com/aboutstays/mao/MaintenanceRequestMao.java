package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.MaintenanceRequestEntity;

public interface MaintenanceRequestMao {
	
	public void save(MaintenanceRequestEntity entity);
	public MaintenanceRequestEntity getById(String id);
	public List<MaintenanceRequestEntity> getAll();
	public void updateNonNull(MaintenanceRequestEntity request);
	public void delete(String id);
	public void changeOrderStatus(String id, int status);

}
