package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.ReservationServiceEntity;

public interface ReservationServiceMao {
	
	public void add(ReservationServiceEntity entity);
	public List<ReservationServiceEntity> getAll();
	public ReservationServiceEntity getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(ReservationServiceEntity entity, String id);

}
