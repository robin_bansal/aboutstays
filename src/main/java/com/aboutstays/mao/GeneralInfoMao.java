package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.GeneralInformation;

public interface GeneralInfoMao {

	public void add(GeneralInformation generalInfo);
	public List<GeneralInformation> getAll();
	public List<GeneralInformation> getAllByStaysStatus(Integer stayStatus);
	public GeneralInformation getById(String id);
	public void deletyById(String id);
	public void updateNonNullById(GeneralInformation information, String id);
}
