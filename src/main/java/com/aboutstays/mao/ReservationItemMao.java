package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.ReservationItem;

public interface ReservationItemMao {

	public void save(ReservationItem item);
	public ReservationItem getById(String id);
	public List<ReservationItem> getAll();
	public void updateNonNull(ReservationItem item);
	public void delete(String id);
	public void update(ReservationItem item);
	public List<ReservationItem> getByHotelId(String hotelId);
	public ReservationItem getByIdCategoryAndHotelId(String id, String categoryId, String hotelId);
}
