package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.Room;

public interface RoomMao {
	public void add(Room room);
	public List<Room> getAll();
	public Room getById(String id);
	public void deletyById(String id);
	public void update(Room room);
	public void updateNonNullById(Room room, String id);
	public List<Room> getByRoomCategory(String roomCategoryId);
	public List<Room> getByFloorId(String floorId);
	public List<Room> getByWingId(String wingId);
	public List<Room> getByHotelId(String hotelId);
	public int getCountByWingId(String id);
	public int getCountByFloorId(String id);
	public void deleteRooms(String hotelId, List<String> roomIds);
	public Room getById(String hotelId, String roomId);
	public void updateRoomFields(String roomId, String roomCategoryId, String roomNumber, Integer panelUiOrder);
	
}
