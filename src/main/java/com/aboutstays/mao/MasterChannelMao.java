package com.aboutstays.mao;

import java.util.List;

import com.aboutstays.entities.MasterChannelItem;

public interface MasterChannelMao {

	public void save(MasterChannelItem masterChannelItem);
	public void saveAll(List<MasterChannelItem> masterChannelItems);
	public void update(MasterChannelItem masterChannelItem);
	public MasterChannelItem getById(String id);
	public List<MasterChannelItem> getAll();
	public void delete(String id);
	public List<MasterChannelItem> getAllByGenereId(String genereId);
}
