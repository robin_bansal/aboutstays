package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.pojos.FoodItemCartRequest;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.request.dto.UserCartRequest;

public class CartConverter {

	public static UserCartRequest<FoodItemCartRequest> getSampleFoodCartRequest(){
		UserCartRequest<FoodItemCartRequest> cartRequest = new UserCartRequest<>();
		cartRequest.setHotelId("hotelId");
		cartRequest.setStaysId("staysId");
		cartRequest.setUserId("userId");
		
		FoodItemCartRequest foodItemCartRequest = new FoodItemCartRequest();
		foodItemCartRequest.setCartOperation(123);
		foodItemCartRequest.setCartType(1234);
		FoodItemOrderDetails foodItemOrderDetails = new FoodItemOrderDetails();
		foodItemOrderDetails.setFoodItemId("foodItemId");
		foodItemOrderDetails.setType(1234567);
		foodItemOrderDetails.setPrice(120.0);
		foodItemOrderDetails.setQuantity(1);
		foodItemCartRequest.setFoodItemOrderDetails(foodItemOrderDetails);
		cartRequest.setData(foodItemCartRequest);
		return cartRequest;
	}

	public static UserCartRequest<List<FoodItemCartRequest>> getSampleFoodCartListRequest() {
		UserCartRequest<List<FoodItemCartRequest>> cartRequest = new UserCartRequest<>();
		cartRequest.setHotelId("hotelId");
		cartRequest.setStaysId("staysId");
		cartRequest.setUserId("userId");
		
		List<FoodItemCartRequest> list = new ArrayList<>();
		FoodItemCartRequest foodItemCartRequest = new FoodItemCartRequest();
		foodItemCartRequest.setCartOperation(123);
		foodItemCartRequest.setCartType(1234);
		FoodItemOrderDetails foodItemOrderDetails = new FoodItemOrderDetails();
		foodItemOrderDetails.setFoodItemId("foodItemId");
		foodItemOrderDetails.setType(1234567);
		foodItemOrderDetails.setPrice(120.0);
		foodItemOrderDetails.setQuantity(1);
		foodItemCartRequest.setFoodItemOrderDetails(foodItemOrderDetails);
		list.add(foodItemCartRequest);
		cartRequest.setData(list);
		return cartRequest;
	}
}
