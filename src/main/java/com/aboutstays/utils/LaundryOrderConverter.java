package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.LaundryOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.pojos.LaundryItemOrderDetails;
import com.aboutstays.request.dto.AddLaundryOrderRequest;
import com.aboutstays.request.dto.UpdateLaundryOrderRequest;
import com.aboutstays.response.dto.GetLaundryOrderResponse;

public class LaundryOrderConverter extends BaseSessionConverter{
	
	public static AddLaundryOrderRequest getSampleRequest(){
		AddLaundryOrderRequest request = new AddLaundryOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(234L);
		request.setLaundryPreference(12);
		List<LaundryItemOrderDetails> listLaundryItems = new ArrayList<>();
		listLaundryItems.add(new LaundryItemOrderDetails("itemId", 2, 40.0, 123));
		listLaundryItems.add(new LaundryItemOrderDetails("itemId", 3, 40.0, 123));
		listLaundryItems.add(new LaundryItemOrderDetails("itemId", 1, 40.0, 123));
		request.setListOrders(listLaundryItems);
		setRequestData(request);
		return request;
	}
	
	

	public static LaundryOrder convertBeanToEntity(AddLaundryOrderRequest request) {
		if(request == null){
			return null;
		}
		LaundryOrder response = new LaundryOrder();
		response.setComments(request.getComment());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		if(request.isRequestedNow()){
			response.setDeliveryTime(response.getCreated());
			response.setStatus(GeneralOrderStatus.PROCESSING.getCode());
		} else {
			response.setDeliveryTime(request.getDeliveryTime());
			response.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
		}
		response.setLaundryItemOrders(request.getListOrders());
		response.setPreferences(request.getLaundryPreference());
		setPojoToEntity(response, request);
		return response;
	}



	public static GetLaundryOrderResponse convertEntityToBean(LaundryOrder request) {
		if(request == null){
			return null;
		}
		GetLaundryOrderResponse response = new GetLaundryOrderResponse();
		response.setComment(request.getComments());
		response.setDeliveryTime(request.getDeliveryTime());
		response.setLaundryPreference(request.getPreferences());
		response.setListOrders(request.getLaundryItemOrders());
		response.setId(request.getId());
		List<LaundryItemOrderDetails> listOrders = new ArrayList<>();
		for(LaundryItemOrderDetails details : request.getLaundryItemOrders()){
			if(details == null){
				continue;
			}
			LaundryItemOrderDetails orderDetails = new LaundryItemOrderDetails();
			orderDetails.setTypeName(LaundryItemType.getNameByCode(details.getType()));
			orderDetails.setLaundryItemId(details.getLaundryItemId());
			orderDetails.setType(details.getType());
			orderDetails.setPrice(details.getPrice());
			orderDetails.setQuantity(details.getQuantity());
			orderDetails.setItemName(details.getItemName());
			listOrders.add(orderDetails);
		}
		response.setListOrders(listOrders);
		response.setStatus(request.getStatus());
		setEntityToPojo(response, request);
		return response;
	}



	public static List<GetLaundryOrderResponse> convertListEntityToListBean(List<LaundryOrder> listOrders) {
		if(CollectionUtils.isNotEmpty(listOrders)){
			List<GetLaundryOrderResponse> listResponse = new ArrayList<>();
			for(LaundryOrder order : listOrders){
				listResponse.add(convertEntityToBean(order));
			}
			return listResponse;
		}
		return null;
	}



	public static LaundryOrder convertUpdateRequestToEntity(UpdateLaundryOrderRequest request) {
		if(request != null){
			LaundryOrder response = new LaundryOrder();
			response.setUpdated(new Date().getTime());
			response.setDeliveryTime(request.getDeliveryTime());
			response.setLaundryItemOrders(request.getListOrders());
			response.setPreferences(request.getLaundryPreference());
			response.setComments(request.getComment());
			response.setId(request.getId());
			response.setStatus(request.getStatus());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}
	
	
	
	

}
