package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.ReservationOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddReservationOrderRequest;
import com.aboutstays.request.dto.UpdateReservationOrderRequest;
import com.aboutstays.response.dto.GetReservationOrderResponse;

public class ReservationOrderConverter extends BaseSessionConverter{
	
	public static AddReservationOrderRequest getSampleRequest(){
		AddReservationOrderRequest request = new AddReservationOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(234L);
		request.setPrice(2132);
		request.setDuration("duration");
		request.setRequestedNow(false);
		request.setRsItemId("rsItemId");
		request.setName("name");
		request.setPax(3);
		setRequestData(request);
		return request;
	}

	public static ReservationOrder convertBeanToEntity(AddReservationOrderRequest request) {
		if(request != null){
			ReservationOrder order = new ReservationOrder();
			order.setComments(request.getComment());
			order.setCreated(new Date().getTime());
			order.setDuration(request.getDuration());
			order.setPrice(request.getPrice());
			order.setRsItemId(request.getRsItemId());
			order.setUpdated(new Date().getTime());
			order.setName(request.getName());
			if(request.isRequestedNow()){
				order.setDeliveryTime(order.getCreated());
				order.setStatus(GeneralOrderStatus.PROCESSING.getCode());
			} else {
				order.setDeliveryTime(request.getDeliveryTime());
				order.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
			}
			order.setPax(request.getPax());
			setPojoToEntity(order, request);
			return order;
		}
		return null;
	}

	public static GetReservationOrderResponse convertEntityToBean(ReservationOrder order) {
		if(order != null){
			GetReservationOrderResponse response = new GetReservationOrderResponse();
			response.setComment(order.getComments());
			response.setDeliveryTime(order.getDeliveryTime());
			response.setDuration(order.getDuration());
			response.setId(order.getId());
			response.setPrice(order.getPrice());
			response.setRsItemId(order.getRsItemId());
			response.setStatus(order.getStatus());
			response.setName(order.getName());
			response.setPax(order.getPax());
			setEntityToPojo(response, order);
			return response;
		}
		return null;
	}

	public static List<GetReservationOrderResponse> convertListEntityToListBean(List<ReservationOrder> listOrder) {
		if(CollectionUtils.isNotEmpty(listOrder)){
			List<GetReservationOrderResponse> listResponse = new ArrayList<>();
			for(ReservationOrder order : listOrder){
				if(order == null){
					continue;
				}
				listResponse.add(convertEntityToBean(order));
			}
			return listResponse;
		}
		return null;
	}

	public static ReservationOrder convertUpdateBeanToEntity(UpdateReservationOrderRequest request) {
		if(request != null){
			ReservationOrder order = new ReservationOrder();
			order.setComments(request.getComment());
			order.setId(request.getId());
			order.setDuration(request.getDuration());
			order.setGsId(request.getGsId());
			order.setHotelId(request.getHotelId());
			order.setPrice(request.getPrice());
			order.setRsItemId(request.getRsItemId());
			order.setStaysId(request.getStaysId());
			order.setUpdated(new Date().getTime());
			order.setUserId(request.getUserId());
			order.setStatus(GeneralOrderStatus.UPDATED.getCode());
			order.setDeliveryTime(request.getDeliveryTime());
			order.setName(request.getName());
			order.setPax(request.getPax());
			setPojoToEntity(order, request);
			return order;
		}
		return null;
	}
	
}
