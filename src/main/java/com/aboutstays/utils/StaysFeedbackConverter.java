package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.StaysFeedback;
import com.aboutstays.pojos.NameQualityTimetaken;
import com.aboutstays.request.dto.AddStaysFeedbackRequest;
import com.aboutstays.request.dto.UpdateStaysFeedbackRequest;
import com.aboutstays.response.dto.StaysFeedbackResponse;

public class StaysFeedbackConverter extends BaseSessionConverter{
	
	public static AddStaysFeedbackRequest getSampleRequest(){
		AddStaysFeedbackRequest request = new AddStaysFeedbackRequest();
		List<NameQualityTimetaken> listFeedbacks = new ArrayList<>();
		NameQualityTimetaken entity = new NameQualityTimetaken();
		entity.setTimeTaken(4);
		entity.setQuality(3);
		entity.setName("housekeeping");
		listFeedbacks.add(entity);
		request.setListFeedback(listFeedbacks);
		request.setSpecialStaff("specialStaff");
		request.setComment("comment");
		setRequestData(request);
		return request;
	}

	public static StaysFeedback convertBeanToEntity(AddStaysFeedbackRequest request) {
		if(request != null){
			StaysFeedback response = new StaysFeedback();
			response.setCreated(new Date().getTime());
			response.setListFeedback(request.getListFeedback());
			response.setUpdated(response.getCreated());
			response.setSpecialStaff(request.getSpecialStaff());
			response.setComment(request.getComment());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

	public static StaysFeedbackResponse convertEntityToBean(StaysFeedback request) {
		if(request != null){
			StaysFeedbackResponse response = new StaysFeedbackResponse();
			response.setListFeedback(request.getListFeedback());
			response.setId(request.getId());
			response.setSpecialStaff(request.getSpecialStaff());
			response.setComment(request.getComment());
			setEntityToPojo(response, request);
			return response;
		}
		return null;
	}

	public static List<StaysFeedbackResponse> convertListEntityToListBean(List<StaysFeedback> listFeedback) {
		if(CollectionUtils.isNotEmpty(listFeedback)){
			List<StaysFeedbackResponse> listResponse = new ArrayList<>();
			for(StaysFeedback feedback : listFeedback){
				if(feedback == null){
					continue;
				}
				listResponse.add(convertEntityToBean(feedback));
			}
			return listResponse;
		}
		return null;
	}

	public static StaysFeedback convertUpdateBeanToEntity(UpdateStaysFeedbackRequest request) {
		if(request != null){
			StaysFeedback response = new StaysFeedback();
			response.setId(request.getId());
			response.setListFeedback(request.getListFeedback());
			response.setUpdated(new Date().getTime());
			response.setSpecialStaff(request.getSpecialStaff());
			response.setComment(request.getComment());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}
	
	

}
