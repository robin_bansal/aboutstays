package com.aboutstays.utils;

import java.util.Date;

import com.aboutstays.entities.EntertaimentItem;
import com.aboutstays.request.dto.AddEntertaimentItemRequest;
import com.aboutstays.request.dto.GetEntertaimentItemResponse;
import com.aboutstays.request.dto.UpdateEntertainmentRequest;
import com.aboutstays.response.dto.GetMasterChannelResponse;

public class EntertainmentItemConverter {

	public static EntertaimentItem convertBeanToEntity(AddEntertaimentItemRequest request, String categoryName) {
		if(request != null){
			EntertaimentItem response = new EntertaimentItem();
			response.setCreated(new Date().getTime());
			response.setCategory(categoryName);
			response.setHotelId(request.getHotelId());
			response.setMasterItemId(request.getMasterItemId());
			response.setNumber(request.getNumber());
			response.setEntServiceId(request.getServiceId());
			response.setUpdated(response.getCreated());
			return response;
		}
		return null;
	}
	
	public static EntertaimentItem convertUpdateBeanToEntity(UpdateEntertainmentRequest request, String categoryName) {
		if(request != null){
			EntertaimentItem response = new EntertaimentItem();
			response.setCategory(categoryName);
			response.setHotelId(request.getHotelId());
			response.setMasterItemId(request.getMasterItemId());
			response.setNumber(request.getNumber());
			response.setEntServiceId(request.getServiceId());
			response.setUpdated(new Date().getTime());
			response.setId(request.getId());
			return response;
		}
		return null;
	}

	public static AddEntertaimentItemRequest getSampleRequest() {
		AddEntertaimentItemRequest request = new AddEntertaimentItemRequest();
		request.setHotelId("hotelId");
		request.setServiceId("serviceId");
		request.setMasterItemId("masterItemId");
		request.setNumber("channel number");
		return request;
	}



	public static GetEntertaimentItemResponse convertEntityToBean(EntertaimentItem request,
			GetMasterChannelResponse masterChannelDetails) {
		if(request != null){
			GetEntertaimentItemResponse response = new GetEntertaimentItemResponse();
			response.setHotelId(request.getHotelId());
			response.setNumber(request.getNumber());
			response.setServiceId(request.getEntServiceId());
			response.setItemDetails(masterChannelDetails);
			response.setItemId(request.getId());
			return response;
		}
		return null;
	}
	
	

}
