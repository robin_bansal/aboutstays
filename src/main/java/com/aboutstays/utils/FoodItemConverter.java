package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.FoodItem;
import com.aboutstays.pojos.GoesWellWithItem;
import com.aboutstays.pojos.GoesWellWithItemPojo;
import com.aboutstays.request.dto.FoodItemRequest;
import com.aboutstays.request.dto.UpdateFoodItemRequest;
import com.aboutstays.response.dto.FoodItemResponse;

public class FoodItemConverter {

	
	public static FoodItem convertRequestToEntity(FoodItemRequest request){
		if(request==null)
			return null;
		FoodItem response = new FoodItem();
		response.setHotelId(request.getHotelId());
		response.setRoomServicesId(request.getRoomServicesId());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setCuisineType(request.getCuisineType());
		response.setDescription(request.getDescription());
		response.setSpicyType(request.getSpicyType());
		response.setFoodLabel(request.getFoodLabel());
		response.setImageUrl(request.getImageUrl());
		response.setListGoesWellItems(request.getListGoesWellItems());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setRecommended(request.isRecommended());
		response.setName(request.getName());
		response.setServes(request.getServes());
		response.setMenuItemtype(request.getType());
		response.setCategory(request.getCategory());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
	
	public static FoodItemResponse convertEntityToResponse(FoodItem request){
		if(request==null)
			return null;
		FoodItemResponse response = new FoodItemResponse();
		response.setFooditemId(request.getId());
		response.setHotelId(request.getHotelId());
		response.setRoomServicesId(request.getRoomServicesId());
		response.setCuisineType(request.getCuisineType());
		response.setDescription(request.getDescription());
		response.setSpicyType(request.getSpicyType());
		response.setFoodLabel(request.getFoodLabel());
		response.setImageUrl(request.getImageUrl());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setRecommended(request.isRecommended());
		response.setName(request.getName());
		response.setServes(request.getServes());
		response.setType(request.getMenuItemtype());
		response.setCategory(request.getCategory());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
	
	public static FoodItemRequest getSampleRequest(){
		FoodItemRequest item = new FoodItemRequest();
		item.setCuisineType("spanish");
		item.setDescription("This dish is native spanish.");
		item.setFoodLabel(123);
		item.setName("Speghetti");
		item.setSpicyType(123);
		item.setHotelId("hotelId");
		item.setRoomServicesId("roomServicesId");
		item.setRecommended(false);
		item.setImageUrl("imageUrl");
		item.setMrp(123.50);
		item.setPrice(120.0);
		item.setServes(2);
		List<GoesWellWithItem> listGoesWellWith = new ArrayList<>();
		listGoesWellWith.add(new GoesWellWithItem("foodItemId1"));
		listGoesWellWith.add(new GoesWellWithItem("foodItemId2"));
		item.setListGoesWellItems(listGoesWellWith);
		item.setType(1234);
		item.setCategory("Main course");
		item.setShowOnApp(true);
		return item;
	}

	public static GoesWellWithItemPojo convertEntityToGWWIP(FoodItem request) {
		if(request==null)
			return null;
		GoesWellWithItemPojo response = new GoesWellWithItemPojo();
//		response.setFooditemId(request.getId());
		response.setHotelId(request.getHotelId());
		response.setRoomServicesId(request.getRoomServicesId());
		response.setCuisineType(request.getCuisineType());
		response.setDescription(request.getDescription());
		response.setSpicyType(request.getSpicyType());
		response.setFoodLabel(request.getFoodLabel());
		response.setImageUrl(request.getImageUrl());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setRecommended(request.isRecommended());
		response.setName(request.getName());
		response.setServes(request.getServes());
		response.setType(request.getMenuItemtype());
		response.setCategory(request.getCategory());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}

	public static FoodItem convertUpdateRequestToEntity(UpdateFoodItemRequest request) {
		if(request==null)
			return null;
		FoodItem response = new FoodItem();
		response.setId(request.getFooditemId());
		response.setHotelId(request.getHotelId());
		response.setRoomServicesId(request.getRoomServicesId());
		response.setUpdated(new Date().getTime());
		response.setCuisineType(request.getCuisineType());
		response.setDescription(request.getDescription());
		response.setSpicyType(request.getSpicyType());
		response.setFoodLabel(request.getFoodLabel());
		response.setImageUrl(request.getImageUrl());
		response.setListGoesWellItems(request.getListGoesWellItems());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setRecommended(request.isRecommended());
		response.setName(request.getName());
		response.setServes(request.getServes());
		response.setMenuItemtype(request.getType());
		response.setCategory(request.getCategory());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
}
