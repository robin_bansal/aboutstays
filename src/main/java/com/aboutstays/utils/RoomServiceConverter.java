package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.RoomServicesEntity;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.pojos.MenuItemsByType;
import com.aboutstays.request.dto.AddRoomServiceRequest;
import com.aboutstays.request.dto.GetRoomServiceResponse;
import com.aboutstays.request.dto.UpdateRoomServiceRequest;

public class RoomServiceConverter {
	
	public static AddRoomServiceRequest getSampleRequest(){
		AddRoomServiceRequest request = new AddRoomServiceRequest();
		request.setId("hotel id");
		request.setSlaApplicable(true);
		request.setSlaTime(40);
		List<MenuItemsByType> listMenuItem = new ArrayList<>();
		
		MenuItemsByType menuItem = new MenuItemsByType();
		menuItem.setType(123456);
		menuItem.setTiming("8AM - 11 AM");
		listMenuItem.add(menuItem);

		menuItem = new MenuItemsByType();
		menuItem.setType(123456);
		menuItem.setTiming("24 Hrs");
		
		listMenuItem.add(menuItem);
		
		request.setListMenuItem(listMenuItem);;
		return request;
	}

	public static RoomServicesEntity convertBeanToEntity(AddRoomServiceRequest request) {
		if(request != null){
			RoomServicesEntity service = new RoomServicesEntity();
			service.setHotelId(request.getId());
			service.setListMenuItem(request.getListMenuItem());
			long currentTime = new Date().getTime();
			service.setCreated(currentTime);
			service.setUpdated(currentTime);
			service.setSlaApplicable(request.isSlaApplicable());
			service.setSlaTime(request.getSlaTime());
			return service;
		}
		return null;
	}
	
	public static GetRoomServiceResponse convertEntityToBean(RoomServicesEntity roomService){
		if(roomService != null){
			GetRoomServiceResponse response = new GetRoomServiceResponse();
			response.setId(roomService.getHotelId());
			response.setListMenuItem(roomService.getListMenuItem());
			response.setSlaApplicable(roomService.isSlaApplicable());
			response.setSlaTime(roomService.getSlaTime());
			if(CollectionUtils.isNotEmpty(response.getListMenuItem())){
				for(MenuItemsByType menuItemsByType : response.getListMenuItem()){
					MenuItemType menuItemType = MenuItemType.getByCode(menuItemsByType.getType());
					if(menuItemType!=null){
						menuItemsByType.setImageUrl(menuItemType.getImageUrl());
						menuItemsByType.setTypeName(menuItemType.getDisplayName());
					}
				}
			}
			return response;
		}
		return null;
	}

	public static List<GetRoomServiceResponse> convertListEntityToListBean(List<RoomServicesEntity> listServices) {
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetRoomServiceResponse> listResponse = new ArrayList<>();
			for(RoomServicesEntity roomService : listServices){
				listResponse.add(convertEntityToBean(roomService));
			}
			return listResponse;
		}
		return null;
	}

	public static RoomServicesEntity convertUpdateBeanToEntity(UpdateRoomServiceRequest request) {
		if(request != null){
			RoomServicesEntity service = new RoomServicesEntity();
			service.setUpdated(new Date().getTime());
			service.setHotelId(request.getId());
			service.setListMenuItem(request.getListMenuItem());
			service.setSlaApplicable(request.isSlaApplicable());
			service.setSlaTime(request.getSlaTime());
			return service;
		}
		return null;
	}
	
	

}
