package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.CommuteService;
import com.aboutstays.pojos.CarDetails;
import com.aboutstays.pojos.CarDetailsAndPrice;
import com.aboutstays.pojos.CommutePackage;
import com.aboutstays.request.dto.AddCommuteServiceRequest;
import com.aboutstays.request.dto.UpdateCommuteServiceRequest;
import com.aboutstays.response.dto.GetCommuteServiceResponse;

public class CommuteServiceConverter {
	
	public static AddCommuteServiceRequest getSampleRequest(){
		AddCommuteServiceRequest request = new AddCommuteServiceRequest();
		request.setId("hotel id");
		List<CommutePackage> listPackage = new ArrayList<>();
		
		List<CarDetailsAndPrice> listCarDetailsAndPrice = new ArrayList<>(); 
		CarDetails carDetails = new CarDetails(1234, "imageUrl1", 3, 60.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));
		carDetails = new CarDetails(1234, "imageUrl2", 3, 90.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));	
		listPackage.add(new CommutePackage("All Day", listCarDetailsAndPrice));
		
		listCarDetailsAndPrice = new ArrayList<>(); 
		carDetails = new CarDetails(1234, "imageUrl3", 3, 80.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));
		carDetails = new CarDetails(1234, "imageUrl4", 3, 80.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));
		listPackage.add(new CommutePackage("Point to Point", listCarDetailsAndPrice));
		
		request.setListCommutePackage(listPackage);
		return request;
	}

	public static CommuteService convertBeanToEntity(AddCommuteServiceRequest request) {
		if(request != null){
			CommuteService service = new CommuteService();
			service.setHotelId(request.getId());
			long currentTime = new Date().getTime();
			service.setCreated(currentTime);
			service.setUpdated(currentTime);
			service.setListCommutePackage(request.getListCommutePackage());
			return service;
		}
		return null;
	}

	public static GetCommuteServiceResponse convertEntityToBean(CommuteService service) {
		if(service != null){
			GetCommuteServiceResponse response = new GetCommuteServiceResponse();
			response.setId(service.getHotelId());
			response.setListCommutePackage(service.getListCommutePackage());
			return response;
		}
		return null;
	}

	public static List<GetCommuteServiceResponse> convertListEntityToListBean(List<CommuteService> listService) {
		if(CollectionUtils.isNotEmpty(listService)){
			List<GetCommuteServiceResponse> listResponse = new ArrayList<>();
			for(CommuteService service : listService){
				if(service == null){
					continue;
				}
				listResponse.add(convertEntityToBean(service));
			}
			return listResponse;
		}
		return null;
	}

	public static CommuteService convertUpdateRequestToEntity(UpdateCommuteServiceRequest request) {
		if(request != null){
			CommuteService service = new CommuteService();
			service.setUpdated(new Date().getTime());
			service.setListCommutePackage(request.getListCommutePackage());
			service.setHotelId(request.getId());
			return service;
		}
		return null;
	}
	
	

}
