package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Hotel;
import com.aboutstays.pojos.Address;
import com.aboutstays.pojos.HotelGeneralInformation;
import com.aboutstays.pojos.HotelPojo;
import com.aboutstays.request.dto.CreateHotelRequest;
import com.aboutstays.request.dto.UpdateHotelRequest;
import com.aboutstays.response.dto.GetHotelGeneralInfoResponse;
import com.aboutstays.response.dto.GetHotelResponse;
import com.aboutstays.response.dto.HotelBasicInfoPojo;

public class HotelConverter {

	public static CreateHotelRequest getSampleRequest() {
		CreateHotelRequest request = new CreateHotelRequest();
		request.setHotelId("hotelId");
		request.setHotelType(3);
		request.setPartnershipType(0);
		HotelGeneralInformation generalInfo = new HotelGeneralInformation();
		generalInfo.setCheckInTime("checkInTime");
		generalInfo.setCheckOutTime("checkOutTime");
		generalInfo.setDescription("description");
		generalInfo.setEmail("emailId");
		generalInfo.setFax("fax");
		generalInfo.setLogoUrl("logoUrl");
		generalInfo.setName("name");
		generalInfo.setPhone1("phone1");
		generalInfo.setPhone2("phone2");
		generalInfo.setImageUrl("imgaeUrl");
		List<String> additionalImageList = new ArrayList<>();
		additionalImageList.add("image1");
		additionalImageList.add("image2");
		additionalImageList.add("image2");
		generalInfo.setAdditionalImageUrls(additionalImageList);
		Address address = new Address();
		address.setAddressLine1("addressLine1");
		address.setAddressLine2("addressLine2");
		address.setCity("city");
		address.setCountry("country");
		address.setDescription("description");
		address.setLatitude("latitude");
		address.setLongitude("longitude");
		address.setPincode("pincode");
		address.setState("state");
		generalInfo.setAddress(address);
		request.setGeneralInformation(generalInfo);

		// List<MasterAmenityPojo> itemsList = new ArrayList<>();
		// itemsList.add(new MasterAmenityPojo("item 1","item url 1"));
		// itemsList.add(new MasterAmenityPojo("item 2","item url 2"));
		// itemsList.add(new MasterAmenityPojo("item 3","item url 3"));
		// Essentials essentials = new Essentials();
		// essentials.setItemsList(itemsList);

		List<String> itemsList = new ArrayList<>();
		itemsList.add("essentilaId 1");
		itemsList.add("essentilaId 2");
		itemsList.add("essentilaId 3");
		return request;
	}

	public static Hotel convertCreateHotelRequestToEntity(CreateHotelRequest createHotelRequest) {
		if (createHotelRequest == null)
			return null;
		Hotel hotel = new Hotel();
		hotel.setGeneralInformation(createHotelRequest.getGeneralInformation());
		hotel.setReviewsAndRatings(createHotelRequest.getReviewsAndRatings());
		hotel.setRoomsIds(createHotelRequest.getRoomsIds());
		hotel.setHotelType(createHotelRequest.getHotelType());
		hotel.setPartnershipType(createHotelRequest.getPartnershipType());
		return hotel;
	}

	public static HotelPojo convertHotelToHotelPojo(Hotel hotel) {
		if (hotel == null) {
			return null;
		}
		HotelPojo hotelPojo = new HotelPojo();
		hotelPojo.setGeneralInformation(hotel.getGeneralInformation());
		hotelPojo.setHotelId(hotel.getId());
		hotelPojo.setHotelType(hotel.getHotelType());
		hotelPojo.setReviewsAndRatings(hotel.getReviewsAndRatings());
		hotelPojo.setRoomsIds(hotel.getRoomsIds());
		hotelPojo.setHotelId(hotel.getId());
		hotelPojo.setPartnershipType(hotel.getPartnershipType());
		return hotelPojo;
	}

	public static List<HotelPojo> convertListHotelsToListHotelPojo(List<Hotel> listHotels) {
		if (listHotels.isEmpty()) {
			return null;
		}
		List<HotelPojo> listHotelPojo = new ArrayList<HotelPojo>();
		for (Hotel hotel : listHotels) {
			listHotelPojo.add(convertHotelToHotelPojo(hotel));
		}
		return listHotelPojo;
	}

	public static GetHotelResponse convertHotelToGetHotelResponse(Hotel hotel) {
		if (hotel == null) {
			return null;
		}
		GetHotelResponse getHotelResponse = new GetHotelResponse();
		getHotelResponse.setGeneralInformation(hotel.getGeneralInformation());
		getHotelResponse.setHotelId(hotel.getId());
		getHotelResponse.setHotelType(hotel.getHotelType());
		getHotelResponse.setRoomsIds(hotel.getRoomsIds());
		getHotelResponse.setReviewsAndRatings(hotel.getReviewsAndRatings());
		getHotelResponse.setHotelId(hotel.getId());
		getHotelResponse.setPartnershipType(hotel.getPartnershipType());
		getHotelResponse.setTimezone(hotel.getTimezone());
		return getHotelResponse;
	}

	public static Hotel convertUpdateHotelRequestToEntity(UpdateHotelRequest updateHotelRequest) {
		if (updateHotelRequest == null) {
			return null;
		}
		Hotel hotel = new Hotel();
		hotel.setGeneralInformation(updateHotelRequest.getGeneralInformation());
		hotel.setReviewsAndRatings(updateHotelRequest.getReviewsAndRatings());
		hotel.setRoomsIds(updateHotelRequest.getRoomsIds());
		hotel.setHotelType(updateHotelRequest.getHotelType());
		hotel.setPartnershipType(updateHotelRequest.getPartnershipType());
		return hotel;
	}

	public static HotelBasicInfoPojo convertBasicInfoListEntityToBean(Hotel hotel) {
		if (hotel == null) {
			return null;
		}
		HotelBasicInfoPojo basicPojo = new HotelBasicInfoPojo();
		basicPojo.setGeneralInformation(hotel.getGeneralInformation());
		basicPojo.setHotelId(hotel.getId());
		basicPojo.setPartnershipType(hotel.getPartnershipType());
		return basicPojo;
	}

	public static List<HotelBasicInfoPojo> convertBasicInfoListToPojoList(List<Hotel> listHotels) {
		if (CollectionUtils.isEmpty(listHotels)) {
			return null;
		}
		List<HotelBasicInfoPojo> list = new ArrayList<HotelBasicInfoPojo>();
		for (Hotel hotel : listHotels) {
			list.add(convertBasicInfoListEntityToBean(hotel));
		}
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list;
	}

	public static GetHotelGeneralInfoResponse convertHotelToGetGeneralInfoResponse(Hotel hotel) {
		if (hotel != null) {
			GetHotelGeneralInfoResponse response = new GetHotelGeneralInfoResponse();
			response.setGeneralInformation(hotel.getGeneralInformation());
			response.setHotelId(hotel.getId());
			response.setHotelType(hotel.getHotelType());
			return response;
		}
		return null;
	}

}
