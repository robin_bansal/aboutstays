package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.LaundryItem;
import com.aboutstays.enums.LaundryFor;
import com.aboutstays.request.dto.LaundryItemRequest;
import com.aboutstays.response.dto.LaundryItemResponse;

public class LaundryItemConverter {

	public static LaundryItem convertAddRequestToEntity(LaundryItemRequest request){
		if(request==null)
			return null;
		LaundryItem response = new LaundryItem();
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setHotelId(request.getHotelId());
		response.setLaundryFor(request.getLaundryFor());
		response.setLaundryItemType(request.getType());
		response.setLaundryServicesId(request.getLaundryServicesId());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setName(request.getName());
		response.setSameDayPrice(request.getSameDayPrice());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
	
	public static LaundryItemResponse convertEntityToGetResponse(LaundryItem request){
		if(request==null)
			return null;
		LaundryItemResponse response = new LaundryItemResponse();
		response.setItemId(request.getId());
		response.setHotelId(request.getHotelId());
		response.setLaundryFor(request.getLaundryFor());
		response.setType(request.getLaundryItemType());
		response.setLaundryServicesId(request.getLaundryServicesId());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setName(request.getName());
		response.setCategory(LaundryFor.getByCode(request.getLaundryFor()).getDisplayName());
		response.setSameDayPrice(request.getSameDayPrice());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
	
	public static LaundryItem converUpdateRequestToEntity(LaundryItemRequest request){
		if(request==null)
			return null;
		LaundryItem response = new LaundryItem();
		response.setUpdated(new Date().getTime());
		response.setHotelId(request.getHotelId());
		response.setLaundryFor(request.getLaundryFor());
		response.setLaundryItemType(request.getType());
		response.setLaundryServicesId(request.getLaundryServicesId());
		response.setMrp(request.getMrp());
		response.setPrice(request.getPrice());
		response.setName(request.getName());
		response.setSameDayPrice(request.getSameDayPrice());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
	
	public static List<LaundryItemResponse> convertEntityListToResponseList(List<LaundryItem> requestList){
		if(CollectionUtils.isEmpty(requestList)){
			return null;
		}
		List<LaundryItemResponse> responseList = new ArrayList<>(requestList.size());
		for(LaundryItem item : requestList){
			responseList.add(convertEntityToGetResponse(item));
		}
		return responseList;
	}

	public static LaundryItemRequest getSampleRequest() {
		LaundryItemRequest request = new LaundryItemRequest();
		request.setHotelId("hotelId");
		request.setLaundryFor(123);
		request.setType(123);
		request.setMrp(120.0);
		request.setPrice(120.0);
		request.setName("Shirt");
		request.setLaundryServicesId("laundryServicesId"); 
		request.setSameDayPrice(120.0);
		request.setShowOnApp(true);
		return request;
	}
}
