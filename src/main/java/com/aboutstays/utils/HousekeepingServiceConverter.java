package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.HousekeepingServiceEntity;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.pojos.HousekeepingItemsByType;
import com.aboutstays.request.dto.AddHousekeepingServiceRequest;
import com.aboutstays.response.dto.GetHousekeepingServiceResponse;
import com.aboutstays.response.dto.UpdateHousekeepingServiceRequest;

public class HousekeepingServiceConverter {
	
	public static AddHousekeepingServiceRequest getSampleRequest(){
		AddHousekeepingServiceRequest request = new AddHousekeepingServiceRequest();
		request.setId("Hotel id");
		request.setSlaApplicable(true);
		request.setSlaTime(40);
		request.setStartTime("10:00 AM");
		request.setEndTime("08:00 PM");
		List<HousekeepingItemsByType> listHKItemsType = new ArrayList<>();
		listHKItemsType.add(new HousekeepingItemsByType(123));
		listHKItemsType.add(new HousekeepingItemsByType(12));
		request.setListHKItems(listHKItemsType);
		return request;
	}

	public static HousekeepingServiceEntity convertBeanToEntity(AddHousekeepingServiceRequest request) {
		if(request == null){
			return null;
		}
		HousekeepingServiceEntity response = new HousekeepingServiceEntity();
		response.setHotelId(request.getId());
		response.setListHKItems(request.getListHKItems());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setSlaApplicable(request.isSlaApplicable());
		response.setSlaTime(request.getSlaTime());
		response.setStartTime(request.getStartTime());
		response.setEndTime(request.getEndTime());
		return response;
	}

	public static GetHousekeepingServiceResponse convertEntityToBean(HousekeepingServiceEntity service) {
		if(service == null){
			return null;
		}
		GetHousekeepingServiceResponse response = new GetHousekeepingServiceResponse();
		response.setId(service.getHotelId());
		response.setListHKItems(service.getListHKItems());
		response.setSlaApplicable(service.isSlaApplicable());
		response.setSlaTime(service.getSlaTime());
		response.setStartTime(service.getStartTime());
		response.setEndTime(service.getEndTime());
		if(CollectionUtils.isNotEmpty(response.getListHKItems())){
			for(HousekeepingItemsByType housekeepingItemsByType : response.getListHKItems()){
				HousekeepingItemType housekeepingItemType = HousekeepingItemType.getByCode(housekeepingItemsByType.getType());
				if(housekeepingItemType!=null){
					housekeepingItemsByType.setImageUrl(housekeepingItemType.getImageUrl());
					housekeepingItemsByType.setTypeName(housekeepingItemType.getDisplayName());
				}
			}
		}
		return response;
	}

	public static List<GetHousekeepingServiceResponse> convertListEntityToListBean(
			List<HousekeepingServiceEntity> listServices) {
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetHousekeepingServiceResponse> listResponse = new ArrayList<>();
			for(HousekeepingServiceEntity service : listServices){
				listResponse.add(convertEntityToBean(service));
			}
			return listResponse;
		}
		return null;
	}

	public static HousekeepingServiceEntity convertUpdateBeanToEntity(UpdateHousekeepingServiceRequest request) {
		if(request == null){
			return null;
		}
		HousekeepingServiceEntity service = new HousekeepingServiceEntity();
		service.setHotelId(request.getId());
		service.setListHKItems(request.getListHKItems());
		service.setUpdated(new Date().getTime());
		service.setSlaApplicable(request.isSlaApplicable());
		service.setSlaTime(request.getSlaTime());
		service.setStartTime(request.getStartTime());
		service.setEndTime(request.getEndTime());
		return service;
	}
	
	
	

}
