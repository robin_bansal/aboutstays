package com.aboutstays.utils;

import java.util.Date;

import org.springframework.util.StringUtils;

import com.aboutstays.entities.CheckoutEntity;
import com.aboutstays.enums.CheckoutRequestStatus;
import com.aboutstays.pojos.Address;
import com.aboutstays.request.dto.InitiateCheckoutRequest;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.CheckoutResponse;

public class CheckoutConverter {
	
	public static InitiateCheckoutRequest getSampleRequest() {
		InitiateCheckoutRequest request = new InitiateCheckoutRequest();
		request.setGrandTotal(2000.0);
		request.setHotelId("hotelId");
		request.setOfficialTrip(true);
		request.setPaymentMethod(12);
		request.setRoomNo("roomNo");
		request.setStaysId("staysId");
		request.setTotalAmount(1234.56);
		request.setUserId("userId");
		
		request.setLastName("lastName");
		request.setEmailId("emailId");
		request.setAddress(new Address());
		request.setFirstName("firstName");
		request.setMobileNo("mobileNo");
		return request;
	}

	public static CheckoutEntity convertInitiateRequestToEntity(InitiateCheckoutRequest request) {
		if(request != null){
			CheckoutEntity response = new CheckoutEntity();
			response.setCreated(new Date().getTime());
			response.setGrandTotal(request.getGrandTotal());
			response.setHotelId(request.getHotelId());
			response.setOfficialTrip(request.isOfficialTrip());
			response.setPaymentMethod(request.getPaymentMethod());
			response.setRoomNo(request.getRoomNo());
			response.setStatus(CheckoutRequestStatus.USER_INITIATED.getCode());
			response.setStaysId(request.getStaysId());
			response.setTotalAmount(request.getTotalAmount());
			response.setUpdated(response.getCreated());
			response.setUserId(request.getUserId());
			response.setAddress(request.getAddress());
			response.setFirstName(request.getFirstName());
			response.setLastName(request.getLastName());
			response.setMobileNo(request.getMobileNo());
			response.setEmailId(request.getEmailId());
			return response;
		}
		return null;
	}

	public static CheckoutResponse convertEntityToResponse(CheckoutEntity request) {
		if(request != null){
			CheckoutResponse response = new CheckoutResponse();
			response.setId(request.getId());
			response.setGrandTotal(request.getGrandTotal());
			response.setHotelId(request.getHotelId());
			response.setOfficialTrip(request.isOfficialTrip());
			response.setPaymentMethod(request.getPaymentMethod());
			response.setRoomNo(request.getRoomNo());
			response.setStaysId(request.getStaysId());
			response.setTotalAmount(request.getTotalAmount());
			response.setUserId(request.getUserId());
			response.setAddress(request.getAddress());
			response.setFirstName(request.getFirstName());
			response.setLastName(request.getLastName());
			response.setMobileNo(request.getMobileNo());
			response.setEmailId(request.getEmailId());
			response.setUpdated(request.getUpdated());
			return response;
		}
		return null;
	}

	public static CheckoutResponse convertCheckinResponseToCheckoutResponse(CheckinResponse request, CheckoutResponse response) {
		if(request != null) {
			response = new CheckoutResponse();
			response.setHotelId(request.getHotelId());
			response.setStaysId(request.getStaysId());
			response.setOfficialTrip(request.isOfficialTrip());
			response.setUserId(request.getUserId());
			response.setCreated(request.getCreated());
			response.setUpdated(request.getUpdated());
			
			if(request.getPersonalInformation() != null){
				response.setFirstName(request.getPersonalInformation().getFirstName());
				response.setLastName(request.getPersonalInformation().getLastName());
				response.setMobileNo(request.getPersonalInformation().getNumber());
				response.setEmailId(request.getPersonalInformation().getEmailId());
				if(response.isOfficialTrip()){
					response.setAddress(request.getPersonalInformation().getOfficeAddress());
				} else {
					response.setAddress(request.getPersonalInformation().getHomeAddress());
				}
			}
			return response;
		}
		return null;
	}

	public static CheckoutEntity convertCheckoutResponseToEntity(CheckoutResponse request) {
		if(request!=null){
			CheckoutEntity response = new CheckoutEntity();
			if(StringUtils.isEmpty(request.getId())){
				response.setId(request.getId());
				response.setCreated(new Date().getTime());
				response.setUpdated(response.getCreated());
			} else {
				response.setUpdated(new Date().getTime());
			}
			response.setGrandTotal(request.getGrandTotal());
			response.setHotelId(request.getHotelId());
			response.setOfficialTrip(request.isOfficialTrip());
			response.setPaymentMethod(request.getPaymentMethod());
			response.setRoomNo(request.getRoomNo());
			response.setStatus(CheckoutRequestStatus.USER_INITIATED.getCode());
			response.setStaysId(request.getStaysId());
			response.setTotalAmount(request.getTotalAmount());
			
			response.setUserId(request.getUserId());
			response.setAddress(request.getAddress());
			response.setFirstName(request.getFirstName());
			response.setLastName(request.getLastName());
			response.setMobileNo(request.getMobileNo());
			response.setEmailId(request.getEmailId());
			return response;
		}
		return null;
	}

}
