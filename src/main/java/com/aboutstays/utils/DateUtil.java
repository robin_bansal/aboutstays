package com.aboutstays.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static final String[] FORMATS = { "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ","dd-MM-yyyy'T'HH:mm:ss'Z'",
			"dd-MM-yyyy'T'HH:mm:ssZ","dd-MM-yyyy HH:mm:ss",
			"yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
			"yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'", "MM/dd/yyyy'T'HH:mm:ss.SSSZ",
			"MM/dd/yyyy'T'HH:mm:ss.SSS", "MM/dd/yyyy'T'HH:mm:ssZ", "MM/dd/yyyy'T'HH:mm:ss", "yyyy:MM:dd HH:mm:ss",
			"yyyy-MM-dd", "yyyy MM dd" };

	public static final String INVALID_FORMAT = "InvalidFormat";
	public static final String BASE_DATE_FORMAT = "dd-MM-yyyy";
	public static final String TZ_DATE_FORMAT = "dd-MM-yyyy'T'HH:mm:ss.SSS'Z'";
	public static final String BASE_TIME_FORMAT = "HH:mm";
	public static final String MENU_ITEM_TIME_FORMAT = "hh:mm a";
	public static final String BASE_DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm";

	public static Date parseDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(BASE_DATE_FORMAT);
		sdf.setLenient(false);
		return sdf.parse(date);
	}

	public static Date parseDate(String date, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(false);
		return sdf.parse(date);
	}
	
	public static String format(Date date, String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	public static String format(Date date){
		if(date == null){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(BASE_DATE_FORMAT);
		return sdf.format(date);
	}

	public static String getSimpleDateFormat(String timestamp) {
		if (timestamp != null) {
			for (String parse : FORMATS) {
				SimpleDateFormat sdf = new SimpleDateFormat(parse);
				try {
					sdf.parse(timestamp);
					return parse;
				} catch (ParseException e) {

				}
			}
		}
		return INVALID_FORMAT;
	}
	
	public static Date modifyDate(Date date,int hours,int minutes, int seconds){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, hours);
		calendar.add(Calendar.MINUTE, minutes);
		calendar.add(Calendar.SECOND, seconds);
		return calendar.getTime();
	}
	
	public static Date setDate(Date date,int hours,int minutes, int seconds){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.SECOND, seconds);
		return calendar.getTime();
	}
	
	public static Date getDateFromMillis(long millis){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	public static Date getDobFromAge(String age) {
		try{
			Integer umar = Integer.parseInt(age);
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.YEAR, -umar);
			return calendar.getTime();
		}catch(Exception e){}
		return null;
	}

	public static String getAgeFromDob(Date dob) {
		if(dob==null)
			return null;
		Calendar calendar = Calendar.getInstance();
		int currentYear = calendar.get(Calendar.YEAR);
		calendar.setTime(dob);
		int birthYear = calendar.get(Calendar.YEAR);
		return String.valueOf(currentYear-birthYear);
	}

	public static boolean isDatePartEqual(Date date1, Date date2) {
		if(date1==null||date2==null)
			return false;
		String strDate1 = format(date1);
		String strDate2 = format(date2);
		return strDate1.equals(strDate2);
	}
	
    public static long convertStringToLong(String dateFormat, String dateValue){
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try{
            Date date = formatter.parse(dateValue);
            return date.getTime();
        } catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

}
