package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Maintenance;
import com.aboutstays.enums.MaintenanceItemType;
import com.aboutstays.pojos.MaintenanceItemByType;
import com.aboutstays.request.dto.AddMaintenanceRequest;
import com.aboutstays.request.dto.UpdateMaintenanceRequest;
import com.aboutstays.response.dto.GetMaintenaneResponse;

public class MaintenanceConverter {
	
	public static AddMaintenanceRequest getSampleRequest(){
		AddMaintenanceRequest request = new AddMaintenanceRequest();
		request.setId("hotel id");
		List<MaintenanceItemByType> listMaintenanceItems  = new ArrayList<>();
		listMaintenanceItems.add(new MaintenanceItemByType(1));
		listMaintenanceItems.add(new MaintenanceItemByType(2));
		listMaintenanceItems.add(new MaintenanceItemByType(3));
		listMaintenanceItems.add(new MaintenanceItemByType(4));
		request.setListMaintenanceItemType(listMaintenanceItems);
		return request;
	}

	public static Maintenance convertBeanToEntity(AddMaintenanceRequest request) {
		if(request != null){
			Maintenance maintenance = new Maintenance();
			maintenance.setHotelId(request.getId());
			long currentTime = new Date().getTime();
			maintenance.setCreated(currentTime);
			maintenance.setUpdated(currentTime);
			maintenance.setListItemType(request.getListMaintenanceItemType());
			return maintenance;
		}
			return null;
	}

	public static GetMaintenaneResponse converteEntityToBean(Maintenance maintenance) {
		if(maintenance != null){
			GetMaintenaneResponse response = new GetMaintenaneResponse();
			response.setId(maintenance.getHotelId());
			List<MaintenanceItemByType> listItemType = new ArrayList<>();
			if(CollectionUtils.isNotEmpty(maintenance.getListItemType())){
				for(MaintenanceItemByType itemType : maintenance.getListItemType()){
					if(itemType == null){
						continue;
					}
					MaintenanceItemByType itemDetails = new MaintenanceItemByType();
					MaintenanceItemType typeEnum = MaintenanceItemType.getByCode(itemType.getType());
					if(typeEnum != null){
						itemDetails.setType(itemType.getType());
						itemDetails.setTypeName(typeEnum.getDisplayName());
						itemDetails.setImageUrl(typeEnum.getImageUrl());
						listItemType.add(itemDetails);
					}
				}
			}
			response.setListMaintenanceItemType(listItemType);
			return response;
		}
		return null;
	}

	public static List<GetMaintenaneResponse> convertListEntityToListBean(List<Maintenance> listMaintenance) {
		if(CollectionUtils.isNotEmpty(listMaintenance)){
			List<GetMaintenaneResponse> listResponse = new ArrayList<>();
			for(Maintenance maintenance : listMaintenance){
				if(maintenance == null){
					continue;
				}
				listResponse.add(converteEntityToBean(maintenance));
			}
			return listResponse;
		}
		return null;
	}

	public static Maintenance convertUpdateBeanToEntity(UpdateMaintenanceRequest request) {
		if(request != null){
			Maintenance maintenance = new Maintenance();
			maintenance.setUpdated(new Date().getTime());
			maintenance.setHotelId(request.getId());
			maintenance.setListItemType(request.getListMaintenanceItemType());
			return maintenance;
		}
		return null;
	}


	
	

}
