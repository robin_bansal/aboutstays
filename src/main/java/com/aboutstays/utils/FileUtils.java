package com.aboutstays.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.aboutstays.constants.RestMappingConstants.FileConstants;

public class FileUtils {
	
	public static String createFileName(String filePath, String fileName, String fileType){
		return filePath + File.separatorChar + fileName + FileConstants.PERIOD + fileType;
	}
	
	public static String createFileName(String filePath, String completeFileName){
		return filePath + File.separatorChar + completeFileName;
	}
	
	public static File saveFileLocally(MultipartFile file) throws IOException{
    	byte[] bytes = file.getBytes();
		
		String catalineHomePath = System.getProperty("catalina.home");
		File tmpDir = new File(catalineHomePath+File.separator+"tmpFiles");
		if(!tmpDir.exists()){
			tmpDir.mkdirs();
		}
		File serverFile = new File(tmpDir.getAbsolutePath()+File.separator+file.getOriginalFilename());
		BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(serverFile));
		stream.write(bytes);
		stream.close();
		System.out.println(""+serverFile.getAbsolutePath());
		return serverFile;
    }

}
