package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Stays;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.enums.StaysOrdering;
import com.aboutstays.enums.StaysStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.StaysPojo;
import com.aboutstays.request.dto.AddStaysRequest;

public class StaysConverter {

	public static AddStaysRequest getSampleRequest(){
		AddStaysRequest request = new AddStaysRequest();
		request.setBookingId("bookingId");
		request.setHotelId("hotelId");
		request.setRoomId("roomId");
		request.setUserId("userId");
		request.setCheckInDate("dd-MM-yyyy");
		request.setCheckOutDate("dd-MM-yyyy");
		request.setCheckInTime("HH:mm");
		request.setCheckOutTime("HH:mm");
		request.setStaysId("staysId");
		request.setOfficial(false);
		return request;
	}
	
	public static Stays convertBeanToEntity(AddStaysRequest request) throws AboutstaysException{
		if(request == null){
			return null;
		}
		Stays stays = new Stays();
		try{
			stays.setBookingId(request.getBookingId());
			stays.setCheckInDate(DateUtil.parseDate(request.getCheckInDate()));
			stays.setCheckInTime(request.getCheckInTime());
			stays.setCheckOutTime(request.getCheckOutTime());
			stays.setHotelId(request.getHotelId());
			stays.setUserId(request.getUserId());
			stays.setRoomId(request.getRoomId());
			stays.setCheckOutDate(DateUtil.parseDate(request.getCheckOutDate()));	
			stays.setOfficial(request.isOfficial());
			stays.setCreated(new Date().getTime());
			stays.setUpdated(stays.getCreated());
			stays.setStaysStatus(StaysStatus.CHECK_IN.getCode());
		} catch(Exception e){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
		return stays;
	}

	public static StaysPojo convertEntityToBean(Stays stays) throws AboutstaysException{
		if(stays == null){
			return null;
		}
		StaysPojo staysPojo = new StaysPojo();
		try{
			staysPojo.setBookingId(stays.getBookingId());
			staysPojo.setHotelId(stays.getHotelId());
			staysPojo.setRoomId(stays.getRoomId());
			staysPojo.setStaysId(stays.getStaysId());
			staysPojo.setUserId(stays.getUserId());
			staysPojo.setCheckInTime(stays.getCheckInTime());
			staysPojo.setCheckOutTime(stays.getCheckOutTime());
			staysPojo.setCheckInDate(DateUtil.format(stays.getCheckInDate()));
			staysPojo.setCheckOutDate(DateUtil.format(stays.getCheckOutDate()));
			staysPojo.setOfficial(stays.isOfficial());
		} catch(Exception e){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
		return staysPojo;
	}
	
	public static List<StaysPojo> convertListEntityToListBean(List<Stays> listStays) throws AboutstaysException {
		if(CollectionUtils.isEmpty(listStays)){
			return null;
		}
		List<StaysPojo> listStaysPojo = new ArrayList<StaysPojo>();
		for(Stays stays : listStays){
			listStaysPojo.add(convertEntityToBean(stays));
		}
		return listStaysPojo;
	}
	
	public static List<GetStayResponse> convertListEntityToListGetResponse(List<Stays> listStays) throws AboutstaysException {
		if(CollectionUtils.isEmpty(listStays)){
			return null;
		}
		List<GetStayResponse> listStaysPojo = new ArrayList<>();
		for(Stays stays : listStays){
			listStaysPojo.add(convertEntityToStayResponse(stays));
		}
		return listStaysPojo;
	}

	public static GetStayResponse convertEntityToStayResponse(Stays stays) {
		if(stays == null){
			return null;
		}
		GetStayResponse response = new GetStayResponse();
		response.setBookingId(stays.getBookingId());
		response.setHotelId(stays.getHotelId());
		response.setRoomId(stays.getRoomId());
		response.setStaysId(stays.getStaysId());
		response.setUserId(stays.getUserId());
		response.setCheckInTime(stays.getCheckInTime());
		response.setCheckOutTime(stays.getCheckOutTime());
		response.setCheckInDate(DateUtil.format(stays.getCheckInDate()));
		response.setCheckOutDate(DateUtil.format(stays.getCheckOutDate()));
		response.setOfficial(stays.isOfficial());
		return response;
	}
	
	public static StaysOrdering getStaysOrdering(Stays stays, long currentTime, int futureStaylineHours) {
		StaysOrdering returnValue = StaysOrdering.getDefaultOrdering();
		
		try{
			Date date = DateUtil.parseDate(stays.getCheckInTime(), "HH:mm");
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			Date checkinDateTime = DateUtil.setDate(stays.getCheckInDate(), c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND)); 
			long timeBeforeCurrent = checkinDateTime.getTime() - currentTime;
			if(timeBeforeCurrent<futureStaylineHours*3600*1000){
				returnValue = StaysOrdering.CURRENT;
			} else {
				returnValue = StaysOrdering.FUTURE;
			}
			
			Date currentDate = DateUtil.setDate(new Date(), 0, 0, 0);
			if(currentDate.after(stays.getCheckOutDate())||StaysStatus.CHECKED_OUT.equals(StaysStatus.findStatusByCode(stays.getStaysStatus()))){
				returnValue = StaysOrdering.PAST;
			} 
			if(currentDate.equals(stays.getCheckOutDate())){
				returnValue = StaysOrdering.CURRENT;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnValue;
	}
}
