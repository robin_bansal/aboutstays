package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.ReservationServiceEntity;
import com.aboutstays.pojos.ReservationItemTypeInfo;
import com.aboutstays.request.dto.UpdateReservationServiceRequest;
import com.aboutstays.response.dto.GetReservationServiceResponse;
import com.aboutstays.services.impl.AddReservationServiceRequest;

public class ReservationServiceConverter {
	
	public static AddReservationServiceRequest getSampleRequest(){
		AddReservationServiceRequest request = new AddReservationServiceRequest();
		request.setId("hotel id");
		List<ReservationItemTypeInfo> listItemTypeInfo = new ArrayList<>();
		listItemTypeInfo.add(new ReservationItemTypeInfo(123));
		listItemTypeInfo.add(new ReservationItemTypeInfo(123));
		
		request.setTypeInfoList(listItemTypeInfo);
		return request;
	}

	public static ReservationServiceEntity convertBeanToEntity(AddReservationServiceRequest request) {
		if(request != null){
			ReservationServiceEntity entity = new ReservationServiceEntity();
			entity.setHotelId(request.getId());
			long currentTime = new Date().getTime();
			entity.setCreated(currentTime);
			entity.setUpdated(currentTime);
			entity.setTypeInfoList(request.getTypeInfoList());
			return entity;
		}
		return null;
	}

	public static GetReservationServiceResponse convertEntityToBean(ReservationServiceEntity entity) {
		if(entity != null){
			GetReservationServiceResponse response = new GetReservationServiceResponse();
			response.setId(entity.getHotelId());
			List<ReservationItemTypeInfo> listTypeInfo = new ArrayList<>();
			if(CollectionUtils.isNotEmpty(entity.getTypeInfoList())){
				for(ReservationItemTypeInfo info : entity.getTypeInfoList()){
					if(info == null){
						continue;
					}
					ReservationItemTypeInfo typeInfo = new ReservationItemTypeInfo();
					typeInfo.setImageUrl(info.getImageUrl());
					typeInfo.setType(info.getType());
					typeInfo.setTypeName(info.getTypeName());
					listTypeInfo.add(typeInfo);
				}
				response.setTypeInfoList(listTypeInfo);
			}
			return response;
		}
		return null;
	}

	public static List<GetReservationServiceResponse> convertListEntityToListBean(List<ReservationServiceEntity> listEntity) {
		if(CollectionUtils.isNotEmpty(listEntity)){
			List<GetReservationServiceResponse> listResponse = new ArrayList<>();
			for(ReservationServiceEntity entity : listEntity){
				if(entity == null){
					continue;
				}
				listResponse.add(convertEntityToBean(entity));
			}
			return listResponse;
		}
		return null;
	}

	public static ReservationServiceEntity convertUpdateBeanToEntity(UpdateReservationServiceRequest request) {
		if(request != null){
			ReservationServiceEntity entity = new ReservationServiceEntity();
			entity.setHotelId(request.getId());
			entity.setUpdated(new Date().getTime());
			entity.setTypeInfoList(request.getTypeInfoList());
			return entity;
		}
		return null;
	}
	
	

}
