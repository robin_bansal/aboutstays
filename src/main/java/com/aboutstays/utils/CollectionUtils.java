package com.aboutstays.utils;

import java.util.Collection;
import java.util.Map;

public class CollectionUtils {
	
	public static <T> boolean isEmpty(Collection<T> collection){
		return (collection==null||collection.isEmpty());
	}
	
	public static <T> boolean isNotEmpty(Collection<T> collection){
		return !isEmpty(collection);
	}
	
	public static boolean isMapEmpty(Map map){
		return (map == null || map.isEmpty());
	}

	public static boolean isMapNotEmpty(Map map){
		return !isMapEmpty(map);
	}
	

}
