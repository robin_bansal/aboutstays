package com.aboutstays.utils;

import java.util.Date;

import com.aboutstays.entities.BellBoyOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddBellBoyOrderRequest;

public class BellBoyOrderConverter extends BaseSessionConverter{
	
	public static AddBellBoyOrderRequest getSampleRequest(){
		AddBellBoyOrderRequest request = new AddBellBoyOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(1233l);
		request.setGsId("gsId");
		setRequestData(request);
		return request;
	}
	
	public static BellBoyOrder convertBeanToEntity(AddBellBoyOrderRequest request){
		if(request != null){
			BellBoyOrder order = new BellBoyOrder();
			order.setComments(request.getComment());
			order.setCreated(new Date().getTime());
			order.setGsId(request.getGsId());
			if(request.isRequestedNow()){
				order.setPreferredTime(order.getCreated());
				order.setStatus(GeneralOrderStatus.PROCESSING.getCode());
			} else{
				order.setPreferredTime(request.getDeliveryTime());
				order.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
			}
			order.setUpdated(new Date().getTime());
			setPojoToEntity(order, request);
			return order;
		}
		return null;
	}
	
	

}
