package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.LaundryServiceEntity;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.pojos.SupportedLaundryType;
import com.aboutstays.request.dto.LaundryServiceRequest;
import com.aboutstays.response.dto.LaundryServiceResponse;

public class LaundryServiceConverter {

	public static LaundryServiceEntity convertAddRequestToEntity(LaundryServiceRequest request){
		if(request==null){
			return null;
		}
		LaundryServiceEntity response = new LaundryServiceEntity();
		response.setHotelId(request.getServiceId());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setSupportedLaundryTypeList(request.getSupportedLaundryTypeList());
		response.setSlaApplicable(request.isSlaApplicable());
		response.setSlaTime(request.getSlaTime());
		response.setStartTime(request.getStartTime());
		response.setEndTime(request.getEndTime());
		response.setSwiftDeliveryEnabled(request.isSwiftDeliveryEnabled());
		response.setSwiftDeliveryMessage(request.getSwiftDeliveryMessage());
		response.setPickupByTime(request.getPickupByTime());
		return response;
	}
	
	public static LaundryServiceResponse convertEntityToGetResponse(LaundryServiceEntity request){
		if(request==null){
			return null;
		}
		LaundryServiceResponse response = new LaundryServiceResponse();
		response.setServiceId(request.getHotelId());
		response.setSupportedLaundryTypeList(request.getSupportedLaundryTypeList());
		response.setSlaApplicable(request.isSlaApplicable());
		response.setSlaTime(request.getSlaTime());
		response.setStartTime(request.getStartTime());
		response.setEndTime(request.getEndTime());
		response.setSwiftDeliveryEnabled(request.isSwiftDeliveryEnabled());
		response.setSwiftDeliveryMessage(request.getSwiftDeliveryMessage());
		response.setPickupByTime(request.getPickupByTime());
		if(CollectionUtils.isNotEmpty(response.getSupportedLaundryTypeList())){
			for(SupportedLaundryType supportedLaundryType : response.getSupportedLaundryTypeList()){
				LaundryItemType laundryItemType = LaundryItemType.findByCode(supportedLaundryType.getType());
				if(laundryItemType!=null){
					supportedLaundryType.setImageUrl(laundryItemType.getImageUrl());
					supportedLaundryType.setTypeName(laundryItemType.getDisplayName());
				}
			}
		}
		return response;
	}

	public static List<LaundryServiceResponse> convertEntityListToGetList(List<LaundryServiceEntity> entitieList) {
		if(CollectionUtils.isEmpty(entitieList))
			return null;
		List<LaundryServiceResponse> responseList = new ArrayList<>();
		for(LaundryServiceEntity entity : entitieList){
			responseList.add(convertEntityToGetResponse(entity));
		}
		return responseList;
	}

	public static LaundryServiceEntity convertUpdateRequestToEntity(LaundryServiceRequest request) {
		if(request==null){
			return null;
		}
		LaundryServiceEntity response = new LaundryServiceEntity();
		response.setHotelId(request.getServiceId());
		response.setUpdated(new Date().getTime());
		response.setSupportedLaundryTypeList(request.getSupportedLaundryTypeList());
		response.setSlaApplicable(request.isSlaApplicable());
		response.setSlaTime(request.getSlaTime());
		response.setStartTime(request.getStartTime());
		response.setEndTime(request.getEndTime());
		response.setSwiftDeliveryEnabled(request.isSwiftDeliveryEnabled());
		response.setSwiftDeliveryMessage(request.getSwiftDeliveryMessage());
		response.setPickupByTime(request.getPickupByTime());
		return response;
	}

	public static LaundryServiceRequest getSampleRequest() {
		LaundryServiceRequest request = new LaundryServiceRequest();
		request.setServiceId("hotel id");
		request.setSlaApplicable(true);
		request.setSlaTime(40);
		request.setStartTime("10:00 AM");
		request.setEndTime("08:00 PM");
		request.setSwiftDeliveryEnabled(true);
		request.setPickupByTime("09:00 AM");
		request.setSwiftDeliveryMessage("Laundry collected before 9AM is eligible for same day delivery.");
		List<SupportedLaundryType> supportedLaundryTypeList = new ArrayList<>();
		
		SupportedLaundryType supportedLaundryType = new SupportedLaundryType();
		supportedLaundryType.setType(1);
		supportedLaundryTypeList.add(supportedLaundryType);
		
		supportedLaundryType = new SupportedLaundryType();
		supportedLaundryType.setType(2);
		supportedLaundryTypeList.add(supportedLaundryType);
		
		supportedLaundryType = new SupportedLaundryType();
		supportedLaundryType.setType(3);
		supportedLaundryTypeList.add(supportedLaundryType);
		
		request.setSupportedLaundryTypeList(supportedLaundryTypeList);
		return request;
	}
}
