package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.GeneralService;
import com.aboutstays.enums.GeneralServiceType;
import com.aboutstays.request.dto.AddGeneralServiceRequest;
import com.aboutstays.request.dto.UpdateGeneralServiceRequest;
import com.aboutstays.response.dto.GetGSResponseWithHS;
import com.aboutstays.response.dto.GetGeneralServiceResponse;
import com.aboutstays.response.dto.GetStaysServiceResponse;

public class GeneralServicesConverter {
	
	public static AddGeneralServiceRequest getSample(){
		AddGeneralServiceRequest request = new AddGeneralServiceRequest();
		request.setActivatedImageUrl("activatedImageUrl");
		request.setServiceType(1234);
		request.setStatus(123);
		request.setUnactivatedImageUrl("unactivatedImageUrl");
		request.setNumerable(false);
		request.setTickable(true);
		request.setGeneralInfo(false);
		request.setUiOrder(123);
		request.setSupportsFuture(false);
		request.setSupportsNonPartneredHotel(false);
		request.setSupportsPast(false);
		return request;
	}

	public static GeneralService convertBeanToEntity(AddGeneralServiceRequest request){
		if(request == null){
			return null;
		}
		GeneralService service = new GeneralService();
		service.setActivatedImageUrl(request.getActivatedImageUrl());
		service.setServiceType(request.getServiceType());
		service.setStatus(request.getStatus());
		service.setUnactivatedImageUrl(request.getUnactivatedImageUrl());
		service.setServiceType(request.getServiceType());
		service.setNumerable(request.isNumerable());
		service.setTickable(request.isTickable());
		service.setGeneralInfo(request.isGeneralInfo());
		service.setExpenseId(request.getExpenseId());
		service.setUiOrder(request.getUiOrder());
		service.setSupportsFuture(request.isSupportsFuture());
		service.setSupportsNonPartneredHotel(request.isSupportsNonPartneredHotel());
		service.setSupportsPast(request.isSupportsPast());
		return service;
	}
	
	public static GetGeneralServiceResponse convertEntityToBean(GeneralService service){
		if(service == null){
			return null;
		}
		GetGeneralServiceResponse response = new GetGeneralServiceResponse();
		response.setId(service.getId());
		response.setActivatedImageUrl(service.getActivatedImageUrl());
		response.setStatus(service.getStatus());
		response.setUnactivatedImageUrl(service.getUnactivatedImageUrl());
		response.setName(GeneralServiceType.getNameFromServiceCode(service.getServiceType()));
		response.setServiceType(service.getServiceType());
		response.setTickable(service.isTickable());
		response.setNumerable(service.isNumerable());
		response.setGeneralInfo(service.getGeneralInfo());
		response.setExpenseId(service.getExpenseId());
		response.setUiOrder(service.getUiOrder());
		response.setSupportsFuture(service.isSupportsFuture());
		response.setSupportsNonPartneredHotel(service.isSupportsNonPartneredHotel());
		response.setSupportsPast(service.isSupportsPast());
		return response;
	}
	
	public static List<GetGeneralServiceResponse> convertListEntityToListBean(List<GeneralService> listRequest){
		if(CollectionUtils.isNotEmpty(listRequest)){
			List<GetGeneralServiceResponse> listResponse = new ArrayList<GetGeneralServiceResponse>();
			for(GeneralService request : listRequest){
				listResponse.add(convertEntityToBean(request));
			}
			return listResponse;
		}
		return null;
	}
	
	public static GeneralService convertUpdateBeanToEntity(UpdateGeneralServiceRequest request){
		if(request != null){
			GeneralService service = new GeneralService();
			service.setId(request.getId());
			service.setActivatedImageUrl(request.getActivatedImageUrl());
			service.setActivatedImageUrl(request.getActivatedImageUrl());
			service.setServiceType(request.getServiceType());
			service.setStatus(request.getStatus());
			service.setUnactivatedImageUrl(request.getUnactivatedImageUrl());
			service.setNumerable(request.isNumerable());
			service.setTickable(request.isTickable());
			service.setGeneralInfo(request.isGeneralInfo());
			service.setExpenseId(request.getExpenseId());
			service.setUiOrder(request.getUiOrder());
			service.setSupportsFuture(request.isSupportsFuture());
			service.setSupportsNonPartneredHotel(request.isSupportsNonPartneredHotel());
			service.setSupportsPast(request.isSupportsPast());
			return service;
		}
		return null;
	}

	public static GetGSResponseWithHS convertEntityToGetGSResponseWithHS(GeneralService service) {
		if(service != null){
			GetGSResponseWithHS response = new GetGSResponseWithHS();
			response.setActivatedImageUrl(service.getActivatedImageUrl());
			response.setId(service.getId());
			response.setUnactivatedImageUrl(service.getUnactivatedImageUrl());
			response.setStatus(service.getStatus());
			response.setName(GeneralServiceType.getNameFromServiceCode(service.getServiceType()));
			response.setServiceType(service.getServiceType());
			response.setTickable(service.isTickable());
			response.setNumerable(service.isNumerable());
			response.setGeneralInfo(service.getGeneralInfo());
			response.setUiOrder(service.getUiOrder());
			return response;
		}
		return null;
	}

	public static GetStaysServiceResponse convertEntityToGetStaysServiceResponse(GeneralService generalService) {
		if(generalService==null)
			return null;
		GetStaysServiceResponse response = new GetStaysServiceResponse();
		response.setGsId(generalService.getId());
		response.setGeneralInfo(generalService.getGeneralInfo());
		response.setServiceImageUrl(generalService.getUnactivatedImageUrl());
		response.setServiceType(generalService.getServiceType());
		response.setServiceName(GeneralServiceType.getNameFromServiceCode(generalService.getServiceType()));
		response.setShowNumber(generalService.isNumerable());
		response.setShowTick(generalService.isTickable());
		response.setStaysStatus(generalService.getStatus());
		response.setUiOrder(generalService.getUiOrder());
		return response;
	}
}
