package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.SuggestedHotels;
import com.aboutstays.request.dto.AddSuggestedHotelRequest;
import com.aboutstays.response.dto.SuggestedHotelsResponse;

public class SuggestedHotelsConverter {

	public static SuggestedHotels convertBeanToEntity(AddSuggestedHotelRequest request) {
		if(request != null) {
			SuggestedHotels response = new SuggestedHotels();
			response.setUserId(request.getUserId());
			response.setCity(request.getCity());
			response.setCreated(new Date().getTime());
			response.setHotel(request.getHotel());
			response.setUpdated(response.getCreated());
			return response;
		}
		return null;
	}

	public static SuggestedHotelsResponse convertEntityToBean(SuggestedHotels request) {
		if(request != null) {
			SuggestedHotelsResponse response = new SuggestedHotelsResponse();
			response.setUserId(request.getUserId());
			response.setCity(request.getCity());
			response.setHotel(request.getHotel());
			response.setId(request.getId());
			return response;
		}
		return null;
	}

	public static List<SuggestedHotelsResponse> convertListEntityToListBean(List<SuggestedHotels> listRequest) {
		if(CollectionUtils.isNotEmpty(listRequest)) {
			List<SuggestedHotelsResponse> listResponse = new ArrayList<>();
			for(SuggestedHotels hotel : listRequest) {
				if(hotel == null) {
					continue;
				}
				listResponse.add(convertEntityToBean(hotel));
			}
			return listResponse;
		}
		return null;
	}
	
	

}
