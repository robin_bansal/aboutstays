package com.aboutstays.utils;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.User;
import com.aboutstays.enums.MaritalStatus;
import com.aboutstays.enums.StayPreferencesType;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.PreferencesType;
import com.aboutstays.pojos.StayPreferences;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.SignupRequest;
import com.aboutstays.request.dto.UpdateContactDetails;
import com.aboutstays.request.dto.UpdatePasswordRequest;
import com.aboutstays.request.dto.UpdatePersonalDetails;
import com.aboutstays.request.dto.UpdateUserRequest;
import com.aboutstays.response.dto.GetUserResponse;
import com.aboutstays.response.dto.LoginResponse;
import com.aboutstays.response.dto.SignupResponse;

public class UserConverter {

	public static User convertSignupRequestToEntity(SignupRequest signupRequest){
		if(signupRequest==null){
			return null;
		}
		User user = new User();
		user.setCreated(new Date().getTime());
		user.setUpdated(new Date().getTime());
		user.setEmailId(signupRequest.getEmailId());
		user.setFirstName(signupRequest.getFirstName());
		user.setGender(signupRequest.getGender());
		user.setImageUrl(signupRequest.getImageUrl());
		user.setLastName(signupRequest.getLastName());
		user.setMobileNmber(signupRequest.getNumber());
		user.setPassword(signupRequest.getPassword());
		user.setOtpValidated(signupRequest.isOtpValidated());
		user.setReferralCode(signupRequest.getReferralCode());
		user.setSignupType(signupRequest.getSignupType());
		user.setTitle(signupRequest.getTitle());
		return user;
	}
	
	
	public static SignupRequest getSampleRequest(){
		SignupRequest request = new SignupRequest();
		request.setEmailId("emailId");
		request.setFirstName("firstName");
		request.setGender("Male/Female/Other");
		request.setImageUrl("imageUrl");
		request.setLastName("lastName");
		request.setNumber("mobileNumber");
		request.setOtpValidated(true);
		request.setPassword("password");
		request.setReferralCode("referralCode");
		request.setSignupType(123);
		request.setUserId("userId");
		request.setTitle("<Mr.><Miss/Mrs.>");
		return request;
	}
	
	
	public static SignupResponse convertEntityToSignupResponse(User user){
		if(user==null)
			return null;
		return new SignupResponse(user.getUserId());
	}
	
	public static UserPojo convertUserToUserPojo(User user){
		if(user == null){
			return null;
		}
		UserPojo userPojo = new UserPojo();
		userPojo.setEmailId(user.getEmailId());
		userPojo.setFirstName(user.getFirstName());
		userPojo.setLastName(user.getLastName());
		userPojo.setNumber(user.getMobileNmber());
		userPojo.setGender(user.getGender());
		userPojo.setImageUrl(user.getImageUrl());
		userPojo.setUserId(user.getUserId());
		userPojo.setAnniversaryDate(DateUtil.format(user.getAnniversary()));
		userPojo.setDob(DateUtil.format(user.getDateOfBirth()));
		userPojo.setHomeAddress(user.getHomeAddress());
		userPojo.setMaritalStatus(user.getMaritalStatus());
		userPojo.setListIdentityDocs(user.getListIdentityDocs());
		userPojo.setOfficeAddress(user.getOfficeAddress());
		List<StayPreferences> listStayPreferences = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(user.getListStayPreferences())) {
			for(PreferencesType prefs : user.getListStayPreferences()) {
				if(prefs == null) {
					continue;
				}
				StayPreferencesType type = StayPreferencesType.findStatusByCode(prefs.getPreference());
				if(type == null) {
					continue;
				}
				StayPreferences stayPrefes = new StayPreferences();
				stayPrefes.setName(type.getDisplayName());
				listStayPreferences.add(stayPrefes);
			}
			userPojo.setListStayPreferences(listStayPreferences);
		}
		userPojo.setTitle(user.getTitle());
		return userPojo;
	}
	
	public static GetUserResponse convertUserToGetUser(User user){
		if(user != null){
			GetUserResponse response = new GetUserResponse();
			response.setEmailId(user.getEmailId());
			response.setFirstName(user.getFirstName());
			response.setLastName(user.getLastName());
			response.setNumber(user.getMobileNmber());
			response.setGender(user.getGender());
			response.setImageUrl(user.getImageUrl());
			response.setUserId(user.getUserId());
			response.setAnniversaryDate(DateUtil.format(user.getAnniversary()));
			response.setDob(DateUtil.format(user.getDateOfBirth()));
			response.setHomeAddress(user.getHomeAddress());
			response.setMaritalStatus(user.getMaritalStatus());
			response.setListIdentityDocs(user.getListIdentityDocs());
			response.setOfficeAddress(user.getOfficeAddress());
			response.setCreated(user.getCreated());
			
			List<StayPreferences> listStayPreferences = new ArrayList<>();
			if(CollectionUtils.isNotEmpty(user.getListStayPreferences())) {
				for(PreferencesType prefs : user.getListStayPreferences()) {
					if(prefs == null) {
						continue;
					}
					StayPreferencesType type = StayPreferencesType.findStatusByCode(prefs.getPreference());
					if(type == null) {
						continue;
					}
					StayPreferences stayPrefes = new StayPreferences();
					stayPrefes.setName(type.getDisplayName());
					listStayPreferences.add(stayPrefes);
				}
				response.setListStayPreferences(listStayPreferences);
			}
			response.setTitle(user.getTitle());
			return response;
		}
		return null;
	}
	
	public static LoginResponse convertEntityToLoginResponse(User user){
		if(user == null){
			return null;
		}
		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setUserId(user.getUserId());
		loginResponse.setEmailId(user.getEmailId());
		loginResponse.setFirstName(user.getFirstName());
		loginResponse.setLastName(user.getLastName());
		loginResponse.setNumber(user.getMobileNmber());
		loginResponse.setGender(user.getGender());
		loginResponse.setImageUrl(user.getImageUrl());
		loginResponse.setOtpValidated(user.isOtpValidated());
		loginResponse.setReferralCode(user.getReferralCode());
		loginResponse.setPassword(user.getPassword());
		loginResponse.setDob(DateUtil.format(user.getDateOfBirth()));
		loginResponse.setAnniversaryDate(DateUtil.format(user.getAnniversary()));
		loginResponse.setHomeAddress(user.getHomeAddress());
		loginResponse.setOfficeAddress(user.getHomeAddress());
		loginResponse.setMaritalStatus(user.getMaritalStatus());
		loginResponse.setListIdentityDocs(user.getListIdentityDocs());
		loginResponse.setCreated(user.getCreated());
		loginResponse.setTitle(user.getTitle());
		List<StayPreferences> listStayPreferences = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(user.getListStayPreferences())) {
			for(PreferencesType prefs : user.getListStayPreferences()) {
				if(prefs == null) {
					continue;
				}
				StayPreferencesType type = StayPreferencesType.findStatusByCode(prefs.getPreference());
				if(type == null) {
					continue;
				}
				StayPreferences stayPrefes = new StayPreferences();
				stayPrefes.setName(type.getDisplayName());
				listStayPreferences.add(stayPrefes);
			}
			loginResponse.setListStayPreferences(listStayPreferences);
		}
		return loginResponse;
	}

	public static User convertUpdateUserRequestToUser(UpdateUserRequest updateUserRequest){
		if(updateUserRequest == null){
			return null;
		}
		User user = new User();
		user.setEmailId(updateUserRequest.getEmailId());
		user.setFirstName(updateUserRequest.getFirstName());
		user.setGender(updateUserRequest.getGender());
		user.setImageUrl(updateUserRequest.getImageUrl());
		user.setLastName(updateUserRequest.getLastName());
		user.setMobileNmber(updateUserRequest.getLastName());
		user.setOtpValidated(updateUserRequest.isOtpValidated());
		user.setReferralCode(updateUserRequest.getReferralCode());
		user.setUserId(updateUserRequest.getUserId());
		user.setUpdated(new Date().getTime());
		user.setTitle(updateUserRequest.getTitle());
		return user;
	}
	
	public static User convertUpdatePasswordRequestToUser(UpdatePasswordRequest updatePasswordRequest){
		if(updatePasswordRequest == null){
			return null;
		}
		User user = new User();
		user.setPassword(updatePasswordRequest.getPassword());
		return user;
	}
	
	public static User convertPersonalDetailsToEntity(UpdatePersonalDetails request) throws AboutstaysException{
		if(request == null){
			return null;
		}
		User user = new User();
		user.setFirstName(request.getFirstName());
		user.setLastName(request.getLastName());
		user.setMaritalStatus(request.getMaritalStatus());
		user.setTitle(request.getTitle());
		try {
			user.setDateOfBirth(DateUtil.parseDate(request.getDob()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(MaritalStatus.MARRIED.getCode().equals(request.getMaritalStatus())){
			try {
				user.setAnniversary(DateUtil.parseDate(request.getAnniversary()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			user.setAnniversary(null);
		}
		
		return user;
	}


	public static User convertContactDetailsToEntity(UpdateContactDetails request) {
		if(request == null){
			return null;
		}
		User user = new User();
		user.setUpdated(new Date().getTime());
		//user.setEmailId(request.getEmail());
		//user.setMobileNmber(request.getMobileNumber());
		user.setHomeAddress(request.getHomeAddress());
		user.setOfficeAddress(request.getOfficeAddress());
		return user;
	}


}
