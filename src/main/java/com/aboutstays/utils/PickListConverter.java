package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.PickList;
import com.aboutstays.request.dto.AddPickListRequest;
import com.aboutstays.response.dto.GetPickListResponse;

public class PickListConverter {

	public static PickList convertAddRequestToEntity(AddPickListRequest request){
		if(request==null)
			return null;
		PickList response = new PickList();
		response.setType(request.getType());
		response.setValues(request.getValues());
		return response;
	}
	
	public static GetPickListResponse convertEntityToGetResponse(PickList request){
		if(request==null)
			return null;
		GetPickListResponse response = new GetPickListResponse();
		response.setType(request.getType());
		response.setValues(request.getValues());
		return response;
	}
	
	public static List<GetPickListResponse> convertEntityListToGetList(List<PickList> requestList){
		if(CollectionUtils.isEmpty(requestList)){
			return null;
		}
		List<GetPickListResponse> responseList = new ArrayList<>(requestList.size());
		for(PickList pickList : requestList){
			responseList.add(convertEntityToGetResponse(pickList));
		}
		return responseList;
	}

	public static AddPickListRequest getSampleRequest() {
		AddPickListRequest request = new AddPickListRequest();
		request.setType(123);
		List<String> values = new ArrayList<>();
		values.add("value1");
		values.add("value2");
		values.add("value3");
		request.setValues(values);
		return request;
	}
}

