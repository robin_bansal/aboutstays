package com.aboutstays.utils;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aboutstays.constants.RestMappingConstants.FileConstants;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class AWSUploader {
	
	private static Logger logger = LoggerFactory.getLogger(AWSUploader.class);
	
	public static String uploadToS3(AmazonS3 s3client,String bucketName, String fileName, String filePath, String fileType){
		String absoluteFileName = FileUtils.createFileName(filePath, fileName, fileType);
		String keyName = fileName + FileConstants.PERIOD + fileType;
		try{
			logger.info("Uploading " + keyName + " to S3.");
			File file = new File(absoluteFileName);
			s3client.putObject(new PutObjectRequest(bucketName, keyName, file));
			logger.info(keyName + " Uploaded to S3 successfully in bucketName : " + bucketName);
			return s3client.getUrl(bucketName, absoluteFileName).toString();
		} catch(AmazonServiceException ase){
			logger.error("Caught an AmazonServiceException:", ase);
		} catch(AmazonClientException ace){
			logger.error("Caught an AmazonClientException:", ace);
		}
		return null;  
	}
	
	public static String uploadToS3(AmazonS3 s3client, String bucketName, File completeFileName, String awsFilePath){
		String awsFileName = FileUtils.createFileName(awsFilePath, completeFileName.getName());
		String keyName = awsFileName;
        try {
        	System.out.println("Uploading " + keyName + " to S3.");
//            logger.info("Uploading " + keyName + " to S3.");
            s3client.putObject(new PutObjectRequest(bucketName, keyName, completeFileName).withCannedAcl(CannedAccessControlList.PublicRead));
            System.out.println(keyName + " Uploaded to S3 successfully in bucketName : " + bucketName);
//            logger.info(keyName + " Uploaded to S3 successfully in bucketName : " + bucketName);
            return s3client.getUrl(bucketName, keyName).toString();
        } catch (AmazonServiceException ase) {
        	System.out.println("Caught an AmazonServiceException: "+ ase.getErrorMessage() + " : "+ase);
//            logger.error("Caught an AmazonServiceException:", ase);
        } catch (AmazonClientException ace) {
        	System.out.println("Caught an AmazonClientException:"+ ace.getMessage()+" :"+ace);
//            logger.error("Caught an AmazonClientException:", ace);
        } catch (Exception e){
        	System.out.println("Unhandled exception: "+e.getMessage());
        }
        return null;
	}

}
