package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Driver;
import com.aboutstays.request.dto.AddDriverRequest;
import com.aboutstays.request.dto.UpdateDriverRequest;
import com.aboutstays.response.dto.GetDriverResponse;

public class DriverConverter {
	
	public static AddDriverRequest getSampleRequest(){
		AddDriverRequest request = new AddDriverRequest();
		request.setImageUrl("imageUrl");
		request.setMobileNumber("mobileNumber");
		request.setName("name");
		return request;
	}
	
	public static Driver convertBeanToEntity(AddDriverRequest request){
		if(request != null){
			Driver driver = new Driver();
			driver.setImageUrl(request.getImageUrl());
			driver.setName(request.getName());
			driver.setMobileNumber(request.getMobileNumber());
			return driver;
		}
		return null;
	}
	
	public static GetDriverResponse convertEntityToBean(Driver driver){
		if(driver != null){
			GetDriverResponse response = new GetDriverResponse();
			response.setId(driver.getId());
			response.setImageUrl(driver.getImageUrl());
			response.setMobileNumber(driver.getMobileNumber());
			response.setName(driver.getName());
			return response;
		}
		return null;
	}
	
	public static List<GetDriverResponse> convertListEntityToListBean(List<Driver> listDriver){
		if(CollectionUtils.isNotEmpty(listDriver)){
			List<GetDriverResponse> listResponse = new ArrayList<>();
			for(Driver driver : listDriver){
				listResponse.add(convertEntityToBean(driver));
			}
			return listResponse;
		}
		return null;
	}
	
	public static Driver convertUpdateRequestToEntity(UpdateDriverRequest request){
		if(request != null){
			Driver driver = new Driver();
			driver.setImageUrl(request.getImageUrl());
			driver.setName(request.getName());
			driver.setMobileNumber(request.getMobileNumber());
			driver.setId(request.getId());
			return driver;
		}
		return null;
	}

}
