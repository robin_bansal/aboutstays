package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.MasterAmenity;
import com.aboutstays.pojos.Amenity;
import com.aboutstays.pojos.AwardPojo;
import com.aboutstays.pojos.CardAccepted;
import com.aboutstays.pojos.DirectionDetail;
import com.aboutstays.pojos.Essential;
import com.aboutstays.request.dto.AddAllMasterAmenitiesRequest;
import com.aboutstays.request.dto.AddMasterAmenityRequest;
import com.aboutstays.response.dto.GetMasterAmenityResponse;

public class AmenityConverter {

	public static MasterAmenity convertAddRequestToEntity(AddMasterAmenityRequest request){
		if(request==null)
			return null;
		MasterAmenity response = new MasterAmenity();
		response.setName(request.getName());
		response.setIconUrl(request.getIconUrl());
		response.setType(request.getType());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setUpdated(currentTime);
		return response;
	}
	
	public static GetMasterAmenityResponse convertEntityToGetResponse(MasterAmenity request){
		if(request==null)
			return null;
		GetMasterAmenityResponse response = new GetMasterAmenityResponse();
		response.setAmenityId(request.getId());
		response.setName(request.getName());
		response.setIconUrl(request.getIconUrl());
		response.setType(request.getType());
		return response;
	}
	
	public static List<MasterAmenity> convertAddLisToEntityList(List<AddMasterAmenityRequest> requestList){
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<MasterAmenity> responseList = new ArrayList<>(requestList.size());
		for(AddMasterAmenityRequest request : requestList){
			responseList.add(convertAddRequestToEntity(request));
		}
		return responseList;
	}
	
	public static List<GetMasterAmenityResponse> convertEntityListToGetList(List<MasterAmenity> requestList){
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<GetMasterAmenityResponse> responseList = new ArrayList<>(requestList.size());
		for(MasterAmenity request : requestList){
			responseList.add(convertEntityToGetResponse(request));
		}
		return responseList;
	}

	public static AddMasterAmenityRequest getSampleRequest() {
		AddMasterAmenityRequest request = new AddMasterAmenityRequest();
		request.setIconUrl("iconUrl");
		request.setName("name");
		request.setType(12);
		return request;
	}

	public static Amenity convertMasterEntityToAmenity(MasterAmenity masterAmenity) {
		if(masterAmenity==null)
			return null;
		Amenity amenity = new Amenity();
		amenity.setAmenityId(masterAmenity.getId());
		amenity.setIconUrl(masterAmenity.getIconUrl());
		amenity.setName(masterAmenity.getName());
		amenity.setType(masterAmenity.getType());
		return amenity;
	}
	public static CardAccepted convertMasterEntityToCard(MasterAmenity masterAmenity) {
		if(masterAmenity==null)
			return null;
		CardAccepted amenity = new CardAccepted();
		amenity.setCardId(masterAmenity.getId());
		amenity.setIconUrl(masterAmenity.getIconUrl());
		amenity.setName(masterAmenity.getName());
		amenity.setType(masterAmenity.getType());
		return amenity;
	}
	public static Essential convertMasterEntityToEssensial(MasterAmenity masterAmenity) {
		if(masterAmenity==null)
			return null;
		Essential essential = new Essential();
		essential.setEssentialId(masterAmenity.getId());
		essential.setIconUrl(masterAmenity.getIconUrl());
		essential.setName(masterAmenity.getName());
		essential.setType(masterAmenity.getType());
		return essential;
	}
	public static AwardPojo convertMasterEntityToAwards(MasterAmenity masterAmenity) {
		if(masterAmenity==null)
			return null;
		AwardPojo award = new AwardPojo();
		award.setAwardId(masterAmenity.getId());
		award.setIconUrl(masterAmenity.getIconUrl());
		award.setName(masterAmenity.getName());
		award.setType(masterAmenity.getType());
		return award;
	}

	public static AddAllMasterAmenitiesRequest getAddAllSampleRequest() {
		AddAllMasterAmenitiesRequest request = new AddAllMasterAmenitiesRequest();
		List<AddMasterAmenityRequest> addList = new ArrayList<>();
		addList.add(getSampleRequest());
		addList.add(getSampleRequest());
		addList.add(getSampleRequest());
		request.setAmenities(addList);
		return request;
	}

	public static DirectionDetail convertMasterAmentityToDirectionDetail(MasterAmenity masterAmenity,
			String directionInfo) {
		if(masterAmenity==null){
			return null;
		}
		DirectionDetail directionDetail = new DirectionDetail();
		directionDetail.setAmenityId(masterAmenity.getId());
		directionDetail.setName(masterAmenity.getName());
		directionDetail.setIconUrl(masterAmenity.getIconUrl());
		directionDetail.setType(masterAmenity.getType());
		directionDetail.setDirectionInfo(directionInfo);
		return directionDetail;
	}
}
