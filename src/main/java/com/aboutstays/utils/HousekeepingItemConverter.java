package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.HousekeepingItem;
import com.aboutstays.request.dto.HousekeepingItemRequest;
import com.aboutstays.response.dto.HousekeepingItemResponse;

public class HousekeepingItemConverter {
	
	public static HousekeepingItemRequest getSampleRequest(){
		HousekeepingItemRequest request = new HousekeepingItemRequest();
		request.setComplementory(true);
		request.setHotelId("hotelId");
		request.setType(1234);
		request.setHousekeepingServicesId("housekeepingServiceId");
		request.setMrp(50.5);
		request.setName("itemName");
		request.setPrice(40.6);
		request.setCategory("Edibles");
		request.setShowOnApp(true);
		return request;
	}

	public static HousekeepingItem convertRequestToEntity(HousekeepingItemRequest request){
		if(request == null){
			return null;
		}
		HousekeepingItem response = new HousekeepingItem();
		response.setComplementory(request.isComplementory());
		response.setHotelId(request.getHotelId());
		response.setHousekeepingItemType(request.getType());
		response.setHousekeepingServicesId(request.getHousekeepingServicesId());
		response.setMrp(request.getMrp());
		response.setName(request.getName());
		response.setPrice(request.getPrice());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setCategory(request.getCategory());
		response.setShowOnApp(response.isShowOnApp());
		return response;
	}

	public static HousekeepingItemResponse convertEntityToResponse(HousekeepingItem item) {
		if(item == null){
			return null;
		}
		HousekeepingItemResponse response = new HousekeepingItemResponse();
		response.setComplementory(item.isComplementory());
		response.setHotelId(item.getHotelId());
		response.setHousekeepingServicesId(item.getHousekeepingServicesId());
		response.setType(item.getHousekeepingItemType());
		response.setItemId(item.getId());
		response.setMrp(item.getMrp());
		response.setName(item.getName());
		response.setPrice(item.getPrice());
		response.setCategory(item.getCategory());
		response.setShowOnApp(item.isShowOnApp());
		return response;
	}

	public static List<HousekeepingItemResponse> convertListEntityToListResponse(List<HousekeepingItem> listHKItems) {
		if(CollectionUtils.isNotEmpty(listHKItems)){
			List<HousekeepingItemResponse> listResponse = new ArrayList<>();
			for(HousekeepingItem item : listHKItems){
				listResponse.add(convertEntityToResponse(item));
			}
			return listResponse;
		}
		return null;
	}

	public static HousekeepingItem convertUpdateRequestToEntity(HousekeepingItemRequest request) {
		if(request == null){
			return null;
		}
		HousekeepingItem response = new HousekeepingItem();
		response.setId(request.getItemId());
		response.setComplementory(request.isComplementory());
		response.setHotelId(request.getHotelId());
		response.setHousekeepingItemType(request.getType());
		response.setHousekeepingServicesId(request.getHousekeepingServicesId());
		response.setMrp(request.getMrp());
		response.setName(request.getName());
		response.setPrice(request.getPrice());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setCategory(request.getCategory());
		response.setShowOnApp(request.isShowOnApp());
		return response;
	}
}
