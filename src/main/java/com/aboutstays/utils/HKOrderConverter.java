package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.HousekeepingOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.pojos.HousekeepingItemOrderDetails;
import com.aboutstays.request.dto.AddHKOrderRequest;
import com.aboutstays.request.dto.UpdateHKOrderRequest;
import com.aboutstays.response.dto.GetHKOrderResponse;

public class HKOrderConverter extends BaseSessionConverter{
	
	public static AddHKOrderRequest getSampleRequest(){
		AddHKOrderRequest request = new AddHKOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(234L);
		List<HousekeepingItemOrderDetails> listHKItems = new ArrayList<>();
		listHKItems.add(new HousekeepingItemOrderDetails("itemId", 2, 40.0, 123));
		listHKItems.add(new HousekeepingItemOrderDetails("itemId", 3, 40.0, 123));
		listHKItems.add(new HousekeepingItemOrderDetails("itemId", 1, 40.0, 123));
		request.setListOrders(listHKItems);
		setRequestData(request);
		return request;
	}

	public static HousekeepingOrder convertBeanToEntity(AddHKOrderRequest request) {
		if(request != null){
			HousekeepingOrder response = new HousekeepingOrder();
			response.setCreated(new Date().getTime());
			response.setUpdated(response.getCreated());
			if(request.isRequestedNow()){
				response.setDeliveryTime(response.getCreated());
				response.setStatus(GeneralOrderStatus.PROCESSING.getCode());
			} else {
				response.setDeliveryTime(request.getDeliveryTime());
				response.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
			}
			response.setHkItemOrders(request.getListOrders());
			response.setComments(request.getComment());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

	public static GetHKOrderResponse convertEntityToBean(HousekeepingOrder request) {
		if(request != null){
			GetHKOrderResponse response = new GetHKOrderResponse();
			response.setComment(request.getComments());
			response.setId(request.getId());
			response.setDeliveryTime(request.getDeliveryTime());
			List<HousekeepingItemOrderDetails> listItemOrders = new ArrayList<>();
			for(HousekeepingItemOrderDetails details : request.getHkItemOrders()){
				if(details == null){
					continue;
				}
				HousekeepingItemOrderDetails item = new HousekeepingItemOrderDetails();
				item.setHkItemId(details.getHkItemId());
				item.setType(details.getType());
				item.setPrice(details.getPrice());
				item.setQuantity(details.getQuantity());
				item.setTypeName(HousekeepingItemType.getNameByCode(details.getType()));
				item.setName(details.getName());
				listItemOrders.add(item);
			}
			response.setListOrders(listItemOrders);
			response.setStatus(request.getStatus());
			setEntityToPojo(response, request);
			return response;
		}
		return null;
	}

	public static List<GetHKOrderResponse> convertListEntityToListBean(List<HousekeepingOrder> listOrders) {
		if(CollectionUtils.isNotEmpty(listOrders)){
			List<GetHKOrderResponse> listResponse = new ArrayList<>();
			for(HousekeepingOrder order : listOrders){
				listResponse.add(convertEntityToBean(order));
			}
			return listResponse;
		}
		return null;
	}

	public static HousekeepingOrder convertUpdateRequestToEntity(UpdateHKOrderRequest request) {
		if(request != null){
			HousekeepingOrder response = new HousekeepingOrder();
			response.setUpdated(new Date().getTime());
			response.setDeliveryTime(request.getDeliveryTime());
			response.setHkItemOrders(request.getListOrders());
			response.setId(request.getId());
			response.setComments(request.getComment());
			response.setStatus(request.getStatus());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

}
