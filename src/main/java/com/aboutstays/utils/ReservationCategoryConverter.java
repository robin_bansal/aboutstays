package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.ReservationCategory;
import com.aboutstays.enums.ReservationType;
import com.aboutstays.pojos.Direction;
import com.aboutstays.request.dto.AddReservationCategoryRequest;
import com.aboutstays.request.dto.UpdateReservationCategoryRequest;
import com.aboutstays.response.dto.GetReservationCategoryResponse;

public class ReservationCategoryConverter {

	public static ReservationCategory convertAddRequestToEntity(AddReservationCategoryRequest request){
		if(request==null)
			return null;
		ReservationCategory response = new ReservationCategory();
		response.setAdditionalImageUrls(request.getAdditionalImageUrls());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setDescription(request.getDescription());
		response.setEndTime(request.getEndTime());
		response.setEntryCriteria(request.getEntryCriteria());
		response.setFloorId(request.getFloorId());
		response.setHasDeals(request.isHasDeals());
		response.setHotelId(request.getHotelId());
		response.setImageUrl(request.getImageUrl());
		response.setMostPopular(request.getMostPopular());
		response.setName(request.getName());
		response.setProvideReservations(request.isProvideReservations());
		response.setReservationType(request.getReservationType());
		response.setStartTime(request.getStartTime());
		response.setUpdated(currentTime);
		response.setWingId(request.getWingId());
		response.setPrimaryCuisine(request.getPrimaryCuisine());
		response.setGuestSenseCategory(request.getGuestSenseCategory());
		response.setAmenityType(request.getAmenityType());
		return response;
	}
	
	public static GetReservationCategoryResponse convertEntityToGetResponse(ReservationCategory request){
		if(request==null)
			return null;
		GetReservationCategoryResponse response = new GetReservationCategoryResponse();
		response.setCategoryId(request.getId());
		response.setAdditionalImageUrls(request.getAdditionalImageUrls());
		response.setDescription(request.getDescription());
		response.setEndTime(request.getEndTime());
		response.setEntryCriteria(request.getEntryCriteria());
		response.setFloorId(request.getFloorId());
		response.setHasDeals(request.isHasDeals());
		response.setHotelId(request.getHotelId());
		response.setImageUrl(request.getImageUrl());
		response.setMostPopular(request.getMostPopular());
		response.setName(request.getName());
		response.setProvideReservations(request.isProvideReservations());
		response.setReservationType(request.getReservationType());
		response.setStartTime(request.getStartTime());
		response.setWingId(request.getWingId());
		ReservationType reservationType = ReservationType.getByCode(request.getReservationType());
		response.setReservationCategoryName(reservationType==null?null:reservationType.getDisplayName());
		response.setPrimaryCuisine(request.getPrimaryCuisine());
		response.setGuestSenseCategory(request.getGuestSenseCategory());
		response.setAmenityType(request.getAmenityType());
		return response;
	}
	
	public static List<GetReservationCategoryResponse> convertEntityListToGetList(List<ReservationCategory> requestList){
		if(CollectionUtils.isEmpty(requestList)){
			return null;
		}
		List<GetReservationCategoryResponse> responseList = new ArrayList<>(requestList.size());
		for(ReservationCategory reservationSubCategory : requestList){
			responseList.add(convertEntityToGetResponse(reservationSubCategory));
		}
		return responseList;
	}
	
	public static ReservationCategory convertUpdateRequestToEntity(UpdateReservationCategoryRequest request){
		if(request==null)
			return null;
		ReservationCategory response = new ReservationCategory();
		response.setId(request.getCategoryId());
		response.setAdditionalImageUrls(request.getAdditionalImageUrls());
		response.setDescription(request.getDescription());
		response.setEndTime(request.getEndTime());
		response.setEntryCriteria(request.getEntryCriteria());
		response.setFloorId(request.getFloorId());
		response.setHasDeals(request.isHasDeals());
		response.setHotelId(request.getHotelId());
		response.setImageUrl(request.getImageUrl());
		response.setMostPopular(request.getMostPopular());
		response.setName(request.getName());
		response.setProvideReservations(request.isProvideReservations());
		response.setReservationType(request.getReservationType());
		response.setStartTime(request.getStartTime());
		long currentTime = new Date().getTime();
		response.setUpdated(currentTime);
		response.setWingId(request.getWingId());
		response.setPrimaryCuisine(request.getPrimaryCuisine());
		response.setGuestSenseCategory(request.getGuestSenseCategory());
		response.setAmenityType(request.getAmenityType());
		return response;
	}

	public static AddReservationCategoryRequest getSampleRequest() {
		AddReservationCategoryRequest request = new AddReservationCategoryRequest();
		List<String> additionalImageUrls = new ArrayList<>();
		additionalImageUrls.add("imageUrl2");
		additionalImageUrls.add("imageUrl3");
		additionalImageUrls.add("imageUrl4");
		request.setAdditionalImageUrls(additionalImageUrls);
		request.setDescription("description");
		request.setEndTime("hh:mm a");
		request.setEntryCriteria("entryCriteria");
		request.setFloorId("floorId");
		request.setHasDeals(false);
		request.setHotelId("hotelId");
		request.setImageUrl("imageUrl");
		List<String> mostPopular = new ArrayList<>();
		mostPopular.add("mP1");
		mostPopular.add("mP2");
		request.setMostPopular(mostPopular);
		request.setName("name");
		request.setProvideReservations(true);
		request.setReservationType(12345);
		request.setStartTime("hh:mm a");
		request.setWingId("wingId");
		request.setPrimaryCuisine("Primary Cuisine");
		request.setGuestSenseCategory("Spicy");
		request.setAmenityType("Resturant");
		List<Direction> directionList = new ArrayList<>();
		directionList.add(new Direction("directionId1","Take left"));
		directionList.add(new Direction("directionId2","Take right"));
		directionList.add(new Direction("directionId3","Go up."));
		request.setDirectionList(directionList);
		return request;
	}
	
}
