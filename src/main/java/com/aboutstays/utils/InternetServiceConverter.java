package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.InternetService;
import com.aboutstays.pojos.InternetPack;
import com.aboutstays.request.dto.AddInternetServiceRequest;
import com.aboutstays.request.dto.UpdateInternetServiceRequest;
import com.aboutstays.response.dto.GetInternetServiceResponse;

public class InternetServiceConverter {
	
	public static AddInternetServiceRequest getSampleRequest(){
		AddInternetServiceRequest request = new AddInternetServiceRequest();
		request.setId("hotel id");
		request.setSlaApplicable(true);
		request.setSlaTime(40);
		request.setStartTime("10:00 AM");
		request.setEndTime("08:00 PM");
		List<InternetPack> listPacks = new ArrayList<>();
		listPacks.add(new InternetPack("title", "description", 10.0, 9.0, true, "free"));
		listPacks.add(new InternetPack("title", "description", 10.0, 9.0, false, "paid"));
		listPacks.add(new InternetPack("title", "description", 10.0, 9.0, false, "paid"));
		listPacks.add(new InternetPack("title", "description", 10.0, 9.0, false, "paid"));
		request.setListPacks(listPacks);
		return request;
	}

	public static InternetService convertBeanToEntity(AddInternetServiceRequest request) {
		if(request != null){
			InternetService service = new InternetService();
			long currentTime = new Date().getTime();
			service.setCreated(currentTime);
			service.setUpdated(currentTime);
			service.setListPacks(request.getListPacks());
			service.setSlaApplicable(request.isSlaApplicable());
			service.setSlaTime(request.getSlaTime());
			service.setStartTime(request.getStartTime());
			service.setEndTime(request.getEndTime());
			return service;
		}
		return null;
	}

	public static List<GetInternetServiceResponse> convertListEntityToListBean(List<InternetService> listServices) {
		if(CollectionUtils.isNotEmpty(listServices)){
			List<GetInternetServiceResponse> listResponse = new ArrayList<>();
			for(InternetService service : listServices){
				if(convertEntityToBean(service) != null){
					listResponse.add(convertEntityToBean(service));
				}
			}
			return listResponse;
		}
		return null;
	}

	public static GetInternetServiceResponse convertEntityToBean(InternetService service) {
		if(service != null){
			GetInternetServiceResponse response = new GetInternetServiceResponse();
			response.setId(service.getHotelId());
			response.setListPacks(service.getListPacks());
			response.setSlaApplicable(service.isSlaApplicable());
			response.setSlaTime(service.getSlaTime());
			response.setStartTime(service.getStartTime());
			response.setEndTime(service.getEndTime());
			return response;
		}
		return null;
	}

	public static InternetService convertUpdateRequestToEntity(UpdateInternetServiceRequest request) {
		if(request != null){
			InternetService service = new InternetService();
			service.setUpdated(new Date().getTime());
			service.setHotelId(request.getId());
			service.setListPacks(request.getListPacks());
			service.setSlaApplicable(request.isSlaApplicable());
			service.setSlaTime(request.getSlaTime());
			service.setStartTime(request.getStartTime());
			service.setEndTime(request.getEndTime());
			return service;
		}
		return null;
	}
	


}
