package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.ComplementaryServicesEntity;
import com.aboutstays.pojos.TabWiseComplementaryService;
import com.aboutstays.pojos.TitleVsDescriptionList;
import com.aboutstays.request.dto.AddComplementaryServicesRequest;
import com.aboutstays.request.dto.UpdateComplementaryServicesRequest;
import com.aboutstays.response.dto.GetComplementaryServicesResponse;

public class ComplementaryServicesConverter {

	public static ComplementaryServicesEntity convertAddRequestToEntity(AddComplementaryServicesRequest request){
		if(request==null){
			return null;
		}
		ComplementaryServicesEntity response = new ComplementaryServicesEntity();
		response.setHotelId(request.getServicesId());
		response.setTabWiseServiceList(request.getTabWiseComplementaryServices());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setUpdated(currentTime);
		return response;
	}
	
	public static ComplementaryServicesEntity convertUpdateRequestToEntity(UpdateComplementaryServicesRequest request){
		if(request==null){
			return null;
		}
		ComplementaryServicesEntity response = new ComplementaryServicesEntity();
		response.setHotelId(request.getServicesId());
		response.setTabWiseServiceList(request.getTabWiseComplementaryServices());
		long currentTime = new Date().getTime();
		response.setUpdated(currentTime);
		return response;
	}
	
	public static GetComplementaryServicesResponse convertEntityToGetResponse(ComplementaryServicesEntity request){
		if(request==null){
			return null;
		}
		GetComplementaryServicesResponse response = new GetComplementaryServicesResponse();
		response.setServicesId(request.getHotelId());
		response.setTabWiseComplementaryServices(request.getTabWiseServiceList());
		return response;
	}
	
	public static List<GetComplementaryServicesResponse> convertEntityListToGetList(List<ComplementaryServicesEntity> requestList){
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<GetComplementaryServicesResponse> responseList = new ArrayList<>(requestList.size());
		for(ComplementaryServicesEntity request : requestList){
			responseList.add(convertEntityToGetResponse(request));
		}
		return responseList;
	}

	public static AddComplementaryServicesRequest getSampleRequest() {
		AddComplementaryServicesRequest request = new AddComplementaryServicesRequest();
		request.setServicesId("hotelId");
		List<TabWiseComplementaryService> tabWiseComplementaryServices = new ArrayList<>();
		
		List<TitleVsDescriptionList> services = new ArrayList<>();
		List<String> descriptionList = new ArrayList<>();
		descriptionList.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
		descriptionList.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
		descriptionList.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
		services.add(new TitleVsDescriptionList("Service 1", descriptionList));
		services.add(new TitleVsDescriptionList("Service 2", descriptionList));
		services.add(new TitleVsDescriptionList("Service 3", descriptionList));
		
		TabWiseComplementaryService tabWiseComplementaryService = new TabWiseComplementaryService();
		
		tabWiseComplementaryService.setTabName("IN ROOM");
		tabWiseComplementaryService.setServices(services);
		tabWiseComplementaryServices.add(tabWiseComplementaryService);
		
		tabWiseComplementaryService = new TabWiseComplementaryService();
		
		tabWiseComplementaryService.setTabName("AMENITIES");
		tabWiseComplementaryService.setServices(services);
		tabWiseComplementaryServices.add(tabWiseComplementaryService);
		
		tabWiseComplementaryService = new TabWiseComplementaryService();
		
		tabWiseComplementaryService.setTabName("LOYALITY PROGRAM");
		tabWiseComplementaryService.setServices(services);
		tabWiseComplementaryServices.add(tabWiseComplementaryService);
		
		request.setTabWiseComplementaryServices(tabWiseComplementaryServices);
		return request;
	}
}
