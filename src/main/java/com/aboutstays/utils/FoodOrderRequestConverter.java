package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.FoodOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.request.dto.AddFoodOrderRequest;
import com.aboutstays.request.dto.UpdateFoodOrderRequest;
import com.aboutstays.response.dto.GetFoodOrderResponse;

public class FoodOrderRequestConverter extends BaseSessionConverter{
	
	public static AddFoodOrderRequest getSampleRequest(){
		AddFoodOrderRequest request = new AddFoodOrderRequest();
		setRequestData(request);
		request.setDeliveryTime(1234l);
		request.setComment("comment");
		List<FoodItemOrderDetails> listDetails = new ArrayList<>();
		listDetails.add(new FoodItemOrderDetails("foodItemId1", 2, 150.0, 1234));
		listDetails.add(new FoodItemOrderDetails("foodItemId2", 3, 250.0, 1234));
		request.setFoodItemOrders(listDetails);
		return request;
	}
	
	public static FoodOrder convertBeanToEntity(AddFoodOrderRequest request){
		if(request == null){
			return null;
		}
		FoodOrder response = new FoodOrder();
		response.setComments(request.getComment());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		if(request.isRequestedNow()){
			response.setDeliveryTime(response.getCreated());
			response.setStatus(GeneralOrderStatus.PROCESSING.getCode());
		} else {
			response.setDeliveryTime(request.getDeliveryTime());
			response.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
		}
			
		response.setFoodItemOrders(request.getFoodItemOrders());
		setPojoToEntity(response, request);
		return response;
	}
	
	public static GetFoodOrderResponse convertEntityToBean(FoodOrder request){
		if(request == null){
			return null;
		}
		GetFoodOrderResponse response = new GetFoodOrderResponse();
		response.setComment(request.getComments());
		response.setDeliveryTime(request.getDeliveryTime());
		response.setFoodItemOrders(request.getFoodItemOrders());
		response.setId(request.getId());
		response.setStatus(request.getStatus());
		setEntityToPojo(response, request);
		return response;
	}
	
	public static List<GetFoodOrderResponse> convertListEntityToListBean(List<FoodOrder> listRequest){
		if(CollectionUtils.isEmpty(listRequest)){
			return null;
		}
		List<GetFoodOrderResponse> listResponse = new ArrayList<>();
		for(FoodOrder request : listRequest){
			listResponse.add(convertEntityToBean(request));
		}
		return listResponse;
	}
	
	public static FoodOrder convertUpdateRequestToEntity(UpdateFoodOrderRequest request){
		if(request == null){
			return null;
		}
		FoodOrder response = new FoodOrder();
		response.setComments(request.getComment());
		response.setUpdated(new Date().getTime());
		response.setDeliveryTime(request.getDeliveryTime());
		response.setFoodItemOrders(request.getFoodItemOrders());
		response.setId(request.getId());
		response.setStatus(request.getStatus());
		setPojoToEntity(response, request);
		return response;
	}
	


	

}
