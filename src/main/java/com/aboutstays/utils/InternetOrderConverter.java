package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.InternetOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.pojos.InternetPack;
import com.aboutstays.request.dto.AddInternetOrderRequest;
import com.aboutstays.response.dto.InternetOrderResponse;
import com.aboutstays.response.dto.UpdateInternetOrderRequest;

public class InternetOrderConverter extends BaseSessionConverter{
	
	public static AddInternetOrderRequest getSampleRequest(){
		AddInternetOrderRequest request = new AddInternetOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(234L);
		request.setInternetPack(new InternetPack("title", "description", 10.0, 9.0, false, "category"));
		setRequestData(request);
		return request;
		
	}

	public static InternetOrder convertBeanToEntity(AddInternetOrderRequest request) {
		if(request != null){
			InternetOrder response = new InternetOrder();
			response.setComments(request.getComment());
			response.setCreated(new Date().getTime());
			response.setInternetPack(request.getInternetPack());
			response.setUpdated(response.getCreated());
			if(request.isRequestedNow()){
				response.setStatus(GeneralOrderStatus.PROCESSING.getCode());
				response.setDeliveryTime(response.getCreated());
			} else {
				response.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
				response.setDeliveryTime(request.getDeliveryTime());
			}
			setPojoToEntity(response, request);
			return response; 
		}
		return null;
	}

	public static InternetOrderResponse convertEntityToBean(InternetOrder request) {
		if(request != null){
			InternetOrderResponse response = new InternetOrderResponse();
			response.setComment(request.getComments());
			response.setInternetPack(request.getInternetPack());
			response.setId(request.getId());
			response.setDeliveryTime(request.getDeliveryTime());
			response.setStatus(request.getStatus());
			setEntityToPojo(response, request);
			return response;
		}
			
		return null;
	}

	public static List<InternetOrderResponse> convertListEntityToListBean(List<InternetOrder> listRequest) {
		if(CollectionUtils.isNotEmpty(listRequest)){
			List<InternetOrderResponse> listResponse = new ArrayList<>();
			for(InternetOrder request : listRequest){
				if(request == null){
					continue;
				}
				listResponse.add(convertEntityToBean(request));
			}
			return listResponse;
		}
		return null;
	}

	public static InternetOrder convertUpdateEntityToBean(UpdateInternetOrderRequest request) {
		if(request != null){
			InternetOrder response = new InternetOrder();
			response.setComments(request.getComment());
			response.setInternetPack(request.getInternetPack());
			response.setUpdated(response.getCreated());
			response.setUpdated(new Date().getTime());
			response.setDeliveryTime(request.getDeliveryTime());
			response.setStatus(GeneralOrderStatus.UPDATED.getCode());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

}
