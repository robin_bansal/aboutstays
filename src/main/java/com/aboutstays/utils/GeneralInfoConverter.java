package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.GeneralInformation;
import com.aboutstays.entities.GeneralService;
import com.aboutstays.enums.GeneralInformationType;
import com.aboutstays.request.dto.AddGeneralInfoRequest;
import com.aboutstays.request.dto.UpdateGeneralInfoRequest;
import com.aboutstays.request.dto.UpdateGeneralServiceRequest;
import com.aboutstays.response.dto.GetGeneralInfoResponse;
import com.aboutstays.response.dto.GetGeneralServiceResponse;

public class GeneralInfoConverter {
	
	public static AddGeneralInfoRequest getSample(){
		AddGeneralInfoRequest request = new AddGeneralInfoRequest();
		request.setActivatedImageUrl("activatedImageUrl");
		request.setStatus(123);
		request.setInfoType(1234);
		request.setUnactivatedImageUrl("unactivatedImageUrl");
		return request;
	}
	
	public static GeneralInformation convertBeanToEntity(AddGeneralInfoRequest request){
		if(request == null){
			return null;
		}
		GeneralInformation info = new GeneralInformation();
		info.setActivatedImageUrl(request.getActivatedImageUrl());
		info.setStatus(request.getStatus());
		info.setUnactivatedImageUrl(request.getUnactivatedImageUrl());
		info.setCreated(new Date().getTime());
		info.setUpdated(new Date().getTime());
		info.setInfoType(request.getInfoType());
		return info;
	}
	
	public static GetGeneralInfoResponse convertEntityToBean(GeneralInformation info){
		if(info == null){
			return null;
		}
		GetGeneralInfoResponse response = new GetGeneralInfoResponse();
		response.setId(info.getId());
		response.setActivatedImageUrl(info.getActivatedImageUrl());
		response.setStatus(info.getStatus());
		response.setUnactivatedImageUrl(info.getUnactivatedImageUrl());
		response.setName(GeneralInformationType.getNameFromServiceCode(info.getInfoType()));
		response.setInfoType(info.getInfoType());
		return response;
	}
	
	public static List<GetGeneralInfoResponse> convertListEntityToListBean(List<GeneralInformation> listRequest){
		if(CollectionUtils.isNotEmpty(listRequest)){
			List<GetGeneralInfoResponse> listResponse = new ArrayList<>();
			for(GeneralInformation request : listRequest){
				listResponse.add(convertEntityToBean(request));
			}
			return listResponse;
		}
		return null;
	}
	
	public static GeneralInformation convertUpdateBeanToEntity(UpdateGeneralInfoRequest request){
		if(request != null){
			GeneralInformation info = new GeneralInformation();
			info.setId(request.getId());
			info.setActivatedImageUrl(request.getActivatedImageUrl());
			info.setStatus(request.getStatus());
			info.setUnactivatedImageUrl(request.getUnactivatedImageUrl());
			info.setInfoType(request.getInfoType());
			info.setUpdated(new Date().getTime());
			return info;
		}
		return null;
	}

}
