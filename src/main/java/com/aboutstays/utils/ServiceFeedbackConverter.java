package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.ServiceFeedback;
import com.aboutstays.request.dto.AddServiceFeedbackRequest;
import com.aboutstays.request.dto.UpdateServiceFeedback;
import com.aboutstays.response.dto.GetServiceFeedbackResponse;

public class ServiceFeedbackConverter {
	
	public static AddServiceFeedbackRequest getSampleRequest(){
		AddServiceFeedbackRequest request = new AddServiceFeedbackRequest();
		request.setUosId("uosId");
		request.setQuality(3);
		request.setTimeTaken(4);
		request.setComments("comments");
		return request;
	}

	public static ServiceFeedback convertBeanToEntity(AddServiceFeedbackRequest request) {
		if(request != null){
			ServiceFeedback response = new ServiceFeedback();
			response.setComments(request.getComments());
			response.setCreated(new Date().getTime());
			response.setQuality(request.getQuality());
			response.setTimeTaken(request.getTimeTaken());
			response.setUosId(request.getUosId());
			response.setUpdated(response.getCreated());
			return response;
		}
		return null;
	}

	public static GetServiceFeedbackResponse convertEntityToBean(ServiceFeedback request) {
		if(request != null){
			GetServiceFeedbackResponse response = new GetServiceFeedbackResponse();
			response.setComments(request.getComments());
			response.setQuality(request.getQuality());
			response.setTimeTaken(request.getTimeTaken());
			response.setUosId(request.getUosId());
			response.setId(request.getId());
			return response;
		}
		return null;
	}

	public static List<GetServiceFeedbackResponse> convertListEntityToListBean(List<ServiceFeedback> listRequest) {
		if(CollectionUtils.isNotEmpty(listRequest)){
			List<GetServiceFeedbackResponse> listResponse = new ArrayList<>();
			for(ServiceFeedback feedback : listRequest){
				if(feedback == null){
					continue;
				}
				listResponse.add(convertEntityToBean(feedback));
			}
			return listResponse;
		}
		return null;
	}

	public static ServiceFeedback convertUpdateBeanToEntity(UpdateServiceFeedback request) {
		if(request != null){
			ServiceFeedback response = new ServiceFeedback();
			response.setComments(request.getComments());
			response.setId(request.getId());
			response.setQuality(request.getQuality());
			response.setTimeTaken(request.getTimeTaken());
			response.setUosId(request.getUosId());
			response.setUpdated(response.getCreated());
			return response;
		}
		return null;
	}
	
	

}
