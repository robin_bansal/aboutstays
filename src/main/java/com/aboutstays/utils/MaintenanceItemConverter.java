package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.MaintenanceItem;
import com.aboutstays.request.dto.UpdateMaintenanceItemRequest;
import com.aboutstays.response.dto.AddMaintenanceItemRequest;
import com.aboutstays.response.dto.MaintenanceItemResponse;

public class MaintenanceItemConverter {
	
	public static AddMaintenanceItemRequest getSampleRequest(){
		AddMaintenanceItemRequest request = new AddMaintenanceItemRequest();
		request.setHotelId("hotelId");
		request.setItemType(123);
		request.setMaintenanceServiceId("maintenanceServiceId");
		request.setName("name");
		return request;
	}

	public static MaintenanceItem convertBeanToEntity(AddMaintenanceItemRequest request) {
		if(request != null){
			MaintenanceItem item = new MaintenanceItem();
			item.setCreated(new Date().getTime());
			item.setHotelId(request.getHotelId());
			item.setItemType(request.getItemType());
			item.setMaintenanceServiceId(request.getMaintenanceServiceId());
			item.setName(request.getName());
			item.setUpdated(new Date().getTime());
			return item;
		}
		return null;
	}

	public static MaintenanceItemResponse convertEntityToBean(MaintenanceItem item) {
		if(item != null){
			MaintenanceItemResponse response = new MaintenanceItemResponse();
			response.setHotelId(item.getHotelId());
			response.setId(item.getId());
			response.setItemType(item.getItemType());
			response.setMaintenanceServiceId(item.getMaintenanceServiceId());
			response.setName(item.getName());
			return response;
		}
		return null;
	}

	public static List<MaintenanceItemResponse> convertListEntityToListBean(List<MaintenanceItem> listItem) {
		if(CollectionUtils.isNotEmpty(listItem)){
			List<MaintenanceItemResponse> listResponse = new ArrayList<>();
			for(MaintenanceItem item : listItem){
				if(item == null){
					continue;
				}
				listResponse.add(convertEntityToBean(item));
			}
			return listResponse;
		}
		return null;
	}

	public static MaintenanceItem convertUpdateBeanToEntity(UpdateMaintenanceItemRequest request) {
		if(request != null){
			MaintenanceItem item = new MaintenanceItem();
			item.setId(request.getId());
			item.setHotelId(request.getHotelId());
			item.setItemType(request.getItemType());
			item.setMaintenanceServiceId(request.getMaintenanceServiceId());
			item.setName(request.getName());
			item.setUpdated(new Date().getTime());
			return item;
		}
		return null;
	}
	

}
