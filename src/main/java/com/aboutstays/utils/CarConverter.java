package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Car;
import com.aboutstays.enums.CarType;
import com.aboutstays.request.dto.AddCarRequest;
import com.aboutstays.request.dto.UpdateCarRequest;
import com.aboutstays.response.dto.GetCarResponse;

public class CarConverter {
	
	public static AddCarRequest getSampleRequest(){
		AddCarRequest request = new AddCarRequest();
		request.setBrand("brand");
		request.setType(1234);
		request.setNumberPlate("numberPlate");
		return request;
	}

	public static Car convertBeanToEntity(AddCarRequest request){
		if(request != null){
			Car car = new Car();
			car.setBrand(request.getBrand());
			car.setNumberPlate(request.getNumberPlate());
			car.setType(request.getType());
			return car;
		}
		return null;
	}

	
	public static GetCarResponse convertEntityToBean(Car car){
		if(car != null){
			GetCarResponse response = new GetCarResponse();
			response.setId(car.getId());
			response.setBrand(car.getBrand());
			response.setNumberPlate(car.getNumberPlate());
			response.setType(car.getType());
			response.setName(CarType.getNameFromServiceCode(car.getType()));
			return response;
		}
		return null;
	}
	
	public static List<GetCarResponse> convertListEntityToListBean(List<Car> listCar){
		if(CollectionUtils.isNotEmpty(listCar)){
			List<GetCarResponse> listResponse = new ArrayList<>();
			for(Car car : listCar){
				listResponse.add(convertEntityToBean(car));
			}
			return listResponse;
		}
		return null;
	}
	
	public static Car convertUpdateRequestToEntity(UpdateCarRequest request){
		if(request != null){
			Car car = new Car();
			car.setId(request.getId());
			car.setBrand(request.getBrand());
			car.setNumberPlate(request.getNumberPlate());
			car.setType(request.getType());
			return car;
		}
		return null;
	}
}
