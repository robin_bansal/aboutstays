package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Message;
import com.aboutstays.entities.MessageStatus;
import com.aboutstays.enums.MessageCreationType;
import com.aboutstays.request.dto.AddMessageRequest;
import com.aboutstays.response.dto.GetMessageResponse;

public class MessageConverter {

	public static Message convertAddRequestToEntity(AddMessageRequest request) {
		if (request == null) {
			return null;
		}
		Message response = new Message();
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setCreationType(request.getCreationType());
		response.setHotelId(request.getHotelId());
		response.setMessageText(request.getMessageText());
		response.setStaysId(request.getStaysId());
		response.setUpdated(currentTime);
		response.setUserId(request.getUserId());
		if (MessageStatus.findByStatus(request.getStatus()) != null) {
			response.setStatus(request.getStatus());
		}else{
			response.setStatus(MessageStatus.SENT.getStatus());			
		}
		return response;
	}

	public static GetMessageResponse convertEntityToGetResponse(Message request) {
		if (request == null) {
			return null;
		}
		GetMessageResponse response = new GetMessageResponse();
		response.setCreated(request.getCreated());
		response.setByUser(MessageCreationType.isMessageByUser(request.getCreationType()));
		response.setHotelId(request.getHotelId());
		response.setMessageText(request.getMessageText());
		response.setStaysId(request.getStaysId());
		response.setUserId(request.getUserId());
		response.setMessageId(request.getId());
		if (MessageStatus.findByStatus(request.getStatus()) != null) {
			response.setStatus(request.getStatus());
		}
		return response;
	}

	public static List<GetMessageResponse> convertEntityListToGetList(List<Message> requestList) {
		if (CollectionUtils.isEmpty(requestList)) {
			return null;
		}
		List<GetMessageResponse> responseList = new ArrayList<>(requestList.size());
		for (Message message : requestList) {
			responseList.add(convertEntityToGetResponse(message));
		}
		return responseList;
	}

	public static AddMessageRequest getSampleRequest() {
		AddMessageRequest request = new AddMessageRequest();
		request.setCreationType(123);
		request.setHotelId("hotelId");
		request.setMessageText("messageText");
		request.setStaysId("staysId");
		request.setUserId("userId");
		request.setStatus(1);
		return request;
	}
}
