package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.CityGuideItemEntity;
import com.aboutstays.pojos.Address;
import com.aboutstays.pojos.CityGuideGeneralInfo;
import com.aboutstays.request.dto.AddCityGuideItemRequest;
import com.aboutstays.request.dto.UpdateCGItemRequest;
import com.aboutstays.response.dto.GetCityGuideItemResponse;

public class CityGuideItemConverter {
	
	public static AddCityGuideItemRequest getSampleRequest(){
		AddCityGuideItemRequest item = new AddCityGuideItemRequest();
		
		CityGuideGeneralInfo info = new CityGuideGeneralInfo();		
		Address address = new Address();
		address.setAddressLine1("addressLine1");
		address.setAddressLine2("addressLine2");
		address.setCity("city");
		address.setCountry("country");
		address.setDescription("description");
		address.setLatitude("latitude");
		address.setLongitude("longitude");
		address.setPincode("pincode");
		address.setState("state");
		info.setAddress(address);
		List<String> additionalImageUrls = new ArrayList<>();
		additionalImageUrls.add("image1");
		additionalImageUrls.add("image2");
		additionalImageUrls.add("image3");
		info.setAdditionalImageUrls(additionalImageUrls);
		info.setEmail("email");
		info.setImageUrl("imageUrl");
		info.setPhone1("phone1");
		info.setPhone2("phone2");
		info.setFax("fax");
		info.setName("name");
		item.setGeneralInfo(info);
	
		item.setClosingTime("closingTime");
		item.setDescription("description");
		item.setDestinationType("destinationType");
		item.setDistance(5.0);
		item.setOpeningTime("openingTime");
	
		List<String> travelOptionsList = new ArrayList<>();
		travelOptionsList.add("image1"); 
		travelOptionsList.add("image2");
		travelOptionsList.add("image3");
		item.setTravelOptionsList(travelOptionsList);
		
		List<String> activitiesList = new ArrayList<>();
		activitiesList.add("Safari");
		activitiesList.add("Boad ride");
		activitiesList.add("climbing");
		activitiesList.add("Trekking");
		item.setActivityList(activitiesList);
		item.setRecommended(false);
		return item;
	}

	public static CityGuideItemEntity convertBeanToEntity(AddCityGuideItemRequest request) {
		if(request != null){
			CityGuideItemEntity response = new CityGuideItemEntity();
			response.setActivityList(request.getActivityList());
			response.setCityGuideId(request.getCityGuideId());
			response.setCityGuideItemType(request.getCityGuideItemType());
			response.setClosingTime(request.getClosingTime());
			response.setCreated(new Date().getTime());
			response.setDescription(request.getDescription());
			response.setDestinationType(request.getDestinationType());
			response.setDistance(request.getDistance());
			response.setGeneralInfo(request.getGeneralInfo());
			response.setHotelId(request.getHotelId());
			response.setOpeningTime(request.getOpeningTime());
			response.setTravelOptionsList(request.getTravelOptionsList());
			response.setUpdated(response.getCreated());
			response.setRecommended(request.isRecommended());
			return response;
		}
		return null;
	}

	public static GetCityGuideItemResponse convertEntityToBean(CityGuideItemEntity request) {
		if(request != null){
			GetCityGuideItemResponse response = new GetCityGuideItemResponse();
			response.setId(request.getId());
			response.setActivityList(request.getActivityList());
			response.setCityGuideId(request.getCityGuideId());
			response.setCityGuideItemType(request.getCityGuideItemType());
			response.setClosingTime(request.getClosingTime());
			response.setDescription(request.getDescription());
			response.setDestinationType(request.getDestinationType());
			response.setDistance(request.getDistance());
			response.setGeneralInfo(request.getGeneralInfo());
			response.setOpeningTime(request.getOpeningTime());
			response.setHotelId(request.getHotelId());
			response.setTravelOptionsList(request.getTravelOptionsList());
			response.setRecommended(request.isRecommended());
			return response;
		}
		return null;
	}

	public static List<GetCityGuideItemResponse> convertListEntityToListBean(List<CityGuideItemEntity> listEntity) {
		if(CollectionUtils.isNotEmpty(listEntity)){
			List<GetCityGuideItemResponse> listResponse = new ArrayList<>();
			for(CityGuideItemEntity entity : listEntity){
				if(entity == null){
					continue;
				}
				listResponse.add(convertEntityToBean(entity));
			}
			return listResponse;
		}
		return null;
	}

	public static CityGuideItemEntity convertUpdateBeanToEntity(UpdateCGItemRequest request) {
		if(request != null){
			CityGuideItemEntity response = new CityGuideItemEntity();
			response.setId(request.getId());
			response.setActivityList(request.getActivityList());
			response.setCityGuideId(request.getCityGuideId());
			response.setCityGuideItemType(request.getCityGuideItemType());
			response.setClosingTime(request.getClosingTime());
			response.setDescription(request.getDescription());
			response.setDestinationType(request.getDescription());
			response.setDistance(request.getDistance());
			response.setGeneralInfo(request.getGeneralInfo());
			response.setHotelId(request.getHotelId());
			response.setOpeningTime(request.getOpeningTime());
			response.setTravelOptionsList(request.getTravelOptionsList());
			response.setUpdated(new Date().getTime());
			response.setRecommended(request.isRecommended());
			return response;
		}
		return null;
	}
	
	
	
	

}
