package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.HouseRules;
import com.aboutstays.pojos.ChildCategoryItem;
import com.aboutstays.pojos.ParentCategoryItem;
import com.aboutstays.request.dto.AddHouseRulesRequest;
import com.aboutstays.request.dto.UpdateHouseRulesRequest;
import com.aboutstays.response.dto.GetHouseRulesResponse;

public class HouseRulesConverter {
	
	public static AddHouseRulesRequest getSampleRequest(){
		AddHouseRulesRequest request = new AddHouseRulesRequest();
		request.setId("hotelId");
		List<ParentCategoryItem> listPGItem = new ArrayList<>();
		
		List<ChildCategoryItem> listCGItem = new ArrayList<>();
		listCGItem.add(new ChildCategoryItem("title1", "description1"));
		listCGItem.add(new ChildCategoryItem("title2", "description2"));
		listCGItem.add(new ChildCategoryItem("title3", "description3"));
		
		listPGItem.add(new ParentCategoryItem("Title", listCGItem));
		listPGItem.add(new ParentCategoryItem("Title 2", listCGItem));
		listPGItem.add(new ParentCategoryItem("Title 3", listCGItem));
		listPGItem.add(new ParentCategoryItem("Title 4", listCGItem));
		listPGItem.add(new ParentCategoryItem("Title 5", listCGItem));
		listPGItem.add(new ParentCategoryItem("Title 6", listCGItem));
		
		request.setListPGItems(listPGItem);
		return request;
	}

	public static HouseRules convertBeanToEntity(AddHouseRulesRequest request) {
		if(request != null){
			HouseRules rules = new HouseRules();
			rules.setHotelId(request.getId());
			long currentTime = new Date().getTime();
			rules.setCreated(currentTime);
			rules.setUpdated(currentTime);
			rules.setListPGItem(request.getListPGItems());
			return rules;
		}
		return null;
	}

	public static GetHouseRulesResponse convertEntityToBean(HouseRules rules) {
		if(rules != null){
			GetHouseRulesResponse response = new GetHouseRulesResponse();
			response.setId(rules.getHotelId());
			response.setListPGItems(rules.getListPGItem());
			return response;
		}
		return null;
	}

	public static List<GetHouseRulesResponse> convertListEntityToListBean(List<HouseRules> listRules) {
		if(CollectionUtils.isNotEmpty(listRules)){
			List<GetHouseRulesResponse> listResponse = new ArrayList<>();
			for(HouseRules rules : listRules){
				if(rules == null){
					continue;
				}
				listResponse.add(convertEntityToBean(rules));
			}
			return listResponse;
		}
		return null;
	}

	public static HouseRules convertUpdateRequestToEntity(UpdateHouseRulesRequest request) {
		if(request != null){
			HouseRules rules = new HouseRules();
			rules.setUpdated(new Date().getTime());
			rules.setHotelId(request.getId());
			rules.setListPGItem(request.getListPGItems());
			return rules;
		}
		return null;
	}
	
	

}
