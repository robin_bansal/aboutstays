package com.aboutstays.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;

import com.aboutstays.entities.TimeZoneDetails;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.AmenityType;
import com.aboutstays.enums.CartOperation;
import com.aboutstays.enums.CartType;
import com.aboutstays.enums.HotelType;
import com.aboutstays.enums.HotelUpdateBy;
import com.aboutstays.enums.HousekeepingItemType;
import com.aboutstays.enums.LaundryFor;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.enums.PartnershipType;
import com.aboutstays.enums.PaymentMethod;
import com.aboutstays.enums.PickListType;
import com.aboutstays.enums.PlaceType;
import com.aboutstays.enums.ReservationType;
import com.aboutstays.enums.Type;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.EntertaimentItemTypeInfo;
import com.aboutstays.pojos.FoodItemOrderDetails;
import com.aboutstays.pojos.HousekeepingItemOrderDetails;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.LaundryItemOrderDetails;
import com.aboutstays.pojos.MenuItemsByType;
import com.aboutstays.pojos.PersonalInformation;
import com.aboutstays.pojos.TabWiseComplementaryService;
import com.aboutstays.pojos.TitleVsDescriptionList;
import com.aboutstays.pojos.UserStayHotelIdsPojo;
import com.aboutstays.request.dto.*;
import com.aboutstays.response.dto.AddMaintenanceItemRequest;
import com.aboutstays.services.impl.AddReservationServiceRequest;

public class ValidateUtils {

	public static void validateSignupRequest(SignupRequest signupRequest) throws AboutstaysException {
		if (signupRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SIGNUP_REQUEST);
		if (StringUtils.isEmpty(signupRequest.getFirstName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FIRST_NAME);
		if (StringUtils.isEmpty(signupRequest.getLastName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAST_NAME);
		if (StringUtils.isEmpty(signupRequest.getEmailId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_EMAIL);
		if (StringUtils.isEmpty(signupRequest.getNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NUMBER);
		if (StringUtils.isEmpty(signupRequest.getPassword()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PASSWORD);
	}

	public static void validateLoginRequest(LoginRequest loginRequest) throws AboutstaysException {
		if (loginRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LOGIN_REQUEST);
		if (StringUtils.isEmpty(loginRequest.getPassword()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PASSWORD);
	}

	public static void validateOtp(ValidateOtpRequest validateOtpRequest) throws AboutstaysException {
		if (validateOtpRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REQUEST);
		if (Type.findTypeByCode(validateOtpRequest.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_TYPE);
		if (StringUtils.isEmpty(validateOtpRequest.getValue()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_VALUE);
		if (StringUtils.isEmpty(validateOtpRequest.isValidated()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_VALIDATED);
	}

	public static void validateTypeValue(Type type, String value) throws AboutstaysException {
		if (type == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_TYPE);
		if (StringUtils.isEmpty(value))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_VALUE);
	}

	public static void validateUpdateUserRequest(UpdateUserRequest updateUserRequest) throws AboutstaysException {
		if (updateUserRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_REQUEST);
		if (StringUtils.isEmpty(updateUserRequest.getType()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_TYPE);
		if (StringUtils.isEmpty(updateUserRequest.getValue()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_VALUE);
	}

	public static void ValidateUpdatePasswordRequest(UpdatePasswordRequest updatePasswordRequest)
			throws AboutstaysException {
		if (updatePasswordRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_REQUEST);
		if (StringUtils.isEmpty(updatePasswordRequest.getPassword()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PASSWORD);
	}

	public static void validateCreateHotelRequest(CreateHotelRequest createHotelRequest) throws AboutstaysException {
		if (createHotelRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HOTEL_REQUEST);
		if (createHotelRequest.getGeneralInformation() == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HOTEL_REQUEST);
		} else {
			String checkInTime = createHotelRequest.getGeneralInformation().getCheckInTime();
			String checkOutTime = createHotelRequest.getGeneralInformation().getCheckOutTime();
			if (!StringUtils.isEmpty(checkInTime) || !StringUtils.isEmpty(checkOutTime)) {
				if (checkInTime.length() != 5 || checkOutTime.length() != 5) {
					throw new AboutstaysException(AboutstaysResponseCode.INVALID_TIME_FORMAT);
				}
				dateAndTimeValidation(checkInTime, DateUtil.BASE_TIME_FORMAT,
						AboutstaysResponseCode.INVALID_TIME_FORMAT);
				dateAndTimeValidation(checkOutTime, DateUtil.BASE_TIME_FORMAT,
						AboutstaysResponseCode.INVALID_TIME_FORMAT);
			}
		}
	}

	public static void validateAddRoomCategoryRequest(AddRoomCategoryRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_ROOM_CATEGORY_REQUEST);
		if (StringUtils.isEmpty(request.getCategoryName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_CATEGORY_NAME);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateUpdateRoomCategoryRequest(UpdateRoomCategoryRequest request) throws AboutstaysException {
		validateAddRoomCategoryRequest(request);
		if (StringUtils.isEmpty(request.getRoomCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_CATEGORY_ID);
	}

	public static void validateAddRoomRequest(AddRoomRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_ROOM_REQUEST);
		if (StringUtils.isEmpty(request.getRoomName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_NAME);
		if (StringUtils.isEmpty(request.getRoomNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_NUMBER);
		if (StringUtils.isEmpty(request.getRoomCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_CATEGORY_ID);
		if (StringUtils.isEmpty(request.getWingId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_ID);
		if (StringUtils.isEmpty(request.getFloorId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_ID);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateUpdateRoomRequest(UpdateRoomRequest request) throws AboutstaysException {
		validateAddRoomRequest(request);
		if (StringUtils.isEmpty(request.getRoomId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_ID);
	}

	public static void validateHotelType(int hotelType) throws AboutstaysException {
		if (!HotelType.matchHotelType(hotelType)) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_TYPE);
		}
	}

	public static void validateHotelId(String hotelId) throws AboutstaysException {
		if (hotelId == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_ID);
	}

	public static void validateHotelUpdateTypeValue(HotelUpdateBy type, String value) throws AboutstaysException {
		if (type == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_UPDATE_TYPE);
		if (StringUtils.isEmpty(value))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_VALUE);
	}

	public static void validateHotelGeneralInfoUpdate(UpdateHotelRequest updateHotelRequest)
			throws AboutstaysException {
		if (updateHotelRequest.getGeneralInformation() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_INFO);
		if (StringUtils.isEmpty(updateHotelRequest.getGeneralInformation().getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_NAME);
		if (StringUtils.isEmpty(updateHotelRequest.getGeneralInformation().getCheckInTime()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKIN_TIME);
		if (StringUtils.isEmpty(updateHotelRequest.getGeneralInformation().getCheckOutTime()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKOUT_TIME);
		dateAndTimeValidation(updateHotelRequest.getGeneralInformation().getCheckInTime(), DateUtil.BASE_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_TIME_FORMAT);
		dateAndTimeValidation(updateHotelRequest.getGeneralInformation().getCheckOutTime(), DateUtil.BASE_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_TIME_FORMAT);
	}

	public static void validateAddRoomToHotelRequest(AddRoomToHotelRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_ROOM_CATEGORY_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ID);
		validateAddRoomCategoryRequest(request.getAddRoomRequest());
	}

	public static void validateCreateBooking(CreateBookingRequest createBookingRequest) throws AboutstaysException {
		if (createBookingRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_BOOKING_REQUEST);
		if (StringUtils.isEmpty(createBookingRequest.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(createBookingRequest.getRoomCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_CATEGORY_ID);
		if (StringUtils.isEmpty(createBookingRequest.getReservationNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_NUMBER);
		if (StringUtils.isEmpty(createBookingRequest.getCheckInDate()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKIN_DATE);
		if (StringUtils.isEmpty(createBookingRequest.getCheckOutDate()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKOUT_DATE);

		dateAndTimeValidation(createBookingRequest.getCheckInDate(), DateUtil.BASE_DATE_FORMAT,
				AboutstaysResponseCode.INVALID_DATE_FORMAT);
		dateAndTimeValidation(createBookingRequest.getCheckOutDate(), DateUtil.BASE_DATE_FORMAT,
				AboutstaysResponseCode.INVALID_DATE_FORMAT);
		if (!StringUtils.isEmpty(createBookingRequest.getBookedByUser().getDate())) {
			dateAndTimeValidation(createBookingRequest.getBookedByUser().getDate(), DateUtil.BASE_DATE_FORMAT,
					AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
		if (!StringUtils.isEmpty(createBookingRequest.getBookedForUser().getDate())) {
			dateAndTimeValidation(createBookingRequest.getBookedForUser().getDate(), DateUtil.BASE_DATE_FORMAT,
					AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
	}

	public static void validateGetBookingsRequest(GetBookingsRequest getBookingsRequest) throws AboutstaysException {
		if (getBookingsRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GET_BOOKINGS_REQUEST);
		if (StringUtils.isEmpty(getBookingsRequest.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(getBookingsRequest.getReservationNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_NUMBER);
		try {
			if (!StringUtils.isEmpty(getBookingsRequest.getCheckInDate())) {
				DateUtil.parseDate(getBookingsRequest.getCheckInDate());
			}
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
	}

	public static void validateCityAndNameRequest(GetHotelsByCityAndNamesRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GET_HOTEL_REQUEST);
		}
		if (StringUtils.isEmpty(request.getName()) && StringUtils.isEmpty(request.getCity()))
			throw new AboutstaysException(AboutstaysResponseCode.NO_CITY_AND_NAME);
	}

	public static void validateUpdatePartnership(UpdatePartnershipRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PARTNERSHIP_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (PartnershipType.findValueByCode(request.getPartnershipType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HOTEL_PARTNER_TYPE);
	}

	public static void validateAddStaysRequest(AddStaysRequest request, PartnershipType partnershipType)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_STAY_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if (StringUtils.isEmpty(request.getCheckInDate()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKIN_DATE);
		if (StringUtils.isEmpty(request.getCheckOutDate()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKOUT_DATE);
		if (PartnershipType.PARTNER.equals(partnershipType)) {
			if (StringUtils.isEmpty(request.getBookingId()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_BOOKING_ID);
			if (StringUtils.isEmpty(request.getRoomId()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_ID);
			if (StringUtils.isEmpty(request.getCheckInTime()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECK_IN_TIME);
			if (StringUtils.isEmpty(request.getCheckOutTime()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECK_OUT_TIME);
		}

		if (request.getCheckInTime().length() != 5 || request.getCheckOutTime().length() != 5) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_TIME_FORMAT);
		}
		dateAndTimeValidation(request.getCheckInDate(), DateUtil.BASE_DATE_FORMAT,
				AboutstaysResponseCode.INVALID_DATE_FORMAT);
		dateAndTimeValidation(request.getCheckOutDate(), DateUtil.BASE_DATE_FORMAT,
				AboutstaysResponseCode.INVALID_DATE_FORMAT);
		dateAndTimeValidation(request.getCheckInTime(), DateUtil.BASE_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_TIME_FORMAT);
		dateAndTimeValidation(request.getCheckOutTime(), DateUtil.BASE_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_TIME_FORMAT);
	}

	public static void validateDeleteBooking(DeleteBookingRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DELETE_REQUEST);
		if (StringUtils.isEmpty(request.getBookingId()) && StringUtils.isEmpty(request.getReservationNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ID_AND_RESERVATION);
	}

	public static void validateBookingUpdateRequest(UpdateBookingRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_BOOKING_REQUEST);
		if (StringUtils.isEmpty(request.getBookingId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_BOOKING_ID);
	}

	public static void validateUserId(String userId) throws AboutstaysException {
		if (StringUtils.isEmpty(userId)) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		}

	}

	public static void validatePersonalDetails(UpdatePersonalDetails request) throws AboutstaysException {
		if (StringUtils.isEmpty(request.getTitle()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_TITLE);
		if (StringUtils.isEmpty(request.getFirstName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FIRST_NAME);
		if (StringUtils.isEmpty(request.getLastName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAST_NAME);
		if (!StringUtils.isEmpty(request.getDob())) {
			try {
				DateUtil.parseDate(request.getDob());
			} catch (ParseException e) {
				e.printStackTrace();
				throw new AboutstaysException(AboutstaysResponseCode
						.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_FORMAT));
			}
		}
		if (!StringUtils.isEmpty(request.getAnniversary())) {
			try {
				DateUtil.parseDate(request.getAnniversary());
			} catch (ParseException e) {
				e.printStackTrace();
				throw new AboutstaysException(AboutstaysResponseCode
						.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_FORMAT));
			}
		}
	}

	public static void validateGeneralServiceRequest(AddGeneralServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GENERAL_SERVICE_REQUEST);
		// if(StringUtils.isEmpty(request.getActivatedImageUrl()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_ACTIVATED_IMAGE_URL);
		// if(StringUtils.isEmpty(request.getUnactivatedImageUrl()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_UNACTIVE_IMAGE_URL);
		if (request.getServiceType() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_TYPE);
		if (request.getStatus() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_STATUS);
	}

	public static void validateAddDriverRequest(AddDriverRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_DRIVER_REQUEST);
		if (StringUtils.isEmpty(request.getMobileNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DRIVER_MOBILE_NUMBER);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DRIVER_NAME);
	}

	public static void validateAddCarRequest(AddCarRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_CAR_REQUEST);
		if (StringUtils.isEmpty(request.getNumberPlate()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CAR_NUMBER_PLATE);
	}

	public static void validateAddWingRequest(AddWingRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_WING_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_NAME);
		// if (StringUtils.isEmpty(request.getCode()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_WING_CODE);
		if (StringUtils.isEmpty(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
	}

	public static void validateAddFloorRequest(AddFloorRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVAILID_ADD_FLOOR_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_NAME);
		// if (StringUtils.isEmpty(request.getCode()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_CODE);
		if (StringUtils.isEmpty(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
		if (StringUtils.isEmpty(request.getWingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_ID);
		}
	}

	public static void validateAddAirportPickupRequest(AddAirportPickupRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_AIRPORT_PICKUP_REQUEST);
		// if(request.getDrop() != null && !request.getDrop()){
		// if(StringUtils.isEmpty(request.getAirlineName()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_AIRLINE_NAME);
		// if(StringUtils.isEmpty(request.getAirportName()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_NAME);
		// if(StringUtils.isEmpty(request.getFlightNumber()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_FLIGHT_NUMBER);
		// }
		if (request.getPickupDateAndTime() <= 0)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PICKUP_DATE_TIME);
		if (request.getPrice() == null)
			throw new AboutstaysException(AboutstaysResponseCode.NULL_MRP_PRICE);
		try {
			DateUtil.parseDate(request.getDeliverTimeString(), DateUtil.BASE_DATE_TIME_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_TIME_FORMAT));
		}

	}

	public static void validateAddUserOptedServiceRequest(AddUserOptedServiceRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_UOS_REQUEST);
		if (StringUtils.isEmpty(request.getRefId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_REF_ID);
	}

	public static void validateApprovePickupRequest(ApprovePickupRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_APPROVE_PICKUP_REQUEST);
		if (StringUtils.isEmpty(request.getCarId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CAR_ID);
		if (StringUtils.isEmpty(request.getDriverId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DRIVER_ID);
	}

	public static void validateGetUOS(GetUOSByUserRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GET_UOS_REQUEST);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if (StringUtils.isEmpty(request.getStayId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
	}

	public static void validateUpdateHotelServicesRequest(UpdateHotelServices request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HOTEL_SERVICE_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (request.getStatus() == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_STAY_STATUS);
		// if(CollectionUtils.isEmpty(request.getListGSIds()) &&
		// CollectionUtils.isMapEmpty(request.getMapGSIdVsHSId()))
		// throw new
		// AboutstaysException(AboutstaysResponseCode.EMPTY_LIST_AND_MAP_GSIDS);
	}

	public static void validateAddRoomServiceRequest(AddRoomServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_ROOM_SERVICE_REQUEST);
		if (StringUtils.isEmpty(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_SERVICE_ID);
		}
		if (CollectionUtils.isEmpty(request.getListMenuItem())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_SERVICE_MENU_TYPE_LIST);
		}
		for (MenuItemsByType menuItemsByType : request.getListMenuItem()) {
			if (StringUtils.isEmpty(menuItemsByType.getStartTime()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_START_TIME);
			if (StringUtils.isEmpty(menuItemsByType.getEndTime()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ENDING_TIME);
			dateAndTimeValidation(menuItemsByType.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
					AboutstaysResponseCode.INVALID_TIME_FORMAT);
			dateAndTimeValidation(menuItemsByType.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
					AboutstaysResponseCode.INVALID_TIME_FORMAT);
		}
	}

	public static void dateAndTimeValidation(String dateOrTime, String format, AboutstaysResponseCode responseCode)
			throws AboutstaysException {
		try {
			DateUtil.parseDate(dateOrTime, format);
		} catch (Exception e) {
			throw new AboutstaysException(responseCode);
		}
	}

	public static void validateFoodItemRequest(FoodItemRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_FOOD_ITEM_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NAME);
		if (StringUtils.isEmpty(request.getCuisineType()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CUISINE_TYPE);
		if (StringUtils.isEmpty(request.getDescription()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DESC);
		if (StringUtils.isEmpty(request.getImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_IMAGE_URL);
		if (request.getMrp() == null || request.getPrice() == null)
			throw new AboutstaysException(AboutstaysResponseCode.NULL_MRP_PRICE);
		if (request.getMrp() <= 0 || request.getPrice() <= 0 || request.getMrp() < request.getPrice())
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_MRP_PRICE);
		if (MenuItemType.getByCode(request.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MENU_ITEM_TYPE);
	}

	public static void validateAddGoesWellWithItemRequest(AddGoesWellItemsRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_GOES_WELL_ITEMS_REQ);
		if (StringUtils.isEmpty(request.getFoodItemId()))
			throw new AboutstaysException(AboutstaysResponseCode.FOOD_ITEM_NOT_FOUND);
		if (CollectionUtils.isEmpty(request.getGoesWellWithItemIds()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GOES_LIST);
		if (request.getGoesWellWithItemIds().contains(request.getFoodItemId()))
			throw new AboutstaysException(AboutstaysResponseCode.ITEM_ID_CLASH);
	}

	public static void validateAddHousekeepingServiceRequest(AddHousekeepingServiceRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HOUSEKEEPING_SERVICE_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOUSEKEEPING_SERVICE_ID);
		if (request.isSlaApplicable() && request.getSlaTime() <= 0)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SLA_TIME);
		dateAndTimeValidation(request.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_START_TIME);
		dateAndTimeValidation(request.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_END_TIME);
	}

	public static void validateAddHousekeepingItemRequest(HousekeepingItemRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HOUSEKEEPING_ITEM_REQEUST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getHousekeepingServicesId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOUSEKEEPING_SERVICE_ID);
	}

	public static void validateAddLaundryServiceRequest(LaundryServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LAUNDRY_ADD_REQUEST);
		if (StringUtils.isEmpty(request.getServiceId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAUNDRY_SERVICES_ID);
		if (CollectionUtils.isEmpty(request.getSupportedLaundryTypeList()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAUNDRY_TYPE_LIST);
		if (request.isSlaApplicable() && request.getSlaTime() <= 0)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SLA_TIME);
		dateAndTimeValidation(request.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_START_TIME);
		dateAndTimeValidation(request.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_END_TIME);
		if (request.isSwiftDeliveryEnabled()) {
			dateAndTimeValidation(request.getPickupByTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
					AboutstaysResponseCode.INVALID_PICKUP_BY_TIME);
		}
	}

	public static void validateAddLaundryItemRequest(LaundryItemRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LAUNDRY_ADD_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getLaundryServicesId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAUNDRY_SERVICES_ID);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAUNDRY_ITEM_NAME);
		if (LaundryFor.getByCode(request.getLaundryFor()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_LAUNDRY_FOR_VALUE);
		if (LaundryItemType.findByCode(request.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_LAUNDRY_TYPE);
		if (request.getMrp() == null || request.getPrice() == null)
			throw new AboutstaysException(AboutstaysResponseCode.NULL_MRP_PRICE);
		if (request.getMrp() <= 0 || request.getPrice() <= 0 || request.getMrp() < request.getPrice())
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_MRP_PRICE);
	}

	public static void validateAddFoodOrderRequest(AddFoodOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_FOOD_REQUEST);
		long currentTime = new Date().getTime();
		if (!request.isRequestedNow()) {
			if (request.getDeliveryTime() < currentTime)
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_DELIVERY_TIME);
		}
	}

	public static void validateUpdateFoodOrderRequest(UpdateFoodOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_FOOD_REQUEST);
	}

	public static void validateCartRequest(UserCartRequest<?> cartRequest) throws AboutstaysException {
		if (cartRequest == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CART_REQUEST);
		if (StringUtils.isEmpty(cartRequest.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if (StringUtils.isEmpty(cartRequest.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(cartRequest.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
	}

	public static void validateFoodItemOrderDetails(FoodItemOrderDetails foodItemOrderDetails)
			throws AboutstaysException {
		if (foodItemOrderDetails == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_FOOD_ITEM_REQUEST);
		if (StringUtils.isEmpty(foodItemOrderDetails.getFoodItemId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FOOD_ITEM_ID);
		if (foodItemOrderDetails.getQuantity() < 0)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_QTY);
		if (MenuItemType.getByCode(foodItemOrderDetails.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_MENU_ITEM_TYPE);
	}

	public static void validateCartParams(Integer cartOperation, Integer cartType) throws AboutstaysException {
		if (CartOperation.getByCode(cartOperation) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CART_OPERATION);
		if (CartType.getByCode(cartType) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CART_TYPE);
	}

	public static void validatePlaceOrderRequest(PlaceOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PLACE_ORDER_REQUEST);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if (StringUtils.isEmpty(request.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (CartType.getByCode(request.getCartType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CAR_TYPE);
		try {
			if (!request.isRequestedNow())
				DateUtil.parseDate(request.getDeliverTimeString(), DateUtil.BASE_DATE_TIME_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_TIME_FORMAT));
		}
	}

	public static void validateAddInternetPackRequest(AddInternetServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_INTERNET_PACK_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_INTERNET_PACK_SERVICE_ID);
		if (CollectionUtils.isEmpty(request.getListPacks()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LIST_INTERNET_PACKS);
		if (request.isSlaApplicable() && request.getSlaTime() <= 0)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SLA_TIME);
		dateAndTimeValidation(request.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_START_TIME);
		dateAndTimeValidation(request.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_END_TIME);
	}

	public static void validateAddLaundryOrder(AddLaundryOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LAUNDRY_ORDER_REQUEST);
		if (request.isRequestedNow() == false) {
			if (request.getDeliveryTime() < new Date().getTime())
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_DELIVERY_TIME);
		}

	}

	public static void validateUpdateLaundryOrderRequest(UpdateLaundryOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_LAUNDRY_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMP_LAUNDRY_ORDER_ID);

	}

	public static void validateLaundryItemOrderDetails(LaundryItemOrderDetails laundryItemOrderDetails)
			throws AboutstaysException {
		if (laundryItemOrderDetails == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LAUNDRY_ITEM_REQUEST);
		if (StringUtils.isEmpty(laundryItemOrderDetails.getLaundryItemId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAUNDRY_ITEM_ID);
		if (laundryItemOrderDetails.getQuantity() < 0)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_QTY);
		if (LaundryItemType.findByCode(laundryItemOrderDetails.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_LAUNDRY_TYPE);
	}

	public static void validateAddHkOrderRequest(AddHKOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HK_ORDER_REQUEST);
		long currentTime = new Date().getTime();
		if (!request.isRequestedNow()) {
			if (request.getDeliveryTime() < currentTime)
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_DELIVERY_TIME);
		}
	}

	public static void validateUpdateHKOrderRequest(UpdateHKOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HK_ORDER_REQUEST);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if (StringUtils.isEmpty(request.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getGsId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_ID);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HK_ORDER_ID);
	}

	public static void validateHKItemOrderDetails(HousekeepingItemOrderDetails hkItemOrderDetails)
			throws AboutstaysException {
		if (hkItemOrderDetails == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_HK_ITEM_ORDER_REQUEST);
		if (StringUtils.isEmpty(hkItemOrderDetails.getHkItemId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HK_ITEM_ID);
		if (hkItemOrderDetails.getQuantity() < 0)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_QTY);
		if (HousekeepingItemType.getByCode(hkItemOrderDetails.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_HOUSEKEEPING_ITEM_TYPE);
	}

	public static void validateGenerateDashboardRequest(UserStayHotelIdsPojo request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GENERATE_DASHBOARD_REQUEST);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		if (StringUtils.isEmpty(request.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateCreateDashboardRequest(CreateDashboardRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CREATE_DASHBOARD_REQUEST);
		}
		if (StringUtils.isEmpty(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
		if (StringUtils.isEmpty(request.getUserId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		}
		if (StringUtils.isEmpty(request.getStaysId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		}
	}

	public static void validateGeneralInfoRequest(AddGeneralInfoRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GENERAL_INFO_REQUEST);
		if (StringUtils.isEmpty(request.getActivatedImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ACTIVATED_IMAGE_URL);
		if (request.getInfoType() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_INFO_TYPE);
		if (request.getStatus() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERAL_SERVICE_STATUS);

	}

	public static void validateAddCityGuideRequest(AddCityGuideRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_CITY_GUIDE_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CITY_GUIDE_ID);
	}

	public static void validateAddCGItemRequest(AddCityGuideItemRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_CG_ITEM_REQUEST);
		if (StringUtils.isEmpty(request.getCityGuideId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CITY_GUIDE_ID);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getGeneralInfo().getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CG_ITEM_NAME);
		if (!StringUtils.isEmpty(request.getOpeningTime())) {
			if (request.getOpeningTime().length() != 5) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_TIME_FORMAT);
			}
			dateAndTimeValidation(request.getOpeningTime(), DateUtil.BASE_TIME_FORMAT,
					AboutstaysResponseCode.INVALID_TIME_FORMAT);
		}
		if (!StringUtils.isEmpty(request.getClosingTime())) {
			if (request.getClosingTime().length() != 5) {
				throw new AboutstaysException(AboutstaysResponseCode.INVALID_TIME_FORMAT);
			}
			dateAndTimeValidation(request.getClosingTime(), DateUtil.BASE_TIME_FORMAT,
					AboutstaysResponseCode.INVALID_TIME_FORMAT);
		}
	}

	public static void validateAddHouseRuleRequest(AddHouseRulesRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_HR_REQUEST);
		if (StringUtils.isEmpty(request.getId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOUSE_RULES_ID);
		}
	}

	public static void validateBellBoyOrderRequest(AddBellBoyOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_BELL_BOY_ORDER_REQUEST);
	}

	public static void validateCancelRequest(CanceRequestByUosID request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_CANCEL_FROM_REQUEST_DASHBOARD_REQUES);
		if (StringUtils.isEmpty(request.getUosId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_OPTED_SERVICE_ID);
	}

	public static void vaildateCommuteServiceRequest(AddCommuteServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_COMMUTE_SERVICE_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_COMMUTE_SERVICE_ID);
		if (CollectionUtils.isEmpty(request.getListCommutePackage()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CS_SERVICE_SUPPORTED_PACKAGES);

	}

	public static void validateCheckExistingStaysByBookingIds(GetStaysForBookingsRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_EXISTING_STAYS_GET_REQUEST);
		if (StringUtils.isEmpty(request.getUserId()) || CollectionUtils.isEmpty(request.getBookingIds()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
	}

	public static void validateAddMaintenanceRequest(AddMaintenanceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MAINTENANCE_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MAINTENANCE_ID);
	}

	public static void validateAddMaintenanceItemRequest(AddMaintenanceItemRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MAINTENANCE_ITEM_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MT_ITEM_NAME);
	}

	public static void validateAddMaintenanceOrderRequst(AddMaintenanceOrderRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MT_ORDER_REQUEST);
		try {
			if (!request.isRequestedNow())
				DateUtil.parseDate(request.getDeliverTimeString(), DateUtil.BASE_DATE_TIME_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_TIME_FORMAT));
		}
	}

	public static void validateInitiateCheckinRequest(InitiateCheckinRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_INITIATE_CHECKIN_REQ);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getStaysId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_STAYS_ID);
		if (request.getPersonalInformation() == null) {
			throw new AboutstaysException(AboutstaysResponseCode.PERSONAL_INFO_REQUIRED);
		}
		if (CollectionUtils.isEmpty(request.getIdentityDocuments()))
			throw new AboutstaysException(AboutstaysResponseCode.IDENTITY_DOCS_REQUIRED);
		if (request.isOfficialTrip()) {
			if (CollectionUtils.isEmpty(request.getCompanies()))
				throw new AboutstaysException(AboutstaysResponseCode.REQUIRED_COMPANY_INFO);
		}

	}

	public static void validatePersonalInfo(PersonalInformation personalInformation) throws AboutstaysException {
		if (personalInformation == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PERSONAL_INFO);
		if (StringUtils.isEmpty(personalInformation.getFirstName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FIRST_NAME);
		if (StringUtils.isEmpty(personalInformation.getLastName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LAST_NAME);
		if (StringUtils.isEmpty(personalInformation.getEmailId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_EMAIL);
		if (StringUtils.isEmpty(personalInformation.getDob()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DOB);
		try {
			DateUtil.parseDate(personalInformation.getDob(), DateUtil.BASE_DATE_FORMAT);
		} catch (ParseException e) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
		if (StringUtils.isEmpty(personalInformation.getNumber()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NUMBER);
		if (personalInformation.getHomeAddress() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ADDRESS);
	}

	public static void validateReservationItemRequest(AddReservationItemRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_RESERVATION_ITEM_REQUEST);
		}
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_NAME);
		if (StringUtils.isEmpty(request.getImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_IMAGE_URL);
		if (StringUtils.isEmpty(request.getDescripton()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_DESC);
		if (StringUtils.isEmpty(request.getCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_ID);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getSubCategory()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_SUB_CAT);
		if (request.isAmountApplicable() && request.getPrice() <= 0) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_MRP_PRICE);
		}
		if (request.isDurationBased() && request.getDuration() <= 0) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DURATION);
		}
		dateAndTimeValidation(request.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_START_TIME);
		dateAndTimeValidation(request.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_END_TIME);

	}

	public static void validateServiceFeedbackRequest(AddServiceFeedbackRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SERVICE_FEEDBACK_REQUEST);
		}
		if (request.getQuality() < 1 || request.getQuality() > 5) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_QUALITY_SELECTION);
		}
		if (request.getTimeTaken() < 1 || request.getTimeTaken() > 5) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_TIME_SELECTION);
		}
	}

	public static void validateServiceFeedbackRequest(UpdateServiceFeedback request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SERVICE_FEEDBACK_REQUEST);
		}
		if (request.getQuality() < 1 || request.getQuality() > 5) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_QUALITY_SELECTION);
		}
		if (request.getTimeTaken() < 1 || request.getTimeTaken() > 5) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_TIME_SELECTION);
		}
	}

	public static void validateAddMasterGenereRequest(AddMasterGenereRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_GENERE_REQUEST);
		}
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_NAME);
		if (StringUtils.isEmpty(request.getLogo()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_LOGO);
	}

	public static void validateUpdateMasterGenereRequest(UpdateMasterGenereRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_GENERE_REQUEST);
		if (StringUtils.isEmpty(request.getMasterGenereId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_ID);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_NAME);
		if (StringUtils.isEmpty(request.getLogo()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_LOGO);
	}

	public static void validateDeleteGenereRequest(DeleteGenereRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_GENERE_REQUEST);
		if (StringUtils.isEmpty(request.getMasterGenereId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_ID);
	}

	public static void validateAddMasterChannelRequest(AddMasterChannelRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_CHANNEL_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_NAME);
		if (StringUtils.isEmpty(request.getLogo()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_LOGO);
		if (StringUtils.isEmpty(request.getImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_IMAGE);
		if (StringUtils.isEmpty(request.getGenereId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_ID);
	}

	public static void validateUpdateMasterChannelRequest(UpdateMasterChannelRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_CHANNEL_REQUEST);
		if (StringUtils.isEmpty(request.getChannelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_ID);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_NAME);
		if (StringUtils.isEmpty(request.getLogo()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_LOGO);
		if (StringUtils.isEmpty(request.getImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_IMAGE);
		if (StringUtils.isEmpty(request.getGenereId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_GENERE_ID);
	}

	public static void validateAddMultipleMasterChannelRequest(AddMultipleMasterChannelRequest request)
			throws AboutstaysException {
		if (request == null || CollectionUtils.isEmpty(request.getChannels()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_CHANNEL_REQUEST);
		Set<String> nameSet = new HashSet<>();
		for (AddMasterChannelRequest addMasterChannelRequest : request.getChannels()) {
			validateAddMasterChannelRequest(addMasterChannelRequest);
			if (nameSet.remove(addMasterChannelRequest.getName().toLowerCase())) {
				throw new AboutstaysException(AboutstaysResponseCode.formatMessage(
						AboutstaysResponseCode.DUPLICATE_CHANNEL_NAME, addMasterChannelRequest.getName()));
			} else {
				nameSet.add(addMasterChannelRequest.getName().toLowerCase());
			}
		}
	}

	public static void validateAddEntertaimentServiceRequest(AddEntertaimentServiceRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_ENT_SERVICE_REQ);
		if (StringUtils.isEmpty(request.getServiceId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ENT_SERVICE_ID);
		if (CollectionUtils.isNotEmpty(request.getTypeInfoList())) {
			Set<String> typeIdSet = new HashSet<>();
			for (EntertaimentItemTypeInfo typeInfo : request.getTypeInfoList()) {
				if (typeInfo == null || StringUtils.isEmpty(typeInfo.getTypeId()))
					throw new AboutstaysException(AboutstaysResponseCode.EMPTY_TYPE_ID);
				if (typeIdSet.remove(typeInfo.getTypeId())) {
					throw new AboutstaysException(AboutstaysResponseCode.DUP_ENTERTAIMENT_TYPE);
				} else {
					typeIdSet.add(typeInfo.getTypeId());
				}
			}
		}
	}

	public static void validateEntertainmentItemRequest(AddEntertaimentItemRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ENTERTAINMENT_ITEM_REQUEST);
		}
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if (StringUtils.isEmpty(request.getServiceId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ENT_SERVICE_ID);
		if (StringUtils.isEmpty(request.getNumber())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHANNEL_NUMBER);
		}
	}

	public static void validateAddMasterCityRequest(AddMasterCityRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MASTER_CITY_REQ);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CITY_NAME);
	}

	public static void validateAddAllMasterCitiesRequest(AddAllMasterCitiesRequest request) throws AboutstaysException {
		if (request == null || CollectionUtils.isEmpty(request.getMasterCities()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MASTER_CITY_REQ);
		Set<String> names = new HashSet<>();
		for (AddMasterCityRequest addRequest : request.getMasterCities()) {
			validateAddMasterCityRequest(addRequest);
			if (names.remove(addRequest.getName())) {
				throw new AboutstaysException(AboutstaysResponseCode.DUP_CITY_NAME);
			} else {
				names.add(addRequest.getName());
			}
		}
	}

	public static void validateAddStaysRequest(AddStaysFeedbackRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.STAYS_FEEDBACK_REQUEST_INVALID);
		if (CollectionUtils.isEmpty(request.getListFeedback()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FEEDBACK_LIST);

	}

	public static void validateAddMasterAmenityRequest(AddMasterAmenityRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MASTER_AMENITY_REQ);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AMENITY_NAME);
		if (StringUtils.isEmpty(request.getIconUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AMENITY_ICON);
		if (AmenityType.findbyType(request.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_AMENITY_TYPE);
	}

	public static void validateAddAllMasterAmenityRequest(AddAllMasterAmenitiesRequest request)
			throws AboutstaysException {
		if (request == null || CollectionUtils.isEmpty(request.getAmenities()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MASTER_AMENITY_REQ);
		for (AddMasterAmenityRequest addMasterAmenityRequest : request.getAmenities()) {
			validateAddMasterAmenityRequest(addMasterAmenityRequest);
		}
	}

	public static void validateLinkAmenityToHotelRequest(AddAmenitiesToHotelRequest request)
			throws AboutstaysException {
		if (request == null || request.getAmenityIds() == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_AMENITY_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateLinkAssentialsToHotelRequest(AddEssentialToHotelRequest request)
			throws AboutstaysException {
		if (request == null || request.getEssentialsIds() == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_ESSENTIAL_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateLinkAwardToHotelRequest(AddAwardRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_AWARD_REQUEST);
		if (request.getAwardData() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMAPTY_AWARD_DATA);
		if (StringUtils.isEmpty(request.getAwardData().getAwardId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMAPTY_AWARD_ID);
		if (StringUtils.isEmpty(request.getAwardData().getYear()))
			throw new AboutstaysException(AboutstaysResponseCode.EMAPTY_AWARD_YEAR);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateLinkCardAcceptedToHotelRequest(AddCardAcceptedRequest request)
			throws AboutstaysException {
		if (request == null || request.getCardIds() == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_CARD_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateAddAmenitiesToRoomCategoryRequest(AddAmenitiesToRoomCategoryRequest request)
			throws AboutstaysException {
		if (request == null || CollectionUtils.isEmpty(request.getAmenityIds()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_AMENITY_REQUEST);
		if (StringUtils.isEmpty(request.getRoomCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_CATEGORY_ID);
	}

	public static void validateAdditionalAmenitiesToRoomRequest(AddAdditionalAmenitiesRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_AMENITY_REQUEST);
		if (StringUtils.isEmpty(request.getRoomId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_ID);
	}

	public static void validateLinkLoyaltyProgramToHotelRequest(LinkLoyaltyProgramsRequest request)
			throws AboutstaysException {
		if (request == null || request.getLoyaltyProgramsId() == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_LINK_LOYALTY_REQUEST);
		if (StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
	}

	public static void validateCheckoutRequest(InitiateCheckoutRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_INITATE_CHECKOUT_REQUEST);
		// if(StringUtils.isEmpty(request.getRoomNo()))
		// throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_NO);
		if (PaymentMethod.findByCode(request.getPaymentMethod()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PAYMENT_METHOD);
	}

	public static void validateModifyCheckutRequest(ModifyCheckoutRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_MODIFY_CHECKOUT_REQUEST);
		// if(StringUtils.isEmpty(request.getRoomNo()))
		// throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_NO);
		if (PaymentMethod.findByCode(request.getPaymentMethod()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PAYMENT_METHOD);
	}

	public static void validateFetchReservationItemsByIdRequest(FetchReservationItemsByServiceIdRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_FETCH_RESERVATION_ITEM_REQUEST);
		if (StringUtils.isEmpty(request.getRsId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_SERVICE_ID);
	}

	public static void validateIdentityDocuments(List<IdentityDoc> identityDocuments) throws AboutstaysException {
		if (CollectionUtils.isEmpty(identityDocuments))
			throw new AboutstaysException(AboutstaysResponseCode.IDENTITY_DOCS_REQUIRED);
		for (IdentityDoc identityDoc : identityDocuments) {
			if (StringUtils.isEmpty(identityDoc.getDocNumber()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DOC_NUMBER);
			// if(DocumentType.findStatusByCode(identityDoc.getDocType())==null)
			// throw new
			// AboutstaysException(AboutstaysResponseCode.INVALID_DOCUMENT_TYPE);
			if (StringUtils.isEmpty(identityDoc.getNameOnDoc()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DOC_NAME);
			if (StringUtils.isEmpty(identityDoc.getDocUrl()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_DOC_URL);
		}
	}

	public static void ModifyCheckinRequest(ModifyCheckinRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_MODIFY_CHECKIN_REQUEST);
		if (request.getEarlyCheckinRequest() != null && request.getEarlyCheckinRequested()) {
			String earlycheckinTime = request.getEarlyCheckinRequest().getRequestedCheckinTime();
			if (StringUtils.isEmpty(earlycheckinTime))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_EARLY_CHECKIN_TIME);
			dateAndTimeValidation(earlycheckinTime, DateUtil.BASE_TIME_FORMAT,
					AboutstaysResponseCode.INVALID_EARLY_CHECKIN_TIME_FORMAT);
		}
	}

	public static void validateSuggestedHotels(AddSuggestedHotelRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SUGGESTED_HOTELS);
		if (StringUtils.isEmpty(request.getHotel()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_NAME);
	}

	public static void validateAddComplementaryServicesRequest(AddComplementaryServicesRequest request)
			throws AboutstaysException {
		if (request == null || CollectionUtils.isEmpty(request.getTabWiseComplementaryServices()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_COMPLEMENTARY_SERVICES_REQUEST);
		if (StringUtils.isEmpty(request.getServicesId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_COMPLEMENTARY_SERVICE_ID);
		for (TabWiseComplementaryService tabWiseComplementaryService : request.getTabWiseComplementaryServices()) {
			if (StringUtils.isEmpty(tabWiseComplementaryService.getTabName())) {
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_TAB_NAME);
			}
			if (CollectionUtils.isEmpty(tabWiseComplementaryService.getServices()))
				continue;
			for (TitleVsDescriptionList titleVsDescription : tabWiseComplementaryService.getServices()) {
				if (StringUtils.isEmpty(titleVsDescription.getTitle()))
					throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SERVICE_NAME);
				if (CollectionUtils.isEmpty(titleVsDescription.getDescriptionList()))
					throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SERVICE_DESCRIPTION);
			}
		}
	}

	public static void validateUpdateComplementaryServicesRequest(UpdateComplementaryServicesRequest request)
			throws AboutstaysException {
		if (request == null || StringUtils.isEmpty(request.getServicesId())
				|| CollectionUtils.isEmpty(request.getTabWiseComplementaryServices()))
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_COMPLEMENTARY_SERVICES_REQUEST);
		for (TabWiseComplementaryService tabWiseComplementaryService : request.getTabWiseComplementaryServices()) {
			if (StringUtils.isEmpty(tabWiseComplementaryService.getTabName())) {
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_TAB_NAME);
			}
			if (CollectionUtils.isEmpty(tabWiseComplementaryService.getServices()))
				continue;
			for (TitleVsDescriptionList titleVsDescription : tabWiseComplementaryService.getServices()) {
				if (StringUtils.isEmpty(titleVsDescription.getTitle()))
					throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SERVICE_NAME);
				if (CollectionUtils.isEmpty(titleVsDescription.getDescriptionList()))
					throw new AboutstaysException(AboutstaysResponseCode.EMPTY_SERVICE_DESCRIPTION);
			}
		}
	}

	public static void validateGetAllStaysServiceRequest(GetAllStaysServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GET_ALL_STAYS_SERVICE_REQUEST);
	}

	public static void validateAddNotificationRequest(AddNotificationRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_NOTIFICATION_REQUEST);
		if (StringUtils.isEmpty(request.getTitle()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NTFY_TITLE);
		if (StringUtils.isEmpty(request.getDescription()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NTFY_DESCRIPTION);
		if (StringUtils.isEmpty(request.getIconUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NTFY_ICON_URL);
		if (StringUtils.isEmpty(request.getType()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_NTFY_TYPE);
	}

	public static void validateGetStayNotificationsRequest(GetStayNotificationsRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_GET_STAY_NOTIFICATION_REQUEST);
	}

	public static void validateAddMessageRequest(AddMessageRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_MESSAGE_REQUEST);
		}
		if (StringUtils.isEmpty(request.getMessageText()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_MESSAGE_TEXT);
	}

	public static void validateDeleteIdentityDocRequest(DeleteIdentityDocRequest request) throws AboutstaysException {
		if (request == null || request.getIdentityDoc() == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DELETE_REQUEST);
		if (StringUtils.isEmpty(request.getUserId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
	}

	public static void validateInternetOrder(AddInternetOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_INTERNET_ORDER_REQUEST);
		try {
			if (!request.isRequestedNow())
				DateUtil.parseDate(request.getDeliverTimeString(), DateUtil.BASE_DATE_TIME_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_TIME_FORMAT));
		}

	}

	public static void validateReservationOrder(AddReservationOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_RESERVATION_ORDER_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESEVATION_ITEM_NAME);
		try {
			if (!request.isRequestedNow())
				DateUtil.parseDate(request.getDeliverTimeString(), DateUtil.BASE_DATE_TIME_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_TIME_FORMAT));
		}

	}

	public static void validateCommuteOrder(AddCommuteOrderRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_COMMUTE_ORDER_REQUEST);
		if (StringUtils.isEmpty(request.getPackageName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_COMMUTE_PACKAGE_NAME);
		try {
			if (!request.isRequestedNow())
				DateUtil.parseDate(request.getDeliverTimeString(), DateUtil.BASE_DATE_TIME_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_GENERAL_TIME_FORMAT, DateUtil.BASE_DATE_TIME_FORMAT));
		}

	}

	public static void validateAddTimezone(TimeZoneDetails request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_TIMEZONE_REQUEST);
		if (StringUtils.isEmpty(request.getTimezone()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_TIMEZONE);
		if (request.getHoursDifference() == null)
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOURS_DIFF);
	}

	public static void validateAddAirportServiceRequest(AddAirportServiceRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_AIRPORT_SERVICE_REQUEST);
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_SERVICE_ID);
		if (CollectionUtils.isEmpty(request.getSupportedAirportList())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_VS_CAR_MAP);
		}
	}

	public static void validateAddReservationServiceRequest(AddReservationServiceRequest request)
			throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_RESERVATION_REQUEST);
		}
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_SERVICE_ID);
	}

	public static void validateLoyaltyRequest(AddLoyaltyRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_LOYALTY_REQUEST);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_LOYALTY_PROGRAM_NAME);

	}

	public static void validateAddReservationCategoryRequest(AddReservationCategoryRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_RESERVATION_CAT_REQ);
		if (StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_NAME);
		if (StringUtils.isEmpty(request.getImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_IMAGE_URL);
		if (StringUtils.isEmpty(request.getDescription()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_DESC);
		if (StringUtils.isEmpty(request.getWingId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_WING_ID);
		if (StringUtils.isEmpty(request.getFloorId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_ID);
		if (ReservationType.getByCode(request.getReservationType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.UNSUPPORTED_RESERVATION_TYPE);
		dateAndTimeValidation(request.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_START_TIME);
		dateAndTimeValidation(request.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT,
				AboutstaysResponseCode.INVALID_END_TIME);
	}

	public static void validateUpdateReservationCatRequest(UpdateReservationCategoryRequest request)
			throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_RES_CAT_REQ);
		if (StringUtils.isEmpty(request.getCategoryId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RES_CAT_ID);
		validateAddReservationCategoryRequest(request);
	}

	public static void validateAddPickListRequest(AddPickListRequest request) throws AboutstaysException {
		if (request == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_ADD_PICKLIST_REQUEST);
		if (PickListType.findByCode(request.getType()) == null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PICKLIST_TYPE);
		if (CollectionUtils.isEmpty(request.getValues()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PICKLIST_VALUES);
	}

	public static void validateUpdateReservationItemRequest(UpdateReservationItemRequest request)
			throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_UPDATE_RES_ITEM_REQUEST);
		}
		if (StringUtils.isEmpty(request.getId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_RESERVATION_ITEM_ID);
		validateReservationItemRequest(request);
	}

	public static void validateAllocateRoom(AllocateRoomRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVAILID_ALLOCATE_REQUEST);
		}
		if (StringUtils.isEmpty(request.getRoomId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_ROOM_ID);
		}
		if (StringUtils.isEmpty(request.getBookingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_BOOKING_ID);
		}
	}

	public static void validateFreezeBooking(FreezeBookingRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVAILID_ADD_FLOOR_REQUEST);
		}
		if (CollectionUtils.isEmpty(request.getBookingIds())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_BOOKING_IDS);
		}
	}

	public static void validateRoomAndStaysRequest(RoomgsAndStaysRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVAILID_ROOMS_AND_STAYS);
		}
		if (StringUtils.isEmpty(request.getFloorId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FLOOR_ID);
		}
		try {
			DateUtil.parseDate(request.getDate(), DateUtil.BASE_DATE_FORMAT);
		} catch (Exception e) {
			throw new AboutstaysException(AboutstaysResponseCode
					.formatMessage(AboutstaysResponseCode.INVALID_DATE_FORMAT, DateUtil.BASE_DATE_FORMAT));
		}
	}

	public static void validateCompleteCheckin(CompleteCheckinRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVAILID_COMPLETE_CHECKIN_REQUEST);
		}
		if (StringUtils.isEmpty(request.getBookingId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CHECKOUT_ID);
		}
	}

	public static void validateReadMessageRequest(ChangeMessageStatusRequest request) throws AboutstaysException {
		if (request == null) {
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_READ_MESSAGE_REQUEST);
		}
		if (StringUtils.isEmpty(request.getHotelId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		}
		if (StringUtils.isEmpty(request.getUserId())) {
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_USER_ID);
		}
	}

	public static void validateUpdateFoodItemRequest(UpdateFoodItemRequest request) throws AboutstaysException {
		if(request==null||StringUtils.isEmpty(request.getFooditemId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_FOOD_ITEM_ID);
	}

	public static void validateApproveEarlyCheckin(ApproveEarlyCheckinRequest request) throws AboutstaysException {
		if(StringUtils.isEmpty(request.getBookingId())){
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_BOOKING_ID);
		}
	}

	public static void validateBellBoyServiceRequest(BellBoyServiceRequest request) throws AboutstaysException {
		if(request==null){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_BELL_BOY_SERVICE_REQUEST);
		}
		if(StringUtils.isEmpty(request.getHotelId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_HOTEL_ID);
		if(request.isSlaApplicable()&&request.getSlaTime()<=0){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_SLA_TIME);
		}
		if(request.isServiceEnabled()){
			dateAndTimeValidation(request.getStartTime(), DateUtil.MENU_ITEM_TIME_FORMAT, AboutstaysResponseCode.INVALID_START_TIME);
			dateAndTimeValidation(request.getEndTime(), DateUtil.MENU_ITEM_TIME_FORMAT, AboutstaysResponseCode.INVALID_END_TIME);
		}
	}

	public static void validatePlaceRequest(PlaceRequest request, boolean isUpdate) throws AboutstaysException {
		if(request==null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PLACE_REQUEST);
		if(isUpdate){
			if(StringUtils.isEmpty(request.getPlaceId()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PLACE_ID);
		}
		if(StringUtils.isEmpty(request.getPlaceName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PLACE_NAME);
		if(StringUtils.isEmpty(request.getIconUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_PLACE_ICON);
		PlaceType placeType = PlaceType.findByType(request.getPlaceType());
		if(placeType==null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_PLACE_TYPE);
	}

	public static void validateAirportRequest(AirportRequest request, boolean isUpdate) throws AboutstaysException {
		if(request==null)
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_AIRPORT_REQUEST);
		if(isUpdate){
			if(StringUtils.isEmpty(request.getAirportId()))
				throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_ID);
		}
		if(StringUtils.isEmpty(request.getCityId()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_CITY_ID);
		if(StringUtils.isEmpty(request.getName()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_NAME);
		if(StringUtils.isEmpty(request.getDescription()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_DESCRIPTION);
		if(StringUtils.isEmpty(request.getImageUrl()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_IMAGE_URL);
		if(StringUtils.isEmpty(request.getType()))
			throw new AboutstaysException(AboutstaysResponseCode.EMPTY_AIRPORT_TYPE);
	}
}
