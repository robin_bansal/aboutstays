package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;

import com.aboutstays.entities.CheckinEntity;
import com.aboutstays.enums.CheckinRequestStatus;
import com.aboutstays.enums.StayPreferencesType;
import com.aboutstays.pojos.Address;
import com.aboutstays.pojos.CheckinSpecialRequest;
import com.aboutstays.pojos.CompanyInfo;
import com.aboutstays.pojos.EarlyCheckinRequest;
import com.aboutstays.pojos.IdentityDoc;
import com.aboutstays.pojos.PersonalInformation;
import com.aboutstays.pojos.PreferencesType;
import com.aboutstays.pojos.StayPreferences;
import com.aboutstays.request.dto.InitiateCheckinRequest;
import com.aboutstays.request.dto.ModifyCheckinRequest;
import com.aboutstays.response.dto.CheckinResponse;

public class CheckinConverter {

	public static CheckinEntity convertInitiateRequestToEntity(InitiateCheckinRequest request){
		if(request==null)
			return null;
		CheckinEntity response = new CheckinEntity();
		response.setCompanies(request.getCompanies());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getCreated());
		response.setEarlyCheckinRequest(request.getEarlyCheckinRequest());
		response.setHotelId(request.getHotelId());
		response.setIdentityDocuments(request.getIdentityDocuments());
		response.setOfficialTrip(request.isOfficialTrip());
		response.setPersonalInformation(request.getPersonalInformation());
		response.setSpecialRequests(request.getSpecialRequests());
		response.setStaysId(request.getStaysId());
		response.setUserId(request.getUserId());
		response.setStatus(CheckinRequestStatus.USER_INITIATED.getCode());
		response.setEarlyCheckinRequested(request.getEarlyCheckinRequested());

		response.setCheckinFormCompleted(request.getCheckinFormCompleted());
		response.setIdVerified(request.getIdVerified());
		response.setIsEarlyCheckinFee(request.getIsEarlyCheckinFee());
		return response;
	}
	
	public static CheckinEntity convertModifyRequestToEntity(ModifyCheckinRequest request){
		if(request==null)
			return null;
		CheckinEntity response = new CheckinEntity();
		response.setCreated(request.getCreated());
		if(StringUtils.isEmpty(request.getStaysId())) {
			response.setCreated(new Date().getTime());
		}
		response.setCompanies(request.getCompanies());
		response.setUpdated(new Date().getTime());
		response.setEarlyCheckinRequest(request.getEarlyCheckinRequest());
		response.setHotelId(request.getHotelId());
		response.setIdentityDocuments(request.getIdentityDocuments());
		response.setOfficialTrip(request.isOfficialTrip());
		response.setPersonalInformation(request.getPersonalInformation());
		response.setSpecialRequests(request.getSpecialRequests());
		response.setStaysId(request.getStaysId());
		response.setUserId(request.getUserId());
		response.setStatus(CheckinRequestStatus.USER_MODIFIED.getCode());
		response.setEarlyCheckinRequested(request.getEarlyCheckinRequested());
		
		response.setCheckinFormCompleted(request.getCheckinFormCompleted());
		response.setIdVerified(request.getIdVerified());
		response.setIsEarlyCheckinFee(request.getIsEarlyCheckinFee());

		return response;
	}
	
	public static CheckinResponse convertEntityToResponse(CheckinEntity request){
		if(request==null)
			return null;
		CheckinResponse response = new CheckinResponse();
		response.setCompanies(request.getCompanies());
		response.setCreated(request.getCreated());
		response.setUpdated(request.getUpdated());
		response.setEarlyCheckinRequest(request.getEarlyCheckinRequest());
		response.setHotelId(request.getHotelId());
		response.setIdentityDocuments(request.getIdentityDocuments());
		response.setOfficialTrip(request.isOfficialTrip());
		response.setPersonalInformation(request.getPersonalInformation());
		response.setSpecialRequests(request.getSpecialRequests());
		response.setStaysId(request.getStaysId());
		response.setUserId(request.getUserId());
		response.setStatus(request.getStatus());
		response.setEarlyCheckinRequested(request.getEarlyCheckinRequested());
		List<StayPreferences> listStayPreferences = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(request.getListStayPreferences())) {
			for(PreferencesType prefs : request.getListStayPreferences()) {
				if(prefs == null) {
					continue;
				}
				StayPreferencesType type = StayPreferencesType.findStatusByCode(prefs.getPreference());
				if(type == null) {
					continue;
				}
				StayPreferences stayPrefes = new StayPreferences();
				stayPrefes.setName(type.getDisplayName());
				listStayPreferences.add(stayPrefes);
			}
			response.setListStayPreferences(listStayPreferences);
		}
		
		response.setCheckinFormCompleted(request.getCheckinFormCompleted());
		response.setIdVerified(request.getIdVerified());
		response.setIsEarlyCheckinFee(request.getIsEarlyCheckinFee());
		return response;
	}

	public static InitiateCheckinRequest getSampleCheckinRequest() {
		InitiateCheckinRequest request = new InitiateCheckinRequest();
		List<CompanyInfo> companies = new ArrayList<>();
		companies.add(new CompanyInfo("company 1"));
		companies.add(new CompanyInfo("company 2"));
		request.setCompanies(companies);
		request.setEarlyCheckinRequested(true);
		
		EarlyCheckinRequest earlyCheckinRequest = new EarlyCheckinRequest();
		earlyCheckinRequest.setCharges(500.0);
		earlyCheckinRequest.setScheduledCheckinTime("HH:mm");
		earlyCheckinRequest.setRequestedCheckinTime("HH:mm");
		request.setEarlyCheckinRequest(earlyCheckinRequest);
		
		request.setHotelId("hotelId");
		request.setUserId("userId");
		request.setStaysId("staysId");
		
		List<IdentityDoc> identityDocuments = new ArrayList<>();
		identityDocuments.add(new IdentityDoc("Aadhar Card","Yogi Adityanath","DOC_NO_420","docUrl"));
		identityDocuments.add(new IdentityDoc("Driving License","Modi","DOC_NO_419","docUrl"));
		request.setIdentityDocuments(identityDocuments);
		
		request.setOfficialTrip(false);
		PersonalInformation personalInformation = new PersonalInformation();
		personalInformation.setFirstName("firstName");
		personalInformation.setLastName("lastName");
		personalInformation.setAnniversaryDate("dd-MM-yyyy");
		personalInformation.setDob("dd-MM-yyyy");
		personalInformation.setEmailId("emailId");
		personalInformation.setGender("male/female");
		
		Address homeAddress = new Address(); 
		personalInformation.setHomeAddress(homeAddress);
		Address officeAddress = new Address();
		personalInformation.setOfficeAddress(officeAddress);
		personalInformation.setMaritalStatus(123);
		personalInformation.setNumber("number");
		request.setPersonalInformation(personalInformation);
		
		
		List<StayPreferences> listStayPreferences = new ArrayList<>();
		listStayPreferences.add(new StayPreferences("mountain view"));
		listStayPreferences.add(new StayPreferences("beach view"));
		listStayPreferences.add(new StayPreferences("smoking"));
		request.setListStayPreferences(listStayPreferences);
		
		List<CheckinSpecialRequest> specialRequests = new ArrayList<>();
		specialRequests.add(new CheckinSpecialRequest("I need bathtub in my room"));
		request.setSpecialRequests(specialRequests);
		
		return request;
	}
}
