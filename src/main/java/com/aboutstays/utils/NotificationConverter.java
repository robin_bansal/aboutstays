package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Notification;
import com.aboutstays.request.dto.AddNotificationRequest;
import com.aboutstays.response.dto.GetNotificationResponse;

public class NotificationConverter {

	public static Notification convertAddRequestToEntity(AddNotificationRequest request){
		if(request==null){
			return null;
		}
		Notification response = new Notification();
		response.setTitle(request.getTitle());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setUpdated(currentTime);
		response.setDescription(request.getDescription());
		response.setHotelId(request.getHotelId());
		response.setIconUrl(request.getIconUrl());
		response.setMaxUiLines(request.getMaxUiLines());
		response.setStaysId(request.getStaysId());
		response.setType(request.getType().toUpperCase());
		response.setUserId(request.getUserId());
		return response;
	}
	
	public static GetNotificationResponse convertEntityToGetRequest(Notification request){
		if(request==null){
			return null;
		}
		GetNotificationResponse response = new GetNotificationResponse();
		response.setNotificationId(request.getId());
		response.setTitle(request.getTitle());
		response.setCreated(request.getCreated());
		response.setDescription(request.getDescription());
		response.setHotelId(request.getHotelId());
		response.setIconUrl(request.getIconUrl());
		response.setMaxUiLines(request.getMaxUiLines());
		response.setStaysId(request.getStaysId());
		response.setType(request.getType());
		response.setUserId(request.getUserId());
		return response;
	}
	
	public static List<GetNotificationResponse> convertEntityListToGetList(List<Notification> requestList){
		if(CollectionUtils.isEmpty(requestList)){
			return null;
		}
		List<GetNotificationResponse> responseList = new ArrayList<>(requestList.size());
		for(Notification notification : requestList){
			responseList.add(convertEntityToGetRequest(notification));
		}
		return responseList;
	}

	public static AddNotificationRequest getSampleRequest() {
		AddNotificationRequest request = new AddNotificationRequest();
		request.setDescription("description");
		request.setHotelId("hotelId");
		request.setUserId("userId");
		request.setStaysId("staysId");
		request.setIconUrl("iconUrl"); 
		request.setMaxUiLines(3);
		request.setTitle("title");
		request.setType("type");
		return request;
	}
}
