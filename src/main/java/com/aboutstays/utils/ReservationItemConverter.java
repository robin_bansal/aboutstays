package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.ReservationItem;
import com.aboutstays.request.dto.AddReservationItemRequest;
import com.aboutstays.request.dto.UpdateReservationItemRequest;
import com.aboutstays.response.dto.ReservationItemResponse;

public class ReservationItemConverter {
	
	public static AddReservationItemRequest getSampleRequest(){
		AddReservationItemRequest request = new AddReservationItemRequest();
		
		List<String> tempList = new ArrayList<>();
		tempList.add("image1");
		tempList.add("image2");
		tempList.add("image3");
		request.setAdditionalImageUrls(tempList);
		tempList.clear();
		tempList.add("mostPopular1");
		tempList.add("most Popular 2");
		tempList.add("most Popular 3");
		request.setMostPopularList(tempList);
		request.setSubCategory("subCategory");
		request.setStartTime("hh:mm a");
		request.setEndTime("hh:mm a");
		request.setEntryCriteria("entryCriteria");
		request.setHotelId("hotelId");
		request.setImageUrl("imageUrl");
		request.setDescripton("Descripiton");
		request.setName("name");
		request.setPrice(20.0);
		request.setDuration(12354l);
		tempList.clear();
		tempList.add("Offering 1");
		tempList.add("Offering 2");
		tempList.add("Offering 3");
		tempList.add("Offering 4");
		request.setOfferings(tempList);
		request.setAmountApplicable(true);
		request.setCategoryId("categoryId");
		request.setDuration(60*60*1000);
		request.setHotelId("hotelId");
		request.setImageUrl("imageUrl");
		return request;
	}

	public static ReservationItem convertBeanToEntity(AddReservationItemRequest request) {
		if(request != null){
			ReservationItem item = new ReservationItem();
			item.setAdditionalImageUrls(request.getAdditionalImageUrls());
			item.setCreated(new Date().getTime());
			item.setEntryCriteria(request.getEntryCriteria());
			item.setHotelId(request.getHotelId());
			item.setImageUrl(request.getImageUrl());
			item.setName(request.getName());
			item.setUpdated(item.getCreated());
			item.setPrice(request.getPrice());
			item.setDuration(request.getDuration());
			item.setMostPopularList(request.getMostPopularList());
			item.setOfferings(request.getOfferings());
			item.setAmountApplicable(request.isAmountApplicable());
			item.setCategoryId(request.getCategoryId());
			item.setDescripton(request.getDescripton());
			item.setDurationBased(request.isDurationBased());
			item.setEndTime(request.getEndTime());
			item.setStartTime(request.getStartTime());
			item.setSubCategory(request.getSubCategory());
			return item;
		}
		return null;
	}

	public static ReservationItemResponse convertEntityToBean(ReservationItem item) {
		if(item != null){
			ReservationItemResponse response = new ReservationItemResponse();
			response.setAdditionalImageUrls(item.getAdditionalImageUrls());
			response.setEntryCriteria(item.getEntryCriteria());
			response.setImageUrl(item.getImageUrl());
			response.setName(item.getName());
			response.setDurationBased(item.isDurationBased());
			if(item.isDurationBased())
				response.setDuration(item.getDuration());
			response.setMostPopularList(item.getMostPopularList());
			response.setAmountApplicable(item.isAmountApplicable());
			if(item.isAmountApplicable())
				response.setPrice(item.getPrice());
			response.setOfferings(item.getOfferings());
			response.setDescripton(item.getDescripton());
			response.setEndTime(item.getEndTime());
			response.setStartTime(item.getStartTime());
			response.setSubCategory(item.getSubCategory());
			response.setId(item.getId());
			response.setCategoryId(item.getCategoryId());
			response.setHotelId(item.getHotelId());
			return response;
		}
		return null;
	}

	public static List<ReservationItemResponse> convertListEntityToListBean(List<ReservationItem> listEntity) {
		if(CollectionUtils.isNotEmpty(listEntity)){
			List<ReservationItemResponse> listResponse = new ArrayList<>();
			for(ReservationItem item : listEntity){
				if(item == null){
					continue;
				}
				listResponse.add(convertEntityToBean(item));
			}
			return listResponse;
		}
		return null;
	}

	public static ReservationItem convertUpdateEntityToBean(UpdateReservationItemRequest request) {
		if(request != null){
			ReservationItem item = new ReservationItem();
			item.setId(request.getId());
			item.setAdditionalImageUrls(request.getAdditionalImageUrls());
			item.setCreated(new Date().getTime());
			item.setEntryCriteria(request.getEntryCriteria());
			item.setHotelId(request.getHotelId());
			item.setImageUrl(request.getImageUrl());
			item.setName(request.getName());
			item.setUpdated(item.getCreated());
			item.setPrice(request.getPrice());
			item.setDuration(request.getDuration());
			item.setMostPopularList(request.getMostPopularList());
			item.setOfferings(request.getOfferings());
			item.setAmountApplicable(request.isAmountApplicable());
			item.setCategoryId(request.getCategoryId());
			item.setDescripton(request.getDescripton());
			item.setDurationBased(request.isDurationBased());
			item.setEndTime(request.getEndTime());
			item.setStartTime(request.getStartTime());
			item.setSubCategory(request.getSubCategory());
			return item;
		}
		return null;
	}
	

}
