package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.aboutstays.entities.RoomCategory;
import com.aboutstays.request.dto.AddRoomCategoryRequest;
import com.aboutstays.request.dto.UpdateRoomCategoryRequest;
import com.aboutstays.response.dto.GetEarlyCheckinRoomResponse;
import com.aboutstays.response.dto.GetLateCheckoutRoomResponse;
import com.aboutstays.response.dto.GetRoomCategoryResponse;

public class RoomCategoryConverter {

	public static RoomCategory convertAddRoomRequestToEntity(AddRoomCategoryRequest request){
		if(request==null){
			return null;
		}
		RoomCategory room = new RoomCategory();
		room.setName(request.getCategoryName());
		room.setCode(request.getCategoryCode());
		room.setCount(request.getCount());
		room.setAdditionalImageUrls(request.getAdditionalImageUrls());
		room.setImageUrl(request.getImageUrl());
		room.setDescription(request.getDescription());
		
		room.setSize(request.getSize());
		room.setSizeMetric(request.getSizeMetric());
		
		room.setAdultCapacity(request.getAdultCapacity());
		room.setChildCapacity(request.getChildCapacity());
		
		room.setEarlyCheckinInfo(request.getEarlyCheckinInfo());
		room.setLateCheckoutInfo(request.getLateCheckoutInfo());
		room.setAdditionalImageUrls(request.getAdditionalImageUrls());
		
		room.setHotelId(request.getHotelId());
		
		return room;
	}

	public static GetRoomCategoryResponse convertEntityToGetResponse(RoomCategory room) {
		if(room==null)
			return null;
		GetRoomCategoryResponse response = new GetRoomCategoryResponse();
		response.setRoomCategoryId(room.getRoomCategegoryId());
		response.setCategoryName(room.getName());
		response.setCategoryCode(room.getCode());
		response.setCount(room.getCount());
		response.setDescription(room.getDescription());
		response.setImageUrl(room.getImageUrl());
		
		response.setSize(room.getSize());
		response.setSizeMetric(room.getSizeMetric());
		
		response.setChildCapacity(room.getChildCapacity());
		response.setAdultCapacity(room.getAdultCapacity());
		
		response.setEarlyCheckinInfo(room.getEarlyCheckinInfo());
		response.setLateCheckoutInfo(room.getLateCheckoutInfo());
		
		response.setAdditionalImageUrls(room.getAdditionalImageUrls());
		
		response.setHotelId(room.getHotelId());
		
		return response;
	}

	public static List<GetRoomCategoryResponse> convertEntityListToGetResponseList(List<RoomCategory> roomList) {
		if(CollectionUtils.isEmpty(roomList))
			return null;
		List<GetRoomCategoryResponse> responseList = new ArrayList<>(roomList.size());
		for(RoomCategory room : roomList){
			responseList.add(convertEntityToGetResponse(room));
		}
		return responseList;
	}

	public static RoomCategory convertUpdateRequestToEntity(UpdateRoomCategoryRequest request) {
		if(request==null)
			return null;
		RoomCategory room = new RoomCategory();
		room.setRoomCategegoryId(request.getRoomCategoryId());
		room.setName(request.getCategoryName());
		room.setCode(request.getCategoryCode());
		room.setCount(request.getCount());
		room.setImageUrl(request.getImageUrl());
		room.setDescription(request.getDescription());
		
		room.setSize(request.getSize());
		room.setSizeMetric(request.getSizeMetric());
		
		room.setAdultCapacity(request.getAdultCapacity());
		room.setChildCapacity(request.getChildCapacity());

		room.setEarlyCheckinInfo(request.getEarlyCheckinInfo());
		room.setLateCheckoutInfo(request.getLateCheckoutInfo());
		
		room.setInRoomAmenities(request.getInRoomAmenities());
		room.setPreferenceBasedAmenities(request.getPreferenceBasedAmenities());
		
		room.setAdditionalImageUrls(request.getAdditionalImageUrls());
		
		room.setHotelId(request.getHotelId());
		
		return room;
	}

	public static AddRoomCategoryRequest generateSampleRequest() {
		AddRoomCategoryRequest request = new AddRoomCategoryRequest();
		request.setHotelId("hotelId");
		request.setCategoryName("Room category name");
		request.setCategoryCode("room category code-100");
		request.setCount(100);
		request.setDescription("Room Description");
		request.setImageUrl("imageUrl");
		
		
		request.setSize("Room Size");
		request.setSizeMetric("700*300");
		
		request.setChildCapacity(1);
		request.setAdultCapacity(2);
		
		List<String> inRoomAmenities = new ArrayList<>();
		inRoomAmenities.add("in room amenity id1");
		inRoomAmenities.add("in room amenity id2");
		inRoomAmenities.add("in room amenity id3");
		inRoomAmenities.add("in room amenity id4");
		request.setInRoomAmenities(inRoomAmenities);
		
		List<String> preferenceBasedAmenities=new ArrayList<>();
		preferenceBasedAmenities.add("prefrence based Aminity id1");
		preferenceBasedAmenities.add("prefrence based Aminity id2");
		preferenceBasedAmenities.add("prefrence based Aminity id3");
		preferenceBasedAmenities.add("prefrence based Aminity id4");
		request.setPreferenceBasedAmenities(preferenceBasedAmenities);
		
		List<String> imageUrlList = new ArrayList<>();
		imageUrlList.add("additional image 1");
		imageUrlList.add("additional image 2");
		imageUrlList.add("additional image 3");
		request.setAdditionalImageUrls(imageUrlList);
		return request;
	}

	public static GetEarlyCheckinRoomResponse convertEarlyCheckInInfo(RoomCategory room) {
		if(room != null){
			GetEarlyCheckinRoomResponse response = new GetEarlyCheckinRoomResponse();
			response.setRoomCategoryId(room.getRoomCategegoryId());
			response.setCheckInInfo(room.getEarlyCheckinInfo());
			return response;
		}
		return null;
	}

	public static GetLateCheckoutRoomResponse convertLateCheckoutInfo(RoomCategory room) {
		if(room != null){
			GetLateCheckoutRoomResponse response = new GetLateCheckoutRoomResponse();
			response.setCheckOutInfo(room.getLateCheckoutInfo());
			response.setRoomCategoryId(room.getRoomCategegoryId());
			return response;
		}
		return null;
	}
}
