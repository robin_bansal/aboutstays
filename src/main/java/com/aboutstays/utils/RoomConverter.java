package com.aboutstays.utils;

import com.aboutstays.entities.Room;
import com.aboutstays.request.dto.AddRoomRequest;
import com.aboutstays.request.dto.UpdateRoomRequest;
import com.aboutstays.response.dto.GetRoomResponse;

public class RoomConverter {

	public static AddRoomRequest generateSampleRequest() {
		AddRoomRequest addRoomRequest=new AddRoomRequest();
		addRoomRequest.setRoomName("Room name");
		addRoomRequest.setRoomNumber("A-100");
		addRoomRequest.setRoomCategoryId("roomCategoryId");
		addRoomRequest.setFloorId("floorId");
		addRoomRequest.setWingId("wingId");
		addRoomRequest.setHotelId("hotelId");
		return addRoomRequest;
	}
	public static Room getEntityFromAddRequest(AddRoomRequest request) {
		if(request==null){
			return null;
		}
		long currentTime = System.currentTimeMillis();
		Room room=new Room();
		room.setCreated(currentTime);
		room.setUpdated(currentTime);
		room.setName(request.getRoomName());
		room.setRoomNumber(request.getRoomNumber());
		room.setRoomCategoryId(request.getRoomCategoryId());
		room.setFloorId(request.getFloorId());
		room.setWingId(request.getWingId());
		room.setHotelId(request.getHotelId());
		if(request.getPanelUiOrder()==null){
			room.setPanelUiOrder(999);
		} else {
			room.setPanelUiOrder(request.getPanelUiOrder());
		}
		return room;
	}
	public static GetRoomResponse getResponseFromEntity(Room room) {
		GetRoomResponse getRoomResponse=new GetRoomResponse();
		getRoomResponse.setRoomId(room.getId());
		getRoomResponse.setRoomName(room.getName());
		getRoomResponse.setRoomNumber(room.getRoomNumber());
		getRoomResponse.setFloorId(room.getFloorId());
		getRoomResponse.setWingId(room.getWingId());
		getRoomResponse.setHotelId(room.getHotelId());
		getRoomResponse.setPanelUiOrder(room.getPanelUiOrder());
		return getRoomResponse;
	}
	public static Room getEntityFromUpdateRequest(UpdateRoomRequest request) {
		if(request==null){
			return null;
		}
		long currentTime = System.currentTimeMillis();
		Room room=new Room();
		room.setId(request.getRoomId());
		room.setUpdated(currentTime);
		room.setName(request.getRoomName());
		room.setRoomNumber(request.getRoomNumber());
		room.setRoomCategoryId(request.getRoomCategoryId());
		room.setFloorId(request.getFloorId());
		room.setWingId(request.getWingId());
		room.setHotelId(request.getHotelId());
		room.setPanelUiOrder(request.getPanelUiOrder());
		return room;
	}

}
