package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;


import com.aboutstays.entities.LoyaltyLevel;
import com.aboutstays.entities.LoyaltyPrograms;
import com.aboutstays.request.dto.AddLoyaltyRequest;
import com.aboutstays.response.dto.GetLoyaltyProgramResponse;
import com.aboutstays.response.dto.UpdateLoyaltyProgram;

public class LoyaltyProgramsConverter {
	public static GetLoyaltyProgramResponse getSampleRequest() {
		GetLoyaltyProgramResponse getLoyaltyProgramResponse=new GetLoyaltyProgramResponse();
		getLoyaltyProgramResponse.setId("id");
		getLoyaltyProgramResponse.setName("Loyalty program");
		getLoyaltyProgramResponse.setIconUrl("icon url");
		List<LoyaltyLevel> levels=new ArrayList<>();
		levels.add(new LoyaltyLevel("name1","code1","color code1","eligibity1","benifits1"));
		levels.add(new LoyaltyLevel("name2","code2","color code2","eligibity2","benifits2"));
		getLoyaltyProgramResponse.setLevels(levels);
		return getLoyaltyProgramResponse;
	}

	public static LoyaltyPrograms conterBeanToEntity(AddLoyaltyRequest request) {
		if(request==null){
			return null;
		}
		LoyaltyPrograms loyalyPrograms=new LoyaltyPrograms();
		loyalyPrograms.setName(request.getName());
		loyalyPrograms.setIconUrl(request.getIconUrl());
		loyalyPrograms.setLevels(request.getLevels());
		return loyalyPrograms;
	}
	public static LoyaltyPrograms conterBeanToEntity(UpdateLoyaltyProgram request) {
		if(request==null){
			return null;
		}
		LoyaltyPrograms loyalyPrograms=new LoyaltyPrograms();
		loyalyPrograms.setId(request.getId());
		loyalyPrograms.setName(request.getName());
		loyalyPrograms.setIconUrl(request.getIconUrl());
		loyalyPrograms.setLevels(request.getLevels());
		return loyalyPrograms;
	}
	public static List<GetLoyaltyProgramResponse> conterEntityListToBeanList(List<LoyaltyPrograms> list) {
		if(list==null){
			return null;
		}

		List<GetLoyaltyProgramResponse> programResponses=new ArrayList<>();
		for(LoyaltyPrograms loyalyPrograms:list){
			programResponses.add(convertEntityToBean(loyalyPrograms));
		}
		return programResponses;
	}
	public static GetLoyaltyProgramResponse convertEntityToBean(LoyaltyPrograms loyalyPrograms) {
		if(loyalyPrograms==null){
			return null;
		}
		GetLoyaltyProgramResponse getLoyaltyProgramResponse=new GetLoyaltyProgramResponse();
		getLoyaltyProgramResponse.setId(loyalyPrograms.getId());
		getLoyaltyProgramResponse.setName(loyalyPrograms.getName());
		getLoyaltyProgramResponse.setIconUrl(loyalyPrograms.getIconUrl());
		getLoyaltyProgramResponse.setLevels(loyalyPrograms.getLevels());
		return getLoyaltyProgramResponse;
	}
	
}
