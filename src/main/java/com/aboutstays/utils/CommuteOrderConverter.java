package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.controller.CommuteOrder;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.pojos.CarDetails;
import com.aboutstays.request.dto.AddCommuteOrderRequest;
import com.aboutstays.request.dto.UpdateCommuteOrderRequest;
import com.aboutstays.response.dto.GetCommuteOrderResponse;

public class CommuteOrderConverter extends BaseSessionConverter{

	
	public static AddCommuteOrderRequest getSampleRequest(){
		AddCommuteOrderRequest request = new AddCommuteOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(234L);
		setRequestData(request);
		request.setPrice(2132);
		request.setPackageName("packageName");
		CarDetails carDetails = new CarDetails(1234, "imageUrl1", 3, 60.0, 3.5, 9.5, "INR");
		request.setCarDetails(carDetails);
		return request;
	}

	public static CommuteOrder convertBeanToEntity(AddCommuteOrderRequest request) {
		if(request != null){
			CommuteOrder order = new CommuteOrder();
			order.setCarDetails(request.getCarDetails());
			order.setComments(request.getComment());
			order.setCreated(new Date().getTime());
			order.setPackageName(request.getPackageName());
			order.setPrice(request.getPrice());
			order.setUpdated(new Date().getTime());
			if(request.isRequestedNow()){
				order.setStatus(GeneralOrderStatus.PROCESSING.getCode());
				order.setDeliveryTime(order.getCreated());
			} else {
				order.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
				order.setDeliveryTime(request.getDeliveryTime());
			}
			setPojoToEntity(order, request);
			return order;
		}
		return null;
	}

	public static GetCommuteOrderResponse convertEntityToBean(CommuteOrder order) {
		if(order != null){
			GetCommuteOrderResponse response = new GetCommuteOrderResponse();
			response.setCarDetails(order.getCarDetails());
			response.setComment(order.getComments());
			response.setDeliveryTime(order.getDeliveryTime());
			response.setId(order.getId());
			response.setPackageName(order.getPackageName());
			response.setPrice(order.getPrice());
			response.setStatus(order.getStatus());
			setEntityToPojo(response, order);
			return response;
		}
		return null;
	}

	public static List<GetCommuteOrderResponse> convertListEntityToListBean(List<CommuteOrder> listOrder) {
		if(CollectionUtils.isNotEmpty(listOrder)){
			List<GetCommuteOrderResponse> listResponse = new ArrayList<>();
			for(CommuteOrder order : listOrder){
				if(order == null){
					continue;
				}
				listResponse.add(convertEntityToBean(order));
			}
			return listResponse;
		}
		return null;
	}

	public static CommuteOrder convertUpdateBeanToEntity(UpdateCommuteOrderRequest request) {
		if(request != null){
			CommuteOrder order = new CommuteOrder();
			order.setCarDetails(request.getCarDetails());
			order.setComments(request.getComment());
			order.setCreated(new Date().getTime());
			order.setPackageName(request.getPackageName());
			order.setPrice(request.getPrice());
			order.setUpdated(new Date().getTime());
			order.setId(request.getId());
			order.setStatus(GeneralOrderStatus.UPDATED.getCode());
			setPojoToEntity(order, request);
			return order;
		}
		return null;
	}
	
	
}
