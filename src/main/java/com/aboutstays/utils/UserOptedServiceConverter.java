package com.aboutstays.utils;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.UserOptedService;
import com.aboutstays.enums.RefCollectionType;
import com.aboutstays.request.dto.AddAirportPickupRequest;
import com.aboutstays.request.dto.AddBellBoyOrderRequest;
import com.aboutstays.request.dto.AddCommuteOrderRequest;
import com.aboutstays.request.dto.AddFoodOrderRequest;
import com.aboutstays.request.dto.AddHKOrderRequest;
import com.aboutstays.request.dto.AddInternetOrderRequest;
import com.aboutstays.request.dto.AddLateCheckoutRequest;
import com.aboutstays.request.dto.AddLaundryOrderRequest;
import com.aboutstays.request.dto.AddMaintenanceOrderRequest;
import com.aboutstays.request.dto.AddReservationOrderRequest;
import com.aboutstays.request.dto.AddStaysFeedbackRequest;
import com.aboutstays.request.dto.AddUserOptedServiceRequest;
import com.aboutstays.request.dto.ModifyCheckinRequest;
import com.aboutstays.request.dto.UpdateLateCheckoutRequest;
import com.aboutstays.request.dto.UpdateUserOptedServiceRequest;
import com.aboutstays.response.dto.AddAirportPickupResponse;
import com.aboutstays.response.dto.CheckinResponse;
import com.aboutstays.response.dto.GetUserOptedServiceResponse;

public class UserOptedServiceConverter extends BaseSessionConverter{

	public static AddUserOptedServiceRequest getSampleRequest(){
		AddUserOptedServiceRequest request = new AddUserOptedServiceRequest();
		setRequestData(request);
		request.setRefId("refId");
		request.setRefCollectionType(1234);
		return request;
	}
	
	public static UserOptedService convertBeanToEntity(AddUserOptedServiceRequest request){
		if(request != null){
			UserOptedService service = new UserOptedService();
			service.setRefCollectionType(request.getRefCollectionType());
			service.setRefId(request.getRefId());
			service.setSfId(request.getSfId());
			setPojoToEntity(service, request);
			return service;
		}
		return null;
	}
	
	public static GetUserOptedServiceResponse convertEntityToBean(UserOptedService service){
		if(service != null){
			GetUserOptedServiceResponse response = new GetUserOptedServiceResponse();
			response.setRefCollectionType(service.getRefCollectionType());
			response.setRefId(service.getRefId());
			response.setId(service.getId());
			response.setSfId(service.getSfId());
			setEntityToPojo(response, service);
			return response;
		}
		return null;
	}
	
	public static List<GetUserOptedServiceResponse> convertListEntityToListBean(List<UserOptedService> listService){
		if(CollectionUtils.isNotEmpty(listService)){
			List<GetUserOptedServiceResponse> listResponse = new ArrayList<>();
			for(UserOptedService service : listService){
				listResponse.add(convertEntityToBean(service));
			}
			return listResponse;
		}
		return null;
	}
	
	public static UserOptedService convertUpdateRequestToEntity(UpdateUserOptedServiceRequest request){
		if(request != null){
			UserOptedService service = new UserOptedService();
			service.setRefCollectionType(request.getRefCollectionType());
			service.setId(request.getId());
			service.setUpdated(new Date().getTime());
			service.setRefId(request.getRefId());
			service.setSfId(request.getSfId());
			setPojoToEntity(service, request);
			return service;
		}
		return null;
	}
	
	public static AddUserOptedServiceRequest createAddUserOptedServiceRequest(AddAirportPickupRequest request, AddAirportPickupResponse response){
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			optedRequest.setHotelId(request.getHotelId());
			optedRequest.setUserId(request.getUserId());
			optedRequest.setStaysId(request.getStaysId());
			optedRequest.setRefId(response.getId());
			optedRequest.setRefCollectionType(RefCollectionType.AIRPORT_PICKUP.getCode());
			setPojoToPojo(optedRequest, request);
			return optedRequest;
		}
		return null;	
	}
	

	public static AddUserOptedServiceRequest createUOSRequestByFoodOrderRequest(AddFoodOrderRequest request, String foodOrderId){
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefId(foodOrderId);
			optedRequest.setRefCollectionType(RefCollectionType.FOOD.getCode());
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSRequestByLaundryOrderRequest(AddLaundryOrderRequest request, String laundryOrderId){
			if(request != null){
				AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
				setPojoToPojo(optedRequest, request);
				optedRequest.setRefId(laundryOrderId);
				optedRequest.setRefCollectionType(RefCollectionType.LAUNDRY.getCode());
				return optedRequest;
			}
			return null;
     }

	public static AddUserOptedServiceRequest createUOSRequestByHKOrderRequest(AddHKOrderRequest request, String hkOrderId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefId(hkOrderId);
			optedRequest.setRefCollectionType(RefCollectionType.HOUSEKEEPING.getCode());
			return optedRequest;
		}		
		return null;
	}

	public static AddUserOptedServiceRequest createUOSRequestByBellBoyOrder(AddBellBoyOrderRequest request, String bellboyOrderId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.BELL_BOY.getCode());
			optedRequest.setRefId(bellboyOrderId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSRequestByMTOrderRequest(AddMaintenanceOrderRequest request, String maintenanceOrderId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.MAINTENANCE.getCode());
			optedRequest.setRefId(maintenanceOrderId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSReqByCommuteOrder(AddCommuteOrderRequest request, String coId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.COMMUTE.getCode());
			optedRequest.setRefId(coId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSReqByRSOrder(AddReservationOrderRequest request, String rsOrderId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.RESERVATION.getCode());
			optedRequest.setRefId(rsOrderId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSReqByInternetOrder(AddInternetOrderRequest request,String internetOrderId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.INTERNET.getCode());
			optedRequest.setRefId(internetOrderId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSReqByLateCheckoutRequest(AddLateCheckoutRequest request,String lcrId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.LATE_CHECKOUT.getCode());
			optedRequest.setRefId(lcrId);
			return optedRequest;
		}
		return null;
	}
	
	public static AddUserOptedServiceRequest createUOSReqByModifyLateCheckoutRequest(UpdateLateCheckoutRequest request,String lcrId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.LATE_CHECKOUT.getCode());
			optedRequest.setRefId(lcrId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSRequestByStaysFeedback(AddStaysFeedbackRequest request,String feedbackId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setRefCollectionType(RefCollectionType.STAYS_FEEDBACK.getCode());
			optedRequest.setRefId(feedbackId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSRequestEarlyCheckinOrder(ModifyCheckinRequest request, String checkineId, String gsId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setGsId(gsId);
			optedRequest.setRefCollectionType(RefCollectionType.EARLY_CHECK_IN.getCode());
			optedRequest.setRefId(checkineId);
			return optedRequest;
		}
		return null;
	}

	public static AddUserOptedServiceRequest createUOSRequestEarlyCheckinOrder(CheckinResponse request, String gsId) {
		if(request != null){
			AddUserOptedServiceRequest optedRequest = new AddUserOptedServiceRequest();
			setPojoToPojo(optedRequest, request);
			optedRequest.setGsId(gsId);
			optedRequest.setRefCollectionType(RefCollectionType.EARLY_CHECK_IN.getCode());
			optedRequest.setRefId(request.getStaysId());
			return optedRequest;
		}
		return null;
	}


}
