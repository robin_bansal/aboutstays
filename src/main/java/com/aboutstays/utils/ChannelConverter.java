package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.MasterChannelItem;
import com.aboutstays.request.dto.AddMasterChannelRequest;
import com.aboutstays.request.dto.AddMultipleMasterChannelRequest;
import com.aboutstays.request.dto.UpdateMasterChannelRequest;
import com.aboutstays.response.dto.GetMasterChannelResponse;
import com.aboutstays.response.dto.GetMasterGenereResponse;

public class ChannelConverter {

	public static MasterChannelItem convertAddRequestToEntity(AddMasterChannelRequest request){
		if(request==null)
			return null;
		MasterChannelItem response = new MasterChannelItem();
		response.setGenereId(request.getGenereId());
		response.setImageUrl(request.getImageUrl());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setUpdated(currentTime);
		response.setDescription(request.getDescription());
		return response;
	}
	
	public static MasterChannelItem convertUpdateRequestToEntity(UpdateMasterChannelRequest request){
		if(request==null)
			return null;
		MasterChannelItem response = new MasterChannelItem();
		response.setId(request.getChannelId());
		response.setGenereId(request.getGenereId());
		response.setImageUrl(request.getImageUrl());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		long currentTime = new Date().getTime();
		response.setUpdated(currentTime);
		response.setDescription(request.getDescription());
		return response;
	}
	
	public static GetMasterChannelResponse convertEntityToGetResponse(MasterChannelItem request, GetMasterGenereResponse genereDetails){
		if(request==null)
			return null;
		GetMasterChannelResponse response = new GetMasterChannelResponse();
		response.setChannelId(request.getId());
		response.setImageUrl(request.getImageUrl());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		response.setGenereDetails(genereDetails);
		response.setDescription(request.getDescription());
		return response;
	}
	
	public static List<MasterChannelItem> convertAddListToEntityList(AddMultipleMasterChannelRequest request){
		if(request==null||CollectionUtils.isEmpty(request.getChannels())){
			return null;
		}
		List<MasterChannelItem> responseList = new ArrayList<>(request.getChannels().size());
		for(AddMasterChannelRequest addMasterChannelRequest : request.getChannels()){
			responseList.add(convertAddRequestToEntity(addMasterChannelRequest));
		}
		return responseList;
	}

	public static AddMasterChannelRequest getSampleRequest() {
		AddMasterChannelRequest request = new AddMasterChannelRequest();
		request.setGenereId("genereId");
		request.setImageUrl("channel imageUrl");
		request.setLogo("channel logo");
		request.setName("name");
		request.setDescription("description");
		return request;
	}
	
}
