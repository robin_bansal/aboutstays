package com.aboutstays.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aboutstays.entities.Booking;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.BookingStatus;
import com.aboutstays.enums.GetStayResponse;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.mao.BookingMao;
import com.aboutstays.pojos.BookedFor;
import com.aboutstays.pojos.UserPojo;
import com.aboutstays.request.dto.AllocateRoomRequest;
import com.aboutstays.request.dto.CreateBookingRequest;
import com.aboutstays.request.dto.GetBookingsRequest;
import com.aboutstays.request.dto.UpdateBookingRequest;
import com.aboutstays.response.dto.CreateBookingResponse;
import com.aboutstays.response.dto.GetBookingReponse;
import com.aboutstays.response.dto.GetIncompleteBookingsResponce;
import com.aboutstays.services.BookingService;
import com.aboutstays.services.CheckinService;
import com.aboutstays.services.RoomCategoryService;
import com.aboutstays.services.RoomService;
import com.aboutstays.services.StaysService;
import com.aboutstays.services.UserService;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	private BookingMao bookingMao;

	@Autowired
	private RoomCategoryService roomCategoryService;

	@Autowired
	private RoomService roomService;

	@Autowired
	private StaysService staysService;

	@Autowired
	private CheckinService checkinService;
	
	@Autowired
	private UserService userService;

	@Override
	public boolean existsByReservationNumber(String reservationNumber) {
		Booking booking = bookingMao.getByReservationNumber(reservationNumber);
		if (booking == null) {
			return false;
		}
		return true;
	}

	@Override
	public boolean existsByBookingId(String bookingId) {
		Booking booking = bookingMao.getByBookingId(bookingId);
		if (booking == null) {
			return false;
		}
		return true;
	}

	@Override
	public CreateBookingResponse createBooking(CreateBookingRequest createBookingRequest) throws AboutstaysException {
		Booking booking = BookingConverter.convertCreateBookngRequestToEntity(createBookingRequest);
		if (booking != null) {
			booking.setBookedStatus(BookingStatus.UN_ALLOCATED.getStatus());
			bookingMao.createBooking(booking);
			String staysId = staysService.addStayForBooking(booking);
			bookingMao.setDefaultStaysId(booking.getBookingId(), staysId);
			return BookingConverter.getCreateBookingResponse(booking);
		}
		return null;
	}

	@Override
	public List<GetBookingReponse> getBookings(GetBookingsRequest getBookingsRequest) throws AboutstaysException {
		List<Booking> listBookings = null;
		if (!StringUtils.isEmpty(getBookingsRequest.getReservationNumber())) {
			listBookings = bookingMao.getBookingsByReservationNumber(getBookingsRequest.getReservationNumber(),
					getBookingsRequest.getHotelId());
			if (!StringUtils.isEmpty(getBookingsRequest.getCheckInDate())
					&& !StringUtils.isEmpty(getBookingsRequest.getName())) {
				try {
					Date bookingDate = DateUtil.parseDate(getBookingsRequest.getCheckInDate());
					Iterator<Booking> iterator = listBookings.iterator();
					while (iterator.hasNext()) {
						Booking booking = iterator.next();
						if (!booking.getCheckInDate().equals(bookingDate) || booking.getBookedFor() == null
								|| !booking.getBookedFor().getName().toLowerCase()
										.contains(getBookingsRequest.getName().toLowerCase())) {
							iterator.remove();
						}
					}
				} catch (Exception e) {
					throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
				}
			} else if (!StringUtils.isEmpty(getBookingsRequest.getCheckInDate())) {
				try {
					Date bookingDate = DateUtil.parseDate(getBookingsRequest.getCheckInDate());
					Iterator<Booking> iterator = listBookings.iterator();
					while (iterator.hasNext()) {
						Booking booking = iterator.next();
						boolean equalDates = DateUtil.isDatePartEqual(booking.getCheckInDate(), bookingDate);
						if (!equalDates) {
							iterator.remove();
						}
					}
				} catch (Exception e) {
					throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
				}

			} else if (!StringUtils.isEmpty(getBookingsRequest.getName())) {
				Iterator<Booking> iterator = listBookings.iterator();
				while (iterator.hasNext()) {
					Booking booking = iterator.next();
					if (booking.getBookedFor() == null || !booking.getBookedFor().getName().toLowerCase()
							.contains(getBookingsRequest.getName().toLowerCase())) {
						iterator.remove();
					}
				}
			}
		}

		if (CollectionUtils.isNotEmpty(listBookings)) {
			List<GetBookingReponse> listBookingPojo = new ArrayList<>();
			for (Booking booking : listBookings) {
				GetBookingReponse response = BookingConverter.convertEntityToBean(booking);
				response.setRoomData(roomService.getById(booking.getBookingId()));
				response.setRoomCategoryData(roomCategoryService.getById(booking.getRoomId()));
				boolean staysExists = staysService.existsByUserIdAndBookingId(getBookingsRequest.getUserId(),
						booking.getBookingId());
				response.setAddedToStayline(staysExists);
				listBookingPojo.add(response);
			}
			return listBookingPojo;
		}
		return null;
	}

	@Override
	public boolean deleteByBookingId(String bookingId) {
		if (StringUtils.isEmpty(bookingId)) {
			return false;
		}
		bookingMao.deleteByBookingId(bookingId);
		return true;
	}

	@Override
	public boolean deleteByReservationNumber(String reservationNumber) {
		if (StringUtils.isEmpty(reservationNumber)) {
			return false;
		}
		bookingMao.deleteByReservationNumber(reservationNumber);
		return true;
	}

	@Override
	public boolean updateBookingByBookingId(UpdateBookingRequest request) throws AboutstaysException {
		if (request == null) {
			return false;
		}
		Booking booking = BookingConverter.convertUpdateRequestToEntity(request);
		if (booking == null) {
			return false;
		}
		bookingMao.updateByBookingId(booking);
		return true;
	}

	@Override
	public List<GetBookingReponse> getBookingsByReservationNumber(String reservationNumber, String hotelId) {
		List<Booking> listBookings = bookingMao.getBookingsByReservationNumber(reservationNumber, hotelId);
		return BookingConverter.convertListEntityToListBean(listBookings);
	}

	@Override
	public List<GetIncompleteBookingsResponce> getUnCompleteBookings(String hotelId, String bookingDate) {
		try {
			List<Booking> bookings = bookingMao.getUnCompleteBookings(hotelId, DateUtil.parseDate(bookingDate));
			if (CollectionUtils.isNotEmpty(bookings)) {
				List<GetIncompleteBookingsResponce> responseList = new ArrayList<>();
				for (Booking booking : bookings) {
					GetIncompleteBookingsResponce bookingsResponce = BookingConverter.convertEntityToGetUnAllocated(booking);
					bookingsResponce.setRoomData(roomService.getById(booking.getRoomId()));
					bookingsResponce.setRoomCategoryData(roomCategoryService.getById(booking.getRoomCategoryId()));
					GetStayResponse getStayResponse = staysService.getStayByStayId(booking.getDefaultStaysId());
					if(getStayResponse==null){
						String defaultStaysId = staysService.addStayForBooking(booking);
						booking.setDefaultStaysId(defaultStaysId);
						bookingMao.setDefaultStaysId(booking.getBookingId(), defaultStaysId);
						getStayResponse = staysService.getStayByStayId(booking.getDefaultStaysId());
					}
					bookingsResponce.setStaysData(getStayResponse);
					if(!StringUtils.isEmpty(getStayResponse.getUserId())){
						bookingsResponce.setUserData(userService.getById(getStayResponse.getUserId()));
					}else{
						BookedFor bookedFor = booking.getBookedFor();
						if(bookedFor!=null){
							UserPojo userPojo=new UserPojo();
							userPojo.setFirstName(bookedFor.getName());
							bookingsResponce.setUserData(userPojo);							
						}
					}
					bookingsResponce.setCheckinData(checkinService.getById(booking.getDefaultStaysId()));
					responseList.add(bookingsResponce);
				}
				return responseList;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean allotRoom(AllocateRoomRequest request) throws AboutstaysException {
		if (request != null) {
			List<Booking> bookings = bookingMao.getByRoomId(request.getBookingId());
			Booking booking = bookingMao.getByBookingId(request.getBookingId());
			if (CollectionUtils.isNotEmpty(bookings)) {
				for (Booking savedBooking : bookings) {
					if (isDateBeetweenEqual(booking.getCheckInDate(), savedBooking.getCheckInDate(),
							savedBooking.getCheckOutDate())) {
						throw new AboutstaysException(AboutstaysResponseCode.ROOM_ALREADY_ALLOTED);
					}
					if (isDateBeetweenEqual(booking.getCheckOutDate(), savedBooking.getCheckInDate(),
							savedBooking.getCheckOutDate())) {
						throw new AboutstaysException(AboutstaysResponseCode.ROOM_ALREADY_ALLOTED);
					}
					if (isDateBeetweenEqual(savedBooking.getCheckInDate(), booking.getCheckInDate(),
							booking.getCheckOutDate())) {
						throw new AboutstaysException(AboutstaysResponseCode.ROOM_ALREADY_ALLOTED);
					}
					if (isDateBeetweenEqual(savedBooking.getCheckOutDate(), booking.getCheckInDate(),
							booking.getCheckOutDate())) {
						throw new AboutstaysException(AboutstaysResponseCode.ROOM_ALREADY_ALLOTED);
					}
				}
			}
			booking.setRoomId(request.getRoomId());
			booking.setBookedStatus(BookingStatus.ALLOCATED.getStatus());
			bookingMao.updateByBookingId(booking);
		}
		return false;
	}

	private boolean isDateBeetweenEqual(Date date, Date dateStart, Date dateEnd) {
		if ((date.after(dateStart)) && (date.before(dateEnd))) {
			return true;
		}
		return false;
	}

	@Override
	public void freezeBooking(List<String> bookingIds) {
		for (String bookingId : bookingIds) {
			changeBookingStatus(bookingId, BookingStatus.FREEZE);
		}
	}

	@Override
	public void changeBookingStatus(String bookingId, BookingStatus bookingStatus) {
		Booking booking = bookingMao.getByBookingId(bookingId);
		if (booking != null) {
			booking.setBookedStatus(bookingStatus.getStatus());
			bookingMao.updateByBookingId(booking);
		}
	}

	@Override
	public Booking getEntityById(String bookingId, String hotelId) {
		return bookingMao.getByBookingId(bookingId, hotelId);
	}

	@Override
	public void setDefaultStaysId(String bookingId, String staysId) {
		bookingMao.setDefaultStaysId(bookingId, staysId);
	}

	@Override
	public Booking getEntityById(String bookingId) {
		return bookingMao.getByBookingId(bookingId);
	}

}
