package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.MaintenanceRequestEntity;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddMaintenanceOrderRequest;
import com.aboutstays.request.dto.UpdateMaintenanceOrderRequest;
import com.aboutstays.response.dto.GetMaintenanceOrderResponse;

public class MaintenanceOrderConverter extends BaseSessionConverter{
	
	public static AddMaintenanceOrderRequest getSampleRequest(){
		AddMaintenanceOrderRequest request = new AddMaintenanceOrderRequest();
		request.setComment("comment");
		request.setDeliveryTime(234L);
		setRequestData(request);
		request.setType(123);
		request.setRequestedNow(false);
		request.setItemName("itemName");
		request.setProblemType("Not Working");
		return request;
	}

	public static MaintenanceRequestEntity convertBeanToEntity(AddMaintenanceOrderRequest request) {
		if(request != null){
			MaintenanceRequestEntity entity = new MaintenanceRequestEntity();
			entity.setComments(request.getComment());
			long currentTime = new Date().getTime();
			entity.setCreated(currentTime);
			entity.setUpdated(currentTime);
			entity.setRequestType(request.getType());
			entity.setItemName(request.getItemName());
			if(request.isRequestedNow()){
				entity.setStatus(GeneralOrderStatus.PROCESSING.getCode());
				entity.setDeliveryTime(currentTime);
			} else {
				entity.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
				entity.setDeliveryTime(request.getDeliveryTime());
			}
			entity.setProblemType(request.getProblemType());
			setPojoToEntity(entity, request);
			return entity;
		}
		return null;
	}

	public static GetMaintenanceOrderResponse convertEntityToBean(MaintenanceRequestEntity entity) {
		if(entity != null){
			GetMaintenanceOrderResponse response = new GetMaintenanceOrderResponse();
			response.setComment(entity.getComments());
			response.setDeliveryTime(entity.getDeliveryTime());
			response.setId(entity.getId());
			response.setType(entity.getRequestType());
			response.setStatus(entity.getStatus());
			response.setItemName(entity.getItemName());
			response.setProblemType(entity.getProblemType());
			setEntityToPojo(response, entity);
			return response;
		}
		return null;
	}

	public static List<GetMaintenanceOrderResponse> convertListEntityToListBean(List<MaintenanceRequestEntity> listEntity) {
		if(CollectionUtils.isNotEmpty(listEntity)){
			List<GetMaintenanceOrderResponse> listResponse = new ArrayList<>();
			for(MaintenanceRequestEntity entity : listEntity){
				if(entity == null){
					continue;
				}
				listResponse.add(convertEntityToBean(entity));
			}
			return listResponse;
		}
		return null;
	}

	public static MaintenanceRequestEntity convertUpdateRequestToEntity(UpdateMaintenanceOrderRequest request) {
		if(request != null){
			MaintenanceRequestEntity entity = new MaintenanceRequestEntity();
			entity.setComments(request.getComment());
			entity.setId(request.getId());
			entity.setCreated(new Date().getTime());
			entity.setUpdated(new Date().getTime());
			entity.setRequestType(request.getType());
			entity.setStatus(GeneralOrderStatus.UPDATED.getCode());
			entity.setDeliveryTime(request.getDeliveryTime());
			entity.setItemName(request.getItemName());
			entity.setProblemType(request.getProblemType());
			setPojoToEntity(entity, request);
			return entity;
		}
		return null;
	}

}
