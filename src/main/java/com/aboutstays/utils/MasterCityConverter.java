package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.MasterCity;
import com.aboutstays.request.dto.AddMasterCityRequest;
import com.aboutstays.response.dto.GetMasterCityResponse;

public class MasterCityConverter {

	public static MasterCity convertRequestToEntity(AddMasterCityRequest request){
		if(request==null)
			return null;
		MasterCity response = new MasterCity();
		response.setName(request.getName());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setUpdated(currentTime);
		return response;
	}
	
	public static GetMasterCityResponse convertEntityToGetResponse(MasterCity request){
		if(request==null)
			return null;
		GetMasterCityResponse response = new GetMasterCityResponse();
		response.setCityId(request.getId());
		response.setName(request.getName());
		return response;
	}
	
	public static List<GetMasterCityResponse> convertEntityListToGetList(List<MasterCity> requestList){
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<GetMasterCityResponse> responseList = new ArrayList<>(requestList.size());
		for(MasterCity masterCity : requestList){
			responseList.add(convertEntityToGetResponse(masterCity));
		}
		return responseList;
	}

	public static List<MasterCity> convertRequestListToEntityList(List<AddMasterCityRequest> requestList) {
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<MasterCity> responseList = new ArrayList<>(requestList.size());
		for(AddMasterCityRequest request : requestList){
			responseList.add(convertRequestToEntity(request));
		}
		return responseList;
	}

	public static AddMasterCityRequest getSampleRequest() {
		AddMasterCityRequest request = new AddMasterCityRequest();
		request.setName("city name");
		return request;
	}
}
