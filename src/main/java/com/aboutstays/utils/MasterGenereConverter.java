package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.MasterGenereItem;
import com.aboutstays.request.dto.AddMasterGenereRequest;
import com.aboutstays.request.dto.UpdateMasterGenereRequest;
import com.aboutstays.response.dto.GetMasterGenereResponse;

public class MasterGenereConverter {

	public static MasterGenereItem convertAddRequestToEntity(AddMasterGenereRequest request){
		if(request==null){
			return null;
		}
		MasterGenereItem response = new MasterGenereItem();
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		response.setCreated(new Date().getTime());
		response.setUpdated(response.getUpdated());
		return response;
	}
	
	public static MasterGenereItem convertUpdateRequestToEntity(UpdateMasterGenereRequest request){
		if(request==null){
			return null;
		}
		MasterGenereItem response = new MasterGenereItem();
		response.setId(request.getMasterGenereId());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		response.setUpdated(new Date().getTime());
		return response;
	}
	
	public static GetMasterGenereResponse convertEntityToGetResponse(MasterGenereItem request){
		if(request==null){
			return null;
		}
		GetMasterGenereResponse response = new GetMasterGenereResponse();
		response.setMasterGenereId(request.getId());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		return response;
	}
	
	public static List<GetMasterGenereResponse> convertEntityListToGetList(List<MasterGenereItem> requestList){
		if(CollectionUtils.isEmpty(requestList)){
			return null;
		}
		List<GetMasterGenereResponse> responseList = new ArrayList<>(requestList.size());
		for(MasterGenereItem masterGenereItem : requestList){
			responseList.add(convertEntityToGetResponse(masterGenereItem));
		}
		return responseList;
	}

	public static AddMasterGenereRequest getSampleRequest() {
		AddMasterGenereRequest request = new AddMasterGenereRequest();
		request.setLogo("logo url");
		request.setName("name");
		return request;
	}
}
