package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;

import com.aboutstays.entities.LateCheckoutRequest;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddLateCheckoutRequest;
import com.aboutstays.request.dto.UpdateLateCheckoutRequest;
import com.aboutstays.response.dto.LateCheckoutResposne;

public class LateCheckoutRequestConverter extends BaseSessionConverter{
	
	public static AddLateCheckoutRequest getSampleRequest(){
		AddLateCheckoutRequest request = new AddLateCheckoutRequest();
		request.setDeliveryTime(234L);
		request.setCheckoutDate(12345l);
		request.setCheckoutTime(12345l);
		request.setPrice(2.0);
		setRequestData(request);
		return request;
	}

	public static LateCheckoutRequest convertBeanToEntity(AddLateCheckoutRequest request) {
		if(request != null){
			LateCheckoutRequest response = new LateCheckoutRequest();
			response.setCreated(new Date().getTime());
			response.setUpdated(response.getCreated());
			response.setCheckoutDate(request.getCheckoutDate());
			response.setCheckoutTime(request.getCheckoutTime());
			response.setPrice(request.getPrice());
			if(request.isRequestedNow()){
				response.setStatus(GeneralOrderStatus.PROCESSING.getCode());
			} else {
				response.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
			}
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

	public static LateCheckoutResposne convertEntityToBean(LateCheckoutRequest request) {
		if(request != null){
			LateCheckoutResposne response = new LateCheckoutResposne();
			response.setCheckoutDate(request.getCheckoutDate());
			response.setCheckoutTime(request.getCheckoutTime());
			response.setPrice(request.getPrice());
			response.setStatus(request.getStatus());
			response.setId(request.getId());
			setEntityToPojo(response, request);
			return response;
		}
		return null;
	}

	public static List<LateCheckoutResposne> convertListEntityToListBean(List<LateCheckoutRequest> listRequest) {
		if(CollectionUtils.isNotEmpty(listRequest)){
			List<LateCheckoutResposne> listResponse = new ArrayList<>();
			for(LateCheckoutRequest request : listRequest){
				if(request == null){
					continue;
				}
				listResponse.add(convertEntityToBean(request));
			}
			return listResponse;
		}
		return null;
	}

	public static LateCheckoutRequest convertUpdateEntityToBean(UpdateLateCheckoutRequest request) {
		if(request != null){
			LateCheckoutRequest response = new LateCheckoutRequest();
			response.setUpdated(response.getCreated());
			response.setId(request.getId());
			response.setCheckoutDate(request.getCheckoutDate());
			response.setCheckoutTime(request.getCheckoutTime());
			response.setPrice(request.getPrice());
			response.setUpdated(new Date().getTime());
			response.setStatus(GeneralOrderStatus.UPDATED.getCode());
			response.setComments(request.getComment());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

	public static LateCheckoutRequest convertModifyRequest(UpdateLateCheckoutRequest request) {
		if(request != null) {
			LateCheckoutRequest response = new LateCheckoutRequest();
			if(StringUtils.isEmpty(request.getId())) {
				response.setCreated(new Date().getTime());
				response.setStatus(GeneralOrderStatus.PROCESSING.getCode());
			} else {
				response.setId(request.getId());
				response.setStatus(GeneralOrderStatus.UPDATED.getCode());
			}
			response.setUpdated(new Date().getTime());
			response.setCheckoutDate(request.getCheckoutDate());
			response.setCheckoutTime(request.getCheckoutTime());
			response.setComments(request.getComment());
			response.setDeliveryTime(request.getDeliveryTime());
			response.setPrice(request.getPrice());
			setPojoToEntity(response, request);
			return response;
		}
		return null;
	}

}
