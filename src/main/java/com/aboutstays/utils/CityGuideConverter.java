package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.CityGuide;
import com.aboutstays.pojos.Address;
import com.aboutstays.pojos.CityGuideGeneralInfo;
import com.aboutstays.pojos.CityGuideItemByType;
import com.aboutstays.request.dto.AddCityGuideRequest;
import com.aboutstays.request.dto.UpdateCityGuideRequest;
import com.aboutstays.response.dto.GetCityGuideResponse;

public class CityGuideConverter {
	
	public static AddCityGuideRequest getSampleRequest(){
		AddCityGuideRequest request = new AddCityGuideRequest();
		request.setId("hotelId");
		CityGuideGeneralInfo generalInfo = new CityGuideGeneralInfo();
		generalInfo.setEmail("email");
		generalInfo.setFax("fax");
		generalInfo.setImageUrl("imageUrl");
		generalInfo.setName("name");
		generalInfo.setPhone1("phone1");
		generalInfo.setPhone2("phone2");
		Address address = new Address();
		address.setAddressLine1("addressLine1");
		address.setAddressLine2("addressLine2");
		address.setCity("city");
		address.setCountry("country");
		address.setDescription("description");
		address.setLatitude("latitude");
		address.setLongitude("longitude");
		address.setPincode("pincode");
		address.setState("state");
		generalInfo.setAddress(address);
		List<String> additionalImageUrls = new ArrayList<>();
		additionalImageUrls.add("image1");
		additionalImageUrls.add("image2");
		additionalImageUrls.add("image3");
		generalInfo.setAdditionalImageUrls(additionalImageUrls);
		request.setGeneralInfo(generalInfo);
		
		List<CityGuideItemByType> cityGuideItemList = new ArrayList<>();
		cityGuideItemList.add(new CityGuideItemByType(1));
		cityGuideItemList.add(new CityGuideItemByType(2));
		cityGuideItemList.add(new CityGuideItemByType(3));
		cityGuideItemList.add(new CityGuideItemByType(4));
		cityGuideItemList.add(new CityGuideItemByType(5));
		cityGuideItemList.add(new CityGuideItemByType(6));
		request.setItemByTypeList(cityGuideItemList);
		return request;
	}

	public static CityGuide convertBeanToEntity(AddCityGuideRequest request) {
		if(request != null){
			CityGuide cityGuide = new CityGuide();
			cityGuide.setHotelId(request.getId());
			long currentTime = new Date().getTime();
			cityGuide.setCreated(currentTime);
			cityGuide.setUpdated(currentTime);
			cityGuide.setListCityGuideItems(request.getItemByTypeList());
			cityGuide.setGeneralInfo(request.getGeneralInfo());
			return cityGuide;
		}
		return null;
	}

	public static List<GetCityGuideResponse> convertListEntityToListBean(List<CityGuide> listCityGuide) {
		if(CollectionUtils.isNotEmpty(listCityGuide)){
			List<GetCityGuideResponse> listResponse = new ArrayList<>();
			for(CityGuide cityGuide : listCityGuide){
				if(cityGuide == null){
					continue;
				}
				listResponse.add(convertEntityToBean(cityGuide));
			}
			return listResponse;
		}
		return null;
	}

	public static GetCityGuideResponse convertEntityToBean(CityGuide cityGuide) {
		if(cityGuide != null){
			GetCityGuideResponse response = new GetCityGuideResponse();
			response.setId(cityGuide.getHotelId());
			response.setGeneralInfo(cityGuide.getGeneralInfo());
			response.setItemByTypeList(cityGuide.getListCityGuideItems());
			return response;
		}
		return null;
	}

	public static CityGuide convertUpdateBeanToEntity(UpdateCityGuideRequest request) {
		if(request != null){
			CityGuide cityGuide = new CityGuide();
			cityGuide.setGeneralInfo(request.getGeneralInfo());
			cityGuide.setHotelId(request.getId());
			cityGuide.setListCityGuideItems(request.getItemByTypeList());
			cityGuide.setUpdated(new Date().getTime());
			return cityGuide;
		}
		return null;
	}

}
