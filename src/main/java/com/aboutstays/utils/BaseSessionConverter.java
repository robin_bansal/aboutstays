package com.aboutstays.utils;

import com.aboutstays.entities.BaseSessionEntity;
import com.aboutstays.pojos.BaseSessionPojo;

public class BaseSessionConverter {
	protected static void setRequestData(BaseSessionPojo request) {
		request.setStaysId("stayId");
		request.setUserId("userId");
		request.setHotelId("hotelId");
		request.setGsId("gsId");
	}
	protected static void setPojoToEntity(BaseSessionEntity response, BaseSessionPojo request) {
		response.setUserId(request.getUserId());
		response.setHotelId(request.getHotelId());
		response.setStaysId(request.getStaysId());
		response.setGsId(request.getGsId());

	}
	protected static void setEntityToPojo(BaseSessionPojo response, BaseSessionEntity request) {
		response.setStaysId(request.getStaysId());
		response.setHotelId(request.getHotelId());
		response.setUserId(request.getUserId());
		response.setGsId(request.getGsId());

	}
	protected static void setPojoToPojo(BaseSessionPojo response, BaseSessionPojo request) {
		response.setStaysId(request.getStaysId());
		response.setHotelId(request.getHotelId());
		response.setUserId(request.getUserId());
		response.setGsId(request.getGsId());
	}

}
