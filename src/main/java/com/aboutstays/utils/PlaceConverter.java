package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Place;
import com.aboutstays.request.dto.PlaceRequest;
import com.aboutstays.response.dto.PlaceResponse;

public class PlaceConverter {

	public static Place convertRequestToEntity(PlaceRequest request, boolean isUpdate){
		if(request==null)
			return null;
		Place place = new Place();
		long currentTime = System.currentTimeMillis();
		if(isUpdate){
			place.setUpdated(currentTime);
			place.setPlaceId(request.getPlaceId());
		} else {
			place.setCreated(currentTime);
			place.setUpdated(currentTime);
			place.setPlaceId(null);
		}
		place.setParentPlaceId(request.getParentPlaceId());
		place.setPlaceName(request.getPlaceName());
		place.setPlaceType(request.getPlaceType());
		place.setIconUrl(request.getIconUrl());
		place.setImageUrl(request.getImageUrl());
		return place;
	}
	
	public static PlaceResponse convertEntityToResponse(Place request){
		if(request==null)
			return null;
		PlaceResponse response = new PlaceResponse();
		response.setPlaceId(request.getPlaceId());
		response.setParentPlaceId(request.getParentPlaceId());
		response.setPlaceName(request.getPlaceName());
		response.setPlaceType(request.getPlaceType());
		response.setIconUrl(request.getIconUrl());
		response.setImageUrl(request.getImageUrl());
		return response;
	}
	
	public static List<PlaceResponse> convertEntityListToResponseList(List<Place> requestList){
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<PlaceResponse> responseList = new ArrayList<>(requestList.size());
		for(Place place : requestList){
			responseList.add(convertEntityToResponse(place));
		}
		return responseList;
	}
	
	public static PlaceRequest getSampleRequest(){
		PlaceRequest request = new PlaceRequest();
		request.setParentPlaceId("parentPlaceId");
		request.setPlaceType(123);
		request.setPlaceName("ASIA");
		request.setIconUrl("iconUrl");
		request.setImageUrl("imageUrl");
		return request;
	}
}
