package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.AirportServices;
import com.aboutstays.enums.CarType;
import com.aboutstays.pojos.CarDetails;
import com.aboutstays.pojos.CarDetailsAndPrice;
import com.aboutstays.pojos.SupportedAirports;
import com.aboutstays.request.dto.AddAirportServiceRequest;
import com.aboutstays.request.dto.UpdateAirportServiceRequest;
import com.aboutstays.response.dto.GetAirportServiceResponse;

public class AirportServiceConverter {

	public static AddAirportServiceRequest getSampleRequest() {
		AddAirportServiceRequest request = new AddAirportServiceRequest();
		List<SupportedAirports> listSupportedAirpots = new ArrayList<>();
		request.setId("hotel id");
		List<CarDetailsAndPrice> listCarDetailsAndPrice = new ArrayList<>(); 
		CarDetails carDetails = new CarDetails(1234, "imageUrl1", 3, 60.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));
		carDetails = new CarDetails(1234, "imageUrl2", 3, 90.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));		
		listSupportedAirpots.add(new SupportedAirports("Airport1", listCarDetailsAndPrice));
		
		listCarDetailsAndPrice = new ArrayList<>(); 
		carDetails = new CarDetails(1234, "imageUrl3", 3, 80.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));
		carDetails = new CarDetails(1234, "imageUrl4", 3, 80.0, 3.5, 9.5, "INR");
		listCarDetailsAndPrice.add(new CarDetailsAndPrice(carDetails, 2000.0));
		listSupportedAirpots.add(new SupportedAirports("Airport2", listCarDetailsAndPrice));
		
		request.setSupportedAirportList(listSupportedAirpots);
		return request;
	}

	public static AirportServices convertBeanToEntity(AddAirportServiceRequest request) {
		if (request == null) {
			return null;
		}
		AirportServices service = new AirportServices();
		service.setHotelId(request.getId());
		service.setSupportedAirports(request.getSupportedAirportList());
		long currentTime = new Date().getTime();
		service.setCreated(currentTime);
		service.setUpdated(currentTime);
		return service;
	}

	public static GetAirportServiceResponse convertEntityToBean(AirportServices service) {
		if (service == null) {
			return null;
		}
		GetAirportServiceResponse response = new GetAirportServiceResponse();
		response.setId(service.getHotelId());
		response.setSupportedAirportList(service.getSupportedAirports());
		for(SupportedAirports airports : response.getSupportedAirportList()){
			if(airports != null){
				for(CarDetailsAndPrice carDetailsAndPrice : airports.getCarDetailsAndPriceList()){
					if(carDetailsAndPrice != null){
						Integer carType = carDetailsAndPrice.getCarDetails().getType();
						String carName = CarType.getNameFromServiceCode(carType);
						carDetailsAndPrice.getCarDetails().setName(carName);
					}
				}
			}

		}
		return response;
	}

	public static List<GetAirportServiceResponse> convertListEntityToListBean(List<AirportServices> listServices) {
		if (CollectionUtils.isNotEmpty(listServices)) {
			List<GetAirportServiceResponse> listResponse = new ArrayList<>();
			for (AirportServices service : listServices) {
				listResponse.add(convertEntityToBean(service));
			}
			return listResponse;
		}
		return null;
	}

	public static AirportServices convertUpdateBeanToEntity(UpdateAirportServiceRequest request) {
		if (request != null) {
			AirportServices service = new AirportServices();
			service.setHotelId(request.getId());
			service.setSupportedAirports(request.getSupportedAirportList());
			service.setUpdated(new Date().getTime());
			return service;
		}
		return null;
	}

}
