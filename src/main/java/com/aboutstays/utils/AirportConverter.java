package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Airport;
import com.aboutstays.pojos.Address;
import com.aboutstays.request.dto.AirportRequest;
import com.aboutstays.response.dto.AirportResponse;

public class AirportConverter {

	public static Airport convertRequestToEntity(AirportRequest request, boolean isUpdate){
		if(request==null){
			return null;
		}
		Airport airport = new Airport();
		long currentTime = System.currentTimeMillis();
		if(isUpdate){
			airport.setUpdated(currentTime);
			airport.setAirportId(request.getAirportId());
		} else {
			airport.setAirportId(null);
			airport.setCreated(currentTime);
			airport.setUpdated(currentTime);
		}
		airport.setAddress(request.getAddress());
		airport.setAmenities(request.getAmenities());
		airport.setCityId(request.getCityId());
		airport.setDescription(request.getDescription());
		airport.setImageUrl(request.getImageUrl());
		airport.setName(request.getName());
		airport.setType(request.getType());
		airport.setTravelOptions(request.getTravelOptions());
		return airport;
	}
	
	public static AirportResponse convertEntityToResponse(Airport request){
		if(request==null){
			return null;
		}
		AirportResponse response = new AirportResponse();
		response.setAirportId(request.getAirportId());
		response.setAddress(request.getAddress());
		response.setAmenities(request.getAmenities());
		response.setDescription(request.getDescription());
		response.setImageUrl(request.getImageUrl());
		response.setName(request.getName());
		response.setType(request.getType());
		response.setTravelOptions(request.getTravelOptions());
		response.setCityId(request.getCityId());
		return response;
	}
	
	public static AirportRequest getSampleRequest(){
		AirportRequest request = new AirportRequest();
		Address address = new Address();
		address.setAddressLine1("addressLine1");
		address.setAddressLine2("addressLine2");
		address.setDescription("address description");
		address.setLatitude("latitude");
		address.setLongitude("longitude");
		request.setAddress(address);
		List<String> amenities = new ArrayList<>();
		amenities.add("amenity1");
		amenities.add("amenity2");
		amenities.add("amenity3");
		amenities.add("amenity4");
		request.setAmenities(amenities);
		request.setCityId("cityId");
		request.setImageUrl("imageUrl");
		request.setName("airport name");
		amenities.clear();
		amenities.add("travelOption1");
		amenities.add("travelOption2");
		amenities.add("travelOption3");
		request.setTravelOptions(amenities);
		request.setType("airport type");
		return request;
	}

	public static List<AirportResponse> convertEntityListToResponseList(List<Airport> requestList) {
		if(CollectionUtils.isEmpty(requestList))
			return null;
		List<AirportResponse> responseList = new ArrayList<>(requestList.size());
		for(Airport airport : requestList){
			responseList.add(convertEntityToResponse(airport));
		}
		return responseList;
	}
}
