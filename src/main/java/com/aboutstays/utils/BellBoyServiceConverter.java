package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.BellBoyServiceEntity;
import com.aboutstays.request.dto.BellBoyServiceRequest;
import com.aboutstays.response.dto.BellBoyServiceResponse;

public class BellBoyServiceConverter {

	public static BellBoyServiceEntity convertRequestToEntity(BellBoyServiceRequest request, boolean isUpdate){
		if(request==null){
			return null;
		}
		BellBoyServiceEntity response = new BellBoyServiceEntity();
		long currentTime = System.currentTimeMillis();
		if(!isUpdate){
			response.setCreated(currentTime);
		}
		response.setUpdated(currentTime);
		response.setSlaApplicable(request.isSlaApplicable());
		response.setSlaTime(request.getSlaTime());
		response.setServiceEnabled(request.isServiceEnabled());
		response.setStartTime(request.getStartTime());
		response.setEndTime(request.getEndTime());
		response.setHotelId(request.getHotelId());
		return response;
	}
	
	public static BellBoyServiceResponse convertEntityToResponse(BellBoyServiceEntity request){
		if(request==null){
			return null;
		}
		BellBoyServiceResponse response = new BellBoyServiceResponse();
		response.setSlaApplicable(request.isSlaApplicable());
		response.setSlaTime(request.getSlaTime());
		response.setServiceEnabled(request.isServiceEnabled());
		response.setStartTime(request.getStartTime());
		response.setEndTime(request.getEndTime());
		response.setHotelId(request.getHotelId());
		return response;
	}
	
	public static List<BellBoyServiceResponse> convertEntityListToResponseList(List<BellBoyServiceEntity> requestList){
		if(CollectionUtils.isEmpty(requestList)){
			return null;
		}
		List<BellBoyServiceResponse> responseList = new ArrayList<>(requestList.size());
		for(BellBoyServiceEntity entity : requestList){
			responseList.add(convertEntityToResponse(entity));
		}
		return responseList;
	}

	public static BellBoyServiceRequest getSampleRequest() {
		BellBoyServiceRequest request = new BellBoyServiceRequest();
		request.setHotelId("hotelId");
		request.setSlaApplicable(true);
		request.setSlaTime(40);
		request.setServiceEnabled(true);
		request.setStartTime("10:00 AM");
		request.setEndTime("08:00 PM");
		return request;
	}
}
