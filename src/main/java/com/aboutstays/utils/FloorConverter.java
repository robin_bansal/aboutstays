package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Floor;
import com.aboutstays.request.dto.AddFloorRequest;
import com.aboutstays.request.dto.UpdateFloorRequest;
import com.aboutstays.response.dto.GetFloorResponse;

public class FloorConverter {
	public static AddFloorRequest getRequestSample() {
		AddFloorRequest addFloorRequest = new AddFloorRequest();
		addFloorRequest.setName("floor name");
		addFloorRequest.setCode("flore code");
		addFloorRequest.setHotelId("hotelId");
		addFloorRequest.setWingId("wingid");
		return addFloorRequest;
	}

	public static Floor convertRequestToEntity(AddFloorRequest request) {
		if (request == null) {
			return null;
		}
		Floor floor = new Floor();
		floor.setName(request.getName());
		floor.setCode(request.getCode());
		floor.setHotelId(request.getHotelId());
		floor.setWingsId(request.getWingId());
		return floor;
	}

	public static Floor convertUpdateRequestToEntity(UpdateFloorRequest request) {
		if (request == null) {
			return null;
		}
		Floor floor = new Floor();
		floor.setId(request.getFloorId());
		floor.setName(request.getName());
		floor.setCode(request.getCode());
		floor.setHotelId(request.getHotelId());
		floor.setWingsId(request.getWingId());
		return floor;
	}

	public static GetFloorResponse convertEntiityToResponse(Floor floor) {
		if (floor == null) {
			return null;
		}
		GetFloorResponse getFloorResponse = new GetFloorResponse();
		getFloorResponse.setFloorId(floor.getId());
		getFloorResponse.setName(floor.getName());
		getFloorResponse.setCode(floor.getCode());
		getFloorResponse.setHotelId(floor.getHotelId());
		getFloorResponse.setWingId(floor.getWingsId());
		return getFloorResponse;
	}

	public static List<GetFloorResponse> getResponseListFromEntityList(List<Floor> floors) {
		if(floors==null){
			return null;
		}
		List<GetFloorResponse> floorResponses=new ArrayList<>();
		for(Floor floor:floors){
			floorResponses.add(convertEntiityToResponse(floor));
		}
		return floorResponses;
	}
}
