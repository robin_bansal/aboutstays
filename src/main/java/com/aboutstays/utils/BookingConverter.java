package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.Booking;
import com.aboutstays.enums.AboutstaysResponseCode;
import com.aboutstays.enums.BookingStatus;
import com.aboutstays.exception.AboutstaysException;
import com.aboutstays.pojos.BookedBy;
import com.aboutstays.pojos.BookedByUser;
import com.aboutstays.pojos.BookedFor;
import com.aboutstays.pojos.BookedForUser;
import com.aboutstays.request.dto.CreateBookingRequest;
import com.aboutstays.request.dto.UpdateBookingRequest;
import com.aboutstays.response.dto.CreateBookingResponse;
import com.aboutstays.response.dto.GetBookingReponse;
import com.aboutstays.response.dto.GetIncompleteBookingsResponce;

public class BookingConverter {
	public static CreateBookingRequest getSampleRequest(){
		CreateBookingRequest request = new CreateBookingRequest();
		request.setHotelId("hotelId*");
		request.setReservationNumber("reservationNumber");
		request.setRoomCategoryId("roomCategoryId");
		request.setCheckInDate("checkInDate");
		request.setCheckOutDate("checkOutDate");
		BookedByUser bookedByUser = new BookedByUser();
		bookedByUser.setName("name");
		bookedByUser.setDate("currentDate");
		request.setBookedByUser(bookedByUser);
		BookedForUser bookedForUser = new BookedForUser();
		bookedForUser.setName("name");
		bookedForUser.setDate("dateOfBooking");
		bookedForUser.setPax("numberOfPerson");
		request.setBookedForUser(bookedForUser);
		return request;
	}
	
	public static Booking convertCreateBookngRequestToEntity(CreateBookingRequest createBookingRequest) throws AboutstaysException {
		if(createBookingRequest == null){
			return null;
		}
		Booking booking = new Booking();
		try{
			if(createBookingRequest.getBookedByUser() != null){
				booking.setBookedBy(new BookedBy());
				Date date = DateUtil.parseDate(createBookingRequest.getBookedByUser().getDate());
				booking.getBookedBy().setDate(date);
				booking.getBookedBy().setName(createBookingRequest.getBookedByUser().getName());
			}
			if(createBookingRequest.getBookedForUser() != null){
				Date date1 = DateUtil.parseDate(createBookingRequest.getBookedForUser().getDate());
				booking.setBookedFor(new BookedFor());
				booking.getBookedFor().setDate(date1);
				booking.getBookedFor().setName(createBookingRequest.getBookedForUser().getName());
				booking.getBookedFor().setPax(createBookingRequest.getBookedForUser().getPax());
			}
			booking.setCheckInDate(DateUtil.parseDate(createBookingRequest.getCheckInDate()));
			booking.setCheckOutDate(DateUtil.parseDate(createBookingRequest.getCheckOutDate()));
			booking.setHotelId(createBookingRequest.getHotelId());
			booking.setRoomCategoryId(createBookingRequest.getRoomCategoryId());
			booking.setReservationNumber(createBookingRequest.getReservationNumber());
		} catch(Exception e){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
		return booking;
	}

	public static CreateBookingResponse getCreateBookingResponse(Booking booking) {
		if(booking == null){
			return null;
		}
		CreateBookingResponse response = new CreateBookingResponse();
		response.setBookingId(booking.getBookingId());
		response.setReservationNumber(booking.getReservationNumber());
		return response;
	}
	
	public static GetBookingReponse convertEntityToBean(Booking booking){
		if(booking == null){
			return null;
		}
		GetBookingReponse bookingPojo = new GetBookingReponse();
		bookingPojo.setBookedByUser(new BookedByUser());
		if(booking.getBookedBy() != null){
			bookingPojo.getBookedByUser().setName(booking.getBookedBy().getName());
			bookingPojo.getBookedByUser().setDate(DateUtil.format(booking.getBookedBy().getDate()));
		}
		bookingPojo.setBookedForUser(new BookedForUser());
		if(booking.getBookedFor() != null){
			bookingPojo.getBookedForUser().setName(booking.getBookedFor().getName());
			bookingPojo.getBookedForUser().setPax(booking.getBookedFor().getPax());
			bookingPojo.getBookedForUser().setDate(DateUtil.format(booking.getBookedFor().getDate()));
		}
		bookingPojo.setCheckInDate(DateUtil.format(booking.getCheckInDate()));
		bookingPojo.setCheckOutDate(DateUtil.format(booking.getCheckOutDate()));
		bookingPojo.setReservationNumber(booking.getReservationNumber());
		bookingPojo.setBookingId(booking.getBookingId());
		return bookingPojo;
	}
	
	public static GetIncompleteBookingsResponce convertEntityToGetUnAllocated(Booking booking){
		if(booking == null){
			return null;
		}
		GetIncompleteBookingsResponce bookingPojo = new GetIncompleteBookingsResponce();
		bookingPojo.setBookedByUser(new BookedByUser());
		if(booking.getBookedBy() != null){
			bookingPojo.getBookedByUser().setName(booking.getBookedBy().getName());
			bookingPojo.getBookedByUser().setDate(DateUtil.format(booking.getBookedBy().getDate()));
		}
		bookingPojo.setBookedForUser(new BookedForUser());
		if(booking.getBookedFor() != null){
			bookingPojo.getBookedForUser().setName(booking.getBookedFor().getName());
			bookingPojo.getBookedForUser().setPax(booking.getBookedFor().getPax());
			bookingPojo.getBookedForUser().setDate(DateUtil.format(booking.getBookedFor().getDate()));
		}
		bookingPojo.setCheckInDate(DateUtil.format(booking.getCheckInDate()));
		bookingPojo.setCheckOutDate(DateUtil.format(booking.getCheckOutDate()));
		bookingPojo.setReservationNumber(booking.getReservationNumber());
		bookingPojo.setBookingId(booking.getBookingId());
		bookingPojo.setBookingStatus(booking.getBookedStatus());
		return bookingPojo;
	}
	
	public static List<GetBookingReponse>  convertListEntityToListBean(List<Booking> listBooking){
		if(CollectionUtils.isEmpty(listBooking)){
			return null;
		}
		List<GetBookingReponse> listBookingPojo = new ArrayList<>();
		for(Booking booking : listBooking){
			listBookingPojo.add(convertEntityToBean(booking));
		}
		return listBookingPojo;			
	}
	
	public static Booking convertUpdateRequestToEntity(UpdateBookingRequest request) throws AboutstaysException{
		if(request == null){
			return null;
		}
		Booking booking = new Booking();
		try{
			booking.setBookingId(request.getBookingId());
			booking.setHotelId(request.getHotelId());
			booking.setReservationNumber(request.getReservationNumber());
			booking.setCheckInDate(DateUtil.parseDate(request.getCheckInDate()));
			booking.setCheckOutDate(DateUtil.parseDate(request.getCheckOutDate()));
			booking.setRoomId(request.getRoomId());
			booking.setRoomCategoryId(request.getRoomCategoryId());
			if(request.getBookedByUser() != null){
				booking.setBookedBy(new BookedBy());
				booking.getBookedBy().setName(request.getBookedByUser().getName());
				booking.getBookedBy().setDate(DateUtil.parseDate(request.getBookedByUser().getDate()));
			}
			if(request.getBookedForUser() != null){
				booking.setBookedFor(new BookedFor());
				booking.getBookedFor().setName(request.getBookedForUser().getName());
				booking.getBookedFor().setPax(request.getBookedForUser().getPax());	
				booking.getBookedFor().setDate(DateUtil.parseDate(request.getBookedForUser().getDate()));
			}			
		} catch(Exception e){
			throw new AboutstaysException(AboutstaysResponseCode.INVALID_DATE_FORMAT);
		}
		return booking;
	}
}
