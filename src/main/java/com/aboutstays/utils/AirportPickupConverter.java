package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.AirportPickup;
import com.aboutstays.enums.GeneralOrderStatus;
import com.aboutstays.request.dto.AddAirportPickupRequest;
import com.aboutstays.request.dto.ApprovePickupRequest;
import com.aboutstays.request.dto.GetAirportPickupResponse;
import com.aboutstays.request.dto.UpdateAirportPickupRequest;

public class AirportPickupConverter extends BaseSessionConverter {

	public static AddAirportPickupRequest getSampleRequest() {
		AddAirportPickupRequest request = new AddAirportPickupRequest();
		setRequestData(request);
		request.setAirlineName("airlineName");
		request.setAirportName("airportName");
		request.setCarType(1234);
		request.setComment("comment");
		request.setFlightNumber("flightNumber");
		request.setPickupDateAndTime(1234567);
		request.setPrice(2000.0);
		request.setDrop(false);
		return request;
	}


	public static AirportPickup convertBeanToEntity(AddAirportPickupRequest request) {
		if (request != null) {
			AirportPickup pickup = new AirportPickup();
			pickup.setAirlineName(request.getAirlineName());
			pickup.setAirportName(request.getAirportName());
			pickup.setCarType(request.getCarType());
			pickup.setComment(request.getComment());
			pickup.setCreated(new Date().getTime());
			pickup.setUpdated(new Date().getTime());
			pickup.setPickupStatus(1);
			pickup.setPickupDateTime(request.getPickupDateAndTime());
			pickup.setStatus(GeneralOrderStatus.SCHEDULED.getCode());
			pickup.setFlightNumber(request.getFlightNumber());
			pickup.setPrice(request.getPrice());
			pickup.setDrop(request.getDrop());
			setPojoToEntity(pickup,request);
			return pickup;
		}
		return null;
	}



	public static GetAirportPickupResponse convertEntityToBean(AirportPickup pickup) {
		if (pickup != null) {
			GetAirportPickupResponse response = new GetAirportPickupResponse();
			response.setAirlineName(pickup.getAirlineName());
			response.setAirportName(pickup.getAirportName());
			response.setId(pickup.getId());
			response.setCarType(pickup.getCarType());
			response.setComment(pickup.getComment());
			response.setFlightNumber(pickup.getFlightNumber());
			response.setPickupDateAndTime(pickup.getPickupDateTime());
			response.setPrice(pickup.getPrice());
			response.setDrop(pickup.getDrop());
			response.setPickupStatus(pickup.getPickupStatus());
			response.setStatus(pickup.getStatus());
			setEntityToPojo(response,pickup);
			return response;
		}
		return null;
	}



	public static List<GetAirportPickupResponse> convertListEntityToListBean(List<AirportPickup> listPickup) {
		if (CollectionUtils.isNotEmpty(listPickup)) {
			List<GetAirportPickupResponse> listResponse = new ArrayList<>();
			for (AirportPickup pickup : listPickup) {
				listResponse.add(convertEntityToBean(pickup));
			}
			return listResponse;
		}
		return null;
	}

	public static AirportPickup convertUpdateRequestToEntity(UpdateAirportPickupRequest request) {
		if (request != null) {
			AirportPickup pickup = new AirportPickup();
			pickup.setAirlineName(request.getAirlineName());
			pickup.setAirportName(request.getAirportName());
			pickup.setCarType(request.getCarType());
			pickup.setComment(request.getComment());
			pickup.setPickupDateTime(request.getPickupDateAndTime());
			pickup.setFlightNumber(request.getFlightNumber());
			pickup.setPrice(request.getPrice());
			pickup.setUpdated(new Date().getTime());
			pickup.setDrop(request.getDrop());
			pickup.setPickupStatus(request.getPickupStatus());
			return pickup;
		}
		return null;
	}

	public static AirportPickup convertApprovePickupRequestToEntity(ApprovePickupRequest request) {
		if (request != null) {
			AirportPickup pickup = new AirportPickup();
			pickup.setCarId(request.getCarId());
			pickup.setDriverId(request.getDriverId());
			pickup.setPickupStatus(request.getStatus());
			return pickup;
		}
		return null;
	}

}
