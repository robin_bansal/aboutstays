package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aboutstays.entities.EntertaimentServiceEntity;
import com.aboutstays.pojos.EntertaimentItemTypeInfo;
import com.aboutstays.pojos.EntertaimentItemTypeInfoPojo;
import com.aboutstays.request.dto.AddEntertaimentServiceRequest;
import com.aboutstays.response.dto.GetEntertaimentServiceResponse;

public class EntertaimentServiceConverter {

	public static EntertaimentServiceEntity convertAddRequestToEntity(AddEntertaimentServiceRequest request){
		if(request==null){
			return null;
		}
		EntertaimentServiceEntity response = new EntertaimentServiceEntity();
		response.setHotelId(request.getServiceId());
		response.setTypeInfoList(request.getTypeInfoList());
		long currentTime = new Date().getTime();
		response.setCreated(currentTime);
		response.setUpdated(currentTime);
		return response;
	}
	
	public static GetEntertaimentServiceResponse converEntityToGetResponse(EntertaimentServiceEntity request, List<EntertaimentItemTypeInfoPojo> typeInfoList){
		if(request==null){
			return null;
		}
		GetEntertaimentServiceResponse response = new GetEntertaimentServiceResponse();
		response.setServiceId(request.getHotelId());
		response.setTypeInfoList(typeInfoList);
		return response;
	}

	public static AddEntertaimentServiceRequest getSampleRequest() {
		AddEntertaimentServiceRequest request = new AddEntertaimentServiceRequest();
		request.setServiceId("hotelId");
		List<EntertaimentItemTypeInfo> typeInfoList = new ArrayList<>();
		typeInfoList.add(new EntertaimentItemTypeInfo("genereId1"));
		typeInfoList.add(new EntertaimentItemTypeInfo("genereId2"));
		request.setTypeInfoList(typeInfoList);
		return request;
	}
}
