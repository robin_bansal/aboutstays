package com.aboutstays.utils;

import java.util.ArrayList;
import java.util.List;

import com.aboutstays.entities.Wing;
import com.aboutstays.request.dto.AddWingRequest;
import com.aboutstays.response.dto.GetWingResponse;

public class WingsConverter {
	public static AddWingRequest getSampleReuest() {
		AddWingRequest addWingRequest = new AddWingRequest();
		addWingRequest.setHotelId("hotelId");
		addWingRequest.setName("Wing Name");
		addWingRequest.setCode("code-1234");
		return addWingRequest;
	}

	public static Wing convertRequestToEntity(AddWingRequest addWingRequest) {
		if(addWingRequest==null){
			return null;
		}
		Wing wing = new Wing();
		wing.setName(addWingRequest.getName());
		wing.setCode(addWingRequest.getCode());
		wing.setHotelId(addWingRequest.getHotelId());
		return wing;
	}
	public static Wing convertRequestToEntity(GetWingResponse request) {
		if(request==null){
			return null;
		}
		Wing wing = new Wing();
		wing.setId(request.getWingId());
		wing.setName(request.getName());
		wing.setCode(request.getCode());
		wing.setHotelId(request.getHotelId());
		return wing;
	}
	public static GetWingResponse convertentityToResponse(Wing wing){
		if(wing==null){
			return null;
		}
		GetWingResponse getWingResponse=new GetWingResponse();
		getWingResponse.setWingId(wing.getId());
		getWingResponse.setName(wing.getName());
		getWingResponse.setCode(wing.getCode());
		getWingResponse.setHotelId(wing.getHotelId());
		return getWingResponse;
	}

	public static List<GetWingResponse> convertEntityListToResponseList(List<Wing> wings) {
		if(wings==null){
			return null;
		}
		List<GetWingResponse> getWingResponses=new ArrayList<>();
		for(Wing wing:wings){
			getWingResponses.add(convertentityToResponse(wing));
		}
		return getWingResponses;
	}
}
